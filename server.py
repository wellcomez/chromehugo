import http.server
import socketserver

# 设置端口
PORT = 38080

# 设置服务器的根目录
DIRECTORY = 'public'

# 定义HTTP请求处理器
class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

# 定义服务器
class MyHttpRequestHandlerServer(socketserver.TCPServer):
    allow_reuse_address = True

# 定义服务器地址
server_address = ('', PORT)

# 创建服务器实例
httpd = MyHttpRequestHandlerServer(server_address, MyHttpRequestHandler)

# 启动服务器
print(f"Serving HTTP on port {PORT} from directory {DIRECTORY}...")
httpd.serve_forever()
