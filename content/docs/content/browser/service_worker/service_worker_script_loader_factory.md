
## class ServiceWorkerScriptLoaderFactory
    : public
 Created per one running service worker for loading its scripts. This is kept
 alive while the WebServiceWorkerNetworkProvider in the renderer process is
 alive.


 This factory handles requests for scripts from service workers that were new
 (non-installed) when they started. For service workers that were already
 installed when they started, ServiceWorkerInstalledScriptsManager is used
 instead.


 This factory creates either a ServiceWorkerNewScriptLoader or a
 ServiceWorkerInstalledScriptLoader to load a script.

### ServiceWorkerScriptLoaderFactory

ServiceWorkerScriptLoaderFactory
~~~cpp
ServiceWorkerScriptLoaderFactory(const ServiceWorkerScriptLoaderFactory&) =
      delete;
~~~

### operator=

ServiceWorkerScriptLoaderFactory
    : public::operator=
~~~cpp
ServiceWorkerScriptLoaderFactory& operator=(
      const ServiceWorkerScriptLoaderFactory&) = delete;
~~~

### ~ServiceWorkerScriptLoaderFactory

ServiceWorkerScriptLoaderFactory
    : public::~ServiceWorkerScriptLoaderFactory
~~~cpp
~ServiceWorkerScriptLoaderFactory() override
~~~

### CreateLoaderAndStart

ServiceWorkerScriptLoaderFactory
    : public::CreateLoaderAndStart
~~~cpp
void CreateLoaderAndStart(
      mojo::PendingReceiver<network::mojom::URLLoader> receiver,
      int32_t request_id,
      uint32_t options,
      const network::ResourceRequest& resource_request,
      mojo::PendingRemote<network::mojom::URLLoaderClient> client,
      const net::MutableNetworkTrafficAnnotationTag& traffic_annotation)
      override;
~~~
 network::mojom::URLLoaderFactory:
### Clone

ServiceWorkerScriptLoaderFactory
    : public::Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver)
      override;
~~~

### Update

ServiceWorkerScriptLoaderFactory
    : public::Update
~~~cpp
void Update(scoped_refptr<network::SharedURLLoaderFactory> loader_factory);
~~~

### CopyScript

ServiceWorkerScriptLoaderFactory
    : public::CopyScript
~~~cpp
void CopyScript(const GURL& url,
                  int64_t resource_id,
                  base::OnceCallback<void(int64_t, net::Error)> callback,
                  int64_t new_resource_id);
~~~
 The callback is called once the copy is done. It normally runs
 asynchronously, and would be synchronous if the operation completes
 synchronously. The first parameter of the callback is the new resource id
 and the second parameter is the result of the operation. net::OK means
 success.

### OnCopyScriptFinished

ServiceWorkerScriptLoaderFactory
    : public::OnCopyScriptFinished
~~~cpp
void OnCopyScriptFinished(
      mojo::PendingReceiver<network::mojom::URLLoader> receiver,
      uint32_t options,
      const network::ResourceRequest& resource_request,
      mojo::PendingRemote<network::mojom::URLLoaderClient> client,
      int64_t new_resource_id,
      net::Error error);
~~~
 This method is called to notify that the operation triggered by
 CopyScript() completed.


 If the copy operation is successful, a ServiceWorkerInstalledScriptLoader
 would be created to load the new copy.

### OnResourceIdAssignedForNewScriptLoader

ServiceWorkerScriptLoaderFactory
    : public::OnResourceIdAssignedForNewScriptLoader
~~~cpp
void OnResourceIdAssignedForNewScriptLoader(
      mojo::PendingReceiver<network::mojom::URLLoader> receiver,
      int32_t request_id,
      uint32_t options,
      const network::ResourceRequest& resource_request,
      mojo::PendingRemote<network::mojom::URLLoaderClient> client,
      const net::MutableNetworkTrafficAnnotationTag& traffic_annotation,
      int64_t resource_id);
~~~
