### GetAllWebContents

GetAllWebContents
~~~cpp
static std::vector<WebContentsImpl*> GetAllWebContents();
~~~

### FromFrameTreeNode

FromFrameTreeNode
~~~cpp
static WebContentsImpl* FromFrameTreeNode(
      const FrameTreeNode* frame_tree_node);
~~~

### FromRenderFrameHostID

FromRenderFrameHostID
~~~cpp
static WebContents* FromRenderFrameHostID(
      GlobalRenderFrameHostId render_frame_host_id);
~~~

### FromRenderFrameHostID

FromRenderFrameHostID
~~~cpp
static WebContents* FromRenderFrameHostID(int render_process_host_id,
                                            int render_frame_host_id);
~~~

### FromFrameTreeNodeId

FromFrameTreeNodeId
~~~cpp
static WebContents* FromFrameTreeNodeId(int frame_tree_node_id);
~~~

### FromOuterFrameTreeNode

FromOuterFrameTreeNode
~~~cpp
static WebContentsImpl* FromOuterFrameTreeNode(
      const FrameTreeNode* frame_tree_node);
~~~

### FromRenderWidgetHostImpl

FromRenderWidgetHostImpl
~~~cpp
static WebContentsImpl* FromRenderWidgetHostImpl(RenderWidgetHostImpl* rwh);
~~~

### FromRenderFrameHostImpl

FromRenderFrameHostImpl
~~~cpp
static WebContentsImpl* FromRenderFrameHostImpl(RenderFrameHostImpl* rfh);
~~~

### Init

Init
~~~cpp
virtual void Init(const WebContents::CreateParams& params,
                    blink::FramePolicy primary_main_frame_policy);
~~~
 Complex initialization here. Specifically needed to avoid having
 members call back into our virtual functions in the constructor.

 The primary main frame policy might be passed down as it is inherited from
 the opener when WebContents is created with an opener.

### save_package

save_package
~~~cpp
SavePackage* save_package() const { return save_package_.get(); }
~~~
 Returns the SavePackage which manages the page saving job. May be NULL.

### SetBrowserPluginGuest

SetBrowserPluginGuest
~~~cpp
void SetBrowserPluginGuest(std::unique_ptr<BrowserPluginGuest> guest);
~~~
 Sets a BrowserPluginGuest object for this WebContents. If this WebContents
 has a BrowserPluginGuest then that implies that it is being hosted by
 a BrowserPlugin object in an embedder renderer process.

### GetBrowserPluginEmbedder

GetBrowserPluginEmbedder
~~~cpp
BrowserPluginEmbedder* GetBrowserPluginEmbedder() const;
~~~
 Returns embedder browser plugin object, or NULL if this WebContents is not
 an embedder.

### GetBrowserPluginGuest

GetBrowserPluginGuest
~~~cpp
BrowserPluginGuest* GetBrowserPluginGuest() const;
~~~
 Returns guest browser plugin object, or nullptr if this WebContents is not
 for guest.

### CreateBrowserPluginEmbedderIfNecessary

CreateBrowserPluginEmbedderIfNecessary
~~~cpp
void CreateBrowserPluginEmbedderIfNecessary();
~~~
 Creates a BrowserPluginEmbedder object for this WebContents if one doesn't
 already exist.

### CancelActiveAndPendingDialogs

CancelActiveAndPendingDialogs
~~~cpp
void CancelActiveAndPendingDialogs();
~~~
 Cancels modal dialogs in this WebContents, as well as in any browser
 plugins it is hosting.

### DragSourceEndedAt

DragSourceEndedAt
~~~cpp
void DragSourceEndedAt(float client_x,
                         float client_y,
                         float screen_x,
                         float screen_y,
                         ui::mojom::DragOperation operation,
                         RenderWidgetHost* source_rwh);
~~~
 Informs the render view host and the BrowserPluginEmbedder, if present, of
 a Drag Source End.

### LoadStateChanged

LoadStateChanged
~~~cpp
void LoadStateChanged(network::mojom::LoadInfoPtr load_info);
~~~
 Notification that the RenderViewHost's load state changed.

### SetVisibilityAndNotifyObservers

SetVisibilityAndNotifyObservers
~~~cpp
void SetVisibilityAndNotifyObservers(Visibility visibility);
~~~
 Updates the visibility and notifies observers. Note that this is
 distinct from UpdateWebContentsVisibility which may also update the
 visibility of renderer-side objects.

### NotifyWebContentsFocused

NotifyWebContentsFocused
~~~cpp
void NotifyWebContentsFocused(RenderWidgetHost* render_widget_host);
~~~
 Notify observers that the web contents has been focused.

### NotifyWebContentsLostFocus

NotifyWebContentsLostFocus
~~~cpp
void NotifyWebContentsLostFocus(RenderWidgetHost* render_widget_host);
~~~
 Notify observers that the web contents has lost focus.

### GetView

GetView
~~~cpp
WebContentsView* GetView() const;
~~~

### OnScreensChange

OnScreensChange
~~~cpp
void OnScreensChange(bool is_multi_screen_changed);
~~~
 Called on screen information changes; |is_multi_screen_changed| is true iff
 the plurality of connected screens changed (e.g. 1 screen <-> 2 screens).

### OnScreenOrientationChange

OnScreenOrientationChange
~~~cpp
void OnScreenOrientationChange();
~~~

### GetScreenOrientationProviderForTesting

GetScreenOrientationProviderForTesting
~~~cpp
ScreenOrientationProvider* GetScreenOrientationProviderForTesting() const {
    return screen_orientation_provider_.get();
  }
~~~

### AddAccessibilityMode

AddAccessibilityMode
~~~cpp
void AddAccessibilityMode(ui::AXMode mode);
~~~
 Adds the given accessibility mode to the current accessibility mode
 bitmap.

### UpdateZoom

UpdateZoom
~~~cpp
void UpdateZoom();
~~~
 Sets the zoom level for frames associated with this WebContents.

### UpdateZoomIfNecessary

UpdateZoomIfNecessary
~~~cpp
void UpdateZoomIfNecessary(const std::string& scheme,
                             const std::string& host);
~~~
 Sets the zoom level for frames associated with this WebContents if it
 matches |host| and (if non-empty) |scheme|. Matching is done on the
 last committed entry.

### GetFocusedWebContents

GetFocusedWebContents
~~~cpp
WebContentsImpl* GetFocusedWebContents();
~~~
 Returns the focused WebContents.

 If there are multiple inner/outer WebContents (when embedding <webview>,
 <guestview>, ...) returns the single one containing the currently focused
 frame. Otherwise, returns this WebContents.

### GetFocusedFrameTree

GetFocusedFrameTree
~~~cpp
FrameTree* GetFocusedFrameTree();
~~~
 Returns the focused FrameTree. For MPArch we may return a different
 focused frame tree even though the focused WebContents is the same.

### SetFocusToLocationBar

SetFocusToLocationBar
~~~cpp
void SetFocusToLocationBar();
~~~
 TODO(lukasza): Maybe this method can be removed altogether (so that the
 focus of the location bar is only set in the //chrome layer).

### GetWebContentsAndAllInner

GetWebContentsAndAllInner
~~~cpp
std::vector<WebContentsImpl*> GetWebContentsAndAllInner();
~~~
 Returns a vector containing this WebContents and all inner WebContents
 within it (recursively).

### GetPrimaryFrameTree

GetPrimaryFrameTree
~~~cpp
FrameTree& GetPrimaryFrameTree() { return primary_frame_tree_; }
~~~
 Returns the primary FrameTree for this WebContents (as opposed to the
 ones held by MPArch features like Prerender or Fenced Frame).

 See docs/frame_trees.md for more details.

### HasAccessedInitialDocument

HasAccessedInitialDocument
~~~cpp
bool HasAccessedInitialDocument();
~~~
 Whether the initial empty page of this view has been accessed by another
 page, making it unsafe to show the pending URL. Always false after the
 first commit.

 TODO(https://crbug.com/1170277): Rename to HasAccessedInitialMainDocument
### SetPrimaryMainFrameImportance

SetPrimaryMainFrameImportance
~~~cpp
void SetPrimaryMainFrameImportance(ChildProcessImportance importance);
~~~

### GetTitleForMediaControls

GetTitleForMediaControls
~~~cpp
std::string GetTitleForMediaControls();
~~~
 Returns the human-readable name for title in Media Controls.

 If the returned value is an empty string, it means that there is no
 human-readable name.

### GetDelegate

GetDelegate
~~~cpp
WebContentsDelegate* GetDelegate() override;
~~~
###  WebContents 
------------------------------------------------------
### SetDelegate

SetDelegate
~~~cpp
void SetDelegate(WebContentsDelegate* delegate) override;
~~~

### GetController

GetController
~~~cpp
NavigationControllerImpl& GetController() override;
~~~

### GetBrowserContext

GetBrowserContext
~~~cpp
BrowserContext* GetBrowserContext() override;
~~~

### GetWeakPtr

GetWeakPtr
~~~cpp
base::WeakPtr<WebContents> GetWeakPtr() override;
~~~

### GetURL

GetURL
~~~cpp
const GURL& GetURL() override;
~~~

### GetVisibleURL

GetVisibleURL
~~~cpp
const GURL& GetVisibleURL() override;
~~~

### GetLastCommittedURL

GetLastCommittedURL
~~~cpp
const GURL& GetLastCommittedURL() override;
~~~

### GetPrimaryMainFrame

GetPrimaryMainFrame
~~~cpp
RenderFrameHostImpl* GetPrimaryMainFrame() override;
~~~

### GetPrimaryPage

GetPrimaryPage
~~~cpp
PageImpl& GetPrimaryPage() override;
~~~

### GetFocusedFrame

GetFocusedFrame
~~~cpp
RenderFrameHostImpl* GetFocusedFrame() override;
~~~

### IsPrerenderedFrame

IsPrerenderedFrame
~~~cpp
bool IsPrerenderedFrame(int frame_tree_node_id) override;
~~~

### UnsafeFindFrameByFrameTreeNodeId

UnsafeFindFrameByFrameTreeNodeId
~~~cpp
RenderFrameHostImpl* UnsafeFindFrameByFrameTreeNodeId(
      int frame_tree_node_id) override;
~~~

### ForEachRenderFrameHostWithAction

ForEachRenderFrameHostWithAction
~~~cpp
void ForEachRenderFrameHostWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHost*)> on_frame)
      override;
~~~

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
void ForEachRenderFrameHost(
      base::FunctionRef<void(RenderFrameHost*)> on_frame) override;
~~~

### GetRenderViewHost

GetRenderViewHost
~~~cpp
RenderViewHostImpl* GetRenderViewHost() override;
~~~

### GetRenderWidgetHostView

GetRenderWidgetHostView
~~~cpp
RenderWidgetHostView* GetRenderWidgetHostView() override;
~~~

### GetTopLevelRenderWidgetHostView

GetTopLevelRenderWidgetHostView
~~~cpp
RenderWidgetHostView* GetTopLevelRenderWidgetHostView() override;
~~~

### ClosePage

ClosePage
~~~cpp
void ClosePage() override;
~~~

### GetThemeColor

GetThemeColor
~~~cpp
absl::optional<SkColor> GetThemeColor() override;
~~~

### GetBackgroundColor

GetBackgroundColor
~~~cpp
absl::optional<SkColor> GetBackgroundColor() override;
~~~

### SetPageBaseBackgroundColor

SetPageBaseBackgroundColor
~~~cpp
void SetPageBaseBackgroundColor(absl::optional<SkColor> color) override;
~~~

### SetColorProviderSource

SetColorProviderSource
~~~cpp
void SetColorProviderSource(ui::ColorProviderSource* source) override;
~~~

### GetWebUI

GetWebUI
~~~cpp
WebUI* GetWebUI() override;
~~~

### SetUserAgentOverride

SetUserAgentOverride
~~~cpp
void SetUserAgentOverride(const blink::UserAgentOverride& ua_override,
                            bool override_in_new_tabs) override;
~~~

### SetRendererInitiatedUserAgentOverrideOption

SetRendererInitiatedUserAgentOverrideOption
~~~cpp
void SetRendererInitiatedUserAgentOverrideOption(
      NavigationController::UserAgentOverrideOption option) override;
~~~

### GetUserAgentOverride

GetUserAgentOverride
~~~cpp
const blink::UserAgentOverride& GetUserAgentOverride() override;
~~~

### ShouldOverrideUserAgentForRendererInitiatedNavigation

ShouldOverrideUserAgentForRendererInitiatedNavigation
~~~cpp
bool ShouldOverrideUserAgentForRendererInitiatedNavigation() override;
~~~

### EnableWebContentsOnlyAccessibilityMode

EnableWebContentsOnlyAccessibilityMode
~~~cpp
void EnableWebContentsOnlyAccessibilityMode() override;
~~~

### IsWebContentsOnlyAccessibilityModeForTesting

IsWebContentsOnlyAccessibilityModeForTesting
~~~cpp
bool IsWebContentsOnlyAccessibilityModeForTesting() override;
~~~

### IsFullAccessibilityModeForTesting

IsFullAccessibilityModeForTesting
~~~cpp
bool IsFullAccessibilityModeForTesting() override;
~~~

### GetTitle

GetTitle
~~~cpp
const std::u16string& GetTitle() override;
~~~

### UpdateTitleForEntry

UpdateTitleForEntry
~~~cpp
void UpdateTitleForEntry(NavigationEntry* entry,
                           const std::u16string& title) override;
~~~

### GetSiteInstance

GetSiteInstance
~~~cpp
SiteInstanceImpl* GetSiteInstance() override;
~~~

### IsLoading

IsLoading
~~~cpp
bool IsLoading() override;
~~~

### GetLoadProgress

GetLoadProgress
~~~cpp
double GetLoadProgress() override;
~~~

### ShouldShowLoadingUI

ShouldShowLoadingUI
~~~cpp
bool ShouldShowLoadingUI() override;
~~~

### IsDocumentOnLoadCompletedInPrimaryMainFrame

IsDocumentOnLoadCompletedInPrimaryMainFrame
~~~cpp
bool IsDocumentOnLoadCompletedInPrimaryMainFrame() override;
~~~

### IsWaitingForResponse

IsWaitingForResponse
~~~cpp
bool IsWaitingForResponse() override;
~~~

### GetLoadState

GetLoadState
~~~cpp
const net::LoadStateWithParam& GetLoadState() override;
~~~

### GetLoadStateHost

GetLoadStateHost
~~~cpp
const std::u16string& GetLoadStateHost() override;
~~~

### RequestAXTreeSnapshot

RequestAXTreeSnapshot
~~~cpp
void RequestAXTreeSnapshot(AXTreeSnapshotCallback callback,
                             ui::AXMode ax_mode,
                             bool exclude_offscreen,
                             size_t max_nodes,
                             base::TimeDelta timeout) override;
~~~

### GetUploadSize

GetUploadSize
~~~cpp
uint64_t GetUploadSize() override;
~~~

### GetUploadPosition

GetUploadPosition
~~~cpp
uint64_t GetUploadPosition() override;
~~~

### GetEncoding

GetEncoding
~~~cpp
const std::string& GetEncoding() override;
~~~

### WasDiscarded

WasDiscarded
~~~cpp
bool WasDiscarded() override;
~~~

### SetWasDiscarded

SetWasDiscarded
~~~cpp
void SetWasDiscarded(bool was_discarded) override;
~~~

### IncrementCapturerCount

IncrementCapturerCount
~~~cpp
[[nodiscard]] base::ScopedClosureRunner IncrementCapturerCount(
      const gfx::Size& capture_size,
      bool stay_hidden,
      bool stay_awake,
      bool is_activity = true) override;
~~~

### GetCaptureHandleConfig

GetCaptureHandleConfig
~~~cpp
const blink::mojom::CaptureHandleConfig& GetCaptureHandleConfig() override;
~~~

### IsBeingCaptured

IsBeingCaptured
~~~cpp
bool IsBeingCaptured() override;
~~~

### IsBeingVisiblyCaptured

IsBeingVisiblyCaptured
~~~cpp
bool IsBeingVisiblyCaptured() override;
~~~

### IsAudioMuted

IsAudioMuted
~~~cpp
bool IsAudioMuted() override;
~~~

### SetAudioMuted

SetAudioMuted
~~~cpp
void SetAudioMuted(bool mute) override;
~~~

### IsCurrentlyAudible

IsCurrentlyAudible
~~~cpp
bool IsCurrentlyAudible() override;
~~~

### IsConnectedToBluetoothDevice

IsConnectedToBluetoothDevice
~~~cpp
bool IsConnectedToBluetoothDevice() override;
~~~

### IsScanningForBluetoothDevices

IsScanningForBluetoothDevices
~~~cpp
bool IsScanningForBluetoothDevices() override;
~~~

### IsConnectedToSerialPort

IsConnectedToSerialPort
~~~cpp
bool IsConnectedToSerialPort() override;
~~~

### IsConnectedToHidDevice

IsConnectedToHidDevice
~~~cpp
bool IsConnectedToHidDevice() override;
~~~

### IsConnectedToUsbDevice

IsConnectedToUsbDevice
~~~cpp
bool IsConnectedToUsbDevice() override;
~~~

### HasFileSystemAccessHandles

HasFileSystemAccessHandles
~~~cpp
bool HasFileSystemAccessHandles() override;
~~~

### HasPictureInPictureVideo

HasPictureInPictureVideo
~~~cpp
bool HasPictureInPictureVideo() override;
~~~

### HasPictureInPictureDocument

HasPictureInPictureDocument
~~~cpp
bool HasPictureInPictureDocument() override;
~~~

### IsCrashed

IsCrashed
~~~cpp
bool IsCrashed() override;
~~~

### GetCrashedStatus

GetCrashedStatus
~~~cpp
base::TerminationStatus GetCrashedStatus() override;
~~~

### GetCrashedErrorCode

GetCrashedErrorCode
~~~cpp
int GetCrashedErrorCode() override;
~~~

### IsBeingDestroyed

IsBeingDestroyed
~~~cpp
bool IsBeingDestroyed() override;
~~~

### NotifyNavigationStateChanged

NotifyNavigationStateChanged
~~~cpp
void NotifyNavigationStateChanged(InvalidateTypes changed_flags) override;
~~~

### OnAudioStateChanged

OnAudioStateChanged
~~~cpp
void OnAudioStateChanged() override;
~~~

### GetLastActiveTime

GetLastActiveTime
~~~cpp
base::TimeTicks GetLastActiveTime() override;
~~~

### WasShown

WasShown
~~~cpp
void WasShown() override;
~~~

### WasHidden

WasHidden
~~~cpp
void WasHidden() override;
~~~

### WasOccluded

WasOccluded
~~~cpp
void WasOccluded() override;
~~~

### GetVisibility

GetVisibility
~~~cpp
Visibility GetVisibility() override;
~~~

### NeedToFireBeforeUnloadOrUnloadEvents

NeedToFireBeforeUnloadOrUnloadEvents
~~~cpp
bool NeedToFireBeforeUnloadOrUnloadEvents() override;
~~~

### DispatchBeforeUnload

DispatchBeforeUnload
~~~cpp
void DispatchBeforeUnload(bool auto_cancel) override;
~~~

### AttachInnerWebContents

AttachInnerWebContents
~~~cpp
void AttachInnerWebContents(
      std::unique_ptr<WebContents> inner_web_contents,
      RenderFrameHost* render_frame_host,
      mojo::PendingAssociatedRemote<blink::mojom::RemoteFrame> remote_frame,
      mojo::PendingAssociatedReceiver<blink::mojom::RemoteFrameHost>
          remote_frame_host_receiver,
      bool is_full_page) override;
~~~

### IsInnerWebContentsForGuest

IsInnerWebContentsForGuest
~~~cpp
bool IsInnerWebContentsForGuest() override;
~~~

### IsPortal

IsPortal
~~~cpp
bool IsPortal() override;
~~~

### GetPortalHostWebContents

GetPortalHostWebContents
~~~cpp
WebContentsImpl* GetPortalHostWebContents() override;
~~~

### GetOuterWebContentsFrame

GetOuterWebContentsFrame
~~~cpp
RenderFrameHostImpl* GetOuterWebContentsFrame() override;
~~~

### GetOuterWebContents

GetOuterWebContents
~~~cpp
WebContentsImpl* GetOuterWebContents() override;
~~~

### GetOutermostWebContents

GetOutermostWebContents
~~~cpp
WebContentsImpl* GetOutermostWebContents() override;
~~~

### GetInnerWebContents

GetInnerWebContents
~~~cpp
std::vector<WebContents*> GetInnerWebContents() override;
~~~

### GetResponsibleWebContents

GetResponsibleWebContents
~~~cpp
WebContentsImpl* GetResponsibleWebContents() override;
~~~

### DidChangeVisibleSecurityState

DidChangeVisibleSecurityState
~~~cpp
void DidChangeVisibleSecurityState() override;
~~~

### SyncRendererPrefs

SyncRendererPrefs
~~~cpp
void SyncRendererPrefs() override;
~~~

### Stop

Stop
~~~cpp
void Stop() override;
~~~

### SetPageFrozen

SetPageFrozen
~~~cpp
void SetPageFrozen(bool frozen) override;
~~~

### Clone

Clone
~~~cpp
std::unique_ptr<WebContents> Clone() override;
~~~

### ReloadFocusedFrame

ReloadFocusedFrame
~~~cpp
void ReloadFocusedFrame() override;
~~~

### Undo

Undo
~~~cpp
void Undo() override;
~~~

### Redo

Redo
~~~cpp
void Redo() override;
~~~

### Cut

Cut
~~~cpp
void Cut() override;
~~~

### Copy

Copy
~~~cpp
void Copy() override;
~~~

### CopyToFindPboard

CopyToFindPboard
~~~cpp
void CopyToFindPboard() override;
~~~

### CenterSelection

CenterSelection
~~~cpp
void CenterSelection() override;
~~~

### Paste

Paste
~~~cpp
void Paste() override;
~~~

### PasteAndMatchStyle

PasteAndMatchStyle
~~~cpp
void PasteAndMatchStyle() override;
~~~

### Delete

Delete
~~~cpp
void Delete() override;
~~~

### SelectAll

SelectAll
~~~cpp
void SelectAll() override;
~~~

### CollapseSelection

CollapseSelection
~~~cpp
void CollapseSelection() override;
~~~

### ScrollToTopOfDocument

ScrollToTopOfDocument
~~~cpp
void ScrollToTopOfDocument() override;
~~~

### ScrollToBottomOfDocument

ScrollToBottomOfDocument
~~~cpp
void ScrollToBottomOfDocument() override;
~~~

### Replace

Replace
~~~cpp
void Replace(const std::u16string& word) override;
~~~

### ReplaceMisspelling

ReplaceMisspelling
~~~cpp
void ReplaceMisspelling(const std::u16string& word) override;
~~~

### NotifyContextMenuClosed

NotifyContextMenuClosed
~~~cpp
void NotifyContextMenuClosed(const GURL& link_followed) override;
~~~

### ExecuteCustomContextMenuCommand

ExecuteCustomContextMenuCommand
~~~cpp
void ExecuteCustomContextMenuCommand(int action,
                                       const GURL& link_followed) override;
~~~

### GetNativeView

GetNativeView
~~~cpp
gfx::NativeView GetNativeView() override;
~~~

### GetContentNativeView

GetContentNativeView
~~~cpp
gfx::NativeView GetContentNativeView() override;
~~~

### GetTopLevelNativeWindow

GetTopLevelNativeWindow
~~~cpp
gfx::NativeWindow GetTopLevelNativeWindow() override;
~~~

### GetContainerBounds

GetContainerBounds
~~~cpp
gfx::Rect GetContainerBounds() override;
~~~

### GetViewBounds

GetViewBounds
~~~cpp
gfx::Rect GetViewBounds() override;
~~~

### GetDropData

GetDropData
~~~cpp
DropData* GetDropData() override;
~~~

### Focus

Focus
~~~cpp
void Focus() override;
~~~

### SetInitialFocus

SetInitialFocus
~~~cpp
void SetInitialFocus() override;
~~~

### StoreFocus

StoreFocus
~~~cpp
void StoreFocus() override;
~~~

### RestoreFocus

RestoreFocus
~~~cpp
void RestoreFocus() override;
~~~

### FocusThroughTabTraversal

FocusThroughTabTraversal
~~~cpp
void FocusThroughTabTraversal(bool reverse) override;
~~~

### IsSavable

IsSavable
~~~cpp
bool IsSavable() override;
~~~

### OnSavePage

OnSavePage
~~~cpp
void OnSavePage() override;
~~~

### SavePage

SavePage
~~~cpp
bool SavePage(const base::FilePath& main_file,
                const base::FilePath& dir_path,
                SavePageType save_type) override;
~~~

### SaveFrame

SaveFrame
~~~cpp
void SaveFrame(const GURL& url,
                 const Referrer& referrer,
                 RenderFrameHost* rfh) override;
~~~

### SaveFrameWithHeaders

SaveFrameWithHeaders
~~~cpp
void SaveFrameWithHeaders(const GURL& url,
                            const Referrer& referrer,
                            const std::string& headers,
                            const std::u16string& suggested_filename,
                            RenderFrameHost* rfh) override;
~~~

### GenerateMHTML

GenerateMHTML
~~~cpp
void GenerateMHTML(const MHTMLGenerationParams& params,
                     base::OnceCallback<void(int64_t)> callback) override;
~~~

### GenerateMHTMLWithResult

GenerateMHTMLWithResult
~~~cpp
void GenerateMHTMLWithResult(
      const MHTMLGenerationParams& params,
      MHTMLGenerationResult::GenerateMHTMLCallback callback) override;
~~~

### GetContentsMimeType

GetContentsMimeType
~~~cpp
const std::string& GetContentsMimeType() override;
~~~

### GetMutableRendererPrefs

GetMutableRendererPrefs
~~~cpp
blink::RendererPreferences* GetMutableRendererPrefs() override;
~~~

### Close

Close
~~~cpp
void Close() override;
~~~

### SetClosedByUserGesture

SetClosedByUserGesture
~~~cpp
void SetClosedByUserGesture(bool value) override;
~~~

### GetClosedByUserGesture

GetClosedByUserGesture
~~~cpp
bool GetClosedByUserGesture() override;
~~~

### GetMinimumZoomPercent

GetMinimumZoomPercent
~~~cpp
int GetMinimumZoomPercent() override;
~~~

### GetMaximumZoomPercent

GetMaximumZoomPercent
~~~cpp
int GetMaximumZoomPercent() override;
~~~

### SetPageScale

SetPageScale
~~~cpp
void SetPageScale(float page_scale_factor) override;
~~~

### GetPreferredSize

GetPreferredSize
~~~cpp
gfx::Size GetPreferredSize() override;
~~~

### GotResponseToLockMouseRequest

GotResponseToLockMouseRequest
~~~cpp
bool GotResponseToLockMouseRequest(
      blink::mojom::PointerLockResult result) override;
~~~

### GotLockMousePermissionResponse

GotLockMousePermissionResponse
~~~cpp
void GotLockMousePermissionResponse(bool allowed) override;
~~~

### DropMouseLockForTesting

DropMouseLockForTesting
~~~cpp
void DropMouseLockForTesting() override;
~~~

### GotResponseToKeyboardLockRequest

GotResponseToKeyboardLockRequest
~~~cpp
bool GotResponseToKeyboardLockRequest(bool allowed) override;
~~~

### HasOpener

HasOpener
~~~cpp
bool HasOpener() override;
~~~

### GetOpener

GetOpener
~~~cpp
RenderFrameHostImpl* GetOpener() override;
~~~

### HasLiveOriginalOpenerChain

HasLiveOriginalOpenerChain
~~~cpp
bool HasLiveOriginalOpenerChain() override;
~~~

### GetFirstWebContentsInLiveOriginalOpenerChain

GetFirstWebContentsInLiveOriginalOpenerChain
~~~cpp
WebContents* GetFirstWebContentsInLiveOriginalOpenerChain() override;
~~~

### DidChooseColorInColorChooser

DidChooseColorInColorChooser
~~~cpp
void DidChooseColorInColorChooser(SkColor color) override;
~~~

### DidEndColorChooser

DidEndColorChooser
~~~cpp
void DidEndColorChooser() override;
~~~

### DownloadImage

DownloadImage
~~~cpp
int DownloadImage(const GURL& url,
                    bool is_favicon,
                    const gfx::Size& preferred_size,
                    uint32_t max_bitmap_size,
                    bool bypass_cache,
                    ImageDownloadCallback callback) override;
~~~
 BUILDFLAG(IS_ANDROID) || BUILDFLAG(IS_MAC)
### DownloadImageInFrame

DownloadImageInFrame
~~~cpp
int DownloadImageInFrame(
      const GlobalRenderFrameHostId& initiator_frame_routing_id,
      const GURL& url,
      bool is_favicon,
      const gfx::Size& preferred_size,
      uint32_t max_bitmap_size,
      bool bypass_cache,
      WebContents::ImageDownloadCallback callback) override;
~~~

### Find

Find
~~~cpp
void Find(int request_id,
            const std::u16string& search_text,
            blink::mojom::FindOptionsPtr options,
            bool skip_delay = false) override;
~~~

### StopFinding

StopFinding
~~~cpp
void StopFinding(StopFindAction action) override;
~~~

### WasEverAudible

WasEverAudible
~~~cpp
bool WasEverAudible() override;
~~~

### IsFullscreen

IsFullscreen
~~~cpp
bool IsFullscreen() override;
~~~

### ShouldShowStaleContentOnEviction

ShouldShowStaleContentOnEviction
~~~cpp
bool ShouldShowStaleContentOnEviction() override;
~~~

### ExitFullscreen

ExitFullscreen
~~~cpp
void ExitFullscreen(bool will_cause_resize) override;
~~~

### ForSecurityDropFullscreen

ForSecurityDropFullscreen
~~~cpp
[[nodiscard]] base::ScopedClosureRunner ForSecurityDropFullscreen(
      int64_t display_id = display::kInvalidDisplayId) override;
~~~

### ResumeLoadingCreatedWebContents

ResumeLoadingCreatedWebContents
~~~cpp
void ResumeLoadingCreatedWebContents() override;
~~~

### SetIsOverlayContent

SetIsOverlayContent
~~~cpp
void SetIsOverlayContent(bool is_overlay_content) override;
~~~

### IsFocusedElementEditable

IsFocusedElementEditable
~~~cpp
bool IsFocusedElementEditable() override;
~~~

### ClearFocusedElement

ClearFocusedElement
~~~cpp
void ClearFocusedElement() override;
~~~

### IsShowingContextMenu

IsShowingContextMenu
~~~cpp
bool IsShowingContextMenu() override;
~~~

### SetShowingContextMenu

SetShowingContextMenu
~~~cpp
void SetShowingContextMenu(bool showing) override;
~~~

### GetAudioGroupId

GetAudioGroupId
~~~cpp
base::UnguessableToken GetAudioGroupId() override;
~~~

### CompletedFirstVisuallyNonEmptyPaint

CompletedFirstVisuallyNonEmptyPaint
~~~cpp
bool CompletedFirstVisuallyNonEmptyPaint() override;
~~~

### UpdateFaviconURL

UpdateFaviconURL
~~~cpp
void UpdateFaviconURL(
      RenderFrameHostImpl* source,
      const std::vector<blink::mojom::FaviconURLPtr>& candidates) override;
~~~

### GetFaviconURLs

GetFaviconURLs
~~~cpp
const std::vector<blink::mojom::FaviconURLPtr>& GetFaviconURLs() override;
~~~

### Resize

Resize
~~~cpp
void Resize(const gfx::Rect& new_bounds) override;
~~~

### GetSize

GetSize
~~~cpp
gfx::Size GetSize() override;
~~~

### UpdateWindowControlsOverlay

UpdateWindowControlsOverlay
~~~cpp
void UpdateWindowControlsOverlay(const gfx::Rect& bounding_rect) override;
~~~

### GetJavaWebContents

GetJavaWebContents
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetJavaWebContents() override;
~~~

### GetJavaCreatorLocation

GetJavaCreatorLocation
~~~cpp
base::android::ScopedJavaLocalRef<jthrowable> GetJavaCreatorLocation()
      override;
~~~

### GetWebContentsAndroid

GetWebContentsAndroid
~~~cpp
WebContentsAndroid* GetWebContentsAndroid();
~~~

### ClearWebContentsAndroid

ClearWebContentsAndroid
~~~cpp
void ClearWebContentsAndroid();
~~~

### ActivateNearestFindResult

ActivateNearestFindResult
~~~cpp
void ActivateNearestFindResult(float x, float y) override;
~~~

### RequestFindMatchRects

RequestFindMatchRects
~~~cpp
void RequestFindMatchRects(int current_version) override;
~~~

### GetJavaInterfaces

GetJavaInterfaces
~~~cpp
service_manager::InterfaceProvider* GetJavaInterfaces() override;
~~~

### HasRecentInteraction

HasRecentInteraction
~~~cpp
bool HasRecentInteraction() override;
~~~

### SetIgnoreInputEvents

SetIgnoreInputEvents
~~~cpp
void SetIgnoreInputEvents(bool ignore_input_events) override;
~~~

### HasActiveEffectivelyFullscreenVideo

HasActiveEffectivelyFullscreenVideo
~~~cpp
bool HasActiveEffectivelyFullscreenVideo() override;
~~~

### WriteIntoTrace

WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedValue context) override;
~~~

### GetCreatorLocation

GetCreatorLocation
~~~cpp
const base::Location& GetCreatorLocation() override;
~~~

### GetPictureInPictureOptions

GetPictureInPictureOptions
~~~cpp
const absl::optional<blink::mojom::PictureInPictureWindowOptions>&
  GetPictureInPictureOptions() const override;
~~~

### UpdateBrowserControlsState

UpdateBrowserControlsState
~~~cpp
void UpdateBrowserControlsState(cc::BrowserControlsState constraints,
                                  cc::BrowserControlsState current,
                                  bool animate) override;
~~~

### SetTabSwitchStartTime

SetTabSwitchStartTime
~~~cpp
void SetTabSwitchStartTime(base::TimeTicks start_time,
                             bool destination_is_loaded) override;
~~~

### OpenURL

OpenURL
~~~cpp
WebContents* OpenURL(const OpenURLParams& params) override;
~~~
 Implementation of PageNavigator.

### GetOrCreateWebPreferences

GetOrCreateWebPreferences
~~~cpp
const blink::web_pref::WebPreferences& GetOrCreateWebPreferences() override;
~~~

### NotifyPreferencesChanged

NotifyPreferencesChanged
~~~cpp
void NotifyPreferencesChanged() override;
~~~

### SetWebPreferences

SetWebPreferences
~~~cpp
void SetWebPreferences(const blink::web_pref::WebPreferences& prefs) override;
~~~

### OnWebPreferencesChanged

OnWebPreferencesChanged
~~~cpp
void OnWebPreferencesChanged() override;
~~~

### AboutToBeDiscarded

AboutToBeDiscarded
~~~cpp
void AboutToBeDiscarded(WebContents* new_contents) override;
~~~

### CreateDisallowCustomCursorScope

CreateDisallowCustomCursorScope
~~~cpp
[[nodiscard]] base::ScopedClosureRunner CreateDisallowCustomCursorScope()
      override;
~~~

### OnMessageReceived

OnMessageReceived
~~~cpp
bool OnMessageReceived(RenderFrameHostImpl* render_frame_host,
                         const IPC::Message& message) override;
~~~
###  RenderFrameHostDelegate 
---------------------------------------------------
### OnDidBlockNavigation

OnDidBlockNavigation
~~~cpp
void OnDidBlockNavigation(
      const GURL& blocked_url,
      const GURL& initiator_url,
      blink::mojom::NavigationBlockedReason reason) override;
~~~

### OnDidFinishLoad

OnDidFinishLoad
~~~cpp
void OnDidFinishLoad(RenderFrameHostImpl* render_frame_host,
                       const GURL& url) override;
~~~

### OnManifestUrlChanged

OnManifestUrlChanged
~~~cpp
void OnManifestUrlChanged(PageImpl& page) override;
~~~

### RenderFrameCreated

RenderFrameCreated
~~~cpp
void RenderFrameCreated(RenderFrameHostImpl* render_frame_host) override;
~~~

### RenderFrameDeleted

RenderFrameDeleted
~~~cpp
void RenderFrameDeleted(RenderFrameHostImpl* render_frame_host) override;
~~~

### ShowContextMenu

ShowContextMenu
~~~cpp
void ShowContextMenu(
      RenderFrameHost& render_frame_host,
      mojo::PendingAssociatedRemote<blink::mojom::ContextMenuClient>
          context_menu_client,
      const ContextMenuParams& params) override;
~~~

### RunJavaScriptDialog

RunJavaScriptDialog
~~~cpp
void RunJavaScriptDialog(RenderFrameHostImpl* render_frame_host,
                           const std::u16string& message,
                           const std::u16string& default_prompt,
                           JavaScriptDialogType dialog_type,
                           bool disable_third_party_subframe_suppresion,
                           JavaScriptDialogCallback response_callback) override;
~~~

### RunBeforeUnloadConfirm

RunBeforeUnloadConfirm
~~~cpp
void RunBeforeUnloadConfirm(
      RenderFrameHostImpl* render_frame_host,
      bool is_reload,
      JavaScriptDialogCallback response_callback) override;
~~~

### DidChangeName

DidChangeName
~~~cpp
void DidChangeName(RenderFrameHostImpl* render_frame_host,
                     const std::string& name) override;
~~~

### DidReceiveUserActivation

DidReceiveUserActivation
~~~cpp
void DidReceiveUserActivation(
      RenderFrameHostImpl* render_frame_host) override;
~~~

### BindDisplayCutoutHost

BindDisplayCutoutHost
~~~cpp
void BindDisplayCutoutHost(
      RenderFrameHostImpl* render_frame_host,
      mojo::PendingAssociatedReceiver<blink::mojom::DisplayCutoutHost> receiver)
      override;
~~~

### DidChangeDisplayState

DidChangeDisplayState
~~~cpp
void DidChangeDisplayState(RenderFrameHostImpl* render_frame_host,
                             bool is_display_none) override;
~~~

### FrameSizeChanged

FrameSizeChanged
~~~cpp
void FrameSizeChanged(RenderFrameHostImpl* render_frame_host,
                        const gfx::Size& frame_size) override;
~~~

### DOMContentLoaded

DOMContentLoaded
~~~cpp
void DOMContentLoaded(RenderFrameHostImpl* render_frame_host) override;
~~~

### DocumentOnLoadCompleted

DocumentOnLoadCompleted
~~~cpp
void DocumentOnLoadCompleted(RenderFrameHostImpl* render_frame_host) override;
~~~

### UpdateTitle

UpdateTitle
~~~cpp
void UpdateTitle(RenderFrameHostImpl* render_frame_host,
                   const std::u16string& title,
                   base::i18n::TextDirection title_direction) override;
~~~

### UpdateTargetURL

UpdateTargetURL
~~~cpp
void UpdateTargetURL(RenderFrameHostImpl* render_frame_host,
                       const GURL& url) override;
~~~

### IsNeverComposited

IsNeverComposited
~~~cpp
bool IsNeverComposited() override;
~~~

### SetCaptureHandleConfig

SetCaptureHandleConfig
~~~cpp
void SetCaptureHandleConfig(
      blink::mojom::CaptureHandleConfigPtr config) override;
~~~

### GetAccessibilityMode

GetAccessibilityMode
~~~cpp
ui::AXMode GetAccessibilityMode() override;
~~~

### SetAccessibilityMode

SetAccessibilityMode
~~~cpp
void SetAccessibilityMode(ui::AXMode mode) override;
~~~
 Broadcasts the mode change to all frames.

### AXTreeIDForMainFrameHasChanged

AXTreeIDForMainFrameHasChanged
~~~cpp
void AXTreeIDForMainFrameHasChanged() override;
~~~

### AccessibilityEventReceived

AccessibilityEventReceived
~~~cpp
void AccessibilityEventReceived(
      const AXEventNotificationDetails& details) override;
~~~

### AccessibilityLocationChangesReceived

AccessibilityLocationChangesReceived
~~~cpp
void AccessibilityLocationChangesReceived(
      const std::vector<AXLocationChangeNotificationDetails>& details) override;
~~~

### DumpAccessibilityTree

DumpAccessibilityTree
~~~cpp
std::string DumpAccessibilityTree(
      bool internal,
      std::vector<ui::AXPropertyFilter> property_filters) override;
~~~

### RecordAccessibilityEvents

RecordAccessibilityEvents
~~~cpp
void RecordAccessibilityEvents(
      bool start_recording,
      absl::optional<ui::AXEventCallback> callback) override;
~~~

### GetGeolocationContext

GetGeolocationContext
~~~cpp
device::mojom::GeolocationContext* GetGeolocationContext() override;
~~~

### GetWakeLockContext

GetWakeLockContext
~~~cpp
device::mojom::WakeLockContext* GetWakeLockContext() override;
~~~

### GetNFC

GetNFC
~~~cpp
void GetNFC(RenderFrameHost*,
              mojo::PendingReceiver<device::mojom::NFC>) override;
~~~

### CanEnterFullscreenMode

CanEnterFullscreenMode
~~~cpp
bool CanEnterFullscreenMode(
      RenderFrameHostImpl* requesting_frame,
      const blink::mojom::FullscreenOptions& options) override;
~~~

### EnterFullscreenMode

EnterFullscreenMode
~~~cpp
void EnterFullscreenMode(
      RenderFrameHostImpl* requesting_frame,
      const blink::mojom::FullscreenOptions& options) override;
~~~

### ExitFullscreenMode

ExitFullscreenMode
~~~cpp
void ExitFullscreenMode(bool will_cause_resize) override;
~~~

### FullscreenStateChanged

FullscreenStateChanged
~~~cpp
void FullscreenStateChanged(
      RenderFrameHostImpl* rfh,
      bool is_fullscreen,
      blink::mojom::FullscreenOptionsPtr options) override;
~~~

### UpdateUserGestureCarryoverInfo

UpdateUserGestureCarryoverInfo
~~~cpp
void UpdateUserGestureCarryoverInfo() override;
~~~

### ShouldRouteMessageEvent

ShouldRouteMessageEvent
~~~cpp
bool ShouldRouteMessageEvent(RenderFrameHostImpl* target_rfh) const override;
~~~

### EnsureOpenerProxiesExist

EnsureOpenerProxiesExist
~~~cpp
void EnsureOpenerProxiesExist(RenderFrameHostImpl* source_rfh) override;
~~~

### CreateWebUIForRenderFrameHost

CreateWebUIForRenderFrameHost
~~~cpp
std::unique_ptr<WebUIImpl> CreateWebUIForRenderFrameHost(
      RenderFrameHostImpl* frame_host,
      const GURL& url) override;
~~~

### DidCallFocus

DidCallFocus
~~~cpp
void DidCallFocus() override;
~~~

### OnFocusedElementChangedInFrame

OnFocusedElementChangedInFrame
~~~cpp
void OnFocusedElementChangedInFrame(
      RenderFrameHostImpl* frame,
      const gfx::Rect& bounds_in_root_view,
      blink::mojom::FocusType focus_type) override;
~~~

### OnAdvanceFocus

OnAdvanceFocus
~~~cpp
void OnAdvanceFocus(RenderFrameHostImpl* source_rfh) override;
~~~

### CreateNewWindow

CreateNewWindow
~~~cpp
FrameTree* CreateNewWindow(
      RenderFrameHostImpl* opener,
      const mojom::CreateNewWindowParams& params,
      bool is_new_browsing_instance,
      bool has_user_gesture,
      SessionStorageNamespace* session_storage_namespace) override;
~~~

### ShowCreatedWindow

ShowCreatedWindow
~~~cpp
void ShowCreatedWindow(RenderFrameHostImpl* opener,
                         int main_frame_widget_route_id,
                         WindowOpenDisposition disposition,
                         const blink::mojom::WindowFeatures& window_features,
                         bool user_gesture) override;
~~~

### PrimaryMainDocumentElementAvailable

PrimaryMainDocumentElementAvailable
~~~cpp
void PrimaryMainDocumentElementAvailable() override;
~~~

### PassiveInsecureContentFound

PassiveInsecureContentFound
~~~cpp
void PassiveInsecureContentFound(const GURL& resource_url) override;
~~~

### ShouldAllowRunningInsecureContent

ShouldAllowRunningInsecureContent
~~~cpp
bool ShouldAllowRunningInsecureContent(bool allowed_per_prefs,
                                         const url::Origin& origin,
                                         const GURL& resource_url) override;
~~~

### ViewSource

ViewSource
~~~cpp
void ViewSource(RenderFrameHostImpl* frame) override;
~~~

### PrintCrossProcessSubframe

PrintCrossProcessSubframe
~~~cpp
void PrintCrossProcessSubframe(
      const gfx::Rect& rect,
      int document_cookie,
      RenderFrameHostImpl* render_frame_host) override;
~~~

### CapturePaintPreviewOfCrossProcessSubframe

CapturePaintPreviewOfCrossProcessSubframe
~~~cpp
void CapturePaintPreviewOfCrossProcessSubframe(
      const gfx::Rect& rect,
      const base::UnguessableToken& guid,
      RenderFrameHostImpl* render_frame_host) override;
~~~

### GetJavaRenderFrameHostDelegate

GetJavaRenderFrameHostDelegate
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetJavaRenderFrameHostDelegate()
      override;
~~~

### ResourceLoadComplete

ResourceLoadComplete
~~~cpp
void ResourceLoadComplete(
      RenderFrameHostImpl* render_frame_host,
      const GlobalRequestID& request_id,
      blink::mojom::ResourceLoadInfoPtr resource_load_information) override;
~~~

### OnCookiesAccessed

OnCookiesAccessed
~~~cpp
void OnCookiesAccessed(RenderFrameHostImpl*,
                         const CookieAccessDetails& details) override;
~~~

### OnTrustTokensAccessed

OnTrustTokensAccessed
~~~cpp
void OnTrustTokensAccessed(RenderFrameHostImpl*,
                             const TrustTokenAccessDetails& details) override;
~~~

### AudioContextPlaybackStarted

AudioContextPlaybackStarted
~~~cpp
void AudioContextPlaybackStarted(RenderFrameHostImpl* host,
                                   int context_id) override;
~~~
 Called when WebAudio starts or stops playing audible audio in an
 AudioContext.

### AudioContextPlaybackStopped

AudioContextPlaybackStopped
~~~cpp
void AudioContextPlaybackStopped(RenderFrameHostImpl* host,
                                   int context_id) override;
~~~

### OnFrameAudioStateChanged

OnFrameAudioStateChanged
~~~cpp
void OnFrameAudioStateChanged(RenderFrameHostImpl* host,
                                bool is_audible) override;
~~~

### GetRecordAggregateWatchTimeCallback

GetRecordAggregateWatchTimeCallback
~~~cpp
media::MediaMetricsProvider::RecordAggregateWatchTimeCallback
  GetRecordAggregateWatchTimeCallback(
      const GURL& page_main_frame_last_committed_url) override;
~~~

### GetUnattachedOwnedNodes

GetUnattachedOwnedNodes
~~~cpp
std::vector<FrameTreeNode*> GetUnattachedOwnedNodes(
      RenderFrameHostImpl* owner) override;
~~~

### RegisterProtocolHandler

RegisterProtocolHandler
~~~cpp
void RegisterProtocolHandler(RenderFrameHostImpl* source,
                               const std::string& protocol,
                               const GURL& url,
                               bool user_gesture) override;
~~~

### UnregisterProtocolHandler

UnregisterProtocolHandler
~~~cpp
void UnregisterProtocolHandler(RenderFrameHostImpl* source,
                                 const std::string& protocol,
                                 const GURL& url,
                                 bool user_gesture) override;
~~~

### IsAllowedToGoToEntryAtOffset

IsAllowedToGoToEntryAtOffset
~~~cpp
bool IsAllowedToGoToEntryAtOffset(int32_t offset) override;
~~~

### IsClipboardPasteContentAllowed

IsClipboardPasteContentAllowed
~~~cpp
void IsClipboardPasteContentAllowed(
      const GURL& url,
      const ui::ClipboardFormatType& data_type,
      const std::string& data,
      IsClipboardPasteContentAllowedCallback callback) override;
~~~

### IsClipboardPasteContentAllowedWrapperCallback

IsClipboardPasteContentAllowedWrapperCallback
~~~cpp
void IsClipboardPasteContentAllowedWrapperCallback(
      IsClipboardPasteContentAllowedCallback callback,
      const absl::optional<std::string>& data);
~~~

### OnPageScaleFactorChanged

OnPageScaleFactorChanged
~~~cpp
void OnPageScaleFactorChanged(PageImpl& source) override;
~~~

### BindScreenOrientation

BindScreenOrientation
~~~cpp
void BindScreenOrientation(
      RenderFrameHost* rfh,
      mojo::PendingAssociatedReceiver<device::mojom::ScreenOrientation>
          receiver) override;
~~~

### HasSeenRecentScreenOrientationChange

HasSeenRecentScreenOrientationChange
~~~cpp
bool HasSeenRecentScreenOrientationChange() override;
~~~

### IsTransientAllowFullscreenActive

IsTransientAllowFullscreenActive
~~~cpp
bool IsTransientAllowFullscreenActive() const override;
~~~

### IsBackForwardCacheSupported

IsBackForwardCacheSupported
~~~cpp
bool IsBackForwardCacheSupported() override;
~~~

### CreateNewPopupWidget

CreateNewPopupWidget
~~~cpp
RenderWidgetHostImpl* CreateNewPopupWidget(
      base::SafeRef<SiteInstanceGroup> site_instance_group,
      int32_t route_id,
      mojo::PendingAssociatedReceiver<blink::mojom::PopupWidgetHost>
          blink_popup_widget_host,
      mojo::PendingAssociatedReceiver<blink::mojom::WidgetHost>
          blink_widget_host,
      mojo::PendingAssociatedRemote<blink::mojom::Widget> blink_widget)
      override;
~~~

### ShowPopupMenu

ShowPopupMenu
~~~cpp
bool ShowPopupMenu(RenderFrameHostImpl* render_frame_host,
                     const gfx::Rect& bounds) override;
~~~

### DidLoadResourceFromMemoryCache

DidLoadResourceFromMemoryCache
~~~cpp
void DidLoadResourceFromMemoryCache(
      RenderFrameHostImpl* source,
      const GURL& url,
      const std::string& http_request,
      const std::string& mime_type,
      network::mojom::RequestDestination request_destination,
      bool include_credentials) override;
~~~

### DomOperationResponse

DomOperationResponse
~~~cpp
void DomOperationResponse(RenderFrameHost* render_frame_host,
                            const std::string& json_string) override;
~~~

### SavableResourceLinksResponse

SavableResourceLinksResponse
~~~cpp
void SavableResourceLinksResponse(
      RenderFrameHostImpl* source,
      const std::vector<GURL>& resources_list,
      blink::mojom::ReferrerPtr referrer,
      const std::vector<blink::mojom::SavableSubframePtr>& subframes) override;
~~~

### SavableResourceLinksError

SavableResourceLinksError
~~~cpp
void SavableResourceLinksError(RenderFrameHostImpl* source) override;
~~~

### RenderFrameHostStateChanged

RenderFrameHostStateChanged
~~~cpp
void RenderFrameHostStateChanged(
      RenderFrameHost* render_frame_host,
      RenderFrameHost::LifecycleState old_state,
      RenderFrameHost::LifecycleState new_state) override;
~~~

### SetWindowRect

SetWindowRect
~~~cpp
void SetWindowRect(const gfx::Rect& new_bounds) override;
~~~

### UpdateWindowPreferredSize

UpdateWindowPreferredSize
~~~cpp
void UpdateWindowPreferredSize(const gfx::Size& pref_size) override;
~~~

### GetActiveTopLevelDocumentsInBrowsingContextGroup

GetActiveTopLevelDocumentsInBrowsingContextGroup
~~~cpp
std::vector<RenderFrameHostImpl*>
  GetActiveTopLevelDocumentsInBrowsingContextGroup(
      RenderFrameHostImpl* render_frame_host) override;
~~~

### GetPrerenderHostRegistry

GetPrerenderHostRegistry
~~~cpp
PrerenderHostRegistry* GetPrerenderHostRegistry() override;
~~~

### OnPepperInstanceCreated

OnPepperInstanceCreated
~~~cpp
void OnPepperInstanceCreated(RenderFrameHostImpl* source,
                               int32_t pp_instance) override;
~~~

### OnPepperInstanceDeleted

OnPepperInstanceDeleted
~~~cpp
void OnPepperInstanceDeleted(RenderFrameHostImpl* source,
                               int32_t pp_instance) override;
~~~

### OnPepperStartsPlayback

OnPepperStartsPlayback
~~~cpp
void OnPepperStartsPlayback(RenderFrameHostImpl* source,
                              int32_t pp_instance) override;
~~~

### OnPepperStopsPlayback

OnPepperStopsPlayback
~~~cpp
void OnPepperStopsPlayback(RenderFrameHostImpl* source,
                             int32_t pp_instance) override;
~~~

### OnPepperPluginCrashed

OnPepperPluginCrashed
~~~cpp
void OnPepperPluginCrashed(RenderFrameHostImpl* source,
                             const base::FilePath& plugin_path,
                             base::ProcessId plugin_pid) override;
~~~

### OnPepperPluginHung

OnPepperPluginHung
~~~cpp
void OnPepperPluginHung(RenderFrameHostImpl* source,
                          int plugin_child_id,
                          const base::FilePath& path,
                          bool is_hung) override;
~~~

### DidChangeLoadProgressForPrimaryMainFrame

DidChangeLoadProgressForPrimaryMainFrame
~~~cpp
void DidChangeLoadProgressForPrimaryMainFrame() override;
~~~
 BUILDFLAG(ENABLE_PPAPI)
### DidFailLoadWithError

DidFailLoadWithError
~~~cpp
void DidFailLoadWithError(RenderFrameHostImpl* render_frame_host,
                            const GURL& url,
                            int error_code) override;
~~~

### GetDelegateView

GetDelegateView
~~~cpp
RenderViewHostDelegateView* GetDelegateView() override;
~~~
###  RenderViewHostDelegate 
----------------------------------------------------
### RenderViewReady

RenderViewReady
~~~cpp
void RenderViewReady(RenderViewHost* render_view_host) override;
~~~

### RenderViewTerminated

RenderViewTerminated
~~~cpp
void RenderViewTerminated(RenderViewHost* render_view_host,
                            base::TerminationStatus status,
                            int error_code) override;
~~~

### RenderViewDeleted

RenderViewDeleted
~~~cpp
void RenderViewDeleted(RenderViewHost* render_view_host) override;
~~~

### DidAddMessageToConsole

DidAddMessageToConsole
~~~cpp
bool DidAddMessageToConsole(
      RenderFrameHostImpl* source_frame,
      blink::mojom::ConsoleMessageLevel log_level,
      const std::u16string& message,
      int32_t line_no,
      const std::u16string& source_id,
      const absl::optional<std::u16string>& untrusted_stack_trace) override;
~~~

### GetRendererPrefs

GetRendererPrefs
~~~cpp
const blink::RendererPreferences& GetRendererPrefs() const override;
~~~

### DidReceiveInputEvent

DidReceiveInputEvent
~~~cpp
void DidReceiveInputEvent(RenderWidgetHostImpl* render_widget_host,
                            const blink::WebInputEvent& event) override;
~~~

### ShouldIgnoreInputEvents

ShouldIgnoreInputEvents
~~~cpp
bool ShouldIgnoreInputEvents() override;
~~~

### OnIgnoredUIEvent

OnIgnoredUIEvent
~~~cpp
void OnIgnoredUIEvent() override;
~~~

### Activate

Activate
~~~cpp
void Activate() override;
~~~

### ShowCreatedWidget

ShowCreatedWidget
~~~cpp
void ShowCreatedWidget(int process_id,
                         int widget_route_id,
                         const gfx::Rect& initial_rect,
                         const gfx::Rect& initial_anchor_rect) override;
~~~

### CreateMediaPlayerHostForRenderFrameHost

CreateMediaPlayerHostForRenderFrameHost
~~~cpp
void CreateMediaPlayerHostForRenderFrameHost(
      RenderFrameHostImpl* frame_host,
      mojo::PendingAssociatedReceiver<media::mojom::MediaPlayerHost> receiver)
      override;
~~~

### RequestMediaAccessPermission

RequestMediaAccessPermission
~~~cpp
void RequestMediaAccessPermission(const MediaStreamRequest& request,
                                    MediaResponseCallback callback) override;
~~~

### CheckMediaAccessPermission

CheckMediaAccessPermission
~~~cpp
bool CheckMediaAccessPermission(RenderFrameHostImpl* render_frame_host,
                                  const url::Origin& security_origin,
                                  blink::mojom::MediaStreamType type) override;
~~~

### GetDefaultMediaDeviceID

GetDefaultMediaDeviceID
~~~cpp
std::string GetDefaultMediaDeviceID(
      blink::mojom::MediaStreamType type) override;
~~~

### IsJavaScriptDialogShowing

IsJavaScriptDialogShowing
~~~cpp
bool IsJavaScriptDialogShowing() const override;
~~~

### ShouldIgnoreUnresponsiveRenderer

ShouldIgnoreUnresponsiveRenderer
~~~cpp
bool ShouldIgnoreUnresponsiveRenderer() override;
~~~

### IsGuest

IsGuest
~~~cpp
bool IsGuest() override;
~~~

### RecomputeWebPreferencesSlow

RecomputeWebPreferencesSlow
~~~cpp
void RecomputeWebPreferencesSlow() override;
~~~

### GetBaseBackgroundColor

GetBaseBackgroundColor
~~~cpp
absl::optional<SkColor> GetBaseBackgroundColor() override;
~~~

### StartPrerendering

StartPrerendering
~~~cpp
std::unique_ptr<PrerenderHandle> StartPrerendering(
      const GURL& prerendering_url,
      PrerenderTriggerType trigger_type,
      const std::string& embedder_histogram_suffix,
      ui::PageTransition page_transition,
      PreloadingAttempt* preloading_attempt,
      absl::optional<base::RepeatingCallback<bool(const GURL&)>>
          url_match_predicate = absl::nullopt) override;
~~~

### DidStartNavigation

DidStartNavigation
~~~cpp
void DidStartNavigation(NavigationHandle* navigation_handle) override;
~~~
###  NavigatorDelegate 
---------------------------------------------------------
### DidRedirectNavigation

DidRedirectNavigation
~~~cpp
void DidRedirectNavigation(NavigationHandle* navigation_handle) override;
~~~

### ReadyToCommitNavigation

ReadyToCommitNavigation
~~~cpp
void ReadyToCommitNavigation(NavigationHandle* navigation_handle) override;
~~~

### DidFinishNavigation

DidFinishNavigation
~~~cpp
void DidFinishNavigation(NavigationHandle* navigation_handle) override;
~~~

### DidNavigateMainFramePreCommit

DidNavigateMainFramePreCommit
~~~cpp
void DidNavigateMainFramePreCommit(FrameTreeNode* frame_tree_node,
                                     bool navigation_is_within_page) override;
~~~

### DidNavigateMainFramePostCommit

DidNavigateMainFramePostCommit
~~~cpp
void DidNavigateMainFramePostCommit(
      RenderFrameHostImpl* render_frame_host,
      const LoadCommittedDetails& details) override;
~~~

### DidNavigateAnyFramePostCommit

DidNavigateAnyFramePostCommit
~~~cpp
void DidNavigateAnyFramePostCommit(
      RenderFrameHostImpl* render_frame_host,
      const LoadCommittedDetails& details) override;
~~~

### NotifyChangedNavigationState

NotifyChangedNavigationState
~~~cpp
void NotifyChangedNavigationState(InvalidateTypes changed_flags) override;
~~~

### ShouldAllowRendererInitiatedCrossProcessNavigation

ShouldAllowRendererInitiatedCrossProcessNavigation
~~~cpp
bool ShouldAllowRendererInitiatedCrossProcessNavigation(
      bool is_outermost_main_frame_navigation) override;
~~~

### CreateThrottlesForNavigation

CreateThrottlesForNavigation
~~~cpp
std::vector<std::unique_ptr<NavigationThrottle>> CreateThrottlesForNavigation(
      NavigationHandle* navigation_handle) override;
~~~

### CreateDeferringConditionsForNavigationCommit

CreateDeferringConditionsForNavigationCommit
~~~cpp
std::vector<std::unique_ptr<CommitDeferringCondition>>
  CreateDeferringConditionsForNavigationCommit(
      NavigationHandle& navigation_handle,
      CommitDeferringCondition::NavigationType type) override;
~~~

### GetNavigationUIData

GetNavigationUIData
~~~cpp
std::unique_ptr<NavigationUIData> GetNavigationUIData(
      NavigationHandle* navigation_handle) override;
~~~

### OnServiceWorkerAccessed

OnServiceWorkerAccessed
~~~cpp
void OnServiceWorkerAccessed(NavigationHandle* navigation,
                               const GURL& scope,
                               AllowServiceWorkerResult allowed) override;
~~~

### OnCookiesAccessed

OnCookiesAccessed
~~~cpp
void OnCookiesAccessed(NavigationHandle*,
                         const CookieAccessDetails& details) override;
~~~

### OnTrustTokensAccessed

OnTrustTokensAccessed
~~~cpp
void OnTrustTokensAccessed(NavigationHandle*,
                             const TrustTokenAccessDetails& details) override;
~~~

### RegisterExistingOriginAsHavingDefaultIsolation

RegisterExistingOriginAsHavingDefaultIsolation
~~~cpp
void RegisterExistingOriginAsHavingDefaultIsolation(
      const url::Origin& origin,
      NavigationRequest* navigation_request_to_exclude) override;
~~~

### SetTopControlsShownRatio

SetTopControlsShownRatio
~~~cpp
void SetTopControlsShownRatio(RenderWidgetHostImpl* render_widget_host,
                                float ratio) override;
~~~
###  RenderWidgetHostDelegate 
--------------------------------------------------
### SetTopControlsGestureScrollInProgress

SetTopControlsGestureScrollInProgress
~~~cpp
void SetTopControlsGestureScrollInProgress(bool in_progress) override;
~~~

### RenderWidgetCreated

RenderWidgetCreated
~~~cpp
void RenderWidgetCreated(RenderWidgetHostImpl* render_widget_host) override;
~~~

### RenderWidgetDeleted

RenderWidgetDeleted
~~~cpp
void RenderWidgetDeleted(RenderWidgetHostImpl* render_widget_host) override;
~~~

### RenderWidgetWasResized

RenderWidgetWasResized
~~~cpp
void RenderWidgetWasResized(RenderWidgetHostImpl* render_widget_host,
                              bool width_changed) override;
~~~

### ResizeDueToAutoResize

ResizeDueToAutoResize
~~~cpp
void ResizeDueToAutoResize(RenderWidgetHostImpl* render_widget_host,
                             const gfx::Size& new_size) override;
~~~

### OnVerticalScrollDirectionChanged

OnVerticalScrollDirectionChanged
~~~cpp
void OnVerticalScrollDirectionChanged(
      viz::VerticalScrollDirection scroll_direction) override;
~~~

### GetVirtualKeyboardResizeHeight

GetVirtualKeyboardResizeHeight
~~~cpp
int GetVirtualKeyboardResizeHeight() override;
~~~

### GetPendingPageZoomLevel

GetPendingPageZoomLevel
~~~cpp
double GetPendingPageZoomLevel() override;
~~~

### PreHandleKeyboardEvent

PreHandleKeyboardEvent
~~~cpp
KeyboardEventProcessingResult PreHandleKeyboardEvent(
      const NativeWebKeyboardEvent& event) override;
~~~

### HandleMouseEvent

HandleMouseEvent
~~~cpp
bool HandleMouseEvent(const blink::WebMouseEvent& event) override;
~~~

### HandleKeyboardEvent

HandleKeyboardEvent
~~~cpp
bool HandleKeyboardEvent(const NativeWebKeyboardEvent& event) override;
~~~

### HandleWheelEvent

HandleWheelEvent
~~~cpp
bool HandleWheelEvent(const blink::WebMouseWheelEvent& event) override;
~~~

### PreHandleGestureEvent

PreHandleGestureEvent
~~~cpp
bool PreHandleGestureEvent(const blink::WebGestureEvent& event) override;
~~~

### GetRootBrowserAccessibilityManager

GetRootBrowserAccessibilityManager
~~~cpp
BrowserAccessibilityManager* GetRootBrowserAccessibilityManager() override;
~~~

### GetOrCreateRootBrowserAccessibilityManager

GetOrCreateRootBrowserAccessibilityManager
~~~cpp
BrowserAccessibilityManager* GetOrCreateRootBrowserAccessibilityManager()
      override;
~~~

### ExecuteEditCommand

ExecuteEditCommand
~~~cpp
void ExecuteEditCommand(const std::string& command,
                          const absl::optional<std::u16string>& value) override;
~~~
 The following 4 functions are already listed under WebContents overrides:
 void Cut() override;
 void Copy() override;
 void Paste() override;
 void SelectAll() override;
### MoveRangeSelectionExtent

MoveRangeSelectionExtent
~~~cpp
void MoveRangeSelectionExtent(const gfx::Point& extent) override;
~~~

### SelectRange

SelectRange
~~~cpp
void SelectRange(const gfx::Point& base, const gfx::Point& extent) override;
~~~

### SelectAroundCaret

SelectAroundCaret
~~~cpp
void SelectAroundCaret(blink::mojom::SelectionGranularity granularity,
                         bool should_show_handle,
                         bool should_show_context_menu) override;
~~~

### MoveCaret

MoveCaret
~~~cpp
void MoveCaret(const gfx::Point& extent) override;
~~~

### AdjustSelectionByCharacterOffset

AdjustSelectionByCharacterOffset
~~~cpp
void AdjustSelectionByCharacterOffset(int start_adjust,
                                        int end_adjust,
                                        bool show_selection_menu) override;
~~~

### GetInputEventRouter

GetInputEventRouter
~~~cpp
RenderWidgetHostInputEventRouter* GetInputEventRouter() override;
~~~

### GetFocusedRenderWidgetHost

GetFocusedRenderWidgetHost
~~~cpp
RenderWidgetHostImpl* GetFocusedRenderWidgetHost(
      RenderWidgetHostImpl* receiving_widget) override;
~~~

### GetRenderWidgetHostWithPageFocus

GetRenderWidgetHostWithPageFocus
~~~cpp
RenderWidgetHostImpl* GetRenderWidgetHostWithPageFocus() override;
~~~

### FocusOwningWebContents

FocusOwningWebContents
~~~cpp
void FocusOwningWebContents(
      RenderWidgetHostImpl* render_widget_host) override;
~~~

### RendererUnresponsive

RendererUnresponsive
~~~cpp
void RendererUnresponsive(
      RenderWidgetHostImpl* render_widget_host,
      base::RepeatingClosure hang_monitor_restarter) override;
~~~

### RendererResponsive

RendererResponsive
~~~cpp
void RendererResponsive(RenderWidgetHostImpl* render_widget_host) override;
~~~

### RequestToLockMouse

RequestToLockMouse
~~~cpp
void RequestToLockMouse(RenderWidgetHostImpl* render_widget_host,
                          bool user_gesture,
                          bool last_unlocked_by_target,
                          bool privileged) override;
~~~

### RequestKeyboardLock

RequestKeyboardLock
~~~cpp
bool RequestKeyboardLock(RenderWidgetHostImpl* render_widget_host,
                           bool esc_key_locked) override;
~~~

### CancelKeyboardLock

CancelKeyboardLock
~~~cpp
void CancelKeyboardLock(RenderWidgetHostImpl* render_widget_host) override;
~~~

### GetKeyboardLockWidget

GetKeyboardLockWidget
~~~cpp
RenderWidgetHostImpl* GetKeyboardLockWidget() override;
~~~

### GetDisplayMode

GetDisplayMode
~~~cpp
blink::mojom::DisplayMode GetDisplayMode() const override;
~~~
 The following function is already listed under WebContents overrides:
 bool IsFullscreen() const override;
### LostMouseLock

LostMouseLock
~~~cpp
void LostMouseLock(RenderWidgetHostImpl* render_widget_host) override;
~~~

### HasMouseLock

HasMouseLock
~~~cpp
bool HasMouseLock(RenderWidgetHostImpl* render_widget_host) override;
~~~

### GetMouseLockWidget

GetMouseLockWidget
~~~cpp
RenderWidgetHostImpl* GetMouseLockWidget() override;
~~~

### OnRenderFrameProxyVisibilityChanged

OnRenderFrameProxyVisibilityChanged
~~~cpp
bool OnRenderFrameProxyVisibilityChanged(
      RenderFrameProxyHost* render_frame_proxy_host,
      blink::mojom::FrameVisibility visibility) override;
~~~

### SendScreenRects

SendScreenRects
~~~cpp
void SendScreenRects() override;
~~~

### SendActiveState

SendActiveState
~~~cpp
void SendActiveState(bool active) override;
~~~

### GetTextInputManager

GetTextInputManager
~~~cpp
TextInputManager* GetTextInputManager() override;
~~~

### IsWidgetForPrimaryMainFrame

IsWidgetForPrimaryMainFrame
~~~cpp
bool IsWidgetForPrimaryMainFrame(
      RenderWidgetHostImpl* render_widget_host) override;
~~~

### IsShowingContextMenuOnPage

IsShowingContextMenuOnPage
~~~cpp
bool IsShowingContextMenuOnPage() const override;
~~~

### DidChangeScreenOrientation

DidChangeScreenOrientation
~~~cpp
void DidChangeScreenOrientation() override;
~~~

### GetWindowsControlsOverlayRect

GetWindowsControlsOverlayRect
~~~cpp
gfx::Rect GetWindowsControlsOverlayRect() const override;
~~~

### GetVisibleTimeRequestTrigger

GetVisibleTimeRequestTrigger
~~~cpp
VisibleTimeRequestTrigger& GetVisibleTimeRequestTrigger() final;
~~~

### CreateRenderViewForRenderManager

CreateRenderViewForRenderManager
~~~cpp
bool CreateRenderViewForRenderManager(
      RenderViewHost* render_view_host,
      const absl::optional<blink::FrameToken>& opener_frame_token,
      RenderFrameProxyHost* proxy_host) override;
~~~
###  RenderFrameHostManager::Delegate 
------------------------------------------
### ReattachOuterDelegateIfNeeded

ReattachOuterDelegateIfNeeded
~~~cpp
void ReattachOuterDelegateIfNeeded() override;
~~~

### CreateRenderWidgetHostViewForRenderManager

CreateRenderWidgetHostViewForRenderManager
~~~cpp
void CreateRenderWidgetHostViewForRenderManager(
      RenderViewHost* render_view_host) override;
~~~

### BeforeUnloadFiredFromRenderManager

BeforeUnloadFiredFromRenderManager
~~~cpp
void BeforeUnloadFiredFromRenderManager(
      bool proceed,
      bool* proceed_to_fire_unload) override;
~~~

### CancelModalDialogsForRenderManager

CancelModalDialogsForRenderManager
~~~cpp
void CancelModalDialogsForRenderManager() override;
~~~

### NotifySwappedFromRenderManager

NotifySwappedFromRenderManager
~~~cpp
void NotifySwappedFromRenderManager(RenderFrameHostImpl* old_frame,
                                      RenderFrameHostImpl* new_frame) override;
~~~

### NotifySwappedFromRenderManagerWithoutFallbackContent

NotifySwappedFromRenderManagerWithoutFallbackContent
~~~cpp
void NotifySwappedFromRenderManagerWithoutFallbackContent(
      RenderFrameHostImpl* new_frame) override;
~~~

### NotifyMainFrameSwappedFromRenderManager

NotifyMainFrameSwappedFromRenderManager
~~~cpp
void NotifyMainFrameSwappedFromRenderManager(
      RenderFrameHostImpl* old_frame,
      RenderFrameHostImpl* new_frame) override;
~~~

### FocusLocationBarByDefault

FocusLocationBarByDefault
~~~cpp
bool FocusLocationBarByDefault() override;
~~~

### OnFrameTreeNodeDestroyed

OnFrameTreeNodeDestroyed
~~~cpp
void OnFrameTreeNodeDestroyed(FrameTreeNode* node) override;
~~~

### OnFirstVisuallyNonEmptyPaint

OnFirstVisuallyNonEmptyPaint
~~~cpp
void OnFirstVisuallyNonEmptyPaint(PageImpl& page) override;
~~~
###  PageDelegate 
-------------------------------------------------------------
### OnThemeColorChanged

OnThemeColorChanged
~~~cpp
void OnThemeColorChanged(PageImpl& page) override;
~~~
 These both check that the color has in fact changed before notifying
 observers.

### OnBackgroundColorChanged

OnBackgroundColorChanged
~~~cpp
void OnBackgroundColorChanged(PageImpl& page) override;
~~~

### DidInferColorScheme

DidInferColorScheme
~~~cpp
void DidInferColorScheme(PageImpl& page) override;
~~~

### OnVirtualKeyboardModeChanged

OnVirtualKeyboardModeChanged
~~~cpp
void OnVirtualKeyboardModeChanged(PageImpl& page) override;
~~~

### NotifyPageBecamePrimary

NotifyPageBecamePrimary
~~~cpp
void NotifyPageBecamePrimary(PageImpl& page) override;
~~~

### OnColorChooserFactoryReceiver

OnColorChooserFactoryReceiver
~~~cpp
void OnColorChooserFactoryReceiver(
      mojo::PendingReceiver<blink::mojom::ColorChooserFactory> receiver);
~~~
###  blink::mojom::ColorChooserFactory 
---------------------------------------
### OpenColorChooser

OpenColorChooser
~~~cpp
void OpenColorChooser(
      mojo::PendingReceiver<blink::mojom::ColorChooser> chooser,
      mojo::PendingRemote<blink::mojom::ColorChooserClient> client,
      SkColor color,
      std::vector<blink::mojom::ColorSuggestionPtr> suggestions) override;
~~~

### DidStartLoading

DidStartLoading
~~~cpp
void DidStartLoading(FrameTreeNode* frame_tree_node,
                       bool should_show_loading_ui) override;
~~~
 BUILDFLAG(IS_ANDROID) || BUILDFLAG(IS_MAC)
###  FrameTree::Delegate 
-------------------------------------------------------
### DidStopLoading

DidStopLoading
~~~cpp
void DidStopLoading() override;
~~~

### IsHidden

IsHidden
~~~cpp
bool IsHidden() override;
~~~

### GetOuterDelegateFrameTreeNodeId

GetOuterDelegateFrameTreeNodeId
~~~cpp
int GetOuterDelegateFrameTreeNodeId() override;
~~~

### GetProspectiveOuterDocument

GetProspectiveOuterDocument
~~~cpp
RenderFrameHostImpl* GetProspectiveOuterDocument() override;
~~~

### LoadingTree

LoadingTree
~~~cpp
FrameTree* LoadingTree() override;
~~~

### SetFocusedFrame

SetFocusedFrame
~~~cpp
void SetFocusedFrame(FrameTreeNode* node, SiteInstanceGroup* source) override;
~~~

### DeprecatedGetWebContents

DeprecatedGetWebContents
~~~cpp
WebContents* DeprecatedGetWebContents() override;
~~~
###  NavigationControllerDelegate 
----------------------------------------------
### NotifyNavigationEntryCommitted

NotifyNavigationEntryCommitted
~~~cpp
void NotifyNavigationEntryCommitted(
      const LoadCommittedDetails& load_details) override;
~~~

### NotifyNavigationEntryChanged

NotifyNavigationEntryChanged
~~~cpp
void NotifyNavigationEntryChanged(
      const EntryChangedDetails& change_details) override;
~~~

### NotifyNavigationListPruned

NotifyNavigationListPruned
~~~cpp
void NotifyNavigationListPruned(const PrunedDetails& pruned_details) override;
~~~

### NotifyNavigationEntriesDeleted

NotifyNavigationEntriesDeleted
~~~cpp
void NotifyNavigationEntriesDeleted() override;
~~~

### ShouldPreserveAbortedURLs

ShouldPreserveAbortedURLs
~~~cpp
bool ShouldPreserveAbortedURLs() override;
~~~

### NotifyNavigationStateChangedFromController

NotifyNavigationStateChangedFromController
~~~cpp
void NotifyNavigationStateChangedFromController(
      InvalidateTypes changed_flags) override;
~~~

### NotifyBeforeFormRepostWarningShow

NotifyBeforeFormRepostWarningShow
~~~cpp
void NotifyBeforeFormRepostWarningShow() override;
~~~
 Invoked before a form repost warning is shown.

### ActivateAndShowRepostFormWarningDialog

ActivateAndShowRepostFormWarningDialog
~~~cpp
void ActivateAndShowRepostFormWarningDialog() override;
~~~
 Activate this WebContents and show a form repost warning.

### MediaMutedStatusChanged

MediaMutedStatusChanged
~~~cpp
void MediaMutedStatusChanged(const MediaPlayerId& id, bool muted);
~~~

### UpdateOverridingUserAgent

UpdateOverridingUserAgent
~~~cpp
void UpdateOverridingUserAgent() override;
~~~

### SetForceDisableOverscrollContent

SetForceDisableOverscrollContent
~~~cpp
void SetForceDisableOverscrollContent(bool force_disable);
~~~
 Forces overscroll to be disabled (used by touch emulation).

### SetDeviceEmulationSize

SetDeviceEmulationSize
~~~cpp
bool SetDeviceEmulationSize(const gfx::Size& new_size);
~~~
 Override the render view/widget size of the main frame, return whether the
 size changed.

### ClearDeviceEmulationSize

ClearDeviceEmulationSize
~~~cpp
void ClearDeviceEmulationSize();
~~~

### audio_stream_monitor

audio_stream_monitor
~~~cpp
AudioStreamMonitor* audio_stream_monitor() { return &audio_stream_monitor_; }
~~~

### GetAudioStreamFactory

GetAudioStreamFactory
~~~cpp
ForwardingAudioStreamFactory* GetAudioStreamFactory();
~~~

### MediaStartedPlaying

MediaStartedPlaying
~~~cpp
void MediaStartedPlaying(
      const WebContentsObserver::MediaPlayerInfo& media_info,
      const MediaPlayerId& id);
~~~
 Called by MediaWebContentsObserver when playback starts or stops.  See the
 WebContentsObserver function stubs for more details.

### MediaStoppedPlaying

MediaStoppedPlaying
~~~cpp
void MediaStoppedPlaying(
      const WebContentsObserver::MediaPlayerInfo& media_info,
      const MediaPlayerId& id,
      WebContentsObserver::MediaStoppedReason reason);
~~~

### MediaResized

MediaResized
~~~cpp
void MediaResized(const gfx::Size& size, const MediaPlayerId& id);
~~~
 This will be called before playback is started, check
 GetCurrentlyPlayingVideoCount if you need this when playback starts.

### MediaEffectivelyFullscreenChanged

MediaEffectivelyFullscreenChanged
~~~cpp
void MediaEffectivelyFullscreenChanged(bool is_fullscreen);
~~~

### MediaBufferUnderflow

MediaBufferUnderflow
~~~cpp
void MediaBufferUnderflow(const MediaPlayerId& id);
~~~
 Called by MediaWebContentsObserver when a buffer underflow occurs. See the
 WebContentsObserver function stubs for more details.

### MediaPlayerSeek

MediaPlayerSeek
~~~cpp
void MediaPlayerSeek(const MediaPlayerId& id);
~~~
 Called by MediaWebContentsObserver when player seek event occurs.

### MediaDestroyed

MediaDestroyed
~~~cpp
void MediaDestroyed(const MediaPlayerId& id);
~~~
 Called by MediaWebContentsObserver when a media player is destroyed.

### GetCurrentlyPlayingVideoCount

GetCurrentlyPlayingVideoCount
~~~cpp
int GetCurrentlyPlayingVideoCount() override;
~~~

### GetFullscreenVideoSize

GetFullscreenVideoSize
~~~cpp
absl::optional<gfx::Size> GetFullscreenVideoSize() override;
~~~

### media_web_contents_observer

media_web_contents_observer
~~~cpp
MediaWebContentsObserver* media_web_contents_observer() {
    return media_web_contents_observer_.get();
  }
~~~

### UpdateWebContentsVisibility

UpdateWebContentsVisibility
~~~cpp
void UpdateWebContentsVisibility(Visibility visibility);
~~~
 Update the web contents visibility.

### GetPageVisibilityState

GetPageVisibilityState
~~~cpp
PageVisibilityState GetPageVisibilityState() const;
~~~
 Returns the PageVisibilityState for the primary page of this web contents,
 taking the capturing state into account.

### NotifyFindReply

NotifyFindReply
~~~cpp
void NotifyFindReply(int request_id,
                       int number_of_matches,
                       const gfx::Rect& selection_rect,
                       int active_match_ordinal,
                       bool final_update);
~~~
 Called by FindRequestManager when find replies come in from a renderer
 process.

### IncrementBluetoothConnectedDeviceCount

IncrementBluetoothConnectedDeviceCount
~~~cpp
void IncrementBluetoothConnectedDeviceCount();
~~~
 Modify the counter of connected devices for this WebContents.

### DecrementBluetoothConnectedDeviceCount

DecrementBluetoothConnectedDeviceCount
~~~cpp
void DecrementBluetoothConnectedDeviceCount();
~~~

### OnIsConnectedToBluetoothDeviceChanged

OnIsConnectedToBluetoothDeviceChanged
~~~cpp
void OnIsConnectedToBluetoothDeviceChanged(
      bool is_connected_to_bluetooth_device);
~~~
 Notifies the delegate and observers when the connected to Bluetooth device
 state changes.

### IncrementBluetoothScanningSessionsCount

IncrementBluetoothScanningSessionsCount
~~~cpp
void IncrementBluetoothScanningSessionsCount();
~~~

### DecrementBluetoothScanningSessionsCount

DecrementBluetoothScanningSessionsCount
~~~cpp
void DecrementBluetoothScanningSessionsCount();
~~~

### IncrementSerialActiveFrameCount

IncrementSerialActiveFrameCount
~~~cpp
void IncrementSerialActiveFrameCount();
~~~
 Modify the counter of frames in this WebContents actively using serial
 ports.

### DecrementSerialActiveFrameCount

DecrementSerialActiveFrameCount
~~~cpp
void DecrementSerialActiveFrameCount();
~~~

### IncrementHidActiveFrameCount

IncrementHidActiveFrameCount
~~~cpp
void IncrementHidActiveFrameCount();
~~~
 Modify the counter of frames in this WebContents actively using HID
 devices.

### DecrementHidActiveFrameCount

DecrementHidActiveFrameCount
~~~cpp
void DecrementHidActiveFrameCount();
~~~

### OnIsConnectedToUsbDeviceChanged

OnIsConnectedToUsbDeviceChanged
~~~cpp
void OnIsConnectedToUsbDeviceChanged(bool is_connected_to_usb_device);
~~~
 Notifies the delegate and observers when the connected to USB device state
 changes.

### IncrementUsbActiveFrameCount

IncrementUsbActiveFrameCount
~~~cpp
void IncrementUsbActiveFrameCount();
~~~
 Modify the counter of frames in this WebContents actively using USB
 devices.

### DecrementUsbActiveFrameCount

DecrementUsbActiveFrameCount
~~~cpp
void DecrementUsbActiveFrameCount();
~~~

### IncrementFileSystemAccessHandleCount

IncrementFileSystemAccessHandleCount
~~~cpp
void IncrementFileSystemAccessHandleCount();
~~~
 Modify the counter of File System Access handles for this WebContents.

### DecrementFileSystemAccessHandleCount

DecrementFileSystemAccessHandleCount
~~~cpp
void DecrementFileSystemAccessHandleCount();
~~~

### SetHasPersistentVideo

SetHasPersistentVideo
~~~cpp
void SetHasPersistentVideo(bool has_persistent_video);
~~~
 Called when the WebContents gains or loses a persistent video.

### IsPictureInPictureAllowedForFullscreenVideo

IsPictureInPictureAllowedForFullscreenVideo
~~~cpp
bool IsPictureInPictureAllowedForFullscreenVideo() const;
~~~
 Whether the WebContents effectively fullscreen active player allows
 Picture-in-Picture.

 |IsFullscreen| must return |true| when this method is called.

### SetAsFocusedWebContentsIfNecessary

SetAsFocusedWebContentsIfNecessary
~~~cpp
void SetAsFocusedWebContentsIfNecessary();
~~~
 Set this WebContents's `primary_frame_tree_` as the focused frame tree.

 `primary_frame_tree_`'s main frame RenderWidget (and all of its
 subframe widgets) will be activated. GetFocusedRenderWidgetHost will search
 this WebContentsImpl for a focused RenderWidgetHost. The previously focused
 WebContentsImpl, if any, will have its RenderWidgetHosts deactivated.

### SetFocusedFrameTree

SetFocusedFrameTree
~~~cpp
void SetFocusedFrameTree(FrameTree* frame_tree_to_focus);
~~~
 Sets the focused frame tree to be the `frame_tree_to_focus`.

 `frame_tree_to_focus` must be either this WebContents's frame tree or
 contained within it (but not owned by another WebContents).

### EnterPictureInPicture

EnterPictureInPicture
~~~cpp
PictureInPictureResult EnterPictureInPicture();
~~~
 Notifies the Picture-in-Picture controller that there is a new video player
 entering video Picture-in-Picture. (This is not used for document
 Picture-in-Picture,
 cf. PictureInPictureWindowManager::EnterDocumentPictureInPicture().)
 Returns the result of the enter request.

### ExitPictureInPicture

ExitPictureInPicture
~~~cpp
void ExitPictureInPicture();
~~~
 Updates the Picture-in-Picture controller with a signal that
 Picture-in-Picture mode has ended.

### SetHasPictureInPictureVideo

SetHasPictureInPictureVideo
~~~cpp
void SetHasPictureInPictureVideo(bool has_picture_in_picture_video);
~~~
 Updates the tracking information for |this| to know if there is
 a video currently in Picture-in-Picture mode.

### SetHasPictureInPictureDocument

SetHasPictureInPictureDocument
~~~cpp
void SetHasPictureInPictureDocument(bool has_picture_in_picture_document);
~~~
 Updates the tracking information for |this| to know if there is
 a document currently in Picture-in-Picture mode.

### SetSpatialNavigationDisabled

SetSpatialNavigationDisabled
~~~cpp
void SetSpatialNavigationDisabled(bool disabled);
~~~
 Sets the spatial navigation state.

### SetStylusHandwritingEnabled

SetStylusHandwritingEnabled
~~~cpp
void SetStylusHandwritingEnabled(bool enabled);
~~~
 Sets the Stylus handwriting feature status. This status is updated to web
 preferences.

### RunFileChooser

RunFileChooser
~~~cpp
void RunFileChooser(
      RenderFrameHost* render_frame_host,
      scoped_refptr<FileChooserImpl::FileSelectListenerImpl> listener,
      const blink::mojom::FileChooserParams& params);
~~~
 Called when a file selection is to be done.

### EnumerateDirectory

EnumerateDirectory
~~~cpp
void EnumerateDirectory(
      RenderFrameHost* render_frame_host,
      scoped_refptr<FileChooserImpl::FileSelectListenerImpl> listener,
      const base::FilePath& directory_path);
~~~
 Request to enumerate a directory.  This is equivalent to running the file
 chooser in directory-enumeration mode and having the user select the given
 directory.

### NotifyFindMatchRectsReply

NotifyFindMatchRectsReply
~~~cpp
void NotifyFindMatchRectsReply(int version,
                                 const std::vector<gfx::RectF>& rects,
                                 const gfx::RectF& active_rect);
~~~
 Called by FindRequestManager when all of the find match rects are in.

### SetDisplayCutoutSafeArea

SetDisplayCutoutSafeArea
~~~cpp
void SetDisplayCutoutSafeArea(gfx::Insets insets);
~~~
 Called by WebContentsAndroid to send the Display Cutout safe area to
 DisplayCutoutHostImpl.

### NotifyViewportFitChanged

NotifyViewportFitChanged
~~~cpp
void NotifyViewportFitChanged(blink::mojom::ViewportFit value);
~~~
 Notify observers that the viewport fit value changed. This is called by
 |DisplayCutoutHostImpl|.

### GetFindRequestManagerForTesting

GetFindRequestManagerForTesting
~~~cpp
FindRequestManager* GetFindRequestManagerForTesting();
~~~
 Returns the current FindRequestManager associated with the WebContents;
 this won't create one if none exists.

### InnerWebContentsCreated

InnerWebContentsCreated
~~~cpp
void InnerWebContentsCreated(WebContents* inner_web_contents);
~~~
 Convenience method to notify observers that an inner WebContents was
 created with |this| WebContents as its owner. This does *not* immediately
 guarantee that |inner_web_contents| has been added to the WebContents tree.

### DetachFromOuterWebContents

DetachFromOuterWebContents
~~~cpp
std::unique_ptr<WebContents> DetachFromOuterWebContents();
~~~
 Detaches this WebContents from its outer WebContents.

### ReattachToOuterWebContentsFrame

ReattachToOuterWebContentsFrame
~~~cpp
virtual void ReattachToOuterWebContentsFrame();
~~~
 Reattaches this inner WebContents to its outer WebContents.

### set_portal

set_portal
~~~cpp
void set_portal(Portal* portal) { portal_ = portal; }
~~~
 Getter/setter for the Portal associated with this WebContents. If non-null
 then this WebContents is embedded in a portal and its outer WebContents can
 be found by using GetOuterWebContents().

### portal

portal
~~~cpp
Portal* portal() const { return portal_; }
~~~

### NotifyInsidePortal

NotifyInsidePortal
~~~cpp
void NotifyInsidePortal(bool inside_portal);
~~~
 Sends a page message to notify every process in the frame tree if the
 web contents is a portal web contents.

### DidActivatePortal

DidActivatePortal
~~~cpp
void DidActivatePortal(WebContentsImpl* predecessor_web_contents,
                         base::TimeTicks activation_time);
~~~
 Notifies observers that this WebContents was activated. This contents'
 former portal host, |predecessor_web_contents|, has become a portal pending
 adoption.

 |activation_time| is the time the activation happened, in wall time.

### OnServiceWorkerAccessed

OnServiceWorkerAccessed
~~~cpp
void OnServiceWorkerAccessed(RenderFrameHost* render_frame_host,
                               const GURL& scope,
                               AllowServiceWorkerResult allowed);
~~~

### JavaScriptDialogDefersNavigations

JavaScriptDialogDefersNavigations
~~~cpp
bool JavaScriptDialogDefersNavigations() {
    return javascript_dialog_dismiss_notifier_.get();
  }
~~~

### NotifyOnJavaScriptDialogDismiss

NotifyOnJavaScriptDialogDismiss
~~~cpp
void NotifyOnJavaScriptDialogDismiss(base::OnceClosure callback);
~~~

### has_persistent_video

has_persistent_video
~~~cpp
bool has_persistent_video() { return has_persistent_video_; }
~~~

### GetFocusedFrameWidgetInputHandler

GetFocusedFrameWidgetInputHandler
~~~cpp
blink::mojom::FrameWidgetInputHandler* GetFocusedFrameWidgetInputHandler();
~~~
 Returns the focused frame's input handler.

### SystemDragEnded

SystemDragEnded
~~~cpp
void SystemDragEnded(RenderWidgetHost* source_rwh);
~~~
 A render view-originated drag has ended. Informs the render view host and
 WebContentsDelegate.

### ForEachRenderFrameHostWithAction

ForEachRenderFrameHostWithAction
~~~cpp
void ForEachRenderFrameHostWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHostImpl*)> on_frame);
~~~
 These are the content internal equivalents of
 |WebContents::ForEachRenderFrameHost| whose comment can be referred to
 for details. Content internals can also access speculative
 RenderFrameHostImpls if necessary by using the
 |ForEachRenderFrameHostIncludingSpeculative| variations.

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
void ForEachRenderFrameHost(
      base::FunctionRef<void(RenderFrameHostImpl*)> on_frame);
~~~

### ForEachRenderFrameHostIncludingSpeculativeWithAction

ForEachRenderFrameHostIncludingSpeculativeWithAction
~~~cpp
void ForEachRenderFrameHostIncludingSpeculativeWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHostImpl*)> on_frame);
~~~

### ForEachRenderFrameHostIncludingSpeculative

ForEachRenderFrameHostIncludingSpeculative
~~~cpp
void ForEachRenderFrameHostIncludingSpeculative(
      base::FunctionRef<void(RenderFrameHostImpl*)> on_frame);
~~~

### ComputeWebPreferences

ComputeWebPreferences
~~~cpp
const blink::web_pref::WebPreferences ComputeWebPreferences();
~~~
 Computes and returns the content specific preferences for this WebContents.

 Recomputes only the "fast" preferences (those not requiring slow
 platform/device polling); the remaining "slow" ones are recomputed only if
 the preference cache is empty.

### OnXrHasRenderTarget

OnXrHasRenderTarget
~~~cpp
void OnXrHasRenderTarget(const viz::FrameSinkId& frame_sink_id);
~~~
 Certain WebXr modes integrate with Viz as a compositor directly, and thus
 have their own FrameSinkId that typically renders fullscreen, obscuring
 the WebContents. This allows those WebXr modes to notify us if that
 is occurring. When it has finished, this method may be called again with an
 Invalid FrameSinkId to indicate such. Note that other fullscreen modes,
 e.g. Fullscreen videos, are largely controlled by the renderer process and
 as such are still parented under the existing FrameSinkId.

### GetCaptureFrameSinkId

GetCaptureFrameSinkId
~~~cpp
viz::FrameSinkId GetCaptureFrameSinkId();
~~~
 Because something else may be rendering as the primary contents of this
 WebContents rather than the RenderHostView, targets that wish to capture
 the contents of this WebContents should query for the FrameSinkId of the
 base compositor here.

### set_show_popup_menu_callback_for_testing

set_show_popup_menu_callback_for_testing
~~~cpp
void set_show_popup_menu_callback_for_testing(
      base::OnceCallback<void(const gfx::Rect&)> callback) {
    show_poup_menu_callback_ = std::move(callback);
  }
~~~

### set_minimum_delay_between_loading_updates_for_testing

set_minimum_delay_between_loading_updates_for_testing
~~~cpp
void set_minimum_delay_between_loading_updates_for_testing(
      base::TimeDelta duration) {
    minimum_delay_between_loading_updates_ms_ = duration;
  }
~~~
 Sets the value in tests to ensure expected ordering and correctness.

### CancelPrerendering

CancelPrerendering
~~~cpp
bool CancelPrerendering(FrameTreeNode* frame_tree_node,
                          PrerenderFinalStatus final_status);
~~~
 If the given frame is prerendered, cancels the associated prerender.

 Returns true if a prerender was canceled.

### set_suppress_ime_events_for_testing

set_suppress_ime_events_for_testing
~~~cpp
void set_suppress_ime_events_for_testing(bool suppress) {
    suppress_ime_events_for_testing_ = suppress;
  }
~~~

### mouse_lock_widget_for_testing

mouse_lock_widget_for_testing
~~~cpp
RenderWidgetHost* mouse_lock_widget_for_testing() {
    return mouse_lock_widget_;
  }
~~~

### set_last_navigation_was_prerender_activation_for_devtools

set_last_navigation_was_prerender_activation_for_devtools
~~~cpp
void set_last_navigation_was_prerender_activation_for_devtools() {
    last_navigation_was_prerender_activation_for_devtools_ = true;
  }
~~~
 Record a prerender activation for DevTools.

### last_navigation_was_prerender_activation_for_devtools

last_navigation_was_prerender_activation_for_devtools
~~~cpp
bool last_navigation_was_prerender_activation_for_devtools() {
    return last_navigation_was_prerender_activation_for_devtools_;
  }
~~~
 Check if prerender was just activated.

### GetVirtualKeyboardMode

GetVirtualKeyboardMode
~~~cpp
ui::mojom::VirtualKeyboardMode GetVirtualKeyboardMode() const;
~~~

### WebContentsImpl

WebContentsImpl
~~~cpp
WebContentsImpl(BrowserContext* browser_context)
~~~

### Create

Create
~~~cpp
static std::unique_ptr<WebContentsImpl> Create(const CreateParams& params);
~~~
 Covariant return type alternative for WebContents::Create(). Avoids
 need for casting of objects inside the content layer.

### AddObserver

AddObserver
~~~cpp
void AddObserver(WebContentsObserver* observer);
~~~
 Add and remove observers for page navigation notifications. The order in
 which notifications are sent to observers is undefined. Clients must be
 sure to remove the observer before they go away.

### RemoveObserver

RemoveObserver
~~~cpp
void RemoveObserver(WebContentsObserver* observer);
~~~

### SetPrimaryMainFrameProcessStatus

SetPrimaryMainFrameProcessStatus
~~~cpp
void SetPrimaryMainFrameProcessStatus(base::TerminationStatus status,
                                        int error_code);
~~~
 Indicates whether this tab should be considered crashed. The setter will
 also notify the delegate when the flag is changed.

### OnWebContentsDestroyed

OnWebContentsDestroyed
~~~cpp
void OnWebContentsDestroyed(WebContentsImpl* web_contents);
~~~
 Clears a pending contents that has been closed before being shown.

### OnRenderWidgetHostDestroyed

OnRenderWidgetHostDestroyed
~~~cpp
void OnRenderWidgetHostDestroyed(RenderWidgetHost* render_widget_host);
~~~
 Clears a pending render widget host that has been closed before being
 shown.

### AddWebContentsDestructionObserver

AddWebContentsDestructionObserver
~~~cpp
void AddWebContentsDestructionObserver(WebContentsImpl* web_contents);
~~~
 Creates and adds to the map a destruction observer watching `web_contents`.

 No-op if such an observer already exists.

### RemoveWebContentsDestructionObserver

RemoveWebContentsDestructionObserver
~~~cpp
void RemoveWebContentsDestructionObserver(WebContentsImpl* web_contents);
~~~
 Deletes and removes from the map a destruction observer
 watching `web_contents`. No-op if there is no such observer.

### AddRenderWidgetHostDestructionObserver

AddRenderWidgetHostDestructionObserver
~~~cpp
void AddRenderWidgetHostDestructionObserver(
      RenderWidgetHost* render_widget_host);
~~~
 Creates and adds to the map a destruction observer watching
 `render_widget_host`. No-op if such an observer already exists.

### RemoveRenderWidgetHostDestructionObserver

RemoveRenderWidgetHostDestructionObserver
~~~cpp
void RemoveRenderWidgetHostDestructionObserver(
      RenderWidgetHost* render_widget_host);
~~~
 Deletes and removes from the map a destruction observer
 watching `render_widget_host`. No-op if there is no such observer.

### GetRenderWidgetHostViewsInWebContentsTree

GetRenderWidgetHostViewsInWebContentsTree
~~~cpp
std::set<RenderWidgetHostViewBase*>
  GetRenderWidgetHostViewsInWebContentsTree();
~~~
 Traverses all the WebContents in the WebContentsTree and creates a set of
 all the unique RenderWidgetHostViews.

### OnDidDownloadImage

OnDidDownloadImage
~~~cpp
void OnDidDownloadImage(base::WeakPtr<RenderFrameHostImpl> rfh,
                          ImageDownloadCallback callback,
                          int id,
                          const GURL& image_url,
                          int32_t http_status_code,
                          const std::vector<SkBitmap>& images,
                          const std::vector<gfx::Size>& original_image_sizes);
~~~
 Called with the result of a DownloadImage() request.

### OnDialogClosed

OnDialogClosed
~~~cpp
void OnDialogClosed(int render_process_id,
                      int render_frame_id,
                      JavaScriptDialogCallback response_callback,
                      base::ScopedClosureRunner fullscreen_block,
                      bool dialog_was_suppressed,
                      bool success,
                      const std::u16string& user_input);
~~~
 Callback function when showing JavaScript dialogs. Takes in a routing ID
 pair to identify the RenderFrameHost that opened the dialog, because it's
 possible for the RenderFrameHost to be deleted by the time this is called.

### OnUpdateZoomLimits

OnUpdateZoomLimits
~~~cpp
void OnUpdateZoomLimits(RenderViewHostImpl* source,
                          int minimum_percent,
                          int maximum_percent);
~~~
 IPC message handlers.

### OnShowValidationMessage

OnShowValidationMessage
~~~cpp
void OnShowValidationMessage(RenderViewHostImpl* source,
                               const gfx::Rect& anchor_in_root_view,
                               const std::u16string& main_text,
                               const std::u16string& sub_text);
~~~

### OnHideValidationMessage

OnHideValidationMessage
~~~cpp
void OnHideValidationMessage(RenderViewHostImpl* source);
~~~

### OnMoveValidationMessage

OnMoveValidationMessage
~~~cpp
void OnMoveValidationMessage(RenderViewHostImpl* source,
                               const gfx::Rect& anchor_in_root_view);
~~~

### CanOverscrollContent

CanOverscrollContent
~~~cpp
bool CanOverscrollContent() const;
~~~
 Determines if content is allowed to overscroll. This value comes from the
 WebContentsDelegate, but can also be overridden by the WebContents.

### RecursivelyRegisterRenderWidgetHostViews

RecursivelyRegisterRenderWidgetHostViews
~~~cpp
void RecursivelyRegisterRenderWidgetHostViews();
~~~
###  Inner WebContents Helpers 
-------------------------------------------------

 These functions are helpers in managing a hierarchy of WebContents
 involved in rendering inner WebContents.

 The following functions update registrations for all RenderWidgetHostViews
 rooted at this WebContents. They are used when attaching/detaching an inner
 WebContents.


 Some properties of RenderWidgetHostViews, such as the FrameSinkId and
 TextInputManager, depend on the outermost WebContents, and must be updated
 during attach/detach.

### RecursivelyUnregisterRenderWidgetHostViews

RecursivelyUnregisterRenderWidgetHostViews
~~~cpp
void RecursivelyUnregisterRenderWidgetHostViews();
~~~

### ContainsOrIsFocusedWebContents

ContainsOrIsFocusedWebContents
~~~cpp
bool ContainsOrIsFocusedWebContents();
~~~
 When multiple WebContents are present within a tab or window, a single one
 is focused and will route keyboard events in most cases to a RenderWidget
 contained within it. |GetFocusedWebContents()|'s main frame widget will
 receive page focus and blur events when the containing window changes focus
 state.

 Returns true if |this| is the focused WebContents or an ancestor of the
 focused WebContents.

### InnerWebContentsAttached

InnerWebContentsAttached
~~~cpp
void InnerWebContentsAttached(WebContents* inner_web_contents);
~~~
 Called just after an inner web contents is attached.

### InnerWebContentsDetached

InnerWebContentsDetached
~~~cpp
void InnerWebContentsDetached(WebContents* inner_web_contents);
~~~
 Called just after an inner web contents is detached.

### GetCreatedWidget

GetCreatedWidget
~~~cpp
RenderWidgetHostView* GetCreatedWidget(int process_id, int route_id);
~~~
###  Navigation helpers 
--------------------------------------------------------

 These functions are helpers for Navigate() and DidNavigate().

 Handles post-navigation tasks in DidNavigate AFTER the entry has been
 committed to the navigation controller. Note that the navigation entry is
 not provided since it may be invalid/changed after being committed. The
 current navigation entry is in the NavigationController at this point.

 Finds the new RenderWidgetHost and returns it. Note that this can only be
 called once as this call also removes it from the internal map.

### GetCreatedWindow

GetCreatedWindow
~~~cpp
absl::optional<CreatedWindow> GetCreatedWindow(
      int process_id,
      int main_frame_widget_route_id);
~~~
 Finds the new CreatedWindow by |main_frame_widget_route_id|, initializes
 it for renderer-initiated creation, and returns it. Note that this can only
 be called once as this call also removes it from the internal map.

### ExecutePageBroadcastMethod

ExecutePageBroadcastMethod
~~~cpp
void ExecutePageBroadcastMethod(PageBroadcastMethodCallback callback);
~~~
 Execute a PageBroadcast Mojo method.

### ExecutePageBroadcastMethodForAllPages

ExecutePageBroadcastMethodForAllPages
~~~cpp
void ExecutePageBroadcastMethodForAllPages(
      PageBroadcastMethodCallback callback);
~~~
 Execute a PageBroadcast Mojo method for all MPArch pages.

### SetOpenerForNewContents

SetOpenerForNewContents
~~~cpp
void SetOpenerForNewContents(FrameTreeNode* opener, bool opener_suppressed);
~~~

### ResetLoadProgressState

ResetLoadProgressState
~~~cpp
void ResetLoadProgressState();
~~~
###  Tracking loading progress 
-------------------------------------------------
 Resets the tracking state of the current load progress.

### SendChangeLoadProgress

SendChangeLoadProgress
~~~cpp
void SendChangeLoadProgress();
~~~
 Notifies the delegate that the load progress was updated.

### LoadingStateChanged

LoadingStateChanged
~~~cpp
void LoadingStateChanged(bool should_show_loading_ui,
                           LoadNotificationDetails* details);
~~~
 Notifies the delegate of a change in loading state.

 |details| is used to provide details on the load that just finished
 (but can be null if not applicable).

### SetHistoryOffsetAndLengthForView

SetHistoryOffsetAndLengthForView
~~~cpp
void SetHistoryOffsetAndLengthForView(RenderViewHost* render_view_host,
                                        int history_offset,
                                        int history_length);
~~~
###  Misc non-view stuff 
-------------------------------------------------------
 Sets the history for a specified RenderViewHost to |history_length|
 entries, with an offset of |history_offset|.

### NotifyViewSwapped

NotifyViewSwapped
~~~cpp
void NotifyViewSwapped(RenderViewHost* old_view, RenderViewHost* new_view);
~~~
 Helper functions for sending notifications.

### NotifyFrameSwapped

NotifyFrameSwapped
~~~cpp
void NotifyFrameSwapped(RenderFrameHostImpl* old_frame,
                          RenderFrameHostImpl* new_frame);
~~~

### GetRenderManager

GetRenderManager
~~~cpp
RenderFrameHostManager* GetRenderManager();
~~~
 TODO(creis): This should take in a FrameTreeNode to know which node's
 render manager to return.  For now, we just return the root's.

### RemoveBrowserPluginEmbedder

RemoveBrowserPluginEmbedder
~~~cpp
void RemoveBrowserPluginEmbedder();
~~~
 Removes browser plugin embedder if there is one.

### GetSizeForMainFrame

GetSizeForMainFrame
~~~cpp
gfx::Size GetSizeForMainFrame();
~~~
 Returns the size that the main frame should be sized to.

### OnPreferredSizeChanged

OnPreferredSizeChanged
~~~cpp
void OnPreferredSizeChanged(const gfx::Size& old_size);
~~~
 Helper method that's called whenever |preferred_size_| or
 |preferred_size_for_capture_| changes, to propagate the new value to the
 |delegate_|.

### CreateWebUI

CreateWebUI
~~~cpp
std::unique_ptr<WebUIImpl> CreateWebUI(RenderFrameHostImpl* frame_host,
                                         const GURL& url);
~~~
 Internal helper to create WebUI objects associated with |this|. |url| is
 used to determine which WebUI should be created (if any).

### SetJavaScriptDialogManagerForTesting

SetJavaScriptDialogManagerForTesting
~~~cpp
void SetJavaScriptDialogManagerForTesting(
      JavaScriptDialogManager* dialog_manager);
~~~

### GetFindRequestManager

GetFindRequestManager
~~~cpp
FindRequestManager* GetFindRequestManager();
~~~
 Returns the FindRequestManager, which may be found in an outer WebContents.

### GetOrCreateFindRequestManager

GetOrCreateFindRequestManager
~~~cpp
FindRequestManager* GetOrCreateFindRequestManager();
~~~
 Returns the FindRequestManager, or tries to create one if it doesn't
  already exist. The FindRequestManager may be found in an outer
 WebContents. If this is an inner WebContents which is not yet attached to
 an outer WebContents the method will return nullptr.

### ShowInsecureLocalhostWarningIfNeeded

ShowInsecureLocalhostWarningIfNeeded
~~~cpp
void ShowInsecureLocalhostWarningIfNeeded(PageImpl& page);
~~~
 Prints a console warning when visiting a localhost site with a bad
 certificate via --allow-insecure-localhost.

### ParseDownloadHeaders

ParseDownloadHeaders
~~~cpp
static download::DownloadUrlParameters::RequestHeadersType
  ParseDownloadHeaders(const std::string& headers);
~~~
 Format of |headers| is a new line separated list of key value pairs:
 "<key1>: <value1>\r\n<key2>: <value2>".

### SetVisibilityForChildViews

SetVisibilityForChildViews
~~~cpp
void SetVisibilityForChildViews(bool visible);
~~~
 Sets the visibility of immediate child views, i.e. views whose parent view
 is that of the main frame.

### ClearTargetURL

ClearTargetURL
~~~cpp
void ClearTargetURL();
~~~
 A helper for clearing the link status bubble after navigating away.

 See also UpdateTargetURL.

### FullscreenFrameSetUpdated

FullscreenFrameSetUpdated
~~~cpp
void FullscreenFrameSetUpdated();
~~~
 Called each time |fullscreen_frames_| is updated. Find the new
 |current_fullscreen_frame_| and notify observers whenever it changes.

### OnNativeThemeUpdated

OnNativeThemeUpdated
~~~cpp
void OnNativeThemeUpdated(ui::NativeTheme* observed_theme) override;
~~~
 ui::NativeThemeObserver:
### OnCaptionStyleUpdated

OnCaptionStyleUpdated
~~~cpp
void OnCaptionStyleUpdated() override;
~~~

### OnColorProviderChanged

OnColorProviderChanged
~~~cpp
void OnColorProviderChanged() override;
~~~
 ui::ColorProviderSourceObserver:
### GetColorProvider

GetColorProvider
~~~cpp
const ui::ColorProvider& GetColorProvider() const override;
~~~
 Returns the ColorProvider instance for this WebContents object. This will
 always return a valid ColorProvider instance.

### UpdateVisibilityAndNotifyPageAndView

UpdateVisibilityAndNotifyPageAndView
~~~cpp
void UpdateVisibilityAndNotifyPageAndView(Visibility new_visibility,
                                            bool is_activity = true);
~~~
 Sets the visibility to |new_visibility| and propagates this to the
 renderer side, taking into account the current capture state. This
 can be called with the current visibility to affect capturing
 changes.

 |is_activity| controls whether a change to |visible| affects
 the value returned by GetLastActiveTime().

### GetCurrentPageUkmSourceId

GetCurrentPageUkmSourceId
~~~cpp
ukm::SourceId GetCurrentPageUkmSourceId() override;
~~~
 Returns UKM source id for the currently displayed page.

 Intentionally kept private, prefer using
 render_frame_host->GetPageUkmSourceId() if you already have a
 |render_frame_host| reference or
 GetPrimaryMainFrame()->GetPageUkmSourceId() if you don't.

### ForEachRenderViewHost

ForEachRenderViewHost
~~~cpp
void ForEachRenderViewHost(
      ForEachRenderViewHostTypes view_mask,
      RenderViewHostIterationCallback on_render_view_host);
~~~
 For each RenderViewHost (including bfcache, prerendering) call the
 callback, this will be filtered by `view_mask`.

### SetSlowWebPreferences

SetSlowWebPreferences
~~~cpp
void SetSlowWebPreferences(const base::CommandLine& command_line,
                             blink::web_pref::WebPreferences* prefs);
~~~
 Sets the hardware-related fields in |prefs| that are slow to compute.  The
 fields are set from cache if available, otherwise recomputed.

### ForEachRenderFrameHostImpl

ForEachRenderFrameHostImpl
~~~cpp
void ForEachRenderFrameHostImpl(
      base::FunctionRef<FrameIterationAction(RenderFrameHostImpl*)> on_frame,
      bool include_speculative);
~~~
 This is the actual implementation of the various overloads of
 |ForEachRenderFrameHost|.

### ForEachFrameTree

ForEachFrameTree
~~~cpp
void ForEachFrameTree(FrameTreeIterationCallback on_frame_tree);
~~~
 Calls |on_frame_tree| for every FrameTree in this WebContents.

 This does not descend into inner WebContents, but does include inner frame
 trees based on MPArch.

### GetOutermostFrameTrees

GetOutermostFrameTrees
~~~cpp
std::vector<FrameTree*> GetOutermostFrameTrees();
~~~
 Returns the primary frame tree, followed by any other outermost frame trees
 in this WebContents. Outermost frame trees include, for example,
 prerendering frame trees, and do not include, for example, fenced frames or
 portals. Also note that bfcached pages do not have a distinct frame tree,
 so the primary frame tree in the result would be the only FrameTree
 representing any bfcached pages.

### GetOutermostMainFrames

GetOutermostMainFrames
~~~cpp
std::vector<RenderFrameHostImpl*> GetOutermostMainFrames();
~~~
 Returns the primary main frame, followed by the main frames of any other
 outermost frame trees in this WebContents and the main frames of any
 bfcached pages. Note that unlike GetOutermostFrameTrees, bfcached pages
 have a distinct RenderFrameHostImpl in this result.

### DecrementCapturerCount

DecrementCapturerCount
~~~cpp
void DecrementCapturerCount(bool stay_hidden,
                              bool stay_awake,
                              bool is_activity = true);
~~~
 Called when the base::ScopedClosureRunner returned by
 IncrementCapturerCount() is destructed.

### CalculatePageVisibilityState

CalculatePageVisibilityState
~~~cpp
PageVisibilityState CalculatePageVisibilityState(Visibility visibility) const;
~~~
 Calculates the PageVisibilityState for |visibility|, taking the capturing
 state into account.

### NotifyPrimaryMainFrameProcessIsAlive

NotifyPrimaryMainFrameProcessIsAlive
~~~cpp
void NotifyPrimaryMainFrameProcessIsAlive();
~~~
 Called when the process hosting the primary main RenderFrameHost is known
 to be alive.

### UpdateTitleForEntryImpl

UpdateTitleForEntryImpl
~~~cpp
bool UpdateTitleForEntryImpl(NavigationEntryImpl* entry,
                               const std::u16string& title);
~~~
 Updates |entry|'s title. |entry| must belong to the WebContents' primary
 NavigationController. Returns true if |entry|'s title was changed, and
 false otherwise.

### NotifyTitleUpdateForEntry

NotifyTitleUpdateForEntry
~~~cpp
void NotifyTitleUpdateForEntry(NavigationEntryImpl* entry);
~~~
 Dispatches WebContentsObserver::TitleWasSet and also notifies the delegate
 of a title change if |entry| is the entry whose title is being used as the
 display title.

### GetNavigationEntryForTitle

GetNavigationEntryForTitle
~~~cpp
NavigationEntry* GetNavigationEntryForTitle();
~~~
 Returns the navigation entry whose title is used as the display title for
 this WebContents (i.e. for WebContents::GetTitle()).

### GetAvailablePointerAndHoverTypes

GetAvailablePointerAndHoverTypes
~~~cpp
static std::pair<int, int> GetAvailablePointerAndHoverTypes();
~~~
 Wrapper for ui::GetAvailablePointerAndHoverTypes which temporarily allows
 blocking calls required on Windows when running on touch enabled devices.

### SetHasPictureInPictureCommon

SetHasPictureInPictureCommon
~~~cpp
void SetHasPictureInPictureCommon(bool has_picture_in_picture);
~~~
 Apply shared logic for SetHasPictureInPictureVideo() and
 SetHasPictureInPictureDocument().

### DisallowCustomCursorScopeExpired

DisallowCustomCursorScopeExpired
~~~cpp
void DisallowCustomCursorScopeExpired();
~~~
 A scope that disallows custom cursors has expired.

## struct CreatedWindow
 CreatedWindow holds the WebContentsImpl and target url between IPC calls to
 CreateNewWindow and ShowCreatedWindow.

### CreatedWindow

CreatedWindow::CreatedWindow
~~~cpp
CreatedWindow(std::unique_ptr<WebContentsImpl> contents, GURL target_url)
~~~

### CreatedWindow

CreatedWindow::CreatedWindow
~~~cpp
CreatedWindow(CreatedWindow&&)
~~~

### CreatedWindow

CreatedWindow
~~~cpp
CreatedWindow(const CreatedWindow&) = delete;
~~~

### operator=

CreatedWindow::operator=
~~~cpp
CreatedWindow& operator=(CreatedWindow&&);
~~~

### operator=

CreatedWindow::operator=
~~~cpp
CreatedWindow& operator=(const CreatedWindow&) = delete;
  ~CreatedWindow();
~~~

## class WebContentsTreeNode
 Represents a WebContents node in a tree of WebContents structure.


 Two WebContents with separate FrameTrees can be connected by
 outer/inner relationship using this class. Note that their FrameTrees
 still remain disjoint.

 The parent is referred to as "outer WebContents" and the descendents are
 referred to as "inner WebContents".

 For each inner WebContents, the outer WebContents will have a
 corresponding FrameTreeNode.

### WebContentsTreeNode

WebContentsTreeNode::WebContentsTreeNode
~~~cpp
explicit WebContentsTreeNode(WebContentsImpl* current_web_contents);
~~~

### ~WebContentsTreeNode

WebContentsTreeNode::~WebContentsTreeNode
~~~cpp
~WebContentsTreeNode() final;
~~~

### AttachInnerWebContents

WebContentsTreeNode::AttachInnerWebContents
~~~cpp
void AttachInnerWebContents(std::unique_ptr<WebContents> inner_web_contents,
                                RenderFrameHostImpl* render_frame_host);
~~~
 Attaches |inner_web_contents| to the |render_frame_host| within this
 WebContents.

### DisconnectFromOuterWebContents

WebContentsTreeNode::DisconnectFromOuterWebContents
~~~cpp
std::unique_ptr<WebContents> DisconnectFromOuterWebContents();
~~~
 Disconnects the current WebContents from its outer WebContents, and
 returns ownership to the caller. This is used when activating a portal,
 which causes the WebContents to transition from an inner WebContents to
 an outer WebContents.

 TODO(lfg): Activating a nested portal currently replaces the outermost
 WebContents with the portal. We should allow replacing only the inner
 WebContents with the nested portal.

### outer_web_contents

outer_web_contents
~~~cpp
WebContentsImpl* outer_web_contents() const { return outer_web_contents_; }
~~~

### outer_contents_frame_tree_node_id

outer_contents_frame_tree_node_id
~~~cpp
int outer_contents_frame_tree_node_id() const {
      return outer_contents_frame_tree_node_id_;
    }
~~~

### OuterContentsFrameTreeNode

WebContentsTreeNode::OuterContentsFrameTreeNode
~~~cpp
FrameTreeNode* OuterContentsFrameTreeNode() const;
~~~

### focused_frame_tree

focused_frame_tree
~~~cpp
FrameTree* focused_frame_tree() { return &*focused_frame_tree_; }
~~~

### SetFocusedFrameTree

WebContentsTreeNode::SetFocusedFrameTree
~~~cpp
void SetFocusedFrameTree(FrameTree* frame_tree);
~~~

### GetInnerWebContentsInFrame

WebContentsTreeNode::GetInnerWebContentsInFrame
~~~cpp
WebContentsImpl* GetInnerWebContentsInFrame(const FrameTreeNode* frame);
~~~
 Returns the inner WebContents within |frame|, if one exists, or nullptr
 otherwise.

### GetInnerWebContents

WebContentsTreeNode::GetInnerWebContents
~~~cpp
std::vector<WebContentsImpl*> GetInnerWebContents() const;
~~~

### DetachInnerWebContents

WebContentsTreeNode::DetachInnerWebContents
~~~cpp
std::unique_ptr<WebContents> DetachInnerWebContents(
        WebContentsImpl* inner_web_contents);
~~~

### OnFrameTreeNodeDestroyed

WebContentsTreeNode::OnFrameTreeNodeDestroyed
~~~cpp
void OnFrameTreeNodeDestroyed(FrameTreeNode* node) final;
~~~
 FrameTreeNode::Observer implementation.

### current_web_contents_



~~~cpp

const raw_ptr<WebContentsImpl, DanglingUntriaged> current_web_contents_;

~~~

 The WebContents that owns this WebContentsTreeNode.

### outer_web_contents_



~~~cpp

raw_ptr<WebContentsImpl, DanglingUntriaged> outer_web_contents_;

~~~

 The outer WebContents of |current_web_contents_|, or nullptr if
 |current_web_contents_| is the outermost WebContents.

### outer_contents_frame_tree_node_id_



~~~cpp

int outer_contents_frame_tree_node_id_;

~~~

 The ID of the FrameTreeNode in the |outer_web_contents_| that hosts
 |current_web_contents_| as an inner WebContents.

### inner_web_contents_



~~~cpp

std::vector<std::unique_ptr<WebContents>> inner_web_contents_;

~~~

 List of inner WebContents that we host. The outer WebContents owns the
 inner WebContents.

### focused_frame_tree_



~~~cpp

base::SafeRef<FrameTree> focused_frame_tree_;

~~~

 Only the root node should have this set. This indicates the FrameTree
 that has the focused frame. The FrameTree tree could be arbitrarily deep.

 An inner WebContents if focused is responsible for setting this back to
 another valid during its destruction. See WebContentsImpl destructor.

 TODO(crbug.com/1257595): Support clearing this for inner frame trees.

### outer_web_contents

outer_web_contents
~~~cpp
WebContentsImpl* outer_web_contents() const { return outer_web_contents_; }
~~~

### outer_contents_frame_tree_node_id

outer_contents_frame_tree_node_id
~~~cpp
int outer_contents_frame_tree_node_id() const {
      return outer_contents_frame_tree_node_id_;
    }
~~~

### focused_frame_tree

focused_frame_tree
~~~cpp
FrameTree* focused_frame_tree() { return &*focused_frame_tree_; }
~~~

### AttachInnerWebContents

WebContentsTreeNode::AttachInnerWebContents
~~~cpp
void AttachInnerWebContents(std::unique_ptr<WebContents> inner_web_contents,
                                RenderFrameHostImpl* render_frame_host);
~~~
 Attaches |inner_web_contents| to the |render_frame_host| within this
 WebContents.

### DisconnectFromOuterWebContents

WebContentsTreeNode::DisconnectFromOuterWebContents
~~~cpp
std::unique_ptr<WebContents> DisconnectFromOuterWebContents();
~~~
 Disconnects the current WebContents from its outer WebContents, and
 returns ownership to the caller. This is used when activating a portal,
 which causes the WebContents to transition from an inner WebContents to
 an outer WebContents.

 TODO(lfg): Activating a nested portal currently replaces the outermost
 WebContents with the portal. We should allow replacing only the inner
 WebContents with the nested portal.

### OuterContentsFrameTreeNode

WebContentsTreeNode::OuterContentsFrameTreeNode
~~~cpp
FrameTreeNode* OuterContentsFrameTreeNode() const;
~~~

### SetFocusedFrameTree

WebContentsTreeNode::SetFocusedFrameTree
~~~cpp
void SetFocusedFrameTree(FrameTree* frame_tree);
~~~

### GetInnerWebContentsInFrame

WebContentsTreeNode::GetInnerWebContentsInFrame
~~~cpp
WebContentsImpl* GetInnerWebContentsInFrame(const FrameTreeNode* frame);
~~~
 Returns the inner WebContents within |frame|, if one exists, or nullptr
 otherwise.

### GetInnerWebContents

WebContentsTreeNode::GetInnerWebContents
~~~cpp
std::vector<WebContentsImpl*> GetInnerWebContents() const;
~~~

### DetachInnerWebContents

WebContentsTreeNode::DetachInnerWebContents
~~~cpp
std::unique_ptr<WebContents> DetachInnerWebContents(
        WebContentsImpl* inner_web_contents);
~~~

### OnFrameTreeNodeDestroyed

WebContentsTreeNode::OnFrameTreeNodeDestroyed
~~~cpp
void OnFrameTreeNodeDestroyed(FrameTreeNode* node) final;
~~~
 FrameTreeNode::Observer implementation.

### current_web_contents_



~~~cpp

const raw_ptr<WebContentsImpl, DanglingUntriaged> current_web_contents_;

~~~

 The WebContents that owns this WebContentsTreeNode.

### outer_web_contents_



~~~cpp

raw_ptr<WebContentsImpl, DanglingUntriaged> outer_web_contents_;

~~~

 The outer WebContents of |current_web_contents_|, or nullptr if
 |current_web_contents_| is the outermost WebContents.

### outer_contents_frame_tree_node_id_



~~~cpp

int outer_contents_frame_tree_node_id_;

~~~

 The ID of the FrameTreeNode in the |outer_web_contents_| that hosts
 |current_web_contents_| as an inner WebContents.

### inner_web_contents_



~~~cpp

std::vector<std::unique_ptr<WebContents>> inner_web_contents_;

~~~

 List of inner WebContents that we host. The outer WebContents owns the
 inner WebContents.

### focused_frame_tree_



~~~cpp

base::SafeRef<FrameTree> focused_frame_tree_;

~~~

 Only the root node should have this set. This indicates the FrameTree
 that has the focused frame. The FrameTree tree could be arbitrarily deep.

 An inner WebContents if focused is responsible for setting this back to
 another valid during its destruction. See WebContentsImpl destructor.

 TODO(crbug.com/1257595): Support clearing this for inner frame trees.

## class WebContentsObserverList
 Container for WebContentsObservers, which knows when we are iterating over
 observer set.

### WebContentsObserverList

WebContentsObserverList::WebContentsObserverList
~~~cpp
WebContentsObserverList();
~~~

### ~WebContentsObserverList

WebContentsObserverList::~WebContentsObserverList
~~~cpp
~WebContentsObserverList();
~~~

### AddObserver

WebContentsObserverList::AddObserver
~~~cpp
void AddObserver(WebContentsObserver* observer);
~~~

### RemoveObserver

WebContentsObserverList::RemoveObserver
~~~cpp
void RemoveObserver(WebContentsObserver* observer);
~~~

### NotifyObservers

NotifyObservers
~~~cpp
void NotifyObservers(T1 func, P1&&... args) {
      TRACE_EVENT0("content", "WebContentsObserverList::NotifyObservers");
      base::AutoReset<bool> scope(&is_notifying_observers_, true);
      for (WebContentsObserver& observer : observers_) {
        TRACE_EVENT0(TRACE_DISABLED_BY_DEFAULT("content.verbose"),
                     "Dispatching WebContentsObserver callback");
        ((observer).*(func))(std::forward<P1>(args)...);
      }
    }
~~~

### is_notifying_observers

is_notifying_observers
~~~cpp
bool is_notifying_observers() { return is_notifying_observers_; }
~~~

### observer_list

observer_list
~~~cpp
const base::ObserverList<WebContentsObserver>::Unchecked& observer_list() {
      return observers_;
    }
~~~
 Exposed to deal with IPC message handlers which need to stop iteration
 early.

### is_notifying_observers_



~~~cpp

bool is_notifying_observers_ = false;

~~~


### observers_



~~~cpp

base::ObserverList<WebContentsObserver>::Unchecked observers_;

~~~


### is_notifying_observers

is_notifying_observers
~~~cpp
bool is_notifying_observers() { return is_notifying_observers_; }
~~~

### observer_list

observer_list
~~~cpp
const base::ObserverList<WebContentsObserver>::Unchecked& observer_list() {
      return observers_;
    }
~~~
 Exposed to deal with IPC message handlers which need to stop iteration
 early.

### AddObserver

WebContentsObserverList::AddObserver
~~~cpp
void AddObserver(WebContentsObserver* observer);
~~~

### RemoveObserver

WebContentsObserverList::RemoveObserver
~~~cpp
void RemoveObserver(WebContentsObserver* observer);
~~~

### is_notifying_observers_



~~~cpp

bool is_notifying_observers_ = false;

~~~


### observers_



~~~cpp

base::ObserverList<WebContentsObserver>::Unchecked observers_;

~~~


## class WebContentsImpl::FriendWrapper
 Dangerous methods which should never be made part of the public API, so we
 grant their use only to an explicit friend list (c++ attorney/client idiom).

### FriendWrapper

FriendWrapper
~~~cpp
FriendWrapper(const FriendWrapper&) = delete;
~~~

### operator=

WebContentsImpl::FriendWrapper::operator=
~~~cpp
FriendWrapper& operator=(const FriendWrapper&) = delete;
~~~

### AddCreatedCallbackForTesting

WebContentsImpl::FriendWrapper::AddCreatedCallbackForTesting
~~~cpp
static base::CallbackListSubscription AddCreatedCallbackForTesting(
      const CreatedCallback& callback);
~~~
 Not instantiable.

 Adds a callback called on creation of each new WebContents.
