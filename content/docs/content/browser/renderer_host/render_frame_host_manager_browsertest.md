
## class RenderFrameHostManagerTest

### RenderFrameHostManagerTest

RenderFrameHostManagerTest::RenderFrameHostManagerTest
~~~cpp
RenderFrameHostManagerTest();
~~~

### ~RenderFrameHostManagerTest

RenderFrameHostManagerTest::~RenderFrameHostManagerTest
~~~cpp
~RenderFrameHostManagerTest() override;
~~~

### SetUpOnMainThread

RenderFrameHostManagerTest::SetUpOnMainThread
~~~cpp
void SetUpOnMainThread() override;
~~~

### DisableBackForwardCache

RenderFrameHostManagerTest::DisableBackForwardCache
~~~cpp
void DisableBackForwardCache(
      BackForwardCacheImpl::DisableForTestingReason reason) const;
~~~

### StartServer

RenderFrameHostManagerTest::StartServer
~~~cpp
void StartServer();
~~~

### StartEmbeddedServer

RenderFrameHostManagerTest::StartEmbeddedServer
~~~cpp
void StartEmbeddedServer();
~~~

### SetupRequestFailForURL

RenderFrameHostManagerTest::SetupRequestFailForURL
~~~cpp
std::unique_ptr<content::URLLoaderInterceptor> SetupRequestFailForURL(
      const GURL& url);
~~~

### GetCrossSiteURL

RenderFrameHostManagerTest::GetCrossSiteURL
~~~cpp
GURL GetCrossSiteURL(const std::string& path);
~~~
 Returns a URL on foo.com with the given path.

### NavigateToPageWithLinks

RenderFrameHostManagerTest::NavigateToPageWithLinks
~~~cpp
void NavigateToPageWithLinks(Shell* shell);
~~~

### AssertCanRemoveSubframeInUnload

RenderFrameHostManagerTest::AssertCanRemoveSubframeInUnload
~~~cpp
void AssertCanRemoveSubframeInUnload(bool same_site);
~~~

### foo_com_



~~~cpp

std::string foo_com_;

~~~


### replace_host_



~~~cpp

GURL::Replacements replace_host_;

~~~


### foo_host_port_



~~~cpp

net::HostPortPair foo_host_port_;

~~~


### feature_list_



~~~cpp

base::test::ScopedFeatureList feature_list_;

~~~


### SetUpOnMainThread

RenderFrameHostManagerTest::SetUpOnMainThread
~~~cpp
void SetUpOnMainThread() override;
~~~

### DisableBackForwardCache

RenderFrameHostManagerTest::DisableBackForwardCache
~~~cpp
void DisableBackForwardCache(
      BackForwardCacheImpl::DisableForTestingReason reason) const;
~~~

### StartServer

RenderFrameHostManagerTest::StartServer
~~~cpp
void StartServer();
~~~

### StartEmbeddedServer

RenderFrameHostManagerTest::StartEmbeddedServer
~~~cpp
void StartEmbeddedServer();
~~~

### SetupRequestFailForURL

RenderFrameHostManagerTest::SetupRequestFailForURL
~~~cpp
std::unique_ptr<content::URLLoaderInterceptor> SetupRequestFailForURL(
      const GURL& url);
~~~

### GetCrossSiteURL

RenderFrameHostManagerTest::GetCrossSiteURL
~~~cpp
GURL GetCrossSiteURL(const std::string& path);
~~~
 Returns a URL on foo.com with the given path.

### NavigateToPageWithLinks

RenderFrameHostManagerTest::NavigateToPageWithLinks
~~~cpp
void NavigateToPageWithLinks(Shell* shell);
~~~

### AssertCanRemoveSubframeInUnload

RenderFrameHostManagerTest::AssertCanRemoveSubframeInUnload
~~~cpp
void AssertCanRemoveSubframeInUnload(bool same_site);
~~~

### foo_com_



~~~cpp

std::string foo_com_;

~~~


### replace_host_



~~~cpp

GURL::Replacements replace_host_;

~~~


### foo_host_port_



~~~cpp

net::HostPortPair foo_host_port_;

~~~


### feature_list_



~~~cpp

base::test::ScopedFeatureList feature_list_;

~~~

