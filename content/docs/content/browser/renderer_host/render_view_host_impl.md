### From

From
~~~cpp
static RenderViewHostImpl* From(RenderWidgetHost* rwh);
~~~
 Convenience function, just like RenderViewHost::From.

### GetPlatformSpecificPrefs

GetPlatformSpecificPrefs
~~~cpp
static void GetPlatformSpecificPrefs(blink::RendererPreferences* prefs);
~~~

### HasNonBackForwardCachedInstancesForProcess

HasNonBackForwardCachedInstancesForProcess
~~~cpp
static bool HasNonBackForwardCachedInstancesForProcess(
      RenderProcessHost* process);
~~~
 Checks whether any RenderViewHostImpl instance associated with a given
 process is not currently in the back-forward cache.

 TODO(https://crbug.com/1125996): Remove once a well-behaved frozen
 RenderFrame never send IPCs messages, even if there are active pages in the
 process.

### RenderViewHostImpl

RenderViewHostImpl
~~~cpp
RenderViewHostImpl(
      FrameTree* frame_tree,
      SiteInstanceGroup* group,
      const StoragePartitionConfig& storage_partition_config,
      std::unique_ptr<RenderWidgetHostImpl> widget,
      RenderViewHostDelegate* delegate,
      int32_t routing_id,
      int32_t main_frame_routing_id,
      bool has_initialized_audio_host,
      scoped_refptr<BrowsingContextState> main_browsing_context_state,
      CreateRenderViewHostCase create_case)
~~~

### RenderViewHostImpl

RenderViewHostImpl
~~~cpp
RenderViewHostImpl(const RenderViewHostImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
RenderViewHostImpl& operator=(const RenderViewHostImpl&) = delete;
~~~

### GetWidget

GetWidget
~~~cpp
RenderWidgetHostImpl* GetWidget() const override;
~~~
 RenderViewHost implementation.

### GetProcess

GetProcess
~~~cpp
RenderProcessHost* GetProcess() const override;
~~~

### GetRoutingID

GetRoutingID
~~~cpp
int GetRoutingID() const override;
~~~

### EnablePreferredSizeMode

EnablePreferredSizeMode
~~~cpp
void EnablePreferredSizeMode() override;
~~~

### WriteIntoTrace

WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedProto<TraceProto> context) const override;
~~~

### SendWebPreferencesToRenderer

SendWebPreferencesToRenderer
~~~cpp
void SendWebPreferencesToRenderer();
~~~

### SendRendererPreferencesToRenderer

SendRendererPreferencesToRenderer
~~~cpp
void SendRendererPreferencesToRenderer(
      const blink::RendererPreferences& preferences);
~~~

### RenderProcessExited

RenderProcessExited
~~~cpp
void RenderProcessExited(RenderProcessHost* host,
                           const ChildProcessTerminationInfo& info) override;
~~~
 RenderProcessHostObserver implementation
### OnGpuSwitched

OnGpuSwitched
~~~cpp
void OnGpuSwitched(gl::GpuPreference active_gpu_heuristic) override;
~~~
 GpuSwitchingObserver implementation.

### CreateRenderView

CreateRenderView
~~~cpp
virtual bool CreateRenderView(
      const absl::optional<blink::FrameToken>& opener_frame_token,
      int proxy_route_id,
      bool window_was_opened_by_another_window);
~~~
 Set up the `blink::WebView` child process. Virtual because it is overridden
 by TestRenderViewHost.

 `opener_route_id` parameter indicates which `blink::WebView` created this
   (MSG_ROUTING_NONE if none).

 `window_was_opened_by_another_window` is true if this top-level frame was
   created by another window, as opposed to independently created (through
   the browser UI, etc). This is true even when the window is opened with
   "noopener", and even if the opener has been closed since.

 `proxy_route_id` is only used when creating a `blink::WebView` in an
   inactive state.

### GetDelegate

GetDelegate
~~~cpp
RenderViewHostDelegate* GetDelegate();
~~~

### is_speculative

is_speculative
~~~cpp
bool is_speculative() { return is_speculative_; }
~~~

### set_is_speculative

set_is_speculative
~~~cpp
void set_is_speculative(bool is_speculative) {
    is_speculative_ = is_speculative;
  }
~~~

### set_is_registered_with_frame_tree

set_is_registered_with_frame_tree
~~~cpp
void set_is_registered_with_frame_tree(bool is_registered) {
    registered_with_frame_tree_ = is_registered;
  }
~~~

### rvh_map_id

rvh_map_id
~~~cpp
FrameTree::RenderViewHostMapId rvh_map_id() const {
    return render_view_host_map_id_;
  }
~~~

### GetWeakPtr

GetWeakPtr
~~~cpp
base::WeakPtr<RenderViewHostImpl> GetWeakPtr();
~~~

### is_active

is_active
~~~cpp
bool is_active() const { return main_frame_routing_id_ != MSG_ROUTING_NONE; }
~~~
 Tracks whether this RenderViewHost is in an active state (rather than
 pending unload or unloaded), according to its main frame
 RenderFrameHost.

### IsRenderViewLive

IsRenderViewLive
~~~cpp
bool IsRenderViewLive() const;
~~~
 Returns true if the `blink::WebView` is active and has not crashed.

### RenderViewCreated

RenderViewCreated
~~~cpp
void RenderViewCreated(RenderFrameHostImpl* local_main_frame);
~~~
 Called when the `blink::WebView` in the renderer process has been created,
 at which point IsRenderViewLive() becomes true, and the mojo connections to
 the renderer process for this view now exist.

### GetMainRenderFrameHost

GetMainRenderFrameHost
~~~cpp
RenderFrameHostImpl* GetMainRenderFrameHost();
~~~
 Returns the main RenderFrameHostImpl associated with this RenderViewHost or
 null if it doesn't exist. It's null if the main frame is represented in
 this RenderViewHost by RenderFrameProxyHost (from Blink perspective,
 blink::Page's main blink::Frame is remote).

### main_browsing_context_state

main_browsing_context_state
~~~cpp
const absl::optional<base::SafeRef<BrowsingContextState>>&
  main_browsing_context_state() const {
    return main_browsing_context_state_;
  }
~~~
 RenderViewHost is associated with a given SiteInstanceGroup and as
 BrowsingContextState in non-legacy BrowsingContextState mode is tied to a
 given BrowsingInstance, so the main BrowsingContextState stays the same
 during the entire lifetime of a RenderViewHost: cross-SiteInstanceGroup
 same-BrowsingInstance navigations might change the representation of the
 main frame in a given `blink::WebView` from RenderFrame to
 `blink::RemoteFrame` and back, while cross-BrowsingInstances result in
 creating a new unrelated RenderViewHost. This is not true in the legacy BCS
 mode, so there the `main_browsing_context_state_` is null.

### GetAgentSchedulingGroup

GetAgentSchedulingGroup
~~~cpp
AgentSchedulingGroupHost& GetAgentSchedulingGroup() const;
~~~
 Returns the `AgentSchedulingGroupHost` this view is associated with (via
 the widget).

### AnimateDoubleTapZoom

AnimateDoubleTapZoom
~~~cpp
void AnimateDoubleTapZoom(const gfx::Point& point, const gfx::Rect& rect);
~~~
 Tells the renderer process to request a page-scale animation based on the
 specified point/rect.

### ZoomToFindInPageRect

ZoomToFindInPageRect
~~~cpp
void ZoomToFindInPageRect(const gfx::Rect& rect_to_zoom);
~~~
 Requests a page-scale animation based on the specified rect.

### SetInitialFocus

SetInitialFocus
~~~cpp
void SetInitialFocus(bool reverse);
~~~
 Tells the renderer view to focus the first (last if reverse is true) node.

### PostRenderViewReady

PostRenderViewReady
~~~cpp
void PostRenderViewReady();
~~~
 Send RenderViewReady to observers once the process is launched, but not
 re-entrantly.

### OnHardwareConfigurationChanged

OnHardwareConfigurationChanged
~~~cpp
void OnHardwareConfigurationChanged();
~~~
 Passes current web preferences to the renderer after recomputing all of
 them, including the slow-to-compute hardware preferences.

 (WebContents::OnWebPreferencesChanged is a faster alternate that avoids
 slow recomputations.)
### SetMainFrameRoutingId

SetMainFrameRoutingId
~~~cpp
void SetMainFrameRoutingId(int routing_id);
~~~
 Sets the routing id for the main frame. When set to MSG_ROUTING_NONE, the
 view is not considered active.

### EnterBackForwardCache

EnterBackForwardCache
~~~cpp
void EnterBackForwardCache();
~~~
 Called when the RenderFrameHostImpls/RenderFrameProxyHosts that own this
 RenderViewHost enter the BackForwardCache.

### DidReceiveBackForwardCacheAck

DidReceiveBackForwardCacheAck
~~~cpp
bool DidReceiveBackForwardCacheAck();
~~~
 Indicates whether or not |this| has received an acknowledgement from
 renderer that it has enered BackForwardCache.

### LeaveBackForwardCache

LeaveBackForwardCache
~~~cpp
void LeaveBackForwardCache(
      blink::mojom::PageRestoreParamsPtr page_restore_params);
~~~
 Called when the RenderFrameHostImpls/RenderFrameProxyHosts that own this
 RenderViewHost leave the BackForwardCache. This occurs immediately before a
 restored document is committed.

 |page_restore_params| includes information that is needed by the page after
 getting restored, which includes the latest history information (offset,
 length) and the timestamp corresponding to the start of the back-forward
 cached navigation, which would be communicated to the page to allow it to
 record the latency of this navigation.

### is_in_back_forward_cache

is_in_back_forward_cache
~~~cpp
bool is_in_back_forward_cache() const { return is_in_back_forward_cache_; }
~~~

### ActivatePrerenderedPage

ActivatePrerenderedPage
~~~cpp
void ActivatePrerenderedPage(blink::mojom::PrerenderPageActivationParamsPtr
                                   prerender_page_activation_params,
                               base::OnceClosure callback);
~~~

### SetFrameTreeVisibility

SetFrameTreeVisibility
~~~cpp
void SetFrameTreeVisibility(blink::mojom::PageVisibilityState visibility);
~~~

### SetIsFrozen

SetIsFrozen
~~~cpp
void SetIsFrozen(bool frozen);
~~~

### OnBackForwardCacheTimeout

OnBackForwardCacheTimeout
~~~cpp
void OnBackForwardCacheTimeout();
~~~

### MaybeEvictFromBackForwardCache

MaybeEvictFromBackForwardCache
~~~cpp
void MaybeEvictFromBackForwardCache();
~~~

### EnforceBackForwardCacheSizeLimit

EnforceBackForwardCacheSizeLimit
~~~cpp
void EnforceBackForwardCacheSizeLimit();
~~~

### GetPageLifecycleStateManager

GetPageLifecycleStateManager
~~~cpp
PageLifecycleStateManager* GetPageLifecycleStateManager() {
    return page_lifecycle_state_manager_.get();
  }
~~~

### CollectSurfaceIdsForEviction

CollectSurfaceIdsForEviction
~~~cpp
std::vector<viz::SurfaceId> CollectSurfaceIdsForEviction();
~~~
 Called during frame eviction to return all SurfaceIds in the frame tree.

 Marks all views in the frame tree as evicted.

### IsTestRenderViewHost

IsTestRenderViewHost
~~~cpp
virtual bool IsTestRenderViewHost() const;
~~~
 Manual RTTI to ensure safe downcasts in tests.

### SetWillEnterBackForwardCacheCallbackForTesting

SetWillEnterBackForwardCacheCallbackForTesting
~~~cpp
void SetWillEnterBackForwardCacheCallbackForTesting(
      const WillEnterBackForwardCacheCallbackForTesting& callback);
~~~

### SetWillSendRendererPreferencesCallbackForTesting

SetWillSendRendererPreferencesCallbackForTesting
~~~cpp
void SetWillSendRendererPreferencesCallbackForTesting(
      const WillSendRendererPreferencesCallbackForTesting& callback);
~~~

### BindPageBroadcast

BindPageBroadcast
~~~cpp
void BindPageBroadcast(
      mojo::PendingAssociatedRemote<blink::mojom::PageBroadcast>
          page_broadcast);
~~~

### GetAssociatedPageBroadcast

GetAssociatedPageBroadcast
~~~cpp
const mojo::AssociatedRemote<blink::mojom::PageBroadcast>&
  GetAssociatedPageBroadcast();
~~~
 The remote mojom::PageBroadcast interface that is used to send messages to
 the renderer's blink::WebViewImpl when broadcasting messages to all
 renderers hosting frames in the frame tree.

### PrepareToLeaveBackForwardCache

PrepareToLeaveBackForwardCache
~~~cpp
void PrepareToLeaveBackForwardCache(base::OnceClosure done_cb);
~~~
 Prepares the renderer page to leave the back-forward cache by disabling
 Javascript eviction. |done_cb| is called upon receipt of the
 acknowledgement from the renderer that this has actually happened.


 After |done_cb| is called you can be certain that this renderer will not
 trigger an eviction of this page.

### frame_tree

frame_tree
~~~cpp
FrameTree* frame_tree() const { return frame_tree_; }
~~~
 TODO(https://crbug.com/1179502): FrameTree and FrameTreeNode will not be
 const as with prerenderer activation the page needs to move between
 FrameTreeNodes and FrameTrees. As it's hard to make sure that all places
 handle this transition correctly, MPArch will remove references from this
 class to FrameTree/FrameTreeNode.

### SetFrameTree

SetFrameTree
~~~cpp
void SetFrameTree(FrameTree& frame_tree);
~~~

### DisallowReuse

DisallowReuse
~~~cpp
void DisallowReuse();
~~~
 Mark this RenderViewHost as not available for reuse. This will remove
 it from being registered with the associated FrameTree.

### GetSafeRef

GetSafeRef
~~~cpp
base::SafeRef<RenderViewHostImpl> GetSafeRef();
~~~

### site_instance_group

site_instance_group
~~~cpp
SiteInstanceGroup* site_instance_group() const {
    return &*site_instance_group_;
  }
~~~

### ~RenderViewHostImpl

~RenderViewHostImpl
~~~cpp
~RenderViewHostImpl() override
~~~

### RenderWidgetGotFocus

RenderWidgetGotFocus
~~~cpp
void RenderWidgetGotFocus() override;
~~~
 RenderWidgetHostOwnerDelegate overrides.

### RenderWidgetLostFocus

RenderWidgetLostFocus
~~~cpp
void RenderWidgetLostFocus() override;
~~~

### RenderWidgetDidForwardMouseEvent

RenderWidgetDidForwardMouseEvent
~~~cpp
void RenderWidgetDidForwardMouseEvent(
      const blink::WebMouseEvent& mouse_event) override;
~~~

### MayRenderWidgetForwardKeyboardEvent

MayRenderWidgetForwardKeyboardEvent
~~~cpp
bool MayRenderWidgetForwardKeyboardEvent(
      const NativeWebKeyboardEvent& key_event) override;
~~~

### ShouldContributePriorityToProcess

ShouldContributePriorityToProcess
~~~cpp
bool ShouldContributePriorityToProcess() override;
~~~

### SetBackgroundOpaque

SetBackgroundOpaque
~~~cpp
void SetBackgroundOpaque(bool opaque) override;
~~~

### IsMainFrameActive

IsMainFrameActive
~~~cpp
bool IsMainFrameActive() override;
~~~

### IsNeverComposited

IsNeverComposited
~~~cpp
bool IsNeverComposited() override;
~~~

### GetWebkitPreferencesForWidget

GetWebkitPreferencesForWidget
~~~cpp
blink::web_pref::WebPreferences GetWebkitPreferencesForWidget() override;
~~~

### OnShowView

OnShowView
~~~cpp
void OnShowView(int route_id,
                  WindowOpenDisposition disposition,
                  const gfx::Rect& initial_rect,
                  bool user_gesture);
~~~
 IPC message handlers.

### OnShowWidget

OnShowWidget
~~~cpp
void OnShowWidget(int widget_route_id, const gfx::Rect& initial_rect);
~~~

### OnPasteFromSelectionClipboard

OnPasteFromSelectionClipboard
~~~cpp
void OnPasteFromSelectionClipboard();
~~~

### OnTakeFocus

OnTakeFocus
~~~cpp
void OnTakeFocus(bool reverse);
~~~

### OnFocus

OnFocus
~~~cpp
void OnFocus();
~~~

### OnMessageReceived

OnMessageReceived
~~~cpp
bool OnMessageReceived(const IPC::Message& msg) override;
~~~
 IPC::Listener implementation.

### ToDebugString

ToDebugString
~~~cpp
std::string ToDebugString() override;
~~~

### RenderViewReady

RenderViewReady
~~~cpp
void RenderViewReady();
~~~

