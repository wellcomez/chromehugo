
## class RenderFrameHostAndroid
 Android wrapper around RenderFrameHost that provides safer passage from java
 and back to native and provides java with a means of communicating with its
 native counterpart.

### RenderFrameHostAndroid

RenderFrameHostAndroid::RenderFrameHostAndroid
~~~cpp
RenderFrameHostAndroid(RenderFrameHostImpl* render_frame_host);
~~~

### RenderFrameHostAndroid

RenderFrameHostAndroid
~~~cpp
RenderFrameHostAndroid(const RenderFrameHostAndroid&) = delete;
~~~

### operator=

operator=
~~~cpp
RenderFrameHostAndroid& operator=(const RenderFrameHostAndroid&) = delete;
~~~

### ~RenderFrameHostAndroid

RenderFrameHostAndroid::~RenderFrameHostAndroid
~~~cpp
~RenderFrameHostAndroid() override;
~~~

### GetJavaObject

RenderFrameHostAndroid::GetJavaObject
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetJavaObject();
~~~

### GetLastCommittedURL

RenderFrameHostAndroid::GetLastCommittedURL
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetLastCommittedURL(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~
 Methods called from Java
### GetLastCommittedOrigin

RenderFrameHostAndroid::GetLastCommittedOrigin
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetLastCommittedOrigin(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&);
~~~

### GetCanonicalUrlForSharing

RenderFrameHostAndroid::GetCanonicalUrlForSharing
~~~cpp
void GetCanonicalUrlForSharing(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jobject>& jcallback) const;
~~~

### GetAllRenderFrameHosts

RenderFrameHostAndroid::GetAllRenderFrameHosts
~~~cpp
base::android::ScopedJavaLocalRef<jobjectArray> GetAllRenderFrameHosts(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~

### IsFeatureEnabled

RenderFrameHostAndroid::IsFeatureEnabled
~~~cpp
bool IsFeatureEnabled(JNIEnv* env,
                        const base::android::JavaParamRef<jobject>&,
                        jint feature) const;
~~~

### GetAndroidOverlayRoutingToken

RenderFrameHostAndroid::GetAndroidOverlayRoutingToken
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetAndroidOverlayRoutingToken(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~
 Returns UnguessableToken.

### NotifyUserActivation

RenderFrameHostAndroid::NotifyUserActivation
~~~cpp
void NotifyUserActivation(JNIEnv* env,
                            const base::android::JavaParamRef<jobject>&);
~~~

### SignalCloseWatcherIfActive

RenderFrameHostAndroid::SignalCloseWatcherIfActive
~~~cpp
jboolean SignalCloseWatcherIfActive(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~

### IsRenderFrameLive

RenderFrameHostAndroid::IsRenderFrameLive
~~~cpp
jboolean IsRenderFrameLive(JNIEnv* env,
                             const base::android::JavaParamRef<jobject>&) const;
~~~

### GetInterfaceToRendererFrame

RenderFrameHostAndroid::GetInterfaceToRendererFrame
~~~cpp
void GetInterfaceToRendererFrame(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jstring>& interface_name,
      jlong message_pipe_handle) const;
~~~

### TerminateRendererDueToBadMessage

RenderFrameHostAndroid::TerminateRendererDueToBadMessage
~~~cpp
void TerminateRendererDueToBadMessage(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      jint reason) const;
~~~

### IsProcessBlocked

RenderFrameHostAndroid::IsProcessBlocked
~~~cpp
jboolean IsProcessBlocked(JNIEnv* env,
                            const base::android::JavaParamRef<jobject>&) const;
~~~

### PerformGetAssertionWebAuthSecurityChecks

RenderFrameHostAndroid::PerformGetAssertionWebAuthSecurityChecks
~~~cpp
base::android::ScopedJavaLocalRef<jobject>
  PerformGetAssertionWebAuthSecurityChecks(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jstring>&,
      const base::android::JavaParamRef<jobject>&,
      jboolean is_payment_credential_get_assertion) const;
~~~

### PerformMakeCredentialWebAuthSecurityChecks

RenderFrameHostAndroid::PerformMakeCredentialWebAuthSecurityChecks
~~~cpp
jint PerformMakeCredentialWebAuthSecurityChecks(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jstring>&,
      const base::android::JavaParamRef<jobject>&,
      jboolean is_payment_credential_creation) const;
~~~

### GetLifecycleState

RenderFrameHostAndroid::GetLifecycleState
~~~cpp
jint GetLifecycleState(JNIEnv* env,
                         const base::android::JavaParamRef<jobject>&) const;
~~~

### InsertVisualStateCallback

RenderFrameHostAndroid::InsertVisualStateCallback
~~~cpp
void InsertVisualStateCallback(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>& jcallback);
~~~

### render_frame_host

render_frame_host
~~~cpp
RenderFrameHostImpl* render_frame_host() const { return render_frame_host_; }
~~~

### render_frame_host_



~~~cpp

const raw_ptr<RenderFrameHostImpl> render_frame_host_;

~~~


### obj_



~~~cpp

JavaObjectWeakGlobalRef obj_;

~~~


### RenderFrameHostAndroid

RenderFrameHostAndroid
~~~cpp
RenderFrameHostAndroid(const RenderFrameHostAndroid&) = delete;
~~~

### operator=

operator=
~~~cpp
RenderFrameHostAndroid& operator=(const RenderFrameHostAndroid&) = delete;
~~~

### render_frame_host

render_frame_host
~~~cpp
RenderFrameHostImpl* render_frame_host() const { return render_frame_host_; }
~~~

### GetJavaObject

RenderFrameHostAndroid::GetJavaObject
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetJavaObject();
~~~

### GetLastCommittedURL

RenderFrameHostAndroid::GetLastCommittedURL
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetLastCommittedURL(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~
 Methods called from Java
### GetLastCommittedOrigin

RenderFrameHostAndroid::GetLastCommittedOrigin
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetLastCommittedOrigin(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&);
~~~

### GetCanonicalUrlForSharing

RenderFrameHostAndroid::GetCanonicalUrlForSharing
~~~cpp
void GetCanonicalUrlForSharing(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jobject>& jcallback) const;
~~~

### GetAllRenderFrameHosts

RenderFrameHostAndroid::GetAllRenderFrameHosts
~~~cpp
base::android::ScopedJavaLocalRef<jobjectArray> GetAllRenderFrameHosts(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~

### IsFeatureEnabled

RenderFrameHostAndroid::IsFeatureEnabled
~~~cpp
bool IsFeatureEnabled(JNIEnv* env,
                        const base::android::JavaParamRef<jobject>&,
                        jint feature) const;
~~~

### GetAndroidOverlayRoutingToken

RenderFrameHostAndroid::GetAndroidOverlayRoutingToken
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetAndroidOverlayRoutingToken(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~
 Returns UnguessableToken.

### NotifyUserActivation

RenderFrameHostAndroid::NotifyUserActivation
~~~cpp
void NotifyUserActivation(JNIEnv* env,
                            const base::android::JavaParamRef<jobject>&);
~~~

### SignalCloseWatcherIfActive

RenderFrameHostAndroid::SignalCloseWatcherIfActive
~~~cpp
jboolean SignalCloseWatcherIfActive(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&) const;
~~~

### IsRenderFrameLive

RenderFrameHostAndroid::IsRenderFrameLive
~~~cpp
jboolean IsRenderFrameLive(JNIEnv* env,
                             const base::android::JavaParamRef<jobject>&) const;
~~~

### GetInterfaceToRendererFrame

RenderFrameHostAndroid::GetInterfaceToRendererFrame
~~~cpp
void GetInterfaceToRendererFrame(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jstring>& interface_name,
      jlong message_pipe_handle) const;
~~~

### TerminateRendererDueToBadMessage

RenderFrameHostAndroid::TerminateRendererDueToBadMessage
~~~cpp
void TerminateRendererDueToBadMessage(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      jint reason) const;
~~~

### IsProcessBlocked

RenderFrameHostAndroid::IsProcessBlocked
~~~cpp
jboolean IsProcessBlocked(JNIEnv* env,
                            const base::android::JavaParamRef<jobject>&) const;
~~~

### PerformGetAssertionWebAuthSecurityChecks

RenderFrameHostAndroid::PerformGetAssertionWebAuthSecurityChecks
~~~cpp
base::android::ScopedJavaLocalRef<jobject>
  PerformGetAssertionWebAuthSecurityChecks(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jstring>&,
      const base::android::JavaParamRef<jobject>&,
      jboolean is_payment_credential_get_assertion) const;
~~~

### PerformMakeCredentialWebAuthSecurityChecks

RenderFrameHostAndroid::PerformMakeCredentialWebAuthSecurityChecks
~~~cpp
jint PerformMakeCredentialWebAuthSecurityChecks(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>&,
      const base::android::JavaParamRef<jstring>&,
      const base::android::JavaParamRef<jobject>&,
      jboolean is_payment_credential_creation) const;
~~~

### GetLifecycleState

RenderFrameHostAndroid::GetLifecycleState
~~~cpp
jint GetLifecycleState(JNIEnv* env,
                         const base::android::JavaParamRef<jobject>&) const;
~~~

### InsertVisualStateCallback

RenderFrameHostAndroid::InsertVisualStateCallback
~~~cpp
void InsertVisualStateCallback(
      JNIEnv* env,
      const base::android::JavaParamRef<jobject>& jcallback);
~~~

### render_frame_host_



~~~cpp

const raw_ptr<RenderFrameHostImpl> render_frame_host_;

~~~


### obj_



~~~cpp

JavaObjectWeakGlobalRef obj_;

~~~

