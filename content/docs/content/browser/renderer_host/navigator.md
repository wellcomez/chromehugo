
## class Navigator::;
 Holds data used to track browser side navigation metrics.

### RecordNavigationMetrics

;::Navigator::RecordNavigationMetrics
~~~cpp
void RecordNavigationMetrics(
      const LoadCommittedDetails& details,
      const mojom::DidCommitProvisionalLoadParams& params,
      SiteInstance* site_instance,
      const GURL& original_request_url);
~~~

## class Navigator
 Navigator is responsible for performing navigations in nodes of the
 FrameTree. Its lifetime is bound to the FrameTree.

### Navigator

Navigator
~~~cpp
Navigator(const Navigator&) = delete;
~~~

### operator=

Navigator::operator=
~~~cpp
Navigator& operator=(const Navigator&) = delete;

  ~Navigator();
~~~

### CheckWebUIRendererDoesNotDisplayNormalURL

Navigator::CheckWebUIRendererDoesNotDisplayNormalURL
~~~cpp
[[nodiscard]] static bool CheckWebUIRendererDoesNotDisplayNormalURL(
      RenderFrameHostImpl* render_frame_host,
      const UrlInfo& url_info,
      bool is_renderer_initiated_check);
~~~
 This method verifies that a navigation to |url| doesn't commit into a WebUI
 process if it is not allowed to. Callers of this method should take one of
 two actions if the method returns false:
 * When called from browser process logic (e.g. NavigationRequest), this
   indicates issues with the navigation logic and the browser process must
   be terminated to avoid security issues.

 * If the codepath is processing an IPC message from a renderer process,
   then the renderer process is misbehaving and must be terminated.

 TODO(nasko): Remove the is_renderer_initiated_check parameter when callers
 of this method are migrated to use CHECK instead of DumpWithoutCrashing.

### ShouldIgnoreIncomingRendererRequest

Navigator::ShouldIgnoreIncomingRendererRequest
~~~cpp
static bool ShouldIgnoreIncomingRendererRequest(
      const NavigationRequest* ongoing_navigation_request,
      bool has_user_gesture);
~~~

### GetDelegate

Navigator::GetDelegate
~~~cpp
NavigatorDelegate* GetDelegate();
~~~
 Returns the delegate of this Navigator.

### DidNavigate

Navigator::DidNavigate
~~~cpp
void DidNavigate(RenderFrameHostImpl* render_frame_host,
                   const mojom::DidCommitProvisionalLoadParams& params,
                   std::unique_ptr<NavigationRequest> navigation_request,
                   bool was_within_same_document);
~~~
###  Notifications coming from the RenderFrameHosts 
----------------------------
 The RenderFrameHostImpl has committed a navigation. The Navigator is
 responsible for resetting |navigation_request| at the end of this method
 and should not attempt to keep it alive. Note: it is possible that
 |navigation_request| is not the NavigationRequest stored in the
 RenderFrameHost that just committed. This happens for example when a
 same-page navigation commits while another navigation is ongoing. The
 Navigator should use the NavigationRequest provided by this method and not
 attempt to access the RenderFrameHost's NavigationsRequests.

### StartHistoryNavigationInNewSubframe

Navigator::StartHistoryNavigationInNewSubframe
~~~cpp
bool StartHistoryNavigationInNewSubframe(
      RenderFrameHostImpl* render_frame_host,
      mojo::PendingAssociatedRemote<mojom::NavigationClient>*
          navigation_client);
~~~
 Called on a newly created subframe during a history navigation. The browser
 process looks up the corresponding FrameNavigationEntry for the new frame
 navigates it in the correct process. Returns false if the
 FrameNavigationEntry can't be found or the navigation fails.

### Navigate

Navigator::Navigate
~~~cpp
void Navigate(std::unique_ptr<NavigationRequest> request,
                ReloadType reload_type);
~~~
###  Navigation requests 
-------------------------------------------------------
 Called by the NavigationController to cause the Navigator to navigate to
 |navigation_request|. The NavigationController should be called back with
 RendererDidNavigate on success or DiscardPendingEntry on failure. The
 callbacks should be called in a future iteration of the message loop.

### RequestOpenURL

Navigator::RequestOpenURL
~~~cpp
void RequestOpenURL(
      RenderFrameHostImpl* render_frame_host,
      const GURL& url,
      const blink::LocalFrameToken* initiator_frame_token,
      int initiator_process_id,
      const absl::optional<url::Origin>& initiator_origin,
      const absl::optional<GURL>& initiator_base_url,
      const scoped_refptr<network::ResourceRequestBody>& post_body,
      const std::string& extra_headers,
      const Referrer& referrer,
      WindowOpenDisposition disposition,
      bool should_replace_current_entry,
      bool user_gesture,
      blink::mojom::TriggeringEventInfo triggering_event_info,
      const std::string& href_translate,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      const absl::optional<blink::Impression>& impression);
~~~
 The RenderFrameHostImpl has received a request to open a URL with the
 specified |disposition|.

### NavigateFromFrameProxy

Navigator::NavigateFromFrameProxy
~~~cpp
void NavigateFromFrameProxy(
      RenderFrameHostImpl* render_frame_host,
      const GURL& url,
      const blink::LocalFrameToken* initiator_frame_token,
      int initiator_process_id,
      const url::Origin& initiator_origin,
      const GURL& initiator_base_url,
      SiteInstance* source_site_instance,
      const Referrer& referrer,
      ui::PageTransition page_transition,
      bool should_replace_current_entry,
      blink::NavigationDownloadPolicy download_policy,
      const std::string& method,
      scoped_refptr<network::ResourceRequestBody> post_body,
      const std::string& extra_headers,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      network::mojom::SourceLocationPtr source_location,
      bool has_user_gesture,
      bool is_form_submission,
      const absl::optional<blink::Impression>& impression,
      blink::mojom::NavigationInitiatorActivationAndAdStatus
          initiator_activation_and_ad_status,
      base::TimeTicks navigation_start_time,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      bool is_unfenced_top_navigation = false,
      bool force_new_browsing_instance = false,
      bool is_container_initiated = false,
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt);
~~~
 Called when a document requests a navigation in another document through a
 `blink::RemoteFrame`. If `method` is "POST", then `post_body` needs to
 specify the request body, otherwise `post_body` should be null.

### BeforeUnloadCompleted

Navigator::BeforeUnloadCompleted
~~~cpp
void BeforeUnloadCompleted(FrameTreeNode* frame_tree_node,
                             bool proceed,
                             const base::TimeTicks& proceed_time);
~~~
 Called after BeforeUnloadCompleted callback is invoked from the renderer.

 If |frame_tree_node| has a NavigationRequest waiting for the renderer
 response, then the request is either started or canceled, depending on the
 value of |proceed|.

### OnBeginNavigation

Navigator::OnBeginNavigation
~~~cpp
void OnBeginNavigation(
      FrameTreeNode* frame_tree_node,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::BeginNavigationParamsPtr begin_params,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      mojo::PendingAssociatedRemote<mojom::NavigationClient> navigation_client,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache,
      mojo::PendingReceiver<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener);
~~~
 Used to start a new renderer-initiated navigation, following a
 BeginNavigation IPC from the renderer.

### RestartNavigationAsCrossDocument

Navigator::RestartNavigationAsCrossDocument
~~~cpp
void RestartNavigationAsCrossDocument(
      std::unique_ptr<NavigationRequest> navigation_request);
~~~
 Used to restart a navigation that was thought to be same-document in
 cross-document mode.

### CancelNavigation

Navigator::CancelNavigation
~~~cpp
void CancelNavigation(FrameTreeNode* frame_tree_node,
                        NavigationDiscardReason reason);
~~~
 Cancels the NavigationRequest owned by |frame_tree_node|. Note that this
 will only cancel NavigationRequests that haven't reached the "pending
 commit" stage yet, as after that the NavigationRequests will no longer be
 owned by the FrameTreeNode.

### LogBeforeUnloadTime

Navigator::LogBeforeUnloadTime
~~~cpp
void LogBeforeUnloadTime(base::TimeTicks renderer_before_unload_start_time,
                           base::TimeTicks renderer_before_unload_end_time,
                           base::TimeTicks before_unload_sent_time,
                           bool for_legacy);
~~~
 Called to record the time it took to execute the beforeunload hook for the
 current navigation. See RenderFrameHostImpl::SendBeforeUnload() for details
 on `for_legacy`.

### LogCommitNavigationSent

Navigator::LogCommitNavigationSent
~~~cpp
void LogCommitNavigationSent();
~~~
 Called to record the time that the RenderFrameHost told the renderer to
 commit the current navigation.

### controller

controller
~~~cpp
NavigationControllerImpl& controller() { return controller_; }
~~~
 Returns the NavigationController associated with this Navigator.

### RecordNavigationMetrics

Navigator::RecordNavigationMetrics
~~~cpp
void RecordNavigationMetrics(
      const LoadCommittedDetails& details,
      const mojom::DidCommitProvisionalLoadParams& params,
      SiteInstance* site_instance,
      const GURL& original_request_url);
~~~

### GetNavigationEntryForRendererInitiatedNavigation

Navigator::GetNavigationEntryForRendererInitiatedNavigation
~~~cpp
NavigationEntryImpl* GetNavigationEntryForRendererInitiatedNavigation(
      const blink::mojom::CommonNavigationParams& common_params,
      FrameTreeNode* frame_tree_node,
      bool override_user_agent);
~~~
 Called when a renderer initiated navigation has started. Returns the
 pending NavigationEntry to be used. Either null or a new one owned
 NavigationController.

### LogRendererInitiatedBeforeUnloadTime

Navigator::LogRendererInitiatedBeforeUnloadTime
~~~cpp
void LogRendererInitiatedBeforeUnloadTime(
      base::TimeTicks renderer_before_unload_start_time,
      base::TimeTicks renderer_before_unload_end_time);
~~~
 Called to record the time it took to execute beforeunload handlers for
 renderer-inititated navigations. It records the time it took to execute
 beforeunload handlers in the renderer process before sending the
 BeginNavigation IPC.
