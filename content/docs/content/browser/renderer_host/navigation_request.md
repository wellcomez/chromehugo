### CreateBrowserInitiated

CreateBrowserInitiated
~~~cpp
static std::unique_ptr<NavigationRequest> CreateBrowserInitiated(
      FrameTreeNode* frame_tree_node,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool was_opener_suppressed,
      const blink::LocalFrameToken* initiator_frame_token,
      int initiator_process_id,
      const std::string& extra_headers,
      FrameNavigationEntry* frame_entry,
      NavigationEntryImpl* entry,
      bool is_form_submission,
      std::unique_ptr<NavigationUIData> navigation_ui_data,
      const absl::optional<blink::Impression>& impression,
      bool is_pdf,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt);
~~~
 Creates a request for a browser-initiated navigation.

### Create

Create
~~~cpp
static std::unique_ptr<NavigationRequest> Create(
      FrameTreeNode* frame_tree_node,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool browser_initiated,
      bool was_opener_suppressed,
      const blink::LocalFrameToken* initiator_frame_token,
      int initiator_process_id,
      const std::string& extra_headers,
      FrameNavigationEntry* frame_entry,
      NavigationEntryImpl* entry,
      bool is_form_submission,
      std::unique_ptr<NavigationUIData> navigation_ui_data,
      const absl::optional<blink::Impression>& impression,
      blink::mojom::NavigationInitiatorActivationAndAdStatus
          initiator_activation_and_ad_status,
      bool is_pdf,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      bool is_container_initiated = false,
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt);
~~~
 Creates a request for either a browser-initiated navigation or a
 renderer-initiated navigation.  Normally, renderer-initiated navigations
 use CreateRendererInitiated(), but some legacy renderer-initiated
 navigation paths, such as OpenURL, are stuck using this path instead;
 these cases specify `browser_initiated` as false.


 Do NOT add more uses of this function.  Browser-initiated navigations
 should use CreateBrowserInitiated() and renderer-initiated navigations
 should use CreateRendererInitiated().


 TODO(crbug.com/1399292): Refactor the remaining uses of this function to
 use either CreateBrowserInitiated() or CreateRendererInitiated() and then
 remove this helper.

### CreateRendererInitiated

CreateRendererInitiated
~~~cpp
static std::unique_ptr<NavigationRequest> CreateRendererInitiated(
      FrameTreeNode* frame_tree_node,
      NavigationEntryImpl* entry,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::BeginNavigationParamsPtr begin_params,
      int current_history_list_offset,
      int current_history_list_length,
      bool override_user_agent,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      mojo::PendingAssociatedRemote<mojom::NavigationClient> navigation_client,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache,
      mojo::PendingReceiver<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener);
~~~
 Creates a request for a renderer-initiated navigation.

### CreateForSynchronousRendererCommit

CreateForSynchronousRendererCommit
~~~cpp
static std::unique_ptr<NavigationRequest> CreateForSynchronousRendererCommit(
      FrameTreeNode* frame_tree_node,
      RenderFrameHostImpl* render_frame_host,
      bool is_same_document,
      const GURL& url,
      const url::Origin& origin,
      const absl::optional<GURL>& initiator_base_url,
      const net::IsolationInfo& isolation_info_for_subresources,
      blink::mojom::ReferrerPtr referrer,
      const ui::PageTransition& transition,
      bool should_replace_current_entry,
      const std::string& method,
      bool has_transient_activation,
      bool is_overriding_user_agent,
      const std::vector<GURL>& redirects,
      const GURL& original_url,
      std::unique_ptr<CrossOriginEmbedderPolicyReporter> coep_reporter,
      std::unique_ptr<SubresourceWebBundleNavigationInfo>
          subresource_web_bundle_navigation_info,
      int http_response_code);
~~~
 Creates a NavigationRequest for synchronous navigation that have committed
 in the renderer process. Those are:
 - same-document renderer-initiated navigations.

 - synchronous about:blank navigations.


 TODO(clamy): Eventually, this should only be called for same-document
 renderer-initiated navigations.

### From

From
~~~cpp
static NavigationRequest* From(NavigationHandle* handle);
~~~

### NavigationTypeToReloadType

NavigationTypeToReloadType
~~~cpp
static ReloadType NavigationTypeToReloadType(
      blink::mojom::NavigationType type);
~~~
 If |type| is a reload, returns the equivalent ReloadType. Otherwise returns
 ReloadType::NONE.

### NavigationRequest

NavigationRequest
~~~cpp
NavigationRequest(const NavigationRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
NavigationRequest& operator=(const NavigationRequest&) = delete;
~~~

### ~NavigationRequest

~NavigationRequest
~~~cpp
~NavigationRequest() override
~~~

### HasCommittingOrigin

HasCommittingOrigin
~~~cpp
bool HasCommittingOrigin(const url::Origin& origin);
~~~
 Returns true if this request's URL matches |origin| and the request state
 is at (or past) WILL_PROCESS_RESPONSE.

### ShouldRequestSiteIsolationForCOOP

ShouldRequestSiteIsolationForCOOP
~~~cpp
bool ShouldRequestSiteIsolationForCOOP();
~~~
 Returns true if this navigation's COOP header implies that the destination
 site of this navigation should be site-isolated.  In addition to checking
 for eligible COOP header values, this function also verifies other
 criteria, such as whether this feature is enabled on the device (e.g.,
 above memory threshold) or whether the site is already isolated.

### GetNavigationId

GetNavigationId
~~~cpp
int64_t GetNavigationId() override;
~~~
 NavigationHandle implementation:
### GetNextPageUkmSourceId

GetNextPageUkmSourceId
~~~cpp
ukm::SourceId GetNextPageUkmSourceId() override;
~~~

### GetURL

GetURL
~~~cpp
const GURL& GetURL() override;
~~~

### GetStartingSiteInstance

GetStartingSiteInstance
~~~cpp
SiteInstanceImpl* GetStartingSiteInstance() override;
~~~

### GetSourceSiteInstance

GetSourceSiteInstance
~~~cpp
SiteInstanceImpl* GetSourceSiteInstance() override;
~~~

### IsInMainFrame

IsInMainFrame
~~~cpp
bool IsInMainFrame() const override;
~~~

### IsInPrimaryMainFrame

IsInPrimaryMainFrame
~~~cpp
bool IsInPrimaryMainFrame() const override;
~~~

### IsInOutermostMainFrame

IsInOutermostMainFrame
~~~cpp
bool IsInOutermostMainFrame() override;
~~~

### IsInPrerenderedMainFrame

IsInPrerenderedMainFrame
~~~cpp
bool IsInPrerenderedMainFrame() const override;
~~~

### IsPrerenderedPageActivation

IsPrerenderedPageActivation
~~~cpp
bool IsPrerenderedPageActivation() const override;
~~~

### IsInFencedFrameTree

IsInFencedFrameTree
~~~cpp
bool IsInFencedFrameTree() const override;
~~~

### GetNavigatingFrameType

GetNavigatingFrameType
~~~cpp
FrameType GetNavigatingFrameType() const override;
~~~

### IsRendererInitiated

IsRendererInitiated
~~~cpp
bool IsRendererInitiated() override;
~~~

### GetNavigationInitiatorActivationAndAdStatus

GetNavigationInitiatorActivationAndAdStatus
~~~cpp
blink::mojom::NavigationInitiatorActivationAndAdStatus
  GetNavigationInitiatorActivationAndAdStatus() override;
~~~

### IsSameOrigin

IsSameOrigin
~~~cpp
bool IsSameOrigin() override;
~~~

### WasServerRedirect

WasServerRedirect
~~~cpp
bool WasServerRedirect() override;
~~~

### GetRedirectChain

GetRedirectChain
~~~cpp
const std::vector<GURL>& GetRedirectChain() override;
~~~

### GetFrameTreeNodeId

GetFrameTreeNodeId
~~~cpp
int GetFrameTreeNodeId() override;
~~~

### GetParentFrame

GetParentFrame
~~~cpp
RenderFrameHostImpl* GetParentFrame() override;
~~~

### GetParentFrameOrOuterDocument

GetParentFrameOrOuterDocument
~~~cpp
RenderFrameHostImpl* GetParentFrameOrOuterDocument() override;
~~~

### NavigationStart

NavigationStart
~~~cpp
base::TimeTicks NavigationStart() override;
~~~

### NavigationInputStart

NavigationInputStart
~~~cpp
base::TimeTicks NavigationInputStart() override;
~~~

### GetNavigationHandleTiming

GetNavigationHandleTiming
~~~cpp
const NavigationHandleTiming& GetNavigationHandleTiming() override;
~~~

### IsPost

IsPost
~~~cpp
bool IsPost() override;
~~~

### GetReferrer

GetReferrer
~~~cpp
const blink::mojom::Referrer& GetReferrer() override;
~~~

### SetReferrer

SetReferrer
~~~cpp
void SetReferrer(blink::mojom::ReferrerPtr referrer) override;
~~~

### HasUserGesture

HasUserGesture
~~~cpp
bool HasUserGesture() override;
~~~

### GetPageTransition

GetPageTransition
~~~cpp
ui::PageTransition GetPageTransition() override;
~~~

### GetNavigationUIData

GetNavigationUIData
~~~cpp
NavigationUIData* GetNavigationUIData() override;
~~~

### IsExternalProtocol

IsExternalProtocol
~~~cpp
bool IsExternalProtocol() override;
~~~

### GetNetErrorCode

GetNetErrorCode
~~~cpp
net::Error GetNetErrorCode() override;
~~~

### GetRenderFrameHost

GetRenderFrameHost
~~~cpp
RenderFrameHostImpl* GetRenderFrameHost() const override;
~~~

### IsSameDocument

IsSameDocument
~~~cpp
bool IsSameDocument() const override;
~~~

### HasCommitted

HasCommitted
~~~cpp
bool HasCommitted() const override;
~~~

### IsErrorPage

IsErrorPage
~~~cpp
bool IsErrorPage() const override;
~~~

### HasSubframeNavigationEntryCommitted

HasSubframeNavigationEntryCommitted
~~~cpp
bool HasSubframeNavigationEntryCommitted() override;
~~~

### DidReplaceEntry

DidReplaceEntry
~~~cpp
bool DidReplaceEntry() override;
~~~

### ShouldUpdateHistory

ShouldUpdateHistory
~~~cpp
bool ShouldUpdateHistory() override;
~~~

### GetPreviousPrimaryMainFrameURL

GetPreviousPrimaryMainFrameURL
~~~cpp
const GURL& GetPreviousPrimaryMainFrameURL() override;
~~~

### GetSocketAddress

GetSocketAddress
~~~cpp
net::IPEndPoint GetSocketAddress() override;
~~~

### GetRequestHeaders

GetRequestHeaders
~~~cpp
const net::HttpRequestHeaders& GetRequestHeaders() override;
~~~

### RemoveRequestHeader

RemoveRequestHeader
~~~cpp
void RemoveRequestHeader(const std::string& header_name) override;
~~~

### SetRequestHeader

SetRequestHeader
~~~cpp
void SetRequestHeader(const std::string& header_name,
                        const std::string& header_value) override;
~~~

### SetCorsExemptRequestHeader

SetCorsExemptRequestHeader
~~~cpp
void SetCorsExemptRequestHeader(const std::string& header_name,
                                  const std::string& header_value) override;
~~~

### GetResponseHeaders

GetResponseHeaders
~~~cpp
const net::HttpResponseHeaders* GetResponseHeaders() override;
~~~

### GetConnectionInfo

GetConnectionInfo
~~~cpp
net::HttpResponseInfo::ConnectionInfo GetConnectionInfo() override;
~~~

### GetSSLInfo

GetSSLInfo
~~~cpp
const absl::optional<net::SSLInfo>& GetSSLInfo() override;
~~~

### GetAuthChallengeInfo

GetAuthChallengeInfo
~~~cpp
const absl::optional<net::AuthChallengeInfo>& GetAuthChallengeInfo() override;
~~~

### GetResolveErrorInfo

GetResolveErrorInfo
~~~cpp
net::ResolveErrorInfo GetResolveErrorInfo() override;
~~~

### GetIsolationInfo

GetIsolationInfo
~~~cpp
net::IsolationInfo GetIsolationInfo() override;
~~~

### RegisterThrottleForTesting

RegisterThrottleForTesting
~~~cpp
void RegisterThrottleForTesting(
      std::unique_ptr<NavigationThrottle> navigation_throttle) override;
~~~

### IsDeferredForTesting

IsDeferredForTesting
~~~cpp
bool IsDeferredForTesting() override;
~~~

### IsCommitDeferringConditionDeferredForTesting

IsCommitDeferringConditionDeferredForTesting
~~~cpp
bool IsCommitDeferringConditionDeferredForTesting() override;
~~~

### GetCommitDeferringConditionForTesting

GetCommitDeferringConditionForTesting
~~~cpp
CommitDeferringCondition* GetCommitDeferringConditionForTesting() override;
~~~

### WasStartedFromContextMenu

WasStartedFromContextMenu
~~~cpp
bool WasStartedFromContextMenu() override;
~~~

### GetSearchableFormURL

GetSearchableFormURL
~~~cpp
const GURL& GetSearchableFormURL() override;
~~~

### GetSearchableFormEncoding

GetSearchableFormEncoding
~~~cpp
const std::string& GetSearchableFormEncoding() override;
~~~

### GetReloadType

GetReloadType
~~~cpp
ReloadType GetReloadType() override;
~~~

### GetRestoreType

GetRestoreType
~~~cpp
RestoreType GetRestoreType() override;
~~~

### GetBaseURLForDataURL

GetBaseURLForDataURL
~~~cpp
const GURL& GetBaseURLForDataURL() override;
~~~

### GetGlobalRequestID

GetGlobalRequestID
~~~cpp
const GlobalRequestID& GetGlobalRequestID() override;
~~~

### IsDownload

IsDownload
~~~cpp
bool IsDownload() override;
~~~

### IsFormSubmission

IsFormSubmission
~~~cpp
bool IsFormSubmission() override;
~~~

### WasInitiatedByLinkClick

WasInitiatedByLinkClick
~~~cpp
bool WasInitiatedByLinkClick() override;
~~~

### IsSignedExchangeInnerResponse

IsSignedExchangeInnerResponse
~~~cpp
bool IsSignedExchangeInnerResponse() override;
~~~

### HasPrefetchedAlternativeSubresourceSignedExchange

HasPrefetchedAlternativeSubresourceSignedExchange
~~~cpp
bool HasPrefetchedAlternativeSubresourceSignedExchange() override;
~~~

### WasResponseCached

WasResponseCached
~~~cpp
bool WasResponseCached() override;
~~~

### GetProxyServer

GetProxyServer
~~~cpp
const net::ProxyServer& GetProxyServer() override;
~~~

### GetHrefTranslate

GetHrefTranslate
~~~cpp
const std::string& GetHrefTranslate() override;
~~~

### GetImpression

GetImpression
~~~cpp
const absl::optional<blink::Impression>& GetImpression() override;
~~~

### GetInitiatorFrameToken

GetInitiatorFrameToken
~~~cpp
const absl::optional<blink::LocalFrameToken>& GetInitiatorFrameToken()
      override;
~~~

### GetInitiatorProcessID

GetInitiatorProcessID
~~~cpp
int GetInitiatorProcessID() override;
~~~

### GetInitiatorOrigin

GetInitiatorOrigin
~~~cpp
const absl::optional<url::Origin>& GetInitiatorOrigin() override;
~~~

### GetInitiatorBaseUrl

GetInitiatorBaseUrl
~~~cpp
const absl::optional<GURL>& GetInitiatorBaseUrl() override;
~~~

### GetDnsAliases

GetDnsAliases
~~~cpp
const std::vector<std::string>& GetDnsAliases() override;
~~~

### IsSameProcess

IsSameProcess
~~~cpp
bool IsSameProcess() override;
~~~

### GetNavigationEntry

GetNavigationEntry
~~~cpp
NavigationEntry* GetNavigationEntry() override;
~~~

### GetNavigationEntryOffset

GetNavigationEntryOffset
~~~cpp
int GetNavigationEntryOffset() override;
~~~

### RegisterSubresourceOverride

RegisterSubresourceOverride
~~~cpp
void RegisterSubresourceOverride(
      blink::mojom::TransferrableURLLoaderPtr transferrable_loader) override;
~~~

### GetPreviousRenderFrameHostId

GetPreviousRenderFrameHostId
~~~cpp
GlobalRenderFrameHostId GetPreviousRenderFrameHostId() override;
~~~

### GetExpectedRenderProcessHostId

GetExpectedRenderProcessHostId
~~~cpp
int GetExpectedRenderProcessHostId() override;
~~~

### IsServedFromBackForwardCache

IsServedFromBackForwardCache
~~~cpp
bool IsServedFromBackForwardCache() override;
~~~

### SetIsOverridingUserAgent

SetIsOverridingUserAgent
~~~cpp
void SetIsOverridingUserAgent(bool override_ua) override;
~~~

### SetSilentlyIgnoreErrors

SetSilentlyIgnoreErrors
~~~cpp
void SetSilentlyIgnoreErrors() override;
~~~

### SandboxFlagsInherited

SandboxFlagsInherited
~~~cpp
network::mojom::WebSandboxFlags SandboxFlagsInherited() override;
~~~

### SandboxFlagsToCommit

SandboxFlagsToCommit
~~~cpp
network::mojom::WebSandboxFlags SandboxFlagsToCommit() override;
~~~

### IsWaitingToCommit

IsWaitingToCommit
~~~cpp
bool IsWaitingToCommit() override;
~~~

### WasResourceHintsReceived

WasResourceHintsReceived
~~~cpp
bool WasResourceHintsReceived() override;
~~~

### IsPdf

IsPdf
~~~cpp
bool IsPdf() override;
~~~

### WriteIntoTrace

WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedProto<TraceProto> context) const override;
~~~

### SetNavigationTimeout

SetNavigationTimeout
~~~cpp
bool SetNavigationTimeout(base::TimeDelta timeout) override;
~~~

### SetAllowCookiesFromBrowser

SetAllowCookiesFromBrowser
~~~cpp
void SetAllowCookiesFromBrowser(bool allow_cookies_from_browser) override;
~~~

### GetPrerenderTriggerType

GetPrerenderTriggerType
~~~cpp
PrerenderTriggerType GetPrerenderTriggerType() override;
~~~

### GetPrerenderEmbedderHistogramSuffix

GetPrerenderEmbedderHistogramSuffix
~~~cpp
std::string GetPrerenderEmbedderHistogramSuffix() override;
~~~

### GetJavaNavigationHandle

GetJavaNavigationHandle
~~~cpp
const base::android::JavaRef<jobject>& GetJavaNavigationHandle() override;
~~~

### GetSafeRef

GetSafeRef
~~~cpp
base::SafeRef<NavigationHandle> GetSafeRef() override;
~~~

### ExistingDocumentWasDiscarded

ExistingDocumentWasDiscarded
~~~cpp
bool ExistingDocumentWasDiscarded() const override;
~~~

### RendererCancellationWindowEnded

RendererCancellationWindowEnded
~~~cpp
void RendererCancellationWindowEnded() override;
~~~
 mojom::NavigationRendererCancellationListener implementation
### RegisterCommitDeferringConditionForTesting

RegisterCommitDeferringConditionForTesting
~~~cpp
void RegisterCommitDeferringConditionForTesting(
      std::unique_ptr<CommitDeferringCondition> condition);
~~~

### BeginNavigation

BeginNavigation
~~~cpp
void BeginNavigation();
~~~
 Called on the UI thread by the Navigator to start the navigation.

 The NavigationRequest can be deleted while BeginNavigation() is called.

### common_params

common_params
~~~cpp
const blink::mojom::CommonNavigationParams& common_params() const {
    return *common_params_;
  }
~~~

### begin_params

begin_params
~~~cpp
const blink::mojom::BeginNavigationParams& begin_params() const {
    return *begin_params_;
  }
~~~

### commit_params

commit_params
~~~cpp
const blink::mojom::CommitNavigationParams& commit_params() const {
    return *commit_params_;
  }
~~~

### set_navigation_start_time

set_navigation_start_time
~~~cpp
void set_navigation_start_time(const base::TimeTicks& time) {
    common_params_->navigation_start = time;
  }
~~~
 Updates the navigation start time.

### set_is_cross_site_cross_browsing_context_group

set_is_cross_site_cross_browsing_context_group
~~~cpp
void set_is_cross_site_cross_browsing_context_group(
      bool is_cross_site_cross_browsing_context_group) {
    commit_params_->is_cross_site_cross_browsing_context_group =
        is_cross_site_cross_browsing_context_group;
  }
~~~

### loader_for_testing

loader_for_testing
~~~cpp
NavigationURLLoader* loader_for_testing() const { return loader_.get(); }
~~~

### state

state
~~~cpp
NavigationState state() const { return state_; }
~~~

### frame_tree_node

frame_tree_node
~~~cpp
FrameTreeNode* frame_tree_node() const { return frame_tree_node_; }
~~~

### is_synchronous_renderer_commit

is_synchronous_renderer_commit
~~~cpp
bool is_synchronous_renderer_commit() const {
    return is_synchronous_renderer_commit_;
  }
~~~

### dest_site_instance

dest_site_instance
~~~cpp
SiteInstanceImpl* dest_site_instance() const {
    return dest_site_instance_.get();
  }
~~~

### bindings

bindings
~~~cpp
int bindings() const { return bindings_; }
~~~

### browser_initiated

browser_initiated
~~~cpp
bool browser_initiated() const {
    return commit_params_->is_browser_initiated;
  }
~~~

### from_begin_navigation

from_begin_navigation
~~~cpp
bool from_begin_navigation() const { return from_begin_navigation_; }
~~~

### GetAssociatedRFHType

GetAssociatedRFHType
~~~cpp
AssociatedRenderFrameHostType GetAssociatedRFHType() const;
~~~

### SetAssociatedRFHType

SetAssociatedRFHType
~~~cpp
void SetAssociatedRFHType(AssociatedRenderFrameHostType type);
~~~

### HasRenderFrameHost

HasRenderFrameHost
~~~cpp
bool HasRenderFrameHost() const { return render_frame_host_.has_value(); }
~~~

### set_was_discarded

set_was_discarded
~~~cpp
void set_was_discarded() { commit_params_->was_discarded = true; }
~~~

### set_net_error

set_net_error
~~~cpp
void set_net_error(net::Error net_error) { net_error_ = net_error; }
~~~

### GetMimeType

GetMimeType
~~~cpp
const std::string& GetMimeType() {
    return response_head_ ? response_head_->mime_type : base::EmptyString();
  }
~~~

### response

response
~~~cpp
const network::mojom::URLResponseHead* response() {
    return response_head_.get();
  }
~~~

### response_body

response_body
~~~cpp
const mojo::DataPipeConsumerHandle& response_body() {
    DCHECK_EQ(state_, WILL_PROCESS_RESPONSE);
    return response_body_.get();
  }
~~~

### mutable_response_body_for_testing

mutable_response_body_for_testing
~~~cpp
mojo::ScopedDataPipeConsumerHandle& mutable_response_body_for_testing() {
    return response_body_;
  }
~~~

### SetWaitingForRendererResponse

SetWaitingForRendererResponse
~~~cpp
void SetWaitingForRendererResponse();
~~~

### StartNavigation

StartNavigation
~~~cpp
void StartNavigation();
~~~
 Notifies the NavigatorDelegate the navigation started. This should be
 called after any previous NavigationRequest for the FrameTreeNode has been
 destroyed.

### set_on_start_checks_complete_closure_for_testing

set_on_start_checks_complete_closure_for_testing
~~~cpp
void set_on_start_checks_complete_closure_for_testing(
      base::OnceClosure closure) {
    on_start_checks_complete_closure_ = std::move(closure);
  }
~~~

### UpdateSiteInfo

UpdateSiteInfo
~~~cpp
void UpdateSiteInfo(RenderProcessHost* post_redirect_process);
~~~
 Updates the destination SiteInfo for this navigation. This is called on
 redirects. |post_redirect_process| is the renderer process that should
 handle the navigation following the redirect if it can be handled by an
 existing RenderProcessHost. Otherwise, it should be null.

### nav_entry_id

nav_entry_id
~~~cpp
int nav_entry_id() const { return nav_entry_id_; }
~~~

### devtools_navigation_token

devtools_navigation_token
~~~cpp
const base::UnguessableToken& devtools_navigation_token() const {
    return devtools_navigation_token_;
  }
~~~
 For automation driver-initiated navigations over the devtools protocol,
 |devtools_navigation_token_| is used to tag the navigation. This navigation
 token is then sent into the renderer and lands on the DocumentLoader. That
 way subsequent Blink-level frame lifecycle events can be associated with
 the concrete navigation.

 - The value should not be sent back to the browser.

 - The value on DocumentLoader may be generated in the renderer in some
 cases, and thus shouldn't be trusted.

 TODO(crbug.com/783506): Replace devtools navigation token with the generic
 navigation token that can be passed from renderer to the browser.

### ResetForCrossDocumentRestart

ResetForCrossDocumentRestart
~~~cpp
void ResetForCrossDocumentRestart();
~~~
 Called on same-document navigation requests that need to be restarted as
 cross-document navigations. This happens when a same-document commit fails
 due to another navigation committing in the meantime.

### ResetStateForSiteInstanceChange

ResetStateForSiteInstanceChange
~~~cpp
void ResetStateForSiteInstanceChange();
~~~
 If the navigation redirects cross-process or otherwise is forced to use a
 different SiteInstance than anticipated (e.g., for switching between error
 states), then reset any sensitive state that shouldn't carry over to the
 new process.

### MaybeAddResourceTimingEntryForCancelledNavigation

MaybeAddResourceTimingEntryForCancelledNavigation
~~~cpp
void MaybeAddResourceTimingEntryForCancelledNavigation();
~~~
 If a navigation has been cancelled, and was initiated by the parent
 document, report it with the appropriate ResourceTiming entry information.


 The ResourceTiming entry may not be sent if the current frame
 does not have a parent, or if the navigation was cancelled before
 a request was made.

### AddResourceTimingEntryForFailedSubframeNavigation

AddResourceTimingEntryForFailedSubframeNavigation
~~~cpp
void AddResourceTimingEntryForFailedSubframeNavigation(
      const network::URLLoaderCompletionStatus& status);
~~~
 Adds a resource timing entry to the parent in case of cancelled navigations
 and failed <object> navigations.

### GetCommitNavigationClient

GetCommitNavigationClient
~~~cpp
mojom::NavigationClient* GetCommitNavigationClient();
~~~
 Lazily initializes and returns the mojo::NavigationClient interface used
 for commit.

### set_transition

set_transition
~~~cpp
void set_transition(ui::PageTransition transition) {
    common_params_->transition = transition;
  }
~~~

### set_has_user_gesture

set_has_user_gesture
~~~cpp
void set_has_user_gesture(bool has_user_gesture) {
    common_params_->has_user_gesture = has_user_gesture;
  }
~~~

### IgnoreCommitInterfaceDisconnection

IgnoreCommitInterfaceDisconnection
~~~cpp
void IgnoreCommitInterfaceDisconnection();
~~~
 Ignores any interface disconnect that might happen to the
 navigation_client used to commit.

### Resume

Resume
~~~cpp
void Resume(NavigationThrottle* resuming_throttle);
~~~
 Resume and CancelDeferredNavigation must only be called by the
 NavigationThrottle that is currently deferring the navigation.

 |resuming_throttle| and |cancelling_throttle| are the throttles calling
 these methods.

### CancelDeferredNavigation

CancelDeferredNavigation
~~~cpp
void CancelDeferredNavigation(NavigationThrottle* cancelling_throttle,
                                NavigationThrottle::ThrottleCheckResult result);
~~~

### GetNavigationThrottleRunnerForTesting

GetNavigationThrottleRunnerForTesting
~~~cpp
NavigationThrottleRunner* GetNavigationThrottleRunnerForTesting() {
    return throttle_runner_.get();
  }
~~~
 Returns the underlying NavigationThrottleRunner for tests to manipulate.

### RendererRequestedNavigationCancellationForTesting

RendererRequestedNavigationCancellationForTesting
~~~cpp
void RendererRequestedNavigationCancellationForTesting();
~~~
 Simulates renderer cancelling the navigation.

### GetDeferringThrottleForTesting

GetDeferringThrottleForTesting
~~~cpp
NavigationThrottle* GetDeferringThrottleForTesting() const {
    return throttle_runner_->GetDeferringThrottle();
  }
~~~

### DidCommitNavigation

DidCommitNavigation
~~~cpp
void DidCommitNavigation(const mojom::DidCommitProvisionalLoadParams& params,
                           bool navigation_entry_committed,
                           bool did_replace_entry,
                           const GURL& previous_main_frame_url);
~~~
 Called when the navigation was committed.

 This will update the |state_|.

 |navigation_entry_committed| indicates whether the navigation changed which
 NavigationEntry is current.

 |did_replace_entry| is true if the committed entry has replaced the
 existing one. A non-user initiated redirect causes such replacement.

### navigation_type

navigation_type
~~~cpp
NavigationType navigation_type() const {
    DCHECK(state_ == DID_COMMIT || state_ == DID_COMMIT_ERROR_PAGE);
    return navigation_type_;
  }
~~~

### set_navigation_type

set_navigation_type
~~~cpp
void set_navigation_type(NavigationType navigation_type) {
    navigation_type_ = navigation_type;
  }
~~~

### post_commit_error_page_html

post_commit_error_page_html
~~~cpp
const std::string& post_commit_error_page_html() {
    return post_commit_error_page_html_;
  }
~~~

### set_post_commit_error_page_html

set_post_commit_error_page_html
~~~cpp
void set_post_commit_error_page_html(
      const std::string& post_commit_error_page_html) {
    post_commit_error_page_html_ = post_commit_error_page_html;
  }
~~~

### set_from_download_cross_origin_redirect

set_from_download_cross_origin_redirect
~~~cpp
void set_from_download_cross_origin_redirect(
      bool from_download_cross_origin_redirect) {
    from_download_cross_origin_redirect_ = from_download_cross_origin_redirect;
  }
~~~

### SetNavigationClient

SetNavigationClient
~~~cpp
void SetNavigationClient(
      mojo::PendingAssociatedRemote<mojom::NavigationClient> navigation_client);
~~~
 This should be a private method. The only valid reason to be used
 outside of the class constructor is in the case of an initial history
 navigation in a subframe. This allows a browser-initiated NavigationRequest
 to be canceled by the renderer.

### IsMhtmlOrSubframe

IsMhtmlOrSubframe
~~~cpp
bool IsMhtmlOrSubframe();
~~~
 Whether the navigation loads an MHTML document or a subframe of an MHTML
 document.  The navigation might or might not be fullfilled from the MHTML
 archive (see `is_mhtml_subframe_loaded_from_achive` in the NeedsUrlLoader
 method).  The navigation will commit in the main frame process.

### IsForMhtmlSubframe

IsForMhtmlSubframe
~~~cpp
bool IsForMhtmlSubframe() const;
~~~
 Whether this navigation navigates a subframe of an MHTML document.

### DidEncounterError

DidEncounterError
~~~cpp
virtual bool DidEncounterError() const;
~~~
 Whether the navigation has a non-OK net error code.

 Note that this is different from IsErrorPage(), which only returns true if
 the navigation has finished committing an error page. The net error code
 can be non-OK before commit and also in cases that didn't result in the
 navigation being committed (e.g. canceled navigations).

### set_begin_navigation_callback_for_testing

set_begin_navigation_callback_for_testing
~~~cpp
void set_begin_navigation_callback_for_testing(base::OnceClosure callback) {
    begin_navigation_callback_for_testing_ = std::move(callback);
  }
~~~

### set_complete_callback_for_testing

set_complete_callback_for_testing
~~~cpp
void set_complete_callback_for_testing(
      ThrottleChecksFinishedCallback callback) {
    complete_callback_for_testing_ = std::move(callback);
  }
~~~

### mutable_url_loader_client_endpoints_for_testing

mutable_url_loader_client_endpoints_for_testing
~~~cpp
network::mojom::URLLoaderClientEndpointsPtr&
  mutable_url_loader_client_endpoints_for_testing() {
    return url_loader_client_endpoints_;
  }
~~~

### set_ready_to_commit_callback_for_testing

set_ready_to_commit_callback_for_testing
~~~cpp
void set_ready_to_commit_callback_for_testing(base::OnceClosure callback) {
    ready_to_commit_callback_for_testing_ = std::move(callback);
  }
~~~

### set_renderer_cancellation_window_ended_callback

set_renderer_cancellation_window_ended_callback
~~~cpp
void set_renderer_cancellation_window_ended_callback(
      base::OnceClosure callback) {
    DCHECK(!renderer_cancellation_window_ended());
    renderer_cancellation_window_ended_callback_ = std::move(callback);
  }
~~~

### renderer_cancellation_window_ended

renderer_cancellation_window_ended
~~~cpp
bool renderer_cancellation_window_ended() const {
    return renderer_cancellation_window_ended_;
  }
~~~

### ShouldWaitForRendererCancellationWindowToEnd

ShouldWaitForRendererCancellationWindowToEnd
~~~cpp
bool ShouldWaitForRendererCancellationWindowToEnd();
~~~
 Returns true if this navigation should wait for the renderer-initiated
 navigation cancellation window to end before committing, and returns false
 otherwise. See comment for `renderer_cancellation_listener_` for more
 details.

### SetCommitTimeoutForTesting

SetCommitTimeoutForTesting
~~~cpp
static void SetCommitTimeoutForTesting(const base::TimeDelta& timeout);
~~~
 Sets the READY_TO_COMMIT -> DID_COMMIT timeout. Resets the timeout to the
 default value if |timeout| is zero.

### rfh_restored_from_back_forward_cache

rfh_restored_from_back_forward_cache
~~~cpp
RenderFrameHostImpl* rfh_restored_from_back_forward_cache() {
    return rfh_restored_from_back_forward_cache_.get();
  }
~~~

### GetDelegate

GetDelegate
~~~cpp
NavigatorDelegate* GetDelegate() const;
~~~
 The NavigatorDelegate to notify/query for various navigation events. This
 is always the WebContents.

### request_context_type

request_context_type
~~~cpp
blink::mojom::RequestContextType request_context_type() const {
    return begin_params_->request_context_type;
  }
~~~

### request_destination

request_destination
~~~cpp
network::mojom::RequestDestination request_destination() const {
    return common_params_->request_destination;
  }
~~~

### mixed_content_context_type

mixed_content_context_type
~~~cpp
blink::mojom::MixedContentContextType mixed_content_context_type() const {
    return begin_params_->mixed_content_context_type;
  }
~~~

### IsNavigationStarted

IsNavigationStarted
~~~cpp
bool IsNavigationStarted() const;
~~~
 Returns true if the navigation was started by the Navigator by calling
 BeginNavigation(), or if the request was created at commit time by calling
 CreateForCommit().

### RestartBackForwardCachedNavigation

RestartBackForwardCachedNavigation
~~~cpp
void RestartBackForwardCachedNavigation();
~~~
 Restart the navigation restoring the page from the back-forward cache
 as a regular non-bfcached history navigation.


 The restart itself is asychronous as it's dangerous to restart navigation
 with arbitrary state on the stack (another navigation might be starting,
 so this function only posts the actual task to do all the work (see
 RestartBackForwardCachedNavigationImpl);
### TakePeakGpuMemoryTracker

TakePeakGpuMemoryTracker
~~~cpp
std::unique_ptr<PeakGpuMemoryTracker> TakePeakGpuMemoryTracker();
~~~

### TakeEarlyHintsManager

TakeEarlyHintsManager
~~~cpp
std::unique_ptr<NavigationEarlyHintsManager> TakeEarlyHintsManager();
~~~

### response_should_be_rendered

response_should_be_rendered
~~~cpp
bool response_should_be_rendered() const {
    DCHECK_GE(state_, WILL_PROCESS_RESPONSE);
    return response_should_be_rendered_;
  }
~~~
 Returns true for navigation responses to be rendered in a renderer process.

 This excludes:
  - 204/205 navigation responses.

  - downloads.


 Must not be called before having received the response.

### BuildClientSecurityState

BuildClientSecurityState
~~~cpp
network::mojom::ClientSecurityStatePtr BuildClientSecurityState();
~~~
 Must only be called after ReadyToCommitNavigation().

### ua_change_requires_reload

ua_change_requires_reload
~~~cpp
bool ua_change_requires_reload() const { return ua_change_requires_reload_; }
~~~

### SetRequiredCSP

SetRequiredCSP
~~~cpp
void SetRequiredCSP(network::mojom::ContentSecurityPolicyPtr csp);
~~~

### TakeRequiredCSP

TakeRequiredCSP
~~~cpp
network::mojom::ContentSecurityPolicyPtr TakeRequiredCSP();
~~~

### is_credentialless

is_credentialless
~~~cpp
bool is_credentialless() const { return is_credentialless_; }
~~~

### is_target_fenced_frame_root_originating_from_opaque_url

is_target_fenced_frame_root_originating_from_opaque_url
~~~cpp
bool is_target_fenced_frame_root_originating_from_opaque_url() const {
    return is_target_fenced_frame_root_originating_from_opaque_url_;
  }
~~~

### GetInitiatorPolicyContainerPolicies

GetInitiatorPolicyContainerPolicies
~~~cpp
const PolicyContainerPolicies* GetInitiatorPolicyContainerPolicies() const;
~~~
 Returns a pointer to the policies copied from the navigation initiator.

 Returns nullptr if this navigation had no initiator.

### GetDocumentToken

GetDocumentToken
~~~cpp
const blink::DocumentToken& GetDocumentToken() const;
~~~
 The DocumentToken that should be used for the document created as a result
 of committing this navigation.

 - must only be called for cross-document navigations
 - must not be called before the navigation is ready to commit
### GetPolicyContainerPolicies

GetPolicyContainerPolicies
~~~cpp
const PolicyContainerPolicies& GetPolicyContainerPolicies() const;
~~~
 Returns the policies of the new document being navigated to.


 Must only be called after ReadyToCommitNavigation().

### CreatePolicyContainerForBlink

CreatePolicyContainerForBlink
~~~cpp
blink::mojom::PolicyContainerPtr CreatePolicyContainerForBlink();
~~~
 Creates a new policy container for Blink connected to this navigation's
 PolicyContainerHost.


 Must only be called after ReadyToCommitNavigation().

### TakePolicyContainerHost

TakePolicyContainerHost
~~~cpp
scoped_refptr<PolicyContainerHost> TakePolicyContainerHost();
~~~
 Moves this navigation's PolicyContainerHost out of this instance.


 Must only be called after ReadyToCommitNavigation().

### coep_reporter

coep_reporter
~~~cpp
CrossOriginEmbedderPolicyReporter* coep_reporter() {
    return coep_reporter_.get();
  }
~~~

### TakeCoepReporter

TakeCoepReporter
~~~cpp
std::unique_ptr<CrossOriginEmbedderPolicyReporter> TakeCoepReporter();
~~~

### GetPreviousPageUkmSourceId

GetPreviousPageUkmSourceId
~~~cpp
ukm::SourceId GetPreviousPageUkmSourceId();
~~~
 Returns UKM SourceId for the page we are navigating away from.

 Equal to GetRenderFrameHost()->GetPageUkmSourceId() for subframe
 and same-document navigations and to
 RenderFrameHost::FromID(GetPreviousRenderFrameHostId())
     ->GetPageUkmSourceId() for main-frame cross-document navigations.

### OnServiceWorkerAccessed

OnServiceWorkerAccessed
~~~cpp
void OnServiceWorkerAccessed(const GURL& scope,
                               AllowServiceWorkerResult allowed);
~~~

### TakeCookieObservers

TakeCookieObservers
~~~cpp
[[nodiscard]] std::vector<
      mojo::PendingReceiver<network::mojom::CookieAccessObserver>>
  TakeCookieObservers();
~~~
 Take all cookie observers associated with this navigation.

 Typically this is called when navigation commits to move these observers to
 the committed document.

### TakeTrustTokenObservers

TakeTrustTokenObservers
~~~cpp
[[nodiscard]] std::vector<
      mojo::PendingReceiver<network::mojom::TrustTokenAccessObserver>>
  TakeTrustTokenObservers();
~~~
 Take all Trust Token observers associated with this navigation.

 Typically this is called when navigation commits to move these observers to
 the committed document.

### coop_status

coop_status
~~~cpp
CrossOriginOpenerPolicyStatus& coop_status() { return coop_status_; }
~~~
 Returns the coop status information relevant to the current navigation.

### IsLoadDataWithBaseURL

IsLoadDataWithBaseURL
~~~cpp
bool IsLoadDataWithBaseURL() const;
~~~
 Returns true if this is a NavigationRequest represents a WebView
 loadDataWithBaseUrl navigation.

### GetTentativeOriginAtRequestTime

GetTentativeOriginAtRequestTime
~~~cpp
url::Origin GetTentativeOriginAtRequestTime();
~~~
 Calculates the origin that this NavigationRequest may commit.


 GetTentativeOriginAtRequestTime must be called before the final HTTP
 response is received (unlike GetOriginToCommit), but the returned origin
 may differ from the final origin committed by this navigation (e.g. the
 origin may change because of subsequent redirects, or because of CSP
 headers in the final response; or because no commit may happen at all in
 case of downloads or 204 responses). Prefer to use GetOriginToCommit if
 possible.

### GetOriginToCommit

GetOriginToCommit
~~~cpp
absl::optional<url::Origin> GetOriginToCommit();
~~~
 Will calculate the origin that this NavigationRequest will commit. (This
 should be reasonably accurate, but some browser-vs-renderer inconsistencies
 might still exist - they are currently tracked in
 https://crbug.com/1220238).


 Returns `nullopt` if the navigation will not commit (e.g. in case of
 downloads, or 204 responses).  This may happen if and only if
 `NavigationRequest::GetRenderFrameHost` returns null.


 This method may only be called after a response has been delivered for
 processing, or after the navigation fails with an error page.

### GetOriginToCommitWithDebugInfo

GetOriginToCommitWithDebugInfo
~~~cpp
std::pair<absl::optional<url::Origin>, std::string>
  GetOriginToCommitWithDebugInfo();
~~~
 Same as `GetOriginToCommit()`, except that includes information about how
 the origin gets calculated, to help debug if the browser-side calculated
 origin for this navigation differs from the origin calculated on the
 renderer side.

 TODO(https://crbug.com/1220238): Remove this.

### SetSilentlyIgnoreBlockedByClient

SetSilentlyIgnoreBlockedByClient
~~~cpp
void SetSilentlyIgnoreBlockedByClient() {
    silently_ignore_blocked_by_client_ = true;
  }
~~~
 If this navigation fails with net::ERR_BLOCKED_BY_CLIENT, act as if it were
 cancelled by the user and do not commit an error page.

### GetUrlInfo

GetUrlInfo
~~~cpp
UrlInfo GetUrlInfo();
~~~
 Returns the current url from GetURL() packaged with other state required to
 properly determine SiteInstances and process allocation.

### is_overriding_user_agent

is_overriding_user_agent
~~~cpp
bool is_overriding_user_agent() const {
    return commit_params_->is_overriding_user_agent;
  }
~~~

### isolation_info_for_subresources

isolation_info_for_subresources
~~~cpp
const net::IsolationInfo& isolation_info_for_subresources() const {
    return isolation_info_for_subresources_;
  }
~~~
 Returns the IsolationInfo that should be used to load subresources.

### NeedsUrlLoader

NeedsUrlLoader
~~~cpp
bool NeedsUrlLoader();
~~~
 NeedsUrlLoader() returns true if the navigation needs to use the
 NavigationURLLoader for loading the document.


 A few types of navigations don't make any network requests. They can be
 committed immediately in BeginNavigation(). They self-contain the data
 needed for commit:
 - about:blank: The renderer already knows how to load the empty document.

 - about:srcdoc: The data is stored in the iframe srcdoc attribute.

 - same-document: Only the history and URL are updated, no new document.

 - MHTML subframe: The data is in the archive, owned by the main frame.


 Note #1: Even though "data:" URLs don't generate actual network requests,
 including within MHTML subframes, they are still handled by the network
 stack. The reason is that a few of them can't always be handled otherwise.

 For instance:
  - the ones resulting in downloads.

  - the "invalid" ones. An error page is generated instead.

  - the ones with an unsupported MIME type.

  - the ones targeting the top-level frame on Android.


 Note #2: Even though "javascript:" URL and RendererDebugURL fit very well
 in this category, they don't use the NavigationRequest.


 Note #3: Navigations that do not use a URL loader do not send the usual
 set of callbacks to NavigationThrottle. Instead, they send a single
 separate callback, WillCommitWithoutUrlLoader().

### local_network_request_policy

local_network_request_policy
~~~cpp
network::mojom::LocalNetworkRequestPolicy local_network_request_policy()
      const {
    return local_network_request_policy_;
  }
~~~

### IsWaitingForBeforeUnload

IsWaitingForBeforeUnload
~~~cpp
bool IsWaitingForBeforeUnload();
~~~
 Whether this navigation request waits for the result of beforeunload before
 proceeding.

### GetWebBundleURL

GetWebBundleURL
~~~cpp
GURL GetWebBundleURL();
~~~
 If the response is loaded from a WebBundle, returns the URL of the
 WebBundle. Otherwise, returns an empty URL.

### GetSubresourceWebBundleNavigationInfo

GetSubresourceWebBundleNavigationInfo
~~~cpp
std::unique_ptr<SubresourceWebBundleNavigationInfo>
  GetSubresourceWebBundleNavigationInfo();
~~~
 Creates a SubresourceWebBundleNavigationInfo if the response is loaded from
 a WebBundle.

### GetOriginalRequestURL

GetOriginalRequestURL
~~~cpp
const GURL& GetOriginalRequestURL();
~~~
 Returns the original request url:
 - If this navigation resulted in an error page, this will return the URL
 of the page that failed to load.

 - If this is navigation is triggered by loadDataWithBaseURL or related
 functions, this will return the data URL (or data header, in case of
 loadDataAsStringWithBaseURL).

 - Otherwise, this will return the first URL in |redirect_chain_|. This
 means if the navigation is started due to a client redirect, we will return
 the URL of the page that initiated the client redirect. Otherwise we will
 return the first destination URL for this navigation.

 NOTE: This might result in a different value than original_url in
 |commit_params_|, which is always set to the first destination URL for this
 navigation.

### GetPreviousMainFrameURL

GetPreviousMainFrameURL
~~~cpp
const GURL& GetPreviousMainFrameURL() const;
~~~
 The previous main frame URL. This may be empty if there was no last
 committed entry.

### IsServedFromBackForwardCache

IsServedFromBackForwardCache
~~~cpp
bool IsServedFromBackForwardCache() const;
~~~
 This is the same as |NavigationHandle::IsServedFromBackForwardCache|, but
 adds a const qualifier.

### IsPageActivation

IsPageActivation
~~~cpp
bool IsPageActivation() const override;
~~~
 Whether this navigation is activating an existing page (e.g. served from
 the BackForwardCache or Prerender)
### SetPrerenderActivationNavigationState

SetPrerenderActivationNavigationState
~~~cpp
void SetPrerenderActivationNavigationState(
      std::unique_ptr<NavigationEntryImpl> prerender_navigation_entry,
      const blink::mojom::FrameReplicationState& replication_state);
~~~
 Sets state pertaining to prerender activations. This is only called if
 this navigation is a prerender activation.

### TakePrerenderNavigationEntry

TakePrerenderNavigationEntry
~~~cpp
std::unique_ptr<NavigationEntryImpl> TakePrerenderNavigationEntry();
~~~

### prerender_main_frame_replication_state

prerender_main_frame_replication_state
~~~cpp
const blink::mojom::FrameReplicationState&
  prerender_main_frame_replication_state() {
    return prerender_navigation_state_->prerender_main_frame_replication_state;
  }
~~~
 Returns value that is only valid for prerender activation navigations.

### AddDeferredConsoleMessage

AddDeferredConsoleMessage
~~~cpp
void AddDeferredConsoleMessage(blink::mojom::ConsoleMessageLevel level,
                                 std::string message);
~~~
 Store a console message, which will be sent to the final RenderFrameHost
 immediately after requesting the navigation to commit.


 /!\ WARNING /!\: Beware of not leaking cross-origin information to a
 potentially compromised renderer when using this method.

### is_deferred_on_fenced_frame_url_mapping_for_testing

is_deferred_on_fenced_frame_url_mapping_for_testing
~~~cpp
bool is_deferred_on_fenced_frame_url_mapping_for_testing() const {
    return is_deferred_on_fenced_frame_url_mapping_;
  }
~~~

### GetWeakPtr

GetWeakPtr
~~~cpp
base::WeakPtr<NavigationRequest> GetWeakPtr();
~~~

### is_potentially_prerendered_page_activation_for_testing

is_potentially_prerendered_page_activation_for_testing
~~~cpp
bool is_potentially_prerendered_page_activation_for_testing() const {
    return is_potentially_prerendered_page_activation_for_testing_;
  }
~~~

### prerender_frame_tree_node_id

prerender_frame_tree_node_id
~~~cpp
int prerender_frame_tree_node_id() const {
    DCHECK(prerender_frame_tree_node_id_.has_value())
        << "Must be called after StartNavigation()";
    return prerender_frame_tree_node_id_.value();
  }
~~~

### GetFencedFrameProperties

GetFencedFrameProperties
~~~cpp
const absl::optional<FencedFrameProperties>& GetFencedFrameProperties()
      const {
    return fenced_frame_properties_;
  }
~~~

### ComputeFencedFrameProperties

ComputeFencedFrameProperties
~~~cpp
const absl::optional<FencedFrameProperties>& ComputeFencedFrameProperties()
      const;
~~~
 Compute and return the `FencedFrameProperties` that this
 `NavigationRequest` acts under, i.e. the properties attached to this
 `NavigationRequest` if present, or the properties attached to the fenced
 frame root (if present) otherwise.

### ComputeFencedFrameNonce

ComputeFencedFrameNonce
~~~cpp
const absl::optional<base::UnguessableToken> ComputeFencedFrameNonce() const;
~~~

### ComputeDeprecatedFencedFrameMode

ComputeDeprecatedFencedFrameMode
~~~cpp
const absl::optional<blink::FencedFrame::DeprecatedFencedFrameMode>
  ComputeDeprecatedFencedFrameMode() const;
~~~

### RenderFallbackContentForObjectTag

RenderFallbackContentForObjectTag
~~~cpp
void RenderFallbackContentForObjectTag();
~~~

### TakeWebFeaturesToLog

TakeWebFeaturesToLog
~~~cpp
std::vector<blink::mojom::WebFeature> TakeWebFeaturesToLog();
~~~
 Returns the vector of web features used during the navigation, whose
 recording was delayed until the new document that used them commits.


 Empties this instance's vector.

### set_topics_url_loader_service_bind_context

set_topics_url_loader_service_bind_context
~~~cpp
void set_topics_url_loader_service_bind_context(
      base::WeakPtr<BrowsingTopicsURLLoaderService::BindContext> bind_context) {
    DCHECK(!topics_url_loader_service_bind_context_);
    topics_url_loader_service_bind_context_ = bind_context;
  }
~~~

### set_prerender_trigger_type

set_prerender_trigger_type
~~~cpp
void set_prerender_trigger_type(PrerenderTriggerType type) {
    DCHECK(!prerender_trigger_type_.has_value());
    prerender_trigger_type_ = type;
  }
~~~
 Prerender2:
### set_prerender_embedder_histogram_suffix

set_prerender_embedder_histogram_suffix
~~~cpp
void set_prerender_embedder_histogram_suffix(const std::string& suffix) {
    prerender_embedder_histogram_suffix_ = suffix;
  }
~~~

### set_force_new_browsing_instance

set_force_new_browsing_instance
~~~cpp
void set_force_new_browsing_instance(bool force_new_browsing_instance) {
    force_new_browsing_instance_ = force_new_browsing_instance;
  }
~~~
 Used in tests to indicate this navigation should force a BrowsingInstance
 swap.

### force_new_browsing_instance

force_new_browsing_instance
~~~cpp
bool force_new_browsing_instance() { return force_new_browsing_instance_; }
~~~
 When this returns true, it indicates this navigation should force a
 BrowsingInstance swap. Used only in tests.

### navigation_or_document_handle

navigation_or_document_handle
~~~cpp
const scoped_refptr<NavigationOrDocumentHandle>&
  navigation_or_document_handle() {
    return navigation_or_document_handle_;
  }
~~~

### browser_side_origin_to_commit_with_debug_info

browser_side_origin_to_commit_with_debug_info
~~~cpp
const std::pair<absl::optional<url::Origin>, std::string>&
  browser_side_origin_to_commit_with_debug_info() {
    return browser_side_origin_to_commit_with_debug_info_;
  }
~~~

### SetViewTransitionState

SetViewTransitionState
~~~cpp
void SetViewTransitionState(blink::ViewTransitionState view_transition_state);
~~~
 Initializes state which is passed from the old Document to the new Document
 for a ViewTransition.

### GetMutableRuntimeFeatureStateContext

GetMutableRuntimeFeatureStateContext
~~~cpp
blink::RuntimeFeatureStateContext& GetMutableRuntimeFeatureStateContext();
~~~
 Returns a mutable reference to a blink::RuntimeFeatureStateContext object,
 which exposes the getters and setters for Blink Runtime-Enabled Features to
 the browser process. Any feature set using the RuntimeFeatureStateContext
 before navigation commit will be communicated back to the renderer process.


 This function should not be called once the navigation has been committed.

 NOTE: these feature changes will apply to the "to-be-created" document.

### GetRuntimeFeatureStateContext

GetRuntimeFeatureStateContext
~~~cpp
const blink::RuntimeFeatureStateContext& GetRuntimeFeatureStateContext();
~~~
 Returns a const reference to a blink::RuntimeFeatureStateContext object.

 Once the commit params are sent to the renderer we no longer allow write
 access to the RFSC, but read access is still available.

### browsing_context_group_swap

browsing_context_group_swap
~~~cpp
BrowsingContextGroupSwap browsing_context_group_swap() const {
    return browsing_context_group_swap_;
  }
~~~

### set_pending_navigation_api_key

set_pending_navigation_api_key
~~~cpp
void set_pending_navigation_api_key(absl::optional<std::string> key) {
    pending_navigation_api_key_ = key;
  }
~~~
 If the navigation fails before commit and |pending_navigation_api_key_| has
 been set, then the renderer will be notified of the pre-commit failure and
 provide |pending_navigation_api_key_| so that the Navigation API can fire
 events and reject promises.

 This should only be set if this request's frame initiated the navigation,
 because only the initiating frame has outstanding promises to reject.

### GetNavigationTokenForDeferringSubframes

GetNavigationTokenForDeferringSubframes
~~~cpp
absl::optional<base::UnguessableToken>
  GetNavigationTokenForDeferringSubframes();
~~~
 Returns the navigation token for this request if this NavigationRequest
 is for a history navigation, and it might be cancelled via the navigate
 event. In that case, any associated subframe history navigations will be
 deferred until this navigation commits.

 This may only be called if this is a main-frame same-document navigation.

 Will return a valid token if canceling traversals via the navigate event is
 enabled, this is a history navigation, a navigate event is registered in
 the renderer, and the frame has met the user activation requirements to be
 allowed to cancel the navigation.

 This token will then be provided to the subframe NavigationRequests via
 set_main_frame_same_document_history_token().

### set_main_frame_same_document_history_token

set_main_frame_same_document_history_token
~~~cpp
void set_main_frame_same_document_history_token(
      absl::optional<base::UnguessableToken> token) {
    main_frame_same_document_navigation_token_ = token;
  }
~~~
 For subframe NavigationRequests, these set and return the main frame's
 NavigationRequest token, in the case that the main frame returns it from
 GetNavigationTokenForDeferringSubframes().

### main_frame_same_document_history_token

main_frame_same_document_history_token
~~~cpp
absl::optional<base::UnguessableToken>
  main_frame_same_document_history_token() {
    return main_frame_same_document_navigation_token_;
  }
~~~

### AddDeferredSubframeNavigationThrottle

AddDeferredSubframeNavigationThrottle
~~~cpp
void AddDeferredSubframeNavigationThrottle(
      base::WeakPtr<SubframeHistoryNavigationThrottle> throttle);
~~~
 When a SubframeHistoryNavigationThrottle is created, it registers itself
 with the NavigationRequest for the main frame. If the main frame
 NavigationRequest commits, then these throttles will be resumed in
 UnblockPendingSubframeNavigationRequestsIfNeeded().

 If it is canceled instead, these throttles will all be canceled.

 Takes a WeakPtr because `throttle` is owned by another NavigationRequest,
 and that request may be canceled outside of our control, in which case
 `throttle` will be destroyed.

### TakeCookieChangeListener

TakeCookieChangeListener
~~~cpp
std::unique_ptr<RenderFrameHostImpl::CookieChangeListener>
  TakeCookieChangeListener() {
    return std::move(cookie_change_listener_);
  }
~~~

### GetIsThirdPartyCookiesUserBypassEnabled

GetIsThirdPartyCookiesUserBypassEnabled
~~~cpp
bool GetIsThirdPartyCookiesUserBypassEnabled();
~~~
 When this returns true, the user has specified that third-party cookies
 should remain enabled for this navigation.

 It does so by checking the Blink runtime-enabled feature (BREF) for
 third-party cookie deprecation user bypass. This pulls the BREF from a
 pending navigation request context; for a committed frame use
 RenderFrameHostImpl::GetIsThirdPartyCookiesUserBypassEnabled.

 For a subframe, the BREF is *not* pulled from the pending navigation
 request and is instead pulled from the committed context on the main frame.

### set_resume_commit_closure_for_test

set_resume_commit_closure_for_test
~~~cpp
void set_resume_commit_closure_for_test(base::OnceClosure closure) {
    resume_commit_closure_ = std::move(closure);
  }
~~~

### NavigationRequest

NavigationRequest
~~~cpp
NavigationRequest(
      FrameTreeNode* frame_tree_node,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::BeginNavigationParamsPtr begin_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool browser_initiated,
      bool from_begin_navigation,
      bool is_synchronous_renderer_commit,
      const FrameNavigationEntry* frame_navigation_entry,
      NavigationEntryImpl* navitation_entry,
      std::unique_ptr<NavigationUIData> navigation_ui_data,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      mojo::PendingAssociatedRemote<mojom::NavigationClient> navigation_client,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache,
      base::WeakPtr<RenderFrameHostImpl> rfh_restored_from_back_forward_cache,
      int initiator_process_id,
      bool was_opener_suppressed,
      bool is_pdf,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      mojo::PendingReceiver<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener = mojo::NullReceiver(),
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt)
~~~

### MaybeStartPrerenderingActivationChecks

MaybeStartPrerenderingActivationChecks
~~~cpp
bool MaybeStartPrerenderingActivationChecks();
~~~
 Checks if this navigation may activate a prerendered page. If it's
 possible, schedules to start running CommitDeferringConditions for
 prerendered page activation and returns true.

### OnPrerenderingActivationChecksComplete

OnPrerenderingActivationChecksComplete
~~~cpp
void OnPrerenderingActivationChecksComplete(
      CommitDeferringCondition::NavigationType navigation_type,
      absl::optional<int> candidate_prerender_frame_tree_node_id);
~~~
 Called from OnCommitDeferringConditionChecksComplete() if this request is
 activating a prerendered page.

### GetFencedFrameURLMap

GetFencedFrameURLMap
~~~cpp
FencedFrameURLMapping& GetFencedFrameURLMap();
~~~
 Get the `FencedFrameURLMapping` associated with the current page.

### NeedFencedFrameURLMapping

NeedFencedFrameURLMapping
~~~cpp
bool NeedFencedFrameURLMapping();
~~~
 True if this is a fenced frame navigation to an urn:uuid.

### OnFencedFrameURLMappingComplete

OnFencedFrameURLMappingComplete
~~~cpp
void OnFencedFrameURLMappingComplete(
      const absl::optional<FencedFrameProperties>& properties) override;
~~~
 FencedFrameURLMapping::MappingResultObserver implementation.

 Called from `FencedFrameURLMapping` when the mapping decision is made, and
 resume the deferred navigation.

### BeginNavigationImpl

BeginNavigationImpl
~~~cpp
void BeginNavigationImpl();
~~~
 Called from BeginNavigation(), OnPrerenderingActivationChecksComplete(),
 or OnFencedFrameURLMappingComplete().

### CheckForIsolationOptIn

CheckForIsolationOptIn
~~~cpp
void CheckForIsolationOptIn(const GURL& url);
~~~
 Checks if the response requests an isolated origin via the
 Origin-Agent-Cluster header, and if so opts in the origin to be isolated.

### AddSameProcessOriginAgentClusterStateIfNecessary

AddSameProcessOriginAgentClusterStateIfNecessary
~~~cpp
void AddSameProcessOriginAgentClusterStateIfNecessary(
      const IsolationContext& isolation_context,
      const GURL& url);
~~~
 Use to manually set Origin-keyed Agent Cluster (OAC) isolation state in
 the event that process-isolation isn't being used for OAC, or
 OAC-by-default means that we're tracking explicit opt-out requests (which
 by definition are same-process).

 TODO(wjmaclean): When we switch to using separate SiteInstances even for
 same-process OAC, then this function can be removed.

### IsOriginAgentClusterOptInRequested

IsOriginAgentClusterOptInRequested
~~~cpp
bool IsOriginAgentClusterOptInRequested();
~~~
 Returns whether this navigation request is requesting opt-in
 origin-isolation.

### IsOriginAgentClusterOptOutRequested

IsOriginAgentClusterOptOutRequested
~~~cpp
bool IsOriginAgentClusterOptOutRequested();
~~~
 Returns whether this navigation request is requesting opt-out from
 origin-isolation. Always returns false if
 AreOriginAgentClustersEnabledByDefault() is false.

### IsIsolationImplied

IsIsolationImplied
~~~cpp
bool IsIsolationImplied();
~~~
 Returns whether this navigation request should use an origin-keyed
 agent cluster (but not an origin-keyed process).

### DetermineOriginAgentClusterEndResult

DetermineOriginAgentClusterEndResult
~~~cpp
void DetermineOriginAgentClusterEndResult();
~~~
 The Origin-Agent-Cluster end result is determined early in the lifecycle of
 a NavigationRequest, but used late. In particular, we want to trigger use
 counters and console warnings once navigation has committed.

### ProcessOriginAgentClusterEndResult

ProcessOriginAgentClusterEndResult
~~~cpp
void ProcessOriginAgentClusterEndResult();
~~~

### PopulateDocumentTokenForCrossDocumentNavigation

PopulateDocumentTokenForCrossDocumentNavigation
~~~cpp
void PopulateDocumentTokenForCrossDocumentNavigation();
~~~

### OnRequestRedirected

OnRequestRedirected
~~~cpp
void OnRequestRedirected(
      const net::RedirectInfo& redirect_info,
      const net::NetworkAnonymizationKey& network_anonymization_key,
      network::mojom::URLResponseHeadPtr response_head) override;
~~~
 NavigationURLLoaderDelegate implementation.

### OnResponseStarted

OnResponseStarted
~~~cpp
void OnResponseStarted(
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      GlobalRequestID request_id,
      bool is_download,
      net::NetworkAnonymizationKey network_anonymization_key,
      absl::optional<SubresourceLoaderParams> subresource_loader_params,
      EarlyHints early_hints) override;
~~~

### OnRequestFailed

OnRequestFailed
~~~cpp
void OnRequestFailed(
      const network::URLLoaderCompletionStatus& status) override;
~~~

### CreateNavigationEarlyHintsManagerParams

CreateNavigationEarlyHintsManagerParams
~~~cpp
absl::optional<NavigationEarlyHintsManagerParams>
  CreateNavigationEarlyHintsManagerParams(
      const network::mojom::EarlyHints& early_hints) override;
~~~

### SelectFrameHostForOnResponseStarted

SelectFrameHostForOnResponseStarted
~~~cpp
void SelectFrameHostForOnResponseStarted(
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      bool is_download,
      absl::optional<SubresourceLoaderParams> subresource_loader_params);
~~~
 Selecting a `RenderFrameHost` to commit a navigation may occasionally fail.

 When this happens, the navigation will bind a closure to continue the
 navigation and assign it to `resume_commit_closure_`. This closure may run
 even when it is still not possible to proceed; see the comment on the
 `resume_commit_closure_` field for the full details.

 Corresponds to navigations committing from `OnResponseStarted()`:
### OnRequestFailedInternal

OnRequestFailedInternal
~~~cpp
void OnRequestFailedInternal(
      const network::URLLoaderCompletionStatus& status,
      bool skip_throttles,
      const absl::optional<std::string>& error_page_content,
      bool collapse_frame);
~~~
 TODO(https://crbug.com/1220337): Implement this logic for
 OnRequestFailedInternal() and BeginNavigationImpl() as well.

 To be called whenever a navigation request fails. If |skip_throttles| is
 true, the registered NavigationThrottle(s) won't get a chance to intercept
 NavigationThrottle::WillFailRequest. It should be used when a request
 failed due to a throttle result itself. |error_page_content| is only used
 when |skip_throttles| is true. If |collapse_frame| is true, the associated
 frame tree node is collapsed.

### ComputeErrorPageProcess

ComputeErrorPageProcess
~~~cpp
ErrorPageProcess ComputeErrorPageProcess(int net_error);
~~~

### OnStartChecksComplete

OnStartChecksComplete
~~~cpp
void OnStartChecksComplete(NavigationThrottle::ThrottleCheckResult result);
~~~
 Called when the NavigationThrottles have been checked by the
 NavigationHandle.

### OnRedirectChecksComplete

OnRedirectChecksComplete
~~~cpp
void OnRedirectChecksComplete(NavigationThrottle::ThrottleCheckResult result);
~~~

### OnFailureChecksComplete

OnFailureChecksComplete
~~~cpp
void OnFailureChecksComplete(NavigationThrottle::ThrottleCheckResult result);
~~~

### OnWillProcessResponseChecksComplete

OnWillProcessResponseChecksComplete
~~~cpp
void OnWillProcessResponseChecksComplete(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### OnWillCommitWithoutUrlLoaderChecksComplete

OnWillCommitWithoutUrlLoaderChecksComplete
~~~cpp
void OnWillCommitWithoutUrlLoaderChecksComplete(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### RunCommitDeferringConditions

RunCommitDeferringConditions
~~~cpp
void RunCommitDeferringConditions();
~~~
 Runs CommitDeferringConditions.


 For prerendered page activation, this is called at the beginning of the
 navigation (i.e., in BeginNavigation()). This is because activating a
 prerendered page must be an atomic, synchronous operation so there is no
 chance for the prerender to be cancelled during the operation. The
 CommitDeferringConditions are asynchronous, so they run at the beginning
 of navigation. Once they finish, the atomic activation sequence runs.

### OnCommitDeferringConditionChecksComplete

OnCommitDeferringConditionChecksComplete
~~~cpp
void OnCommitDeferringConditionChecksComplete(
      CommitDeferringCondition::NavigationType navigation_type,
      absl::optional<int> candidate_prerender_frame_tree_node_id) override;
~~~
 Similar to the NavigationThrottle checks above but this is called from
 CommitDeferringConditionRunner rather than NavigationThrottles and is
 invoked after all throttle checks and commit checks have completed and the
 navigation can proceed to commit.

### CommitErrorPage

CommitErrorPage
~~~cpp
void CommitErrorPage(const absl::optional<std::string>& error_page_content);
~~~
 Called either by OnFailureChecksComplete() or OnRequestFailed() directly.

 |error_page_content| contains the content of the error page (i.e. flattened
 HTML, JS, CSS).

### CommitNavigation

CommitNavigation
~~~cpp
void CommitNavigation();
~~~
 Have a RenderFrameHost commit the navigation. The NavigationRequest will
 be destroyed sometime after this call, typically after the renderer has
 informed the browser process that the commit has finished.

### CommitPageActivation

CommitPageActivation
~~~cpp
void CommitPageActivation();
~~~
 Commits the navigation to an existing page (back-forward cache navigation
 or prerender activation). NavigationRequest will be destroyed after this
 call.

### IsAllowedByCSPDirective

IsAllowedByCSPDirective
~~~cpp
bool IsAllowedByCSPDirective(
      const std::vector<network::mojom::ContentSecurityPolicyPtr>& policies,
      network::CSPContext* context,
      network::mojom::CSPDirectiveName directive,
      bool has_followed_redirect,
      bool url_upgraded_after_redirect,
      bool is_response_check,
      bool is_opaque_fenced_frame,
      network::CSPContext::CheckCSPDisposition disposition);
~~~
 Checks if the specified CSP context's relevant CSP directive
 allows the navigation. This is called to perform the frame-src
 and navigate-to checks.

### CheckCSPDirectives

CheckCSPDirectives
~~~cpp
net::Error CheckCSPDirectives(
      RenderFrameHostCSPContext parent_context,
      const PolicyContainerPolicies* parent_policies,
      RenderFrameHostCSPContext initiator_context,
      const PolicyContainerPolicies* initiator_policies,
      bool has_followed_redirect,
      bool url_upgraded_after_redirect,
      bool is_response_check,
      network::CSPContext::CheckCSPDisposition disposition);
~~~
 Checks if CSP allows the navigation. This will check the frame-src,
 fenced-frame-src and navigate-to directives. Returns net::OK if the checks
 pass, and net::ERR_ABORTED or net::ERR_BLOCKED_BY_CSP depending on which
 checks fail.

### CheckContentSecurityPolicy

CheckContentSecurityPolicy
~~~cpp
net::Error CheckContentSecurityPolicy(bool has_followed_redirect,
                                        bool url_upgraded_after_redirect,
                                        bool is_response_check);
~~~
 Check whether a request should be allowed to continue or should be blocked
 because it violates a CSP. This method can have two side effects:
 - If a CSP is configured to send reports and the request violates the CSP,
   a report will be sent.

 - The navigation request may be upgraded from HTTP to HTTPS if a CSP is
   configured to upgrade insecure requests.

### MakeDidCommitProvisionalLoadParamsForBFCacheRestore

MakeDidCommitProvisionalLoadParamsForBFCacheRestore
~~~cpp
mojom::DidCommitProvisionalLoadParamsPtr
  MakeDidCommitProvisionalLoadParamsForBFCacheRestore();
~~~
 Builds the parameters used to commit a navigation to a page that was
 restored from the back-forward cache.

### MakeDidCommitProvisionalLoadParamsForPrerenderActivation

MakeDidCommitProvisionalLoadParamsForPrerenderActivation
~~~cpp
mojom::DidCommitProvisionalLoadParamsPtr
  MakeDidCommitProvisionalLoadParamsForPrerenderActivation();
~~~
 Builds the parameters used to commit a navigation to a prerendered page
 that was activated.

### MakeDidCommitProvisionalLoadParamsForActivation

MakeDidCommitProvisionalLoadParamsForActivation
~~~cpp
mojom::DidCommitProvisionalLoadParamsPtr
  MakeDidCommitProvisionalLoadParamsForActivation();
~~~
 Builds generic activation parameters used to commit a navigation to a page.

### CheckCredentialedSubresource

CheckCredentialedSubresource
~~~cpp
CredentialedSubresourceCheckResult CheckCredentialedSubresource() const;
~~~
 Chrome blocks subresource requests whose URLs contain embedded credentials
 (e.g. `https://user:pass@example.com/page.html`). Check whether the
 request should be allowed to continue or should be blocked.

### CheckAboutSrcDoc

CheckAboutSrcDoc
~~~cpp
AboutSrcDocCheckResult CheckAboutSrcDoc() const;
~~~

### SetupCSPEmbeddedEnforcement

SetupCSPEmbeddedEnforcement
~~~cpp
void SetupCSPEmbeddedEnforcement();
~~~
 When the embedder requires the use of Content Security Policy via Embedded
 Enforcement, framed documents must either:
 1. Use the 'allow-csp-from' header to opt-into enforcement.

 2. Enforce its own CSP that subsumes the required CSP.

 Framed documents that fail to do either of these will be blocked.


 See:
 - https://w3c.github.io/webappsec-cspee/#required-csp-header
 - https://w3c.github.io/webappsec-cspee/#allow-csp-from-header

 SetupCSPEmbeddedEnforcement() retrieve the iframe 'csp' attribute applying.

 CheckCSPEmbeddedEnforcement() inspects the response headers. It decides if
 the 'csp' attribute should be installed into the child. This might also
 block it and display an error page instead.

### CheckCSPEmbeddedEnforcement

CheckCSPEmbeddedEnforcement
~~~cpp
CSPEmbeddedEnforcementResult CheckCSPEmbeddedEnforcement();
~~~

### UpdateCommitNavigationParamsHistory

UpdateCommitNavigationParamsHistory
~~~cpp
void UpdateCommitNavigationParamsHistory();
~~~
 Called before a commit. Updates the history index and length held in
 CommitNavigationParams. This is used to update this shared state with the
 renderer process.

### OnNavigationClientDisconnected

OnNavigationClientDisconnected
~~~cpp
void OnNavigationClientDisconnected(uint32_t reason,
                                      const std::string& description);
~~~
 The disconnect handler for the NavigationClient Mojo interface; used as a
 signal to potentially cancel navigations, e.g. when the renderer replaces
 an existing NavigationClient connection with a new one or when the renderer
 process crashes.

### HandleInterfaceDisconnection

HandleInterfaceDisconnection
~~~cpp
void HandleInterfaceDisconnection(
      mojo::AssociatedRemote<mojom::NavigationClient>&);
~~~
 Binds the given error_handler to be called when an interface disconnection
 happens on the renderer side.

### IgnoreInterfaceDisconnection

IgnoreInterfaceDisconnection
~~~cpp
void IgnoreInterfaceDisconnection();
~~~
 When called, this NavigationRequest will no longer interpret the interface
 disconnection on the renderer side as an AbortNavigation.

 TODO(ahemery): remove this function when NavigationRequest properly handles
 interface disconnection in all cases.

### SetExpectedProcess

SetExpectedProcess
~~~cpp
void SetExpectedProcess(RenderProcessHost* expected_process);
~~~
 Sets ID of the RenderProcessHost we expect the navigation to commit in.

 This is used to inform the RenderProcessHost to expect a navigation to the
 url we're navigating to.

### ResetExpectedProcess

ResetExpectedProcess
~~~cpp
void ResetExpectedProcess();
~~~
 Inform the RenderProcessHost to no longer expect a navigation.

### AddOldPageInfoToCommitParamsIfNeeded

AddOldPageInfoToCommitParamsIfNeeded
~~~cpp
void AddOldPageInfoToCommitParamsIfNeeded();
~~~
 If this is a same-site main-frame navigation where we did a proactive
 BrowsingInstance swap but we're reusing the old page's process, we need
 to send the routing ID and the updated lifecycle state of the old page so
 that we can run pagehide and visibilitychange handlers of the old page
 when we commit the new page.

### RecordDownloadUseCountersPrePolicyCheck

RecordDownloadUseCountersPrePolicyCheck
~~~cpp
void RecordDownloadUseCountersPrePolicyCheck();
~~~
 Record download related UseCounters when navigation is a download before
 filtered by download_policy.

### RecordDownloadUseCountersPostPolicyCheck

RecordDownloadUseCountersPostPolicyCheck
~~~cpp
void RecordDownloadUseCountersPostPolicyCheck();
~~~
 Record download related UseCounters when navigation is a download after
 filtered by download_policy.

### OnNavigationEventProcessed

OnNavigationEventProcessed
~~~cpp
void OnNavigationEventProcessed(
      NavigationThrottleRunner::Event event,
      NavigationThrottle::ThrottleCheckResult result) override;
~~~
 NavigationThrottleRunner::Delegate:
### OnWillStartRequestProcessed

OnWillStartRequestProcessed
~~~cpp
void OnWillStartRequestProcessed(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### OnWillRedirectRequestProcessed

OnWillRedirectRequestProcessed
~~~cpp
void OnWillRedirectRequestProcessed(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### OnWillFailRequestProcessed

OnWillFailRequestProcessed
~~~cpp
void OnWillFailRequestProcessed(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### OnWillProcessResponseProcessed

OnWillProcessResponseProcessed
~~~cpp
void OnWillProcessResponseProcessed(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### OnWillCommitWithoutUrlLoaderProcessed

OnWillCommitWithoutUrlLoaderProcessed
~~~cpp
void OnWillCommitWithoutUrlLoaderProcessed(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### CancelDeferredNavigationInternal

CancelDeferredNavigationInternal
~~~cpp
void CancelDeferredNavigationInternal(
      NavigationThrottle::ThrottleCheckResult result);
~~~

### WillStartRequest

WillStartRequest
~~~cpp
void WillStartRequest();
~~~
 TODO(zetamoo): Remove the Will* methods and fold them into their callers.

 Called when the URLRequest will start in the network stack.

### WillRedirectRequest

WillRedirectRequest
~~~cpp
void WillRedirectRequest(const GURL& new_referrer_url,
                           RenderProcessHost* post_redirect_process);
~~~
 Called when the URLRequest will be redirected in the network stack.

 This will also inform the delegate that the request was redirected.


 |post_redirect_process| is the renderer process we expect to use to commit
 the navigation now that it has been redirected. It can be null if there is
 no live process that can be used. In that case, a suitable renderer process
 will be created at commit time.


### WillFailRequest

WillFailRequest
~~~cpp
void WillFailRequest();
~~~
 Called when the URLRequest will fail.

### WillProcessResponse

WillProcessResponse
~~~cpp
void WillProcessResponse();
~~~
 Called when the URLRequest has delivered response headers and metadata.

 |callback| will be called when all throttle checks have completed,
 allowing the caller to cancel the navigation or let it proceed.

 NavigationHandle will not call |callback| with a result of DEFER.

 If the result is PROCEED, then 'ReadyToCommitNavigation' will be called
 just before calling |callback|.

### WillCommitWithoutUrlLoader

WillCommitWithoutUrlLoader
~~~cpp
void WillCommitWithoutUrlLoader();
~~~
 Called when no URLRequest will be needed to perform this navigation, just
 before commit.

### IsSelfReferentialURL

IsSelfReferentialURL
~~~cpp
bool IsSelfReferentialURL();
~~~
 Checks for attempts to navigate to a page that is already referenced more
 than once in the frame's ancestors.  This is a helper function used by
 WillStartRequest and WillRedirectRequest to prevent the navigation.

### RenderProcessExited

RenderProcessExited
~~~cpp
void RenderProcessExited(RenderProcessHost* host,
                           const ChildProcessTerminationInfo& info) override;
~~~
 RenderProcessHostObserver implementation.

### RenderProcessHostDestroyed

RenderProcessHostDestroyed
~~~cpp
void RenderProcessHostDestroyed(RenderProcessHost* host) override;
~~~

### UpdateNavigationHandleTimingsOnResponseReceived

UpdateNavigationHandleTimingsOnResponseReceived
~~~cpp
void UpdateNavigationHandleTimingsOnResponseReceived(bool is_first_response);
~~~
 Updates navigation handle timings.

### UpdateNavigationHandleTimingsOnCommitSent

UpdateNavigationHandleTimingsOnCommitSent
~~~cpp
void UpdateNavigationHandleTimingsOnCommitSent();
~~~

### GetSiteInfoForCommonParamsURL

GetSiteInfoForCommonParamsURL
~~~cpp
SiteInfo GetSiteInfoForCommonParamsURL();
~~~
 Helper function that computes the SiteInfo for |common_params_.url|.

 Note: |site_info_| should only be updated with the result of this function.

### UpdateStateFollowingRedirect

UpdateStateFollowingRedirect
~~~cpp
void UpdateStateFollowingRedirect(const GURL& new_referrer_url);
~~~
 Updates the state of the navigation handle after encountering a server
 redirect.

### UpdateLocalNetworkRequestPolicy

UpdateLocalNetworkRequestPolicy
~~~cpp
void UpdateLocalNetworkRequestPolicy();
~~~
 Updates |local_network_request_policy_| for ReadyToCommitNavigation().


 Must not be called for same-document navigation requests nor for requests
 served from the back-forward cache or from prerendered pages.

### ReadyToCommitNavigation

ReadyToCommitNavigation
~~~cpp
void ReadyToCommitNavigation(bool is_error);
~~~
 Called when the navigation is ready to be committed. This will update the
 |state_| and inform the delegate.

### OnCommitTimeout

OnCommitTimeout
~~~cpp
void OnCommitTimeout();
~~~
 Called if READY_TO_COMMIT -> COMMIT state transition takes an unusually
 long time.

### RenderProcessBlockedStateChanged

RenderProcessBlockedStateChanged
~~~cpp
void RenderProcessBlockedStateChanged(bool blocked);
~~~
 Called by the RenderProcessHost to handle the case when the process changed
 its state of being blocked.

### StopCommitTimeout

StopCommitTimeout
~~~cpp
void StopCommitTimeout();
~~~

### RestartCommitTimeout

RestartCommitTimeout
~~~cpp
void RestartCommitTimeout();
~~~

### TakeRemovedRequestHeaders

TakeRemovedRequestHeaders
~~~cpp
std::vector<std::string> TakeRemovedRequestHeaders() {
    return std::move(removed_request_headers_);
  }
~~~

### TakeModifiedRequestHeaders

TakeModifiedRequestHeaders
~~~cpp
net::HttpRequestHeaders TakeModifiedRequestHeaders() {
    return std::move(modified_request_headers_);
  }
~~~

### RequiresInitiatorBasedSourceSiteInstance

RequiresInitiatorBasedSourceSiteInstance
~~~cpp
bool RequiresInitiatorBasedSourceSiteInstance() const;
~~~
 Returns true if the contents of |common_params_| requires
 |source_site_instance_| to be set. This is used to ensure that data: and
 about:blank URLs with valid initiator origins always have
 |source_site_instance_| set so that site isolation enforcements work
 properly.

### SetSourceSiteInstanceToInitiatorIfNeeded

SetSourceSiteInstanceToInitiatorIfNeeded
~~~cpp
void SetSourceSiteInstanceToInitiatorIfNeeded();
~~~
 Sets |source_site_instance_| to a SiteInstance that is derived from
 |common_params_->initiator_origin| and related to the |frame_tree_node_|'s
 current SiteInstance. |source_site_instance_| is only set if it doesn't
 already have a value and RequiresInitiatorBasedSourceSiteInstance() returns
 true.

### RestartBackForwardCachedNavigationImpl

RestartBackForwardCachedNavigationImpl
~~~cpp
void RestartBackForwardCachedNavigationImpl();
~~~
 See RestartBackForwardCachedNavigation.

### ForceEnableOriginTrials

ForceEnableOriginTrials
~~~cpp
void ForceEnableOriginTrials(const std::vector<std::string>& trials) override;
~~~

### CreateCoepReporter

CreateCoepReporter
~~~cpp
void CreateCoepReporter(StoragePartition* storage_partition);
~~~

### ComputeCrossOriginEmbedderPolicy

ComputeCrossOriginEmbedderPolicy
~~~cpp
network::CrossOriginEmbedderPolicy ComputeCrossOriginEmbedderPolicy();
~~~
 [spec]: https://html.spec.whatwg.org/C/#obtain-an-embedder-policy

 Returns the CrossOriginEmbedderPolicy for the document, which is inherited
 or retrieved from response headers.

### CheckResponseAdherenceToCoep

CheckResponseAdherenceToCoep
~~~cpp
bool CheckResponseAdherenceToCoep(const GURL& url);
~~~
 [spec]:
 https://html.spec.whatwg.org/C/#check-a-navigation-response's-adherence-to-its-embedder-policy

 Return whether the response's COEP is compatible with its parent's COEP. It
 also sends COEP reports if needed.

### EnforceCOEP

EnforceCOEP
~~~cpp
absl::optional<network::mojom::BlockedByResponseReason> EnforceCOEP();
~~~

### CoopCoepSanityCheck

CoopCoepSanityCheck
~~~cpp
bool CoopCoepSanityCheck();
~~~
 Check the COOP value of the page is compatible with the COEP value of each
 of its documents. COOP:kSameOriginPlusCoep is incompatible with COEP:kNone.

 If they aren't, this returns false and emits a crash report.

### CheckPermissionsPoliciesForFencedFrames

CheckPermissionsPoliciesForFencedFrames
~~~cpp
bool CheckPermissionsPoliciesForFencedFrames(const url::Origin&);
~~~
 Checks that, given an origin to be committed, all of the permissions
 policies that a fenced frame requires to be enabled are enabled. If not, it
 logs a console message and returns false.

### IsFencedFrameRequiredPolicyFeatureAllowed

IsFencedFrameRequiredPolicyFeatureAllowed
~~~cpp
bool IsFencedFrameRequiredPolicyFeatureAllowed(
      const url::Origin&,
      const blink::mojom::PermissionsPolicyFeature feature);
~~~
 Helper function that determines if a given required permissions policy
 feature is properly enabled for a given origin to be committed.

### GetUserAgentOverride

GetUserAgentOverride
~~~cpp
std::string GetUserAgentOverride();
~~~
 Returns the user-agent override, or an empty string if one isn't set.

### CreateCookieAccessObserver

CreateCookieAccessObserver
~~~cpp
mojo::PendingRemote<network::mojom::CookieAccessObserver>
  CreateCookieAccessObserver();
~~~

### OnCookiesAccessed

OnCookiesAccessed
~~~cpp
void OnCookiesAccessed(std::vector<network::mojom::CookieAccessDetailsPtr>
                             details_vector) override;
~~~
 network::mojom::CookieAccessObserver:
### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::CookieAccessObserver>
                 observer) override;
~~~

### CreateTrustTokenAccessObserver

CreateTrustTokenAccessObserver
~~~cpp
mojo::PendingRemote<network::mojom::TrustTokenAccessObserver>
  CreateTrustTokenAccessObserver();
~~~

### OnTrustTokensAccessed

OnTrustTokensAccessed
~~~cpp
void OnTrustTokensAccessed(
      network::mojom::TrustTokenAccessDetailsPtr details) override;
~~~
 network::mojom::TrustTokenAccessObserver:
### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::TrustTokenAccessObserver>
                 observer) override;
~~~

### GetNavigationController

GetNavigationController
~~~cpp
NavigationControllerImpl* GetNavigationController();
~~~
 Convenience function to return the NavigationControllerImpl this
 NavigationRequest is in.

### GetPrerenderHostRegistry

GetPrerenderHostRegistry
~~~cpp
PrerenderHostRegistry& GetPrerenderHostRegistry();
~~~
 Convenience function to return the PrerenderHostRegistry this
 NavigationRequest can be associated with.

### GetInitiatorDocumentRenderFrameHost

GetInitiatorDocumentRenderFrameHost
~~~cpp
RenderFrameHostImpl* GetInitiatorDocumentRenderFrameHost();
~~~
 Returns the RenderFrameHost of the initiator document, iff there is such
 a document and its RenderFrameHost has not committed a different document
 since this navigation started. Otherwise returns nullptr.

### RecordAddressSpaceFeature

RecordAddressSpaceFeature
~~~cpp
void RecordAddressSpaceFeature();
~~~
 Records the appropriate kAddressSpace* WebFeature for the response we just
 received on the initiator document, if possible.

### ComputePoliciesToCommit

ComputePoliciesToCommit
~~~cpp
void ComputePoliciesToCommit();
~~~
 Computes the PolicyContainerPolicies and the sandbox flags to use for
 committing a regular document.

 Called when the response to commit is known.

### ComputePoliciesToCommitForError

ComputePoliciesToCommitForError
~~~cpp
void ComputePoliciesToCommitForError();
~~~
 Computes the PolicyContainerPolicies and the sandbox flags to use for
 committing an error document.


 Note:
 |ComputePoliciesToCommit()| can be followed by
 |ComputePoliciesToCommitForErrorPage()|. This happens when the decision to
 commit an error document happens after receiving the regular document's
 response.

### CheckStateTransition

CheckStateTransition
~~~cpp
void CheckStateTransition(NavigationState state) const;
~~~
 DCHECK that tranistioning from the current state to |state| valid. This
 does nothing in non-debug builds.

### SetState

SetState
~~~cpp
void SetState(NavigationState state);
~~~
 Set |state_| to |state| and also DCHECK that this state transition is
 valid.

### MaybeCancelFailedNavigation

MaybeCancelFailedNavigation
~~~cpp
bool MaybeCancelFailedNavigation();
~~~
 When a navigation fails, one of two things can happen:
 1) An error page commits and replaces the old document.

 2) The navigation is canceled, and the previous document is kept.


 If appropriate, this applies (2), deletes |this|, and returns true.

 In that case, the caller must immediately return.

### SendDeferredConsoleMessages

SendDeferredConsoleMessages
~~~cpp
void SendDeferredConsoleMessages();
~~~
 Called just after a navigation commits (also in case of error): it
 sends all console messages to the final RenderFrameHost.

### ShouldRenderFallbackContentForResponse

ShouldRenderFallbackContentForResponse
~~~cpp
bool ShouldRenderFallbackContentForResponse(
      const net::HttpResponseHeaders& response_head) const;
~~~

### ShouldReplaceCurrentEntryForSameUrlNavigation

ShouldReplaceCurrentEntryForSameUrlNavigation
~~~cpp
bool ShouldReplaceCurrentEntryForSameUrlNavigation() const;
~~~
 Whether this is a same-URL navigation that should replace the current entry
 or not. Called when the navigation just started.

### ShouldReplaceCurrentEntryForNavigationFromInitialEmptyDocumentOrEntry

ShouldReplaceCurrentEntryForNavigationFromInitialEmptyDocumentOrEntry
~~~cpp
bool ShouldReplaceCurrentEntryForNavigationFromInitialEmptyDocumentOrEntry()
      const;
~~~
 Whether this navigation happens on the initial empty document or initial
 NavigationEntry, and thus should replace the current entry. Called when the
 navigation just started.

### ShouldReplaceCurrentEntryForFailedNavigation

ShouldReplaceCurrentEntryForFailedNavigation
~~~cpp
bool ShouldReplaceCurrentEntryForFailedNavigation() const;
~~~
 Whether a failed navigation should replace the current entry or not. Called
 when an error page is about to be committed.

### GetOriginForURLLoaderFactoryBeforeResponse

GetOriginForURLLoaderFactoryBeforeResponse
~~~cpp
url::Origin GetOriginForURLLoaderFactoryBeforeResponse(
      network::mojom::WebSandboxFlags sandbox_flags);
~~~
 Calculates the origin that this NavigationRequest may commit. See also the
 comment of GetOriginToCommit(). Performs calculation without information
 from RenderFrameHostImpl (e.g. CSPs are ignored). Should be used only in
 situations where the final frame host hasn't been determined but the origin
 is needed to create URLLoaderFactory.

### GetOriginForURLLoaderFactoryAfterResponse

GetOriginForURLLoaderFactoryAfterResponse
~~~cpp
absl::optional<url::Origin> GetOriginForURLLoaderFactoryAfterResponse();
~~~
 Superset of GetOriginForURLLoaderFactoryBeforeResponse(). Calculates
 the origin with information from the final frame host. Can be called only
 after the final response is received or ready.

### GetOriginForURLLoaderFactoryBeforeResponseWithDebugInfo

GetOriginForURLLoaderFactoryBeforeResponseWithDebugInfo
~~~cpp
std::pair<url::Origin, std::string>
  GetOriginForURLLoaderFactoryBeforeResponseWithDebugInfo(
      network::mojom::WebSandboxFlags sandbox_flags);
~~~
 These functions are the same as their non-WithDebugInfo counterparts,
 except that they include information about how the origin gets calculated,
 to help debug if the browser-side calculated origin for this navigation
 differs from the origin calculated on the renderer side.

 TODO(https://crbug.com/1220238): Remove this.

### GetOriginForURLLoaderFactoryAfterResponseWithDebugInfo

GetOriginForURLLoaderFactoryAfterResponseWithDebugInfo
~~~cpp
std::pair<absl::optional<url::Origin>, std::string>
  GetOriginForURLLoaderFactoryAfterResponseWithDebugInfo();
~~~

### ComputeWebExposedIsolationInfo

ComputeWebExposedIsolationInfo
~~~cpp
absl::optional<WebExposedIsolationInfo> ComputeWebExposedIsolationInfo();
~~~
 Computes the web-exposed isolation information based on `coop_status_` and
 current `frame_tree_node_` info.

 If the return result is nullopt, it means that the WebExposedIsolationInfo
 is not relevant or unknown. This can happen for example when we do not have
 a network response yet, or when going to an "about:blank" page.

### ComputeCommonCoopOrigin

ComputeCommonCoopOrigin
~~~cpp
absl::optional<url::Origin> ComputeCommonCoopOrigin();
~~~
 Computes whether the navigation is for a document that should live in a
 BrowsingInstance only containing other documents with the same COOP value
 set by the same origin. This is the case if this document or its top-level
 document sets COOP: same-origin or COOP: restrict-properties. If it is a
 top-level document, simply return its origin, otherwise inherit the
 top-level document value.


 If the return value is nullopt, it indicates that neither COOP: same-origin
 nor COOP: restrict-properties were used for this document or for its parent
 in the case of a subframe.

### MaybeAssignInvalidPrerenderFrameTreeNodeId

MaybeAssignInvalidPrerenderFrameTreeNodeId
~~~cpp
void MaybeAssignInvalidPrerenderFrameTreeNodeId();
~~~
 Assign an invalid frame tree node id to `prerender_frame_tree_node_id_`.

 Called as soon as when we are certain that this navigation won't activate a
 prerendered page. This is needed because `IsPrerenderedPageActivation()`,
 which may be called at any point after BeginNavigation(), will assume that
 'prerender_frame_tree_node_id_' has an value assigned.

### MaybeInjectIsolatedAppHeaders

MaybeInjectIsolatedAppHeaders
~~~cpp
void MaybeInjectIsolatedAppHeaders();
~~~
 Check if the current navigation request is to an isolated app and injects
 the appropriate Cross-Origin-Opener-Policy, Cross-Origin-Embedder-Policy,
 Cross-Origin-Resource-Policy, and X-Frame-Options headers to enforce the
 security requirements for isolated apps.


 This is a temporary method to make sure that these policies are enforced
 for isolated apps. Longer term, it would be better to validate that these
 headers are included for isolated apps by developers.

 TODO(https://crbug.com/1311061): Remove or replace this method with the
 header validation logic for isolated apps.

### ComputeDownloadPolicy

ComputeDownloadPolicy
~~~cpp
void ComputeDownloadPolicy();
~~~
 The NavigationDownloadPolicy is currently fully computed by the renderer
 process. It is left empty for browser side initiated navigation. This is a
 problem. This function is an incomplete attempt to start computing it from
 the browser process instead.

 TODO(https://crbug.com/1395742): Complete the implementation the browser
 side implementation.

### download_policy

download_policy
~~~cpp
blink::NavigationDownloadPolicy& download_policy() {
    return common_params_->download_policy;
  }
~~~

### ResumeCommitIfNeeded

ResumeCommitIfNeeded
~~~cpp
void ResumeCommitIfNeeded();
~~~
 Called on FrameTreeNode's NavigationRequest (if any) when another
 NavigationRequest associated with the same FrameTreeNode is destroyed.

### MaybeRegisterOriginForUnpartitionedSessionStorageAccess

MaybeRegisterOriginForUnpartitionedSessionStorageAccess
~~~cpp
void MaybeRegisterOriginForUnpartitionedSessionStorageAccess();
~~~
 Used to detect if the page being navigated to is participating in the
 related deprecation trial and recording that in NavigationControllerImpl.


 Not called for same-document navigation requests nor for requests served
 from the back-forward cache or from prerendered pages as work would be
 redundant.


 TODO(crbug.com/1407150): Remove this when deprecation trial is complete.

### CheckSoftNavigationHeuristicsInvariants

CheckSoftNavigationHeuristicsInvariants
~~~cpp
void CheckSoftNavigationHeuristicsInvariants();
~~~
 See https://crbug.com/1412365
### UnblockPendingSubframeNavigationRequestsIfNeeded

UnblockPendingSubframeNavigationRequestsIfNeeded
~~~cpp
void UnblockPendingSubframeNavigationRequestsIfNeeded();
~~~
 Used to resume any SubframeHistoryNavigationThrottles in this FrameTree
 when this NavigationRequest commits.

 `subframe_history_navigation_throttles_` will only be populated if this
 IsInMainFrame().

### ShouldAddCookieChangeListener

ShouldAddCookieChangeListener
~~~cpp
bool ShouldAddCookieChangeListener();
~~~
 Returns if we should add/reset the `CookieChangeListener` for the current
 navigation.

### GetStoragePartitionWithCurrentSiteInfo

GetStoragePartitionWithCurrentSiteInfo
~~~cpp
StoragePartition* GetStoragePartitionWithCurrentSiteInfo();
~~~
 Returns the `StoragePartition` based on the config from the `site_info_`.

## class ScopedCrashKeys
 Helper for logging crash keys related to a NavigationRequest (e.g.

 "navigation_request_url", "navigation_request_initiator", and
 "navigation_request_is_same_document").  The crash keys will be logged if a
 ScopedCrashKeys instance exists when a crash or DumpWithoutCrashing
 happens.

### ScopedCrashKeys

ScopedCrashKeys::ScopedCrashKeys
~~~cpp
explicit ScopedCrashKeys(NavigationRequest& navigation_request);
~~~

### ~ScopedCrashKeys

ScopedCrashKeys::~ScopedCrashKeys
~~~cpp
~ScopedCrashKeys();
~~~

### ScopedCrashKeys

ScopedCrashKeys
~~~cpp
ScopedCrashKeys(const ScopedCrashKeys&) = delete;
~~~
 No copy constructor and no copy assignment operator.

### operator=

operator=
~~~cpp
ScopedCrashKeys& operator=(const ScopedCrashKeys&) = delete;
~~~

### initiator_origin_



~~~cpp

url::debug::ScopedOriginCrashKey initiator_origin_;

~~~


### url_



~~~cpp

url::debug::ScopedUrlCrashKey url_;

~~~


### is_same_document_



~~~cpp

base::debug::ScopedCrashKeyString is_same_document_;

~~~


### ScopedCrashKeys

ScopedCrashKeys
~~~cpp
ScopedCrashKeys(const ScopedCrashKeys&) = delete;
~~~
 No copy constructor and no copy assignment operator.

### operator=

operator=
~~~cpp
ScopedCrashKeys& operator=(const ScopedCrashKeys&) = delete;
~~~

### initiator_origin_



~~~cpp

url::debug::ScopedOriginCrashKey initiator_origin_;

~~~


### url_



~~~cpp

url::debug::ScopedUrlCrashKey url_;

~~~


### is_same_document_



~~~cpp

base::debug::ScopedCrashKeyString is_same_document_;

~~~


## class ;

### NavigationRequest

;::NavigationRequest
~~~cpp
NavigationRequest(
      FrameTreeNode* frame_tree_node,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::BeginNavigationParamsPtr begin_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool browser_initiated,
      bool from_begin_navigation,
      bool is_synchronous_renderer_commit,
      const FrameNavigationEntry* frame_navigation_entry,
      NavigationEntryImpl* navitation_entry,
      std::unique_ptr<NavigationUIData> navigation_ui_data,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      mojo::PendingAssociatedRemote<mojom::NavigationClient> navigation_client,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache,
      base::WeakPtr<RenderFrameHostImpl> rfh_restored_from_back_forward_cache,
      int initiator_process_id,
      bool was_opener_suppressed,
      bool is_pdf,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      mojo::PendingReceiver<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener = mojo::NullReceiver(),
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt)
~~~
