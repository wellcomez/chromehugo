
## class RenderFrameHostDelegate
 An interface implemented by an object interested in knowing about the state
 of the RenderFrameHost.


 Layering note: Generally, WebContentsImpl should be the only implementation
 of this interface. In particular, WebContents::FromRenderFrameHost() assumes
 this. This delegate interface is useful for renderer_host/ to make requests
 to WebContentsImpl, as renderer_host/ is not permitted to know the
 WebContents type (see //renderer_host/DEPS).

### OnMessageReceived

RenderFrameHostDelegate::OnMessageReceived
~~~cpp
virtual bool OnMessageReceived(RenderFrameHostImpl* render_frame_host,
                                 const IPC::Message& message);
~~~
 This is used to give the delegate a chance to filter IPC messages.

### OnDidBlockNavigation

OnDidBlockNavigation
~~~cpp
virtual void OnDidBlockNavigation(
      const GURL& blocked_url,
      const GURL& initiator_url,
      blink::mojom::NavigationBlockedReason reason) {}
~~~
 Notification from the renderer host that a suspicious navigation of the
 main frame has been blocked. Allows the delegate to provide some UI to let
 the user know about the blocked navigation and give them the option to
 recover from it.

 |blocked_url| is the blocked navigation target, |initiator_url| is the URL
 of the frame initiating the navigation, |reason| specifies why the
 navigation was blocked.

### OnDidFinishLoad

OnDidFinishLoad
~~~cpp
virtual void OnDidFinishLoad(RenderFrameHostImpl* render_frame_host,
                               const GURL& url) {}
~~~
 Called when blink.mojom.LocalFrameHost::DidFinishLoad() is invoked.

### OnManifestUrlChanged

OnManifestUrlChanged
~~~cpp
virtual void OnManifestUrlChanged(PageImpl& page) {}
~~~
 Notifies that the manifest URL is updated.

### DidAddMessageToConsole

RenderFrameHostDelegate::DidAddMessageToConsole
~~~cpp
virtual bool DidAddMessageToConsole(
      RenderFrameHostImpl* source_frame,
      blink::mojom::ConsoleMessageLevel log_level,
      const std::u16string& message,
      int32_t line_no,
      const std::u16string& source_id,
      const absl::optional<std::u16string>& untrusted_stack_trace);
~~~
 A message was added to to the console. |source_id| is a URL.

 |untrusted_stack_trace| is not present for most messages; only when
 requested in advance and only for exceptions.

### RenderFrameCreated

RenderFrameCreated
~~~cpp
virtual void RenderFrameCreated(RenderFrameHostImpl* render_frame_host) {}
~~~
 Called when a RenderFrame for |render_frame_host| is created in the
 renderer process. Use |RenderFrameDeleted| to listen for when this
 RenderFrame goes away.

### RenderFrameDeleted

RenderFrameDeleted
~~~cpp
virtual void RenderFrameDeleted(RenderFrameHostImpl* render_frame_host) {}
~~~
 Called when a RenderFrame for |render_frame_host| is deleted or the
 renderer process in which it runs it has died. Use |RenderFrameCreated| to
 listen for when RenderFrame objects are created.

### ShowContextMenu

ShowContextMenu
~~~cpp
virtual void ShowContextMenu(
      RenderFrameHost& render_frame_host,
      mojo::PendingAssociatedRemote<blink::mojom::ContextMenuClient>
          context_menu_client,
      const ContextMenuParams& params) {}
~~~
 A context menu should be shown, to be built using the context information
 provided in the supplied params.

### RunJavaScriptDialog

RunJavaScriptDialog
~~~cpp
virtual void RunJavaScriptDialog(RenderFrameHostImpl* render_frame_host,
                                   const std::u16string& message,
                                   const std::u16string& default_prompt,
                                   JavaScriptDialogType type,
                                   bool disable_third_party_subframe_suppresion,
                                   JavaScriptDialogCallback callback) {}
~~~
 A JavaScript alert, confirmation or prompt dialog should be shown.

 Will only be called for active frames belonging to a primary page.

### RunBeforeUnloadConfirm

RunBeforeUnloadConfirm
~~~cpp
virtual void RunBeforeUnloadConfirm(RenderFrameHostImpl* render_frame_host,
                                      bool is_reload,
                                      JavaScriptDialogCallback callback) {}
~~~
 Will only be called for active frames belonging to a primary page.

### UpdateFaviconURL

UpdateFaviconURL
~~~cpp
virtual void UpdateFaviconURL(
      RenderFrameHostImpl* source,
      const std::vector<blink::mojom::FaviconURLPtr>& candidates) {}
~~~
 Notifies when new blink::mojom::FaviconURLPtr candidates are received from
 the renderer process.

### DidChangeName

DidChangeName
~~~cpp
virtual void DidChangeName(RenderFrameHostImpl* render_frame_host,
                             const std::string& name) {}
~~~
 The frame changed its window.name property.

### DidReceiveUserActivation

DidReceiveUserActivation
~~~cpp
virtual void DidReceiveUserActivation(
      RenderFrameHostImpl* render_frame_host) {}
~~~
 Called when a frame receives user activation. This may be called multiple
 times for the same frame. Does not include frames activated by the
 same-origin visibility heuristic, see `UserActivationState` for details.

### BindDisplayCutoutHost

BindDisplayCutoutHost
~~~cpp
virtual void BindDisplayCutoutHost(
      RenderFrameHostImpl* render_frame_host,
      mojo::PendingAssociatedReceiver<blink::mojom::DisplayCutoutHost>
          receiver) {}
~~~
 Binds a DisplayCutoutHost object associated to |render_frame_host|.

### DidChangeDisplayState

DidChangeDisplayState
~~~cpp
virtual void DidChangeDisplayState(RenderFrameHostImpl* render_frame_host,
                                     bool is_display_none) {}
~~~
 The display style of the frame has changed.

### FrameSizeChanged

FrameSizeChanged
~~~cpp
virtual void FrameSizeChanged(RenderFrameHostImpl* render_frame_host,
                                const gfx::Size& frame_size) {}
~~~
 The size of the frame has changed.

### DOMContentLoaded

DOMContentLoaded
~~~cpp
virtual void DOMContentLoaded(RenderFrameHostImpl* render_frame_host) {}
~~~
 The DOMContentLoaded handler in the frame has completed.

### DocumentOnLoadCompleted

DocumentOnLoadCompleted
~~~cpp
virtual void DocumentOnLoadCompleted(RenderFrameHostImpl* render_frame_host) {
  }
~~~
 The onload handler in the frame has completed. Only called for the top-
 level frame.

### UpdateTitle

UpdateTitle
~~~cpp
virtual void UpdateTitle(RenderFrameHostImpl* render_frame_host,
                           const std::u16string& title,
                           base::i18n::TextDirection title_direction) {}
~~~
 The page's title was changed and should be updated. Only called for the
 top-level frame.

### UpdateTargetURL

UpdateTargetURL
~~~cpp
virtual void UpdateTargetURL(RenderFrameHostImpl* render_frame_host,
                               const GURL& url) {}
~~~
 The destination URL has changed and should be updated.

### CreateMediaPlayerHostForRenderFrameHost

CreateMediaPlayerHostForRenderFrameHost
~~~cpp
virtual void CreateMediaPlayerHostForRenderFrameHost(
      RenderFrameHostImpl* frame_host,
      mojo::PendingAssociatedReceiver<media::mojom::MediaPlayerHost> receiver) {
  }
~~~
 Creates a MediaPlayerHost object associated to |frame_host| via its
 associated MediaWebContentsObserver, and binds |receiver| to it.

### RequestMediaAccessPermission

RenderFrameHostDelegate::RequestMediaAccessPermission
~~~cpp
virtual void RequestMediaAccessPermission(const MediaStreamRequest& request,
                                            MediaResponseCallback callback);
~~~
 The render frame has requested access to media devices listed in
 |request|, and the client should grant or deny that permission by
 calling |callback|.

### CheckMediaAccessPermission

RenderFrameHostDelegate::CheckMediaAccessPermission
~~~cpp
virtual bool CheckMediaAccessPermission(
      RenderFrameHostImpl* render_frame_host,
      const url::Origin& security_origin,
      blink::mojom::MediaStreamType type);
~~~
 Checks if we have permission to access the microphone or camera. Note that
 this does not query the user. |type| must be MEDIA_DEVICE_AUDIO_CAPTURE
 or MEDIA_DEVICE_VIDEO_CAPTURE.

### GetDefaultMediaDeviceID

RenderFrameHostDelegate::GetDefaultMediaDeviceID
~~~cpp
virtual std::string GetDefaultMediaDeviceID(
      blink::mojom::MediaStreamType type);
~~~
 Returns the ID of the default device for the given media device |type|.

 If the returned value is an empty string, it means that there is no
 default device for the given |type|.

### SetCaptureHandleConfig

SetCaptureHandleConfig
~~~cpp
virtual void SetCaptureHandleConfig(
      blink::mojom::CaptureHandleConfigPtr config) {}
~~~
 Setter for the capture handle config, which allows a captured application
 to opt-in to exposing information to its capturer(s).

### GetAccessibilityMode

RenderFrameHostDelegate::GetAccessibilityMode
~~~cpp
virtual ui::AXMode GetAccessibilityMode();
~~~
 Get the accessibility mode for the WebContents that owns this frame.

### AXTreeIDForMainFrameHasChanged

AXTreeIDForMainFrameHasChanged
~~~cpp
virtual void AXTreeIDForMainFrameHasChanged() {}
~~~
 Called whenever the AXTreeID for the topmost RenderFrameHost has changed.

### AccessibilityEventReceived

AccessibilityEventReceived
~~~cpp
virtual void AccessibilityEventReceived(
      const AXEventNotificationDetails& details) {}
~~~
 Called when accessibility events or location changes are received
 from a render frame, when the accessibility mode has the
 ui::AXMode::kWebContents flag set.

### AccessibilityLocationChangesReceived

AccessibilityLocationChangesReceived
~~~cpp
virtual void AccessibilityLocationChangesReceived(
      const std::vector<AXLocationChangeNotificationDetails>& details) {}
~~~

### GetGeolocationContext

RenderFrameHostDelegate::GetGeolocationContext
~~~cpp
virtual device::mojom::GeolocationContext* GetGeolocationContext();
~~~
 Gets the GeolocationContext associated with this delegate.

### GetNFC

RenderFrameHostDelegate::GetNFC
~~~cpp
virtual void GetNFC(RenderFrameHost* render_frame_host,
                      mojo::PendingReceiver<device::mojom::NFC> receiver);
~~~
 Gets an NFC implementation within the context of this delegate.

### CanEnterFullscreenMode

RenderFrameHostDelegate::CanEnterFullscreenMode
~~~cpp
virtual bool CanEnterFullscreenMode(
      RenderFrameHostImpl* requesting_frame,
      const blink::mojom::FullscreenOptions& options);
~~~
 Returns whether entering fullscreen with EnterFullscreenMode() is allowed.

### EnterFullscreenMode

EnterFullscreenMode
~~~cpp
virtual void EnterFullscreenMode(
      RenderFrameHostImpl* requesting_frame,
      const blink::mojom::FullscreenOptions& options) {}
~~~
 Notification that the frame with the given host wants to enter fullscreen
 mode. Must only be called if CanEnterFullscreenMode returns true.

### ExitFullscreenMode

ExitFullscreenMode
~~~cpp
virtual void ExitFullscreenMode(bool will_cause_resize) {}
~~~
 Notification that the frame wants to go out of fullscreen mode.

 |will_cause_resize| indicates whether the fullscreen change causes a
 view resize. e.g. This will be false when going from tab fullscreen to
 browser fullscreen.

### FullscreenStateChanged

RenderFrameHostDelegate::FullscreenStateChanged
~~~cpp
virtual void FullscreenStateChanged(
      RenderFrameHostImpl* rfh,
      bool is_fullscreen,
      blink::mojom::FullscreenOptionsPtr options);
~~~
 Notification that this frame has changed fullscreen state.

### UpdateUserGestureCarryoverInfo

UpdateUserGestureCarryoverInfo
~~~cpp
virtual void UpdateUserGestureCarryoverInfo() {}
~~~
 Updates information to determine whether a user gesture should carryover to
 future navigations. This is needed so navigations within a certain
 timeframe of a request initiated by a gesture will be treated as if they
 were initiated by a gesture too, otherwise the navigation may be blocked.

### ShouldRouteMessageEvent

RenderFrameHostDelegate::ShouldRouteMessageEvent
~~~cpp
virtual bool ShouldRouteMessageEvent(RenderFrameHostImpl* target_rfh) const;
~~~
 Let the delegate decide whether postMessage should be delivered to
 |target_rfh| from a source frame in the given SiteInstance.  This defaults
 to false and overrides the RenderFrameHost's decision if true.

### EnsureOpenerProxiesExist

EnsureOpenerProxiesExist
~~~cpp
virtual void EnsureOpenerProxiesExist(RenderFrameHostImpl* source_rfh) {}
~~~
 Ensure that |source_rfh| has swapped-out RenderViews and
 RenderFrameProxies for itself and for all frames on its opener chain in
 the current frame's SiteInstance.


 TODO(alexmos): This method currently supports cross-process postMessage,
 where we may need to create any missing proxies for the message's source
 frame and its opener chain. It currently exists in WebContents due to a
 special case for <webview> guests, but this logic should eventually be
 moved down into RenderFrameProxyHost::RouteMessageEvent when <webview>
 refactoring for --site-per-process mode is further along.  See
 https://crbug.com/330264.

### DidCallFocus

DidCallFocus
~~~cpp
virtual void DidCallFocus() {}
~~~
 The frame called |window.focus()|.

### IsInnerWebContentsForGuest

RenderFrameHostDelegate::IsInnerWebContentsForGuest
~~~cpp
virtual bool IsInnerWebContentsForGuest();
~~~
 Returns whether this delegate is an inner WebContents for a guest.

 TODO(https://crbug.com/1295431): Remove in favor of tracking pending guest
 initializations instead.

### GetFocusedFrame

RenderFrameHostDelegate::GetFocusedFrame
~~~cpp
virtual RenderFrameHostImpl* GetFocusedFrame();
~~~
 Returns the focused frame if it exists, potentially in an inner frame tree.

### OnAdvanceFocus

OnAdvanceFocus
~~~cpp
virtual void OnAdvanceFocus(RenderFrameHostImpl* source_rfh) {}
~~~
 Called by when |source_rfh| advances focus to a RenderFrameProxyHost.

### CreateWebUIForRenderFrameHost

RenderFrameHostDelegate::CreateWebUIForRenderFrameHost
~~~cpp
virtual std::unique_ptr<WebUIImpl> CreateWebUIForRenderFrameHost(
      RenderFrameHostImpl* frame_host,
      const GURL& url);
~~~
 Creates a WebUI object for a frame navigating to |url|. If no WebUI
 applies, returns null.

### OnFocusedElementChangedInFrame

OnFocusedElementChangedInFrame
~~~cpp
virtual void OnFocusedElementChangedInFrame(
      RenderFrameHostImpl* frame,
      const gfx::Rect& bounds_in_root_view,
      blink::mojom::FocusType focus_type) {}
~~~
 Called by |frame| to notify that it has received an update on focused
 element. |bounds_in_root_view| is the rectangle containing the element that
 is focused and is with respect to root frame's RenderWidgetHost's
 coordinate space.

### CreateNewWindow

RenderFrameHostDelegate::CreateNewWindow
~~~cpp
virtual FrameTree* CreateNewWindow(
      RenderFrameHostImpl* opener,
      const mojom::CreateNewWindowParams& params,
      bool is_new_browsing_instance,
      bool has_user_gesture,
      SessionStorageNamespace* session_storage_namespace);
~~~
 The page is trying to open a new page (e.g. a popup window). The window
 should be created associated the process of |opener|, but it should not
 be shown yet. That should happen in response to ShowCreatedWindow.

 |params.window_container_type| describes the type of RenderViewHost
 container that is requested -- in particular, the window.open call may
 have specified 'background' and 'persistent' in the feature string.


 The passed |opener| is the RenderFrameHost initiating the window creation.

 It will never be null, even if the opener is suppressed via |params|.


 The passed |params.frame_name| parameter is the name parameter that was
 passed to window.open(), and will be empty if none was passed.


 Note: this is not called "CreateWindow" because that will clash with
 the Windows function which is actually a #define.


 On success, a non-owning pointer to the new FrameTree is returned.


 The caller is expected to handle cleanup if this operation fails or is
 suppressed by checking if the return value is null.

### ShowCreatedWindow

ShowCreatedWindow
~~~cpp
virtual void ShowCreatedWindow(
      RenderFrameHostImpl* opener,
      int main_frame_widget_route_id,
      WindowOpenDisposition disposition,
      const blink::mojom::WindowFeatures& window_features,
      bool user_gesture) {}
~~~
 Show a previously created page with the specified disposition and window
 features. The window is identified by the |main_frame_widget_route_id|
 passed to CreateNewWindow.


 The passed |opener| is the RenderFrameHost initiating the window creation.

 It will never be null, even if the opener is suppressed via |params|.


 Note: this is not called "ShowWindow" because that will clash with
 the Windows function which is actually a #define.

### PrimaryMainDocumentElementAvailable

PrimaryMainDocumentElementAvailable
~~~cpp
virtual void PrimaryMainDocumentElementAvailable() {}
~~~
 The main frame document element is ready. This happens when the document
 has finished parsing.

### PassiveInsecureContentFound

PassiveInsecureContentFound
~~~cpp
virtual void PassiveInsecureContentFound(const GURL& resource_url) {}
~~~
 Reports that passive mixed content was found at the specified url.

### ShouldAllowRunningInsecureContent

RenderFrameHostDelegate::ShouldAllowRunningInsecureContent
~~~cpp
virtual bool ShouldAllowRunningInsecureContent(bool allowed_per_prefs,
                                                 const url::Origin& origin,
                                                 const GURL& resource_url);
~~~
 Checks if running of active mixed content is allowed in the current tab.

### ViewSource

ViewSource
~~~cpp
virtual void ViewSource(RenderFrameHostImpl* frame) {}
~~~
 Opens a new view-source tab for the last committed document in |frame|.

### GetJavaRenderFrameHostDelegate

RenderFrameHostDelegate::GetJavaRenderFrameHostDelegate
~~~cpp
virtual base::android::ScopedJavaLocalRef<jobject>
  GetJavaRenderFrameHostDelegate();
~~~

### ResourceLoadComplete

ResourceLoadComplete
~~~cpp
virtual void ResourceLoadComplete(
      RenderFrameHostImpl* render_frame_host,
      const GlobalRequestID& request_id,
      blink::mojom::ResourceLoadInfoPtr resource_load_info) {}
~~~
 Notified that the render finished loading a subresource for the frame
 associated with |render_frame_host|.

### PrintCrossProcessSubframe

PrintCrossProcessSubframe
~~~cpp
virtual void PrintCrossProcessSubframe(
      const gfx::Rect& rect,
      int document_cookie,
      RenderFrameHostImpl* render_frame_host) {}
~~~
 Request to print a frame that is in a different process than its parent.

### CapturePaintPreviewOfCrossProcessSubframe

CapturePaintPreviewOfCrossProcessSubframe
~~~cpp
virtual void CapturePaintPreviewOfCrossProcessSubframe(
      const gfx::Rect& rect,
      const base::UnguessableToken& guid,
      RenderFrameHostImpl* render_frame_host) {}
~~~
 Request to paint preview a frame that is in a different process that its
 parent.

### GetOrCreateWebPreferences

RenderFrameHostDelegate::GetOrCreateWebPreferences
~~~cpp
virtual const blink::web_pref::WebPreferences&
  GetOrCreateWebPreferences() = 0;
~~~
 Returns a copy of the current WebPreferences associated with this
 RenderFrameHost's WebContents. If it does not exist, this will create one
 and send the newly computed value to all renderers.

 Note that this will not trigger a recomputation of WebPreferences if it
 already exists - this will return the last computed/set value of
 WebPreferences. If we want to guarantee that the value reflects the current
 state of the WebContents, NotifyPreferencesChanged() should be called
 before calling this.

### GetVisibility

RenderFrameHostDelegate::GetVisibility
~~~cpp
virtual Visibility GetVisibility();
~~~
 Returns the visibility of the delegate.

### AudioContextPlaybackStarted

AudioContextPlaybackStarted
~~~cpp
virtual void AudioContextPlaybackStarted(RenderFrameHostImpl* host,
                                           int context_id) {}
~~~
 Notify observers if WebAudio AudioContext has started (or stopped) playing
 audible sounds.

### AudioContextPlaybackStopped

AudioContextPlaybackStopped
~~~cpp
virtual void AudioContextPlaybackStopped(RenderFrameHostImpl* host,
                                           int context_id) {}
~~~

### OnFrameAudioStateChanged

OnFrameAudioStateChanged
~~~cpp
virtual void OnFrameAudioStateChanged(RenderFrameHostImpl* host,
                                        bool is_audible) {}
~~~
 Notifies observers if the frame has changed audible state.

### GetUnattachedOwnedNodes

RenderFrameHostDelegate::GetUnattachedOwnedNodes
~~~cpp
virtual std::vector<FrameTreeNode*> GetUnattachedOwnedNodes(
      RenderFrameHostImpl* owner);
~~~
 Returns FrameTreeNodes that are logically owned by another frame even
 though this relationship is not yet reflected in their frame trees. This
 can happen, for example, with unattached guests and orphaned portals.

### RegisterProtocolHandler

RegisterProtocolHandler
~~~cpp
virtual void RegisterProtocolHandler(RenderFrameHostImpl* host,
                                       const std::string& scheme,
                                       const GURL& url,
                                       bool user_gesture) {}
~~~
 Registers a new URL handler for the given protocol.

### UnregisterProtocolHandler

UnregisterProtocolHandler
~~~cpp
virtual void UnregisterProtocolHandler(RenderFrameHostImpl* host,
                                         const std::string& scheme,
                                         const GURL& url,
                                         bool user_gesture) {}
~~~
 Unregisters a given URL handler for the given protocol.

### IsAllowedToGoToEntryAtOffset

RenderFrameHostDelegate::IsAllowedToGoToEntryAtOffset
~~~cpp
virtual bool IsAllowedToGoToEntryAtOffset(int32_t offset);
~~~
 Returns true if the delegate allows to go to the session history entry at
 the given offset (ie, -1 will return the "back" item).

### GetRecordAggregateWatchTimeCallback

RenderFrameHostDelegate::GetRecordAggregateWatchTimeCallback
~~~cpp
virtual media::MediaMetricsProvider::RecordAggregateWatchTimeCallback
  GetRecordAggregateWatchTimeCallback(
      const GURL& page_main_frame_last_committed_url);
~~~

### IsClipboardPasteContentAllowed

RenderFrameHostDelegate::IsClipboardPasteContentAllowed
~~~cpp
virtual void IsClipboardPasteContentAllowed(
      const GURL& url,
      const ui::ClipboardFormatType& data_type,
      const std::string& data,
      IsClipboardPasteContentAllowedCallback callback);
~~~
 Determines if a clipboard paste using |data| of type |data_type| is allowed
 in this renderer frame.  Possible data types supported for paste can be
 seen in the ClipboardHostImpl class.  Text based formats will use the
 data_type ui::ClipboardFormatType::PlainTextType() unless it is known
 to be of a more specific type, like RTF or HTML, in which case a type
 such as ui::ClipboardFormatType::RtfType() or
 ui::ClipboardFormatType::HtmlType() is used.


 It is also possible for the data type to be
 ui::ClipboardFormatType::WebCustomDataType() indicating that the paste
 uses a custom data format.  It is up to the implementation to attempt to
 understand the type if possible.  It is acceptable to deny pastes of
 unknown data types.


 The implementation is expected to show UX to the user if needed.  If
 shown, the UX should be associated with the specific RenderFrameHost.


 The callback is called, possibly asynchronously, with a status indicating
 whether the operation is allowed or not.

### OnPageScaleFactorChanged

OnPageScaleFactorChanged
~~~cpp
virtual void OnPageScaleFactorChanged(PageImpl& source) {}
~~~
 Notified when the main frame of `source` adjusts the page scale.

### BindScreenOrientation

BindScreenOrientation
~~~cpp
virtual void BindScreenOrientation(
      RenderFrameHost* render_frame_host,
      mojo::PendingAssociatedReceiver<device::mojom::ScreenOrientation>
          receiver) {}
~~~
 Binds a ScreenOrientation object associated to |render_frame_host|.

### HasSeenRecentScreenOrientationChange

RenderFrameHostDelegate::HasSeenRecentScreenOrientationChange
~~~cpp
virtual bool HasSeenRecentScreenOrientationChange();
~~~
 Return true if we have seen a recent orientation change, which is used to
 decide if we should consume user activation when entering fullscreen.

### IsTransientAllowFullscreenActive

RenderFrameHostDelegate::IsTransientAllowFullscreenActive
~~~cpp
virtual bool IsTransientAllowFullscreenActive() const;
~~~
 Return true if the page has a transient affordance to enter fullscreen
 without consuming user activation.

### IsBackForwardCacheSupported

RenderFrameHostDelegate::IsBackForwardCacheSupported
~~~cpp
virtual bool IsBackForwardCacheSupported();
~~~
 Return true if the back forward cache is supported. This is not an
 indication that the cache will be used.

### CreateNewPopupWidget

RenderFrameHostDelegate::CreateNewPopupWidget
~~~cpp
virtual RenderWidgetHostImpl* CreateNewPopupWidget(
      base::SafeRef<SiteInstanceGroup> site_instance_group,
      int32_t route_id,
      mojo::PendingAssociatedReceiver<blink::mojom::PopupWidgetHost>
          blink_popup_widget_host,
      mojo::PendingAssociatedReceiver<blink::mojom::WidgetHost>
          blink_widget_host,
      mojo::PendingAssociatedRemote<blink::mojom::Widget> blink_widget);
~~~
 The page is trying to open a new widget (e.g. a select popup). The
 widget should be created associated with the given
 |site_instance_group|, but it should not be shown yet. That should
 happen in response to ShowCreatedWidget.

### ShowPopupMenu

RenderFrameHostDelegate::ShowPopupMenu
~~~cpp
virtual bool ShowPopupMenu(RenderFrameHostImpl* render_frame_host,
                             const gfx::Rect& bounds);
~~~
 Returns true if the popup is shown through WebContentsObserver. Else, the
 Android / Mac flavors of `RenderViewHostDelegateView` will show the popup
 menu correspondingly, and `WebContentsViewChildFrame` will show the popup
 for Mac's GuestView.

### DidLoadResourceFromMemoryCache

DidLoadResourceFromMemoryCache
~~~cpp
virtual void DidLoadResourceFromMemoryCache(
      RenderFrameHostImpl* source,
      const GURL& url,
      const std::string& http_request,
      const std::string& mime_type,
      network::mojom::RequestDestination request_destination,
      bool include_credentials) {}
~~~

### DomOperationResponse

DomOperationResponse
~~~cpp
virtual void DomOperationResponse(RenderFrameHost* render_frame_host,
                                    const std::string& json_string) {}
~~~
 Called when the renderer sends a response via DomAutomationController.

 For example, `window.domAutomationController.send(foo())` sends the result
 of foo() here.

### OnCookiesAccessed

OnCookiesAccessed
~~~cpp
virtual void OnCookiesAccessed(RenderFrameHostImpl* render_frame_host,
                                 const CookieAccessDetails& details) {}
~~~

### OnTrustTokensAccessed

OnTrustTokensAccessed
~~~cpp
virtual void OnTrustTokensAccessed(RenderFrameHostImpl* render_frame_host,
                                     const TrustTokenAccessDetails& details) {}
~~~

### SavableResourceLinksResponse

SavableResourceLinksResponse
~~~cpp
virtual void SavableResourceLinksResponse(
      RenderFrameHostImpl* source,
      const std::vector<GURL>& resources_list,
      blink::mojom::ReferrerPtr referrer,
      const std::vector<blink::mojom::SavableSubframePtr>& subframes) {}
~~~
 Notified that the renderer responded after calling GetSavableResourceLinks.

### SavableResourceLinksError

SavableResourceLinksError
~~~cpp
virtual void SavableResourceLinksError(RenderFrameHostImpl* source) {}
~~~
 Notified that the renderer returned an error after calling
 GetSavableResourceLinks in case the frame contains non-savable content
 (i.e. from a non-savable scheme) or if there were errors gathering the
 links.

### RenderFrameHostStateChanged

RenderFrameHostStateChanged
~~~cpp
virtual void RenderFrameHostStateChanged(
      RenderFrameHost* host,
      RenderFrameHost::LifecycleState old_state,
      RenderFrameHost::LifecycleState new_state) {}
~~~
 Called when |RenderFrameHost::GetLifecycleState()| changes i.e.,
 when RenderFrameHost LifecycleState changes from old_state to
 new_state.

### SetWindowRect

SetWindowRect
~~~cpp
virtual void SetWindowRect(const gfx::Rect& new_bounds) {}
~~~
 The page is trying to move the main frame's representation in the client.

### UpdateWindowPreferredSize

UpdateWindowPreferredSize
~~~cpp
virtual void UpdateWindowPreferredSize(const gfx::Size& pref_size) {}
~~~
 The page's preferred size changed.

### GetActiveTopLevelDocumentsInBrowsingContextGroup

RenderFrameHostDelegate::GetActiveTopLevelDocumentsInBrowsingContextGroup
~~~cpp
virtual std::vector<RenderFrameHostImpl*>
  GetActiveTopLevelDocumentsInBrowsingContextGroup(
      RenderFrameHostImpl* render_frame_host);
~~~
 Returns the list of top-level RenderFrameHosts hosting active documents
 that belong to the same browsing context group as |render_frame_host|.

### GetPrerenderHostRegistry

RenderFrameHostDelegate::GetPrerenderHostRegistry
~~~cpp
virtual PrerenderHostRegistry* GetPrerenderHostRegistry();
~~~
 Returns the PrerenderHostRegistry to start/cancel prerendering. This
 doesn't return nullptr except for some tests.

### OnPepperInstanceCreated

OnPepperInstanceCreated
~~~cpp
virtual void OnPepperInstanceCreated(RenderFrameHostImpl* source,
                                       int32_t pp_instance) {}
~~~

### OnPepperInstanceDeleted

OnPepperInstanceDeleted
~~~cpp
virtual void OnPepperInstanceDeleted(RenderFrameHostImpl* source,
                                       int32_t pp_instance) {}
~~~

### OnPepperStartsPlayback

OnPepperStartsPlayback
~~~cpp
virtual void OnPepperStartsPlayback(RenderFrameHostImpl* source,
                                      int32_t pp_instance) {}
~~~

### OnPepperStopsPlayback

OnPepperStopsPlayback
~~~cpp
virtual void OnPepperStopsPlayback(RenderFrameHostImpl* source,
                                     int32_t pp_instance) {}
~~~

### OnPepperPluginCrashed

OnPepperPluginCrashed
~~~cpp
virtual void OnPepperPluginCrashed(RenderFrameHostImpl* source,
                                     const base::FilePath& plugin_path,
                                     base::ProcessId plugin_pid) {}
~~~

### OnPepperPluginHung

OnPepperPluginHung
~~~cpp
virtual void OnPepperPluginHung(RenderFrameHostImpl* source,
                                  int plugin_child_id,
                                  const base::FilePath& path,
                                  bool is_hung) {}
~~~

### DidChangeLoadProgressForPrimaryMainFrame

DidChangeLoadProgressForPrimaryMainFrame
~~~cpp
virtual void DidChangeLoadProgressForPrimaryMainFrame() {}
~~~
 The load progress for the primary main frame was changed.

### DidFailLoadWithError

DidFailLoadWithError
~~~cpp
virtual void DidFailLoadWithError(RenderFrameHostImpl* render_frame_host,
                                    const GURL& url,
                                    int error_code) {}
~~~
 Document load in |render_frame_host| failed.

### Close

Close
~~~cpp
virtual void Close() {}
~~~
 Called by the primary main frame to close the current tab/window.

### IsJavaScriptDialogShowing

RenderFrameHostDelegate::IsJavaScriptDialogShowing
~~~cpp
virtual bool IsJavaScriptDialogShowing() const;
~~~
 True if the delegate is currently showing a JavaScript dialog.

### ShouldIgnoreUnresponsiveRenderer

RenderFrameHostDelegate::ShouldIgnoreUnresponsiveRenderer
~~~cpp
virtual bool ShouldIgnoreUnresponsiveRenderer();
~~~
 If a timer for an unresponsive renderer fires, whether it should be
 ignored.
