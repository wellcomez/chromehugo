
## class RenderFrameHostFactory
 A factory for creating RenderFrameHosts. There is a global factory function
 that can be installed for the purposes of testing to provide a specialized
 RenderFrameHostImpl class.

### RenderFrameHostFactory

RenderFrameHostFactory
~~~cpp
RenderFrameHostFactory(const RenderFrameHostFactory&) = delete;
~~~

### operator=

RenderFrameHostFactory::operator=
~~~cpp
RenderFrameHostFactory& operator=(const RenderFrameHostFactory&) = delete;
~~~

### has_factory

has_factory
~~~cpp
static bool has_factory() { return !!factory_; }
~~~
 Returns true if there is currently a globally-registered factory.

### ~RenderFrameHostFactory

~RenderFrameHostFactory
~~~cpp
virtual ~RenderFrameHostFactory() {}
~~~

### CreateRenderFrameHost

RenderFrameHostFactory::CreateRenderFrameHost
~~~cpp
virtual std::unique_ptr<RenderFrameHostImpl> CreateRenderFrameHost(
      SiteInstance* site_instance,
      scoped_refptr<RenderViewHostImpl> render_view_host,
      RenderFrameHostDelegate* delegate,
      FrameTree* frame_tree,
      FrameTreeNode* frame_tree_node,
      int32_t routing_id,
      mojo::PendingAssociatedRemote<mojom::Frame> frame_remote,
      const blink::LocalFrameToken& frame_token,
      const blink::DocumentToken& document_token,
      base::UnguessableToken devtools_frame_token,
      bool renderer_initiated_creation,
      RenderFrameHostImpl::LifecycleStateImpl lifecycle_state,
      scoped_refptr<BrowsingContextState> browsing_context_state) = 0;
~~~
 You can derive from this class and specify an implementation for this
 function to create an alternate kind of RenderFrameHostImpl for testing.

### RegisterFactory

RenderFrameHostFactory::RegisterFactory
~~~cpp
static void RegisterFactory(RenderFrameHostFactory* factory);
~~~
 Registers a factory to be called when new RenderFrameHostImpls are created.

 We have only one global factory, so there must be no factory registered
 before the call. This class does NOT take ownership of the pointer.

### UnregisterFactory

RenderFrameHostFactory::UnregisterFactory
~~~cpp
static void UnregisterFactory();
~~~
 Unregister the previously registered factory. With no factory registered,
 regular RenderFrameHostImpls will be created.
