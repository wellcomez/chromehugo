
## class RenderFrameHostOwner
 An interface for RenderFrameHostImpl to communicate with FrameTreeNode owning
 it (e.g. to initiate or cancel a navigation in the frame).


 As main RenderFrameHostImpl can be moved between different FrameTreeNodes
 (i.e.during prerender activations), RenderFrameHostImpl should not reference
 FrameTreeNode directly to prevent accident violation of implicit "associated
 FTN stays the same" assumptions. Instead, a targeted interface is exposed
 instead.


 If you need to store information which should persist during prerender
 activations and same-BrowsingContext navigations, consider using
 BrowsingContextState instead.

### RenderFrameHostOwner

RenderFrameHostOwner
~~~cpp
RenderFrameHostOwner() = default;
~~~

### ~RenderFrameHostOwner

~RenderFrameHostOwner
~~~cpp
virtual ~RenderFrameHostOwner() = default;
~~~

### DidStartLoading

DidStartLoading
~~~cpp
virtual void DidStartLoading(bool should_show_loading_ui,
                               bool was_previously_loading) = 0;
~~~
 A RenderFrameHost started loading:

 - `should_show_loading_ui` indicates whether the loading indicator UI
   should be shown or not. It must be true for:
   * cross-document navigations
   * navigations intercepted by the navigation API's intercept().


 - `was_previously_loading` is false if the FrameTree was not loading
   before. The caller is required to provide this boolean as the delegate
   should only be notified if the FrameTree went from non-loading to loading
   state. However, when it is called, the FrameTree should be in a loading
   state.

### DidStopLoading

DidStopLoading
~~~cpp
virtual void DidStopLoading() = 0;
~~~
 A RenderFrameHost in this owner stopped loading.

### RestartNavigationAsCrossDocument

RestartNavigationAsCrossDocument
~~~cpp
virtual void RestartNavigationAsCrossDocument(
      std::unique_ptr<NavigationRequest> navigation_request) = 0;
~~~

### Reload

Reload
~~~cpp
virtual bool Reload() = 0;
~~~
 Reload the current document in this frame again. Return whether an actual
 navigation request was created or not.

### GetCurrentNavigator

GetCurrentNavigator
~~~cpp
virtual Navigator& GetCurrentNavigator() = 0;
~~~

### GetRenderFrameHostManager

GetRenderFrameHostManager
~~~cpp
virtual RenderFrameHostManager& GetRenderFrameHostManager() = 0;
~~~

### GetOpener

GetOpener
~~~cpp
virtual FrameTreeNode* GetOpener() const = 0;
~~~

### SetFocusedFrame

SetFocusedFrame
~~~cpp
virtual void SetFocusedFrame(SiteInstanceGroup* source) = 0;
~~~

### DidChangeReferrerPolicy

DidChangeReferrerPolicy
~~~cpp
virtual void DidChangeReferrerPolicy(
      network::mojom::ReferrerPolicy referrer_policy) = 0;
~~~
 Called when the referrer policy changes.

### UpdateUserActivationState

UpdateUserActivationState
~~~cpp
virtual bool UpdateUserActivationState(
      blink::mojom::UserActivationUpdateType update_type,
      blink::mojom::UserActivationNotificationType notification_type) = 0;
~~~

### DidConsumeHistoryUserActivation

DidConsumeHistoryUserActivation
~~~cpp
virtual void DidConsumeHistoryUserActivation() = 0;
~~~
 Called to notify all frames of a page that the history user activation
 has been consumed, in response to an event in the renderer process.

### CreateNavigationRequestForSynchronousRendererCommit

CreateNavigationRequestForSynchronousRendererCommit
~~~cpp
virtual std::unique_ptr<NavigationRequest>
  CreateNavigationRequestForSynchronousRendererCommit(
      RenderFrameHostImpl* render_frame_host,
      bool is_same_document,
      const GURL& url,
      const url::Origin& origin,
      const absl::optional<GURL>& initiator_base_url,
      const net::IsolationInfo& isolation_info_for_subresources,
      blink::mojom::ReferrerPtr referrer,
      const ui::PageTransition& transition,
      bool should_replace_current_entry,
      const std::string& method,
      bool has_transient_activation,
      bool is_overriding_user_agent,
      const std::vector<GURL>& redirects,
      const GURL& original_url,
      std::unique_ptr<CrossOriginEmbedderPolicyReporter> coep_reporter,
      std::unique_ptr<SubresourceWebBundleNavigationInfo>
          subresource_web_bundle_navigation_info,
      int http_response_code) = 0;
~~~
 Creates a NavigationRequest  for a synchronous navigation that has
 committed in the renderer process. Those are:
 - same-document renderer-initiated navigations.

 - synchronous about:blank navigations.

### CancelNavigation

CancelNavigation
~~~cpp
virtual void CancelNavigation() = 0;
~~~
 Cancels the navigation owned by the FrameTreeNode.

 Note: this does not cancel navigations that are owned by the current or
 speculative RenderFrameHosts.

### Credentialless

Credentialless
~~~cpp
virtual bool Credentialless() const = 0;
~~~
 Return the iframe.credentialless attribute value.

### SetFencedFrameAutomaticBeaconReportEventData

SetFencedFrameAutomaticBeaconReportEventData
~~~cpp
virtual void SetFencedFrameAutomaticBeaconReportEventData(
      const std::string& event_data,
      const std::vector<blink::FencedFrame::ReportingDestination>&
          destination) = 0;
~~~
 Stores the payload that will be sent as part of an automatic beacon. Right
 now only the "reserved.top_navigation" beacon is supported.

### GetVirtualAuthenticatorManager

GetVirtualAuthenticatorManager
~~~cpp
virtual void GetVirtualAuthenticatorManager(
      mojo::PendingReceiver<blink::test::mojom::VirtualAuthenticatorManager>
          receiver) = 0;
~~~

### RenderFrameHostOwner

RenderFrameHostOwner
~~~cpp
RenderFrameHostOwner() = default;
~~~

### ~RenderFrameHostOwner

~RenderFrameHostOwner
~~~cpp
virtual ~RenderFrameHostOwner() = default;
~~~

### DidStartLoading

DidStartLoading
~~~cpp
virtual void DidStartLoading(bool should_show_loading_ui,
                               bool was_previously_loading) = 0;
~~~
 A RenderFrameHost started loading:

 - `should_show_loading_ui` indicates whether the loading indicator UI
   should be shown or not. It must be true for:
   * cross-document navigations
   * navigations intercepted by the navigation API's intercept().


 - `was_previously_loading` is false if the FrameTree was not loading
   before. The caller is required to provide this boolean as the delegate
   should only be notified if the FrameTree went from non-loading to loading
   state. However, when it is called, the FrameTree should be in a loading
   state.

### DidStopLoading

DidStopLoading
~~~cpp
virtual void DidStopLoading() = 0;
~~~
 A RenderFrameHost in this owner stopped loading.

### RestartNavigationAsCrossDocument

RestartNavigationAsCrossDocument
~~~cpp
virtual void RestartNavigationAsCrossDocument(
      std::unique_ptr<NavigationRequest> navigation_request) = 0;
~~~

### Reload

Reload
~~~cpp
virtual bool Reload() = 0;
~~~
 Reload the current document in this frame again. Return whether an actual
 navigation request was created or not.

### GetCurrentNavigator

GetCurrentNavigator
~~~cpp
virtual Navigator& GetCurrentNavigator() = 0;
~~~

### GetRenderFrameHostManager

GetRenderFrameHostManager
~~~cpp
virtual RenderFrameHostManager& GetRenderFrameHostManager() = 0;
~~~

### GetOpener

GetOpener
~~~cpp
virtual FrameTreeNode* GetOpener() const = 0;
~~~

### SetFocusedFrame

SetFocusedFrame
~~~cpp
virtual void SetFocusedFrame(SiteInstanceGroup* source) = 0;
~~~

### DidChangeReferrerPolicy

DidChangeReferrerPolicy
~~~cpp
virtual void DidChangeReferrerPolicy(
      network::mojom::ReferrerPolicy referrer_policy) = 0;
~~~
 Called when the referrer policy changes.

### UpdateUserActivationState

UpdateUserActivationState
~~~cpp
virtual bool UpdateUserActivationState(
      blink::mojom::UserActivationUpdateType update_type,
      blink::mojom::UserActivationNotificationType notification_type) = 0;
~~~

### DidConsumeHistoryUserActivation

DidConsumeHistoryUserActivation
~~~cpp
virtual void DidConsumeHistoryUserActivation() = 0;
~~~
 Called to notify all frames of a page that the history user activation
 has been consumed, in response to an event in the renderer process.

### CreateNavigationRequestForSynchronousRendererCommit

CreateNavigationRequestForSynchronousRendererCommit
~~~cpp
virtual std::unique_ptr<NavigationRequest>
  CreateNavigationRequestForSynchronousRendererCommit(
      RenderFrameHostImpl* render_frame_host,
      bool is_same_document,
      const GURL& url,
      const url::Origin& origin,
      const absl::optional<GURL>& initiator_base_url,
      const net::IsolationInfo& isolation_info_for_subresources,
      blink::mojom::ReferrerPtr referrer,
      const ui::PageTransition& transition,
      bool should_replace_current_entry,
      const std::string& method,
      bool has_transient_activation,
      bool is_overriding_user_agent,
      const std::vector<GURL>& redirects,
      const GURL& original_url,
      std::unique_ptr<CrossOriginEmbedderPolicyReporter> coep_reporter,
      std::unique_ptr<SubresourceWebBundleNavigationInfo>
          subresource_web_bundle_navigation_info,
      int http_response_code) = 0;
~~~
 Creates a NavigationRequest  for a synchronous navigation that has
 committed in the renderer process. Those are:
 - same-document renderer-initiated navigations.

 - synchronous about:blank navigations.

### CancelNavigation

CancelNavigation
~~~cpp
virtual void CancelNavigation() = 0;
~~~
 Cancels the navigation owned by the FrameTreeNode.

 Note: this does not cancel navigations that are owned by the current or
 speculative RenderFrameHosts.

### Credentialless

Credentialless
~~~cpp
virtual bool Credentialless() const = 0;
~~~
 Return the iframe.credentialless attribute value.

### SetFencedFrameAutomaticBeaconReportEventData

SetFencedFrameAutomaticBeaconReportEventData
~~~cpp
virtual void SetFencedFrameAutomaticBeaconReportEventData(
      const std::string& event_data,
      const std::vector<blink::FencedFrame::ReportingDestination>&
          destination) = 0;
~~~
 Stores the payload that will be sent as part of an automatic beacon. Right
 now only the "reserved.top_navigation" beacon is supported.
