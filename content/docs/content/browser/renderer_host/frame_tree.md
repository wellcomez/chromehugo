
## class FrameTree::NodeIterator

### NodeIterator

NodeIterator::FrameTree::NodeIterator
~~~cpp
NodeIterator(const NodeIterator& other)
~~~

### ~NodeIterator

NodeIterator::FrameTree::~NodeIterator
~~~cpp
~NodeIterator()
~~~

### operator++

NodeIterator::FrameTree::operator++
~~~cpp
NodeIterator& operator++();
~~~

### AdvanceSkippingChildren

NodeIterator::FrameTree::AdvanceSkippingChildren
~~~cpp
NodeIterator& AdvanceSkippingChildren();
~~~
 Advances the iterator and excludes the children of the current node
### operator==

NodeIterator::FrameTree::operator==
~~~cpp
bool operator==(const NodeIterator& rhs) const;
~~~

### operator!=

operator!=
~~~cpp
bool operator!=(const NodeIterator& rhs) const { return !(*this == rhs); }
~~~

### operator*

operator*
~~~cpp
FrameTreeNode* operator*() { return current_node_; }
~~~

### NodeIterator

NodeIterator::FrameTree::NodeIterator
~~~cpp
NodeIterator(const std::vector<FrameTreeNode*>& starting_nodes,
                 const FrameTreeNode* root_of_subtree_to_skip,
                 bool should_descend_into_inner_trees,
                 bool include_delegate_nodes_for_inner_frame_trees)
~~~

### AdvanceNode

NodeIterator::FrameTree::AdvanceNode
~~~cpp
void AdvanceNode();
~~~

## class FrameTree::NodeRange

### ~NodeRange

NodeRange::FrameTree::~NodeRange
~~~cpp
~NodeRange()
~~~

### begin

NodeRange::FrameTree::begin
~~~cpp
NodeIterator begin();
~~~

### end

NodeRange::FrameTree::end
~~~cpp
NodeIterator end();
~~~

### NodeRange

NodeRange::FrameTree::NodeRange
~~~cpp
NodeRange(const std::vector<FrameTreeNode*>& starting_nodes,
              const FrameTreeNode* root_of_subtree_to_skip,
              bool should_descend_into_inner_trees,
              bool include_delegate_nodes_for_inner_frame_trees)
~~~

## class FrameTree::Delegate

### DidStopLoading

Delegate::FrameTree::DidStopLoading
~~~cpp
virtual void DidStopLoading() = 0;
~~~
 This is called when all nodes in the FrameTree stopped loading. This
 corresponds to the browser UI stop showing a spinner or other visual
 indicator for loading.

### LoadingTree

Delegate::FrameTree::LoadingTree
~~~cpp
virtual FrameTree* LoadingTree() = 0;
~~~
 Returns the delegate's top loading tree, which should be used to infer
 the values of loading-related states. The state of
 IsLoadingIncludingInnerFrameTrees() is a WebContents level concept and
 LoadingTree would return the frame tree to which loading events should be
 directed.


 TODO(crbug.com/1261928): Remove this method and directly rely on
 GetOutermostMainFrame() once portals and guest views are migrated to
 MPArch.

### IsHidden

Delegate::FrameTree::IsHidden
~~~cpp
virtual bool IsHidden() = 0;
~~~
 Returns true when the active RenderWidgetHostView should be hidden.

### GetOuterDelegateFrameTreeNodeId

Delegate::FrameTree::GetOuterDelegateFrameTreeNodeId
~~~cpp
virtual int GetOuterDelegateFrameTreeNodeId() = 0;
~~~
 If the FrameTree using this delegate is an inner/nested FrameTree, then
 there may be a FrameTreeNode in the outer FrameTree that is considered
 our outer delegate FrameTreeNode. This method returns the outer delegate
 FrameTreeNode ID if one exists. If we don't have a an outer delegate
 FrameTreeNode, this method returns
 FrameTreeNode::kFrameTreeNodeInvalidId.

### GetProspectiveOuterDocument

Delegate::FrameTree::GetProspectiveOuterDocument
~~~cpp
virtual RenderFrameHostImpl* GetProspectiveOuterDocument() = 0;
~~~
 If the FrameTree using this delegate is an inner/nested FrameTree that
 has not yet been attached to an outer FrameTreeNode, returns the parent
 RenderFrameHost of the intended outer FrameTreeNode to which the inner
 frame tree will be attached. This is usually the RenderFrameHost that is
 the outer document once attachment occurs, however in the case of some
 kinds of GuestView, the outer document may end up being a same-origin
 subframe of the RenderFrameHost returned by this method (see the
 `testNewWindowAttachInSubFrame` webview test for an example of this).

 Otherwise, returns null.

### IsPortal

Delegate::FrameTree::IsPortal
~~~cpp
virtual bool IsPortal() = 0;
~~~
 Returns if this FrameTree represents a portal.

### SetFocusedFrame

Delegate::FrameTree::SetFocusedFrame
~~~cpp
virtual void SetFocusedFrame(FrameTreeNode* node,
                                 SiteInstanceGroup* source) = 0;
~~~
 Set the `node` frame as focused in its own FrameTree as well as possibly
 changing the focused frame tree in the case of inner/outer FrameTrees.

## class FrameTree
 Represents the frame tree for a page. With the exception of the main frame,
 all FrameTreeNodes will be created/deleted in response to frame attach and
 detach events in the DOM.


 The main frame's FrameTreeNode is special in that it is reused. This allows
 it to serve as an anchor for state that needs to persist across top-level
 page navigations.


 TODO(ajwong): Move NavigationController ownership to the main frame
 FrameTreeNode. Possibly expose access to it from here.


 This object is only used on the UI thread.

### FrameTree

FrameTree::FrameTree
~~~cpp
FrameTree(BrowserContext* browser_context,
            Delegate* delegate,
            NavigationControllerDelegate* navigation_controller_delegate,
            NavigatorDelegate* navigator_delegate,
            RenderFrameHostDelegate* render_frame_delegate,
            RenderViewHostDelegate* render_view_delegate,
            RenderWidgetHostDelegate* render_widget_delegate,
            RenderFrameHostManager::Delegate* manager_delegate,
            PageDelegate* page_delegate,
            Type type)
~~~

### FrameTree

FrameTree
~~~cpp
FrameTree(const FrameTree&) = delete;
~~~

### operator=

FrameTree::operator=
~~~cpp
FrameTree& operator=(const FrameTree&) = delete;

  ~FrameTree();
~~~

### Init

FrameTree::Init
~~~cpp
void Init(SiteInstanceImpl* main_frame_site_instance,
            bool renderer_initiated_creation,
            const std::string& main_frame_name,
            RenderFrameHostImpl* opener_for_origin,
            const blink::FramePolicy& frame_policy,
            const base::UnguessableToken& devtools_frame_token);
~~~
 Initializes the main frame for this FrameTree. That is it creates the
 initial `RenderFrameHost` in the root node's RenderFrameHostManager, and also
 creates an initial `NavigationEntry` that potentially inherits
 `opener_for_origin`'s origin in its NavigationController. This method will
 call back into the delegates so it should only be called once they have
 completed their initialization. Pass in `frame_policy` so that it can be set
 in the root node's replication_state.

 TODO(carlscab): It would be great if initialization could happened in the
 constructor so we do not leave objects in a half initialized state.

### type

type
~~~cpp
Type type() const { return type_; }
~~~

### root

root
~~~cpp
FrameTreeNode* root() { return &root_; }
~~~

### root

root
~~~cpp
const FrameTreeNode* root() const { return &root_; }
~~~

### is_prerendering

is_prerendering
~~~cpp
bool is_prerendering() const { return type_ == FrameTree::Type::kPrerender; }
~~~

### IsPortal

FrameTree::IsPortal
~~~cpp
bool IsPortal();
~~~
 Returns true if this frame tree is a portal.


 TODO(crbug.com/1254770): Once portals are migrated to MPArch, portals will
 have their own FrameTree::Type.

### delegate

delegate
~~~cpp
Delegate* delegate() { return delegate_; }
~~~

### render_frame_delegate

render_frame_delegate
~~~cpp
RenderFrameHostDelegate* render_frame_delegate() {
    return render_frame_delegate_;
  }
~~~
 Delegates for various objects.  These can be kept centrally on the
 FrameTree because they are expected to be the same for all frames on a
 given FrameTree.

### render_view_delegate

render_view_delegate
~~~cpp
RenderViewHostDelegate* render_view_delegate() {
    return render_view_delegate_;
  }
~~~

### render_widget_delegate

render_widget_delegate
~~~cpp
RenderWidgetHostDelegate* render_widget_delegate() {
    return render_widget_delegate_;
  }
~~~

### manager_delegate

manager_delegate
~~~cpp
RenderFrameHostManager::Delegate* manager_delegate() {
    return manager_delegate_;
  }
~~~

### page_delegate

page_delegate
~~~cpp
PageDelegate* page_delegate() { return page_delegate_; }
~~~

### ForEachRenderViewHost

FrameTree::ForEachRenderViewHost
~~~cpp
void ForEachRenderViewHost(
      base::FunctionRef<void(RenderViewHostImpl*)> on_host);
~~~
 Iterate over all RenderViewHosts, including speculative RenderViewHosts.

 See `speculative_render_view_host_` for more details.

### speculative_render_view_host

speculative_render_view_host
~~~cpp
RenderViewHostImpl* speculative_render_view_host() const {
    return speculative_render_view_host_.get();
  }
~~~
 Speculative RenderViewHost accessors.

### set_speculative_render_view_host

set_speculative_render_view_host
~~~cpp
void set_speculative_render_view_host(
      base::WeakPtr<RenderViewHostImpl> render_view_host) {
    speculative_render_view_host_ = render_view_host;
  }
~~~

### MakeSpeculativeRVHCurrent

FrameTree::MakeSpeculativeRVHCurrent
~~~cpp
void MakeSpeculativeRVHCurrent();
~~~
 Moves `speculative_render_view_host_` to `render_view_host_map_`. This
 should be called every time a main-frame same-SiteInstanceGroup speculative
 RenderFrameHost gets swapped in and becomes the active RenderFrameHost.

 This overwrites the previous RenderViewHost for the SiteInstanceGroup in
 `render_view_host_map_`, if one exists.

### FindByID

FrameTree::FindByID
~~~cpp
FrameTreeNode* FindByID(int frame_tree_node_id);
~~~
 Returns the FrameTreeNode with the given |frame_tree_node_id| if it is part
 of this FrameTree.

### FindByRoutingID

FrameTree::FindByRoutingID
~~~cpp
FrameTreeNode* FindByRoutingID(int process_id, int routing_id);
~~~
 Returns the FrameTreeNode with the given renderer-specific |routing_id|.

### FindByName

FrameTree::FindByName
~~~cpp
FrameTreeNode* FindByName(const std::string& name);
~~~
 Returns the first frame in this tree with the given |name|, or the main
 frame if |name| is empty.

 Note that this does NOT support pseudo-names like _self, _top, and _blank,
 nor searching other FrameTrees (unlike blink::WebView::findFrameByName).

### Nodes

FrameTree::Nodes
~~~cpp
NodeRange Nodes();
~~~
 Returns a range to iterate over all FrameTreeNodes in the frame tree in
 breadth-first traversal order.

### SubtreeNodes

FrameTree::SubtreeNodes
~~~cpp
NodeRange SubtreeNodes(FrameTreeNode* subtree_root);
~~~
 Returns a range to iterate over all FrameTreeNodes in a subtree of the
 frame tree, starting from |subtree_root|.

### NodesIncludingInnerTreeNodes

FrameTree::NodesIncludingInnerTreeNodes
~~~cpp
NodeRange NodesIncludingInnerTreeNodes();
~~~
 Returns a range to iterate over all FrameTreeNodes in this frame tree, as
 well as any FrameTreeNodes of inner frame trees. Note that this includes
 inner frame trees of inner WebContents as well.

### SubtreeAndInnerTreeNodes

FrameTree::SubtreeAndInnerTreeNodes
~~~cpp
static NodeRange SubtreeAndInnerTreeNodes(
      RenderFrameHostImpl* parent,
      bool include_delegate_nodes_for_inner_frame_trees = false);
~~~
 Returns a range to iterate over all FrameTreeNodes in a subtree, starting
 from, but not including |parent|, as well as any FrameTreeNodes of inner
 frame trees. Note that this includes inner frame trees of inner WebContents
 as well. If `include_delegate_nodes_for_inner_frame_trees` is true the
 delegate RenderFrameHost owning the inner frame tree will also be returned.

### AddFrame

FrameTree::AddFrame
~~~cpp
FrameTreeNode* AddFrame(
      RenderFrameHostImpl* parent,
      int process_id,
      int new_routing_id,
      mojo::PendingAssociatedRemote<mojom::Frame> frame_remote,
      mojo::PendingReceiver<blink::mojom::BrowserInterfaceBroker>
          browser_interface_broker_receiver,
      blink::mojom::PolicyContainerBindParamsPtr policy_container_bind_params,
      mojo::PendingAssociatedReceiver<blink::mojom::AssociatedInterfaceProvider>
          associated_interface_provider_receiver,
      blink::mojom::TreeScopeType scope,
      const std::string& frame_name,
      const std::string& frame_unique_name,
      bool is_created_by_script,
      const blink::LocalFrameToken& frame_token,
      const base::UnguessableToken& devtools_frame_token,
      const blink::DocumentToken& document_token,
      const blink::FramePolicy& frame_policy,
      const blink::mojom::FrameOwnerProperties& frame_owner_properties,
      bool was_discarded,
      blink::FrameOwnerElementType owner_type,
      bool is_dummy_frame_for_inner_tree);
~~~
 Adds a new child frame to the frame tree. |process_id| is required to
 disambiguate |new_routing_id|, and it must match the process of the
 |parent| node. Otherwise no child is added and this method returns nullptr.

 |interface_provider_receiver| is the receiver end of the InterfaceProvider
 interface through which the child RenderFrame can access Mojo services
 exposed by the corresponding RenderFrameHost. The caller takes care of
 sending the client end of the interface down to the
 RenderFrame. |policy_container_bind_params|, if not null, is used for
 binding Blink's policy container to the new RenderFrameHost's
 PolicyContainerHost. This is only needed if this frame is the result of the
 CreateChildFrame mojo call, which also delivers the
 |policy_container_bind_params|. |is_dummy_frame_for_inner_tree| is true if
 the added frame is only to serve as a placeholder for an inner frame tree
 (e.g. fenced frames, portals) and will not have a live RenderFrame of its
 own.

### RemoveFrame

FrameTree::RemoveFrame
~~~cpp
void RemoveFrame(FrameTreeNode* child);
~~~
 Removes a frame from the frame tree. |child|, its children, and objects
 owned by their RenderFrameHostManagers are immediately deleted. The root
 node cannot be removed this way.

### CreateProxiesForSiteInstance

FrameTree::CreateProxiesForSiteInstance
~~~cpp
void CreateProxiesForSiteInstance(FrameTreeNode* source,
                                    SiteInstanceImpl* site_instance,
                                    const scoped_refptr<BrowsingContextState>&
                                        source_new_browsing_context_state);
~~~
 This method walks the entire frame tree and creates a RenderFrameProxyHost
 for the given |site_instance| in each node except the |source| one --
 the source will have a RenderFrameHost.  |source| may be null if there is
 no node navigating in this frame tree (such as when this is called
 for an opener's frame tree), in which case no nodes are skipped for
 RenderFrameProxyHost creation. |source_new_browsing_context_state| is the
 BrowsingContextState used by the speculative frame host, which may differ
 from the BrowsingContextState in |source| during cross-origin cross-
 browsing-instance navigations.

### GetMainFrame

FrameTree::GetMainFrame
~~~cpp
RenderFrameHostImpl* GetMainFrame() const;
~~~
 Convenience accessor for the main frame's RenderFrameHostImpl.

### GetFocusedFrame

FrameTree::GetFocusedFrame
~~~cpp
FrameTreeNode* GetFocusedFrame();
~~~
 Returns the focused frame.

### SetFocusedFrame

FrameTree::SetFocusedFrame
~~~cpp
void SetFocusedFrame(FrameTreeNode* node, SiteInstanceGroup* source);
~~~
 Sets the focused frame to |node|.  |source| identifies the
 SiteInstanceGroup that initiated this focus change.  If this FrameTree has
 SiteInstanceGroups other than |source|, those SiteInstanceGroups will be
 notified about the new focused frame.   Note that |source| may differ from
 |node|'s current SiteInstanceGroup (e.g., this happens for cross-process
 window.focus() calls).

### CreateRenderViewHost

FrameTree::CreateRenderViewHost
~~~cpp
scoped_refptr<RenderViewHostImpl> CreateRenderViewHost(
      SiteInstanceImpl* site_instance,
      int32_t main_frame_routing_id,
      bool renderer_initiated_creation,
      scoped_refptr<BrowsingContextState> main_browsing_context_state,
      CreateRenderViewHostCase create_case);
~~~
 Creates a RenderViewHostImpl for a given |site_instance| in the tree.


 The RenderFrameHostImpls and the RenderFrameProxyHosts will share ownership
 of this object.

 `create_case` indicates whether or not the RenderViewHost being created is
 speculative or not. It should only be registered with the FrameTree if it
 is not speculative.

### GetRenderViewHost

FrameTree::GetRenderViewHost
~~~cpp
scoped_refptr<RenderViewHostImpl> GetRenderViewHost(SiteInstanceGroup* group);
~~~
 Returns the existing RenderViewHost for a new RenderFrameHost.

 There should always be such a RenderViewHost, because the main frame
 RenderFrameHost for each SiteInstance should be created before subframes.

 Note that this will never return `speculative_render_view_host_`. If that
 is needed, call `speculative_render_view_host()` instead.

### GetRenderViewHostMapId

FrameTree::GetRenderViewHostMapId
~~~cpp
RenderViewHostMapId GetRenderViewHostMapId(
      SiteInstanceGroup* site_instance_group) const;
~~~
 Returns the ID used for the RenderViewHost associated with
 |site_instance_group|.

### RegisterRenderViewHost

FrameTree::RegisterRenderViewHost
~~~cpp
void RegisterRenderViewHost(RenderViewHostMapId id, RenderViewHostImpl* rvh);
~~~
 Registers a RenderViewHost so that it can be reused by other frames
 whose SiteInstance maps to the same RenderViewHostMapId.


 This method does not take ownership of|rvh|.


 NOTE: This method CHECK fails if a RenderViewHost is already registered for
 |rvh|'s SiteInstance.


 ALSO NOTE: After calling RegisterRenderViewHost, UnregisterRenderViewHost
 *must* be called for |rvh| when it is destroyed or put into the
 BackForwardCache, to prevent FrameTree::CreateRenderViewHost from trying to
 reuse it.

### UnregisterRenderViewHost

FrameTree::UnregisterRenderViewHost
~~~cpp
void UnregisterRenderViewHost(RenderViewHostMapId id,
                                RenderViewHostImpl* render_view_host);
~~~
 Unregisters the RenderViewHostImpl that's available for reuse for a
 particular RenderViewHostMapId. NOTE: This method CHECK fails if it is
 called for a |render_view_host| that is not currently set for reuse.

### FrameUnloading

FrameTree::FrameUnloading
~~~cpp
void FrameUnloading(FrameTreeNode* frame);
~~~
 This is called when the frame is about to be removed and started to run
 unload handlers.

### FrameRemoved

FrameTree::FrameRemoved
~~~cpp
void FrameRemoved(FrameTreeNode* frame);
~~~
 This is only meant to be called by FrameTreeNode. Triggers calling
 the listener installed by SetFrameRemoveListener.

### DidStartLoadingNode

FrameTree::DidStartLoadingNode
~~~cpp
void DidStartLoadingNode(FrameTreeNode& node,
                           bool should_show_loading_ui,
                           bool was_previously_loading);
~~~

### DidStopLoadingNode

FrameTree::DidStopLoadingNode
~~~cpp
void DidStopLoadingNode(FrameTreeNode& node);
~~~

### DidCancelLoading

FrameTree::DidCancelLoading
~~~cpp
void DidCancelLoading();
~~~

### GetLoadProgress

FrameTree::GetLoadProgress
~~~cpp
double GetLoadProgress();
~~~
 Returns this FrameTree's total load progress. If the `root_` FrameTreeNode
 is navigating returns `blink::kInitialLoadProgress`.

### IsLoadingIncludingInnerFrameTrees

FrameTree::IsLoadingIncludingInnerFrameTrees
~~~cpp
bool IsLoadingIncludingInnerFrameTrees() const;
~~~
 Returns true if at least one of the nodes in this frame tree or nodes in
 any inner frame tree of the same WebContents is loading.

### ReplicatePageFocus

FrameTree::ReplicatePageFocus
~~~cpp
void ReplicatePageFocus(bool is_focused);
~~~
 Set page-level focus in all SiteInstances involved in rendering
 this FrameTree, not including the current main frame's
 SiteInstance. The focus update will be sent via the main frame's proxies
 in those SiteInstances.

### SetPageFocus

FrameTree::SetPageFocus
~~~cpp
void SetPageFocus(SiteInstanceGroup* group, bool is_focused);
~~~
 Updates page-level focus for this FrameTree in the subframe renderer
 identified by |group|.

### RegisterExistingOriginAsHavingDefaultIsolation

FrameTree::RegisterExistingOriginAsHavingDefaultIsolation
~~~cpp
void RegisterExistingOriginAsHavingDefaultIsolation(
      const url::Origin& previously_visited_origin,
      NavigationRequest* navigation_request_to_exclude);
~~~
 Walks the current frame tree and registers any origins matching
 `previously_visited_origin`, either the last committed origin of a
 RenderFrameHost or the origin associated with a NavigationRequest that has
 been assigned to a SiteInstance, as having the default origin isolation
 state. This is only necessary when `previously_visited_origin` is seen with
 an OriginAgentCluster header explicitly requesting something other than the
 default.

### controller

controller
~~~cpp
NavigationControllerImpl& controller() { return navigator_.controller(); }
~~~

### navigator

navigator
~~~cpp
Navigator& navigator() { return navigator_; }
~~~

### DidAccessInitialMainDocument

FrameTree::DidAccessInitialMainDocument
~~~cpp
void DidAccessInitialMainDocument();
~~~
 Another page accessed the initial empty main document, which means it
 is no longer safe to display a pending URL without risking a URL spoof.

### has_accessed_initial_main_document

has_accessed_initial_main_document
~~~cpp
bool has_accessed_initial_main_document() const {
    return has_accessed_initial_main_document_;
  }
~~~

### ResetHasAccessedInitialMainDocument

ResetHasAccessedInitialMainDocument
~~~cpp
void ResetHasAccessedInitialMainDocument() {
    has_accessed_initial_main_document_ = false;
  }
~~~

### IsHidden

IsHidden
~~~cpp
bool IsHidden() const { return delegate_->IsHidden(); }
~~~

### LoadingTree

FrameTree::LoadingTree
~~~cpp
FrameTree* LoadingTree();
~~~
 LoadingTree returns the following for different frame trees to direct
 loading related events. Please see FrameTree::Delegate::LoadingTree for
 more comments.

 - For prerender frame tree -> returns the frame tree itself.

 - For fenced frame and primary frame tree (including portal) -> returns
 the delegate's primary frame tree.

### StopLoading

FrameTree::StopLoading
~~~cpp
void StopLoading();
~~~
 Stops all ongoing navigations in each of the nodes of this FrameTree.

### Shutdown

FrameTree::Shutdown
~~~cpp
void Shutdown();
~~~
 Prepares this frame tree for destruction, cleaning up the internal state
 and firing the appropriate events like FrameDeleted.

 Must be called before FrameTree is destroyed.

### IsBeingDestroyed

IsBeingDestroyed
~~~cpp
bool IsBeingDestroyed() const { return is_being_destroyed_; }
~~~

### GetSafeRef

FrameTree::GetSafeRef
~~~cpp
base::SafeRef<FrameTree> GetSafeRef();
~~~

### FocusOuterFrameTrees

FrameTree::FocusOuterFrameTrees
~~~cpp
void FocusOuterFrameTrees();
~~~
 Walks up the FrameTree chain and focuses the FrameTreeNode where
 each inner FrameTree is attached.

### RegisterOriginForUnpartitionedSessionStorageAccess

FrameTree::RegisterOriginForUnpartitionedSessionStorageAccess
~~~cpp
void RegisterOriginForUnpartitionedSessionStorageAccess(
      const url::Origin& origin);
~~~
 This should only be called by NavigationRequest when it detects that an
 origin is participating in the deprecation trial.


 TODO(crbug.com/1407150): Remove this when deprecation trial is complete.

### UnregisterOriginForUnpartitionedSessionStorageAccess

FrameTree::UnregisterOriginForUnpartitionedSessionStorageAccess
~~~cpp
void UnregisterOriginForUnpartitionedSessionStorageAccess(
      const url::Origin& origin);
~~~
 This should only be called by NavigationRequest when it detects that an
 origin is not participating in the deprecation trial.


 TODO(crbug.com/1407150): Remove this when deprecation trial is complete.

### GetSessionStorageKey

FrameTree::GetSessionStorageKey
~~~cpp
const blink::StorageKey GetSessionStorageKey(
      const blink::StorageKey& storage_key);
~~~
 This should be used for all session storage related bindings as it adjusts
 the storage key used depending on the deprecation trial.


 TODO(crbug.com/1407150): Remove this when deprecation trial is complete.

### NodesExceptSubtree

FrameTree::NodesExceptSubtree
~~~cpp
NodeRange NodesExceptSubtree(FrameTreeNode* node);
~~~
 Returns a range to iterate over all FrameTreeNodes in the frame tree in
 breadth-first traversal order, skipping the subtree rooted at
 |node|, but including |node| itself.

### CollectNodesForIsLoading

FrameTree::CollectNodesForIsLoading
~~~cpp
std::vector<FrameTreeNode*> CollectNodesForIsLoading();
~~~
 Returns all FrameTreeNodes in this frame tree, as well as any
 FrameTreeNodes of inner frame trees. Note that this doesn't include inner
 frame trees of inner delegates. This is used to find the aggregate
 IsLoading value for a frame tree.


 TODO(crbug.com/1261928, crbug.com/1261928): Remove this method and directly
 rely on GetOutermostMainFrame() and NodesIncludingInnerTreeNodes() once
 portals and guest views are migrated to MPArch.
