
## class FrameTreeNode : public
 When a page contains iframes, its renderer process maintains a tree structure
 of those frames. We are mirroring this tree in the browser process. This
 class represents a node in this tree and is a wrapper for all objects that
 are frame-specific (as opposed to page-specific).


 Each FrameTreeNode has a current RenderFrameHost, which can change over
 time as the frame is navigated. Any immediate subframes of the current
 document are tracked using FrameTreeNodes owned by the current
 RenderFrameHost, rather than as children of FrameTreeNode itself. This
 allows subframe FrameTreeNodes to stay alive while a RenderFrameHost is
 still alive - for example while pending deletion, after a new current
 RenderFrameHost has replaced it.

### GloballyFindByID

FrameTreeNode : public::GloballyFindByID
~~~cpp
static FrameTreeNode* GloballyFindByID(int frame_tree_node_id);
~~~
 Returns the FrameTreeNode with the given global |frame_tree_node_id|,
 regardless of which FrameTree it is in.

### From

FrameTreeNode : public::From
~~~cpp
static FrameTreeNode* From(RenderFrameHost* rfh);
~~~
 Returns the FrameTreeNode for the given |rfh|. Same as
 rfh->frame_tree_node(), but also supports nullptrs.

### FrameTreeNode

FrameTreeNode : public::FrameTreeNode
~~~cpp
FrameTreeNode(
      FrameTree& frame_tree,
      RenderFrameHostImpl* parent,
      blink::mojom::TreeScopeType tree_scope_type,
      bool is_created_by_script,
      const blink::mojom::FrameOwnerProperties& frame_owner_properties,
      blink::FrameOwnerElementType owner_type,
      const blink::FramePolicy& frame_owner)
~~~

### FrameTreeNode

FrameTreeNode
~~~cpp
FrameTreeNode(const FrameTreeNode&) = delete;
~~~

### operator=

FrameTreeNode : public::operator=
~~~cpp
FrameTreeNode& operator=(const FrameTreeNode&) = delete;
~~~

### ~FrameTreeNode

FrameTreeNode : public::~FrameTreeNode
~~~cpp
~FrameTreeNode() override
~~~

### AddObserver

FrameTreeNode : public::AddObserver
~~~cpp
void AddObserver(Observer* observer);
~~~

### RemoveObserver

FrameTreeNode : public::RemoveObserver
~~~cpp
void RemoveObserver(Observer* observer);
~~~

### IsMainFrame

FrameTreeNode : public::IsMainFrame
~~~cpp
bool IsMainFrame() const;
~~~
 Frame trees may be nested so it can be the case that IsMainFrame() is true,
 but is not the outermost main frame. In particular, !IsMainFrame() cannot
 be used to check if the frame is an embedded frame -- use
 !IsOutermostMainFrame() instead. NB: this does not escape guest views;
 IsOutermostMainFrame will be true for the outermost main frame in an inner
 guest view.

### IsOutermostMainFrame

FrameTreeNode : public::IsOutermostMainFrame
~~~cpp
bool IsOutermostMainFrame() const;
~~~

### ResetForNavigation

FrameTreeNode : public::ResetForNavigation
~~~cpp
void ResetForNavigation();
~~~
 Clears any state in this node which was set by the document itself (CSP &
 UserActivationState) and notifies proxies as appropriate. Invoked after
 committing navigation to a new document (since the new document comes with
 a fresh set of CSP).

 TODO(arthursonzogni): Remove this function. The frame/document must not be
 left temporarily with lax state.

### frame_tree

frame_tree
~~~cpp
FrameTree& frame_tree() const { return frame_tree_.get(); }
~~~

### navigator

FrameTreeNode : public::navigator
~~~cpp
Navigator& navigator();
~~~

### render_manager

render_manager
~~~cpp
RenderFrameHostManager* render_manager() { return &render_manager_; }
~~~

### render_manager

render_manager
~~~cpp
const RenderFrameHostManager* render_manager() const {
    return &render_manager_;
  }
~~~

### frame_tree_node_id

frame_tree_node_id
~~~cpp
int frame_tree_node_id() const { return frame_tree_node_id_; }
~~~

### frame_name

frame_name
~~~cpp
const std::string& frame_name() const {
    return render_manager_.current_replication_state().name;
  }
~~~
 This reflects window.name, which is initially set to the the "name"
 attribute. But this won't reflect changes of 'name' attribute and instead
 reflect changes to the Window object's name property.

 This is different from IframeAttributes' name in that this will not get
 updated when 'name' attribute gets updated.

### unique_name

unique_name
~~~cpp
const std::string& unique_name() const {
    return render_manager_.current_replication_state().unique_name;
  }
~~~

### child_count

child_count
~~~cpp
size_t child_count() const { return current_frame_host()->child_count(); }
~~~

### parent

parent
~~~cpp
RenderFrameHostImpl* parent() const { return parent_; }
~~~

### GetParentOrOuterDocument

FrameTreeNode : public::GetParentOrOuterDocument
~~~cpp
RenderFrameHostImpl* GetParentOrOuterDocument() const;
~~~
 See `RenderFrameHost::GetParentOrOuterDocument()` for
 documentation.

### GetParentOrOuterDocumentOrEmbedder

FrameTreeNode : public::GetParentOrOuterDocumentOrEmbedder
~~~cpp
RenderFrameHostImpl* GetParentOrOuterDocumentOrEmbedder();
~~~
 See `RenderFrameHostImpl::GetParentOrOuterDocumentOrEmbedder()` for
 documentation.

### opener

opener
~~~cpp
FrameTreeNode* opener() const { return opener_; }
~~~

### first_live_main_frame_in_original_opener_chain

first_live_main_frame_in_original_opener_chain
~~~cpp
FrameTreeNode* first_live_main_frame_in_original_opener_chain() const {
    return first_live_main_frame_in_original_opener_chain_;
  }
~~~

### opener_devtools_frame_token

opener_devtools_frame_token
~~~cpp
const absl::optional<base::UnguessableToken>& opener_devtools_frame_token() {
    return opener_devtools_frame_token_;
  }
~~~

### GetFrameType

FrameTreeNode : public::GetFrameType
~~~cpp
FrameType GetFrameType() const;
~~~
 Returns the type of the frame. Refer to frame_type.h for the details.

### SetOpener

FrameTreeNode : public::SetOpener
~~~cpp
void SetOpener(FrameTreeNode* opener);
~~~
 Assigns a new opener for this node and, if |opener| is non-null, registers
 an observer that will clear this node's opener if |opener| is ever
 destroyed.

### SetOriginalOpener

FrameTreeNode : public::SetOriginalOpener
~~~cpp
void SetOriginalOpener(FrameTreeNode* opener);
~~~
 Assigns the initial opener for this node, and if |opener| is non-null,
 registers an observer that will clear this node's opener if |opener| is
 ever destroyed. The value set here is the root of the tree.


 It is not possible to change the opener once it was set.

### SetOpenerDevtoolsFrameToken

FrameTreeNode : public::SetOpenerDevtoolsFrameToken
~~~cpp
void SetOpenerDevtoolsFrameToken(
      base::UnguessableToken opener_devtools_frame_token);
~~~
 Assigns an opener frame id for this node. This string id is only set once
 and cannot be changed. It persists, even if the |opener| is destroyed. It
 is used for attribution in the DevTools frontend.

### child_at

child_at
~~~cpp
FrameTreeNode* child_at(size_t index) const {
    return current_frame_host()->child_at(index);
  }
~~~

### current_url

current_url
~~~cpp
const GURL& current_url() const {
    return current_frame_host()->GetLastCommittedURL();
  }
~~~
 Returns the URL of the last committed page in the current frame.

### is_on_initial_empty_document

is_on_initial_empty_document
~~~cpp
bool is_on_initial_empty_document() const {
    return current_frame_host()
               ? current_frame_host()->is_initial_empty_document()
               : true;
  }
~~~
 Note that the current RenderFrameHost might not exist yet when calling this
 during FrameTreeNode initialization. In this case the FrameTreeNode must be
 on the initial empty document. Refer RFHI::is_initial_empty_document for a
 more details.

### is_collapsed

is_collapsed
~~~cpp
bool is_collapsed() const { return is_collapsed_; }
~~~
 Returns whether the frame's owner element in the parent document is
 collapsed, that is, removed from the layout as if it did not exist, as per
 request by the embedder (of the content/ layer).

### SetCollapsed

FrameTreeNode : public::SetCollapsed
~~~cpp
void SetCollapsed(bool collapsed);
~~~
 Sets whether to collapse the frame's owner element in the parent document,
 that is, to remove it from the layout as if it did not exist, as per
 request by the embedder (of the content/ layer). Cannot be called for main
 frames.


 This only has an effect for <iframe> owner elements, and is a no-op when
 called on sub-frames hosted in <frame>, <object>, and <embed> elements.

### current_origin

current_origin
~~~cpp
const url::Origin& current_origin() const {
    return render_manager_.current_replication_state().origin;
  }
~~~
 Returns the origin of the last committed page in this frame.

 WARNING: To get the last committed origin for a particular
 RenderFrameHost, use RenderFrameHost::GetLastCommittedOrigin() instead,
 which will behave correctly even when the RenderFrameHost is not the
 current one for this frame (such as when it's pending deletion).

### pending_frame_policy

pending_frame_policy
~~~cpp
const blink::FramePolicy& pending_frame_policy() const {
    return pending_frame_policy_;
  }
~~~
 Returns the latest frame policy (sandbox flags and container policy) for
 this frame. This includes flags inherited from parent frames and the latest
 flags from the <iframe> element hosting this frame. The returned policies
 may not yet have taken effect, since "sandbox" and "allow" attribute
 updates in an <iframe> element take effect on next navigation. For
 <fencedframe> elements, not everything in the frame policy might actually
 take effect after the navigation. To retrieve the currently active policy
 for this frame, use effective_frame_policy().

### SetPendingFramePolicy

FrameTreeNode : public::SetPendingFramePolicy
~~~cpp
void SetPendingFramePolicy(blink::FramePolicy frame_policy);
~~~
 Update this frame's sandbox flags and container policy.  This is called
 when a parent frame updates the "sandbox" attribute in the <iframe> element
 for this frame, or any of the attributes which affect the container policy
 ("allowfullscreen", "allowpaymentrequest", "allow", and "src".)
 These policies won't take effect until next navigation.  If this frame's
 parent is itself sandboxed, the parent's sandbox flags are combined with
 those in |frame_policy|.

 Attempting to change the container policy on the main frame will have no
 effect.

### effective_frame_policy

effective_frame_policy
~~~cpp
const blink::FramePolicy& effective_frame_policy() const {
    return render_manager_.current_replication_state().frame_policy;
  }
~~~
 Returns the currently active frame policy for this frame, including the
 sandbox flags which were present at the time the document was loaded, and
 the permissions policy container policy, which is set by the iframe's
 allowfullscreen, allowpaymentrequest, and allow attributes, along with the
 origin of the iframe's src attribute (which may be different from the URL
 of the document currently loaded into the frame). This does not include
 policy changes that have been made by updating the containing iframe
 element attributes since the frame was last navigated; use
 pending_frame_policy() for those.

### frame_owner_properties

frame_owner_properties
~~~cpp
const blink::mojom::FrameOwnerProperties& frame_owner_properties() {
    return frame_owner_properties_;
  }
~~~

### set_frame_owner_properties

set_frame_owner_properties
~~~cpp
void set_frame_owner_properties(
      const blink::mojom::FrameOwnerProperties& frame_owner_properties) {
    frame_owner_properties_ = frame_owner_properties;
  }
~~~

### csp_attribute

csp_attribute
~~~cpp
const network::mojom::ContentSecurityPolicy* csp_attribute() const {
    return attributes_->parsed_csp_attribute.get();
  }
~~~
 Reflects the attributes of the corresponding iframe html element, such
 as 'credentialless', 'id', 'name' and 'src'. These values should not be
 exposed to cross-origin renderers.

### browsing_topics

browsing_topics
~~~cpp
bool browsing_topics() const { return attributes_->browsing_topics; }
~~~
 Tracks iframe's 'browsingtopics' attribute, indicating whether the
 navigation requests on this frame should calculate and send the
 `Sec-Browsing-Topics` header.

### html_id

html_id
~~~cpp
const absl::optional<std::string> html_id() const { return attributes_->id; }
~~~

### html_name

html_name
~~~cpp
const absl::optional<std::string> html_name() const {
    return attributes_->name;
  }
~~~
 This tracks iframe's 'name' attribute instead of window.name, which is
 tracked in FrameReplicationState. See the comment for frame_name() for
 more details.

### html_src

html_src
~~~cpp
const absl::optional<std::string> html_src() const {
    return attributes_->src;
  }
~~~

### SetAttributes

FrameTreeNode : public::SetAttributes
~~~cpp
void SetAttributes(blink::mojom::IframeAttributesPtr attributes);
~~~

### HasSameOrigin

HasSameOrigin
~~~cpp
bool HasSameOrigin(const FrameTreeNode& node) const {
    return render_manager_.current_replication_state().origin.IsSameOriginWith(
        node.current_replication_state().origin);
  }
~~~

### current_replication_state

current_replication_state
~~~cpp
const blink::mojom::FrameReplicationState& current_replication_state() const {
    return render_manager_.current_replication_state();
  }
~~~

### current_frame_host

current_frame_host
~~~cpp
RenderFrameHostImpl* current_frame_host() const {
    return render_manager_.current_frame_host();
  }
~~~

### IsLoading

FrameTreeNode : public::IsLoading
~~~cpp
bool IsLoading() const;
~~~
 Returns true if this node is in a loading state.

### HasPendingCrossDocumentNavigation

FrameTreeNode : public::HasPendingCrossDocumentNavigation
~~~cpp
bool HasPendingCrossDocumentNavigation() const;
~~~
 Returns true if this node has a cross-document navigation in progress.

### navigation_request

navigation_request
~~~cpp
NavigationRequest* navigation_request() { return navigation_request_.get(); }
~~~

### TransferNavigationRequestOwnership

FrameTreeNode : public::TransferNavigationRequestOwnership
~~~cpp
void TransferNavigationRequestOwnership(
      RenderFrameHostImpl* render_frame_host);
~~~
 Transfers the ownership of the NavigationRequest to |render_frame_host|.

 From ReadyToCommit to DidCommit, the NavigationRequest is owned by the
 RenderFrameHost that is committing the navigation.

### TakeNavigationRequest

FrameTreeNode : public::TakeNavigationRequest
~~~cpp
void TakeNavigationRequest(
      std::unique_ptr<NavigationRequest> navigation_request);
~~~
 Takes ownership of |navigation_request| and makes it the current
 NavigationRequest of this frame. This corresponds to the start of a new
 navigation. If there was an ongoing navigation request before calling this
 function, it is canceled. |navigation_request| should not be null.

### ResetNavigationRequest

FrameTreeNode : public::ResetNavigationRequest
~~~cpp
void ResetNavigationRequest(NavigationDiscardReason reason);
~~~
 Resets the navigation request owned by `this` (which shouldn't have reached
 the "pending commit" stage yet) and any state created by it, including the
 speculative RenderFrameHost (if there are no other navigations associated
 with it). Note that this does not affect navigations that have reached the
 "pending commit" stage, which are owned by their corresponding
 RenderFrameHosts instead.

### ResetNavigationRequestButKeepState

FrameTreeNode : public::ResetNavigationRequestButKeepState
~~~cpp
void ResetNavigationRequestButKeepState();
~~~
 Similar to `ResetNavigationRequest()`, but keeps the state created by the
 NavigationRequest (e.g. speculative RenderFrameHost, loading state).

### DidChangeLoadProgress

FrameTreeNode : public::DidChangeLoadProgress
~~~cpp
void DidChangeLoadProgress(double load_progress);
~~~
 The load progress for a RenderFrameHost in this node was updated to
 |load_progress|. This will notify the FrameTree which will in turn notify
 the WebContents.

### StopLoading

FrameTreeNode : public::StopLoading
~~~cpp
bool StopLoading();
~~~
 Called when the user directed the page to stop loading. Stops all loads
 happening in the FrameTreeNode. This method should be used with
 FrameTree::ForEach to stop all loads in the entire FrameTree.

### last_focus_time

last_focus_time
~~~cpp
base::TimeTicks last_focus_time() const { return last_focus_time_; }
~~~
 Returns the time this frame was last focused.

### DidFocus

FrameTreeNode : public::DidFocus
~~~cpp
void DidFocus();
~~~
 Called when this node becomes focused.  Updates the node's last focused
 time and notifies observers.

### BeforeUnloadCanceled

FrameTreeNode : public::BeforeUnloadCanceled
~~~cpp
void BeforeUnloadCanceled();
~~~
 Called when the user closed the modal dialogue for BeforeUnload and
 cancelled the navigation. This should stop any load happening in the
 FrameTreeNode.

### active_sandbox_flags

active_sandbox_flags
~~~cpp
network::mojom::WebSandboxFlags active_sandbox_flags() const {
    return render_manager_.current_replication_state().active_sandbox_flags;
  }
~~~
 Returns the sandbox flags currently in effect for this frame. This includes
 flags inherited from parent frames, the currently active flags from the
 <iframe> element hosting this frame, as well as any flags set from a
 Content-Security-Policy HTTP header. This does not include flags that have
 have been updated in an <iframe> element but have not taken effect yet; use
 pending_frame_policy() for those. To see the flags which will take effect
 on navigation (which does not include the CSP-set flags), use
 effective_frame_policy().

### has_received_user_gesture_before_nav

has_received_user_gesture_before_nav
~~~cpp
bool has_received_user_gesture_before_nav() const {
    return render_manager_.current_replication_state()
        .has_received_user_gesture_before_nav;
  }
~~~
 Returns whether the frame received a user gesture on a previous navigation
 on the same eTLD+1.

### set_was_discarded

set_was_discarded
~~~cpp
void set_was_discarded() { was_discarded_ = true; }
~~~
 When a tab is discarded, WebContents sets was_discarded on its
 root FrameTreeNode.

 In addition, when a child frame is created, this bit is passed on from
 parent to child.

 When a navigation request is created, was_discarded is passed on to the
 request and reset to false in FrameTreeNode.

### was_discarded

was_discarded
~~~cpp
bool was_discarded() const { return was_discarded_; }
~~~

### HasStickyUserActivation

HasStickyUserActivation
~~~cpp
bool HasStickyUserActivation() const {
    return current_frame_host()->HasStickyUserActivation();
  }
~~~
 Deprecated. Use directly HasStickyUserActivation in RFHI.

 Returns the sticky bit of the User Activation v2 state of the
 |FrameTreeNode|.

### HasTransientUserActivation

HasTransientUserActivation
~~~cpp
bool HasTransientUserActivation() {
    return current_frame_host()->HasTransientUserActivation();
  }
~~~
 Deprecated. Use directly HasStickyUserActivation in RFHI.

 Returns the transient bit of the User Activation v2 state of the
 |FrameTreeNode|.

### PruneChildFrameNavigationEntries

FrameTreeNode : public::PruneChildFrameNavigationEntries
~~~cpp
void PruneChildFrameNavigationEntries(NavigationEntryImpl* entry);
~~~
 Remove history entries for all frames created by script in this frame's
 subtree. If a frame created by a script is removed, then its history entry
 will never be reused - this saves memory.

### fenced_frame_status

fenced_frame_status
~~~cpp
FencedFrameStatus fenced_frame_status() const { return fenced_frame_status_; }
~~~

### frame_owner_element_type

frame_owner_element_type
~~~cpp
blink::FrameOwnerElementType frame_owner_element_type() const {
    return frame_owner_element_type_;
  }
~~~

### tree_scope_type

tree_scope_type
~~~cpp
blink::mojom::TreeScopeType tree_scope_type() const {
    return tree_scope_type_;
  }
~~~

### SetInitialPopupURL

FrameTreeNode : public::SetInitialPopupURL
~~~cpp
void SetInitialPopupURL(const GURL& initial_popup_url);
~~~
 The initial popup URL for new window opened using:
 `window.open(initial_popup_url)`.

 An empty GURL otherwise.


 [WARNING] There is no guarantee the FrameTreeNode will ever host a
 document served from this URL. The FrameTreeNode always starts hosting the
 initial empty document and attempts a navigation toward this URL. However
 the navigation might be delayed, redirected and even cancelled.

### initial_popup_url

initial_popup_url
~~~cpp
const GURL& initial_popup_url() const { return initial_popup_url_; }
~~~

### SetPopupCreatorOrigin

FrameTreeNode : public::SetPopupCreatorOrigin
~~~cpp
void SetPopupCreatorOrigin(const url::Origin& popup_creator_origin);
~~~
 The origin of the document that used window.open() to create this frame.

 Otherwise, an opaque Origin with a nonce different from all previously
 existing Origins.

### popup_creator_origin

popup_creator_origin
~~~cpp
const url::Origin& popup_creator_origin() const {
    return popup_creator_origin_;
  }
~~~

### SetFrameTree

FrameTreeNode : public::SetFrameTree
~~~cpp
void SetFrameTree(FrameTree& frame_tree);
~~~
 Sets the associated FrameTree for this node. The node can change FrameTrees
 when blink::features::Prerender2 is enabled, which allows a page loaded in
 the prerendered FrameTree to be used for a navigation in the primary frame
 tree.

### WriteIntoTrace

FrameTreeNode : public::WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedProto<TraceProto> proto) const;
~~~
 Write a representation of this object into a trace.

### HasNavigation

FrameTreeNode : public::HasNavigation
~~~cpp
bool HasNavigation();
~~~
 Returns true the node is navigating, i.e. it has an associated
 NavigationRequest.

### IsFencedFrameRoot

FrameTreeNode : public::IsFencedFrameRoot
~~~cpp
bool IsFencedFrameRoot() const;
~~~
 Fenced frames (meta-bug crbug.com/1111084):
 Note that these two functions cannot be invoked from a FrameTree's or
 its root node's constructor since they require the frame tree and the
 root node to be completely constructed.


 Returns false if fenced frames are disabled. Returns true if the feature is
 enabled and if |this| is a fenced frame. Returns false for
 iframes embedded in a fenced frame. To clarify: for the MPArch
 implementation this only returns true if |this| is the actual
 root node of the inner FrameTree and not the proxy FrameTreeNode in the
 outer FrameTree.

### IsInFencedFrameTree

FrameTreeNode : public::IsInFencedFrameTree
~~~cpp
bool IsInFencedFrameTree() const;
~~~
 Returns false if fenced frames are disabled. Returns true if the
 feature is enabled and if |this| or any of its ancestor nodes is a
 fenced frame.

### GetFencedFrameNonce

FrameTreeNode : public::GetFencedFrameNonce
~~~cpp
absl::optional<base::UnguessableToken> GetFencedFrameNonce();
~~~
 Returns a valid nonce if `IsInFencedFrameTree()` returns true for `this`.

 Returns nullopt otherwise.


 Nonce used in the net::IsolationInfo and blink::StorageKey for a fenced
 frame and any iframes nested within it. Not set if this frame is not in a
 fenced frame's FrameTree. Note that this could be a field in FrameTree for
 the MPArch version but for the shadow DOM version we need to keep it here
 since the fenced frame root is not a main frame for the latter. The value
 of the nonce will be the same for all of the the iframes inside a fenced
 frame tree. If there is a nested fenced frame it will have a different
 nonce than its parent fenced frame. The nonce will stay the same across
 navigations initiated from the fenced frame tree because it is always used
 in conjunction with other fields of the keys and would be good to access
 the same storage across same-origin navigations. If the navigation is
 same-origin/site then the same network stack partition/storage will be
 reused and if it's cross-origin/site then other parts of the key will
 change and so, even with the same nonce, another partition will be used.

 But if the navigation is initiated from the embedder, the nonce will be
 reinitialized irrespective of same or cross origin such that there is no
 privacy leak via storage shared between two embedder initiated navigations.

 Note that this reinitialization is implemented for all embedder-initiated
 navigations in MPArch, but only urn:uuid navigations in ShadowDOM.

### SetFencedFramePropertiesIfNeeded

FrameTreeNode : public::SetFencedFramePropertiesIfNeeded
~~~cpp
void SetFencedFramePropertiesIfNeeded();
~~~
 If applicable, initialize the default fenced frame properties. Right now,
 this means setting a new fenced frame nonce. See comment on
 fenced_frame_nonce() for when it is set to a non-null value. Invoked
 by FrameTree::Init() or FrameTree::AddFrame().

### SetFencedFramePropertiesOpaqueAdsModeForTesting

SetFencedFramePropertiesOpaqueAdsModeForTesting
~~~cpp
void SetFencedFramePropertiesOpaqueAdsModeForTesting() {
    if (fenced_frame_properties_.has_value()) {
      fenced_frame_properties_->mode_ =
          blink::FencedFrame::DeprecatedFencedFrameMode::kOpaqueAds;
    }
  }
~~~
 Set the current FencedFrameProperties to have "opaque ads mode".

 This should only be used during tests, when the proper embedder-initiated
 fenced frame root urn/config navigation flow isn't available.

 TODO(crbug.com/1347953): Refactor and expand use of test utils so there is
 a consistent way to do this properly everywhere. Consider removing
 arbitrary restrictions in "default mode" so that using opaque ads mode is
 less necessary.

### GetDeprecatedFencedFrameMode

FrameTreeNode : public::GetDeprecatedFencedFrameMode
~~~cpp
blink::FencedFrame::DeprecatedFencedFrameMode GetDeprecatedFencedFrameMode();
~~~
 Returns the mode attribute from the `FencedFrameProperties` if this frame
 is in a fenced frame tree, otherwise returns `kDefault`.

### GetParentOrOuterDocumentHelper

FrameTreeNode : public::GetParentOrOuterDocumentHelper
~~~cpp
RenderFrameHostImpl* GetParentOrOuterDocumentHelper(
      bool escape_guest_view,
      bool include_prospective) const;
~~~
 Helper for GetParentOrOuterDocument/GetParentOrOuterDocumentOrEmbedder.

 Do not use directly.

 `escape_guest_view` determines whether to iterate out of guest views and is
 the behaviour distinction between GetParentOrOuterDocument and
 GetParentOrOuterDocumentOrEmbedder. See the comment on
 GetParentOrOuterDocumentOrEmbedder for details.

 `include_prospective` includes embedders which own our frame tree, but have
 not yet attached it to the outer frame tree.

### set_frame_name_for_activation

set_frame_name_for_activation
~~~cpp
void set_frame_name_for_activation(const std::string& unique_name,
                                     const std::string& name) {
    current_frame_host()->browsing_context_state()->set_frame_name(unique_name,
                                                                   name);
  }
~~~
 Sets the unique_name and name fields on replication_state_. To be used in
 prerender activation to make sure the FrameTreeNode replication state is
 correct after the RenderFrameHost is moved between FrameTreeNodes. The
 renderers should already have the correct value, so unlike
 FrameTreeNode::SetFrameName, we do not notify them here.

 TODO(https://crbug.com/1237091): Remove this once the BrowsingContextState
  is implemented to utilize the new path.

### IsErrorPageIsolationEnabled

FrameTreeNode : public::IsErrorPageIsolationEnabled
~~~cpp
bool IsErrorPageIsolationEnabled() const;
~~~
 Returns true if error page isolation is enabled.

### SetSrcdocValue

FrameTreeNode : public::SetSrcdocValue
~~~cpp
void SetSrcdocValue(const std::string& srcdoc_value);
~~~
 Functions to store and retrieve a frame's srcdoc value on this
 FrameTreeNode.

### srcdoc_value

srcdoc_value
~~~cpp
const std::string& srcdoc_value() const { return srcdoc_value_; }
~~~

### set_fenced_frame_properties

set_fenced_frame_properties
~~~cpp
void set_fenced_frame_properties(
      const absl::optional<FencedFrameProperties>& fenced_frame_properties) {
    // TODO(crbug.com/1262022): Reenable this DCHECK once ShadowDOM and
    // loading urns in iframes (for FLEDGE OT) are gone.
    // DCHECK_EQ(fenced_frame_status_,
    //          RenderFrameHostImpl::FencedFrameStatus::kFencedFrameRoot);
    fenced_frame_properties_ = fenced_frame_properties;
  }
~~~

### GetFencedFrameProperties

FrameTreeNode : public::GetFencedFrameProperties
~~~cpp
const absl::optional<FencedFrameProperties>& GetFencedFrameProperties();
~~~
 Return the fenced frame properties for this fenced frame tree (if any).

 That is to say, this function returns the `fenced_frame_properties_`
 variable attached to the fenced frame root FrameTreeNode, which may be
 either this node or an ancestor of it.

### SetFencedFrameAutomaticBeaconReportEventData

FrameTreeNode : public::SetFencedFrameAutomaticBeaconReportEventData
~~~cpp
void SetFencedFrameAutomaticBeaconReportEventData(
      const std::string& event_data,
      const std::vector<blink::FencedFrame::ReportingDestination>& destination)
      override;
~~~
 Called from the currently active document via the
 `Fence.setReportEventDataForAutomaticBeacons` JS API.

### GetFencedFrameDepth

FrameTreeNode : public::GetFencedFrameDepth
~~~cpp
size_t GetFencedFrameDepth(size_t& shared_storage_fenced_frame_root_count);
~~~
 Returns the number of fenced frame boundaries above this frame. The
 outermost main frame's frame tree has fenced frame depth 0, a topmost
 fenced frame tree embedded in the outermost main frame has fenced frame
 depth 1, etc.


 Also, sets `shared_storage_fenced_frame_root_count` to the
 number of fenced frame boundaries (roots) above this frame that originate
 from shared storage. This is used to check whether a fenced frame
 originates from shared storage only (i.e. not from FLEDGE).

 TODO(crbug.com/1347953): Remove this check once we put permissions inside
 FencedFrameConfig.

### FindSharedStorageBudgetMetadata

FrameTreeNode : public::FindSharedStorageBudgetMetadata
~~~cpp
std::vector<const SharedStorageBudgetMetadata*>
  FindSharedStorageBudgetMetadata();
~~~
 Traverse up from this node. Return all valid
 `node->fenced_frame_properties_->shared_storage_budget_metadata` (i.e. this
 node is subjected to the shared storage budgeting associated with those
 metadata). Every node that originates from sharedStorage.selectURL() will
 have an associated metadata. This indicates that the metadata can only
 possibly be associated with a fenced frame root, unless when
 `kAllowURNsInIframes` is enabled in which case they could be be associated
 with any node.

### GetEmbedderSharedStorageContextIfAllowed

FrameTreeNode : public::GetEmbedderSharedStorageContextIfAllowed
~~~cpp
absl::optional<std::u16string> GetEmbedderSharedStorageContextIfAllowed();
~~~
 Returns any shared storage context string that was written to a
 `blink::FencedFrameConfig` before navigation via
 `setSharedStorageContext()`, as long as the request is for a same-origin
 frame within the config's fenced frame tree (or a same-origin descendant of
 a URN iframe).

### GetBrowsingContextStateForSubframe

FrameTreeNode : public::GetBrowsingContextStateForSubframe
~~~cpp
const scoped_refptr<BrowsingContextState>&
  GetBrowsingContextStateForSubframe() const;
~~~
 Accessor to BrowsingContextState for subframes only. Only main frame
 navigations can change BrowsingInstances and BrowsingContextStates,
 therefore for subframes associated BrowsingContextState never changes. This
 helper method makes this more explicit and guards against calling this on
 main frames (there an appropriate BrowsingContextState should be obtained
 from RenderFrameHost or from RenderFrameProxyHost as e.g. during
 cross-BrowsingInstance navigations multiple BrowsingContextStates exist in
 the same frame).

### ClearOpenerReferences

FrameTreeNode : public::ClearOpenerReferences
~~~cpp
void ClearOpenerReferences();
~~~
 Clears the opener property of popups referencing this FrameTreeNode as
 their opener.

### AncestorOrSelfHasCSPEE

FrameTreeNode : public::AncestorOrSelfHasCSPEE
~~~cpp
bool AncestorOrSelfHasCSPEE() const;
~~~
 Calculates whether one of the ancestor frames or this frame has a CSPEE in
 place. This is eventually sent over to LocalFrame in the renderer where it
 will be used by NavigatorAuction::canLoadAdAuctionFencedFrame for
 information it can't get on its own.

### ResetAllNavigationsForFrameDetach

FrameTreeNode : public::ResetAllNavigationsForFrameDetach
~~~cpp
void ResetAllNavigationsForFrameDetach();
~~~
 Reset every navigation in this frame, and its descendants. This is called
 after the <iframe> element has been removed, or after the document owning
 this frame has been navigated away.


 This takes into account:
 - Non-pending commit NavigationRequest owned by the FrameTreeNode
 - Pending commit NavigationRequest owned by the current RenderFrameHost
 - Speculative RenderFrameHost and its pending commit NavigationRequests.

### DidStartLoading

FrameTreeNode : public::DidStartLoading
~~~cpp
void DidStartLoading(bool should_show_loading_ui,
                       bool was_previously_loading) override;
~~~
 RenderFrameHostOwner implementation:
### DidStopLoading

FrameTreeNode : public::DidStopLoading
~~~cpp
void DidStopLoading() override;
~~~

### RestartNavigationAsCrossDocument

FrameTreeNode : public::RestartNavigationAsCrossDocument
~~~cpp
void RestartNavigationAsCrossDocument(
      std::unique_ptr<NavigationRequest> navigation_request) override;
~~~

### Reload

FrameTreeNode : public::Reload
~~~cpp
bool Reload() override;
~~~

### GetCurrentNavigator

FrameTreeNode : public::GetCurrentNavigator
~~~cpp
Navigator& GetCurrentNavigator() override;
~~~

### GetRenderFrameHostManager

FrameTreeNode : public::GetRenderFrameHostManager
~~~cpp
RenderFrameHostManager& GetRenderFrameHostManager() override;
~~~

### GetOpener

FrameTreeNode : public::GetOpener
~~~cpp
FrameTreeNode* GetOpener() const override;
~~~

### SetFocusedFrame

FrameTreeNode : public::SetFocusedFrame
~~~cpp
void SetFocusedFrame(SiteInstanceGroup* source) override;
~~~

### DidChangeReferrerPolicy

FrameTreeNode : public::DidChangeReferrerPolicy
~~~cpp
void DidChangeReferrerPolicy(
      network::mojom::ReferrerPolicy referrer_policy) override;
~~~

### UpdateUserActivationState

FrameTreeNode : public::UpdateUserActivationState
~~~cpp
bool UpdateUserActivationState(
      blink::mojom::UserActivationUpdateType update_type,
      blink::mojom::UserActivationNotificationType notification_type) override;
~~~
 Updates the user activation state in the browser frame tree and in the
 frame trees in all renderer processes except the renderer for this node
 (which initiated the update).  Returns |false| if the update tries to
 consume an already consumed/expired transient state, |true| otherwise.  See
 the comment on `user_activation_state_` in RenderFrameHostImpl.


 The |notification_type| parameter is used for histograms, only for the case
 |update_state == kNotifyActivation|.

### DidConsumeHistoryUserActivation

FrameTreeNode : public::DidConsumeHistoryUserActivation
~~~cpp
void DidConsumeHistoryUserActivation() override;
~~~

### CreateNavigationRequestForSynchronousRendererCommit

FrameTreeNode : public::CreateNavigationRequestForSynchronousRendererCommit
~~~cpp
std::unique_ptr<NavigationRequest>
  CreateNavigationRequestForSynchronousRendererCommit(
      RenderFrameHostImpl* render_frame_host,
      bool is_same_document,
      const GURL& url,
      const url::Origin& origin,
      const absl::optional<GURL>& initiator_base_url,
      const net::IsolationInfo& isolation_info_for_subresources,
      blink::mojom::ReferrerPtr referrer,
      const ui::PageTransition& transition,
      bool should_replace_current_entry,
      const std::string& method,
      bool has_transient_activation,
      bool is_overriding_user_agent,
      const std::vector<GURL>& redirects,
      const GURL& original_url,
      std::unique_ptr<CrossOriginEmbedderPolicyReporter> coep_reporter,
      std::unique_ptr<SubresourceWebBundleNavigationInfo>
          subresource_web_bundle_navigation_info,
      int http_response_code) override;
~~~

### CancelNavigation

FrameTreeNode : public::CancelNavigation
~~~cpp
void CancelNavigation() override;
~~~

### Credentialless

FrameTreeNode : public::Credentialless
~~~cpp
bool Credentialless() const override;
~~~

### GetVirtualAuthenticatorManager

FrameTreeNode : public::GetVirtualAuthenticatorManager
~~~cpp
void GetVirtualAuthenticatorManager(
      mojo::PendingReceiver<blink::test::mojom::VirtualAuthenticatorManager>
          receiver) override;
~~~

### DestroyInnerFrameTreeIfExists

FrameTreeNode : public::DestroyInnerFrameTreeIfExists
~~~cpp
void DestroyInnerFrameTreeIfExists();
~~~
 Called by the destructor. When `this` is an outer dummy FrameTreeNode
 representing an inner FrameTree, this method destroys said inner FrameTree.

### NotifyUserActivation

FrameTreeNode : public::NotifyUserActivation
~~~cpp
bool NotifyUserActivation(
      blink::mojom::UserActivationNotificationType notification_type);
~~~
 The |notification_type| parameter is used for histograms only.

### ConsumeTransientUserActivation

FrameTreeNode : public::ConsumeTransientUserActivation
~~~cpp
bool ConsumeTransientUserActivation();
~~~

### ClearUserActivation

FrameTreeNode : public::ClearUserActivation
~~~cpp
bool ClearUserActivation();
~~~

### VerifyUserActivation

FrameTreeNode : public::VerifyUserActivation
~~~cpp
bool VerifyUserActivation();
~~~
 Verify that the renderer process is allowed to set user activation on this
 frame by checking whether this frame's RenderWidgetHost had previously seen
 an input event that might lead to user activation. If user activation
 should be allowed, this returns true and also clears corresponding pending
 user activation state in the widget. Otherwise, this returns false.

### GetFencedFramePropertiesForEditing

FrameTreeNode : public::GetFencedFramePropertiesForEditing
~~~cpp
absl::optional<FencedFrameProperties>& GetFencedFramePropertiesForEditing();
~~~
