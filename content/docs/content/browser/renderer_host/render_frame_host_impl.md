### BASE_DECLARE_FEATURE

BASE_DECLARE_FEATURE
~~~cpp
CONTENT_EXPORT BASE_DECLARE_FEATURE(
    kDisableFrameNameUpdateOnNonCurrentRenderFrameHost);
~~~
 Feature to prevent name updates to RenderFrameHost (and by extension its
 relevant BrowsingContextState) when it is not current (i.e. is in the
 BackForwardCache or is pending delete). This primarily will affect the
 non-legacy implementation of BrowsingContextState.

### BASE_DECLARE_FEATURE

BASE_DECLARE_FEATURE
~~~cpp
CONTENT_EXPORT BASE_DECLARE_FEATURE(kEvictOnAXEvents);
~~~
 Feature to evict when accessibility events occur while in back/forward cache.

### BASE_DECLARE_FEATURE

BASE_DECLARE_FEATURE
~~~cpp
CONTENT_EXPORT BASE_DECLARE_FEATURE(kUnblockSpeechSynthesisForBFCache);
~~~
 Feature to make SpeechSynthesis no longer block back/forward cache.

### FromID

FromID
~~~cpp
static RenderFrameHostImpl* FromID(GlobalRenderFrameHostId id);
~~~

### FromID

FromID
~~~cpp
static RenderFrameHostImpl* FromID(int process_id, int routing_id);
~~~

### FromFrameToken

FromFrameToken
~~~cpp
static RenderFrameHostImpl* FromFrameToken(
      int process_id,
      const blink::LocalFrameToken& frame_token,
      mojo::ReportBadMessageCallback* process_mismatch_callback = nullptr);
~~~
 Returns the `RenderFrameHostImpl` with the given `blink::LocalFrameToken`,
 or `nullptr` if no such `RenderFrameHostImpl` exists.


 Note that though local frame tokens are globally unique, the process ID is
 required as a second factor to reduce the risk of inadvertent info leaks
 leading to security failures. If there is a `RenderFrameHostImpl` with a
 matching local frame token but not a matching process ID, invokes
 `process_mismatch_callback` (if non-null) and returns `nullptr`.

### FromDocumentToken

FromDocumentToken
~~~cpp
static RenderFrameHostImpl* FromDocumentToken(
      int process_id,
      const blink::DocumentToken& document_token,
      mojo::ReportBadMessageCallback* process_mismatch_callback = nullptr);
~~~
 Returns the `RenderFrameHostImpl` with the given `blink::DocumentToken`, or
 `nullptr` if no such `RenderFrameHostImpl` exists.


 Note that though document tokens are globally unique, the process ID is
 required as a second factor to reduce the risk of inadvertent info leaks
 leading to security failures. If there is a `RenderFrameHostImpl` with a
 matching document token but not a matching process ID, invokes
 `process_mismatch_callback` (if non-null) and returns `nullptr`.

### FromAXTreeID

FromAXTreeID
~~~cpp
static RenderFrameHostImpl* FromAXTreeID(ui::AXTreeID ax_tree_id);
~~~

### FromOverlayRoutingToken

FromOverlayRoutingToken
~~~cpp
static RenderFrameHostImpl* FromOverlayRoutingToken(
      const base::UnguessableToken& token);
~~~

### ClearAllPrefetchedSignedExchangeCache

ClearAllPrefetchedSignedExchangeCache
~~~cpp
static void ClearAllPrefetchedSignedExchangeCache();
~~~
 Clears the all prefetched cached signed exchanges.

### SetCodeCacheHostReceiverHandlerForTesting

SetCodeCacheHostReceiverHandlerForTesting
~~~cpp
static void SetCodeCacheHostReceiverHandlerForTesting(
      CodeCacheHostReceiverHandler handler);
~~~

### RenderFrameHostImpl

RenderFrameHostImpl
~~~cpp
RenderFrameHostImpl(const RenderFrameHostImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
RenderFrameHostImpl& operator=(const RenderFrameHostImpl&) = delete;
~~~

### ~RenderFrameHostImpl

~RenderFrameHostImpl
~~~cpp
~RenderFrameHostImpl() override
~~~

### GetRoutingID

GetRoutingID
~~~cpp
int GetRoutingID() const override;
~~~
 RenderFrameHost
### GetFrameToken

GetFrameToken
~~~cpp
const blink::LocalFrameToken& GetFrameToken() const override;
~~~

### GetReportingSource

GetReportingSource
~~~cpp
const base::UnguessableToken& GetReportingSource() override;
~~~

### GetAXTreeID

GetAXTreeID
~~~cpp
ui::AXTreeID GetAXTreeID() override;
~~~

### RequestAXTreeSnapshot

RequestAXTreeSnapshot
~~~cpp
void RequestAXTreeSnapshot(AXTreeSnapshotCallback callback,
                             const ui::AXMode& ax_mode,
                             bool exclude_offscreen,
                             size_t max_nodes,
                             const base::TimeDelta& timeout) override;
~~~

### GetSiteInstance

GetSiteInstance
~~~cpp
SiteInstanceImpl* GetSiteInstance() const override;
~~~

### GetProcess

GetProcess
~~~cpp
RenderProcessHost* GetProcess() const override;
~~~

### GetGlobalId

GetGlobalId
~~~cpp
GlobalRenderFrameHostId GetGlobalId() const override;
~~~

### GetRenderWidgetHost

GetRenderWidgetHost
~~~cpp
RenderWidgetHostImpl* GetRenderWidgetHost() override;
~~~

### GetView

GetView
~~~cpp
RenderWidgetHostView* GetView() override;
~~~

### GetParent

GetParent
~~~cpp
RenderFrameHostImpl* GetParent() const override;
~~~

### GetParentOrOuterDocument

GetParentOrOuterDocument
~~~cpp
RenderFrameHostImpl* GetParentOrOuterDocument() const override;
~~~

### GetParentOrOuterDocumentOrEmbedder

GetParentOrOuterDocumentOrEmbedder
~~~cpp
RenderFrameHostImpl* GetParentOrOuterDocumentOrEmbedder() const override;
~~~

### GetMainFrame

GetMainFrame
~~~cpp
RenderFrameHostImpl* GetMainFrame() override;
~~~

### GetPage

GetPage
~~~cpp
PageImpl& GetPage() override;
~~~

### IsInPrimaryMainFrame

IsInPrimaryMainFrame
~~~cpp
bool IsInPrimaryMainFrame() override;
~~~

### GetOutermostMainFrame

GetOutermostMainFrame
~~~cpp
RenderFrameHostImpl* GetOutermostMainFrame() override;
~~~

### GetOutermostMainFrameOrEmbedder

GetOutermostMainFrameOrEmbedder
~~~cpp
RenderFrameHostImpl* GetOutermostMainFrameOrEmbedder() override;
~~~

### IsFencedFrameRoot

IsFencedFrameRoot
~~~cpp
bool IsFencedFrameRoot() const override;
~~~

### IsNestedWithinFencedFrame

IsNestedWithinFencedFrame
~~~cpp
bool IsNestedWithinFencedFrame() const override;
~~~

### ForEachRenderFrameHostWithAction

ForEachRenderFrameHostWithAction
~~~cpp
void ForEachRenderFrameHostWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHost*)> on_frame)
      override;
~~~

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
void ForEachRenderFrameHost(
      base::FunctionRef<void(RenderFrameHost*)> on_frame) override;
~~~

### GetFrameTreeNodeId

GetFrameTreeNodeId
~~~cpp
int GetFrameTreeNodeId() const override;
~~~
 TODO (crbug.com/1251545) : Frame tree node id should only be known for
 subframes. As such, update this method.

### GetDevToolsFrameToken

GetDevToolsFrameToken
~~~cpp
const base::UnguessableToken& GetDevToolsFrameToken() override;
~~~

### GetEmbeddingToken

GetEmbeddingToken
~~~cpp
absl::optional<base::UnguessableToken> GetEmbeddingToken() override;
~~~

### GetFrameName

GetFrameName
~~~cpp
const std::string& GetFrameName() override;
~~~

### IsFrameDisplayNone

IsFrameDisplayNone
~~~cpp
bool IsFrameDisplayNone() override;
~~~

### GetFrameSize

GetFrameSize
~~~cpp
const absl::optional<gfx::Size>& GetFrameSize() override;
~~~

### GetFrameDepth

GetFrameDepth
~~~cpp
size_t GetFrameDepth() override;
~~~

### IsCrossProcessSubframe

IsCrossProcessSubframe
~~~cpp
bool IsCrossProcessSubframe() override;
~~~

### GetWebExposedIsolationLevel

GetWebExposedIsolationLevel
~~~cpp
WebExposedIsolationLevel GetWebExposedIsolationLevel() override;
~~~

### GetLastCommittedURL

GetLastCommittedURL
~~~cpp
const GURL& GetLastCommittedURL() const override;
~~~

### GetLastCommittedOrigin

GetLastCommittedOrigin
~~~cpp
const url::Origin& GetLastCommittedOrigin() const override;
~~~

### GetNetworkIsolationKey

GetNetworkIsolationKey
~~~cpp
const net::NetworkIsolationKey& GetNetworkIsolationKey() override;
~~~

### GetIsolationInfoForSubresources

GetIsolationInfoForSubresources
~~~cpp
const net::IsolationInfo& GetIsolationInfoForSubresources() override;
~~~

### GetPendingIsolationInfoForSubresources

GetPendingIsolationInfoForSubresources
~~~cpp
net::IsolationInfo GetPendingIsolationInfoForSubresources() override;
~~~

### GetNativeView

GetNativeView
~~~cpp
gfx::NativeView GetNativeView() override;
~~~

### AddMessageToConsole

AddMessageToConsole
~~~cpp
void AddMessageToConsole(blink::mojom::ConsoleMessageLevel level,
                           const std::string& message) override;
~~~

### ExecuteJavaScriptMethod

ExecuteJavaScriptMethod
~~~cpp
void ExecuteJavaScriptMethod(const std::u16string& object_name,
                               const std::u16string& method_name,
                               base::Value::List arguments,
                               JavaScriptResultCallback callback) override;
~~~

### ExecuteJavaScript

ExecuteJavaScript
~~~cpp
void ExecuteJavaScript(const std::u16string& javascript,
                         JavaScriptResultCallback callback) override;
~~~

### ExecuteJavaScriptInIsolatedWorld

ExecuteJavaScriptInIsolatedWorld
~~~cpp
void ExecuteJavaScriptInIsolatedWorld(const std::u16string& javascript,
                                        JavaScriptResultCallback callback,
                                        int32_t world_id) override;
~~~

### ExecuteJavaScriptForTests

ExecuteJavaScriptForTests
~~~cpp
void ExecuteJavaScriptForTests(
      const std::u16string& javascript,
      JavaScriptResultCallback callback,
      int32_t world_id = ISOLATED_WORLD_ID_GLOBAL) override;
~~~

### ExecuteJavaScriptWithUserGestureForTests

ExecuteJavaScriptWithUserGestureForTests
~~~cpp
void ExecuteJavaScriptWithUserGestureForTests(
      const std::u16string& javascript,
      JavaScriptResultCallback callback,
      int32_t world_id = ISOLATED_WORLD_ID_GLOBAL) override;
~~~

### ExecutePluginActionAtLocalLocation

ExecutePluginActionAtLocalLocation
~~~cpp
void ExecutePluginActionAtLocalLocation(
      const gfx::Point& local_location,
      blink::mojom::PluginActionType plugin_action) override;
~~~

### ActivateFindInPageResultForAccessibility

ActivateFindInPageResultForAccessibility
~~~cpp
void ActivateFindInPageResultForAccessibility(int request_id) override;
~~~

### InsertVisualStateCallback

InsertVisualStateCallback
~~~cpp
void InsertVisualStateCallback(VisualStateCallback callback) override;
~~~

### CopyImageAt

CopyImageAt
~~~cpp
void CopyImageAt(int x, int y) override;
~~~

### SaveImageAt

SaveImageAt
~~~cpp
void SaveImageAt(int x, int y) override;
~~~

### GetRenderViewHost

GetRenderViewHost
~~~cpp
RenderViewHost* GetRenderViewHost() const override;
~~~

### GetRemoteInterfaces

GetRemoteInterfaces
~~~cpp
service_manager::InterfaceProvider* GetRemoteInterfaces() override;
~~~

### GetRemoteAssociatedInterfaces

GetRemoteAssociatedInterfaces
~~~cpp
blink::AssociatedInterfaceProvider* GetRemoteAssociatedInterfaces() override;
~~~

### GetVisibilityState

GetVisibilityState
~~~cpp
content::PageVisibilityState GetVisibilityState() override;
~~~

### IsLastCommitIPAddressPubliclyRoutable

IsLastCommitIPAddressPubliclyRoutable
~~~cpp
bool IsLastCommitIPAddressPubliclyRoutable() const override;
~~~

### IsRenderFrameLive

IsRenderFrameLive
~~~cpp
bool IsRenderFrameLive() override;
~~~

### GetLifecycleState

GetLifecycleState
~~~cpp
LifecycleState GetLifecycleState() override;
~~~

### IsInLifecycleState

IsInLifecycleState
~~~cpp
bool IsInLifecycleState(LifecycleState lifecycle_state) override;
~~~

### IsActive

IsActive
~~~cpp
bool IsActive() override;
~~~

### IsInactiveAndDisallowActivation

IsInactiveAndDisallowActivation
~~~cpp
bool IsInactiveAndDisallowActivation(uint64_t reason) override;
~~~

### GetProxyCount

GetProxyCount
~~~cpp
size_t GetProxyCount() override;
~~~

### HasSelection

HasSelection
~~~cpp
bool HasSelection() override;
~~~

### GetLastResponseHeaders

GetLastResponseHeaders
~~~cpp
const net::HttpResponseHeaders* GetLastResponseHeaders() override;
~~~

### RequestTextSurroundingSelection

RequestTextSurroundingSelection
~~~cpp
void RequestTextSurroundingSelection(
      blink::mojom::LocalFrame::GetTextSurroundingSelectionCallback callback,
      int max_length) override;
~~~

### SendInterventionReport

SendInterventionReport
~~~cpp
void SendInterventionReport(const std::string& id,
                              const std::string& message) override;
~~~

### GetWebUI

GetWebUI
~~~cpp
WebUI* GetWebUI() override;
~~~

### AllowBindings

AllowBindings
~~~cpp
void AllowBindings(int binding_flags) override;
~~~

### GetEnabledBindings

GetEnabledBindings
~~~cpp
int GetEnabledBindings() override;
~~~

### SetWebUIProperty

SetWebUIProperty
~~~cpp
void SetWebUIProperty(const std::string& name,
                        const std::string& value) override;
~~~

### DisableBeforeUnloadHangMonitorForTesting

DisableBeforeUnloadHangMonitorForTesting
~~~cpp
void DisableBeforeUnloadHangMonitorForTesting() override;
~~~

### IsBeforeUnloadHangMonitorDisabledForTesting

IsBeforeUnloadHangMonitorDisabledForTesting
~~~cpp
bool IsBeforeUnloadHangMonitorDisabledForTesting() override;
~~~

### GetSuddenTerminationDisablerState

GetSuddenTerminationDisablerState
~~~cpp
bool GetSuddenTerminationDisablerState(
      blink::mojom::SuddenTerminationDisablerType disabler_type) override;
~~~

### IsFeatureEnabled

IsFeatureEnabled
~~~cpp
bool IsFeatureEnabled(
      blink::mojom::PermissionsPolicyFeature feature) override;
~~~

### GetPermissionsPolicy

GetPermissionsPolicy
~~~cpp
const blink::PermissionsPolicy* GetPermissionsPolicy() override;
~~~

### GetPermissionsPolicyHeader

GetPermissionsPolicyHeader
~~~cpp
const blink::ParsedPermissionsPolicy& GetPermissionsPolicyHeader() override;
~~~

### ViewSource

ViewSource
~~~cpp
void ViewSource() override;
~~~

### ExecuteMediaPlayerActionAtLocation

ExecuteMediaPlayerActionAtLocation
~~~cpp
void ExecuteMediaPlayerActionAtLocation(
      const gfx::Point&,
      const blink::mojom::MediaPlayerAction& action) override;
~~~

### CreateNetworkServiceDefaultFactory

CreateNetworkServiceDefaultFactory
~~~cpp
bool CreateNetworkServiceDefaultFactory(
      mojo::PendingReceiver<network::mojom::URLLoaderFactory>
          default_factory_receiver) override;
~~~

### MarkIsolatedWorldsAsRequiringSeparateURLLoaderFactory

MarkIsolatedWorldsAsRequiringSeparateURLLoaderFactory
~~~cpp
void MarkIsolatedWorldsAsRequiringSeparateURLLoaderFactory(
      const base::flat_set<url::Origin>& isolated_world_origins,
      bool push_to_renderer_now) override;
~~~

### IsSandboxed

IsSandboxed
~~~cpp
bool IsSandboxed(network::mojom::WebSandboxFlags flags) override;
~~~

### FlushNetworkAndNavigationInterfacesForTesting

FlushNetworkAndNavigationInterfacesForTesting
~~~cpp
void FlushNetworkAndNavigationInterfacesForTesting(
      bool do_nothing_if_no_network_service_connection = false) override;
~~~

### PrepareForInnerWebContentsAttach

PrepareForInnerWebContentsAttach
~~~cpp
void PrepareForInnerWebContentsAttach(
      PrepareForInnerWebContentsAttachCallback callback) override;
~~~

### GetFrameOwnerElementType

GetFrameOwnerElementType
~~~cpp
blink::FrameOwnerElementType GetFrameOwnerElementType() override;
~~~

### HasTransientUserActivation

HasTransientUserActivation
~~~cpp
bool HasTransientUserActivation() override;
~~~

### NotifyUserActivation

NotifyUserActivation
~~~cpp
void NotifyUserActivation(
      blink::mojom::UserActivationNotificationType notification_type) override;
~~~

### Reload

Reload
~~~cpp
bool Reload() override;
~~~

### IsDOMContentLoaded

IsDOMContentLoaded
~~~cpp
bool IsDOMContentLoaded() override;
~~~

### UpdateIsAdFrame

UpdateIsAdFrame
~~~cpp
void UpdateIsAdFrame(bool is_ad_frame) override;
~~~

### SetIsXrOverlaySetup

SetIsXrOverlaySetup
~~~cpp
void SetIsXrOverlaySetup() override;
~~~

### GetPageUkmSourceId

GetPageUkmSourceId
~~~cpp
ukm::SourceId GetPageUkmSourceId() override;
~~~

### GetStoragePartition

GetStoragePartition
~~~cpp
StoragePartitionImpl* GetStoragePartition() override;
~~~

### GetBrowserContext

GetBrowserContext
~~~cpp
BrowserContext* GetBrowserContext() override;
~~~

### ReportInspectorIssue

ReportInspectorIssue
~~~cpp
void ReportInspectorIssue(blink::mojom::InspectorIssueInfoPtr info) override;
~~~

### WriteIntoTrace

WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedProto<TraceProto> context) const override;
~~~

### GetCanonicalUrl

GetCanonicalUrl
~~~cpp
void GetCanonicalUrl(
      base::OnceCallback<void(const absl::optional<GURL>&)> callback) override;
~~~

### GetOpenGraphMetadata

GetOpenGraphMetadata
~~~cpp
void GetOpenGraphMetadata(
      base::OnceCallback<void(blink::mojom::OpenGraphMetadataPtr)> callback)
      override;
~~~

### IsErrorDocument

IsErrorDocument
~~~cpp
bool IsErrorDocument() override;
~~~

### GetDocumentRef

GetDocumentRef
~~~cpp
DocumentRef GetDocumentRef() override;
~~~

### GetWeakDocumentPtr

GetWeakDocumentPtr
~~~cpp
WeakDocumentPtr GetWeakDocumentPtr() override;
~~~

### EnableMojoJsBindings

EnableMojoJsBindings
~~~cpp
void EnableMojoJsBindings(
      content::mojom::ExtraMojoJsFeaturesPtr features) override;
~~~

### GetMainFrame

GetMainFrame
~~~cpp
const RenderFrameHostImpl* GetMainFrame() const;
~~~
 Additional non-override const version of GetMainFrame.

### GetPage

GetPage
~~~cpp
const PageImpl& GetPage() const;
~~~
 Additional non-override const version of GetPage.

### GetDocumentToken

GetDocumentToken
~~~cpp
const blink::DocumentToken& GetDocumentToken() const;
~~~
 Returns the token for the document currently associated with this frame.

 This can change over time if a `RenderFrameHost` is reused when navigating
 to a new document.


 Retrieving the document token is disallowed if:
 - the RenderFrameHost is pending commit, e.g. waiting for the renderer to
   acknowledge the commit, since the DocumentToken will change as soon as
   the navigation actually commits.

 - the RenderFrameHost is speculative
### GetDocumentTokenIgnoringSafetyRestrictions

GetDocumentTokenIgnoringSafetyRestrictions
~~~cpp
const blink::DocumentToken& GetDocumentTokenIgnoringSafetyRestrictions()
      const {
    return document_associated_data_->token();
  }
~~~
 Retrieving the document token is disallowed during times when the result
 might be misleading / confusing (kPendingCommit or kSpeculative).

 Internally, the content implementation may still need to retrieve the
 document token at those times, so provide an escape hatch.

### GetDocumentTokenForCrossDocumentNavigationReuse

GetDocumentTokenForCrossDocumentNavigationReuse
~~~cpp
const blink::DocumentToken* GetDocumentTokenForCrossDocumentNavigationReuse(
      base::PassKey<NavigationRequest>);
~~~
 Returns a non-null DocumentToken pointer if a cross-document navigation
 should reuse the DocumentToken. This is only ever the case for the first
 cross-document commit in a speculative RenderFrameHost. Otherwise, returns
 nullptr.

### is_render_frame_deleted

is_render_frame_deleted
~~~cpp
bool is_render_frame_deleted() const {
    return render_frame_state_ == RenderFrameState::kDeleted;
  }
~~~
 A RenderFrame was previously created but no longer exists, e.g. the
 renderer process is gone due to a crash.

### ReinitializeDocumentAssociatedDataForReuseAfterCrash

ReinitializeDocumentAssociatedDataForReuseAfterCrash
~~~cpp
void ReinitializeDocumentAssociatedDataForReuseAfterCrash(
      base::PassKey<RenderFrameHostManager>);
~~~
 Immediately reinitializes DocumentUserData when the RenderFrameHost needs
 to be immediately reused after a crash. Only usable for a main frame where
 `is_render_frame_deleted()` is true.

### ReinitializeDocumentAssociatedDataForTesting

ReinitializeDocumentAssociatedDataForTesting
~~~cpp
void ReinitializeDocumentAssociatedDataForTesting();
~~~
 Immediately reinitializes DocumentUserData for testing a corner case crash
 scenario. See usage in
 ManifestBrowserTest.GetManifestInterruptedByDestruction.

### IsClipboardPasteContentAllowed

IsClipboardPasteContentAllowed
~~~cpp
void IsClipboardPasteContentAllowed(
      const ui::ClipboardFormatType& data_type,
      const std::string& data,
      IsClipboardPasteContentAllowedCallback callback);
~~~
 Determines if a clipboard paste using |data| of type |data_type| is allowed
 in this renderer frame.  The implementation delegates to
 RenderFrameHostDelegate::IsClipboardPasteContentAllowed().  See the
 description of the latter method for complete details.

### IsInactiveAndDisallowActivationForAXEvents

IsInactiveAndDisallowActivationForAXEvents
~~~cpp
bool IsInactiveAndDisallowActivationForAXEvents(
      const std::vector<ui::AXEvent>& events);
~~~
 This is called when accessibility events arrive from renderer to browser.

 This could cause eviction if the page is in back/forward cache. Returns
 true if the eviction happens, and otherwise calls
 |RenderFrameHost::IsInactiveAndDisallowActivation()| and returns the value
 from there. This is only called when the flag to evict on accessibility
 events is on. When the flag is off, we do not evict the entry and keep
 processing the events, thus do not call this function.

### SendAccessibilityEventsToManager

SendAccessibilityEventsToManager
~~~cpp
void SendAccessibilityEventsToManager(
      const AXEventNotificationDetails& details);
~~~

### EvictFromBackForwardCacheWithReason

EvictFromBackForwardCacheWithReason
~~~cpp
void EvictFromBackForwardCacheWithReason(
      BackForwardCacheMetrics::NotRestoredReason reason);
~~~
 Evict the RenderFrameHostImpl with |reason| that causes the eviction. This
 constructs a flattened list of NotRestoredReasons and calls
 |EvictFromBackForwardCacheWithFlattenedReasons|.

### EvictFromBackForwardCacheWithFlattenedReasons

EvictFromBackForwardCacheWithFlattenedReasons
~~~cpp
void EvictFromBackForwardCacheWithFlattenedReasons(
      BackForwardCacheCanStoreDocumentResult can_store_flat);
~~~
 Evict the RenderFrameHostImpl with |can_store_flat| as the eviction reason.

 This constructs a tree of NotRestoredReasons based on |can_store_flat| and
 calls |EvictFromBackForwardCacheWithFlattenedAndTreeReasons|.

### EvictFromBackForwardCacheWithFlattenedAndTreeReasons

EvictFromBackForwardCacheWithFlattenedAndTreeReasons
~~~cpp
void EvictFromBackForwardCacheWithFlattenedAndTreeReasons(
      BackForwardCacheCanStoreDocumentResultWithTree& can_store);
~~~
 Evict the RenderFrameHostImpl with |can_store| that causes the eviction.

 This reports the flattened list and the tree of NotRestoredReasons to
 metrics, and posts a task to evict the frame.

### UseDummyStickyBackForwardCacheDisablingFeatureForTesting

UseDummyStickyBackForwardCacheDisablingFeatureForTesting
~~~cpp
void UseDummyStickyBackForwardCacheDisablingFeatureForTesting();
~~~
 Only for testing sticky WebBackForwardCacheDisablingFeature.

 This is implemented solely in the browser and should only be used when
 stickiness is required, otherwise
 BackForwardCacheBrowserTest::AddBlocklistedFeature should be used.

### NotRestoredReasonsForTesting

NotRestoredReasonsForTesting
~~~cpp
const blink::mojom::BackForwardCacheNotRestoredReasonsPtr&
  NotRestoredReasonsForTesting() {
    return not_restored_reasons_for_testing_;
  }
~~~

### GetOrCreateWebPreferences

GetOrCreateWebPreferences
~~~cpp
blink::web_pref::WebPreferences GetOrCreateWebPreferences();
~~~
 Returns the current WebPreferences for the WebContents associated with this
 RenderFrameHost. Will create one if it does not exist (and update all the
 renderers with the newly computed value).

### Send

Send
~~~cpp
bool Send(IPC::Message* msg) override;
~~~
 IPC::Sender
### OnMessageReceived

OnMessageReceived
~~~cpp
bool OnMessageReceived(const IPC::Message& msg) override;
~~~
 IPC::Listener
### OnAssociatedInterfaceRequest

OnAssociatedInterfaceRequest
~~~cpp
void OnAssociatedInterfaceRequest(
      const std::string& interface_name,
      mojo::ScopedInterfaceEndpointHandle handle) override;
~~~

### ToDebugString

ToDebugString
~~~cpp
std::string ToDebugString() override;
~~~

### AccessibilityPerformAction

AccessibilityPerformAction
~~~cpp
void AccessibilityPerformAction(const ui::AXActionData& data) override;
~~~
 WebAXPlatformTreeManagerDelegate
### AccessibilityViewHasFocus

AccessibilityViewHasFocus
~~~cpp
bool AccessibilityViewHasFocus() override;
~~~

### AccessibilityViewSetFocus

AccessibilityViewSetFocus
~~~cpp
void AccessibilityViewSetFocus() override;
~~~

### AccessibilityGetViewBounds

AccessibilityGetViewBounds
~~~cpp
gfx::Rect AccessibilityGetViewBounds() override;
~~~

### AccessibilityGetDeviceScaleFactor

AccessibilityGetDeviceScaleFactor
~~~cpp
float AccessibilityGetDeviceScaleFactor() override;
~~~

### AccessibilityFatalError

AccessibilityFatalError
~~~cpp
void AccessibilityFatalError() override;
~~~

### AccessibilityGetAcceleratedWidget

AccessibilityGetAcceleratedWidget
~~~cpp
gfx::AcceleratedWidget AccessibilityGetAcceleratedWidget() override;
~~~

### AccessibilityGetNativeViewAccessible

AccessibilityGetNativeViewAccessible
~~~cpp
gfx::NativeViewAccessible AccessibilityGetNativeViewAccessible() override;
~~~

### AccessibilityGetNativeViewAccessibleForWindow

AccessibilityGetNativeViewAccessibleForWindow
~~~cpp
gfx::NativeViewAccessible AccessibilityGetNativeViewAccessibleForWindow()
      override;
~~~

### AccessibilityHitTest

AccessibilityHitTest
~~~cpp
void AccessibilityHitTest(
      const gfx::Point& point_in_frame_pixels,
      const ax::mojom::Event& opt_event_to_fire,
      int opt_request_id,
      base::OnceCallback<void(ui::AXPlatformTreeManager* hit_manager,
                              ui::AXNodeID hit_node_id)> opt_callback) override;
~~~

### AccessibilityIsRootFrame

AccessibilityIsRootFrame
~~~cpp
bool AccessibilityIsRootFrame() const override;
~~~

### AccessibilityRenderFrameHost

AccessibilityRenderFrameHost
~~~cpp
RenderFrameHostImpl* AccessibilityRenderFrameHost() override;
~~~

### AccessibilityGetWebContentsAccessibility

AccessibilityGetWebContentsAccessibility
~~~cpp
WebContentsAccessibility* AccessibilityGetWebContentsAccessibility() override;
~~~

### RenderProcessGone

RenderProcessGone
~~~cpp
void RenderProcessGone(SiteInstanceGroup* site_instance_group,
                         const ChildProcessTerminationInfo& info) override;
~~~
 SiteInstanceGroup::Observer
### PerformAction

PerformAction
~~~cpp
void PerformAction(const ui::AXActionData& data) override;
~~~
 ui::AXActionHandlerBase:
### RequiresPerformActionPointInPixels

RequiresPerformActionPointInPixels
~~~cpp
bool RequiresPerformActionPointInPixels() const override;
~~~

### CreateRenderFrame

CreateRenderFrame
~~~cpp
bool CreateRenderFrame(
      const absl::optional<blink::FrameToken>& previous_frame_token,
      const absl::optional<blink::FrameToken>& opener_frame_token,
      const absl::optional<blink::FrameToken>& parent_frame_token,
      const absl::optional<blink::FrameToken>& previous_sibling_frame_token);
~~~
 Creates a RenderFrame in the renderer process.

### DeleteRenderFrame

DeleteRenderFrame
~~~cpp
void DeleteRenderFrame(mojom::FrameDeleteIntention intent);
~~~
 Deletes the RenderFrame in the renderer process.

 Postcondition: |IsPendingDeletion()| is true.

### RenderFrameCreated

RenderFrameCreated
~~~cpp
void RenderFrameCreated();
~~~
 Track whether the RenderFrame for this RenderFrameHost has been created in
 or destroyed in the renderer process.

### RenderFrameDeleted

RenderFrameDeleted
~~~cpp
void RenderFrameDeleted();
~~~

### Init

Init
~~~cpp
void Init();
~~~
 Signals that the renderer has requested for this main-frame's window to be
 shown, at which point we can service navigation requests.

### PropagateEmbeddingTokenToParentFrame

PropagateEmbeddingTokenToParentFrame
~~~cpp
void PropagateEmbeddingTokenToParentFrame();
~~~
 This needs to be called to make sure that the parent-child relationship
 between frames is properly established both for cross-process iframes as
 well as for inner web contents (i.e. right after having attached it to the
 outer web contents), so that navigating the whole tree is possible.


 It's safe to call this method multiple times, or even before the embedding
 token has been set for this frame, to account for the realistic possibility
 that inner web contents can became attached to the outer one both before
 and after the embedding token has been set.

### is_audible

is_audible
~~~cpp
bool is_audible() const { return is_audible_; }
~~~
 Returns true if the frame recently plays an audio.

### OnAudibleStateChanged

OnAudibleStateChanged
~~~cpp
void OnAudibleStateChanged(bool is_audible);
~~~
 Toggles the audible state of this render frame. This should only be called
 from AudioStreamMonitor, and should not be invoked with the same value
 successively.

### OnCreateChildFrame

OnCreateChildFrame
~~~cpp
void OnCreateChildFrame(
      int new_routing_id,
      mojo::PendingAssociatedRemote<mojom::Frame> frame_remote,
      mojo::PendingReceiver<blink::mojom::BrowserInterfaceBroker>
          browser_interface_broker_receiver,
      blink::mojom::PolicyContainerBindParamsPtr policy_container_bind_params,
      mojo::PendingAssociatedReceiver<blink::mojom::AssociatedInterfaceProvider>
          associated_interface_provider_receiver,
      blink::mojom::TreeScopeType scope,
      const std::string& frame_name,
      const std::string& frame_unique_name,
      bool is_created_by_script,
      const blink::LocalFrameToken& frame_token,
      const base::UnguessableToken& devtools_frame_token,
      const blink::DocumentToken& document_token,
      const blink::FramePolicy& frame_policy,
      const blink::mojom::FrameOwnerProperties& frame_owner_properties,
      blink::FrameOwnerElementType owner_type,
      ukm::SourceId document_ukm_source_id);
~~~
 Called when this frame has added a child. This is a continuation of an IPC
 that was partially handled on the IO thread (to allocate |new_routing_id|,
 |frame_token|, |devtools_frame_token|, |document_token|), and is forwarded
 here. The renderer has already been told to create a RenderFrame with the
 specified ID values. |browser_interface_broker_receiver| is the receiver
 end of the BrowserInterfaceBroker interface in the child frame.

 RenderFrameHost should bind this receiver to expose services to the
 renderer process. The caller takes care of sending down the client end of
 the pipe to the child RenderFrame to use.

### DidNavigate

DidNavigate
~~~cpp
void DidNavigate(const mojom::DidCommitProvisionalLoadParams& params,
                   NavigationRequest* navigation_request,
                   bool was_within_same_document);
~~~
 Update this frame's state at the appropriate time when a navigation
 commits. This is called by Navigator::DidNavigate as a helper, in the
 midst of a DidCommitProvisionalLoad call. If |was_within_same_document| is
 true the navigation was same-document.

### render_view_host

render_view_host
~~~cpp
RenderViewHostImpl* render_view_host() { return render_view_host_.get(); }
~~~

### delegate

delegate
~~~cpp
RenderFrameHostDelegate* delegate() { return delegate_; }
~~~

### frame_tree

frame_tree
~~~cpp
FrameTree* frame_tree() const { return frame_tree_; }
~~~
 FrameTree references are being removed from RenderFrameHostImpl as a part
 of MPArch. Please avoid using these APIs. See crbug.com/1179502 for
 details.

### frame_tree_node

frame_tree_node
~~~cpp
FrameTreeNode* frame_tree_node() const { return frame_tree_node_; }
~~~

### child_count

child_count
~~~cpp
size_t child_count() { return children_.size(); }
~~~
 Methods to add/remove/reset/query child FrameTreeNodes of this frame.

 See class-level comment for FrameTreeNode for how the frame tree is
 represented.

### child_at

child_at
~~~cpp
FrameTreeNode* child_at(size_t index) const { return children_[index].get(); }
~~~

### AddChild

AddChild
~~~cpp
FrameTreeNode* AddChild(
      std::unique_ptr<FrameTreeNode> child,
      int frame_routing_id,
      mojo::PendingAssociatedRemote<mojom::Frame> frame_remote,
      const blink::LocalFrameToken& frame_token,
      const blink::DocumentToken& document_token,
      base::UnguessableToken devtools_frame_token,
      const blink::FramePolicy& frame_policy,
      std::string frame_name,
      std::string frame_unique_name);
~~~

### RemoveChild

RemoveChild
~~~cpp
void RemoveChild(FrameTreeNode* child);
~~~

### ResetChildren

ResetChildren
~~~cpp
void ResetChildren();
~~~

### SetLastCommittedUrl

SetLastCommittedUrl
~~~cpp
void SetLastCommittedUrl(const GURL& url);
~~~
 Set the URL of the document represented by this RenderFrameHost. Called
 when the navigation commits. See also `GetLastCommittedURL`.

### last_successful_url

last_successful_url
~~~cpp
const GURL& last_successful_url() const { return last_successful_url_; }
~~~
 The most recent non-net-error URL to commit in this frame.  In almost all
 cases, use GetLastCommittedURL instead.

### GetInheritedBaseUrl

GetInheritedBaseUrl
~~~cpp
const GURL& GetInheritedBaseUrl() const;
~~~
 For about:blank and about:srcdoc documents, this tracks the inherited base
 URL, snapshotted from the initiator's FrameLoadRequest. This is an empty
 URL for all other cases. This is currently only set if
 IsNewBaseUrlInheritanceBehaviorEnabled() returns true. See
 https://crbug.com/1356658.

### last_document_url_in_renderer

last_document_url_in_renderer
~~~cpp
const GURL& last_document_url_in_renderer() const {
    return renderer_url_info_.last_document_url;
  }
~~~
 The current URL of the document in the renderer process. Note that this
 includes URL updates due to document.open() (where it will be updated to
 the document-open-initiator URL) and special cases like error pages
 (where kUnreachableWebDataURL is used) and loadDataWithBaseURL() (where the
 base URL is used), so it might be different than the "last committed
 URL" provided by GetLastCommittedURL().

 In almost all cases, use GetLastCommittedURL() instead, as this should only
 be used when the caller wants to know the current state of the URL in the
 renderer (e.g. when predicting whether a navigation will do a replacement
 or not).

### was_loaded_from_load_data_with_base_url

was_loaded_from_load_data_with_base_url
~~~cpp
bool was_loaded_from_load_data_with_base_url() const {
    return renderer_url_info_.was_loaded_from_load_data_with_base_url;
  }
~~~
 Whether the last committed document was loaded from loadDataWithBaseURL or
 not.

### credentialless_iframes_nonce

credentialless_iframes_nonce
~~~cpp
const base::UnguessableToken& credentialless_iframes_nonce() const {
    DCHECK(is_main_frame() || IsFencedFrameRoot());
    return credentialless_iframes_nonce_;
  }
~~~

### storage_key

storage_key
~~~cpp
const blink::StorageKey& storage_key() const { return storage_key_; }
~~~
 Returns the storage key for the last committed document in this
 RenderFrameHostImpl. It is used for partitioning storage by the various
 storage APIs.

### last_http_method

last_http_method
~~~cpp
const std::string& last_http_method() { return last_http_method_; }
~~~
 Returns the http method of the last committed navigation.

### last_http_status_code

last_http_status_code
~~~cpp
int last_http_status_code() { return last_http_status_code_; }
~~~
 Returns the http status code of the last committed navigation.

### last_post_id

last_post_id
~~~cpp
int64_t last_post_id() { return last_post_id_; }
~~~
 Returns the POST ID of the last committed navigation.

### last_committed_common_params_has_user_gesture

last_committed_common_params_has_user_gesture
~~~cpp
bool last_committed_common_params_has_user_gesture() const {
    return last_committed_common_params_has_user_gesture_;
  }
~~~
 Returns true if last committed navigation's CommonNavigationParam's
 `has_user_gesture` is true. Should only be used to get the state of the
 last navigation, and not the current state of user activation of this
 RenderFrameHost. See comment on the variable declaration for more details.

### IsNavigationSameSite

IsNavigationSameSite
~~~cpp
bool IsNavigationSameSite(const UrlInfo& dest_url_info) const;
~~~
 Returns true if `dest_url_info` should be considered the same site as the
 current contents of this frame. This is the primary entry point for
 determining if a navigation to `dest_url_info` should stay in this
 RenderFrameHost's SiteInstance.

### ComputeTopFrameOrigin

ComputeTopFrameOrigin
~~~cpp
const url::Origin& ComputeTopFrameOrigin(
      const url::Origin& frame_origin) const;
~~~
 Returns |frame_origin| if this frame is the top (i.e. root) frame in the
 frame tree. Otherwise, it returns the top frame's origin.

### ComputeIsolationInfoForNavigation

ComputeIsolationInfoForNavigation
~~~cpp
net::IsolationInfo ComputeIsolationInfoForNavigation(
      const GURL& destination,
      bool is_credentialless,
      absl::optional<base::UnguessableToken> fenced_frame_nonce_for_navigation);
~~~
 Computes the IsolationInfo for this frame to `destination`. Set
 `is_credentialless` to true if the navigation will be loaded as a
 credentialless document (note that the navigation might be committing a
 credentialless document even if the document currently loaded in this RFH
 is not credentialless, and vice versa). Populate
 `fenced_frame_nonce_for_navigation` with
 `NavigationRequest::ComputeFencedFrameNonce()`.

### ComputeIsolationInfoForNavigation

ComputeIsolationInfoForNavigation
~~~cpp
net::IsolationInfo ComputeIsolationInfoForNavigation(const GURL& destination);
~~~
 Computes the IsolationInfo for this frame to |destination|.

### ComputeIsolationInfoForSubresourcesForPendingCommit

ComputeIsolationInfoForSubresourcesForPendingCommit
~~~cpp
net::IsolationInfo ComputeIsolationInfoForSubresourcesForPendingCommit(
      const url::Origin& main_world_origin_for_url_loader_factory,
      bool is_credentialless,
      absl::optional<base::UnguessableToken> fenced_frame_nonce_for_navigation);
~~~
 Computes the IsolationInfo that should be used for subresources, if
 |main_world_origin_for_url_loader_factory| is committed to this frame. The
 boolean `is_credentialless` specifies whether this frame will commit an
 credentialless document.

 Populate `fenced_frame_nonce_for_navigation` with
 `NavigationRequest::ComputeFencedFrameNonce()`.

### ComputeSiteForCookies

ComputeSiteForCookies
~~~cpp
net::SiteForCookies ComputeSiteForCookies();
~~~
 Computes site_for_cookies for this frame. A non-empty result denotes which
 domains are considered first-party to the top-level site when resources are
 loaded inside this frame. An empty result means that nothing will be
 first-party, as the frame hierarchy makes this context third-party already.


 The result can be used to check if cookies (including storage APIs and
 shared/service workers) are accessible.

### SetLastCommittedOriginForTesting

SetLastCommittedOriginForTesting
~~~cpp
void SetLastCommittedOriginForTesting(const url::Origin& origin);
~~~
 Allows overriding the last committed origin in tests.

### GetSerializedHtmlWithLocalLinks

GetSerializedHtmlWithLocalLinks
~~~cpp
void GetSerializedHtmlWithLocalLinks(
      const base::flat_map<GURL, base::FilePath>& url_map,
      const base::flat_map<blink::FrameToken, base::FilePath>& frame_token_map,
      bool save_with_empty_url,
      mojo::PendingRemote<mojom::FrameHTMLSerializerHandler>
          serializer_handler);
~~~
 Get HTML data for this RenderFrame by serializing contents on the renderer
 side and replacing all links to both same-site and cross-site resources
 with paths to local copies as specified by |url_map| and |frame_token_map|.

### web_ui

web_ui
~~~cpp
WebUIImpl* web_ui() const { return web_ui_.get(); }
~~~
 Returns the associated WebUI or null if none applies.

### web_ui_type

web_ui_type
~~~cpp
WebUI::TypeID web_ui_type() const { return web_ui_type_; }
~~~

### EnableMojoJsBindingsWithBroker

EnableMojoJsBindingsWithBroker
~~~cpp
void EnableMojoJsBindingsWithBroker(
      mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker> broker);
~~~
 Enable Mojo JavaScript bindings in the renderer process, and use the
 provided BrowserInterfaceBroker to handle JavaScript calls to
 Mojo.bindInterface. This method should be called in
 ReadyToCommitNavigation.

### is_main_frame

is_main_frame
~~~cpp
bool is_main_frame() const { return !parent_; }
~~~
 Frame trees may be nested so it can be the case that is_main_frame() is
 true, but is not the outermost RenderFrameHost (it only checks for nullity
 of |parent_|. In particular, !is_main_frame() cannot be used to check if
 this RenderFrameHost is embedded -- use !IsOutermostMainFrame() instead.

 NB: this does not escape guest views; IsOutermostMainFrame() will be true
 for the outermost main frame in an inner guest view.

### IsOutermostMainFrame

IsOutermostMainFrame
~~~cpp
bool IsOutermostMainFrame() const;
~~~

### is_loading

is_loading
~~~cpp
bool is_loading() const { return is_loading_; }
~~~
 Returns this RenderFrameHost's loading state. This method is only used by
 FrameTreeNode. The proper way to check whether a frame is loading is to
 call FrameTreeNode::IsLoading.

### SetIsLoadingForRendererDebugURL

SetIsLoadingForRendererDebugURL
~~~cpp
void SetIsLoadingForRendererDebugURL() { is_loading_ = true; }
~~~
 Sets `is_loading_` to true to handle renderer debug URLs. This is needed
 to generate DidStopLoading events for these URLs.

### is_local_root

is_local_root
~~~cpp
bool is_local_root() const { return !!GetLocalRenderWidgetHost(); }
~~~
 Returns true if this is a top-level frame, or if this frame
 uses a proxy to communicate with its parent frame. Local roots are
 distinguished by owning a RenderWidgetHost, which manages input events
 and painting for this frame and its contiguous local subtree in the
 renderer process.

### is_local_root_subframe

is_local_root_subframe
~~~cpp
bool is_local_root_subframe() const {
    return !is_main_frame() && is_local_root();
  }
~~~
 Returns true if this is not a top-level frame but is still a local root.

### GetRecordAggregateWatchTimeCallback

GetRecordAggregateWatchTimeCallback
~~~cpp
media::MediaMetricsProvider::RecordAggregateWatchTimeCallback
  GetRecordAggregateWatchTimeCallback();
~~~

### nav_entry_id

nav_entry_id
~~~cpp
int nav_entry_id() const { return nav_entry_id_; }
~~~
 The unique ID of the latest NavigationEntry that this RenderFrameHost is
 showing. This may change even when this frame hasn't committed a page,
 such as for a new subframe navigation in a different frame.

### set_nav_entry_id

set_nav_entry_id
~~~cpp
void set_nav_entry_id(int nav_entry_id) { nav_entry_id_ = nav_entry_id; }
~~~

### last_committed_frame_entry

last_committed_frame_entry
~~~cpp
FrameNavigationEntry* last_committed_frame_entry() {
    return last_committed_frame_entry_.get();
  }
~~~
 The FrameNavigationEntry for the current document in this RenderFrameHost.

 See last_committed_frame_entry_ declaration for more details.

### set_last_committed_frame_entry

set_last_committed_frame_entry
~~~cpp
void set_last_committed_frame_entry(
      scoped_refptr<FrameNavigationEntry> frame_entry) {
    last_committed_frame_entry_ = frame_entry;
  }
~~~

### HasPendingCommitNavigation

HasPendingCommitNavigation
~~~cpp
bool HasPendingCommitNavigation() const;
~~~
 Return true if this contains at least one NavigationRequest waiting to
 commit in this RenderFrameHost. This includes both same-document and
 cross-document NavigationRequests.

### HasPendingCommitForCrossDocumentNavigation

HasPendingCommitForCrossDocumentNavigation
~~~cpp
bool HasPendingCommitForCrossDocumentNavigation() const;
~~~
 Return true if this contains at least one NavigationRequest waiting to
 commit in this RenderFrameHost, excluding same-document navigations.

 NOTE: RenderFrameHostManager surfaces a similar method that will check
 all the RenderFrameHosts for that FrameTreeNode.

### IsPendingDeletion

IsPendingDeletion
~~~cpp
bool IsPendingDeletion() const;
~~~
 Return true if Unload() was called on the frame or one of its ancestors.

 If true, this corresponds either to unload handlers running for this
 RenderFrameHost (LifecycleStateImpl::kRunningUnloadHandlers) or when this
 RenderFrameHost is ready to be deleted
 (LifecycleStateImpl::kReadyToBeDeleted).

### IsInBackForwardCache

IsInBackForwardCache
~~~cpp
bool IsInBackForwardCache() const;
~~~
 Returns true if this RenderFrameHost is currently stored in the
 back-forward cache i.e., when lifecycle_state() is kInBackForwardCache.

### GetSameDocumentNavigationRequest

GetSameDocumentNavigationRequest
~~~cpp
NavigationRequest* GetSameDocumentNavigationRequest(
      const base::UnguessableToken& token);
~~~
 Returns a pending same-document navigation request in this frame that has
 the navigation_token |token|, if any.

### ResetOwnedNavigationRequests

ResetOwnedNavigationRequests
~~~cpp
void ResetOwnedNavigationRequests(NavigationDiscardReason reason);
~~~
 Resets the NavigationRequests stored in this RenderFrameHost, which are all
 "pending commit". Note this won't affect navigations that are "pending
 commit" but not owned by this RenderFrameHost, and any navigation that
 hasn't reached the "pending commit" stage yet, which would still be owned
 by the FrameTreeNode.

 TODO(https://crbug.com/1220337): Don't allow this to be called when there
 are pending cross-document navigations except for FrameTreeNode detach,
 RFH destruction, or when the renderer process is gone, so that we don't
 have to "undo" the commit that already happens in the renderer.

### SetNavigationRequest

SetNavigationRequest
~~~cpp
void SetNavigationRequest(
      std::unique_ptr<NavigationRequest> navigation_request);
~~~
 Called when a navigation is ready to commit in this
 RenderFrameHost. Transfers ownership of the NavigationRequest associated
 with the navigation to this RenderFrameHost.

### GetNavigationOrDocumentHandle

GetNavigationOrDocumentHandle
~~~cpp
const scoped_refptr<NavigationOrDocumentHandle>&
  GetNavigationOrDocumentHandle();
~~~

### Unload

Unload
~~~cpp
void Unload(RenderFrameProxyHost* proxy, bool is_loading);
~~~
 Tells the renderer that this RenderFrame is being replaced with one in a
 different renderer process.  It should run its unload handler and move to
 a blank document.  If |proxy| is not null, it should also create a
 `blink::RemoteFrame` to replace the RenderFrame and set it to `is_loading`
 state. The renderer process keeps the `blink::RemoteFrame` object around as
 a placeholder while the frame is rendered in a different process.


 There should always be a `proxy` to replace the old RenderFrameHost. If
 there are no remaining active views in the process, the proxy will be
 short-lived and will be deleted when the unload ACK is received.


 RenderDocument: After a local<->local swap, this function is called with a
 null |proxy|. It executes common cleanup and marks this RenderFrameHost to
 have completed its unload handler. The RenderFrameHost may be immediately
 deleted or deferred depending on its children's unload status.

### UndoCommitNavigation

UndoCommitNavigation
~~~cpp
void UndoCommitNavigation(RenderFrameProxyHost& proxy, bool is_loading);
~~~
 Sent to a renderer when the browser needs to cancel a navigation associated
 with a speculative RenderFrameHost that has already been asked to commit
 via `CommitNavigation()`. The renderer will swap out the already-committed
 RenderFrame, replacing it with a `blink::RemoteFrame` for `proxy`.


 TODO(https://crbug.com/1220337): This method is fundamentally incompatible
 with RenderDocument, as there is no `blink::RemoteFrame` to restore for a
 local<->local swap.

### SwapOuterDelegateFrame

SwapOuterDelegateFrame
~~~cpp
void SwapOuterDelegateFrame(RenderFrameProxyHost* proxy);
~~~
 Unload this frame for the proxy. Similar to `Unload()` but without
 managing the lifecycle of this object.

### OnUnloadACK

OnUnloadACK
~~~cpp
void OnUnloadACK();
~~~
 Process the acknowledgment of the unload of this frame from the renderer.

### DetachFromProxy

DetachFromProxy
~~~cpp
void DetachFromProxy();
~~~
 Remove this frame and its children. This happens asynchronously, an IPC
 round trip with the renderer process is needed to ensure children's unload
 handlers are run.

 Postcondition: |IsPendingDeletion()| is true.

### is_waiting_for_beforeunload_completion

is_waiting_for_beforeunload_completion
~~~cpp
bool is_waiting_for_beforeunload_completion() const {
    return is_waiting_for_beforeunload_completion_;
  }
~~~
 Whether an ongoing navigation in this frame is waiting for a BeforeUnload
 completion callback either from this RenderFrame or from one of its
 subframes.

### BeforeUnloadTimedOut

BeforeUnloadTimedOut
~~~cpp
bool BeforeUnloadTimedOut() const;
~~~
 True if more than |beforeunload_timeout_delay_| has elapsed since starting
 beforeunload. This may be true before |beforeunload_timeout_| actually
 fires, as the task can be delayed by task scheduling. See crbug.com/1056257
### IsWaitingForUnloadACK

IsWaitingForUnloadACK
~~~cpp
bool IsWaitingForUnloadACK() const;
~~~
 Whether the RFH is waiting for an unload ACK from the renderer.

### OnUnloaded

OnUnloaded
~~~cpp
void OnUnloaded();
~~~
 Called when either the Unload() request has been acknowledged or has timed
 out.

### Stop

Stop
~~~cpp
void Stop();
~~~
 Stop the load in progress.

### LifecycleStateImplToString

LifecycleStateImplToString
~~~cpp
static const char* LifecycleStateImplToString(LifecycleStateImpl state);
~~~
 Returns the string corresponding to LifecycleStateImpl, used for logging
 crash keys.

### lifecycle_state

lifecycle_state
~~~cpp
LifecycleStateImpl lifecycle_state() const { return lifecycle_state_; }
~~~

### SetLifecycleState

SetLifecycleState
~~~cpp
void SetLifecycleState(LifecycleStateImpl new_state);
~~~
 Updates the `lifecycle_state_`. This will also notify the delegate
 about `RenderFrameHostStateChanged` when the old and new
 `RenderFrameHost::LifecycleState` changes.


 When the `new_state == LifecycleStateImpl::kActive`, LifecycleStateImpl of
 RenderFrameHost and all its children are also updated to
 `LifecycleStateImpl::kActive`.

### SetHasPendingLifecycleStateUpdate

SetHasPendingLifecycleStateUpdate
~~~cpp
void SetHasPendingLifecycleStateUpdate();
~~~
 Sets |has_pending_lifecycle_state_update_| to true for this
 RenderFrameHost and its children. Called when this RenderFrameHost stops
 being the current one in the RenderFrameHostManager, but its new
 LifecycleStateImpl is not immediately determined. This boolean is reset
 when this RenderFrameHost enters the back-forward-cache or the pending
 deletion list.

### DispatchBeforeUnload

DispatchBeforeUnload
~~~cpp
void DispatchBeforeUnload(BeforeUnloadType type, bool is_reload);
~~~
 Runs the beforeunload handler for this frame and its subframes. |type|
 indicates whether this call is for a navigation or tab close. |is_reload|
 indicates whether the navigation is a reload of the page.  If |type|
 corresponds to tab close and not a navigation, |is_reload| should be
 false.

### SimulateBeforeUnloadCompleted

SimulateBeforeUnloadCompleted
~~~cpp
void SimulateBeforeUnloadCompleted(bool proceed);
~~~
 Simulate beforeunload completion callback on behalf of renderer if it's
 unrenresponsive.

### ShouldDispatchBeforeUnload

ShouldDispatchBeforeUnload
~~~cpp
bool ShouldDispatchBeforeUnload(
      bool check_subframes_only,
      bool* no_dispatch_because_avoid_unnecessary_sync = nullptr);
~~~
 Returns true if a call to DispatchBeforeUnload will actually send the
 BeforeUnload IPC.  This can be called on a main frame or subframe.  If
 |check_subframes_only| is false, it covers handlers for the frame
 itself and all its descendants.  If |check_subframes_only| is true, it
 only checks the frame's descendants but not the frame itself. See
 CheckOrDispatchBeforeUnloadForSubtree() for details on
 |no_dispatch_because_avoid_unnecessary_sync|.

### SetBeforeUnloadTimeoutDelayForTesting

SetBeforeUnloadTimeoutDelayForTesting
~~~cpp
void SetBeforeUnloadTimeoutDelayForTesting(const base::TimeDelta& timeout);
~~~
 Allow tests to override how long to wait for beforeunload completion
 callbacks to be invoked before timing out.

### SetFocusedFrame

SetFocusedFrame
~~~cpp
void SetFocusedFrame();
~~~
 Set this frame as focused in the renderer process.  This supports
 cross-process window.focus() calls.

### AdvanceFocus

AdvanceFocus
~~~cpp
void AdvanceFocus(blink::mojom::FocusType type,
                    RenderFrameProxyHost* source_proxy);
~~~
 Continues sequential focus navigation in this frame. |source_proxy|
 represents the frame that requested a focus change. It must be in the same
 process as this or |nullptr|.

### UpdateAccessibilityMode

UpdateAccessibilityMode
~~~cpp
void UpdateAccessibilityMode();
~~~
 Get the accessibility mode from the delegate and Send a message to the
 renderer process to change the accessibility mode.

### RequestSmartClipExtract

RequestSmartClipExtract
~~~cpp
void RequestSmartClipExtract(ExtractSmartClipDataCallback callback,
                               gfx::Rect rect);
~~~

### OnSmartClipDataExtracted

OnSmartClipDataExtracted
~~~cpp
void OnSmartClipDataExtracted(int32_t callback_id,
                                const std::u16string& text,
                                const std::u16string& html,
                                const gfx::Rect& clip_rect);
~~~

### RequestAXTreeSnapshot

RequestAXTreeSnapshot
~~~cpp
void RequestAXTreeSnapshot(AXTreeSnapshotCallback callback,
                             mojom::SnapshotAccessibilityTreeParamsPtr params);
~~~
 BUILDFLAG(IS_ANDROID)
 Request a one-time snapshot of the accessibility tree without changing
 the accessibility mode.

### AccessibilityReset

AccessibilityReset
~~~cpp
void AccessibilityReset();
~~~
 Resets the accessibility serializer in the renderer.

### SetAccessibilityCallbackForTesting

SetAccessibilityCallbackForTesting
~~~cpp
void SetAccessibilityCallbackForTesting(
      const AccessibilityCallbackForTesting& callback);
~~~
 Turn on accessibility testing. The given callback will be run
 every time an accessibility notification is received from the
 renderer process.

### UpdateAXTreeData

UpdateAXTreeData
~~~cpp
void UpdateAXTreeData();
~~~
 Called when the metadata about the accessibility tree for this frame
 changes due to a browser-side change, as opposed to due to an IPC from
 a renderer.

### browser_accessibility_manager

browser_accessibility_manager
~~~cpp
BrowserAccessibilityManager* browser_accessibility_manager() const {
    return browser_accessibility_manager_.get();
  }
~~~
 Access the BrowserAccessibilityManager if it already exists.

### GetOrCreateBrowserAccessibilityManager

GetOrCreateBrowserAccessibilityManager
~~~cpp
BrowserAccessibilityManager* GetOrCreateBrowserAccessibilityManager();
~~~
 If accessibility is enabled, get the BrowserAccessibilityManager for
 this frame, or create one if it doesn't exist yet, otherwise return
 null.

### set_no_create_browser_accessibility_manager_for_testing

set_no_create_browser_accessibility_manager_for_testing
~~~cpp
void set_no_create_browser_accessibility_manager_for_testing(bool flag) {
    no_create_browser_accessibility_manager_for_testing_ = flag;
  }
~~~

### SetWantErrorMessageStackTrace

SetWantErrorMessageStackTrace
~~~cpp
void SetWantErrorMessageStackTrace();
~~~
 Indicates that this process wants the |untrusted_stack_trace| parameter of
 FrameHost.DidAddMessageToConsole() to be filled in as much as possible for
 log_level == kError messages.

### CommitNavigation

CommitNavigation
~~~cpp
void CommitNavigation(
      NavigationRequest* navigation_request,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      absl::optional<SubresourceLoaderParams> subresource_loader_params,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      blink::mojom::ServiceWorkerContainerInfoForClientPtr container_info,
      const absl::optional<blink::DocumentToken>& document_token,
      const base::UnguessableToken& devtools_navigation_token);
~~~
 Indicates that a navigation is ready to commit and can be
 handled by this RenderFrame.

 |subresource_loader_params| is used in network service land to pass
 the parameters to create a custom subresource loader in the renderer
 process.

### FailedNavigation

FailedNavigation
~~~cpp
void FailedNavigation(
      NavigationRequest* navigation_request,
      const blink::mojom::CommonNavigationParams& common_params,
      const blink::mojom::CommitNavigationParams& commit_params,
      bool has_stale_copy_in_cache,
      int error_code,
      int extended_error_code,
      const absl::optional<std::string>& error_page_content,
      const blink::DocumentToken& document_token);
~~~
 Indicates that a navigation failed and that this RenderFrame should display
 an error page.

### AddResourceTimingEntryForFailedSubframeNavigation

AddResourceTimingEntryForFailedSubframeNavigation
~~~cpp
void AddResourceTimingEntryForFailedSubframeNavigation(
      FrameTreeNode* child_frame,
      base::TimeTicks start_time,
      base::TimeTicks redirect_time,
      const GURL& initial_url,
      const GURL& final_url,
      network::mojom::URLResponseHeadPtr response_head,
      bool allow_response_details,
      const network::URLLoaderCompletionStatus& completion_status);
~~~

### HandleRendererDebugURL

HandleRendererDebugURL
~~~cpp
void HandleRendererDebugURL(const GURL& url);
~~~
 Sends a renderer-debug URL to the renderer process for handling.

### CreateMessageFilterForAssociatedReceiverInternal

CreateMessageFilterForAssociatedReceiverInternal
~~~cpp
std::unique_ptr<mojo::MessageFilter>
  CreateMessageFilterForAssociatedReceiverInternal(
      const char* interface_name,
      BackForwardCacheImpl::MessageHandlingPolicyWhenCached policy);
~~~
 BEGIN IPC REVIEW BOUNDARY: to enforce security review for IPC, these
 methods are defined in render_frame_host_impl_interface_bindings.cc.

 Similar to the public `CreateMessageFilterForAssociatedReceiver()` but
 allows a specific message handling policy to be specified.

### SetUpMojoConnection

SetUpMojoConnection
~~~cpp
void SetUpMojoConnection();
~~~
 Sets up the Mojo connection between this instance and its associated render
 frame.

### TearDownMojoConnection

TearDownMojoConnection
~~~cpp
void TearDownMojoConnection();
~~~
 Tears down the browser-side state relating to the Mojo connection between
 this instance and its associated render frame.

### IsFocused

IsFocused
~~~cpp
bool IsFocused();
~~~
 END IPC REVIEW BOUNDARY
 Returns whether the frame is focused. A frame is considered focused when it
 is the parent chain of the focused frame within the frame tree. In
 addition, its associated RenderWidgetHost has to be focused.

### CreateWebUI

CreateWebUI
~~~cpp
bool CreateWebUI(const GURL& dest_url, int entry_bindings);
~~~
 Creates a WebUI for this RenderFrameHost based on the provided |dest_url|
 if required. Returns true if a new WebUI was created.

 If this is a history navigation its NavigationEntry bindings should be
 provided through |entry_bindings| to allow verifying that they are not
 being set differently this time around. Otherwise |entry_bindings| should
 be set to NavigationEntryImpl::kInvalidBindings so that no checks are done.

### ClearWebUI

ClearWebUI
~~~cpp
void ClearWebUI();
~~~
 Destroys WebUI instance and resets related data.

 This indirectly calls content's embedders and may have arbitrary side
 effect, like deleting `this`.

### GetMojoImageDownloader

GetMojoImageDownloader
~~~cpp
const mojo::Remote<blink::mojom::ImageDownloader>& GetMojoImageDownloader();
~~~
 Returns the Mojo ImageDownloader service.

### GetFindInPage

GetFindInPage
~~~cpp
const mojo::AssociatedRemote<blink::mojom::FindInPage>& GetFindInPage();
~~~
 Returns remote to renderer side FindInPage associated with this frame.

### GetAssociatedLocalFrame

GetAssociatedLocalFrame
~~~cpp
const mojo::AssociatedRemote<blink::mojom::LocalFrame>&
  GetAssociatedLocalFrame();
~~~
 Returns associated remote for the blink::mojom::LocalFrame Mojo interface.

### GetAgentSchedulingGroup

GetAgentSchedulingGroup
~~~cpp
virtual AgentSchedulingGroupHost& GetAgentSchedulingGroup();
~~~
 Returns the AgentSchedulingGroupHost associated with this
 RenderFrameHostImpl.

### GetAssociatedLocalMainFrame

GetAssociatedLocalMainFrame
~~~cpp
virtual blink::mojom::LocalMainFrame* GetAssociatedLocalMainFrame();
~~~
 Returns associated remote for the blink::mojom::LocalMainFrame Mojo
 interface. May be overridden by subclasses, e.g. tests which wish to
 intercept outgoing local main frame messages.

### GetHighPriorityLocalFrame

GetHighPriorityLocalFrame
~~~cpp
const mojo::Remote<blink::mojom::HighPriorityLocalFrame>&
  GetHighPriorityLocalFrame();
~~~
 Returns remote to blink::mojom::HighPriorityLocalFrame Mojo interface. Note
 this interface is highly experimental and is being tested to address
 crbug.com/1042118. It is not an associated interface and may be actively
 reordered. GetAssociatedLocalFrame() should be used in most cases and any
 additional use cases of this interface should probably consider discussing
 with navigation-dev@chromium.org first.

### GetFrameBindingsControl

GetFrameBindingsControl
~~~cpp
const mojo::AssociatedRemote<mojom::FrameBindingsControl>&
  GetFrameBindingsControl();
~~~
 Returns associated remote for the blink::mojom::FrameBindingsControl Mojo
 interface.

### ResetLoadingState

ResetLoadingState
~~~cpp
void ResetLoadingState();
~~~
 Resets the loading state. Following this call, the RenderFrameHost will be
 in a non-loading state.

### permissions_policy

permissions_policy
~~~cpp
const blink::PermissionsPolicy* permissions_policy() const {
    return permissions_policy_.get();
  }
~~~
 Returns the permissions policy which should be enforced on this
 RenderFrame.

### ClearFocusedElement

ClearFocusedElement
~~~cpp
void ClearFocusedElement();
~~~

### has_focused_editable_element

has_focused_editable_element
~~~cpp
bool has_focused_editable_element() const {
    return has_focused_editable_element_;
  }
~~~

### BindDevToolsAgent

BindDevToolsAgent
~~~cpp
void BindDevToolsAgent(
      mojo::PendingAssociatedRemote<blink::mojom::DevToolsAgentHost> host,
      mojo::PendingAssociatedReceiver<blink::mojom::DevToolsAgent> receiver);
~~~
 Binds a DevToolsAgent interface for debugging.

### GetJavaRenderFrameHost

GetJavaRenderFrameHost
~~~cpp
base::android::ScopedJavaLocalRef<jobject> GetJavaRenderFrameHost() override;
~~~

### GetJavaInterfaces

GetJavaInterfaces
~~~cpp
service_manager::InterfaceProvider* GetJavaInterfaces() override;
~~~

### SetVisibilityForChildViews

SetVisibilityForChildViews
~~~cpp
void SetVisibilityForChildViews(bool visible);
~~~
 Propagates the visibility state along the immediate local roots by calling
 RenderWidgetHostViewChildFrame::Show()/Hide(). Calling this on a pending
 or speculative RenderFrameHost (that has not committed) should be avoided.

### GetTopFrameToken

GetTopFrameToken
~~~cpp
const blink::LocalFrameToken& GetTopFrameToken();
~~~

### GetOverlayRoutingToken

GetOverlayRoutingToken
~~~cpp
const base::UnguessableToken& GetOverlayRoutingToken() const {
    return frame_token_.value();
  }
~~~
 Returns an unguessable token for this RFHI.  This provides a temporary way
 to identify a RenderFrameHost that's compatible with IPC.  Else, one needs
 to send pid + RoutingID, but one cannot send pid.  One can get it from the
 channel, but this makes it much harder to get wrong.

 Once media switches to mojo, we should be able to remove this in favor of
 sending a mojo overlay factory. Note that this value should only be shared
 with the renderer process that hosts the frame and other utility processes;
 it should specifically *not* be shared with any renderer processes that
 do not host the frame.

### BindBrowserInterfaceBrokerReceiver

BindBrowserInterfaceBrokerReceiver
~~~cpp
void BindBrowserInterfaceBrokerReceiver(
      mojo::PendingReceiver<blink::mojom::BrowserInterfaceBroker>);
~~~
 Binds the receiver end of the BrowserInterfaceBroker interface through
 which services provided by this RenderFrameHost are exposed to the
 corresponding RenderFrame. The caller is responsible for plumbing the
 client end to the renderer process.

### BindAssociatedInterfaceProviderReceiver

BindAssociatedInterfaceProviderReceiver
~~~cpp
void BindAssociatedInterfaceProviderReceiver(
      mojo::PendingAssociatedReceiver<
          blink::mojom::AssociatedInterfaceProvider>);
~~~
 Binds the receiver end of the `AssociatedInterfaceProvider` interface. This
 is called whenever we generate a remote/receiver pair for this interface
 and the remote end gets passed to the renderer.

### BindDomOperationControllerHostReceiver

BindDomOperationControllerHostReceiver
~~~cpp
void BindDomOperationControllerHostReceiver(
      mojo::PendingAssociatedReceiver<mojom::DomAutomationControllerHost>
          receiver);
~~~
 Binds the receiver end of the DomOperationControllerHost interface through
 which services provided by this RenderFrameHost are exposed to the
 corresponding RenderFrame. The caller is responsible for plumbing the
 client end to the renderer process.

### frame_host_receiver_for_testing

frame_host_receiver_for_testing
~~~cpp
mojo::AssociatedReceiver<mojom::FrameHost>&
  frame_host_receiver_for_testing() {
    return frame_host_associated_receiver_;
  }
~~~
 Exposed so that tests can swap the implementation and intercept calls.

### local_frame_host_receiver_for_testing

local_frame_host_receiver_for_testing
~~~cpp
mojo::AssociatedReceiver<blink::mojom::LocalFrameHost>&
  local_frame_host_receiver_for_testing() {
    return local_frame_host_receiver_;
  }
~~~
 Exposed so that tests can swap the implementation and intercept calls.

### local_main_frame_host_receiver_for_testing

local_main_frame_host_receiver_for_testing
~~~cpp
mojo::AssociatedReceiver<blink::mojom::LocalMainFrameHost>&
  local_main_frame_host_receiver_for_testing() {
    return local_main_frame_host_receiver_;
  }
~~~
 Exposed so that tests can swap the implementation and intercept calls.

### browser_interface_broker_receiver_for_testing

browser_interface_broker_receiver_for_testing
~~~cpp
mojo::Receiver<blink::mojom::BrowserInterfaceBroker>&
  browser_interface_broker_receiver_for_testing() {
    return broker_receiver_;
  }
~~~
 Exposed so that tests can swap the implementation and intercept calls.

### SetKeepAliveTimeoutForTesting

SetKeepAliveTimeoutForTesting
~~~cpp
void SetKeepAliveTimeoutForTesting(base::TimeDelta timeout);
~~~

### active_sandbox_flags

active_sandbox_flags
~~~cpp
network::mojom::WebSandboxFlags active_sandbox_flags() {
    return policy_container_host_->sandbox_flags();
  }
~~~

### is_mhtml_document

is_mhtml_document
~~~cpp
bool is_mhtml_document() { return is_mhtml_document_; }
~~~

### is_overriding_user_agent

is_overriding_user_agent
~~~cpp
bool is_overriding_user_agent() { return is_overriding_user_agent_; }
~~~

### DidReceiveUserActivation

DidReceiveUserActivation
~~~cpp
void DidReceiveUserActivation();
~~~
 Notifies the render frame that |frame_tree_node_| has received user
 activation. May be invoked multiple times. This is called both for the
 actual frame that saw user activation and any ancestor frames that might
 also be activated as part of UserActivationV2 requirements. Does not
 include frames activated by the same-origin visibility heuristic, see
 `UserActivationState` for details.

### MaybeIsolateForUserActivation

MaybeIsolateForUserActivation
~~~cpp
void MaybeIsolateForUserActivation();
~~~
 Apply any isolation policies, such as site isolation triggered by COOP
 headers, that might be triggered when a particular frame has just seen a
 user activation. Called whenever this frame sees a user activation (which
 may or may not be the first activation in this frame).

### frame_size

frame_size
~~~cpp
const absl::optional<gfx::Size>& frame_size() const { return frame_size_; }
~~~
 Returns the current size for this frame.

### SetSubframeUnloadTimeoutForTesting

SetSubframeUnloadTimeoutForTesting
~~~cpp
void SetSubframeUnloadTimeoutForTesting(const base::TimeDelta& timeout);
~~~
 Allow tests to override the timeout used to keep subframe processes alive
 for unload handler processing.

### AudioContextPlaybackStarted

AudioContextPlaybackStarted
~~~cpp
void AudioContextPlaybackStarted(int audio_context_id);
~~~
 Called when the WebAudio AudioContext given by |audio_context_id| has
 started (or stopped) playing audible audio.

### AudioContextPlaybackStopped

AudioContextPlaybackStopped
~~~cpp
void AudioContextPlaybackStopped(int audio_context_id);
~~~

### DidEnterBackForwardCache

DidEnterBackForwardCache
~~~cpp
void DidEnterBackForwardCache();
~~~
 Called when this RenderFrameHostImpl enters the BackForwardCache, the
 document enters in a "Frozen" state where no Javascript can run.

### WillLeaveBackForwardCache

WillLeaveBackForwardCache
~~~cpp
void WillLeaveBackForwardCache();
~~~
 Called when this RenderFrameHostImpl leaves the BackForwardCache. This
 occurs immediately before a restored document is committed.

### TakeLastCommitParams

TakeLastCommitParams
~~~cpp
mojom::DidCommitProvisionalLoadParamsPtr TakeLastCommitParams();
~~~
 Take ownership over the DidCommitProvisionalLoadParams that were last used
 to commit this navigation. This is used by the BackForwardCache to
 re-commit when navigating to a restored page.

### StartBackForwardCacheEvictionTimer

StartBackForwardCacheEvictionTimer
~~~cpp
void StartBackForwardCacheEvictionTimer();
~~~
 Start a timer that will evict this RenderFrameHost from the
 BackForwardCache after time to live.

### IsBackForwardCacheDisabled

IsBackForwardCacheDisabled
~~~cpp
bool IsBackForwardCacheDisabled() const;
~~~

### DisableBackForwardCache

DisableBackForwardCache
~~~cpp
void DisableBackForwardCache(
      BackForwardCache::DisabledReason reason,
      absl::optional<ukm::SourceId> source_id = absl::nullopt);
~~~
 Prevents this frame (along with its parents/children) from being added to
 the BackForwardCache. If the frame is already in the cache an eviction is
 triggered.

 For what `source_id` means and when it's set, see `DisabledReasonsMap` from
 `back_forward_cache_can_store_document_result.h`.

### is_evicted_from_back_forward_cache

is_evicted_from_back_forward_cache
~~~cpp
bool is_evicted_from_back_forward_cache() {
    return is_evicted_from_back_forward_cache_;
  }
~~~

### back_forward_cache_disabled_reasons

back_forward_cache_disabled_reasons
~~~cpp
const BackForwardCacheCanStoreDocumentResult::DisabledReasonsMap&
  back_forward_cache_disabled_reasons() const {
    return back_forward_cache_disabled_reasons_;
  }
~~~

### was_restored_from_back_forward_cache_for_debugging

was_restored_from_back_forward_cache_for_debugging
~~~cpp
bool was_restored_from_back_forward_cache_for_debugging() {
    return was_restored_from_back_forward_cache_for_debugging_;
  }
~~~

### DisableProactiveBrowsingInstanceSwapForTesting

DisableProactiveBrowsingInstanceSwapForTesting
~~~cpp
void DisableProactiveBrowsingInstanceSwapForTesting();
~~~
 Prevents this frame to do a proactive BrowsingInstance swap (for all
 navigations on this frame - cross-site and same-site).

### HasTestDisabledProactiveBrowsingInstanceSwap

HasTestDisabledProactiveBrowsingInstanceSwap
~~~cpp
bool HasTestDisabledProactiveBrowsingInstanceSwap() const {
    return has_test_disabled_proactive_browsing_instance_swap_;
  }
~~~

### AddServiceWorkerContainerHost

AddServiceWorkerContainerHost
~~~cpp
void AddServiceWorkerContainerHost(
      const std::string& uuid,
      base::WeakPtr<ServiceWorkerContainerHost> host);
~~~

### RemoveServiceWorkerContainerHost

RemoveServiceWorkerContainerHost
~~~cpp
void RemoveServiceWorkerContainerHost(const std::string& uuid);
~~~

### GetLastCommittedServiceWorkerHost

GetLastCommittedServiceWorkerHost
~~~cpp
base::WeakPtr<ServiceWorkerContainerHost> GetLastCommittedServiceWorkerHost();
~~~
 Returns the last committed ServiceWorkerContainerHost of this frame.

### OnGrantedMediaStreamAccess

OnGrantedMediaStreamAccess
~~~cpp
void OnGrantedMediaStreamAccess();
~~~
 Called to taint |this| so the pages which have requested MediaStream
 (audio/video/etc capture stream) access would not enter BackForwardCache.

### was_granted_media_access

was_granted_media_access
~~~cpp
bool was_granted_media_access() { return was_granted_media_access_; }
~~~

### GetNavigationClientFromInterfaceProvider

GetNavigationClientFromInterfaceProvider
~~~cpp
mojo::AssociatedRemote<mojom::NavigationClient>
  GetNavigationClientFromInterfaceProvider();
~~~
 Request a new NavigationClient interface from the renderer and returns the
 ownership of the mojo::AssociatedRemote. This is intended for use by the
 NavigationRequest.

### NavigationRequestCancelled

NavigationRequestCancelled
~~~cpp
void NavigationRequestCancelled(NavigationRequest* navigation_request);
~~~
 Called to signify the RenderFrameHostImpl that one of its ongoing
 NavigationRequest's has been cancelled.

### OnPortalActivated

OnPortalActivated
~~~cpp
void OnPortalActivated(
      std::unique_ptr<Portal> predecessor,
      mojo::PendingAssociatedRemote<blink::mojom::Portal> pending_portal,
      mojo::PendingAssociatedReceiver<blink::mojom::PortalClient>
          client_receiver,
      blink::TransferableMessage data,
      uint64_t trace_id,
      base::OnceCallback<void(blink::mojom::PortalActivateResult)> callback);
~~~
 Called on the main frame of a page embedded in a Portal when it is
 activated. The frame has the option to adopt the previous page,
 |predecessor|, as a portal. The activation can optionally include a message
 |data| dispatched with the PortalActivateEvent. The |trace_id| is used for
 generating flow events.

### OnPortalCreatedForTesting

OnPortalCreatedForTesting
~~~cpp
void OnPortalCreatedForTesting(std::unique_ptr<Portal> portal);
~~~
 Called in tests that synthetically create portals but need them to be
 properly associated with the owning RenderFrameHost.

### FindPortalByToken

FindPortalByToken
~~~cpp
Portal* FindPortalByToken(const blink::PortalToken& portal_token);
~~~
 Look up a portal by its token (as received from the renderer process).

### GetPortals

GetPortals
~~~cpp
std::vector<Portal*> GetPortals() const;
~~~
 Return portals owned by |this|.

### DestroyPortal

DestroyPortal
~~~cpp
void DestroyPortal(Portal* portal);
~~~
 Called when a Portal needs to be destroyed.

### GetFencedFrames

GetFencedFrames
~~~cpp
std::vector<FencedFrame*> GetFencedFrames() const;
~~~
 Return fenced frames owned by |this|. The returned vector is in the order
 the fenced frames were added (most recent at end).

### DestroyFencedFrame

DestroyFencedFrame
~~~cpp
void DestroyFencedFrame(FencedFrame& fenced_frame);
~~~
 Called when a fenced frame needs to be destroyed.

### ForwardMessageFromHost

ForwardMessageFromHost
~~~cpp
void ForwardMessageFromHost(blink::TransferableMessage message,
                              const url::Origin& source_origin);
~~~
 Called on the main frame of a page embedded in a Portal to forward a
 message from the host of a portal.

### visibility

visibility
~~~cpp
blink::mojom::FrameVisibility visibility() const { return visibility_; }
~~~

### SetCommitCallbackInterceptorForTesting

SetCommitCallbackInterceptorForTesting
~~~cpp
void SetCommitCallbackInterceptorForTesting(
      CommitCallbackInterceptor* interceptor);
~~~
 Sets the specified |interceptor|. The caller is responsible for ensuring
 |interceptor| remains live as long as it is set as the interceptor.

### SetCreateNewPopupCallbackForTesting

SetCreateNewPopupCallbackForTesting
~~~cpp
void SetCreateNewPopupCallbackForTesting(
      const CreateNewPopupWidgetCallbackForTesting& callback);
~~~
 Set a callback to listen to the |CreateNewPopupWidget| for testing.

### SetUnloadACKCallbackForTesting

SetUnloadACKCallbackForTesting
~~~cpp
void SetUnloadACKCallbackForTesting(
      const UnloadACKCallbackForTesting& callback);
~~~
 Set a callback to listen to the |OnUnloadACK| for testing.

### PostMessageEvent

PostMessageEvent
~~~cpp
void PostMessageEvent(
      const absl::optional<blink::RemoteFrameToken>& source_token,
      const std::u16string& source_origin,
      const std::u16string& target_origin,
      blink::TransferableMessage message);
~~~
 Posts a message from a frame in another process to the current renderer.

### SwapIn

SwapIn
~~~cpp
void SwapIn();
~~~
 Requests to swap the current frame into the frame tree, replacing the
 `blink::RemoteFrame` it is associated with.

### IsTestRenderFrameHost

IsTestRenderFrameHost
~~~cpp
virtual bool IsTestRenderFrameHost() const;
~~~
 Manual RTTI to ensure safe downcasts in tests.

### GetBackForwardCacheDisablingFeatures

GetBackForwardCacheDisablingFeatures
~~~cpp
BackForwardCacheDisablingFeatures GetBackForwardCacheDisablingFeatures()
      const;
~~~
 BackForwardCache disabling feature for |this|, used in determing |this|
 frame's BackForwardCache eligibility.

 See comments at |renderer_reported_bfcache_disabling_features_| and
 |browser_reported_bfcache_disabling_features_|.

### SetBackForwardCacheDisablingFeaturesCallbackForTesting

SetBackForwardCacheDisablingFeaturesCallbackForTesting
~~~cpp
void SetBackForwardCacheDisablingFeaturesCallbackForTesting(
      BackForwardCacheDisablingFeaturesCallback callback) {
    back_forward_cache_disabling_features_callback_for_testing_ = callback;
  }
~~~

### EnsurePrefetchedSignedExchangeCache

EnsurePrefetchedSignedExchangeCache
~~~cpp
scoped_refptr<PrefetchedSignedExchangeCache>
  EnsurePrefetchedSignedExchangeCache();
~~~
 Returns a PrefetchedSignedExchangeCache which is attached to |this|.

### ClearPrefetchedSignedExchangeCache

ClearPrefetchedSignedExchangeCache
~~~cpp
void ClearPrefetchedSignedExchangeCache();
~~~
 Clears the entries in the PrefetchedSignedExchangeCache if exists.

### set_did_stop_loading_callback_for_testing

set_did_stop_loading_callback_for_testing
~~~cpp
void set_did_stop_loading_callback_for_testing(base::OnceClosure callback) {
    did_stop_loading_callback_ = std::move(callback);
  }
~~~

### RegisterBackForwardCacheDisablingNonStickyFeature

RegisterBackForwardCacheDisablingNonStickyFeature
~~~cpp
BackForwardCacheDisablingFeatureHandle
  RegisterBackForwardCacheDisablingNonStickyFeature(
      BackForwardCacheDisablingFeature feature);
~~~
 A feature that blocks back/forward cache is used. This function is used for
 non sticky blocking features.

### OnBackForwardCacheDisablingStickyFeatureUsed

OnBackForwardCacheDisablingStickyFeatureUsed
~~~cpp
void OnBackForwardCacheDisablingStickyFeatureUsed(
      BackForwardCacheDisablingFeature feature);
~~~
 A feature that blocks back/forward cache is used. This function is used for
 sticky blocking features.

### IsFrozen

IsFrozen
~~~cpp
bool IsFrozen();
~~~
 Returns true if the frame is frozen.

### SetMojomFrameRemote

SetMojomFrameRemote
~~~cpp
void SetMojomFrameRemote(mojo::PendingAssociatedRemote<mojom::Frame>);
~~~
 Set the `frame_` for sending messages to the renderer process.

### GetAudioContextManager

GetAudioContextManager
~~~cpp
void GetAudioContextManager(
      mojo::PendingReceiver<blink::mojom::AudioContextManager> receiver);
~~~

### GetFileSystemManager

GetFileSystemManager
~~~cpp
void GetFileSystemManager(
      mojo::PendingReceiver<blink::mojom::FileSystemManager> receiver);
~~~

### GetGeolocationService

GetGeolocationService
~~~cpp
void GetGeolocationService(
      mojo::PendingReceiver<blink::mojom::GeolocationService> receiver);
~~~

### GetDeviceInfoService

GetDeviceInfoService
~~~cpp
void GetDeviceInfoService(
      mojo::PendingReceiver<blink::mojom::DeviceAPIService> receiver);
~~~

### GetManagedConfigurationService

GetManagedConfigurationService
~~~cpp
void GetManagedConfigurationService(
      mojo::PendingReceiver<blink::mojom::ManagedConfigurationService>
          receiver);
~~~

### GetFontAccessManager

GetFontAccessManager
~~~cpp
void GetFontAccessManager(
      mojo::PendingReceiver<blink::mojom::FontAccessManager> receiver);
~~~

### GetFileSystemAccessManager

GetFileSystemAccessManager
~~~cpp
void GetFileSystemAccessManager(
      mojo::PendingReceiver<blink::mojom::FileSystemAccessManager> receiver);
~~~

### GetHidService

GetHidService
~~~cpp
void GetHidService(mojo::PendingReceiver<blink::mojom::HidService> receiver);
~~~

### BindSerialService

BindSerialService
~~~cpp
void BindSerialService(
      mojo::PendingReceiver<blink::mojom::SerialService> receiver);
~~~

### GetSmartCardService

GetSmartCardService
~~~cpp
void GetSmartCardService(
      mojo::PendingReceiver<blink::mojom::SmartCardService> receiver);
~~~

### GetIdleManager

GetIdleManager
~~~cpp
IdleManagerImpl* GetIdleManager();
~~~

### BindIdleManager

BindIdleManager
~~~cpp
void BindIdleManager(
      mojo::PendingReceiver<blink::mojom::IdleManager> receiver);
~~~

### GetPresentationService

GetPresentationService
~~~cpp
void GetPresentationService(
      mojo::PendingReceiver<blink::mojom::PresentationService> receiver);
~~~

### GetPresentationServiceForTesting

GetPresentationServiceForTesting
~~~cpp
PresentationServiceImpl& GetPresentationServiceForTesting();
~~~

### GetSpeechSynthesis

GetSpeechSynthesis
~~~cpp
void GetSpeechSynthesis(
      mojo::PendingReceiver<blink::mojom::SpeechSynthesis> receiver);
~~~

### CreateLockManager

CreateLockManager
~~~cpp
void CreateLockManager(
      mojo::PendingReceiver<blink::mojom::LockManager> receiver);
~~~

### CreateIDBFactory

CreateIDBFactory
~~~cpp
void CreateIDBFactory(
      mojo::PendingReceiver<blink::mojom::IDBFactory> receiver);
~~~

### CreateBucketManagerHost

CreateBucketManagerHost
~~~cpp
void CreateBucketManagerHost(
      mojo::PendingReceiver<blink::mojom::BucketManagerHost> receiver);
~~~

### GetSensorProvider

GetSensorProvider
~~~cpp
void GetSensorProvider(
      mojo::PendingReceiver<device::mojom::SensorProvider> receiver);
~~~

### CreatePermissionService

CreatePermissionService
~~~cpp
void CreatePermissionService(
      mojo::PendingReceiver<blink::mojom::PermissionService> receiver);
~~~

### CreatePaymentManager

CreatePaymentManager
~~~cpp
void CreatePaymentManager(
      mojo::PendingReceiver<payments::mojom::PaymentManager> receiver);
~~~

### CreateWebBluetoothService

CreateWebBluetoothService
~~~cpp
void CreateWebBluetoothService(
      mojo::PendingReceiver<blink::mojom::WebBluetoothService> receiver);
~~~

### GetWebAuthenticationService

GetWebAuthenticationService
~~~cpp
void GetWebAuthenticationService(
      mojo::PendingReceiver<blink::mojom::Authenticator> receiver);
~~~

### GetVirtualAuthenticatorManager

GetVirtualAuthenticatorManager
~~~cpp
void GetVirtualAuthenticatorManager(
      mojo::PendingReceiver<blink::test::mojom::VirtualAuthenticatorManager>
          receiver);
~~~

### GetPushMessaging

GetPushMessaging
~~~cpp
void GetPushMessaging(
      mojo::PendingReceiver<blink::mojom::PushMessaging> receiver);
~~~

### CreateDedicatedWorkerHostFactory

CreateDedicatedWorkerHostFactory
~~~cpp
void CreateDedicatedWorkerHostFactory(
      mojo::PendingReceiver<blink::mojom::DedicatedWorkerHostFactory> receiver);
~~~

### CreateWebTransportConnector

CreateWebTransportConnector
~~~cpp
void CreateWebTransportConnector(
      mojo::PendingReceiver<blink::mojom::WebTransportConnector> receiver);
~~~

### CreateNotificationService

CreateNotificationService
~~~cpp
void CreateNotificationService(
      mojo::PendingReceiver<blink::mojom::NotificationService> receiver);
~~~

### CreateInstalledAppProvider

CreateInstalledAppProvider
~~~cpp
void CreateInstalledAppProvider(
      mojo::PendingReceiver<blink::mojom::InstalledAppProvider> receiver);
~~~

### CreateCodeCacheHost

CreateCodeCacheHost
~~~cpp
void CreateCodeCacheHost(
      mojo::PendingReceiver<blink::mojom::CodeCacheHost> receiver);
~~~

### CreateCodeCacheHostWithIsolationKey

CreateCodeCacheHostWithIsolationKey
~~~cpp
void CreateCodeCacheHostWithIsolationKey(
      mojo::PendingReceiver<blink::mojom::CodeCacheHost> receiver,
      const net::NetworkIsolationKey& nik);
~~~

### BindNFCReceiver

BindNFCReceiver
~~~cpp
void BindNFCReceiver(mojo::PendingReceiver<device::mojom::NFC> receiver);
~~~

### BindCacheStorage

BindCacheStorage
~~~cpp
void BindCacheStorage(
      mojo::PendingReceiver<blink::mojom::CacheStorage> receiver);
~~~
 Binds a `CacheStorage` object for the default bucket.

### BindBlobUrlStoreReceiver

BindBlobUrlStoreReceiver
~~~cpp
void BindBlobUrlStoreReceiver(
      mojo::PendingReceiver<blink::mojom::BlobURLStore> receiver);
~~~
 For threaded worklets we expose an interface via BrowserInterfaceBrokers to
 bind `receiver` to a `BlobURLStore` instance, which implements the Blob URL
 API in the browser process. Note that this is only exposed when the
 kSupportPartitionedBlobUrl flag is enabled.

### BindInputInjectorReceiver

BindInputInjectorReceiver
~~~cpp
void BindInputInjectorReceiver(
      mojo::PendingReceiver<mojom::InputInjector> receiver);
~~~

### BindWebOTPServiceReceiver

BindWebOTPServiceReceiver
~~~cpp
void BindWebOTPServiceReceiver(
      mojo::PendingReceiver<blink::mojom::WebOTPService> receiver);
~~~

### BindFederatedAuthRequestReceiver

BindFederatedAuthRequestReceiver
~~~cpp
void BindFederatedAuthRequestReceiver(
      mojo::PendingReceiver<blink::mojom::FederatedAuthRequest> receiver);
~~~

### BindRestrictedCookieManager

BindRestrictedCookieManager
~~~cpp
void BindRestrictedCookieManager(
      mojo::PendingReceiver<network::mojom::RestrictedCookieManager> receiver);
~~~

### BindRestrictedCookieManagerWithOrigin

BindRestrictedCookieManagerWithOrigin
~~~cpp
void BindRestrictedCookieManagerWithOrigin(
      mojo::PendingReceiver<network::mojom::RestrictedCookieManager> receiver,
      const net::IsolationInfo& isolation_info,
      const url::Origin& origin,
      net::CookieSettingOverrides cookie_setting_overrides);
~~~

### BindTrustTokenQueryAnswerer

BindTrustTokenQueryAnswerer
~~~cpp
void BindTrustTokenQueryAnswerer(
      mojo::PendingReceiver<network::mojom::TrustTokenQueryAnswerer> receiver);
~~~
 Requires the following preconditions, reporting a bad message otherwise.


 1. This frame's top-frame origin must be potentially trustworthy and
 have scheme HTTP or HTTPS. (See network::SuitableTrustTokenOrigin's class
 comment for the rationale.)

 2. Private State Tokens must be enabled
 (network::features::kPrivateStateTokens).


 3. This frame's origin must be potentially trustworthy.

### CreateWebUsbService

CreateWebUsbService
~~~cpp
void CreateWebUsbService(
      mojo::PendingReceiver<blink::mojom::WebUsbService> receiver);
~~~
 Creates connections to WebUSB interfaces bound to this frame.

### CreateWebSocketConnector

CreateWebSocketConnector
~~~cpp
void CreateWebSocketConnector(
      mojo::PendingReceiver<blink::mojom::WebSocketConnector> receiver);
~~~

### BindMediaInterfaceFactoryReceiver

BindMediaInterfaceFactoryReceiver
~~~cpp
void BindMediaInterfaceFactoryReceiver(
      mojo::PendingReceiver<media::mojom::InterfaceFactory> receiver);
~~~

### BindMediaMetricsProviderReceiver

BindMediaMetricsProviderReceiver
~~~cpp
void BindMediaMetricsProviderReceiver(
      mojo::PendingReceiver<media::mojom::MediaMetricsProvider> receiver);
~~~

### BindMediaRemoterFactoryReceiver

BindMediaRemoterFactoryReceiver
~~~cpp
void BindMediaRemoterFactoryReceiver(
      mojo::PendingReceiver<media::mojom::RemoterFactory> receiver);
~~~

### CreateAudioInputStreamFactory

CreateAudioInputStreamFactory
~~~cpp
void CreateAudioInputStreamFactory(
      mojo::PendingReceiver<blink::mojom::RendererAudioInputStreamFactory>
          receiver);
~~~

### CreateAudioOutputStreamFactory

CreateAudioOutputStreamFactory
~~~cpp
void CreateAudioOutputStreamFactory(
      mojo::PendingReceiver<blink::mojom::RendererAudioOutputStreamFactory>
          receiver);
~~~

### GetFeatureObserver

GetFeatureObserver
~~~cpp
void GetFeatureObserver(
      mojo::PendingReceiver<blink::mojom::FeatureObserver> receiver);
~~~

### BindRenderAccessibilityHost

BindRenderAccessibilityHost
~~~cpp
void BindRenderAccessibilityHost(
      mojo::PendingReceiver<blink::mojom::RenderAccessibilityHost> receiver);
~~~

### BindNonAssociatedLocalFrameHost

BindNonAssociatedLocalFrameHost
~~~cpp
void BindNonAssociatedLocalFrameHost(
      mojo::PendingReceiver<blink::mojom::NonAssociatedLocalFrameHost>
          receiver);
~~~

### CreateRuntimeFeatureStateController

CreateRuntimeFeatureStateController
~~~cpp
void CreateRuntimeFeatureStateController(
      mojo::PendingReceiver<blink::mojom::RuntimeFeatureStateController>
          receiver);
~~~

### CancelPrerendering

CancelPrerendering
~~~cpp
bool CancelPrerendering(const PrerenderCancellationReason& reason);
~~~
 Prerender2:
 Tells PrerenderHostRegistry to cancel the prerendering of the page this
 frame is in, which destroys this frame.

 Returns true if a prerender was canceled. Does nothing and returns false if
 `this` is not prerendered.

### CancelPrerenderingByMojoBinderPolicy

CancelPrerenderingByMojoBinderPolicy
~~~cpp
void CancelPrerenderingByMojoBinderPolicy(const std::string& interface_name);
~~~
 Called by MojoBinderPolicyApplier when it receives a kCancel interface.

### RendererWillActivateForPrerendering

RendererWillActivateForPrerendering
~~~cpp
void RendererWillActivateForPrerendering();
~~~
 Called when the Activate IPC is sent to the renderer. Puts the
 MojoPolicyBinderApplier in "loose" mode via PrepareToGrantAll() until
 DidActivateForPrerending() is called.

### RendererDidActivateForPrerendering

RendererDidActivateForPrerendering
~~~cpp
void RendererDidActivateForPrerendering();
~~~
 Prerender2:
 Called when the Activate IPC is acknowledged by the renderer. Relinquishes
 the MojoPolicyBinderApplier.

### cross_origin_embedder_policy

cross_origin_embedder_policy
~~~cpp
const network::CrossOriginEmbedderPolicy& cross_origin_embedder_policy()
      const {
    return policy_container_host_->cross_origin_embedder_policy();
  }
~~~
 https://mikewest.github.io/corpp/#initialize-embedder-policy-for-global
### coep_reporter

coep_reporter
~~~cpp
CrossOriginEmbedderPolicyReporter* coep_reporter() {
    return coep_reporter_.get();
  }
~~~

### SetCrossOriginOpenerPolicyReporter

SetCrossOriginOpenerPolicyReporter
~~~cpp
void SetCrossOriginOpenerPolicyReporter(
      std::unique_ptr<CrossOriginOpenerPolicyReporter> coop_reporter);
~~~

### cross_origin_opener_policy

cross_origin_opener_policy
~~~cpp
network::CrossOriginOpenerPolicy cross_origin_opener_policy() const {
    return policy_container_host_->cross_origin_opener_policy();
  }
~~~
 Semi-formal definition of COOP:
 https://gist.github.com/annevk/6f2dd8c79c77123f39797f6bdac43f3e
### coop_access_report_manager

coop_access_report_manager
~~~cpp
CrossOriginOpenerPolicyAccessReportManager* coop_access_report_manager() {
    return &coop_access_report_manager_;
  }
~~~

### virtual_browsing_context_group

virtual_browsing_context_group
~~~cpp
int virtual_browsing_context_group() const {
    return virtual_browsing_context_group_;
  }
~~~

### soap_by_default_virtual_browsing_context_group

soap_by_default_virtual_browsing_context_group
~~~cpp
int soap_by_default_virtual_browsing_context_group() const {
    return soap_by_default_virtual_browsing_context_group_;
  }
~~~

### required_csp

required_csp
~~~cpp
const network::mojom::ContentSecurityPolicy* required_csp() {
    return required_csp_.get();
  }
~~~

### IsCredentialless

IsCredentialless
~~~cpp
bool IsCredentialless() const override;
~~~

### IsLastCrossDocumentNavigationStartedByUser

IsLastCrossDocumentNavigationStartedByUser
~~~cpp
bool IsLastCrossDocumentNavigationStartedByUser() const override;
~~~

### is_fenced_frame_root_originating_from_opaque_url

is_fenced_frame_root_originating_from_opaque_url
~~~cpp
bool is_fenced_frame_root_originating_from_opaque_url() const {
    return is_fenced_frame_root_originating_from_opaque_url_;
  }
~~~

### policy_container_host

policy_container_host
~~~cpp
PolicyContainerHost* policy_container_host() {
    return policy_container_host_.get();
  }
~~~

### SetPolicyContainerForEarlyCommitAfterCrash

SetPolicyContainerForEarlyCommitAfterCrash
~~~cpp
void SetPolicyContainerForEarlyCommitAfterCrash(
      scoped_refptr<PolicyContainerHost> policy_container_host);
~~~
 This is used by RenderFrameHostManager to ensure the replacement
 RenderFrameHost is properly initialized when performing an early commit
 as a recovery for a crashed frame.

 TODO(https://crbug.com/1072817): Remove this logic when removing the
 early commit.

### commit_navigation_sent_counter

commit_navigation_sent_counter
~~~cpp
int commit_navigation_sent_counter() {
    return commit_navigation_sent_counter_;
  }
~~~
 A counter which is incremented by one every time `CommitNavigation` or
 `CommitFailedNavigation` is sent to the renderer.

### DidCommitPageActivation

DidCommitPageActivation
~~~cpp
void DidCommitPageActivation(NavigationRequest* committing_navigation_request,
                               mojom::DidCommitProvisionalLoadParamsPtr params);
~~~
 This function mimics DidCommitProvisionalLoad for page activation
 (back-forward cache restore or prerender activation).

### has_unload_handler

has_unload_handler
~~~cpp
bool has_unload_handler() const { return has_unload_handler_; }
~~~

### has_committed_any_navigation

has_committed_any_navigation
~~~cpp
bool has_committed_any_navigation() const {
    return has_committed_any_navigation_;
  }
~~~

### has_navigate_event_handler

has_navigate_event_handler
~~~cpp
bool has_navigate_event_handler() const {
    return has_navigate_event_handler_;
  }
~~~
 Whether the document in this frame currently has a navigate event handler
 registered.

### must_be_replaced

must_be_replaced
~~~cpp
bool must_be_replaced() const { return must_be_replaced_; }
~~~
 Return true if the process this RenderFrameHost is using has crashed and we
 are replacing RenderFrameHosts for crashed frames rather than reusing them.


 This is not exactly the opposite of IsRenderFrameLive().

 IsRenderFrameLive() is false when the RenderProcess died, but it is also
 false when it hasn't been initialized.

### reset_must_be_replaced

reset_must_be_replaced
~~~cpp
void reset_must_be_replaced() { must_be_replaced_ = false; }
~~~
 Resets the must_be_replaced after the RFH has been reinitialized. Do not
 add any more usages of this.

 TODO(https://crbug.com/936696): Remove this.

### renderer_exit_count

renderer_exit_count
~~~cpp
int renderer_exit_count() const { return renderer_exit_count_; }
~~~

### UpdateSubresourceLoaderFactories

UpdateSubresourceLoaderFactories
~~~cpp
void UpdateSubresourceLoaderFactories();
~~~
 Re-creates loader factories and pushes them to |RenderFrame|.

 Used in case we need to add or remove intercepting proxies to the
 running renderer, or in case of Network Service connection errors.

### CreateCrossOriginPrefetchLoaderFactoryBundle

CreateCrossOriginPrefetchLoaderFactoryBundle
~~~cpp
std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
  CreateCrossOriginPrefetchLoaderFactoryBundle();
~~~

### GetBackForwardCacheMetrics

GetBackForwardCacheMetrics
~~~cpp
BackForwardCacheMetrics* GetBackForwardCacheMetrics();
~~~
 Returns the BackForwardCacheMetrics associated with the last
 NavigationEntry this RenderFrameHostImpl committed.

### GetMediaDeviceIDSaltBase

GetMediaDeviceIDSaltBase
~~~cpp
const std::string& GetMediaDeviceIDSaltBase() const {
    return media_device_id_salt_base_;
  }
~~~
 Returns a base salt used to generate frame-specific IDs for media-device
 enumerations.

### set_inner_tree_main_frame_tree_node_id

set_inner_tree_main_frame_tree_node_id
~~~cpp
void set_inner_tree_main_frame_tree_node_id(int id) {
    inner_tree_main_frame_tree_node_id_ = id;
  }
~~~

### inner_tree_main_frame_tree_node_id

inner_tree_main_frame_tree_node_id
~~~cpp
int inner_tree_main_frame_tree_node_id() const {
    return inner_tree_main_frame_tree_node_id_;
  }
~~~

### ForEachRenderFrameHostWithAction

ForEachRenderFrameHostWithAction
~~~cpp
void ForEachRenderFrameHostWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHostImpl*)> on_frame);
~~~
 These are the content internal equivalents of
 |RenderFrameHost::ForEachRenderFrameHost| whose comment can be referred to
 for details. Content internals can also access speculative
 RenderFrameHostImpls if necessary by using the
 |ForEachRenderFrameHostIncludingSpeculative| variations.

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
void ForEachRenderFrameHost(
      base::FunctionRef<void(RenderFrameHostImpl*)> on_frame);
~~~

### ForEachRenderFrameHostIncludingSpeculativeWithAction

ForEachRenderFrameHostIncludingSpeculativeWithAction
~~~cpp
void ForEachRenderFrameHostIncludingSpeculativeWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHostImpl*)> on_frame);
~~~

### ForEachRenderFrameHostIncludingSpeculative

ForEachRenderFrameHostIncludingSpeculative
~~~cpp
void ForEachRenderFrameHostIncludingSpeculative(
      base::FunctionRef<void(RenderFrameHostImpl*)> on_frame);
~~~

### DocumentUsedWebOTP

DocumentUsedWebOTP
~~~cpp
bool DocumentUsedWebOTP() override;
~~~

### GetWebAuthRequestSecurityChecker

GetWebAuthRequestSecurityChecker
~~~cpp
scoped_refptr<WebAuthRequestSecurityChecker>
  GetWebAuthRequestSecurityChecker();
~~~

### GetWeakPtr

GetWeakPtr
~~~cpp
base::WeakPtr<RenderFrameHostImpl> GetWeakPtr();
~~~

### GetSafeRef

GetSafeRef
~~~cpp
base::SafeRef<RenderFrameHostImpl> GetSafeRef() const;
~~~

### EnterFullscreen

EnterFullscreen
~~~cpp
void EnterFullscreen(blink::mojom::FullscreenOptionsPtr options,
                       EnterFullscreenCallback callback) override;
~~~
 blink::mojom::LocalFrameHost
### ExitFullscreen

ExitFullscreen
~~~cpp
void ExitFullscreen() override;
~~~

### FullscreenStateChanged

FullscreenStateChanged
~~~cpp
void FullscreenStateChanged(
      bool is_fullscreen,
      blink::mojom::FullscreenOptionsPtr options) override;
~~~

### RegisterProtocolHandler

RegisterProtocolHandler
~~~cpp
void RegisterProtocolHandler(const std::string& scheme,
                               const GURL& url,
                               bool user_gesture) override;
~~~

### UnregisterProtocolHandler

UnregisterProtocolHandler
~~~cpp
void UnregisterProtocolHandler(const std::string& scheme,
                                 const GURL& url,
                                 bool user_gesture) override;
~~~

### DidDisplayInsecureContent

DidDisplayInsecureContent
~~~cpp
void DidDisplayInsecureContent() override;
~~~

### DidContainInsecureFormAction

DidContainInsecureFormAction
~~~cpp
void DidContainInsecureFormAction() override;
~~~

### MainDocumentElementAvailable

MainDocumentElementAvailable
~~~cpp
void MainDocumentElementAvailable(bool uses_temporary_zoom_level) override;
~~~

### SetNeedsOcclusionTracking

SetNeedsOcclusionTracking
~~~cpp
void SetNeedsOcclusionTracking(bool needs_tracking) override;
~~~

### SetVirtualKeyboardMode

SetVirtualKeyboardMode
~~~cpp
void SetVirtualKeyboardMode(ui::mojom::VirtualKeyboardMode mode) override;
~~~

### VisibilityChanged

VisibilityChanged
~~~cpp
void VisibilityChanged(blink::mojom::FrameVisibility) override;
~~~

### DidChangeThemeColor

DidChangeThemeColor
~~~cpp
void DidChangeThemeColor(absl::optional<SkColor> theme_color) override;
~~~

### DidChangeBackgroundColor

DidChangeBackgroundColor
~~~cpp
void DidChangeBackgroundColor(SkColor background_color,
                                bool color_adjust) override;
~~~

### DidFailLoadWithError

DidFailLoadWithError
~~~cpp
void DidFailLoadWithError(const GURL& url, int32_t error_code) override;
~~~

### DidFocusFrame

DidFocusFrame
~~~cpp
void DidFocusFrame() override;
~~~

### DidCallFocus

DidCallFocus
~~~cpp
void DidCallFocus() override;
~~~

### EnforceInsecureRequestPolicy

EnforceInsecureRequestPolicy
~~~cpp
void EnforceInsecureRequestPolicy(
      blink::mojom::InsecureRequestPolicy policy) override;
~~~

### EnforceInsecureNavigationsSet

EnforceInsecureNavigationsSet
~~~cpp
void EnforceInsecureNavigationsSet(const std::vector<uint32_t>& set) override;
~~~

### SuddenTerminationDisablerChanged

SuddenTerminationDisablerChanged
~~~cpp
void SuddenTerminationDisablerChanged(
      bool present,
      blink::mojom::SuddenTerminationDisablerType disabler_type) override;
~~~

### HadStickyUserActivationBeforeNavigationChanged

HadStickyUserActivationBeforeNavigationChanged
~~~cpp
void HadStickyUserActivationBeforeNavigationChanged(bool value) override;
~~~

### ScrollRectToVisibleInParentFrame

ScrollRectToVisibleInParentFrame
~~~cpp
void ScrollRectToVisibleInParentFrame(
      const gfx::RectF& rect_to_scroll,
      blink::mojom::ScrollIntoViewParamsPtr params) override;
~~~

### BubbleLogicalScrollInParentFrame

BubbleLogicalScrollInParentFrame
~~~cpp
void BubbleLogicalScrollInParentFrame(
      blink::mojom::ScrollDirection direction,
      ui::ScrollGranularity granularity) override;
~~~

### DidBlockNavigation

DidBlockNavigation
~~~cpp
void DidBlockNavigation(
      const GURL& blocked_url,
      const GURL& initiator_url,
      blink::mojom::NavigationBlockedReason reason) override;
~~~

### DidChangeLoadProgress

DidChangeLoadProgress
~~~cpp
void DidChangeLoadProgress(double load_progress) override;
~~~

### DidFinishLoad

DidFinishLoad
~~~cpp
void DidFinishLoad(const GURL& validated_url) override;
~~~

### DispatchLoad

DispatchLoad
~~~cpp
void DispatchLoad() override;
~~~

### GoToEntryAtOffset

GoToEntryAtOffset
~~~cpp
void GoToEntryAtOffset(int32_t offset,
                         bool has_user_gesture,
                         absl::optional<blink::scheduler::TaskAttributionId>
                             soft_navigation_heuristics_task_id) override;
~~~

### NavigateToNavigationApiKey

NavigateToNavigationApiKey
~~~cpp
void NavigateToNavigationApiKey(
      const std::string& key,
      bool has_user_gesture,
      absl::optional<blink::scheduler::TaskAttributionId> task_id) override;
~~~

### NavigateEventHandlerPresenceChanged

NavigateEventHandlerPresenceChanged
~~~cpp
void NavigateEventHandlerPresenceChanged(bool present) override;
~~~

### UpdateTitle

UpdateTitle
~~~cpp
void UpdateTitle(const absl::optional<::std::u16string>& title,
                   base::i18n::TextDirection title_direction) override;
~~~

### UpdateUserActivationState

UpdateUserActivationState
~~~cpp
void UpdateUserActivationState(
      blink::mojom::UserActivationUpdateType update_type,
      blink::mojom::UserActivationNotificationType notification_type) override;
~~~

### DidConsumeHistoryUserActivation

DidConsumeHistoryUserActivation
~~~cpp
void DidConsumeHistoryUserActivation() override;
~~~

### HandleAccessibilityFindInPageResult

HandleAccessibilityFindInPageResult
~~~cpp
void HandleAccessibilityFindInPageResult(
      blink::mojom::FindInPageResultAXParamsPtr params) override;
~~~

### HandleAccessibilityFindInPageTermination

HandleAccessibilityFindInPageTermination
~~~cpp
void HandleAccessibilityFindInPageTermination() override;
~~~

### DocumentOnLoadCompleted

DocumentOnLoadCompleted
~~~cpp
void DocumentOnLoadCompleted() override;
~~~

### ForwardResourceTimingToParent

ForwardResourceTimingToParent
~~~cpp
void ForwardResourceTimingToParent(
      blink::mojom::ResourceTimingInfoPtr timing) override;
~~~

### DidDispatchDOMContentLoadedEvent

DidDispatchDOMContentLoadedEvent
~~~cpp
void DidDispatchDOMContentLoadedEvent() override;
~~~

### RunModalAlertDialog

RunModalAlertDialog
~~~cpp
void RunModalAlertDialog(const std::u16string& alert_message,
                           bool disable_third_party_subframe_suppresion,
                           RunModalAlertDialogCallback callback) override;
~~~

### RunModalConfirmDialog

RunModalConfirmDialog
~~~cpp
void RunModalConfirmDialog(const std::u16string& alert_message,
                             bool disable_third_party_subframe_suppresion,
                             RunModalConfirmDialogCallback callback) override;
~~~

### RunModalPromptDialog

RunModalPromptDialog
~~~cpp
void RunModalPromptDialog(const std::u16string& alert_message,
                            const std::u16string& default_value,
                            bool disable_third_party_subframe_suppresion,
                            RunModalPromptDialogCallback callback) override;
~~~

### RunBeforeUnloadConfirm

RunBeforeUnloadConfirm
~~~cpp
void RunBeforeUnloadConfirm(bool is_reload,
                              RunBeforeUnloadConfirmCallback callback) override;
~~~

### WillPotentiallyStartOutermostMainFrameNavigation

WillPotentiallyStartOutermostMainFrameNavigation
~~~cpp
void WillPotentiallyStartOutermostMainFrameNavigation(
      const GURL& url) override;
~~~

### UpdateFaviconURL

UpdateFaviconURL
~~~cpp
void UpdateFaviconURL(
      std::vector<blink::mojom::FaviconURLPtr> favicon_urls) override;
~~~

### DownloadURL

DownloadURL
~~~cpp
void DownloadURL(blink::mojom::DownloadURLParamsPtr params) override;
~~~

### FocusedElementChanged

FocusedElementChanged
~~~cpp
void FocusedElementChanged(bool is_editable_element,
                             const gfx::Rect& bounds_in_frame_widget,
                             blink::mojom::FocusType focus_type) override;
~~~

### TextSelectionChanged

TextSelectionChanged
~~~cpp
void TextSelectionChanged(const std::u16string& text,
                            uint32_t offset,
                            const gfx::Range& range) override;
~~~

### ShowPopupMenu

ShowPopupMenu
~~~cpp
void ShowPopupMenu(
      mojo::PendingRemote<blink::mojom::PopupMenuClient> popup_client,
      const gfx::Rect& bounds,
      int32_t item_height,
      double font_size,
      int32_t selected_item,
      std::vector<blink::mojom::MenuItemPtr> menu_items,
      bool right_aligned,
      bool allow_multiple_selection) override;
~~~

### CreateNewPopupWidget

CreateNewPopupWidget
~~~cpp
void CreateNewPopupWidget(
      mojo::PendingAssociatedReceiver<blink::mojom::PopupWidgetHost>
          blink_popup_widget_host,
      mojo::PendingAssociatedReceiver<blink::mojom::WidgetHost>
          blink_widget_host,
      mojo::PendingAssociatedRemote<blink::mojom::Widget> blink_widget)
      override;
~~~

### ShowContextMenu

ShowContextMenu
~~~cpp
void ShowContextMenu(
      mojo::PendingAssociatedRemote<blink::mojom::ContextMenuClient>
          context_menu_client,
      const blink::UntrustworthyContextMenuParams& params) override;
~~~

### DidLoadResourceFromMemoryCache

DidLoadResourceFromMemoryCache
~~~cpp
void DidLoadResourceFromMemoryCache(
      const GURL& url,
      const std::string& http_method,
      const std::string& mime_type,
      network::mojom::RequestDestination request_destination,
      bool include_credentials) override;
~~~

### DidChangeFrameOwnerProperties

DidChangeFrameOwnerProperties
~~~cpp
void DidChangeFrameOwnerProperties(
      const blink::FrameToken& child_frame_token,
      blink::mojom::FrameOwnerPropertiesPtr frame_owner_properties) override;
~~~

### DidChangeOpener

DidChangeOpener
~~~cpp
void DidChangeOpener(
      const absl::optional<blink::LocalFrameToken>& opener_frame) override;
~~~

### DidChangeIframeAttributes

DidChangeIframeAttributes
~~~cpp
void DidChangeIframeAttributes(
      const blink::FrameToken& child_frame_token,
      blink::mojom::IframeAttributesPtr attributes) override;
~~~

### DidChangeFramePolicy

DidChangeFramePolicy
~~~cpp
void DidChangeFramePolicy(const blink::FrameToken& child_frame_token,
                            const blink::FramePolicy& frame_policy) override;
~~~

### CapturePaintPreviewOfSubframe

CapturePaintPreviewOfSubframe
~~~cpp
void CapturePaintPreviewOfSubframe(
      const gfx::Rect& clip_rect,
      const base::UnguessableToken& guid) override;
~~~

### SetCloseListener

SetCloseListener
~~~cpp
void SetCloseListener(
      mojo::PendingRemote<blink::mojom::CloseListener> listener) override;
~~~

### Detach

Detach
~~~cpp
void Detach() override;
~~~

### DidAddMessageToConsole

DidAddMessageToConsole
~~~cpp
void DidAddMessageToConsole(
      blink::mojom::ConsoleMessageLevel log_level,
      const std::u16string& message,
      uint32_t line_no,
      const absl::optional<std::u16string>& source_id,
      const absl::optional<std::u16string>& untrusted_stack_trace) override;
~~~

### FrameSizeChanged

FrameSizeChanged
~~~cpp
void FrameSizeChanged(const gfx::Size& frame_size) override;
~~~

### DidChangeSrcDoc

DidChangeSrcDoc
~~~cpp
void DidChangeSrcDoc(const blink::FrameToken& child_frame_token,
                       const std::string& srcdoc_value) override;
~~~

### ReceivedDelegatedCapability

ReceivedDelegatedCapability
~~~cpp
void ReceivedDelegatedCapability(
      blink::mojom::DelegatedCapability delegated_capability) override;
~~~

### SendFencedFrameReportingBeacon

SendFencedFrameReportingBeacon
~~~cpp
void SendFencedFrameReportingBeacon(
      const std::string& event_data,
      const std::string& event_type,
      blink::FencedFrame::ReportingDestination destination) override;
~~~

### SetFencedFrameAutomaticBeaconReportEventData

SetFencedFrameAutomaticBeaconReportEventData
~~~cpp
void SetFencedFrameAutomaticBeaconReportEventData(
      const std::string& event_data,
      const std::vector<blink::FencedFrame::ReportingDestination>& destination)
      override;
~~~

### SendPrivateAggregationRequestsForFencedFrameEvent

SendPrivateAggregationRequestsForFencedFrameEvent
~~~cpp
void SendPrivateAggregationRequestsForFencedFrameEvent(
      const std::string& event_type) override;
~~~

### CreatePortal

CreatePortal
~~~cpp
void CreatePortal(
      mojo::PendingAssociatedReceiver<blink::mojom::Portal> pending_receiver,
      mojo::PendingAssociatedRemote<blink::mojom::PortalClient> client,
      blink::mojom::RemoteFrameInterfacesFromRendererPtr
          remote_frame_interfaces,
      CreatePortalCallback callback) override;
~~~

### AdoptPortal

AdoptPortal
~~~cpp
void AdoptPortal(const blink::PortalToken& portal_token,
                   blink::mojom::RemoteFrameInterfacesFromRendererPtr
                       remote_frame_interfaces,
                   AdoptPortalCallback callback) override;
~~~

### CreateFencedFrame

CreateFencedFrame
~~~cpp
void CreateFencedFrame(
      mojo::PendingAssociatedReceiver<blink::mojom::FencedFrameOwnerHost>
          pending_receiver,
      blink::mojom::RemoteFrameInterfacesFromRendererPtr
          remote_frame_interfaces,
      const blink::RemoteFrameToken& frame_token,
      const base::UnguessableToken& devtools_frame_token) override;
~~~

### OnViewTransitionOptInChanged

OnViewTransitionOptInChanged
~~~cpp
void OnViewTransitionOptInChanged(blink::mojom::ViewTransitionSameOriginOptIn
                                        view_transition_opt_in) override;
~~~

### EvictFromBackForwardCache

EvictFromBackForwardCache
~~~cpp
void EvictFromBackForwardCache(blink::mojom::RendererEvictionReason) override;
~~~
 blink::mojom::BackForwardCacheControllerHost:
### DidChangeBackForwardCacheDisablingFeatures

DidChangeBackForwardCacheDisablingFeatures
~~~cpp
void DidChangeBackForwardCacheDisablingFeatures(
      BackForwardCacheBlockingDetails details) override;
~~~

### ScaleFactorChanged

ScaleFactorChanged
~~~cpp
void ScaleFactorChanged(float scale) override;
~~~
 blink::LocalMainFrameHost overrides:
### ContentsPreferredSizeChanged

ContentsPreferredSizeChanged
~~~cpp
void ContentsPreferredSizeChanged(const gfx::Size& pref_size) override;
~~~

### TextAutosizerPageInfoChanged

TextAutosizerPageInfoChanged
~~~cpp
void TextAutosizerPageInfoChanged(
      blink::mojom::TextAutosizerPageInfoPtr page_info) override;
~~~

### FocusPage

FocusPage
~~~cpp
void FocusPage() override;
~~~

### TakeFocus

TakeFocus
~~~cpp
void TakeFocus(bool reverse) override;
~~~

### UpdateTargetURL

UpdateTargetURL
~~~cpp
void UpdateTargetURL(const GURL& url,
                       blink::mojom::LocalMainFrameHost::UpdateTargetURLCallback
                           callback) override;
~~~

### RequestClose

RequestClose
~~~cpp
void RequestClose() override;
~~~

### ShowCreatedWindow

ShowCreatedWindow
~~~cpp
void ShowCreatedWindow(const blink::LocalFrameToken& opener_frame_token,
                         WindowOpenDisposition disposition,
                         blink::mojom::WindowFeaturesPtr window_features,
                         bool user_gesture,
                         ShowCreatedWindowCallback callback) override;
~~~

### SetWindowRect

SetWindowRect
~~~cpp
void SetWindowRect(const gfx::Rect& bounds,
                     SetWindowRectCallback callback) override;
~~~

### DidFirstVisuallyNonEmptyPaint

DidFirstVisuallyNonEmptyPaint
~~~cpp
void DidFirstVisuallyNonEmptyPaint() override;
~~~

### DidAccessInitialMainDocument

DidAccessInitialMainDocument
~~~cpp
void DidAccessInitialMainDocument() override;
~~~

### ReportNoBinderForInterface

ReportNoBinderForInterface
~~~cpp
void ReportNoBinderForInterface(const std::string& error);
~~~

### HasCommittingNavigationRequestForOrigin

HasCommittingNavigationRequestForOrigin
~~~cpp
bool HasCommittingNavigationRequestForOrigin(
      const url::Origin& origin,
      NavigationRequest* navigation_request_to_exclude);
~~~
 Returns true if this object has any NavigationRequests matching |origin|.

 Since this function is used to find existing committed/committing origins
 that have not opted-in to isolation, and since any calls to this function
 will be initiated by a NavigationRequest that is itself requesting opt-in
 isolation, |navigation_request_to_exclude| allows that request to exclude
 itself from consideration.

### DoNotDeleteForTesting

DoNotDeleteForTesting
~~~cpp
void DoNotDeleteForTesting();
~~~
 Force the RenderFrameHost to be left in pending deletion state instead of
 being actually deleted after navigating away:
 - Force waiting for unload handler result regardless of whether an
   unload handler is present or not.

 - Disable unload timeout monitor.

 - Ignore any OnUnloadACK sent by the renderer process.

### ResumeDeletionForTesting

ResumeDeletionForTesting
~~~cpp
void ResumeDeletionForTesting();
~~~
 This method will unset the flag |do_not_delete_for_testing_| to resume
 deletion on the RenderFrameHost. Deletion will only be triggered if
 RenderFrameHostImpl::Detach() is called for the RenderFrameHost. This is a
 counterpart for DoNotDeleteForTesting() which sets the flag
 |do_not_delete_for_testing_|.

### DetachForTesting

DetachForTesting
~~~cpp
void DetachForTesting();
~~~
 This method will detach forcely RenderFrameHost with setting the states,
 |do_not_delete_for_testing_| and |detach_state_|, to resume deletion on
 the RenderFrameHost.

### GetDocumentUserData

GetDocumentUserData
~~~cpp
base::SupportsUserData::Data* GetDocumentUserData(const void* key) const {
    return document_associated_data_->GetUserData(key);
  }
~~~
 Document-associated data. This is cleared whenever a new document is hosted
 by this RenderFrameHost. Please refer to the description at
 content/public/browser/document_user_data.h for more details.

### SetDocumentUserData

SetDocumentUserData
~~~cpp
void SetDocumentUserData(const void* key,
                           std::unique_ptr<base::SupportsUserData::Data> data) {
    document_associated_data_->SetUserData(key, std::move(data));
  }
~~~

### RemoveDocumentUserData

RemoveDocumentUserData
~~~cpp
void RemoveDocumentUserData(const void* key) {
    document_associated_data_->RemoveUserData(key);
  }
~~~

### AddDocumentService

AddDocumentService
~~~cpp
void AddDocumentService(internal::DocumentServiceBase* document_service,
                          base::PassKey<internal::DocumentServiceBase>);
~~~

### RemoveDocumentService

RemoveDocumentService
~~~cpp
void RemoveDocumentService(internal::DocumentServiceBase* document_service,
                             base::PassKey<internal::DocumentServiceBase>);
~~~

### OnCommittedSpeculativeBeforeNavigationCommit

OnCommittedSpeculativeBeforeNavigationCommit
~~~cpp
void OnCommittedSpeculativeBeforeNavigationCommit() {
    committed_speculative_rfh_before_navigation_commit_ = true;
  }
~~~
 Called when we commit speculative RFH early due to not having an alive
 current frame. This happens when the renderer crashes before navigating to
 a new URL using speculative RenderFrameHost.

 TODO(https://crbug.com/1072817): Undo this plumbing after removing the
 early post-crash CommitPending() call.

### FindAndVerifyChild

FindAndVerifyChild
~~~cpp
FrameTreeNode* FindAndVerifyChild(int32_t child_frame_routing_id,
                                    bad_message::BadMessageReason reason);
~~~
 Returns the child frame if |child_frame_routing_id| is an immediate child
 of this RenderFrameHost. |child_frame_routing_id| is considered untrusted,
 so the renderer process is killed if it refers to a RenderFrameHostImpl
 that is not a child of this node.

### FindAndVerifyChild

FindAndVerifyChild
~~~cpp
FrameTreeNode* FindAndVerifyChild(const blink::FrameToken& child_frame_token,
                                    bad_message::BadMessageReason reason);
~~~
 Returns the child frame if |child_frame_token| is an immediate child of
 this RenderFrameHostImpl. |child_frame_token| is considered untrusted, so
 the renderer process is killed if it refers to a RenderFrameHostImpl that
 is not a child of this node.

### ShouldDispatchPagehideAndVisibilitychangeDuringCommit

ShouldDispatchPagehideAndVisibilitychangeDuringCommit
~~~cpp
bool ShouldDispatchPagehideAndVisibilitychangeDuringCommit(
      RenderFrameHostImpl* old_frame_host,
      const UrlInfo& dest_url_info);
~~~
 Whether we should run the pagehide/visibilitychange handlers of the
 RenderFrameHost we're navigating away from (|old_frame_host|) during the
 commit to a new RenderFrameHost (this RenderFrameHost). Should only return
 true when we're doing a same-site navigation and we did a proactive
 BrowsingInstance swap but we're reusing the old page's renderer process.

 We should run pagehide and visibilitychange handlers of the old page during
 the commit of the new main frame in those cases because in other same-site
 navigations we will run those handlers before the new page finished
 committing. Note that unload handlers will still run after the new page
 finished committing. Ideally we would run unload handlers alongside
 pagehide and visibilitychange handlers at commit time too, but we'd need to
 actually unload/freeze the page in that case which is more complex.

 TODO(crbug.com/1110744): Support unload-in-commit.

### CreateURLLoaderNetworkObserver

CreateURLLoaderNetworkObserver
~~~cpp
mojo::PendingRemote<network::mojom::URLLoaderNetworkServiceObserver>
  CreateURLLoaderNetworkObserver();
~~~

### CreateCookieAccessObserver

CreateCookieAccessObserver
~~~cpp
mojo::PendingRemote<network::mojom::CookieAccessObserver>
  CreateCookieAccessObserver();
~~~

### CreateTrustTokenAccessObserver

CreateTrustTokenAccessObserver
~~~cpp
mojo::PendingRemote<network::mojom::TrustTokenAccessObserver>
  CreateTrustTokenAccessObserver();
~~~

### OnCookiesAccessed

OnCookiesAccessed
~~~cpp
void OnCookiesAccessed(std::vector<network::mojom::CookieAccessDetailsPtr>
                             details_vector) override;
~~~
 network::mojom::CookieAccessObserver:
### OnTrustTokensAccessed

OnTrustTokensAccessed
~~~cpp
void OnTrustTokensAccessed(
      network::mojom::TrustTokenAccessDetailsPtr details) override;
~~~
 network::mojom::TrustTokenAccessObserver:
### GetSavableResourceLinksFromRenderer

GetSavableResourceLinksFromRenderer
~~~cpp
void GetSavableResourceLinksFromRenderer();
~~~

### GetPendingBeaconHost

GetPendingBeaconHost
~~~cpp
void GetPendingBeaconHost(
      mojo::PendingReceiver<blink::mojom::PendingBeaconHost> receiver);
~~~

### ShouldBypassSecurityChecksForErrorPage

ShouldBypassSecurityChecksForErrorPage
~~~cpp
bool ShouldBypassSecurityChecksForErrorPage(
      NavigationRequest* navigation_request,
      bool* should_commit_error_page = nullptr);
~~~
 Helper for checking if a navigation to an error page should be excluded
 from CanAccessDataForOrigin and/or CanCommitOriginAndUrl security checks.


 It is allowed for |navigation_request| to be null - for example when
 committing a same-document navigation.


 The optional |should_commit_unreachable_url| will be set to |true| if the
 caller should verify that DidCommitProvisionalLoadParams'
 url_is_unreachable is |true|.

### SetAudioOutputDeviceIdForGlobalMediaControls

SetAudioOutputDeviceIdForGlobalMediaControls
~~~cpp
void SetAudioOutputDeviceIdForGlobalMediaControls(
      std::string hashed_device_id);
~~~
 Explicitly allow the use of an audio output device in this render frame.

 When called with a hashed device id string the renderer will be allowed to
 use the associated device for audio output until this method is called
 again with a different hashed device id or the origin changes. To remove
 this permission, this method may be called with the empty string.

### MaybeEvictFromBackForwardCache

MaybeEvictFromBackForwardCache
~~~cpp
void MaybeEvictFromBackForwardCache();
~~~
 Evicts the document from the BackForwardCache if it is in the cache,
 and ineligible for caching.

### CreateMessageFilterForAssociatedReceiver

CreateMessageFilterForAssociatedReceiver
~~~cpp
std::unique_ptr<mojo::MessageFilter> CreateMessageFilterForAssociatedReceiver(
      const char* interface_name);
~~~
 Returns a filter that should be associated with all AssociatedReceivers for
 this frame. |interface_name| is used for logging purposes and must be valid
 for the entire program duration.

### accessibility_fatal_error_count_for_testing

accessibility_fatal_error_count_for_testing
~~~cpp
int accessibility_fatal_error_count_for_testing() const {
    return accessibility_fatal_error_count_;
  }
~~~

### SetFrameTreeNode

SetFrameTreeNode
~~~cpp
void SetFrameTreeNode(FrameTreeNode& frame_tree_node);
~~~
 TODO(https://crbug.com/1179502): FrameTree and FrameTreeNode are not const
 as with prerenderer activation the page needs to move between
 FrameTreeNodes and FrameTrees. Note that FrameTreeNode can only change for
 root nodes. As it's hard to make sure that all places handle this
 transition correctly, MPArch will remove references from this class to
 FrameTree/FrameTreeNode.

### SetFrameTree

SetFrameTree
~~~cpp
void SetFrameTree(FrameTree& frame_tree);
~~~

### SetRenderFrameHostOwner

SetRenderFrameHostOwner
~~~cpp
void SetRenderFrameHostOwner(RenderFrameHostOwner* owner) { owner_ = owner; }
~~~
 RenderFrameHostImpl should not directly reference its FrameTreeNode as
 associated FrameTreeNode can change during RenderFrameHostImpl's lifetime
 (crbug.com/1179502). Instead, a dedicated interface (RenderFrameHostOwner)
 is exposed here.

 TODO(crbug.com/1179502): Remove RenderFrameHostImpl::SetFrameTreeNode in
 favour of this method.

### BuildClientSecurityState

BuildClientSecurityState
~~~cpp
network::mojom::ClientSecurityStatePtr BuildClientSecurityState() const;
~~~
 Builds and return a ClientSecurityState based on the internal
 RenderFrameHostImpl state. This is never null.

### BuildClientSecurityStateForWorkers

BuildClientSecurityStateForWorkers
~~~cpp
network::mojom::ClientSecurityStatePtr BuildClientSecurityStateForWorkers()
      const;
~~~
 For worker script fetches/updates or fetches within workers.

### OnDidRunInsecureContent

OnDidRunInsecureContent
~~~cpp
void OnDidRunInsecureContent(const GURL& security_origin,
                               const GURL& target_url);
~~~

### OnDidDisplayContentWithCertificateErrors

OnDidDisplayContentWithCertificateErrors
~~~cpp
void OnDidDisplayContentWithCertificateErrors();
~~~

### OnDidRunContentWithCertificateErrors

OnDidRunContentWithCertificateErrors
~~~cpp
void OnDidRunContentWithCertificateErrors();
~~~

### GetPeerConnectionTrackerHost

GetPeerConnectionTrackerHost
~~~cpp
PeerConnectionTrackerHost& GetPeerConnectionTrackerHost();
~~~

### BindPeerConnectionTrackerHost

BindPeerConnectionTrackerHost
~~~cpp
void BindPeerConnectionTrackerHost(
      mojo::PendingReceiver<blink::mojom::PeerConnectionTrackerHost> receiver);
~~~

### EnableWebRtcEventLogOutput

EnableWebRtcEventLogOutput
~~~cpp
void EnableWebRtcEventLogOutput(int lid, int output_period_ms) override;
~~~

### DisableWebRtcEventLogOutput

DisableWebRtcEventLogOutput
~~~cpp
void DisableWebRtcEventLogOutput(int lid) override;
~~~

### IsDocumentOnLoadCompletedInMainFrame

IsDocumentOnLoadCompletedInMainFrame
~~~cpp
bool IsDocumentOnLoadCompletedInMainFrame() override;
~~~

### FaviconURLs

FaviconURLs
~~~cpp
const std::vector<blink::mojom::FaviconURLPtr>& FaviconURLs() override;
~~~

### CreateMdnsResponder

CreateMdnsResponder
~~~cpp
void CreateMdnsResponder(
      mojo::PendingReceiver<network::mojom::MdnsResponder> receiver);
~~~

### last_response_head

last_response_head
~~~cpp
const network::mojom::URLResponseHeadPtr& last_response_head() const {
    return last_response_head_;
  }
~~~
 BUILDFLAG(ENABLE_MDNS)
### early_hints_manager

early_hints_manager
~~~cpp
NavigationEarlyHintsManager* early_hints_manager() {
    return early_hints_manager_.get();
  }
~~~

### CreateSubresourceLoaderFactoriesForInitialEmptyDocument

CreateSubresourceLoaderFactoriesForInitialEmptyDocument
~~~cpp
std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
  CreateSubresourceLoaderFactoriesForInitialEmptyDocument();
~~~
 Create a bundle of subresource factories for an initial empty document.

 Used for browser-initiated (e.g. no opener) creation of a new main frame.

 Example scenarios:
 1. `window.open` with `rel=noopener` (this path is not used if rel=noopener
    is missing;  child frames also don't go through this path).

 2. Browser-initiated (e.g. browser-UI-driven) opening of a new tab.

 3. Recreating an active main frame when recovering from a renderer crash.

 4. Creating a speculative main frame if navigation requires a process swap.


 Currently the returned bundle is mostly empty - in practice it is
 sufficient to provide only a NetworkService-bound default factory (i.e. no
 chrome-extension:// or file:// or data: factories are present today).

 TODO(lukasza): Revisit the above is necessary.


 The parameters of the NetworkService-bound default factory (e.g.

 `request_initiator_origin_lock`, IPAddressSpace, etc.) are associated
 with the current `last_committed_origin_` (typically an opaque, unique
 origin for a browser-created initial empty document;  it may be a regular
 origin when recovering from a renderer crash).

### MaybeDispatchDOMContentLoadedOnPrerenderActivation

MaybeDispatchDOMContentLoadedOnPrerenderActivation
~~~cpp
void MaybeDispatchDOMContentLoadedOnPrerenderActivation();
~~~
 Prerender2:
 Dispatches DidFinishLoad and DOMContentLoaded if it occurred pre-activation
 and was deferred to be dispatched after activation.

### MaybeDispatchDidFinishLoadOnPrerenderActivation

MaybeDispatchDidFinishLoadOnPrerenderActivation
~~~cpp
void MaybeDispatchDidFinishLoadOnPrerenderActivation();
~~~

### GetParentOrOuterDocumentOrEmbedderExcludingProspectiveOwners

GetParentOrOuterDocumentOrEmbedderExcludingProspectiveOwners
~~~cpp
RenderFrameHostImpl*
  GetParentOrOuterDocumentOrEmbedderExcludingProspectiveOwners() const;
~~~
 Versions of `GetParentOrOuterDocumentOrEmbedder` and
 `GetOutermostMainFrameOrEmbedder` which exclude embedders which own, but
 have not yet attached, our frame tree for rendering. These should be
 avoided in favor of `GetParentOrOuterDocumentOrEmbedder` and
 `GetOutermostMainFrameOrEmbedder`. This distinction should only matter for
 cases which implement a view hierarchy such as the implementation of
 RenderWidgetHostView.

### GetOutermostMainFrameOrEmbedderExcludingProspectiveOwners

GetOutermostMainFrameOrEmbedderExcludingProspectiveOwners
~~~cpp
RenderFrameHostImpl*
  GetOutermostMainFrameOrEmbedderExcludingProspectiveOwners();
~~~

### ComputeNonce

ComputeNonce
~~~cpp
absl::optional<base::UnguessableToken> ComputeNonce(
      bool is_credentialless,
      absl::optional<base::UnguessableToken> fenced_frame_nonce_for_navigation);
~~~
 Computes the nonce to be used for isolation info and storage key.

 For navigations, populate `fenced_frame_nonce_for_navigation` with
 `NavigationRequest::ComputeFencedFrameNonce()`.

### PreviousSibling

PreviousSibling
~~~cpp
FrameTreeNode* PreviousSibling() const;
~~~
 Return the frame immediately preceding this RenderFrameHost in its parent's
 children, or nullptr if there is no such node.

### NextSibling

NextSibling
~~~cpp
FrameTreeNode* NextSibling() const;
~~~
 Return the frame immediately following this RenderFrameHost in its parent's
 children, or nullptr if there is no such node.

### SetOriginDependentStateOfNewFrame

SetOriginDependentStateOfNewFrame
~~~cpp
void SetOriginDependentStateOfNewFrame(RenderFrameHostImpl* creator_frame);
~~~
 Set the |last_committed_origin_|, |isolation_info_|,
 |permissions_policy_|, and the RuntimeFeatureStateDocumentData of |this|
 frame, inheriting both the origin from |creator_frame| as appropriate (e.g.

 depending on whether |this| frame should be sandboxed / should have an
 opaque origin instead).

### CalculateStorageKey

CalculateStorageKey
~~~cpp
blink::StorageKey CalculateStorageKey(const url::Origin& new_rfh_origin,
                                        const base::UnguessableToken* nonce);
~~~
 Calculates the storage key for this RenderFrameHostImpl using the passed
 `new_rfh_origin`, and `nonce`, and deriving the storage key's
 top_level_site` and `ancestor_bit` parameters. If
 `is_third_party_storage_partitioning_allowed` is provided then that value
 is used for allowing third-party storage partitioning, if it is
 absl::nullopt then the value is calculated using the top frame's
 RuntimeFeatureStateReadContext, the enterprise policy, and the
 base::feature. This also takes into account special embedding cases, such
 as extensions embedding web iframes, where the top-level frame is not the
 same as the top-level site for the purposes of calculating the storage key.

 See the implementation for more details.

### browsing_context_state

browsing_context_state
~~~cpp
const scoped_refptr<BrowsingContextState>& browsing_context_state() const {
    return browsing_context_state_;
  }
~~~
 Returns the BrowsingContextState associated with this RenderFrameHostImpl.

 See class comments in BrowsingContextState for a more detailed description.

### IsBackForwardCacheEvictionTimeRunningForTesting

IsBackForwardCacheEvictionTimeRunningForTesting
~~~cpp
bool IsBackForwardCacheEvictionTimeRunningForTesting() const;
~~~
 Returns true if the back/forward cache eviction timer has been started, and
 false otherwise.

### GetProxyToParent

GetProxyToParent
~~~cpp
RenderFrameProxyHost* GetProxyToParent();
~~~
 Retrieve proxies in a way that is no longer dependent on access to
 FrameTreeNode or RenderFrameHostManager.

### GetProxyToOuterDelegate

GetProxyToOuterDelegate
~~~cpp
RenderFrameProxyHost* GetProxyToOuterDelegate();
~~~
 Returns the proxy to inner WebContents in the outer WebContents's
 SiteInstance. Note that this is not allowed to call this function on
 inactive RenderFrameHost.

### DidChangeReferrerPolicy

DidChangeReferrerPolicy
~~~cpp
void DidChangeReferrerPolicy(network::mojom::ReferrerPolicy referrer_policy);
~~~

### GetPageScaleFactor

GetPageScaleFactor
~~~cpp
float GetPageScaleFactor() const;
~~~

### PerformGetAssertionWebAuthSecurityChecks

PerformGetAssertionWebAuthSecurityChecks
~~~cpp
std::pair<blink::mojom::AuthenticatorStatus, bool>
  PerformGetAssertionWebAuthSecurityChecks(
      const std::string& relying_party_id,
      const url::Origin& effective_origin,
      bool is_payment_credential_get_assertion,
      const blink::mojom::RemoteDesktopClientOverridePtr&
          remote_desktop_client_override);
~~~
 Perform security checks on Web Authentication requests. These can be
 called by the Android Java |Authenticator| mojo interface implementation
 so that they don't have to duplicate security policies.

 For requests originating from the render process, |effective_origin| will
 be the same as the last committed origin. However, for request originating
 from the browser process, this may be different.

 |is_payment_credential_creation| indicates whether MakeCredential is making
 a payment credential.

 |remote_desktop_client_override| optionally contains a
 RemoteDesktopClientOverride client extension for the request.

 |PerformGetAssertionWebAuthSecurityChecks| returns a security check result
 and a boolean representing whether the given origin is cross-origin with
 any frame in this frame's ancestor chain. This extra cross-origin bit is
 relevant in case callers need it for crypto signature.

### PerformMakeCredentialWebAuthSecurityChecks

PerformMakeCredentialWebAuthSecurityChecks
~~~cpp
blink::mojom::AuthenticatorStatus PerformMakeCredentialWebAuthSecurityChecks(
      const std::string& relying_party_id,
      const url::Origin& effective_origin,
      bool is_payment_credential_creation,
      const blink::mojom::RemoteDesktopClientOverridePtr&
          remote_desktop_client_override);
~~~

### ExecuteJavaScriptForTests

ExecuteJavaScriptForTests
~~~cpp
void ExecuteJavaScriptForTests(const std::u16string& javascript,
                                 bool has_user_gesture,
                                 bool resolve_promises,
                                 int32_t world_id,
                                 JavaScriptResultAndTypeCallback callback);
~~~
 Runs JavaScript in this frame, without restrictions. ONLY FOR TESTS.

 This method can optionally trigger a fake user activation notification,
 and can wait for returned promises to be resolved.

### HandleAXEventsForTests

HandleAXEventsForTests
~~~cpp
void HandleAXEventsForTests(
      const ui::AXTreeID& tree_id,
      blink::mojom::AXUpdatesAndEventsPtr updates_and_events,
      int32_t reset_token) {
    HandleAXEvents(tree_id, std::move(updates_and_events), reset_token);
  }
~~~
 Call |HandleAXEvents()| for tests.

### GetBucketStorageKey

GetBucketStorageKey
~~~cpp
blink::StorageKey GetBucketStorageKey() override;
~~~
 BucketContext:
### GetPermissionStatus

GetPermissionStatus
~~~cpp
blink::mojom::PermissionStatus GetPermissionStatus(
      blink::PermissionType permission_type) override;
~~~

### BindCacheStorageForBucket

BindCacheStorageForBucket
~~~cpp
void BindCacheStorageForBucket(
      const storage::BucketInfo& bucket,
      mojo::PendingReceiver<blink::mojom::CacheStorage> receiver) override;
~~~

### GetSandboxedFileSystemForBucket

GetSandboxedFileSystemForBucket
~~~cpp
void GetSandboxedFileSystemForBucket(
      const storage::BucketInfo& bucket,
      blink::mojom::BucketHost::GetDirectoryCallback callback) override;
~~~

### GetAssociatedRenderFrameHostId

GetAssociatedRenderFrameHostId
~~~cpp
GlobalRenderFrameHostId GetAssociatedRenderFrameHostId() const override;
~~~

### SendAllPendingBeaconsOnNavigation

SendAllPendingBeaconsOnNavigation
~~~cpp
void SendAllPendingBeaconsOnNavigation();
~~~
 Sends out all pending beacons held by this document and all its child
 documents.


 This method must be called when navigating away from the current
 document.

### SetNotInitialEmptyDocument

SetNotInitialEmptyDocument
~~~cpp
void SetNotInitialEmptyDocument() { is_initial_empty_document_ = false; }
~~~
 Sets `is_initial_empty_document_` to false.

### is_initial_empty_document

is_initial_empty_document
~~~cpp
bool is_initial_empty_document() const { return is_initial_empty_document_; }
~~~
 Returns false if this document not the initial empty document, or if the
 current document's input stream has been opened with document.open(),
 causing the document to lose its "initial empty document" status. For more
 details, see the definition of `is_initial_empty_document_`.

### DidOpenDocumentInputStream

DidOpenDocumentInputStream
~~~cpp
void DidOpenDocumentInputStream() { is_initial_empty_document_ = false; }
~~~
 Sets `is_initial_empty_document_` to
 false. Must only be called after the current document's input stream has
 been opened with document.open().

### SnapshotDocumentForViewTransition

SnapshotDocumentForViewTransition
~~~cpp
void SnapshotDocumentForViewTransition(
      blink::mojom::LocalFrame::SnapshotDocumentForViewTransitionCallback
          callback);
~~~

### devtools_frame_token

devtools_frame_token
~~~cpp
const base::UnguessableToken& devtools_frame_token() const {
    return devtools_frame_token_;
  }
~~~
 See comment on the member declaration.

### GetPpapiSupport

GetPpapiSupport
~~~cpp
RenderFrameHostImplPpapiSupport& GetPpapiSupport();
~~~

### HasStickyUserActivation

HasStickyUserActivation
~~~cpp
bool HasStickyUserActivation() const;
~~~
 Returns the sticky bit of the User Activation v2 state of this document.

### IsActiveUserActivation

IsActiveUserActivation
~~~cpp
bool IsActiveUserActivation() const;
~~~

### ClearUserActivation

ClearUserActivation
~~~cpp
void ClearUserActivation();
~~~

### ConsumeTransientUserActivation

ConsumeTransientUserActivation
~~~cpp
void ConsumeTransientUserActivation();
~~~

### ActivateUserActivation

ActivateUserActivation
~~~cpp
void ActivateUserActivation(
      blink::mojom::UserActivationNotificationType notification_type);
~~~

### IsHistoryUserActivationActive

IsHistoryUserActivationActive
~~~cpp
bool IsHistoryUserActivationActive() const;
~~~
 These are called only when RenderFrameHostOwner is iterating over all
 frames, not directly from the renderer.

### ConsumeHistoryUserActivation

ConsumeHistoryUserActivation
~~~cpp
void ConsumeHistoryUserActivation();
~~~

### ClosePage

ClosePage
~~~cpp
void ClosePage(ClosePageSource source);
~~~
 Tells the renderer process to run the page's unload handler.

 A completion callback is invoked by the renderer when the handler
 execution completes. The `source` parameter indicates whether this request
 comes from the browser or the renderer. If the request comes from the
 renderer, then it may be ignored if a different document commits first.

### IsPageReadyToBeClosed

IsPageReadyToBeClosed
~~~cpp
bool IsPageReadyToBeClosed();
~~~
 When true, indicates that the unload handlers have already executed (e.g.,
 after receiving a ClosePage ACK) or that they should be ignored. This is
 queried while attempting to close the current tab/window.  Should only be
 used on primary main frames.

### GetCookieSettingOverrides

GetCookieSettingOverrides
~~~cpp
net::CookieSettingOverrides GetCookieSettingOverrides();
~~~
 Checks Blink runtime-enabled features (BREF) to create and return
 a CookieSettingOverrides pertaining to the last committed document in the
 frame. Can only be called on a frame with a committed navigation.

### GetIsThirdPartyCookiesUserBypassEnabled

GetIsThirdPartyCookiesUserBypassEnabled
~~~cpp
bool GetIsThirdPartyCookiesUserBypassEnabled();
~~~
 When this returns true, the user has specified that third-party cookies
 should remain enabled for this frame.

 It does so by checking the Blink runtime-enabled feature (BREF) for
 third-party cookie deprecation user bypass. This pulls the BREF from the
 committed document context; for a committing navigation use
 NavigationRequest::GetIsThirdPartyCookiesUserBypassEnabled.

 For a subframe, the BREF is pulled from the main frame context. If the main
 frame has no committed navigation (eg. an empty initial document), then
 false is returned.

### GetCookieChangeInfo

GetCookieChangeInfo
~~~cpp
CookieChangeListener::CookieChangeInfo GetCookieChangeInfo();
~~~
 Retrieves the information about the cookie changes that are observed on the
 last committed document.

### SetResourceCache

SetResourceCache
~~~cpp
void SetResourceCache(
      mojo::PendingRemote<blink::mojom::ResourceCache> remote);
~~~
 Sets a ResourceCache in the renderer. `remote` must have the same process
 isolation policy.

 TODO(https://crbug.com/1414262): Add checks to ensure the preconditions.

### FlushMojomFrameRemoteForTesting

FlushMojomFrameRemoteForTesting
~~~cpp
void FlushMojomFrameRemoteForTesting();
~~~

### RecordNavigationSuddenTerminationHandlers

RecordNavigationSuddenTerminationHandlers
~~~cpp
void RecordNavigationSuddenTerminationHandlers();
~~~
 Records metrics on sudden termination handlers found in this frame and
 subframes.

### RenderFrameHostImpl

RenderFrameHostImpl
~~~cpp
RenderFrameHostImpl(
      SiteInstance* site_instance,
      scoped_refptr<RenderViewHostImpl> render_view_host,
      RenderFrameHostDelegate* delegate,
      FrameTree* frame_tree,
      FrameTreeNode* frame_tree_node,
      int32_t routing_id,
      mojo::PendingAssociatedRemote<mojom::Frame> frame_remote,
      const blink::LocalFrameToken& frame_token,
      const blink::DocumentToken& document_token,
      base::UnguessableToken devtools_frame_token,
      bool renderer_initiated_creation_of_main_frame,
      LifecycleStateImpl lifecycle_state,
      scoped_refptr<BrowsingContextState> browsing_context_state,
      blink::FrameOwnerElementType frame_owner_element_type,
      RenderFrameHostImpl* parent,
      FencedFrameStatus fenced_frame_status)
~~~

### SendCommitNavigation

SendCommitNavigation
~~~cpp
virtual void SendCommitNavigation(
      mojom::NavigationClient* navigation_client,
      NavigationRequest* navigation_request,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
          subresource_loader_factories,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      blink::mojom::ControllerServiceWorkerInfoPtr
          controller_service_worker_info,
      blink::mojom::ServiceWorkerContainerInfoForClientPtr container_info,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          prefetch_loader_factory,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          topics_loader_factory,
      const absl::optional<blink::ParsedPermissionsPolicy>& permissions_policy,
      blink::mojom::PolicyContainerPtr policy_container,
      const blink::DocumentToken& document_token,
      const base::UnguessableToken& devtools_navigation_token);
~~~
 The SendCommit* functions below are wrappers for commit calls
 made to mojom::NavigationClient.

 These exist to be overridden in tests to retain mojo callbacks.

### SendCommitFailedNavigation

SendCommitFailedNavigation
~~~cpp
virtual void SendCommitFailedNavigation(
      mojom::NavigationClient* navigation_client,
      NavigationRequest* navigation_request,
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool has_stale_copy_in_cache,
      int32_t error_code,
      int32_t extended_error_code,
      const absl::optional<std::string>& error_page_content,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
          subresource_loader_factories,
      const blink::DocumentToken& document_token,
      blink::mojom::PolicyContainerPtr policy_container);
~~~

### BuildCommitNavigationCallback

BuildCommitNavigationCallback
~~~cpp
mojom::NavigationClient::CommitNavigationCallback
  BuildCommitNavigationCallback(NavigationRequest* navigation_request);
~~~
 The Build*Callback functions below are responsible for building the
 callbacks for either successful or failed commits.

 Protected because they need to be called from test overrides.

### BuildCommitFailedNavigationCallback

BuildCommitFailedNavigationCallback
~~~cpp
mojom::NavigationClient::CommitFailedNavigationCallback
  BuildCommitFailedNavigationCallback(NavigationRequest* navigation_request);
~~~

### SendBeforeUnload

SendBeforeUnload
~~~cpp
virtual void SendBeforeUnload(bool is_reload,
                                base::WeakPtr<RenderFrameHostImpl> impl,
                                bool for_legacy);
~~~
 Protected / virtual so it can be overridden by tests.

 If `for_legacy` is true, the beforeunload handler is not actually present,
 nor required to run. In this case the renderer is not notified, but
 PostTask() is used. PostTask() is used because synchronously proceeding
 with navigation could lead to reentrancy problems. In particular, there
 are tests and android WebView using NavigationThrottles to navigate from
 WillStartRequest(). If PostTask() is not used, then CHECKs would trigger
 in a NavigationController. See https://crbug.com/365039 for more details.

### GetSibling

GetSibling
~~~cpp
FrameTreeNode* GetSibling(int relative_offset) const;
~~~

### FindAndVerifyChildInternal

FindAndVerifyChildInternal
~~~cpp
FrameTreeNode* FindAndVerifyChildInternal(
      RenderFrameHostOrProxy child_frame_or_proxy,
      bad_message::BadMessageReason reason);
~~~

### OnForwardResourceTimingToParent

OnForwardResourceTimingToParent
~~~cpp
void OnForwardResourceTimingToParent(
      const ResourceTimingInfo& resource_timing);
~~~
 IPC Message handlers.

### OnSetNeedsOcclusionTracking

OnSetNeedsOcclusionTracking
~~~cpp
void OnSetNeedsOcclusionTracking(bool needs_tracking);
~~~

### OnSaveImageFromDataURL

OnSaveImageFromDataURL
~~~cpp
void OnSaveImageFromDataURL(const std::string& url_str);
~~~

### ComputeIsolationInfoInternal

ComputeIsolationInfoInternal
~~~cpp
net::IsolationInfo ComputeIsolationInfoInternal(
      const url::Origin& frame_origin,
      net::IsolationInfo::RequestType request_type,
      bool is_credentialless,
      absl::optional<base::UnguessableToken> fenced_frame_nonce_for_navigation);
~~~
 Computes the IsolationInfo for both navigations and subresources.


 For navigations, |frame_origin| is the origin being navigated to. For
 subresources, |frame_origin| is the value of |last_committed_origin_|. The
 boolean `credentialless` specifies whether this resource should be loaded
 with the restrictions of a credentialless iframe.


 For navigations, populate `fenced_frame_nonce_for_navigation` with
 `NavigationRequest::ComputeFencedFrameNonce()`.

### IsDescendantOfWithinFrameTree

IsDescendantOfWithinFrameTree
~~~cpp
bool IsDescendantOfWithinFrameTree(RenderFrameHostImpl* ancestor);
~~~
 Returns whether or not this RenderFrameHost is a descendant of |ancestor|.

 This is equivalent to check that |ancestor| is reached by iterating on
 GetParent().

 This is a strict relationship, a RenderFrameHost is never an ancestor of
 itself.

 This does not consider inner frame trees (i.e. not accounting for fenced
 frames, portals or GuestView).

### CreateNewWindow

CreateNewWindow
~~~cpp
void CreateNewWindow(mojom::CreateNewWindowParamsPtr params,
                       CreateNewWindowCallback callback) override;
~~~
 mojom::FrameHost:
### GetKeepAliveHandleFactory

GetKeepAliveHandleFactory
~~~cpp
void GetKeepAliveHandleFactory(
      mojo::PendingReceiver<blink::mojom::KeepAliveHandleFactory> receiver)
      override;
~~~

### DidCommitProvisionalLoad

DidCommitProvisionalLoad
~~~cpp
void DidCommitProvisionalLoad(
      mojom::DidCommitProvisionalLoadParamsPtr params,
      mojom::DidCommitProvisionalLoadInterfaceParamsPtr interface_params)
      override;
~~~

### CreateChildFrame

CreateChildFrame
~~~cpp
void CreateChildFrame(
      int new_routing_id,
      mojo::PendingAssociatedRemote<mojom::Frame> frame_remote,
      mojo::PendingReceiver<blink::mojom::BrowserInterfaceBroker>
          browser_interface_broker_receiver,
      blink::mojom::PolicyContainerBindParamsPtr policy_container_bind_params,
      mojo::PendingAssociatedReceiver<blink::mojom::AssociatedInterfaceProvider>
          associated_interface_provider_receiver,
      blink::mojom::TreeScopeType scope,
      const std::string& frame_name,
      const std::string& frame_unique_name,
      bool is_created_by_script,
      const blink::FramePolicy& frame_policy,
      blink::mojom::FrameOwnerPropertiesPtr frame_owner_properties,
      blink::FrameOwnerElementType owner_type,
      ukm::SourceId document_ukm_source_id) override;
~~~

### DidCommitSameDocumentNavigation

DidCommitSameDocumentNavigation
~~~cpp
void DidCommitSameDocumentNavigation(
      mojom::DidCommitProvisionalLoadParamsPtr params,
      mojom::DidCommitSameDocumentNavigationParamsPtr same_document_params)
      override;
~~~

### DidOpenDocumentInputStream

DidOpenDocumentInputStream
~~~cpp
void DidOpenDocumentInputStream(const GURL& url) override;
~~~

### BeginNavigation

BeginNavigation
~~~cpp
void BeginNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::BeginNavigationParamsPtr begin_params,
      mojo::PendingRemote<blink::mojom::BlobURLToken> blob_url_token,
      mojo::PendingAssociatedRemote<mojom::NavigationClient> navigation_client,
      mojo::PendingRemote<blink::mojom::PolicyContainerHostKeepAliveHandle>
          initiator_policy_container_host_keep_alive_handle,
      mojo::PendingReceiver<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener) override;
~~~
 |initiator_policy_container_host_keep_alive_handle| is needed to
 ensure that the PolicyContainerHost of the initiator RenderFrameHost
 can still be retrieved even if the RenderFrameHost has been deleted in
 between.

### SubresourceResponseStarted

SubresourceResponseStarted
~~~cpp
void SubresourceResponseStarted(const url::SchemeHostPort& final_response_url,
                                  net::CertStatus cert_status) override;
~~~

### ResourceLoadComplete

ResourceLoadComplete
~~~cpp
void ResourceLoadComplete(
      blink::mojom::ResourceLoadInfoPtr resource_load_info) override;
~~~

### DidChangeName

DidChangeName
~~~cpp
void DidChangeName(const std::string& name,
                     const std::string& unique_name) override;
~~~

### CancelInitialHistoryLoad

CancelInitialHistoryLoad
~~~cpp
void CancelInitialHistoryLoad() override;
~~~

### DidInferColorScheme

DidInferColorScheme
~~~cpp
void DidInferColorScheme(
      blink::mojom::PreferredColorScheme color_scheme) override;
~~~

### UpdateEncoding

UpdateEncoding
~~~cpp
void UpdateEncoding(const std::string& encoding) override;
~~~

### UpdateState

UpdateState
~~~cpp
void UpdateState(const blink::PageState& state) override;
~~~

### OpenURL

OpenURL
~~~cpp
void OpenURL(blink::mojom::OpenURLParamsPtr params) override;
~~~

### DidStopLoading

DidStopLoading
~~~cpp
void DidStopLoading() override;
~~~

### GetAssociatedInterface

GetAssociatedInterface
~~~cpp
void GetAssociatedInterface(
      const std::string& name,
      mojo::PendingAssociatedReceiver<blink::mojom::AssociatedInterface>
          receiver) override;
~~~
 blink::mojom::AssociatedInterfaceProvider:
### UpdateUserGestureCarryoverInfo

UpdateUserGestureCarryoverInfo
~~~cpp
void UpdateUserGestureCarryoverInfo() override;
~~~

### HandleAXEvents

HandleAXEvents
~~~cpp
void HandleAXEvents(const ui::AXTreeID& tree_id,
                      blink::mojom::AXUpdatesAndEventsPtr updates_and_events,
                      int32_t reset_token);
~~~

### HandleAXLocationChanges

HandleAXLocationChanges
~~~cpp
void HandleAXLocationChanges(
      const ui::AXTreeID& tree_id,
      std::vector<blink::mojom::LocationChangesPtr> changes);
~~~

### DomOperationResponse

DomOperationResponse
~~~cpp
void DomOperationResponse(const std::string& json_string) override;
~~~
 mojom::DomAutomationControllerHost:
### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::CookieAccessObserver>
                 observer) override;
~~~
 network::mojom::CookieAccessObserver
### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::TrustTokenAccessObserver>
                 observer) override;
~~~
 network::mojom::TrustTokenAccessObserver
### ResetWaitingState

ResetWaitingState
~~~cpp
void ResetWaitingState();
~~~
 Resets any waiting state of this RenderFrameHost that is no longer
 relevant.

### CanCommitOriginAndUrl

CanCommitOriginAndUrl
~~~cpp
CanCommitStatus CanCommitOriginAndUrl(const url::Origin& origin,
                                        const GURL& url,
                                        bool is_same_document_navigation,
                                        bool is_pdf,
                                        bool is_sandboxed);
~~~
 Returns whether the given origin and URL is allowed to commit in the
 current RenderFrameHost. The |url| is used to ensure it matches the origin
 in cases where it is applicable. This is a more conservative check than
 RenderProcessHost::FilterURL, since it will be used to kill processes that
 commit unauthorized origins.

### CanSubframeCommitOriginAndUrl

CanSubframeCommitOriginAndUrl
~~~cpp
bool CanSubframeCommitOriginAndUrl(NavigationRequest* navigation_request);
~~~
 Returns whether a subframe navigation request should be allowed to commit
 to the current RenderFrameHost.

### IsSameSiteInstance

IsSameSiteInstance
~~~cpp
bool IsSameSiteInstance(RenderFrameHostImpl* other_render_frame_host);
~~~
 Asserts that the given RenderFrameHostImpl is part of the same browser
 context (and crashes if not), then returns whether the given frame is
 part of the same site instance.

### CanAccessFilesOfPageState

CanAccessFilesOfPageState
~~~cpp
bool CanAccessFilesOfPageState(const blink::PageState& state);
~~~
 Returns whether the current RenderProcessHost has read access to all the
 files reported in |state|.

### GrantFileAccessFromPageState

GrantFileAccessFromPageState
~~~cpp
void GrantFileAccessFromPageState(const blink::PageState& validated_state);
~~~
 Grants the current RenderProcessHost read access to any file listed in
 |validated_state|.  It is important that the PageState has been validated
 upon receipt from the renderer process to prevent it from forging access to
 files without the user's consent.

### GrantFileAccessFromResourceRequestBody

GrantFileAccessFromResourceRequestBody
~~~cpp
void GrantFileAccessFromResourceRequestBody(
      const network::ResourceRequestBody& body);
~~~
 Grants the current RenderProcessHost read access to any file listed in
 |body|.  It is important that the ResourceRequestBody has been validated
 upon receipt from the renderer process to prevent it from forging access to
 files without the user's consent.

### UpdatePermissionsForNavigation

UpdatePermissionsForNavigation
~~~cpp
void UpdatePermissionsForNavigation(NavigationRequest* request);
~~~

### WindowManagementAllowsFullscreen

WindowManagementAllowsFullscreen
~~~cpp
bool WindowManagementAllowsFullscreen();
~~~
 Returns true if there is an active transient fullscreen allowance for the
 Window Management feature (i.e. on screen configuration changes).

### FindLatestNavigationRequestThatIsStillCommitting

FindLatestNavigationRequestThatIsStillCommitting
~~~cpp
NavigationRequest* FindLatestNavigationRequestThatIsStillCommitting();
~~~
 Returns the latest NavigationRequest that has resulted in sending a Commit
 IPC to the renderer process that hasn't yet been acked by the DidCommit IPC
 from the renderer process.  Returns null if no such NavigationRequest
 exists.

### CreateURLLoaderFactoryParamsForMainWorld

CreateURLLoaderFactoryParamsForMainWorld
~~~cpp
network::mojom::URLLoaderFactoryParamsPtr
  CreateURLLoaderFactoryParamsForMainWorld(
      const SubresourceLoaderFactoriesConfig& config,
      base::StringPiece debug_tag);
~~~
 Creates URLLoaderFactoryParams for main world of |this|, either based on
 the |navigation_request|, or (if |navigation_request| is null) on the last
 committed navigation.

### CreateNetworkServiceDefaultFactoryAndObserve

CreateNetworkServiceDefaultFactoryAndObserve
~~~cpp
bool CreateNetworkServiceDefaultFactoryAndObserve(
      network::mojom::URLLoaderFactoryParamsPtr params,
      ukm::SourceIdObj ukm_source_id,
      mojo::PendingReceiver<network::mojom::URLLoaderFactory>
          default_factory_receiver);
~~~
 Like CreateNetworkServiceDefaultFactoryInternal but also sets up a
 connection error handler to detect and recover from NetworkService
 crashes.

### CreateNetworkServiceDefaultFactoryInternal

CreateNetworkServiceDefaultFactoryInternal
~~~cpp
bool CreateNetworkServiceDefaultFactoryInternal(
      network::mojom::URLLoaderFactoryParamsPtr params,
      ukm::SourceIdObj ukm_source_id,
      mojo::PendingReceiver<network::mojom::URLLoaderFactory>
          default_factory_receiver);
~~~
 Asks an appropriate `NetworkService` to create a new URLLoaderFactory with
 the given `params` and binds the factory to the `default_factory_receiver`.


 The callers typically base the `params` on either 1) the current state of
 `this` RenderFrameHostImpl (e.g. the origin of the initial empty document,
 or the last committed origin) or 2) a pending NavigationRequest.


 If this returns true, any redirect safety checks should be bypassed in
 downstream loaders.  (This indicates that a layer above //content has
 wrapped `default_factory_receiver` and may inject arbitrary redirects - for
 example see WebRequestAPI::MaybeProxyURLLoaderFactory.)
### WillCreateURLLoaderFactory

WillCreateURLLoaderFactory
~~~cpp
void WillCreateURLLoaderFactory(
      const url::Origin& request_initiator,
      mojo::PendingReceiver<network::mojom::URLLoaderFactory>* factory_receiver,
      ukm::SourceIdObj ukm_source_id,
      mojo::PendingRemote<network::mojom::TrustedURLLoaderHeaderClient>*
          header_client = nullptr,
      bool* bypass_redirect_checks = nullptr,
      bool* disable_secure_dns = nullptr,
      network::mojom::URLLoaderFactoryOverridePtr* factory_override = nullptr);
~~~
 Lets ContentBrowserClient and devtools_instrumentation wrap the subresource
 factories before they are sent to a renderer process.

### CanExecuteJavaScript

CanExecuteJavaScript
~~~cpp
bool CanExecuteJavaScript();
~~~
 Returns true if the ExecuteJavaScript() API can be used on this host.

 The checks do not apply to ExecuteJavaScriptInIsolatedWorld, nor to
 ExecuteJavaScriptForTests.  See also AssertNonSpeculativeFrame method.

### GetParentAXTreeID

GetParentAXTreeID
~~~cpp
ui::AXTreeID GetParentAXTreeID();
~~~
 Returns the AXTreeID of the parent when the current frame is a child frame
 (i.e. not a main frame) or when it's an embedded browser plugin guest, or
 ui::AXTreeIDUnknown() otherwise.

### GetFocusedAXTreeID

GetFocusedAXTreeID
~~~cpp
ui::AXTreeID GetFocusedAXTreeID();
~~~
 Returns the AXTreeID of the currently focused frame in the frame tree if
 the current frame is the root frame, or ui::AXTreeIDUnknown otherwise.

### GetAXTreeData

GetAXTreeData
~~~cpp
ui::AXTreeData GetAXTreeData();
~~~
 Returns the AXTreeData associated to the current frame, ensuring that the
 AXTreeIDs values for the current, parent and focused frames are up to date.

### AccessibilityHitTestCallback

AccessibilityHitTestCallback
~~~cpp
void AccessibilityHitTestCallback(
      int action_request_id,
      ax::mojom::Event event_to_fire,
      base::OnceCallback<void(ui::AXPlatformTreeManager* hit_manager,
                              ui::AXNodeID hit_node_id)> opt_callback,
      blink::mojom::HitTestResponsePtr hit_test_response);
~~~
 Callback in response to an accessibility hit test triggered by
 AccessibilityHitTest.

### RequestAXTreeSnapshotCallback

RequestAXTreeSnapshotCallback
~~~cpp
void RequestAXTreeSnapshotCallback(AXTreeSnapshotCallback callback,
                                     const ui::AXTreeUpdate& snapshot);
~~~
 Callback that will be called as a response to the call to the method
 content::mojom::RenderAccessibility::SnapshotAccessibilityTree(). The
 |callback| passed will be invoked after the renderer has responded with a
 standalone snapshot of the accessibility tree as |snapshot|.

### CopyAXTreeUpdate

CopyAXTreeUpdate
~~~cpp
void CopyAXTreeUpdate(const ui::AXTreeUpdate& snapshot,
                        ui::AXTreeUpdate* snapshot_copy);
~~~
 Makes a copy of an AXTreeUpdate to send to the destination.

### GetSavableResourceLinksCallback

GetSavableResourceLinksCallback
~~~cpp
void GetSavableResourceLinksCallback(
      blink::mojom::GetSavableResourceLinksReplyPtr reply);
~~~
 Callback that will be called as a response to the call to the method
 blink::mojom::LocalFrame::GetSavableResourceLinks(). The |reply| passed
 will be a nullptr when the url is not the savable URLs or valid.

### GetWebBluetoothServiceForTesting

GetWebBluetoothServiceForTesting
~~~cpp
WebBluetoothServiceImpl* GetWebBluetoothServiceForTesting();
~~~
 Returns a raw pointer to the last Web Bluetooth Service associated with the
 frame, or nullptr otherwise. Used for testing purposes only (see
 |TestRenderFrameHost|).

### DisableUnloadTimerForTesting

DisableUnloadTimerForTesting
~~~cpp
void DisableUnloadTimerForTesting();
~~~
 Allows tests to disable the unload event timer to simulate bugs that
 happen before it fires (to avoid flakiness).

### CreateNavigationRequestForSynchronousRendererCommit

CreateNavigationRequestForSynchronousRendererCommit
~~~cpp
std::unique_ptr<NavigationRequest>
  CreateNavigationRequestForSynchronousRendererCommit(
      const GURL& url,
      const url::Origin& origin,
      const absl::optional<GURL>& initiator_base_url,
      blink::mojom::ReferrerPtr referrer,
      const ui::PageTransition& transition,
      bool should_replace_current_entry,
      bool has_user_gesture,
      const std::vector<GURL>& redirects,
      const GURL& original_request_url,
      bool is_same_document,
      bool is_same_document_history_api_navigation);
~~~
 Creates a NavigationRequest for synchronous navigation that have committed
 in the renderer process. Those are:
 - same-document renderer-initiated navigations.

 - synchronous about:blank navigations.


 TODO(clamy): Eventually, this should only be called for same-document
 renderer-initiated navigations.

### ProcessBeforeUnloadCompleted

ProcessBeforeUnloadCompleted
~~~cpp
void ProcessBeforeUnloadCompleted(
      bool proceed,
      bool treat_as_final_completion_callback,
      const base::TimeTicks& renderer_before_unload_start_time,
      const base::TimeTicks& renderer_before_unload_end_time,
      bool for_legacy);
~~~
 Helper to process the beforeunload completion callback. |proceed| indicates
 whether the navigation or tab close should be allowed to proceed.  If
 |treat_as_final_completion_callback| is true, the frame should stop waiting
 for any further completion callbacks from subframes. Completion callbacks
 invoked from the renderer set |treat_as_final_completion_callback| to
 false, whereas a beforeunload timeout sets it to true. See
 SendBeforeUnload() for details on `for_legacy`.

### GetBeforeUnloadInitiator

GetBeforeUnloadInitiator
~~~cpp
RenderFrameHostImpl* GetBeforeUnloadInitiator();
~~~
 Find the frame that triggered the beforeunload handler to run in this
 frame, which might be the frame itself or its ancestor.  This will
 return the frame that is navigating, or the main frame if beforeunload was
 triggered by closing the current tab.  It will return null if no
 beforeunload is currently in progress.

### ProcessBeforeUnloadCompletedFromFrame

ProcessBeforeUnloadCompletedFromFrame
~~~cpp
void ProcessBeforeUnloadCompletedFromFrame(
      bool proceed,
      bool treat_as_final_completion_callback,
      RenderFrameHostImpl* frame,
      bool is_frame_being_destroyed,
      const base::TimeTicks& renderer_before_unload_start_time,
      const base::TimeTicks& renderer_before_unload_end_time,
      bool for_legacy);
~~~
 Called when a particular frame finishes running a beforeunload handler,
 possibly as part of processing beforeunload for an ancestor frame. In
 that case, this is called on the ancestor frame that is navigating or
 closing, and |frame| indicates which beforeunload completion callback has
 been invoked on. If a beforeunload timeout occurred,
 |treat_as_final_completion_callback| is set to true.

 |is_frame_being_destroyed| is set to true if this was called as part of
 destroying |frame|. See SendBeforeUnload() for details on `for_legacy`.

### CheckOrDispatchBeforeUnloadForSubtree

CheckOrDispatchBeforeUnloadForSubtree
~~~cpp
bool CheckOrDispatchBeforeUnloadForSubtree(
      bool subframes_only,
      bool send_ipc,
      bool is_reload,
      bool* no_dispatch_because_avoid_unnecessary_sync);
~~~
 Helper function to check whether the current frame and its subframes need
 to run beforeunload and, if |send_ipc| is true, send all the necessary
 IPCs for this frame's subtree. If |send_ipc| is false, this only checks
 whether beforeunload is needed and returns the answer.  |subframes_only|
 indicates whether to only check subframes of the current frame, and skip
 the current frame itself.

 |no_dispatch_because_avoid_unnecessary_sync| is set to true if there are no
 before-unload handlers because the feature
 |kAvoidUnnecessaryBeforeUnloadCheckSync| is enabled (see description of
 |kAvoidUnnecessaryBeforeUnloadCheckSync| for more details).

### CheckOrDispatchBeforeUnloadForFrame

CheckOrDispatchBeforeUnloadForFrame
~~~cpp
FrameIterationAction CheckOrDispatchBeforeUnloadForFrame(
      bool subframes_only,
      bool send_ipc,
      bool is_reload,
      bool* found_beforeunload,
      bool* run_beforeunload_for_legacy,
      RenderFrameHostImpl* rfh);
~~~
 |ForEachRenderFrameHost|'s callback used in
 |CheckOrDispatchBeforeUnloadForSubtree|.

### BeforeUnloadTimeout

BeforeUnloadTimeout
~~~cpp
void BeforeUnloadTimeout();
~~~
 Called by |beforeunload_timeout_| when the beforeunload timeout fires.

### SetLastCommittedOrigin

SetLastCommittedOrigin
~~~cpp
void SetLastCommittedOrigin(const url::Origin& origin);
~~~
 Update this frame's last committed origin.

### SetInheritedBaseUrl

SetInheritedBaseUrl
~~~cpp
void SetInheritedBaseUrl(const GURL& inherited_base_url);
~~~
 Stores a snapshot of the inherited base URL from the initiator's
 FrameLoadRequest, if this document inherited one (e.g., about:srcdoc).

 This value is currently only set when the NewBaseUrlInheritanceBehavior
 feature is enabled.

 TODO(1356658): about:blank frames will also need to inherit base URLs,
 from the initiator rather than the parent. See
 https://crbug.com/1356658#c7.

### SetLastCommittedSiteInfo

SetLastCommittedSiteInfo
~~~cpp
void SetLastCommittedSiteInfo(const UrlInfo& url_info);
~~~
 Called when a navigation commits successfully to |url_info->url|. This
 will update |last_committed_site_info_| with the SiteInfo corresponding to
 |url_info|.  If |url_info| is empty, |last_committed_site_info_| will be
 cleared.


 Note that this will recompute the SiteInfo from |url_info| rather than
 using GetSiteInstance()->GetSiteInfo(), so that
 |last_committed_site_info_| is always meaningful: e.g., without site
 isolation, b.com could commit in a SiteInstance for a.com, but this
 function will still compute the last committed SiteInfo as b.com.  For
 example, this can be used to track which sites have committed in which
 process.

### ResetPermissionsPolicy

ResetPermissionsPolicy
~~~cpp
void ResetPermissionsPolicy();
~~~
 Clears any existing policy and constructs a new policy for this frame,
 based on its parent frame.

### ForEachImmediateLocalRoot

ForEachImmediateLocalRoot
~~~cpp
void ForEachImmediateLocalRoot(
      base::FunctionRef<void(RenderFrameHostImpl*)> func_ref);
~~~
 Runs |callback| for all the local roots immediately under this frame, i.e.

 local roots which are under this frame and their first ancestor which is a
 local root is either this frame or this frame's local root. For instance,
 in a frame tree such as:
                    A0                   //
                 /  |   \                //
                B   A1   E               //
               /   /  \   \              //
              D  A2    C   F             //
 RFHs at nodes B, E, D, C, and F are all local roots in the given frame tree
 under the root at A0, but only B, C, and E are considered immediate local
 roots of A0. Note that this will exclude any speculative RFHs.

### ForEachRenderFrameHostImpl

ForEachRenderFrameHostImpl
~~~cpp
void ForEachRenderFrameHostImpl(
      base::FunctionRef<FrameIterationAction(RenderFrameHostImpl*)> on_frame,
      bool include_speculative);
~~~
 This is the actual implementation of the various overloads of
 |ForEachRenderFrameHost|.

### GetMojomFrameInRenderer

GetMojomFrameInRenderer
~~~cpp
mojom::Frame* GetMojomFrameInRenderer();
~~~
 Returns the mojom::Frame interface for this frame in the renderer process.

 May be overridden by friend subclasses for e.g. tests which wish to
 intercept outgoing messages. May only be called when the RenderFrame in the
 renderer exists (i.e. RenderFrameCreated() is true).

### ValidateDidCommitParams

ValidateDidCommitParams
~~~cpp
bool ValidateDidCommitParams(NavigationRequest* navigation_request,
                               mojom::DidCommitProvisionalLoadParams* params,
                               bool is_same_document_navigation);
~~~
 Utility function used to validate potentially harmful parameters sent by
 the renderer during the commit notification.

 A return value of true means that the commit should proceed.

### ValidateURLAndOrigin

ValidateURLAndOrigin
~~~cpp
bool ValidateURLAndOrigin(const GURL& url,
                            const url::Origin& origin,
                            bool is_same_document_navigation,
                            NavigationRequest* navigation_request);
~~~
 Validates whether we can commit |url| and |origin| for a navigation or a
 document.open() URL update.

 A return value of true means that the URL & origin can be committed.

### DidCommitNavigation

DidCommitNavigation
~~~cpp
void DidCommitNavigation(
      NavigationRequest* committing_navigation_request,
      mojom::DidCommitProvisionalLoadParamsPtr params,
      mojom::DidCommitProvisionalLoadInterfaceParamsPtr interface_params);
~~~
 The actual implementation of committing a navigation in the browser
 process. Called by the DidCommitProvisionalLoad IPC handler.

### UpdateIsolatableSandboxedIframeTracking

UpdateIsolatableSandboxedIframeTracking
~~~cpp
void UpdateIsolatableSandboxedIframeTracking(
      NavigationRequest* navigation_request);
~~~
 Updates tracking of potentially isolatable sandboxed iframes, i.e. iframes
 that could be isolated if features::kIsolateSandboxedIframes is enabled.

 A frame can only be an out-of-process sandboxed iframe (OOPSIF) if, in
 addition to the iframe being sandboxed, the url being committed is not
 about:blank and is same-site to the parent's site (i.e. is not already an
 OOPIF). Also, the sandbox permissions must not include 'allow-same-origin'.

 Anytime the commit is a potential OOPSIF, this RenderFrameHostImpl will be
 tracked in a global list from which we can determine how many potential
 OOPSIFs exist at any instant in time. Metrics based on the tracked
 isolatable frames are generated in LogSandboxedIframesIsolationMetrics()
 when it is called by the metrics recording codepath. Note: sandboxed main
 frames that have been opened by an OOPSIF are considered isolatable for the
 purposes of this function, since they could lead to process overhead under
 a per-origin isolation model. Assumes that `policy_container_host_` is set.

### DidCommitNavigationInternal

DidCommitNavigationInternal
~~~cpp
bool DidCommitNavigationInternal(
      std::unique_ptr<NavigationRequest> navigation_request,
      mojom::DidCommitProvisionalLoadParamsPtr params,
      mojom::DidCommitSameDocumentNavigationParamsPtr same_document_params);
~~~
 Called when we receive the confirmation that a navigation committed in the
 renderer. Used by both DidCommitSameDocumentNavigation and
 DidCommitNavigation.

 Returns true if the navigation did commit properly, false if the commit
 state should be restored to its pre-commit value.

### ShouldResetDocumentAssociatedDataAtCommit

ShouldResetDocumentAssociatedDataAtCommit
~~~cpp
bool ShouldResetDocumentAssociatedDataAtCommit() const;
~~~
 Whether or not to reset DocumentAssociatedData at commit. Normally, this
 data is reset with each cross-document navigation, but there are some
 exceptions involving speculative RenderFrameHosts.

### DidCommitNewDocument

DidCommitNewDocument
~~~cpp
void DidCommitNewDocument(const mojom::DidCommitProvisionalLoadParams& params,
                            NavigationRequest* navigation);
~~~
 Called when we received the confirmation a new document committed in the
 renderer. It was created from the |navigation|.

### TakeNewDocumentPropertiesFromNavigation

TakeNewDocumentPropertiesFromNavigation
~~~cpp
void TakeNewDocumentPropertiesFromNavigation(
      NavigationRequest* navigation_request);
~~~
 Transfers several document properties stored by |navigation_request| into
 |this|. Helper for DidCommitNewDocument.

### OnSameDocumentCommitProcessed

OnSameDocumentCommitProcessed
~~~cpp
void OnSameDocumentCommitProcessed(
      const base::UnguessableToken& navigation_token,
      bool should_replace_current_entry,
      blink::mojom::CommitResult result);
~~~
 Called by the renderer process when it is done processing a same-document
 commit request.

### CreateURLLoaderFactoriesForIsolatedWorlds

CreateURLLoaderFactoriesForIsolatedWorlds
~~~cpp
blink::PendingURLLoaderFactoryBundle::OriginMap
  CreateURLLoaderFactoriesForIsolatedWorlds(
      const SubresourceLoaderFactoriesConfig& config,
      const base::flat_set<url::Origin>& isolated_world_origins);
~~~
 Creates URLLoaderFactory objects for |isolated_world_origins|.


 Properties of the factories (e.g. their client security state) are either
 based on the |navigation_request|, or (if |navigation_request| is null) on
 the last committed navigation.


 TODO(https://crbug.com/1098410): Remove the method below once Chrome
 Platform Apps are gone.

### MaybeGenerateCrashReport

MaybeGenerateCrashReport
~~~cpp
void MaybeGenerateCrashReport(base::TerminationStatus status, int exit_code);
~~~
 Based on the termination |status| and |exit_code|, may generate a crash
 report to be routed to the Reporting API.

### FindSuddenTerminationHandlers

FindSuddenTerminationHandlers
~~~cpp
uint32_t FindSuddenTerminationHandlers(bool same_origin);
~~~
 Returns information to be recoreded in UMA about sudden termination
 disablers presence.

 The return value is ORed values from
 `NavigationSuddenTerminationDisablerType`.

 If `same_origin` is true, the tree traversal will only recurse into frames
 that are the same origin as `this`. E.g. a(b(a)) will not find the second
 a.

### StartPendingDeletionOnSubtree

StartPendingDeletionOnSubtree
~~~cpp
void StartPendingDeletionOnSubtree(
      PendingDeletionReason pending_deletion_reason);
~~~
 Move every child frame into the pending deletion state.

 For each process, send the command to delete the local subtree, and wait
 for unload handlers to finish if needed.

 This function can be called because the FrameTreeNode containing this
 RenderFrameHost is getting detached, or if the RenderFrameHost is being
 unloaded because another RenderFrameHost had committed in the
 FrameTreeNode.

### PendingDeletionCheckCompleted

PendingDeletionCheckCompleted
~~~cpp
void PendingDeletionCheckCompleted();
~~~
 This function checks whether a pending deletion frame and all of its
 subframes have completed running unload handlers. If so, this function
 destroys this frame. This will happen as soon as...

 1) The children in other processes have been deleted.

 2) The ack (mojo::AgentSchedulingGroupHost::DidUnloadRenderFrame or
 mojom::FrameHost::Detach) has been
    received. It means this frame in the renderer process is gone.

### PendingDeletionCheckCompletedOnSubtree

PendingDeletionCheckCompletedOnSubtree
~~~cpp
void PendingDeletionCheckCompletedOnSubtree();
~~~
 Call |PendingDeletionCheckCompleted| recursively on this frame and its
 children. This is useful for pruning frames with no unload handlers from
 this frame's subtree.

### ResetNavigationsUsingSwappedOutRFH

ResetNavigationsUsingSwappedOutRFH
~~~cpp
void ResetNavigationsUsingSwappedOutRFH();
~~~
 In this RenderFramehost, cancels every:
 - Non-pending commit NavigationRequest owned by the FrameTreeNode that
 intends to commit in this RFH
 - Pending commit NavigationRequest owned by the RenderFrameHost
 This function should only be called on swapped out RenderFrameHosts.

### OnUnloadTimeout

OnUnloadTimeout
~~~cpp
void OnUnloadTimeout();
~~~
 Called on an unloading frame when its unload timeout is reached. This
 immediately deletes the RenderFrameHost.

### MaybeInterceptCommitCallback

MaybeInterceptCommitCallback
~~~cpp
bool MaybeInterceptCommitCallback(
      NavigationRequest* navigation_request,
      mojom::DidCommitProvisionalLoadParamsPtr* params,
      mojom::DidCommitProvisionalLoadInterfaceParamsPtr* interface_params);
~~~
 Runs interception set up in testing code, if any.

 Returns true if we should proceed to the Commit callback, false otherwise.

### GetLocalRenderWidgetHost

GetLocalRenderWidgetHost
~~~cpp
RenderWidgetHostImpl* GetLocalRenderWidgetHost() const;
~~~
 If this RenderFrameHost is a local root (i.e., either the main frame or a
 subframe in a different process than its parent), this returns the
 RenderWidgetHost corresponding to this frame. Otherwise this returns null.

 See also GetRenderWidgetHost(), which walks up the tree to find the nearest
 local root.

 Main frame: RenderWidgetHost is owned by the RenderViewHost.

 Subframe: RenderWidgetHost is owned by this RenderFrameHost.

### EnsureDescendantsAreUnloading

EnsureDescendantsAreUnloading
~~~cpp
void EnsureDescendantsAreUnloading();
~~~
 Called after a new document commit. Every children of the previous document
 are expected to be deleted or at least to be pending deletion waiting for
 unload completion. A compromised renderer process or bugs can cause the
 renderer to "forget" to start deletion. In this case the browser deletes
 them immediately, without waiting for unload completion.

 https://crbug.com/950625.

### AddMessageToConsoleImpl

AddMessageToConsoleImpl
~~~cpp
void AddMessageToConsoleImpl(blink::mojom::ConsoleMessageLevel level,
                               const std::string& message,
                               bool discard_duplicates);
~~~
 Implements AddMessageToConsole() and AddUniqueMessageToConsole().

### LogCannotCommitUrlCrashKeys

LogCannotCommitUrlCrashKeys
~~~cpp
void LogCannotCommitUrlCrashKeys(const GURL& url,
                                   bool is_same_document_navigation,
                                   NavigationRequest* navigation_request);
~~~
 Helper functions for logging crash keys when ValidateDidCommitParams()
 determines it cannot commit a URL or origin.

### LogCannotCommitOriginCrashKeys

LogCannotCommitOriginCrashKeys
~~~cpp
void LogCannotCommitOriginCrashKeys(const GURL& url,
                                      const url::Origin& origin,
                                      const ProcessLock& process_lock,
                                      bool is_same_document_navigation,
                                      NavigationRequest* navigation_request);
~~~

### VerifyThatBrowserAndRendererCalculatedDidCommitParamsMatch

VerifyThatBrowserAndRendererCalculatedDidCommitParamsMatch
~~~cpp
void VerifyThatBrowserAndRendererCalculatedDidCommitParamsMatch(
      NavigationRequest* navigation_request,
      const mojom::DidCommitProvisionalLoadParams& params,
      mojom::DidCommitSameDocumentNavigationParamsPtr same_document_params);
~~~
 Verifies that browser-calculated and renderer-calculated values for some
 params in DidCommitProvisionalLoadParams match, to ensure we can completely
 remove the dependence on the renderer-calculated values. Logs crash keys
 and dumps them without crashing if some params don't match.

 See crbug.com/1131832.

### RunJavaScriptDialog

RunJavaScriptDialog
~~~cpp
void RunJavaScriptDialog(const std::u16string& message,
                           const std::u16string& default_prompt,
                           JavaScriptDialogType dialog_type,
                           bool disable_third_party_subframe_suppresion,
                           JavaScriptDialogCallback callback);
~~~
 Common handler for displaying a javascript dialog from the Run*Dialog
 mojo handlers. This method sets up some initial state before asking the
 delegate to create a dialog.

### JavaScriptDialogClosed

JavaScriptDialogClosed
~~~cpp
void JavaScriptDialogClosed(JavaScriptDialogCallback response_callback,
                              bool success,
                              const std::u16string& user_input);
~~~
 Callback function used to handle the dialog being closed. It will reset
 the state in the associated RenderFrameHostImpl and call the associated
 callback when done.

### HasSeenRecentXrOverlaySetup

HasSeenRecentXrOverlaySetup
~~~cpp
bool HasSeenRecentXrOverlaySetup();
~~~
 See |SetIsXrOverlaySetup()|
### has_unload_handlers

has_unload_handlers
~~~cpp
bool has_unload_handlers() const {
    return has_unload_handler_ || has_pagehide_handler_ ||
           has_visibilitychange_handler_ || do_not_delete_for_testing_;
  }
~~~

### GetLifecycleStateFromImpl

GetLifecycleStateFromImpl
~~~cpp
static RenderFrameHost::LifecycleState GetLifecycleStateFromImpl(
      LifecycleStateImpl state);
~~~
 Converts a content-internal RenderFrameHostImpl::LifecycleStateImpl into a
 coarser-grained RenderFrameHost::LifecycleState which can be exposed
 outside of content.

### BindReportingObserver

BindReportingObserver
~~~cpp
void BindReportingObserver(
      mojo::PendingReceiver<blink::mojom::ReportingObserver>
          reporting_observer_receiver);
~~~

### CheckSandboxFlags

CheckSandboxFlags
~~~cpp
void CheckSandboxFlags();
~~~
 Check the renderer provided sandbox flags matches with what the browser
 process computed on its own. This triggers DCHECK and DumpWithoutCrashing()

 TODO(https://crbug.com/1041376) Remove this when we are confident the value
 computed from the browser is always matching.

### SetEmbeddingToken

SetEmbeddingToken
~~~cpp
void SetEmbeddingToken(const base::UnguessableToken& embedding_token);
~~~
 Sets the embedding token corresponding to the document in this
 RenderFrameHost.

### RecordDocumentCreatedUkmEvent

RecordDocumentCreatedUkmEvent
~~~cpp
void RecordDocumentCreatedUkmEvent(
      const url::Origin& origin,
      const ukm::SourceId document_ukm_source_id,
      ukm::UkmRecorder* ukm_recorder,
      bool only_record_identifiability_metric = false);
~~~
 Records a DocumentCreated UKM event and the corresponding identifiability
 study metric. Called when a Document is committed in this frame.

### is_render_frame_created

is_render_frame_created
~~~cpp
bool is_render_frame_created() const {
    return render_frame_state_ == RenderFrameState::kCreated;
  }
~~~
 Has the RenderFrame been created in the renderer process and not yet been
 deleted, exited or crashed. See RenderFrameState.

### InitializePolicyContainerHost

InitializePolicyContainerHost
~~~cpp
void InitializePolicyContainerHost(
      bool renderer_initiated_creation_of_main_frame);
~~~
 Initializes |policy_container_host_|. Constructor helper.


 |renderer_initiated_creation_of_main_frame| specifies whether this render
 frame host's creation was initiated by the renderer process, and this is
 a main frame. See the constructor for more details.

### SetPolicyContainerHost

SetPolicyContainerHost
~~~cpp
void SetPolicyContainerHost(
      scoped_refptr<PolicyContainerHost> policy_container_host);
~~~
 Sets |policy_container_host_| and associates it with the current frame.

 |policy_container_host| must not be nullptr.

### InitializeLocalNetworkRequestPolicy

InitializeLocalNetworkRequestPolicy
~~~cpp
void InitializeLocalNetworkRequestPolicy();
~~~
 Initializes |local_network_request_policy_|. Constructor helper.

### RequiresProxyToParent

RequiresProxyToParent
~~~cpp
bool RequiresProxyToParent();
~~~
 Returns true if this frame requires a proxy to talk to its parent.

 Note: Using a proxy to talk to a parent does not imply that the parent
 is in a different process.

 (e.g. kProcessSharingWithStrictSiteInstances mode uses proxies for frames
  that are in the same process.)
### IncreaseCommitNavigationCounter

IncreaseCommitNavigationCounter
~~~cpp
void IncreaseCommitNavigationCounter();
~~~
 Increases by one `commit_navigation_sent_counter_`.

### SetStorageKey

SetStorageKey
~~~cpp
void SetStorageKey(const blink::StorageKey& storage_key);
~~~
 Sets the storage key for the last committed document in this
 RenderFrameHostImpl.

### ShouldWaitForUnloadHandlers

ShouldWaitForUnloadHandlers
~~~cpp
bool ShouldWaitForUnloadHandlers() const;
~~~
 Check if we should wait for unload handlers when shutting down the
 renderer.

### AssertNonSpeculativeFrame

AssertNonSpeculativeFrame
~~~cpp
void AssertNonSpeculativeFrame() const;
~~~
 Asserts that `this` is not a speculative frame and calls
 DumpWithoutCrashing otherwise.  This method should be called from
 RenderFrameHostImpl's methods that require the caller to "be careful" not
 to call them on a speculative frame.  One such example is
 JavaScriptExecuteRequestInIsolatedWorld.

### AssertBrowserContextShutdownHasntStarted

AssertBrowserContextShutdownHasntStarted
~~~cpp
void AssertBrowserContextShutdownHasntStarted();
~~~
 Asserts that the BrowserContext (e.g. Profile in the //chrome layer) of
 this frame hasn't started shutting down.  The owner of the BrowserContext
 is responsible for closing all WebContents before initiating destruction of
 the BrowserContext (and closing the WebContents should destroy all the
 associated RenderFrameHostImpl objects).

### OnBackForwardCacheDisablingFeatureUsed

OnBackForwardCacheDisablingFeatureUsed
~~~cpp
void OnBackForwardCacheDisablingFeatureUsed(
      BackForwardCacheDisablingFeature feature);
~~~
 A feature that blocks back/forward cache is used. Count the usage and evict
 the entry if necessary.

### OnBackForwardCacheDisablingFeatureRemoved

OnBackForwardCacheDisablingFeatureRemoved
~~~cpp
void OnBackForwardCacheDisablingFeatureRemoved(
      BackForwardCacheDisablingFeature feature);
~~~
 A feature that blocks back/forward cache is removed. Update the count of
 feature usage. This should only be called from
 |BackForwardCacheDisablingFeatureHandle|.

### CreateBroadcastChannelProvider

CreateBroadcastChannelProvider
~~~cpp
void CreateBroadcastChannelProvider(
      mojo::PendingAssociatedReceiver<blink::mojom::BroadcastChannelProvider>
          receiver);
~~~
 Create a self-owned receiver that handles incoming BroadcastChannel
 ConnectToChannel messages from the renderer.

### BindBlobUrlStoreAssociatedReceiver

BindBlobUrlStoreAssociatedReceiver
~~~cpp
void BindBlobUrlStoreAssociatedReceiver(
      mojo::PendingAssociatedReceiver<blink::mojom::BlobURLStore> receiver);
~~~
 For frames and main thread worklets we use a navigation-associated
 interface and bind `receiver` to a `BlobURLStore` instance, which
 implements the Blob URL API in the browser process. Note that this is only
 exposed when the kSupportPartitionedBlobUrl flag is enabled.

### LifecycleStateToProto

LifecycleStateToProto
~~~cpp
TraceProto::LifecycleState LifecycleStateToProto() const;
~~~

### GetFrameTypeProto

GetFrameTypeProto
~~~cpp
perfetto::protos::pbzero::FrameTreeNodeInfo::FrameType GetFrameTypeProto()
      const;
~~~

### BindCacheStorageInternal

BindCacheStorageInternal
~~~cpp
void BindCacheStorageInternal(
      mojo::PendingReceiver<blink::mojom::CacheStorage> receiver,
      const storage::BucketLocator& bucket_locator);
~~~

### DidEnterBackForwardCacheInternal

DidEnterBackForwardCacheInternal
~~~cpp
void DidEnterBackForwardCacheInternal();
~~~

### WillLeaveBackForwardCacheInternal

WillLeaveBackForwardCacheInternal
~~~cpp
void WillLeaveBackForwardCacheInternal();
~~~

### GetBackForwardCache

GetBackForwardCache
~~~cpp
BackForwardCacheImpl& GetBackForwardCache();
~~~
 Returns the BackForwardCacheImpl for the outermost main frame.

### GetFrameTreeNodeForUnload

GetFrameTreeNodeForUnload
~~~cpp
FrameTreeNode* GetFrameTreeNodeForUnload();
~~~
 Return the FrameTreeNode currently owning this RenderFrameHost. In general,
 we don't want the RenderFrameHost to depend on it, because it might change,
 or might be missing. An exception is made here during unload. It is invalid
 to use this function elsewhere.

 In other cases, the use of the RenderFrameHostOwner interface should be
 used for communicating with the FrameTreeNode.

### ClosePageIgnoringUnloadEvents

ClosePageIgnoringUnloadEvents
~~~cpp
void ClosePageIgnoringUnloadEvents(ClosePageSource source);
~~~
 Close the page ignoring whether it has unload events registered. This is
 called either (1) when the unload events have already run in the renderer
 and the ACK is received, or (2) when a timeout for running those events
 has expired. The `source` parameter indicates whether this request comes
 from the browser or the renderer. If the request comes from the renderer,
 then it may be ignored if a different document commits first.

### ClosePageTimeout

ClosePageTimeout
~~~cpp
void ClosePageTimeout(ClosePageSource source);
~~~
 Called when this frame's page has started closing via ClosePage(), and the
 timer for running unload events has expired. The `source` parameter
 indicates whether this request comes from the browser or the renderer. If
 the request comes from the renderer, then it may be ignored if a different
 document commits first.

### MaybeSendFencedFrameReportingBeacon

MaybeSendFencedFrameReportingBeacon
~~~cpp
void MaybeSendFencedFrameReportingBeacon(
      NavigationRequest& navigation_request);
~~~
 Send an automatic `reserved.top_navigation` beacon if one was registered
 with the NavigationRequest's initiator frame using the
 `window.fence.setReportEventDataForAutomaticBeacons` API.

### SendFencedFrameReportingBeaconInternal

SendFencedFrameReportingBeaconInternal
~~~cpp
void SendFencedFrameReportingBeaconInternal(
      const std::string& event_data,
      const std::string& event_type,
      blink::FencedFrame::ReportingDestination destination,
      bool from_renderer,
      absl::optional<int64_t> navigation_id = absl::nullopt);
~~~
 Helper function that handles creating and sending a fenced frame beacon.

 This also handles sending console messages if the call to send the beacon
 originated from the renderer.

 Calls to this function for automatic beacons (i.e. the
 "reserved.top_navigation" beacon) will originate from the browser. All
 other fenced frame beacon calls happen through a `fence.sendBeacon()`
 JavaScript call which will originate from the renderer.

### IsThirdPartyStoragePartitioningEnabled

IsThirdPartyStoragePartitioningEnabled
~~~cpp
bool IsThirdPartyStoragePartitioningEnabled(
      RenderFrameHostImpl* main_frame_for_storage_partitioning);
~~~
 Indicates whether this frame has third-party storage
 partitioning enabled. This depends on the deprecation trial (which can
 block), content browser client (which can block), and base feature (which
 can allow/block). `main_frame_for_storage_partitioning` indicates the top
 most frame that third-party storage partitioning is affected by (this can
 be the real main frame or it could be a subframe when taking into account
 special embedding cases, see CalculateStorageKey's implementation for more
 information.)
### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
CONTENT_EXPORT std::ostream& operator<<(
    std::ostream& o,
    const RenderFrameHostImpl::LifecycleStateImpl& s);
~~~
 Used when DCHECK_STATE_TRANSITION triggers.

## class UpdateAXFocusDeferScope
 Updating focus in the presence of multiple frame trees requires multiple
 focus changes. The existence of this class will defer UpdateAXTreeData()
 until this process has finished and the focus states are consistent.

### UpdateAXFocusDeferScope

UpdateAXFocusDeferScope::UpdateAXFocusDeferScope
~~~cpp
explicit UpdateAXFocusDeferScope(RenderFrameHostImpl& rfh);
~~~

### ~UpdateAXFocusDeferScope

UpdateAXFocusDeferScope::~UpdateAXFocusDeferScope
~~~cpp
~UpdateAXFocusDeferScope();
~~~

### rfh_



~~~cpp

const base::SafeRef<RenderFrameHostImpl> rfh_;

~~~


### rfh_



~~~cpp

const base::SafeRef<RenderFrameHostImpl> rfh_;

~~~


## class CookieChangeListener
 Listens to the change events of the cookies associated with the domain of
 the specified URL during initialization. It also contains the information
 of whether any cookie/HTTPOnly cookie had been changed before, which can be
 used to determine if a document with Cache-control: no-store header set is
 eligible for BFCache.

### field error



~~~cpp

struct CookieChangeInfo {
      // The number of observed cookie modifications.
      int64_t cookie_modification_count_ = 0;
      // The number of observed HTTPOnly cookie modifications.
      int64_t http_only_cookie_modification_count_ = 0;
    };

~~~


### CookieChangeListener

CookieChangeListener::CookieChangeListener
~~~cpp
CookieChangeListener(StoragePartition* storage_partition, GURL& url);
~~~

### ~CookieChangeListener

CookieChangeListener::~CookieChangeListener
~~~cpp
~CookieChangeListener() override;
~~~

### CookieChangeListener

CookieChangeListener
~~~cpp
CookieChangeListener(const CookieChangeListener&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieChangeListener& operator=(const CookieChangeListener&) = delete;
~~~

### cookie_change_info

cookie_change_info
~~~cpp
CookieChangeInfo cookie_change_info() { return cookie_change_info_; }
~~~
 Returns a copy of the `cookie_change_info_`.

### RemoveNavigationCookieModificationCount

RemoveNavigationCookieModificationCount
~~~cpp
void RemoveNavigationCookieModificationCount(
        base::PassKey<content::NavigationRequest> navigation_request,
        uint64_t cookie_modification_count_delta,
        uint64_t http_only_cookie_modification_count_delta) {
      cookie_change_info_.cookie_modification_count_ -=
          cookie_modification_count_delta;
      cookie_change_info_.http_only_cookie_modification_count_ -=
          http_only_cookie_modification_count_delta;
    }
~~~
 We don't want to count the cookie modification made by the
 `NavigationRequest` itself, so provide this function to allow the count
 adjustment.

 Passing the `base::PassKey` to restrict the caller of this method to
 `NavigationRequest` only.

### OnCookieChange

CookieChangeListener::OnCookieChange
~~~cpp
void OnCookieChange(const net::CookieChangeInfo& change) override;
~~~
 network::mojom::CookieChangeListener
### cookie_change_listener_receiver_



~~~cpp

mojo::Receiver<network::mojom::CookieChangeListener>
        cookie_change_listener_receiver_{this};

~~~


### cookie_change_info_



~~~cpp

CookieChangeInfo cookie_change_info_;

~~~


### CookieChangeListener

CookieChangeListener
~~~cpp
CookieChangeListener(const CookieChangeListener&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieChangeListener& operator=(const CookieChangeListener&) = delete;
~~~

### cookie_change_info

cookie_change_info
~~~cpp
CookieChangeInfo cookie_change_info() { return cookie_change_info_; }
~~~
 Returns a copy of the `cookie_change_info_`.

### RemoveNavigationCookieModificationCount

RemoveNavigationCookieModificationCount
~~~cpp
void RemoveNavigationCookieModificationCount(
        base::PassKey<content::NavigationRequest> navigation_request,
        uint64_t cookie_modification_count_delta,
        uint64_t http_only_cookie_modification_count_delta) {
      cookie_change_info_.cookie_modification_count_ -=
          cookie_modification_count_delta;
      cookie_change_info_.http_only_cookie_modification_count_ -=
          http_only_cookie_modification_count_delta;
    }
~~~
 We don't want to count the cookie modification made by the
 `NavigationRequest` itself, so provide this function to allow the count
 adjustment.

 Passing the `base::PassKey` to restrict the caller of this method to
 `NavigationRequest` only.

### field error



~~~cpp

struct CookieChangeInfo {
      // The number of observed cookie modifications.
      int64_t cookie_modification_count_ = 0;
      // The number of observed HTTPOnly cookie modifications.
      int64_t http_only_cookie_modification_count_ = 0;
    };

~~~


### OnCookieChange

CookieChangeListener::OnCookieChange
~~~cpp
void OnCookieChange(const net::CookieChangeInfo& change) override;
~~~
 network::mojom::CookieChangeListener
### cookie_change_listener_receiver_



~~~cpp

mojo::Receiver<network::mojom::CookieChangeListener>
        cookie_change_listener_receiver_{this};

~~~


### cookie_change_info_



~~~cpp

CookieChangeInfo cookie_change_info_;

~~~


## class CommitCallbackInterceptor
 A CommitCallbackInterceptor is used to modify parameters for or cancel a
 DidCommitNavigation call in tests.

 WillProcessDidCommitNavigation will be run right after entering a
 navigation callback and if returning false, will return straight away.

### CommitCallbackInterceptor

CommitCallbackInterceptor
~~~cpp
CommitCallbackInterceptor() = default;
~~~

### ~CommitCallbackInterceptor

~CommitCallbackInterceptor
~~~cpp
virtual ~CommitCallbackInterceptor() = default;
~~~

### WillProcessDidCommitNavigation

WillProcessDidCommitNavigation
~~~cpp
virtual bool WillProcessDidCommitNavigation(
        NavigationRequest* navigation_request,
        mojom::DidCommitProvisionalLoadParamsPtr* params,
        mojom::DidCommitProvisionalLoadInterfaceParamsPtr*
            interface_params) = 0;
~~~

### CommitCallbackInterceptor

CommitCallbackInterceptor
~~~cpp
CommitCallbackInterceptor() = default;
~~~

### ~CommitCallbackInterceptor

~CommitCallbackInterceptor
~~~cpp
virtual ~CommitCallbackInterceptor() = default;
~~~

### WillProcessDidCommitNavigation

WillProcessDidCommitNavigation
~~~cpp
virtual bool WillProcessDidCommitNavigation(
        NavigationRequest* navigation_request,
        mojom::DidCommitProvisionalLoadParamsPtr* params,
        mojom::DidCommitProvisionalLoadInterfaceParamsPtr*
            interface_params) = 0;
~~~

## class BackForwardCacheDisablingFeatureHandle

### BackForwardCacheDisablingFeatureHandle

BackForwardCacheDisablingFeatureHandle::BackForwardCacheDisablingFeatureHandle
~~~cpp
BackForwardCacheDisablingFeatureHandle();
~~~

### BackForwardCacheDisablingFeatureHandle

BackForwardCacheDisablingFeatureHandle::BackForwardCacheDisablingFeatureHandle
~~~cpp
BackForwardCacheDisablingFeatureHandle(
        BackForwardCacheDisablingFeatureHandle&&);
~~~

### operator=

operator=
~~~cpp
BackForwardCacheDisablingFeatureHandle& operator=(
        BackForwardCacheDisablingFeatureHandle&& other) = default;
~~~

### ~BackForwardCacheDisablingFeatureHandle

~BackForwardCacheDisablingFeatureHandle
~~~cpp
inline ~BackForwardCacheDisablingFeatureHandle() { reset(); }
~~~

### reset

reset
~~~cpp
inline void reset() {
      if (render_frame_host_)
        render_frame_host_->OnBackForwardCacheDisablingFeatureRemoved(feature_);
      render_frame_host_ = nullptr;
    }
~~~
 This will reduce the feature count for |feature_| for the first time, and
 do nothing for further calls.

### BackForwardCacheDisablingFeatureHandle

BackForwardCacheDisablingFeatureHandle::BackForwardCacheDisablingFeatureHandle
~~~cpp
BackForwardCacheDisablingFeatureHandle(
        RenderFrameHostImpl* render_frame_host,
        BackForwardCacheDisablingFeature feature);
~~~

### render_frame_host_



~~~cpp

base::WeakPtr<RenderFrameHostImpl> render_frame_host_ = nullptr;

~~~


### feature_



~~~cpp

BackForwardCacheDisablingFeature feature_;

~~~


### operator=

operator=
~~~cpp
BackForwardCacheDisablingFeatureHandle& operator=(
        BackForwardCacheDisablingFeatureHandle&& other) = default;
~~~

### ~BackForwardCacheDisablingFeatureHandle

~BackForwardCacheDisablingFeatureHandle
~~~cpp
inline ~BackForwardCacheDisablingFeatureHandle() { reset(); }
~~~

### reset

reset
~~~cpp
inline void reset() {
      if (render_frame_host_)
        render_frame_host_->OnBackForwardCacheDisablingFeatureRemoved(feature_);
      render_frame_host_ = nullptr;
    }
~~~
 This will reduce the feature count for |feature_| for the first time, and
 do nothing for further calls.

### render_frame_host_



~~~cpp

base::WeakPtr<RenderFrameHostImpl> render_frame_host_ = nullptr;

~~~


### feature_



~~~cpp

BackForwardCacheDisablingFeature feature_;

~~~


## class DocumentAssociatedData
 Container for arbitrary document-associated feature-specific data. Should
 be reset when committing a cross-document navigation in this
 RenderFrameHost. RenderFrameHostImpl stores internal members here
 directly while consumers of RenderFrameHostImpl should store data via
 GetDocumentUserData(). Please refer to the description at
 content/public/browser/document_user_data.h for more details.

### DocumentAssociatedData

DocumentAssociatedData::DocumentAssociatedData
~~~cpp
explicit DocumentAssociatedData(RenderFrameHostImpl& document,
                                    const blink::DocumentToken& token);
~~~

### ~DocumentAssociatedData

DocumentAssociatedData::~DocumentAssociatedData
~~~cpp
~DocumentAssociatedData() override;
~~~

### DocumentAssociatedData

DocumentAssociatedData
~~~cpp
DocumentAssociatedData(const DocumentAssociatedData&) = delete;
~~~

### operator=

operator=
~~~cpp
DocumentAssociatedData& operator=(const DocumentAssociatedData&) = delete;
~~~

### token

token
~~~cpp
const blink::DocumentToken& token() const { return token_; }
~~~
 An opaque token that uniquely identifies the document currently
 associated with this RenderFrameHost. Note that in the case of
 speculative RenderFrameHost that has not yet committed, the renderer side
 will not have a document with a matching token!
### owned_page

owned_page
~~~cpp
PageImpl* owned_page() { return owned_page_.get(); }
~~~
 The Page object associated with the main document. It is nullptr for
 subframes.

### owned_page

owned_page
~~~cpp
const PageImpl* owned_page() const { return owned_page_.get(); }
~~~

### dom_content_loaded

dom_content_loaded
~~~cpp
bool dom_content_loaded() const { return dom_content_loaded_; }
~~~
 Indicates whether `blink::mojom::DidDispatchDOMContentLoadedEvent` was
 called for this document or not.

### MarkDomContentLoaded

MarkDomContentLoaded
~~~cpp
void MarkDomContentLoaded() { dom_content_loaded_ = true; }
~~~

### pending_did_finish_load_url_for_prerendering

pending_did_finish_load_url_for_prerendering
~~~cpp
const absl::optional<GURL>& pending_did_finish_load_url_for_prerendering()
        const {
      return pending_did_finish_load_url_for_prerendering_;
    }
~~~
 Prerender2:

 The URL that `blink.mojom.LocalFrameHost::DidFinishLoad()` passed to
 DidFinishLoad, nullopt if DidFinishLoad wasn't called for this document
 or this document is not in prerendering. This is used to defer and
 dispatch DidFinishLoad notification on prerender activation.

### set_pending_did_finish_load_url_for_prerendering

set_pending_did_finish_load_url_for_prerendering
~~~cpp
void set_pending_did_finish_load_url_for_prerendering(const GURL& url) {
      pending_did_finish_load_url_for_prerendering_.emplace(url);
    }
~~~

### reset_pending_did_finish_load_url_for_prerendering

reset_pending_did_finish_load_url_for_prerendering
~~~cpp
void reset_pending_did_finish_load_url_for_prerendering() {
      pending_did_finish_load_url_for_prerendering_.reset();
    }
~~~

### reporting_source

reporting_source
~~~cpp
const base::UnguessableToken& reporting_source() const {
      return token_.value();
    }
~~~
 Reporting API:

 Contains the reporting source token for this document, which will be
 associated with the reporting endpoint configuration in the network
 service, as well as with any reports which are queued by this document.

### services

services
~~~cpp
std::vector<internal::DocumentServiceBase*>& services() {
      return services_;
    }
~~~
 "Owned" but not with std::unique_ptr, as a DocumentServiceBase is
 allowed to delete itself directly.

### navigation_or_document_handle

navigation_or_document_handle
~~~cpp
const scoped_refptr<NavigationOrDocumentHandle>&
    navigation_or_document_handle() const {
      return navigation_or_document_handle_;
    }
~~~
 This handle supports a seamless transfer from a navigation to a committed
 document.

### set_navigation_or_document_handle

DocumentAssociatedData::set_navigation_or_document_handle
~~~cpp
void set_navigation_or_document_handle(
        scoped_refptr<NavigationOrDocumentHandle> handle);
~~~

### weak_factory

weak_factory
~~~cpp
base::WeakPtrFactory<RenderFrameHostImpl>& weak_factory() {
      return weak_factory_;
    }
~~~
 Produces weak pointers to the hosting RenderFrameHostImpl. This is
 invalidated whenever DocumentAssociatedData is destroyed, due to
 RenderFrameHost deletion or cross-document navigation.

### token_



~~~cpp

const blink::DocumentToken token_;

~~~


### owned_page_



~~~cpp

std::unique_ptr<PageImpl> owned_page_;

~~~


### dom_content_loaded_



~~~cpp

bool dom_content_loaded_ = false;

~~~


### pending_did_finish_load_url_for_prerendering_



~~~cpp

absl::optional<GURL> pending_did_finish_load_url_for_prerendering_;

~~~


### services_



~~~cpp

std::vector<internal::DocumentServiceBase*> services_;

~~~


### navigation_or_document_handle_



~~~cpp

scoped_refptr<NavigationOrDocumentHandle> navigation_or_document_handle_;

~~~


### weak_factory_



~~~cpp

base::WeakPtrFactory<RenderFrameHostImpl> weak_factory_;

~~~


### DocumentAssociatedData

DocumentAssociatedData
~~~cpp
DocumentAssociatedData(const DocumentAssociatedData&) = delete;
~~~

### operator=

operator=
~~~cpp
DocumentAssociatedData& operator=(const DocumentAssociatedData&) = delete;
~~~

### token

token
~~~cpp
const blink::DocumentToken& token() const { return token_; }
~~~
 An opaque token that uniquely identifies the document currently
 associated with this RenderFrameHost. Note that in the case of
 speculative RenderFrameHost that has not yet committed, the renderer side
 will not have a document with a matching token!
### owned_page

owned_page
~~~cpp
PageImpl* owned_page() { return owned_page_.get(); }
~~~
 The Page object associated with the main document. It is nullptr for
 subframes.

### owned_page

owned_page
~~~cpp
const PageImpl* owned_page() const { return owned_page_.get(); }
~~~

### dom_content_loaded

dom_content_loaded
~~~cpp
bool dom_content_loaded() const { return dom_content_loaded_; }
~~~
 Indicates whether `blink::mojom::DidDispatchDOMContentLoadedEvent` was
 called for this document or not.

### MarkDomContentLoaded

MarkDomContentLoaded
~~~cpp
void MarkDomContentLoaded() { dom_content_loaded_ = true; }
~~~

### pending_did_finish_load_url_for_prerendering

pending_did_finish_load_url_for_prerendering
~~~cpp
const absl::optional<GURL>& pending_did_finish_load_url_for_prerendering()
        const {
      return pending_did_finish_load_url_for_prerendering_;
    }
~~~
 Prerender2:

 The URL that `blink.mojom.LocalFrameHost::DidFinishLoad()` passed to
 DidFinishLoad, nullopt if DidFinishLoad wasn't called for this document
 or this document is not in prerendering. This is used to defer and
 dispatch DidFinishLoad notification on prerender activation.

### set_pending_did_finish_load_url_for_prerendering

set_pending_did_finish_load_url_for_prerendering
~~~cpp
void set_pending_did_finish_load_url_for_prerendering(const GURL& url) {
      pending_did_finish_load_url_for_prerendering_.emplace(url);
    }
~~~

### reset_pending_did_finish_load_url_for_prerendering

reset_pending_did_finish_load_url_for_prerendering
~~~cpp
void reset_pending_did_finish_load_url_for_prerendering() {
      pending_did_finish_load_url_for_prerendering_.reset();
    }
~~~

### reporting_source

reporting_source
~~~cpp
const base::UnguessableToken& reporting_source() const {
      return token_.value();
    }
~~~
 Reporting API:

 Contains the reporting source token for this document, which will be
 associated with the reporting endpoint configuration in the network
 service, as well as with any reports which are queued by this document.

### services

services
~~~cpp
std::vector<internal::DocumentServiceBase*>& services() {
      return services_;
    }
~~~
 "Owned" but not with std::unique_ptr, as a DocumentServiceBase is
 allowed to delete itself directly.

### navigation_or_document_handle

navigation_or_document_handle
~~~cpp
const scoped_refptr<NavigationOrDocumentHandle>&
    navigation_or_document_handle() const {
      return navigation_or_document_handle_;
    }
~~~
 This handle supports a seamless transfer from a navigation to a committed
 document.

### weak_factory

weak_factory
~~~cpp
base::WeakPtrFactory<RenderFrameHostImpl>& weak_factory() {
      return weak_factory_;
    }
~~~
 Produces weak pointers to the hosting RenderFrameHostImpl. This is
 invalidated whenever DocumentAssociatedData is destroyed, due to
 RenderFrameHost deletion or cross-document navigation.

### set_navigation_or_document_handle

DocumentAssociatedData::set_navigation_or_document_handle
~~~cpp
void set_navigation_or_document_handle(
        scoped_refptr<NavigationOrDocumentHandle> handle);
~~~

### token_



~~~cpp

const blink::DocumentToken token_;

~~~


### owned_page_



~~~cpp

std::unique_ptr<PageImpl> owned_page_;

~~~


### dom_content_loaded_



~~~cpp

bool dom_content_loaded_ = false;

~~~


### pending_did_finish_load_url_for_prerendering_



~~~cpp

absl::optional<GURL> pending_did_finish_load_url_for_prerendering_;

~~~


### services_



~~~cpp

std::vector<internal::DocumentServiceBase*> services_;

~~~


### navigation_or_document_handle_



~~~cpp

scoped_refptr<NavigationOrDocumentHandle> navigation_or_document_handle_;

~~~


### weak_factory_



~~~cpp

base::WeakPtrFactory<RenderFrameHostImpl> weak_factory_;

~~~

