
## class RenderFrameHostImplPpapiSupport

### RenderFrameHostImplPpapiSupport

RenderFrameHostImplPpapiSupport::RenderFrameHostImplPpapiSupport
~~~cpp
explicit RenderFrameHostImplPpapiSupport(RenderFrameHostImpl& frame_host);
~~~

### ~RenderFrameHostImplPpapiSupport

RenderFrameHostImplPpapiSupport::~RenderFrameHostImplPpapiSupport
~~~cpp
~RenderFrameHostImplPpapiSupport() override;
~~~

### Bind

RenderFrameHostImplPpapiSupport::Bind
~~~cpp
void Bind(mojo::PendingAssociatedReceiver<mojom::PepperHost> receiver);
~~~

### SetVolume

RenderFrameHostImplPpapiSupport::SetVolume
~~~cpp
void SetVolume(int32_t instance_id, double volume);
~~~

### InstanceCreated

RenderFrameHostImplPpapiSupport::InstanceCreated
~~~cpp
void InstanceCreated(
      int32_t instance_id,
      mojo::PendingAssociatedRemote<mojom::PepperPluginInstance> instance,
      mojo::PendingAssociatedReceiver<mojom::PepperPluginInstanceHost> host)
      override;
~~~
 mojom::PepperHost overrides:
### BindHungDetectorHost

RenderFrameHostImplPpapiSupport::BindHungDetectorHost
~~~cpp
void BindHungDetectorHost(
      mojo::PendingReceiver<mojom::PepperHungDetectorHost> hung_host,
      int32_t plugin_child_id,
      const base::FilePath& path) override;
~~~

### GetPluginInfo

RenderFrameHostImplPpapiSupport::GetPluginInfo
~~~cpp
void GetPluginInfo(const GURL& url,
                     const std::string& mime_type,
                     GetPluginInfoCallback callback) override;
~~~

### DidCreateInProcessInstance

RenderFrameHostImplPpapiSupport::DidCreateInProcessInstance
~~~cpp
void DidCreateInProcessInstance(int32_t instance,
                                  int32_t render_frame_id,
                                  const GURL& document_url,
                                  const GURL& plugin_url) override;
~~~

### DidDeleteInProcessInstance

RenderFrameHostImplPpapiSupport::DidDeleteInProcessInstance
~~~cpp
void DidDeleteInProcessInstance(int32_t instance) override;
~~~

### DidCreateOutOfProcessPepperInstance

RenderFrameHostImplPpapiSupport::DidCreateOutOfProcessPepperInstance
~~~cpp
void DidCreateOutOfProcessPepperInstance(
      int32_t plugin_child_id,
      int32_t pp_instance,
      bool is_external,
      int32_t render_frame_id,
      const GURL& document_url,
      const GURL& plugin_url,
      bool is_priviledged_context,
      DidCreateOutOfProcessPepperInstanceCallback callback) override;
~~~

### DidDeleteOutOfProcessPepperInstance

RenderFrameHostImplPpapiSupport::DidDeleteOutOfProcessPepperInstance
~~~cpp
void DidDeleteOutOfProcessPepperInstance(int32_t plugin_child_id,
                                           int32_t pp_instance,
                                           bool is_external) override;
~~~

### OpenChannelToPepperPlugin

RenderFrameHostImplPpapiSupport::OpenChannelToPepperPlugin
~~~cpp
void OpenChannelToPepperPlugin(
      const url::Origin& embedder_origin,
      const base::FilePath& path,
      const absl::optional<url::Origin>& origin_lock,
      OpenChannelToPepperPluginCallback callback) override;
~~~

### PluginHung

RenderFrameHostImplPpapiSupport::PluginHung
~~~cpp
void PluginHung(bool is_hung) override;
~~~
 mojom::PepperHungDetectorHost overrides:
### InstanceClosed

RenderFrameHostImplPpapiSupport::InstanceClosed
~~~cpp
void InstanceClosed(int32_t instance_id);
~~~

### render_frame_host

render_frame_host
~~~cpp
RenderFrameHostImpl& render_frame_host() { return *render_frame_host_; }
~~~

### GetProcess

RenderFrameHostImplPpapiSupport::GetProcess
~~~cpp
RenderProcessHostImpl* GetProcess();
~~~

### render_frame_host_



~~~cpp

const raw_ref<RenderFrameHostImpl> render_frame_host_;

~~~


### receiver_



~~~cpp

mojo::AssociatedReceiver<mojom::PepperHost> receiver_{this};

~~~


### pepper_plugin_instances_



~~~cpp

std::map<int32_t, std::unique_ptr<PepperPluginInstanceHost>>
      pepper_plugin_instances_;

~~~


### field error



~~~cpp

struct HungDetectorContext {
    int32_t plugin_child_id;
    const base::FilePath plugin_path;
  };

~~~


### pepper_hung_detectors_



~~~cpp

mojo::ReceiverSet<mojom::PepperHungDetectorHost, HungDetectorContext>
      pepper_hung_detectors_;

~~~


### render_frame_host

render_frame_host
~~~cpp
RenderFrameHostImpl& render_frame_host() { return *render_frame_host_; }
~~~

### Bind

RenderFrameHostImplPpapiSupport::Bind
~~~cpp
void Bind(mojo::PendingAssociatedReceiver<mojom::PepperHost> receiver);
~~~

### SetVolume

RenderFrameHostImplPpapiSupport::SetVolume
~~~cpp
void SetVolume(int32_t instance_id, double volume);
~~~

### InstanceCreated

RenderFrameHostImplPpapiSupport::InstanceCreated
~~~cpp
void InstanceCreated(
      int32_t instance_id,
      mojo::PendingAssociatedRemote<mojom::PepperPluginInstance> instance,
      mojo::PendingAssociatedReceiver<mojom::PepperPluginInstanceHost> host)
      override;
~~~
 mojom::PepperHost overrides:
### BindHungDetectorHost

RenderFrameHostImplPpapiSupport::BindHungDetectorHost
~~~cpp
void BindHungDetectorHost(
      mojo::PendingReceiver<mojom::PepperHungDetectorHost> hung_host,
      int32_t plugin_child_id,
      const base::FilePath& path) override;
~~~

### GetPluginInfo

RenderFrameHostImplPpapiSupport::GetPluginInfo
~~~cpp
void GetPluginInfo(const GURL& url,
                     const std::string& mime_type,
                     GetPluginInfoCallback callback) override;
~~~

### DidCreateInProcessInstance

RenderFrameHostImplPpapiSupport::DidCreateInProcessInstance
~~~cpp
void DidCreateInProcessInstance(int32_t instance,
                                  int32_t render_frame_id,
                                  const GURL& document_url,
                                  const GURL& plugin_url) override;
~~~

### DidDeleteInProcessInstance

RenderFrameHostImplPpapiSupport::DidDeleteInProcessInstance
~~~cpp
void DidDeleteInProcessInstance(int32_t instance) override;
~~~

### DidCreateOutOfProcessPepperInstance

RenderFrameHostImplPpapiSupport::DidCreateOutOfProcessPepperInstance
~~~cpp
void DidCreateOutOfProcessPepperInstance(
      int32_t plugin_child_id,
      int32_t pp_instance,
      bool is_external,
      int32_t render_frame_id,
      const GURL& document_url,
      const GURL& plugin_url,
      bool is_priviledged_context,
      DidCreateOutOfProcessPepperInstanceCallback callback) override;
~~~

### DidDeleteOutOfProcessPepperInstance

RenderFrameHostImplPpapiSupport::DidDeleteOutOfProcessPepperInstance
~~~cpp
void DidDeleteOutOfProcessPepperInstance(int32_t plugin_child_id,
                                           int32_t pp_instance,
                                           bool is_external) override;
~~~

### OpenChannelToPepperPlugin

RenderFrameHostImplPpapiSupport::OpenChannelToPepperPlugin
~~~cpp
void OpenChannelToPepperPlugin(
      const url::Origin& embedder_origin,
      const base::FilePath& path,
      const absl::optional<url::Origin>& origin_lock,
      OpenChannelToPepperPluginCallback callback) override;
~~~

### PluginHung

RenderFrameHostImplPpapiSupport::PluginHung
~~~cpp
void PluginHung(bool is_hung) override;
~~~
 mojom::PepperHungDetectorHost overrides:
### InstanceClosed

RenderFrameHostImplPpapiSupport::InstanceClosed
~~~cpp
void InstanceClosed(int32_t instance_id);
~~~

### GetProcess

RenderFrameHostImplPpapiSupport::GetProcess
~~~cpp
RenderProcessHostImpl* GetProcess();
~~~

### render_frame_host_



~~~cpp

const raw_ref<RenderFrameHostImpl> render_frame_host_;

~~~


### receiver_



~~~cpp

mojo::AssociatedReceiver<mojom::PepperHost> receiver_{this};

~~~


### pepper_plugin_instances_



~~~cpp

std::map<int32_t, std::unique_ptr<PepperPluginInstanceHost>>
      pepper_plugin_instances_;

~~~


### field error



~~~cpp

struct HungDetectorContext {
    int32_t plugin_child_id;
    const base::FilePath plugin_path;
  };

~~~


### pepper_hung_detectors_



~~~cpp

mojo::ReceiverSet<mojom::PepperHungDetectorHost, HungDetectorContext>
      pepper_hung_detectors_;

~~~

