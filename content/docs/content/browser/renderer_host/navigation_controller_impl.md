### NavigationControllerImpl

NavigationControllerImpl
~~~cpp
NavigationControllerImpl(const NavigationControllerImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
NavigationControllerImpl& operator=(const NavigationControllerImpl&) = delete;
~~~

### ~NavigationControllerImpl

~NavigationControllerImpl
~~~cpp
~NavigationControllerImpl() override
~~~

### DeprecatedGetWebContents

DeprecatedGetWebContents
~~~cpp
WebContents* DeprecatedGetWebContents() override;
~~~
 NavigationController implementation:
### GetBrowserContext

GetBrowserContext
~~~cpp
BrowserContext* GetBrowserContext() override;
~~~

### Restore

Restore
~~~cpp
void Restore(int selected_navigation,
               RestoreType type,
               std::vector<std::unique_ptr<NavigationEntry>>* entries) override;
~~~

### GetActiveEntry

GetActiveEntry
~~~cpp
NavigationEntryImpl* GetActiveEntry() override;
~~~

### GetVisibleEntry

GetVisibleEntry
~~~cpp
NavigationEntryImpl* GetVisibleEntry() override;
~~~

### GetCurrentEntryIndex

GetCurrentEntryIndex
~~~cpp
int GetCurrentEntryIndex() override;
~~~

### GetLastCommittedEntry

GetLastCommittedEntry
~~~cpp
NavigationEntryImpl* GetLastCommittedEntry() override;
~~~

### GetLastCommittedEntryIndex

GetLastCommittedEntryIndex
~~~cpp
int GetLastCommittedEntryIndex() override;
~~~

### CanViewSource

CanViewSource
~~~cpp
bool CanViewSource() override;
~~~

### GetEntryCount

GetEntryCount
~~~cpp
int GetEntryCount() override;
~~~

### GetEntryAtIndex

GetEntryAtIndex
~~~cpp
NavigationEntryImpl* GetEntryAtIndex(int index) override;
~~~

### GetEntryAtOffset

GetEntryAtOffset
~~~cpp
NavigationEntryImpl* GetEntryAtOffset(int offset) override;
~~~

### DiscardNonCommittedEntries

DiscardNonCommittedEntries
~~~cpp
void DiscardNonCommittedEntries() override;
~~~

### GetPendingEntry

GetPendingEntry
~~~cpp
NavigationEntryImpl* GetPendingEntry() override;
~~~

### GetPendingEntryIndex

GetPendingEntryIndex
~~~cpp
int GetPendingEntryIndex() override;
~~~

### LoadURL

LoadURL
~~~cpp
base::WeakPtr<NavigationHandle> LoadURL(
      const GURL& url,
      const Referrer& referrer,
      ui::PageTransition type,
      const std::string& extra_headers) override;
~~~

### LoadURLWithParams

LoadURLWithParams
~~~cpp
base::WeakPtr<NavigationHandle> LoadURLWithParams(
      const LoadURLParams& params) override;
~~~

### LoadIfNecessary

LoadIfNecessary
~~~cpp
void LoadIfNecessary() override;
~~~

### LoadPostCommitErrorPage

LoadPostCommitErrorPage
~~~cpp
base::WeakPtr<NavigationHandle> LoadPostCommitErrorPage(
      RenderFrameHost* render_frame_host,
      const GURL& url,
      const std::string& error_page_html,
      net::Error error) override;
~~~

### CanGoBack

CanGoBack
~~~cpp
bool CanGoBack() override;
~~~

### CanGoForward

CanGoForward
~~~cpp
bool CanGoForward() override;
~~~

### CanGoToOffset

CanGoToOffset
~~~cpp
bool CanGoToOffset(int offset) override;
~~~

### GoBack

GoBack
~~~cpp
void GoBack() override;
~~~

### GoForward

GoForward
~~~cpp
void GoForward() override;
~~~

### GoToIndex

GoToIndex
~~~cpp
void GoToIndex(int index) override;
~~~

### GoToOffset

GoToOffset
~~~cpp
void GoToOffset(int offset) override;
~~~

### RemoveEntryAtIndex

RemoveEntryAtIndex
~~~cpp
bool RemoveEntryAtIndex(int index) override;
~~~

### PruneForwardEntries

PruneForwardEntries
~~~cpp
void PruneForwardEntries() override;
~~~

### GetSessionStorageNamespaceMap

GetSessionStorageNamespaceMap
~~~cpp
const SessionStorageNamespaceMap& GetSessionStorageNamespaceMap() override;
~~~

### GetDefaultSessionStorageNamespace

GetDefaultSessionStorageNamespace
~~~cpp
SessionStorageNamespace* GetDefaultSessionStorageNamespace() override;
~~~

### NeedsReload

NeedsReload
~~~cpp
bool NeedsReload() override;
~~~

### SetNeedsReload

SetNeedsReload
~~~cpp
void SetNeedsReload() override;
~~~

### CancelPendingReload

CancelPendingReload
~~~cpp
void CancelPendingReload() override;
~~~

### ContinuePendingReload

ContinuePendingReload
~~~cpp
void ContinuePendingReload() override;
~~~

### IsInitialNavigation

IsInitialNavigation
~~~cpp
bool IsInitialNavigation() override;
~~~

### IsInitialBlankNavigation

IsInitialBlankNavigation
~~~cpp
bool IsInitialBlankNavigation() override;
~~~

### Reload

Reload
~~~cpp
void Reload(ReloadType reload_type, bool check_for_repost) override;
~~~

### NotifyEntryChanged

NotifyEntryChanged
~~~cpp
void NotifyEntryChanged(NavigationEntry* entry) override;
~~~

### CopyStateFrom

CopyStateFrom
~~~cpp
void CopyStateFrom(NavigationController* source, bool needs_reload) override;
~~~

### CopyStateFromAndPrune

CopyStateFromAndPrune
~~~cpp
void CopyStateFromAndPrune(NavigationController* source,
                             bool replace_entry) override;
~~~

### CanPruneAllButLastCommitted

CanPruneAllButLastCommitted
~~~cpp
bool CanPruneAllButLastCommitted() override;
~~~

### PruneAllButLastCommitted

PruneAllButLastCommitted
~~~cpp
void PruneAllButLastCommitted() override;
~~~

### DeleteNavigationEntries

DeleteNavigationEntries
~~~cpp
void DeleteNavigationEntries(
      const DeletionPredicate& deletionPredicate) override;
~~~

### IsEntryMarkedToBeSkipped

IsEntryMarkedToBeSkipped
~~~cpp
bool IsEntryMarkedToBeSkipped(int index) override;
~~~

### GetBackForwardCache

GetBackForwardCache
~~~cpp
BackForwardCacheImpl& GetBackForwardCache() override;
~~~

### DiscardNonCommittedEntriesWithCommitDetails

DiscardNonCommittedEntriesWithCommitDetails
~~~cpp
void DiscardNonCommittedEntriesWithCommitDetails(
      LoadCommittedDetails* commit_details);
~~~
 Discards the pending entry if any. If this is caused by a navigation
 committing a new entry, `commit_details` will contain the committed
 navigation's details.

### CreateInitialEntry

CreateInitialEntry
~~~cpp
void CreateInitialEntry();
~~~
 Creates the initial NavigationEntry for the NavigationController when its
 FrameTree is being initialized. See NavigationEntry::IsInitialEntry() on
 what this means.

### StartHistoryNavigationInNewSubframe

StartHistoryNavigationInNewSubframe
~~~cpp
bool StartHistoryNavigationInNewSubframe(
      RenderFrameHostImpl* render_frame_host,
      mojo::PendingAssociatedRemote<mojom::NavigationClient>*
          navigation_client);
~~~
 Starts a navigation in a newly created subframe as part of a history
 navigation. Returns true if the history navigation could start, false
 otherwise.  If this returns false, the caller should do a regular
 navigation to the default src URL for the frame instead.

### ReloadFrame

ReloadFrame
~~~cpp
bool ReloadFrame(FrameTreeNode* frame_tree_node);
~~~
 Reloads the |frame_tree_node| and returns true. In some rare cases, there
 is no history related to the frame, nothing happens and this returns false.

### GoToOffsetFromRenderer

GoToOffsetFromRenderer
~~~cpp
void GoToOffsetFromRenderer(
      int offset,
      RenderFrameHostImpl* initiator_rfh,
      absl::optional<blink::scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id);
~~~
 Navigates to the specified offset from the "current entry" and marks the
 navigations as initiated by the renderer.

 |initiator_rfh| is the frame that requested the navigation.

 |soft_navigation_heuristics_task_id| is the task in the renderer that
 initiated this call (if any).

### CanGoToOffsetWithSkipping

CanGoToOffsetWithSkipping
~~~cpp
bool CanGoToOffsetWithSkipping(int offset);
~~~
 The difference between (Can)GoToOffsetWithSkipping and
 (Can)GoToOffset/(Can)GoToOffsetInSandboxedFrame is that this respects the
 history manipulation intervention and will exclude skippable entries.

 These should only be used for browser-initiated navigaitons.

### GoToOffsetWithSkipping

GoToOffsetWithSkipping
~~~cpp
void GoToOffsetWithSkipping(int offset);
~~~

### NavigateFromFrameProxy

NavigateFromFrameProxy
~~~cpp
void NavigateFromFrameProxy(
      RenderFrameHostImpl* render_frame_host,
      const GURL& url,
      const blink::LocalFrameToken* initiator_frame_token,
      int initiator_process_id,
      const absl::optional<url::Origin>& initiator_origin,
      const absl::optional<GURL>& initiator_base_url,
      bool is_renderer_initiated,
      SiteInstance* source_site_instance,
      const Referrer& referrer,
      ui::PageTransition page_transition,
      bool should_replace_current_entry,
      blink::NavigationDownloadPolicy download_policy,
      const std::string& method,
      scoped_refptr<network::ResourceRequestBody> post_body,
      const std::string& extra_headers,
      network::mojom::SourceLocationPtr source_location,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      bool is_form_submission,
      const absl::optional<blink::Impression>& impression,
      blink::mojom::NavigationInitiatorActivationAndAdStatus
          initiator_activation_and_ad_status,
      base::TimeTicks navigation_start_time,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      bool is_unfenced_top_navigation = false,
      bool force_new_browsing_instance = false,
      bool is_container_initiated = false,
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt);
~~~
 Called when a document requests a navigation through a
 RenderFrameProxyHost.

### NavigateToNavigationApiKey

NavigateToNavigationApiKey
~~~cpp
void NavigateToNavigationApiKey(
      RenderFrameHostImpl* initiator_rfh,
      absl::optional<blink::scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id,
      const std::string& key);
~~~
 Navigates to the history entry associated with the given navigation API
 |key|. Searches |entries_| for a FrameNavigationEntry associated with
 |initiator_rfh|'s FrameTreeNode that has |key| as its navigation API key.

 Searches back from the current index, then forward, so if there are
 multiple entries with the same key, the nearest to current should be
 selected. Stops searching in the current direction if it finds a
 NavigationEntry without a FrameNavigationEntry for |initiator_rfh|'s
 FrameTreeNode, or if the FrameNavigationEntry doesn't match origin or site
 instance.


 If no matching entry is found, the navigation is dropped. The renderer
 should only send the navigation to the browser if it believes the entry is
 in |entries_|, but it might be wrong (if the entry was dropped from
 |entries_|, or due to a race condition) or compromised.

 If a matching entry is found, navigate to that entry and proceed like any
 other history navigation.

 |soft_navigation_heuristics_task_id|: The task in the renderer that
 initiated this call (if any).

### IsUnmodifiedBlankTab

IsUnmodifiedBlankTab
~~~cpp
bool IsUnmodifiedBlankTab();
~~~
 Whether this is the initial navigation in an unmodified new tab.  In this
 case, we know there is no content displayed in the page.

### GetSessionStorageNamespace

GetSessionStorageNamespace
~~~cpp
SessionStorageNamespace* GetSessionStorageNamespace(
      const StoragePartitionConfig& partition_config);
~~~
 The session storage namespace that all child `blink::WebView`s associated
 with `partition_config` should use.

### GetIndexOfEntry

GetIndexOfEntry
~~~cpp
int GetIndexOfEntry(const NavigationEntryImpl* entry) const;
~~~
 Returns the index of the specified entry, or -1 if entry is not contained
 in this NavigationController.

### GetEntryIndexWithUniqueID

GetEntryIndexWithUniqueID
~~~cpp
int GetEntryIndexWithUniqueID(int nav_entry_id) const;
~~~
 Return the index of the entry with the given unique id, or -1 if not found.

### GetIndexForGoBack

GetIndexForGoBack
~~~cpp
absl::optional<int> GetIndexForGoBack();
~~~
 Returns the index that would be used by `GoBack`. This respects skippable
 entries. Returns nullopt if no unskippable back entry exists.

### GetIndexForGoForward

GetIndexForGoForward
~~~cpp
absl::optional<int> GetIndexForGoForward();
~~~
 Returns the index that would be used by `GoForward`. This respects
 skippable entries. Returns nullopt if no forward entry exists.

### GetEntryWithUniqueID

GetEntryWithUniqueID
~~~cpp
NavigationEntryImpl* GetEntryWithUniqueID(int nav_entry_id) const;
~~~
 Return the entry with the given unique id, or null if not found.

### GetEntryWithUniqueIDIncludingPending

GetEntryWithUniqueIDIncludingPending
~~~cpp
NavigationEntryImpl* GetEntryWithUniqueIDIncludingPending(
      int nav_entry_id) const;
~~~
 Same as above method, but also includes the pending entry in the search
 space.

### delegate

delegate
~~~cpp
NavigationControllerDelegate* delegate() const { return delegate_; }
~~~

### SetNeedsReload

SetNeedsReload
~~~cpp
void SetNeedsReload(NeedsReloadType type);
~~~
 Request a reload to happen when activated.  Same as the public
 SetNeedsReload(), but takes in a |type| which specifies why the reload is
 being requested.

### RegisterExistingOriginAsHavingDefaultIsolation

RegisterExistingOriginAsHavingDefaultIsolation
~~~cpp
void RegisterExistingOriginAsHavingDefaultIsolation(
      const url::Origin& origin);
~~~
###  For use by WebContentsImpl 
------------------------------------------------
 Visit all FrameNavigationEntries as well as all frame trees and register
 any instances of |origin| as having the default isolation state with their
 respective BrowsingInstances. This is important when |origin| is seen with
 an OriginAgentCluster header, so that we only accept such requests in
 BrowsingInstances that haven't seen it before.

### SetPendingEntry

SetPendingEntry
~~~cpp
void SetPendingEntry(std::unique_ptr<NavigationEntryImpl> entry);
~~~
 Allow renderer-initiated navigations to create a pending entry when the
 provisional load starts.

### RendererDidNavigate

RendererDidNavigate
~~~cpp
bool RendererDidNavigate(RenderFrameHostImpl* rfh,
                           const mojom::DidCommitProvisionalLoadParams& params,
                           LoadCommittedDetails* details,
                           bool is_same_document_navigation,
                           bool was_on_initial_empty_document,
                           bool previous_document_was_activated,
                           NavigationRequest* navigation_request);
~~~
 Handles updating the navigation state after the renderer has navigated.

 This is used by the WebContentsImpl.


 If a new entry is created, it will return true and will have filled the
 given details structure and broadcast the NOTIFY_NAV_ENTRY_COMMITTED
 notification. The caller can then use the details without worrying about
 listening for the notification.


 In the case that nothing has changed, the details structure is undefined
 and it will return false.


 |was_on_initial_empty_document| indicates whether the document being
 navigated away from was an initial empty document.


 |previous_document_was_activated| is true if the previous document had user
 interaction. This is used for a new renderer-initiated navigation to decide
 if the page that initiated the navigation should be skipped on
 back/forward button.

### SetActive

SetActive
~~~cpp
void SetActive(bool is_active);
~~~
 Notifies us that we just became active. This is used by the WebContentsImpl
 so that we know to load URLs that were pending as "lazy" loads.

### SetSessionStorageNamespace

SetSessionStorageNamespace
~~~cpp
void SetSessionStorageNamespace(
      const StoragePartitionConfig& partition_config,
      SessionStorageNamespace* session_storage_namespace);
~~~
 Sets the SessionStorageNamespace for the given |partition_config|. This is
 used during initialization of a new NavigationController to allow
 pre-population of the SessionStorageNamespace objects. Session restore,
 prerendering, and the implementation of window.open() are the primary users
 of this API.


 Calling this function when a SessionStorageNamespace has already been
 associated with a |partition_id| will CHECK() fail.

### frame_tree

frame_tree
~~~cpp
FrameTree& frame_tree() { return *frame_tree_; }
~~~
###  Random data 
---------------------------------------------------------------
### ssl_manager

ssl_manager
~~~cpp
SSLManager* ssl_manager() { return &ssl_manager_; }
~~~

### set_max_entry_count_for_testing

set_max_entry_count_for_testing
~~~cpp
static void set_max_entry_count_for_testing(size_t max_entry_count) {
    max_entry_count_for_testing_ = max_entry_count;
  }
~~~
 Maximum number of entries before we start removing entries from the front.

### max_entry_count

max_entry_count
~~~cpp
static size_t max_entry_count();
~~~

### SetGetTimestampCallbackForTest

SetGetTimestampCallbackForTest
~~~cpp
void SetGetTimestampCallbackForTest(
      const base::RepeatingCallback<base::Time()>& get_timestamp_callback);
~~~

### DiscardPendingEntry

DiscardPendingEntry
~~~cpp
void DiscardPendingEntry(bool was_failure);
~~~
 Discards only the pending entry. |was_failure| should be set if the pending
 entry is being discarded because it failed to load.

### SetPendingNavigationSSLError

SetPendingNavigationSSLError
~~~cpp
void SetPendingNavigationSSLError(bool error);
~~~
 Sets a flag on the pending NavigationEntryImpl instance if any that the
 navigation failed due to an SSL error.

### ValidateDataURLAsString

ValidateDataURLAsString
~~~cpp
static bool ValidateDataURLAsString(
      const scoped_refptr<const base::RefCountedString>& data_url_as_string);
~~~

### NotifyUserActivation

NotifyUserActivation
~~~cpp
void NotifyUserActivation();
~~~
 Invoked when a user activation occurs within the page, so that relevant
 entries can be updated as needed.

### ReferencePendingEntry

ReferencePendingEntry
~~~cpp
std::unique_ptr<PendingEntryRef> ReferencePendingEntry();
~~~
 Tracks a new association between the current pending entry and a
 NavigationRequest. Callers are responsible for only calling this for
 requests corresponding to the current pending entry.

### DidAccessInitialMainDocument

DidAccessInitialMainDocument
~~~cpp
void DidAccessInitialMainDocument();
~~~
 Another page accessed the initial empty main document, which means it
 is no longer safe to display a pending URL without risking a URL spoof.

### UpdateStateForFrame

UpdateStateForFrame
~~~cpp
void UpdateStateForFrame(RenderFrameHostImpl* render_frame_host,
                           const blink::PageState& page_state);
~~~
 The state for the page changed and should be updated in session history.

### CreateNavigationEntry

CreateNavigationEntry
~~~cpp
static std::unique_ptr<NavigationEntryImpl> CreateNavigationEntry(
      const GURL& url,
      Referrer referrer,
      absl::optional<url::Origin> initiator_origin,
      absl::optional<GURL> initiator_base_url,
      SiteInstance* source_site_instance,
      ui::PageTransition transition,
      bool is_renderer_initiated,
      const std::string& extra_headers,
      BrowserContext* browser_context,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory,
      bool rewrite_virtual_urls);
~~~
 Like NavigationController::CreateNavigationEntry, but takes an extra
 argument, |source_site_instance|.

 `rewrite_virtual_urls` is true when it needs to rewrite virtual urls
 (e.g., for outermost frames).

### GetNavigationApiHistoryEntryVectors

GetNavigationApiHistoryEntryVectors
~~~cpp
blink::mojom::NavigationApiHistoryEntryArraysPtr
  GetNavigationApiHistoryEntryVectors(FrameTreeNode* node,
                                      NavigationRequest* request);
~~~
 Called just before sending the commit to the renderer, or when restoring
 from back/forward cache. Walks the session history entries for the relevant
 FrameTreeNode, forward and backward from the pending entry. All contiguous
 and same-origin FrameNavigationEntries are serialized and returned.

 |request| may be nullptr when getting entries for an iframe that is being
 restored for back/forward cache (in that case, the iframe itself is not
 navigated, so there is no NavigationRequest).

### ShouldProtectUrlInNavigationApi

ShouldProtectUrlInNavigationApi
~~~cpp
static bool ShouldProtectUrlInNavigationApi(
      network::mojom::ReferrerPolicy referrer_policy);
~~~
 The window.navigation API exposes the urls of some non-current same-origin
 FrameNavigationEntries to the renderer. This helper checks whether the
 given ReferrerPolicy makes an attempt to hide a page's URL (e.g., in
 referrer headers) and thus whether the URL should be hidden from navigation
 API history entries as well.

### has_post_commit_error_entry

has_post_commit_error_entry
~~~cpp
bool has_post_commit_error_entry() const {
    return entry_replaced_by_post_commit_error_ != nullptr;
  }
~~~
 Returns whether the last NavigationEntry encountered a post-commit error.

### in_navigate_to_pending_entry

in_navigate_to_pending_entry
~~~cpp
bool in_navigate_to_pending_entry() const {
    return in_navigate_to_pending_entry_;
  }
~~~
 Whether the current call stack includes NavigateToPendingEntry, to avoid
 re-entrant calls to NavigateToPendingEntry.

 TODO(https://crbug.com/1327907): Don't expose this once we figure out the
 root cause for the navigation re-entrancy case in the linked bug.

### ShouldMaintainTrivialSessionHistory

ShouldMaintainTrivialSessionHistory
~~~cpp
bool ShouldMaintainTrivialSessionHistory(
      const FrameTreeNode* frame_tree_node) const;
~~~
 Whether to maintain a session history with just one entry.


 This returns true for a prerendering page and for fenced frames.

 `frame_tree_node` is checked to see if it belongs to a frame tree for
 prerendering or for a fenced frame.

 Explainer:
 https://github.com/jeremyroman/alternate-loading-modes/blob/main/browsing-context.md#session-history)

 TODO(crbug.com/914108): Consider portals here as well.

### DidChangeReferrerPolicy

DidChangeReferrerPolicy
~~~cpp
void DidChangeReferrerPolicy(FrameTreeNode* node,
                               network::mojom::ReferrerPolicy referrer_policy);
~~~
 Called when the referrer policy changes. It updates whether to protect the
 url in the navigation API.

### GoToIndex

GoToIndex
~~~cpp
void GoToIndex(int index,
                 RenderFrameHostImpl* initiator_rfh,
                 absl::optional<blink::scheduler::TaskAttributionId>
                     soft_navigation_heuristics_task_id,
                 const std::string* navigation_api_key);
~~~
 Navigates in session history to the given index.

 |initiator_rfh| is nullptr for browser-initiated navigations.

 |soft_navigation_heuristics_task_id|: The task in the renderer that
 initiated this call (if any).

 If this navigation originated from the navigation API, |navigation_api_key|
 will be set and indicate the navigation api key that |initiator_rfh|
 asked to be navigated to.

### NavigateToExistingPendingEntry

NavigateToExistingPendingEntry
~~~cpp
void NavigateToExistingPendingEntry(
      ReloadType reload_type,
      RenderFrameHostImpl* initiator_rfh,
      absl::optional<blink::scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id,
      const std::string* navigation_api_key);
~~~
 Starts a navigation to an already existing pending NavigationEntry.

 |initiator_rfh| is nullptr for browser-initiated navigations.

 If this navigation originated from the navigation API, |navigation_api_key|
 will be set and indicate the navigation api key that |initiator_rfh|
 asked to be navigated to.

 |soft_navigation_heuristics_task_id|: The task in the renderer that
 initiated this call (if any).

### DetermineActionForHistoryNavigation

DetermineActionForHistoryNavigation
~~~cpp
HistoryNavigationAction DetermineActionForHistoryNavigation(
      FrameTreeNode* frame,
      ReloadType reload_type);
~~~
 Helper function used by FindFramesToNavigate to determine the appropriate
 action to take for a particular frame while navigating to
 |pending_entry_|.

### FindFramesToNavigate

FindFramesToNavigate
~~~cpp
void FindFramesToNavigate(
      FrameTreeNode* frame,
      ReloadType reload_type,
      bool is_browser_initiated,
      absl::optional<blink::scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id,
      std::vector<std::unique_ptr<NavigationRequest>>* same_document_loads,
      std::vector<std::unique_ptr<NavigationRequest>>*
          different_document_loads);
~~~
 Recursively identifies which frames need to be navigated for a navigation
 to |pending_entry_|, starting at |frame| and exploring its children.

 |same_document_loads| and |different_document_loads| will be filled with
 the NavigationRequests needed to navigate to |pending_entry_|.

 |soft_navigation_heuristics_task_id|: The task in the renderer that
 initiated this call (if any).

### NavigateWithoutEntry

NavigateWithoutEntry
~~~cpp
base::WeakPtr<NavigationHandle> NavigateWithoutEntry(
      const LoadURLParams& load_params);
~~~
 Starts a new navigation based on |load_params|, that doesn't correspond to
 an existing NavigationEntry.

### HandleRendererDebugURL

HandleRendererDebugURL
~~~cpp
void HandleRendererDebugURL(FrameTreeNode* frame_tree_node, const GURL& url);
~~~
 Handles a navigation to a renderer-debug URL.

### CreateNavigationEntryFromLoadParams

CreateNavigationEntryFromLoadParams
~~~cpp
std::unique_ptr<NavigationEntryImpl> CreateNavigationEntryFromLoadParams(
      FrameTreeNode* node,
      const LoadURLParams& load_params,
      bool override_user_agent,
      bool should_replace_current_entry,
      bool has_user_gesture);
~~~
 Creates and returns a NavigationEntry based on |load_params| for a
 navigation in |node|.

 |override_user_agent|, |should_replace_current_entry| and
 |has_user_gesture| will override the values from |load_params|. The same
 values should be passed to CreateNavigationRequestFromLoadParams.

### CreateNavigationRequestFromLoadParams

CreateNavigationRequestFromLoadParams
~~~cpp
std::unique_ptr<NavigationRequest> CreateNavigationRequestFromLoadParams(
      FrameTreeNode* node,
      const LoadURLParams& load_params,
      bool override_user_agent,
      bool should_replace_current_entry,
      bool has_user_gesture,
      network::mojom::SourceLocationPtr source_location,
      ReloadType reload_type,
      NavigationEntryImpl* entry,
      FrameNavigationEntry* frame_entry,
      base::TimeTicks navigation_start_time,
      bool is_embedder_initiated_fenced_frame_navigation = false,
      bool is_unfenced_top_navigation = false,
      bool is_container_initiated = false,
      absl::optional<std::u16string> embedder_shared_storage_context =
          absl::nullopt);
~~~
 Creates and returns a NavigationRequest based on |load_params| for a
 new navigation in |node|.

 Will return nullptr if the parameters are invalid and the navigation cannot
 start.

 |override_user_agent|, |should_replace_current_entry| and
 |has_user_gesture| will override the values from |load_params|. The same
 values should be passed to CreateNavigationEntryFromLoadParams.

 TODO(clamy): Remove the dependency on NavigationEntry and
 FrameNavigationEntry.

### CreateNavigationRequestFromEntry

CreateNavigationRequestFromEntry
~~~cpp
std::unique_ptr<NavigationRequest> CreateNavigationRequestFromEntry(
      FrameTreeNode* frame_tree_node,
      NavigationEntryImpl* entry,
      FrameNavigationEntry* frame_entry,
      ReloadType reload_type,
      bool is_same_document_history_load,
      bool is_history_navigation_in_new_child_frame,
      bool is_browser_initiated,
      absl::optional<blink::scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id = absl::nullopt);
~~~
 Creates and returns a NavigationRequest for a navigation to |entry|. Will
 return nullptr if the parameters are invalid and the navigation cannot
 start.

 |soft_navigation_heuristics_task_id|: The task in the renderer that
 initiated this call (if any).

 TODO(clamy): Ensure this is only called for navigations to existing
 NavigationEntries.

### PendingEntryMatchesRequest

PendingEntryMatchesRequest
~~~cpp
bool PendingEntryMatchesRequest(NavigationRequest* request) const;
~~~
 Returns whether there is a pending NavigationEntry whose unique ID matches
 the given NavigationRequest's pending_nav_entry_id.

### ClassifyNavigation

ClassifyNavigation
~~~cpp
NavigationType ClassifyNavigation(
      RenderFrameHostImpl* rfh,
      const mojom::DidCommitProvisionalLoadParams& params,
      NavigationRequest* navigation_request);
~~~
 Classifies the given renderer navigation (see the NavigationType enum).

### RendererDidNavigateToNewEntry

RendererDidNavigateToNewEntry
~~~cpp
void RendererDidNavigateToNewEntry(
      RenderFrameHostImpl* rfh,
      const mojom::DidCommitProvisionalLoadParams& params,
      bool is_same_document,
      bool replace_entry,
      bool previous_document_was_activated,
      NavigationRequest* request,
      LoadCommittedDetails* details);
~~~
 Handlers for the different types of navigation types. They will actually
 handle the navigations corresponding to the different NavClasses above.

 They will NOT broadcast the commit notification, that should be handled by
 the caller.


 RendererDidNavigateAutoSubframe is special, it may not actually change
 anything if some random subframe is loaded. It will return true if anything
 changed, or false if not.


 The NewEntry and NewSubframe functions take in |replace_entry| to pass to
 InsertOrReplaceEntry, in case the newly created NavigationEntry is meant to
 replace the current one (e.g., for location.replace or successful loads
 after net errors), in contrast to updating a NavigationEntry in place
 (e.g., for history.replaceState).

### RendererDidNavigateToExistingEntry

RendererDidNavigateToExistingEntry
~~~cpp
void RendererDidNavigateToExistingEntry(
      RenderFrameHostImpl* rfh,
      const mojom::DidCommitProvisionalLoadParams& params,
      bool is_same_document,
      bool was_restored,
      NavigationRequest* request,
      bool keep_pending_entry,
      LoadCommittedDetails* details);
~~~

### RendererDidNavigateNewSubframe

RendererDidNavigateNewSubframe
~~~cpp
void RendererDidNavigateNewSubframe(
      RenderFrameHostImpl* rfh,
      const mojom::DidCommitProvisionalLoadParams& params,
      bool is_same_document,
      bool replace_entry,
      bool previous_document_was_activated,
      NavigationRequest* request,
      LoadCommittedDetails* details);
~~~

### RendererDidNavigateAutoSubframe

RendererDidNavigateAutoSubframe
~~~cpp
bool RendererDidNavigateAutoSubframe(
      RenderFrameHostImpl* rfh,
      const mojom::DidCommitProvisionalLoadParams& params,
      bool is_same_document,
      bool was_on_initial_empty_document,
      NavigationRequest* request,
      LoadCommittedDetails* details);
~~~

### NotifyNavigationEntryCommitted

NotifyNavigationEntryCommitted
~~~cpp
void NotifyNavigationEntryCommitted(LoadCommittedDetails* details);
~~~
 Allows the derived class to issue notifications that a load has been
 committed. This will fill in the active entry to the details structure.

### UpdateVirtualURLToURL

UpdateVirtualURLToURL
~~~cpp
void UpdateVirtualURLToURL(NavigationEntryImpl* entry, const GURL& new_url);
~~~
 Updates the virtual URL of an entry to match a new URL, for cases where
 the real renderer URL is derived from the virtual URL, like view-source:
### FinishRestore

FinishRestore
~~~cpp
void FinishRestore(int selected_index, RestoreType type);
~~~
 Invoked after session/tab restore or cloning a tab. Resets the transition
 type of the entries, updates the max page id and creates the active
 contents.

### InsertOrReplaceEntry

InsertOrReplaceEntry
~~~cpp
void InsertOrReplaceEntry(std::unique_ptr<NavigationEntryImpl> entry,
                            bool replace,
                            bool was_post_commit_error,
                            bool is_in_fenced_frame_tree,
                            LoadCommittedDetails* details);
~~~
 Inserts a new entry or replaces the current entry with a new one, removing
 all entries after it. The new entry will become the active one.

 If |was_post_commit_error_| is set, the last committed entry will be saved,
 the new entry will replace it, and on any navigation away from the new
 entry or on reloads, the old one will replace |entry|.

### RemoveEntryAtIndexInternal

RemoveEntryAtIndexInternal
~~~cpp
void RemoveEntryAtIndexInternal(int index);
~~~
 Removes the entry at |index|, as long as it is not the current entry.

### PruneOldestSkippableEntryIfFull

PruneOldestSkippableEntryIfFull
~~~cpp
void PruneOldestSkippableEntryIfFull();
~~~
 If we have the maximum number of entries, remove the oldest entry that is
 marked to be skipped on back/forward button, in preparation to add another.

 If no entry is skippable, then the oldest entry will be pruned.

### PruneAllButLastCommittedInternal

PruneAllButLastCommittedInternal
~~~cpp
void PruneAllButLastCommittedInternal();
~~~
 Removes all entries except the last committed entry.  If there is a new
 pending navigation it is preserved. In contrast to
 PruneAllButLastCommitted() this does not update the session history of the
 `blink::WebView`.  Callers must ensure that `CanPruneAllButLastCommitted`
 returns true before calling this.

### InsertEntriesFrom

InsertEntriesFrom
~~~cpp
void InsertEntriesFrom(NavigationControllerImpl* source, int max_index);
~~~
 Inserts up to |max_index| entries from |source| into this. This does NOT
 adjust any of the members that reference entries_
 (last_committed_entry_index_ or pending_entry_index_)
### GetIndexForOffset

GetIndexForOffset
~~~cpp
int GetIndexForOffset(int offset);
~~~
 Returns the navigation index that differs from the current entry by the
 specified |offset|.  The index returned is not guaranteed to be valid.

 This does not account for skippable entries or the history manipulation
 intervention.

### SetShouldSkipOnBackForwardUIIfNeeded

SetShouldSkipOnBackForwardUIIfNeeded
~~~cpp
void SetShouldSkipOnBackForwardUIIfNeeded(
      bool replace_entry,
      bool previous_document_was_activated,
      bool is_renderer_initiated,
      ukm::SourceId previous_page_load_ukm_source_id);
~~~
 History Manipulation intervention:
 The previous document that started this navigation needs to be skipped in
 subsequent back/forward UI navigations if it never received any user
 gesture. This is to intervene against pages that manipulate the history
 such that the user is not able to go back to the last site they interacted
 with (crbug.com/907167).

 Note that this function must be called before the new navigation entry is
 inserted in |entries_| to make sure UKM reports the URL of the document
 adding the entry.

### SetSkippableForSameDocumentEntries

SetSkippableForSameDocumentEntries
~~~cpp
void SetSkippableForSameDocumentEntries(int reference_index, bool skippable);
~~~
 This function sets all same document entries with the same value
 of skippable flag. This is to avoid back button abuse by inserting
 multiple history entries and also to help valid cases where a user gesture
 on the document should apply to all same document history entries and none
 should be skipped. All entries belonging to the same document as the entry
 at |reference_index| will get their skippable flag set to |skippable|.

### PendingEntryRefDeleted

PendingEntryRefDeleted
~~~cpp
void PendingEntryRefDeleted(PendingEntryRef* ref);
~~~
 Called when one PendingEntryRef is deleted. When all of the refs for the
 current pending entry have been deleted, this automatically discards the
 pending NavigationEntry.

### ComputePolicyContainerPoliciesForFrameEntry

ComputePolicyContainerPoliciesForFrameEntry
~~~cpp
std::unique_ptr<PolicyContainerPolicies>
  ComputePolicyContainerPoliciesForFrameEntry(RenderFrameHostImpl* rfh,
                                              bool is_same_document,
                                              const GURL& url);
~~~
 Computes the policy container policies to be stored in the
 FrameNavigationEntry by RendererDidNavigate.

### UpdateNavigationEntryDetails

UpdateNavigationEntryDetails
~~~cpp
void UpdateNavigationEntryDetails(
      NavigationEntryImpl* entry,
      RenderFrameHostImpl* rfh,
      const mojom::DidCommitProvisionalLoadParams& params,
      NavigationRequest* request,
      NavigationEntryImpl::UpdatePolicy update_policy,
      bool is_new_entry,
      LoadCommittedDetails* commit_details);
~~~
 Adds details from a committed navigation to `entry` and the
 FrameNavigationEntry corresponding to `rfh`.

### BroadcastHistoryOffsetAndLength

BroadcastHistoryOffsetAndLength
~~~cpp
void BroadcastHistoryOffsetAndLength();
~~~
 Broadcasts this controller's session history offset and length to all
 renderers involved in rendering the current page. The offset is
 GetLastCommittedEntryIndex() and length is GetEntryCount().

### PopulateSingleNavigationApiHistoryEntryVector

PopulateSingleNavigationApiHistoryEntryVector
~~~cpp
std::vector<blink::mojom::NavigationApiHistoryEntryPtr>
  PopulateSingleNavigationApiHistoryEntryVector(
      Direction direction,
      int entry_index,
      const url::Origin& pending_origin,
      FrameTreeNode* node,
      SiteInstance* site_instance,
      int64_t pending_item_sequence_number,
      int64_t pending_document_sequence_number);
~~~
 Used by PopulateNavigationApiHistoryEntryVectors to initialize a single
 vector.

### ShouldNavigateToEntryForNavigationApiKey

ShouldNavigateToEntryForNavigationApiKey
~~~cpp
HistoryNavigationAction ShouldNavigateToEntryForNavigationApiKey(
      FrameNavigationEntry* current_entry,
      FrameNavigationEntry* target_entry,
      const std::string& navigation_api_key);
~~~
 Helper for NavigateToNavigationApiKey(). Ensures that we only navigate to
 |target_entry| if it matches |current_entry|'s origin and site instance, as
 well as having |navigation_api_key| as its key.

## class NavigationControllerImpl
 NavigationControllerImpl is 1:1 with FrameTree. See comments on the base
 class.

### PendingEntryRef

PendingEntryRef
~~~cpp
PendingEntryRef(const PendingEntryRef&) = delete;
~~~

### operator=

NavigationControllerImpl::operator=
~~~cpp
PendingEntryRef& operator=(const PendingEntryRef&) = delete;

    ~PendingEntryRef();
~~~

## class TimeSmoother
 Helper class to smooth out runs of duplicate timestamps while still
 allowing time to jump backwards.

### GetSmoothedTime

TimeSmoother::GetSmoothedTime
~~~cpp
GetSmoothedTime(base::Time t)
~~~

## class ScopedShowRepostDialogForTesting
 The repost dialog is suppressed during testing. However, it should be shown
 in some tests. This allows a test to elect to allow the repost dialog to
 show for a scoped duration.

### ~ScopedShowRepostDialogForTesting

ScopedShowRepostDialogForTesting::~ScopedShowRepostDialogForTesting
~~~cpp
~ScopedShowRepostDialogForTesting()
~~~

### ScopedShowRepostDialogForTesting

ScopedShowRepostDialogForTesting
~~~cpp
ScopedShowRepostDialogForTesting(const ScopedShowRepostDialogForTesting&) =
        delete;
~~~

### operator=

ScopedShowRepostDialogForTesting::operator=
~~~cpp
ScopedShowRepostDialogForTesting& operator=(
        const ScopedShowRepostDialogForTesting&) = delete;
~~~

## class RemovedEntriesTracker
 Records which navigation API keys are associated with live frames.

 On destruction, does a final pass to filter out any keys that are still
 present in |entries_|, then sends the removed navigation API keys to the
 renderer so that the navigation API can fire dispose events for the
 entries associated with those keys.

### RemovedEntriesTracker

RemovedEntriesTracker::RemovedEntriesTracker
~~~cpp
explicit RemovedEntriesTracker(
        base::SafeRef<NavigationControllerImpl> controller);
~~~

### ~RemovedEntriesTracker

RemovedEntriesTracker::~RemovedEntriesTracker
~~~cpp
~RemovedEntriesTracker();
~~~

### PopulateKeySet

RemovedEntriesTracker::PopulateKeySet
~~~cpp
void PopulateKeySet(Direction direction);
~~~
 Walk both directions from the last committed entry to find the navigation
 API keys of any FNEs that could be known by currently live documents.

 These FNEs are contiguous, so the walk can stop for a given frame when it
 reaches an FNE whose API key is no longer known to the current document.

### controller_



~~~cpp

base::SafeRef<NavigationControllerImpl> controller_;

~~~


### names_to_nodes_



~~~cpp

std::map<std::string, FrameTreeNode*> names_to_nodes_;

~~~

 Preprocessed maps used in PopulateKeySet(), mapping frame names
 to their respective FrameTreeNodes, and FrameTreeNode ids to their
 current document sequences numbers.

### frame_tree_node_id_to_doc_seq_nos_



~~~cpp

std::map<int, int64_t> frame_tree_node_id_to_doc_seq_nos_;

~~~


### frame_tree_node_id_to_keys_



~~~cpp

std::map<int, std::set<std::string>> frame_tree_node_id_to_keys_;

~~~

 The output of PopulateKeySet(), which maps FrameTreeNode ids to the keys
 that frame knows about in the renderer. Used in the destructor.

### PopulateKeySet

RemovedEntriesTracker::PopulateKeySet
~~~cpp
void PopulateKeySet(Direction direction);
~~~
 Walk both directions from the last committed entry to find the navigation
 API keys of any FNEs that could be known by currently live documents.

 These FNEs are contiguous, so the walk can stop for a given frame when it
 reaches an FNE whose API key is no longer known to the current document.

### controller_



~~~cpp

base::SafeRef<NavigationControllerImpl> controller_;

~~~


### names_to_nodes_



~~~cpp

std::map<std::string, FrameTreeNode*> names_to_nodes_;

~~~

 Preprocessed maps used in PopulateKeySet(), mapping frame names
 to their respective FrameTreeNodes, and FrameTreeNode ids to their
 current document sequences numbers.

### frame_tree_node_id_to_doc_seq_nos_



~~~cpp

std::map<int, int64_t> frame_tree_node_id_to_doc_seq_nos_;

~~~


### frame_tree_node_id_to_keys_



~~~cpp

std::map<int, std::set<std::string>> frame_tree_node_id_to_keys_;

~~~

 The output of PopulateKeySet(), which maps FrameTreeNode ids to the keys
 that frame knows about in the renderer. Used in the destructor.
