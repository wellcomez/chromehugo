
## class RenderFrameHostCSPContext : public
 RenderFrameHostCSPContext is a network::CSPContext that reports Content
 Security Policy violations through the mojo connection between a
 RenderFrameHostImpl and its corresponding LocalFrame.

### ReportContentSecurityPolicyViolation

RenderFrameHostCSPContext : public::ReportContentSecurityPolicyViolation
~~~cpp
void ReportContentSecurityPolicyViolation(
      network::mojom::CSPViolationPtr violation_params) override;
~~~
 network::CSPContext
### SanitizeDataForUseInCspViolation

RenderFrameHostCSPContext : public::SanitizeDataForUseInCspViolation
~~~cpp
void SanitizeDataForUseInCspViolation(
      network::mojom::CSPDirectiveName directive,
      GURL* blocked_url,
      network::mojom::SourceLocation* source_location) const override;
~~~
