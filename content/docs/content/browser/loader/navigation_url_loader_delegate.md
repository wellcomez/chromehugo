### NavigationURLLoaderDelegate

NavigationURLLoaderDelegate
~~~cpp
NavigationURLLoaderDelegate(const NavigationURLLoaderDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
NavigationURLLoaderDelegate& operator=(const NavigationURLLoaderDelegate&) =
      delete;
~~~

### OnRequestRedirected

OnRequestRedirected
~~~cpp
virtual void OnRequestRedirected(
      const net::RedirectInfo& redirect_info,
      const net::NetworkAnonymizationKey& network_anonymization_key,
      network::mojom::URLResponseHeadPtr response) = 0;
~~~
 Called when the request is redirected. Call FollowRedirect to continue
 processing the request.


 |network_anonymization_key| is the NetworkAnonymizationKey associated with
 the request that was redirected, not the one that will be used if the
 redirect is followed.

### OnResponseStarted

OnResponseStarted
~~~cpp
virtual void OnResponseStarted(
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      GlobalRequestID request_id,
      bool is_download,
      net::NetworkAnonymizationKey network_anonymization_key,
      absl::optional<SubresourceLoaderParams> subresource_loader_params,
      EarlyHints early_hints) = 0;
~~~
 Called when the request receives its response. No further calls will be
 made to the delegate. The response body can be retrieved by implementing an
 URLLoaderClient and binding the |url_loader_client_endpoints|.

 |navigation_data| is passed to the NavigationHandle.

 |subresource_loader_params| is used in the network service only for passing
 necessary info to create a custom subresource loader in the renderer
 process if the navigated context is controlled by a request interceptor.


 |is_download| is true if the request must be downloaded, if it isn't
 disallowed.


 Invoking this method will delete the URLLoader, so it needs to take all
 arguments by value.

### OnRequestFailed

OnRequestFailed
~~~cpp
virtual void OnRequestFailed(
      const network::URLLoaderCompletionStatus& status) = 0;
~~~
 Called if the request fails before receving a response. Specific
 fields which are used: |status.error_code| holds the error code
 for the failure; |status.extended_error_code| holds details if
 available; |status.exists_in_cache| indicates a stale cache
 entry; |status.ssl_info| is available when |status.error_code| is
 a certificate error.

### CreateNavigationEarlyHintsManagerParams

CreateNavigationEarlyHintsManagerParams
~~~cpp
virtual absl::optional<NavigationEarlyHintsManagerParams>
  CreateNavigationEarlyHintsManagerParams(
      const network::mojom::EarlyHints& early_hints) = 0;
~~~
 Creates parameters to construct NavigationEarlyHintsManager. Returns
 absl::nullopt when this delegate cannot create parameters.

### ~NavigationURLLoaderDelegate

~NavigationURLLoaderDelegate
~~~cpp
virtual ~NavigationURLLoaderDelegate() {}
~~~

## class NavigationURLLoaderDelegate
 The delegate interface to NavigationURLLoader.

### ~EarlyHints

NavigationURLLoaderDelegate::~EarlyHints
~~~cpp
~EarlyHints()
~~~

### operator=

NavigationURLLoaderDelegate::operator=
~~~cpp
EarlyHints& operator=(EarlyHints&& other);
~~~

### EarlyHints

EarlyHints
~~~cpp
EarlyHints(const EarlyHints& other) = delete;
~~~

### operator=

NavigationURLLoaderDelegate::operator=
~~~cpp
EarlyHints& operator=(const EarlyHints& other) = delete;
~~~
