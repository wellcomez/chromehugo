
## class NavigationURLLoader
 The navigation logic's UI thread entry point into the resource loading stack.

 It exposes an interface to control the request prior to receiving the
 response. If the NavigationURLLoader is destroyed before OnResponseStarted is
 called, the request is aborted.

### Create

NavigationURLLoader::Create
~~~cpp
static std::unique_ptr<NavigationURLLoader> Create(
      BrowserContext* browser_context,
      StoragePartition* storage_partition,
      std::unique_ptr<NavigationRequestInfo> request_info,
      std::unique_ptr<NavigationUIData> navigation_ui_data,
      ServiceWorkerMainResourceHandle* service_worker_handle,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache,
      NavigationURLLoaderDelegate* delegate,
      LoaderType loader_type,
      mojo::PendingRemote<network::mojom::CookieAccessObserver> cookie_observer,
      mojo::PendingRemote<network::mojom::TrustTokenAccessObserver>
          trust_token_observer,
      mojo::PendingRemote<network::mojom::URLLoaderNetworkServiceObserver>
          url_loader_network_observer,
      mojo::PendingRemote<network::mojom::DevToolsObserver> devtools_observer,
      network::mojom::URLResponseHeadPtr cached_response_head = nullptr,
      std::vector<std::unique_ptr<NavigationLoaderInterceptor>>
          initial_interceptors = {});
~~~
 Creates a NavigationURLLoader. The caller is responsible for ensuring that
 `delegate` outlives the loader. `request_body` must not be accessed on the
 UI thread after this point.


 If `loader_type` is LoaderType::kNoopForBackForwardCache or
 LoaderType::kNoopoForPrerender, a noop CachedNavigationURLLoader will be
 returned.


 TODO(davidben): When navigation is disentangled from the loader, the
 request parameters should not come in as a navigation-specific
 structure. Information like `has_user_gesture` and
 `should_replace_current_entry` in `request_info->common_params` shouldn't
 be needed at this layer.

### SetFactoryForTesting

NavigationURLLoader::SetFactoryForTesting
~~~cpp
static void SetFactoryForTesting(NavigationURLLoaderFactory* factory);
~~~
 For testing purposes; sets the factory for use in testing. The factory is
 not used for prerendered page activation as it needs to run a specific
 loader to satisfy its unique requirement. See the implementation comment in
 NavigationURLLoader::Create() for details.

 TODO(https://crbug.com/1226442): Update this comment for restoration from
 BackForwardCache when it also starts depending on the requirement.

### NavigationURLLoader

NavigationURLLoader
~~~cpp
NavigationURLLoader(const NavigationURLLoader&) = delete;
~~~

### operator=

NavigationURLLoader::operator=
~~~cpp
NavigationURLLoader& operator=(const NavigationURLLoader&) = delete;
~~~

### ~NavigationURLLoader

~NavigationURLLoader
~~~cpp
virtual ~NavigationURLLoader() {}
~~~

### Start

NavigationURLLoader::Start
~~~cpp
virtual void Start() = 0;
~~~
 Called right after the loader is constructed.

### FollowRedirect

NavigationURLLoader::FollowRedirect
~~~cpp
virtual void FollowRedirect(
      const std::vector<std::string>& removed_headers,
      const net::HttpRequestHeaders& modified_headers,
      const net::HttpRequestHeaders& modified_cors_exempt_headers) = 0;
~~~
 Called in response to OnRequestRedirected to continue processing the
 request.

### SetNavigationTimeout

NavigationURLLoader::SetNavigationTimeout
~~~cpp
virtual bool SetNavigationTimeout(base::TimeDelta timeout) = 0;
~~~
 Sets an overall request timeout for this navigation, which will cause the
 navigation to fail if it expires before the navigation commits. This is
 separate from any //net level timeouts. Returns `true` if the timeout was
 started successfully. Repeated calls will be ignored (they won't reset the
 timeout) and will return `false`.
