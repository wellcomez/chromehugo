
## class PrefetchURLLoaderService
 A URLLoaderFactory that can be passed to a renderer to use for performing
 prefetches. The renderer uses it for prefetch requests including <link
 rel="prefetch">.

### PrefetchURLLoaderService

PrefetchURLLoaderService::PrefetchURLLoaderService
~~~cpp
explicit PrefetchURLLoaderService(BrowserContext* browser_context);
~~~

### ~PrefetchURLLoaderService

PrefetchURLLoaderService::~PrefetchURLLoaderService
~~~cpp
~PrefetchURLLoaderService() override;
~~~

### PrefetchURLLoaderService

PrefetchURLLoaderService
~~~cpp
PrefetchURLLoaderService(const PrefetchURLLoaderService&) = delete;
~~~

### operator=

operator=
~~~cpp
PrefetchURLLoaderService& operator=(const PrefetchURLLoaderService&) = delete;
~~~

### GetFactory

PrefetchURLLoaderService::GetFactory
~~~cpp
void GetFactory(
      mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver,
      int frame_tree_node_id,
      std::unique_ptr<network::PendingSharedURLLoaderFactory> pending_factory,
      base::WeakPtr<RenderFrameHostImpl> render_frame_host,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache);
~~~

### RegisterPrefetchLoaderCallbackForTest

RegisterPrefetchLoaderCallbackForTest
~~~cpp
void RegisterPrefetchLoaderCallbackForTest(
      const base::RepeatingClosure& prefetch_load_callback) {
    prefetch_load_callback_for_testing_ = prefetch_load_callback;
  }
~~~
 Register a callback that is fired right before a prefetch load is started
 by this service.

### SetAcceptLanguages

SetAcceptLanguages
~~~cpp
void SetAcceptLanguages(const std::string& accept_langs) {
    accept_langs_ = accept_langs;
  }
~~~

### field error



~~~cpp

struct BindContext;

~~~


### CreateLoaderAndStart

PrefetchURLLoaderService::CreateLoaderAndStart
~~~cpp
void CreateLoaderAndStart(
      mojo::PendingReceiver<network::mojom::URLLoader> receiver,
      int32_t request_id,
      uint32_t options,
      const network::ResourceRequest& resource_request_in,
      mojo::PendingRemote<network::mojom::URLLoaderClient> client,
      const net::MutableNetworkTrafficAnnotationTag& traffic_annotation)
      override;
~~~
 network::mojom::URLLoaderFactory:
### Clone

PrefetchURLLoaderService::Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver)
      override;
~~~

### EnsureCrossOriginFactory

PrefetchURLLoaderService::EnsureCrossOriginFactory
~~~cpp
void EnsureCrossOriginFactory();
~~~
 This ensures that the BindContext's |cross_origin_factory| member exists
 by setting it to a special URLLoaderFactory created by the current
 context's RenderFrameHost.

### IsValidCrossOriginPrefetch

PrefetchURLLoaderService::IsValidCrossOriginPrefetch
~~~cpp
bool IsValidCrossOriginPrefetch(
      const network::ResourceRequest& resource_request);
~~~

### GenerateRecursivePrefetchToken

PrefetchURLLoaderService::GenerateRecursivePrefetchToken
~~~cpp
base::UnguessableToken GenerateRecursivePrefetchToken(
      base::WeakPtr<BindContext> bind_context,
      const network::ResourceRequest& request);
~~~

### NotifyUpdate

PrefetchURLLoaderService::NotifyUpdate
~~~cpp
void NotifyUpdate(const blink::RendererPreferences& new_prefs) override;
~~~
 blink::mojom::RendererPreferenceWatcher.

### CreateURLLoaderThrottles

PrefetchURLLoaderService::CreateURLLoaderThrottles
~~~cpp
std::vector<std::unique_ptr<blink::URLLoaderThrottle>>
  CreateURLLoaderThrottles(const network::ResourceRequest& request,
                           int frame_tree_node_id);
~~~
 For URLLoaderThrottlesGetter.

### current_bind_context

current_bind_context
~~~cpp
const std::unique_ptr<BindContext>& current_bind_context() const {
    return loader_factory_receivers_.current_context();
  }
~~~

### loader_factory_getter_



~~~cpp

scoped_refptr<URLLoaderFactoryGetter> loader_factory_getter_;

~~~


### browser_context_



~~~cpp

raw_ptr<BrowserContext> browser_context_ = nullptr;

~~~


### loader_factory_receivers_



~~~cpp

mojo::ReceiverSet<network::mojom::URLLoaderFactory,
                    std::unique_ptr<BindContext>>
      loader_factory_receivers_;

~~~


### prefetch_receivers_



~~~cpp

mojo::ReceiverSet<network::mojom::URLLoader,
                    std::unique_ptr<network::mojom::URLLoader>>
      prefetch_receivers_;

~~~


### preference_watcher_receiver_



~~~cpp

mojo::Receiver<blink::mojom::RendererPreferenceWatcher>
      preference_watcher_receiver_{this};

~~~

 Used in the IO thread.

### prefetch_load_callback_for_testing_



~~~cpp

base::RepeatingClosure prefetch_load_callback_for_testing_;

~~~


### accept_langs_



~~~cpp

std::string accept_langs_;

~~~


### PrefetchURLLoaderService

PrefetchURLLoaderService
~~~cpp
PrefetchURLLoaderService(const PrefetchURLLoaderService&) = delete;
~~~

### operator=

operator=
~~~cpp
PrefetchURLLoaderService& operator=(const PrefetchURLLoaderService&) = delete;
~~~

### RegisterPrefetchLoaderCallbackForTest

RegisterPrefetchLoaderCallbackForTest
~~~cpp
void RegisterPrefetchLoaderCallbackForTest(
      const base::RepeatingClosure& prefetch_load_callback) {
    prefetch_load_callback_for_testing_ = prefetch_load_callback;
  }
~~~
 Register a callback that is fired right before a prefetch load is started
 by this service.

### SetAcceptLanguages

SetAcceptLanguages
~~~cpp
void SetAcceptLanguages(const std::string& accept_langs) {
    accept_langs_ = accept_langs;
  }
~~~

### current_bind_context

current_bind_context
~~~cpp
const std::unique_ptr<BindContext>& current_bind_context() const {
    return loader_factory_receivers_.current_context();
  }
~~~

### GetFactory

PrefetchURLLoaderService::GetFactory
~~~cpp
void GetFactory(
      mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver,
      int frame_tree_node_id,
      std::unique_ptr<network::PendingSharedURLLoaderFactory> pending_factory,
      base::WeakPtr<RenderFrameHostImpl> render_frame_host,
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache);
~~~

### field error



~~~cpp

struct BindContext;

~~~


### CreateLoaderAndStart

PrefetchURLLoaderService::CreateLoaderAndStart
~~~cpp
void CreateLoaderAndStart(
      mojo::PendingReceiver<network::mojom::URLLoader> receiver,
      int32_t request_id,
      uint32_t options,
      const network::ResourceRequest& resource_request_in,
      mojo::PendingRemote<network::mojom::URLLoaderClient> client,
      const net::MutableNetworkTrafficAnnotationTag& traffic_annotation)
      override;
~~~
 network::mojom::URLLoaderFactory:
### Clone

PrefetchURLLoaderService::Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver)
      override;
~~~

### EnsureCrossOriginFactory

PrefetchURLLoaderService::EnsureCrossOriginFactory
~~~cpp
void EnsureCrossOriginFactory();
~~~
 This ensures that the BindContext's |cross_origin_factory| member exists
 by setting it to a special URLLoaderFactory created by the current
 context's RenderFrameHost.

### IsValidCrossOriginPrefetch

PrefetchURLLoaderService::IsValidCrossOriginPrefetch
~~~cpp
bool IsValidCrossOriginPrefetch(
      const network::ResourceRequest& resource_request);
~~~

### GenerateRecursivePrefetchToken

PrefetchURLLoaderService::GenerateRecursivePrefetchToken
~~~cpp
base::UnguessableToken GenerateRecursivePrefetchToken(
      base::WeakPtr<BindContext> bind_context,
      const network::ResourceRequest& request);
~~~

### NotifyUpdate

PrefetchURLLoaderService::NotifyUpdate
~~~cpp
void NotifyUpdate(const blink::RendererPreferences& new_prefs) override;
~~~
 blink::mojom::RendererPreferenceWatcher.

### CreateURLLoaderThrottles

PrefetchURLLoaderService::CreateURLLoaderThrottles
~~~cpp
std::vector<std::unique_ptr<blink::URLLoaderThrottle>>
  CreateURLLoaderThrottles(const network::ResourceRequest& request,
                           int frame_tree_node_id);
~~~
 For URLLoaderThrottlesGetter.

### loader_factory_getter_



~~~cpp

scoped_refptr<URLLoaderFactoryGetter> loader_factory_getter_;

~~~


### browser_context_



~~~cpp

raw_ptr<BrowserContext> browser_context_ = nullptr;

~~~


### loader_factory_receivers_



~~~cpp

mojo::ReceiverSet<network::mojom::URLLoaderFactory,
                    std::unique_ptr<BindContext>>
      loader_factory_receivers_;

~~~


### prefetch_receivers_



~~~cpp

mojo::ReceiverSet<network::mojom::URLLoader,
                    std::unique_ptr<network::mojom::URLLoader>>
      prefetch_receivers_;

~~~


### preference_watcher_receiver_



~~~cpp

mojo::Receiver<blink::mojom::RendererPreferenceWatcher>
      preference_watcher_receiver_{this};

~~~

 Used in the IO thread.

### prefetch_load_callback_for_testing_



~~~cpp

base::RepeatingClosure prefetch_load_callback_for_testing_;

~~~


### accept_langs_



~~~cpp

std::string accept_langs_;

~~~

