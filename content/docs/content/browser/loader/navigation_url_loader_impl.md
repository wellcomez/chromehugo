### NavigationURLLoaderImpl

NavigationURLLoaderImpl
~~~cpp
NavigationURLLoaderImpl(const NavigationURLLoaderImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
NavigationURLLoaderImpl& operator=(const NavigationURLLoaderImpl&) = delete;
~~~

### ~NavigationURLLoaderImpl

~NavigationURLLoaderImpl
~~~cpp
~NavigationURLLoaderImpl() override
~~~

### SetURLLoaderFactoryInterceptorForTesting

SetURLLoaderFactoryInterceptorForTesting
~~~cpp
static void SetURLLoaderFactoryInterceptorForTesting(
      const URLLoaderFactoryInterceptor& interceptor);
~~~

### CreateURLLoaderFactoryWithHeaderClient

CreateURLLoaderFactoryWithHeaderClient
~~~cpp
static void CreateURLLoaderFactoryWithHeaderClient(
      mojo::PendingRemote<network::mojom::TrustedURLLoaderHeaderClient>
          header_client,
      mojo::PendingReceiver<network::mojom::URLLoaderFactory> factory_receiver,
      StoragePartitionImpl* partition);
~~~
 Creates a URLLoaderFactory for a navigation. The factory uses
 `header_client`. This should have the same settings as the factory from
 the URLLoaderFactoryGetter. Called on the UI thread.

### CreateNetworkLoaderFactory

CreateNetworkLoaderFactory
~~~cpp
static scoped_refptr<network::SharedURLLoaderFactory>
  CreateNetworkLoaderFactory(BrowserContext* browser_context,
                             StoragePartitionImpl* storage_partition,
                             FrameTreeNode* frame_tree_node,
                             const ukm::SourceIdObj& ukm_id,
                             bool* bypass_redirect_checks);
~~~
 Creates a SharedURLLoaderFactory for network-service-bound requests.

### StartImpl

StartImpl
~~~cpp
void StartImpl(
      scoped_refptr<PrefetchedSignedExchangeCache>
          prefetched_signed_exchange_cache,
      mojo::PendingRemote<network::mojom::URLLoaderFactory> factory_for_webui,
      std::string accept_langs);
~~~
 Starts the loader by finalizing loader factories initialization and
 calling Restart().

 This is called only once (while Restart can be called multiple times).

 Sets `started_` true.

### BindNonNetworkURLLoaderFactoryReceiver

BindNonNetworkURLLoaderFactoryReceiver
~~~cpp
void BindNonNetworkURLLoaderFactoryReceiver(
      const GURL& url,
      mojo::PendingReceiver<network::mojom::URLLoaderFactory> factory_receiver);
~~~

### BindAndInterceptNonNetworkURLLoaderFactoryReceiver

BindAndInterceptNonNetworkURLLoaderFactoryReceiver
~~~cpp
void BindAndInterceptNonNetworkURLLoaderFactoryReceiver(
      const GURL& url,
      mojo::PendingReceiver<network::mojom::URLLoaderFactory> factory_receiver);
~~~

### CreateInterceptors

CreateInterceptors
~~~cpp
void CreateInterceptors(scoped_refptr<PrefetchedSignedExchangeCache>
                              prefetched_signed_exchange_cache,
                          const std::string& accept_langs);
~~~

### Restart

Restart
~~~cpp
void Restart();
~~~
 This could be called multiple times to follow a chain of redirects.

 This internally rather recreates another loader than actually following the
 redirects.

### MaybeStartLoader

MaybeStartLoader
~~~cpp
void MaybeStartLoader(
      NavigationLoaderInterceptor* interceptor,
      scoped_refptr<network::SharedURLLoaderFactory> single_request_factory);
~~~
 `interceptor` is non-null if this is called by one of the interceptors
 (via a LoaderCallback).

 `single_request_handler` is the RequestHandler given by the
 `interceptor`, non-null if the interceptor wants to handle the request.

### FallbackToNonInterceptedRequest

FallbackToNonInterceptedRequest
~~~cpp
void FallbackToNonInterceptedRequest(bool reset_subresource_loader_params);
~~~
 This is the `fallback_callback` passed to
 NavigationLoaderInterceptor::MaybeCreateLoader. It allows an interceptor
 to initially elect to handle a request, and later decide to fallback to
 the default behavior. This is needed for service worker network fallback
 and signed exchange (SXG) fallback redirect.

### PrepareForNonInterceptedRequest

PrepareForNonInterceptedRequest
~~~cpp
scoped_refptr<network::SharedURLLoaderFactory>
  PrepareForNonInterceptedRequest();
~~~

### CheckPluginAndContinueOnReceiveResponse

CheckPluginAndContinueOnReceiveResponse
~~~cpp
void CheckPluginAndContinueOnReceiveResponse(
      network::mojom::URLResponseHeadPtr head,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      bool is_download_if_not_handled_by_plugin,
      const std::vector<WebPluginInfo>& plugins);
~~~

### CallOnReceivedResponse

CallOnReceivedResponse
~~~cpp
void CallOnReceivedResponse(
      network::mojom::URLResponseHeadPtr head,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      bool is_download);
~~~

### MaybeCreateLoaderForResponse

MaybeCreateLoaderForResponse
~~~cpp
bool MaybeCreateLoaderForResponse(
      const network::URLLoaderCompletionStatus& status,
      network::mojom::URLResponseHeadPtr* response);
~~~

### CreateURLLoaderThrottles

CreateURLLoaderThrottles
~~~cpp
std::vector<std::unique_ptr<blink::URLLoaderThrottle>>
  CreateURLLoaderThrottles();
~~~

### CreateSignedExchangeRequestHandler

CreateSignedExchangeRequestHandler
~~~cpp
std::unique_ptr<SignedExchangeRequestHandler>
  CreateSignedExchangeRequestHandler(
      const NavigationRequestInfo& request_info,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      std::string accept_langs);
~~~

### ParseHeaders

ParseHeaders
~~~cpp
void ParseHeaders(const GURL& url,
                    network::mojom::URLResponseHead* head,
                    base::OnceClosure continuation);
~~~

### NotifyResponseStarted

NotifyResponseStarted
~~~cpp
void NotifyResponseStarted(
      network::mojom::URLResponseHeadPtr response_head,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      mojo::ScopedDataPipeConsumerHandle response_body,
      const GlobalRequestID& global_request_id,
      bool is_download);
~~~

### NotifyRequestRedirected

NotifyRequestRedirected
~~~cpp
void NotifyRequestRedirected(net::RedirectInfo redirect_info,
                               network::mojom::URLResponseHeadPtr response);
~~~

### NotifyRequestFailed

NotifyRequestFailed
~~~cpp
void NotifyRequestFailed(const network::URLLoaderCompletionStatus& status);
~~~

### OnReceiveEarlyHints

OnReceiveEarlyHints
~~~cpp
void OnReceiveEarlyHints(network::mojom::EarlyHintsPtr early_hints) override;
~~~
 network::mojom::URLLoaderClient implementation:
### OnReceiveResponse

OnReceiveResponse
~~~cpp
void OnReceiveResponse(
      network::mojom::URLResponseHeadPtr head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      absl::optional<mojo_base::BigBuffer> cached_metadata) override;
~~~

### OnReceiveRedirect

OnReceiveRedirect
~~~cpp
void OnReceiveRedirect(const net::RedirectInfo& redirect_info,
                         network::mojom::URLResponseHeadPtr head) override;
~~~

### OnUploadProgress

OnUploadProgress
~~~cpp
void OnUploadProgress(int64_t current_position,
                        int64_t total_size,
                        OnUploadProgressCallback callback) override;
~~~

### OnTransferSizeUpdated

OnTransferSizeUpdated
~~~cpp
void OnTransferSizeUpdated(int32_t transfer_size_diff) override;
~~~

### OnComplete

OnComplete
~~~cpp
void OnComplete(const network::URLLoaderCompletionStatus& status) override;
~~~

### OnAcceptCHFrameReceived

OnAcceptCHFrameReceived
~~~cpp
void OnAcceptCHFrameReceived(
      const url::Origin& origin,
      const std::vector<network::mojom::WebClientHintsType>& accept_ch_frame,
      OnAcceptCHFrameReceivedCallback callback) override;
~~~
 network::mojom::AcceptCHFrameObserver implementation
### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::AcceptCHFrameObserver>
                 listener) override;
~~~

### Start

Start
~~~cpp
void Start() override;
~~~
 NavigationURLLoader implementation:
### FollowRedirect

FollowRedirect
~~~cpp
void FollowRedirect(
      const std::vector<std::string>& removed_headers,
      const net::HttpRequestHeaders& modified_headers,
      const net::HttpRequestHeaders& modified_cors_exempt_headers) override;
~~~

### SetNavigationTimeout

SetNavigationTimeout
~~~cpp
bool SetNavigationTimeout(base::TimeDelta timeout) override;
~~~

### RecordReceivedResponseUkmForOutermostMainFrame

RecordReceivedResponseUkmForOutermostMainFrame
~~~cpp
void RecordReceivedResponseUkmForOutermostMainFrame();
~~~
 Records UKM for the navigation load.

