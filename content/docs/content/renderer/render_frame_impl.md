### CreateFrame

CreateFrame
~~~cpp
static void CreateFrame(
      AgentSchedulingGroup& agent_scheduling_group,
      const blink::LocalFrameToken& token,
      int routing_id,
      mojo::PendingAssociatedReceiver<mojom::Frame> frame_receiver,
      mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker>
          browser_interface_broker,
      mojo::PendingAssociatedRemote<blink::mojom::AssociatedInterfaceProvider>
          associated_interface_provider,
      blink::WebView* web_view,
      const absl::optional<blink::FrameToken>& previous_frame_token,
      const absl::optional<blink::FrameToken>& opener_frame_token,
      const absl::optional<blink::FrameToken>& parent_frame_token,
      const absl::optional<blink::FrameToken>& previous_sibling_frame_token,
      const base::UnguessableToken& devtools_frame_token,
      blink::mojom::TreeScopeType tree_scope_type,
      blink::mojom::FrameReplicationStatePtr replicated_state,
      mojom::CreateFrameWidgetParamsPtr widget_params,
      blink::mojom::FrameOwnerPropertiesPtr frame_owner_properties,
      bool is_on_initial_empty_document,
      const blink::DocumentToken& document_token,
      blink::mojom::PolicyContainerPtr policy_container);
~~~
 Creates a new RenderFrame with |routing_id|. If |previous_frame_token| is
 not provided, it creates the Blink WebLocalFrame and inserts it into
 the frame tree after the frame identified by |previous_sibling_routing_id|,
 or as the first child if |previous_sibling_routing_id| is MSG_ROUTING_NONE.

 Otherwise, the frame is semi-orphaned until it commits, at which point it
 replaces the previous object identified by |previous_frame_token|. The
 previous object can either be a RenderFrame or a RenderFrameProxy.

 The frame's opener is set to the frame identified by |opener_routing_id|.

 The frame is created as a child of the RenderFrame identified by
 |parent_routing_id| or as the top-level frame if
 the latter is MSG_ROUTING_NONE.

 |devtools_frame_token| is passed from the browser and corresponds to the
 owner FrameTreeNode. It can only be used for tagging requests and calls
 for context frame attribution. It should never be passed back to the
 browser as a frame identifier in the control flows calls.

 The |widget_params| is not null if the frame is to be a local root, which
 means it will own a RenderWidget, in which case the |widget_params| hold
 the routing id and initialization properties for the RenderWidget.

 The |web_view| param will only be set when the frame to be created will use
 new WebView, instead of using the previous Frame's WebView. This is only
 possible for provisional main RenderFrames that will do a local main
 RenderFrame swap later on with the frame that has the token
 |previous_frame_token|.


 Note: This is called only when RenderFrame is being created in response
 to IPC message from the browser process. All other frame creation is driven
 through Blink and Create.

### FromRoutingID

FromRoutingID
~~~cpp
static RenderFrameImpl* FromRoutingID(int routing_id);
~~~
 Returns the RenderFrameImpl for the given routing ID.

### FromWebFrame

FromWebFrame
~~~cpp
static RenderFrameImpl* FromWebFrame(blink::WebFrame* web_frame);
~~~
 Just like RenderFrame::FromWebFrame but returns the implementation.

### InstallCreateHook

InstallCreateHook
~~~cpp
static void InstallCreateHook(CreateRenderFrameImplFunction create_frame);
~~~
 Web tests override the creation of RenderFrames in order to inject a
 partial testing fake.

### OverrideFlashEmbedWithHTML

OverrideFlashEmbedWithHTML
~~~cpp
blink::WebURL OverrideFlashEmbedWithHTML(const blink::WebURL& url) override;
~~~
 Overwrites the given URL to use an HTML5 embed if possible.

### RenderFrameImpl

RenderFrameImpl
~~~cpp
RenderFrameImpl(const RenderFrameImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
RenderFrameImpl& operator=(const RenderFrameImpl&) = delete;
~~~

### ~RenderFrameImpl

~RenderFrameImpl
~~~cpp
~RenderFrameImpl() override
~~~

### unique_name

unique_name
~~~cpp
const std::string& unique_name() const { return unique_name_helper_.value(); }
~~~
 Returns the unique name of the RenderFrame.

### GetLocalRootWebFrameWidget

GetLocalRootWebFrameWidget
~~~cpp
blink::WebFrameWidget* GetLocalRootWebFrameWidget();
~~~
 Returns the blink::WebFrameWidget attached to the local root of this
 frame.

### Initialize

Initialize
~~~cpp
virtual void Initialize(blink::WebFrame* parent);
~~~
 This method must be called after the WebLocalFrame backing this RenderFrame
 has been created and added to the frame tree. It creates all objects that
 depend on the frame being at its proper spot.


 Virtual for web tests to inject their own behaviour into the WebLocalFrame.

### DidStartLoading

DidStartLoading
~~~cpp
void DidStartLoading() override;
~~~
 Start/Stop loading notifications.

 TODO(nasko): Those are page-level methods at this time and come from
 WebViewClient. We should move them to be WebLocalFrameClient calls and put
 logic in the browser side to balance starts/stops.

### DidStopLoading

DidStopLoading
~~~cpp
void DidStopLoading() override;
~~~

### GetRenderAccessibilityManager

GetRenderAccessibilityManager
~~~cpp
RenderAccessibilityManager* GetRenderAccessibilityManager() {
    return render_accessibility_manager_.get();
  }
~~~
 Returns the object implementing the RenderAccessibility mojo interface and
 serves as a bridge between RenderFrameImpl and RenderAccessibilityImpl.

### NotifyAccessibilityModeChange

NotifyAccessibilityModeChange
~~~cpp
void NotifyAccessibilityModeChange(ui::AXMode new_mode);
~~~
 Called from RenderAccessibilityManager to let the RenderFrame know when the
 accessibility mode has changed, so that it can notify its observers.

### in_frame_tree

in_frame_tree
~~~cpp
bool in_frame_tree() { return in_frame_tree_; }
~~~
 Whether or not the frame is currently swapped into the frame tree.  If
 this is false, this is a provisional frame which has not committed yet,
 and which will swap with a proxy when it commits.


 TODO(https://crbug.com/578349): Remove this once provisional frames are
 gone, and clean up code that depends on it.

### GetPepperHost

GetPepperHost
~~~cpp
mojom::PepperHost* GetPepperHost();
~~~

### PepperPluginCreated

PepperPluginCreated
~~~cpp
void PepperPluginCreated(RendererPpapiHost* host);
~~~
 Notification that a PPAPI plugin has been created.

### PepperTextInputTypeChanged

PepperTextInputTypeChanged
~~~cpp
void PepperTextInputTypeChanged(PepperPluginInstanceImpl* instance);
~~~
 Informs the render view that a PPAPI plugin has changed text input status.

### PepperCaretPositionChanged

PepperCaretPositionChanged
~~~cpp
void PepperCaretPositionChanged(PepperPluginInstanceImpl* instance);
~~~

### PepperCancelComposition

PepperCancelComposition
~~~cpp
void PepperCancelComposition(PepperPluginInstanceImpl* instance);
~~~
 Cancels current composition.

### PepperSelectionChanged

PepperSelectionChanged
~~~cpp
void PepperSelectionChanged(PepperPluginInstanceImpl* instance);
~~~
 Informs the render view that a PPAPI plugin has changed selection.

### Send

Send
~~~cpp
bool Send(IPC::Message* msg) override;
~~~
 BUILDFLAG(ENABLE_PPAPI)
 IPC::Sender
### OnMessageReceived

OnMessageReceived
~~~cpp
bool OnMessageReceived(const IPC::Message& msg) override;
~~~
 IPC::Listener
### OnAssociatedInterfaceRequest

OnAssociatedInterfaceRequest
~~~cpp
void OnAssociatedInterfaceRequest(
      const std::string& interface_name,
      mojo::ScopedInterfaceEndpointHandle handle) override;
~~~

### GetMainRenderFrame

GetMainRenderFrame
~~~cpp
RenderFrame* GetMainRenderFrame() override;
~~~
 RenderFrame implementation:
### GetRenderAccessibility

GetRenderAccessibility
~~~cpp
RenderAccessibility* GetRenderAccessibility() override;
~~~

### CreateAXTreeSnapshotter

CreateAXTreeSnapshotter
~~~cpp
std::unique_ptr<AXTreeSnapshotter> CreateAXTreeSnapshotter(
      ui::AXMode ax_mode) override;
~~~

### GetRoutingID

GetRoutingID
~~~cpp
int GetRoutingID() override;
~~~

### GetWebFrame

GetWebFrame
~~~cpp
blink::WebLocalFrame* GetWebFrame() override;
~~~

### GetWebFrame

GetWebFrame
~~~cpp
const blink::WebLocalFrame* GetWebFrame() const override;
~~~

### GetWebView

GetWebView
~~~cpp
blink::WebView* GetWebView() override;
~~~

### GetWebView

GetWebView
~~~cpp
const blink::WebView* GetWebView() const override;
~~~

### GetBlinkPreferences

GetBlinkPreferences
~~~cpp
const blink::web_pref::WebPreferences& GetBlinkPreferences() override;
~~~

### ShowVirtualKeyboard

ShowVirtualKeyboard
~~~cpp
void ShowVirtualKeyboard() override;
~~~

### CreatePlugin

CreatePlugin
~~~cpp
blink::WebPlugin* CreatePlugin(const WebPluginInfo& info,
                                 const blink::WebPluginParams& params) override;
~~~

### ExecuteJavaScript

ExecuteJavaScript
~~~cpp
void ExecuteJavaScript(const std::u16string& javascript) override;
~~~

### IsMainFrame

IsMainFrame
~~~cpp
bool IsMainFrame() override;
~~~

### IsInFencedFrameTree

IsInFencedFrameTree
~~~cpp
bool IsInFencedFrameTree() const override;
~~~

### IsHidden

IsHidden
~~~cpp
bool IsHidden() override;
~~~

### BindLocalInterface

BindLocalInterface
~~~cpp
void BindLocalInterface(
      const std::string& interface_name,
      mojo::ScopedMessagePipeHandle interface_pipe) override;
~~~

### GetAssociatedInterfaceRegistry

GetAssociatedInterfaceRegistry
~~~cpp
blink::AssociatedInterfaceRegistry* GetAssociatedInterfaceRegistry() override;
~~~

### GetRemoteAssociatedInterfaces

GetRemoteAssociatedInterfaces
~~~cpp
blink::AssociatedInterfaceProvider* GetRemoteAssociatedInterfaces() override;
~~~

### SetSelectedText

SetSelectedText
~~~cpp
void SetSelectedText(const std::u16string& selection_text,
                       size_t offset,
                       const gfx::Range& range) override;
~~~

### AddMessageToConsole

AddMessageToConsole
~~~cpp
void AddMessageToConsole(blink::mojom::ConsoleMessageLevel level,
                           const std::string& message) override;
~~~

### IsPasting

IsPasting
~~~cpp
bool IsPasting() override;
~~~

### IsBrowserSideNavigationPending

IsBrowserSideNavigationPending
~~~cpp
bool IsBrowserSideNavigationPending() override;
~~~

### LoadHTMLStringForTesting

LoadHTMLStringForTesting
~~~cpp
void LoadHTMLStringForTesting(const std::string& html,
                                const GURL& base_url,
                                const std::string& text_encoding,
                                const GURL& unreachable_url,
                                bool replace_current_item) override;
~~~

### GetTaskRunner

GetTaskRunner
~~~cpp
scoped_refptr<base::SingleThreadTaskRunner> GetTaskRunner(
      blink::TaskType task_type) override;
~~~

### GetEnabledBindings

GetEnabledBindings
~~~cpp
int GetEnabledBindings() override;
~~~

### SetAccessibilityModeForTest

SetAccessibilityModeForTest
~~~cpp
void SetAccessibilityModeForTest(ui::AXMode new_mode) override;
~~~

### GetRenderFrameMediaPlaybackOptions

GetRenderFrameMediaPlaybackOptions
~~~cpp
const RenderFrameMediaPlaybackOptions& GetRenderFrameMediaPlaybackOptions()
      override;
~~~

### SetRenderFrameMediaPlaybackOptions

SetRenderFrameMediaPlaybackOptions
~~~cpp
void SetRenderFrameMediaPlaybackOptions(
      const RenderFrameMediaPlaybackOptions& opts) override;
~~~

### SetAllowsCrossBrowsingInstanceFrameLookup

SetAllowsCrossBrowsingInstanceFrameLookup
~~~cpp
void SetAllowsCrossBrowsingInstanceFrameLookup() override;
~~~

### ElementBoundsInWindow

ElementBoundsInWindow
~~~cpp
gfx::RectF ElementBoundsInWindow(const blink::WebElement& element) override;
~~~

### ConvertViewportToWindow

ConvertViewportToWindow
~~~cpp
void ConvertViewportToWindow(gfx::Rect* rect) override;
~~~

### GetDeviceScaleFactor

GetDeviceScaleFactor
~~~cpp
float GetDeviceScaleFactor() override;
~~~

### GetAgentGroupScheduler

GetAgentGroupScheduler
~~~cpp
blink::scheduler::WebAgentGroupScheduler& GetAgentGroupScheduler() override;
~~~

### AddAutoplayFlags

AddAutoplayFlags
~~~cpp
void AddAutoplayFlags(const url::Origin& origin,
                        const int32_t flags) override;
~~~
 blink::mojom::AutoplayConfigurationClient implementation:
### NotifyUpdateUserGestureCarryoverInfo

NotifyUpdateUserGestureCarryoverInfo
~~~cpp
void NotifyUpdateUserGestureCarryoverInfo() override;
~~~

### NotifyResourceRedirectReceived

NotifyResourceRedirectReceived
~~~cpp
void NotifyResourceRedirectReceived(
      const net::RedirectInfo& redirect_info,
      network::mojom::URLResponseHeadPtr redirect_response) override;
~~~

### NotifyResourceResponseReceived

NotifyResourceResponseReceived
~~~cpp
void NotifyResourceResponseReceived(
      int64_t request_id,
      const url::SchemeHostPort& final_response_url,
      network::mojom::URLResponseHeadPtr head,
      network::mojom::RequestDestination request_destination) override;
~~~

### NotifyResourceTransferSizeUpdated

NotifyResourceTransferSizeUpdated
~~~cpp
void NotifyResourceTransferSizeUpdated(int64_t request_id,
                                         int32_t transfer_size_diff) override;
~~~

### NotifyResourceLoadCompleted

NotifyResourceLoadCompleted
~~~cpp
void NotifyResourceLoadCompleted(
      blink::mojom::ResourceLoadInfoPtr resource_load_info,
      const ::network::URLLoaderCompletionStatus& status) override;
~~~

### NotifyResourceLoadCanceled

NotifyResourceLoadCanceled
~~~cpp
void NotifyResourceLoadCanceled(int64_t request_id) override;
~~~

### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<blink::mojom::ResourceLoadInfoNotifier>
                 pending_resource_load_info_notifier) override;
~~~

### AllowBindings

AllowBindings
~~~cpp
void AllowBindings(int32_t enabled_bindings_flags) override;
~~~
 mojom::FrameBindingsControl implementation:
### EnableMojoJsBindings

EnableMojoJsBindings
~~~cpp
void EnableMojoJsBindings(
      content::mojom::ExtraMojoJsFeaturesPtr features) override;
~~~

### EnableMojoJsBindingsWithBroker

EnableMojoJsBindingsWithBroker
~~~cpp
void EnableMojoJsBindingsWithBroker(
      mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker>) override;
~~~

### BindWebUI

BindWebUI
~~~cpp
void BindWebUI(
      mojo::PendingAssociatedReceiver<mojom::WebUI> Receiver,
      mojo::PendingAssociatedRemote<mojom::WebUIHost> remote) override;
~~~

### CommitNavigation

CommitNavigation
~~~cpp
void CommitNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
          subresource_loader_factories,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      blink::mojom::ControllerServiceWorkerInfoPtr
          controller_service_worker_info,
      blink::mojom::ServiceWorkerContainerInfoForClientPtr container_info,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          prefetch_loader_factory,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          topics_loader_factory,
      const blink::DocumentToken& document_token,
      const base::UnguessableToken& devtools_navigation_token,
      const absl::optional<blink::ParsedPermissionsPolicy>& permissions_policy,
      blink::mojom::PolicyContainerPtr policy_container,
      mojo::PendingRemote<blink::mojom::CodeCacheHost> code_cache_host,
      mojom::CookieManagerInfoPtr cookie_manager_info,
      mojom::StorageInfoPtr storage_info,
      mojom::NavigationClient::CommitNavigationCallback commit_callback);
~~~
 These mirror mojom::NavigationClient, called by NavigationClient.

### CommitFailedNavigation

CommitFailedNavigation
~~~cpp
void CommitFailedNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool has_stale_copy_in_cache,
      int error_code,
      int extended_error_code,
      net::ResolveErrorInfo resolve_error_info,
      const absl::optional<std::string>& error_page_content,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
          subresource_loader_factories,
      const blink::DocumentToken& document_token,
      blink::mojom::PolicyContainerPtr policy_container,
      mojom::AlternativeErrorPageOverrideInfoPtr alternative_error_page_info,
      mojom::NavigationClient::CommitFailedNavigationCallback
          per_navigation_mojo_interface_callback);
~~~

### SerializeAsMHTML

SerializeAsMHTML
~~~cpp
void SerializeAsMHTML(const mojom::SerializeAsMHTMLParamsPtr params,
                        SerializeAsMHTMLCallback callback) override;
~~~
 mojom::MhtmlFileWriter implementation:
### BindToFrame

BindToFrame
~~~cpp
void BindToFrame(blink::WebNavigationControl* frame) override;
~~~
 blink::WebLocalFrameClient implementation:
### CreatePlugin

CreatePlugin
~~~cpp
blink::WebPlugin* CreatePlugin(const blink::WebPluginParams& params) override;
~~~

### CreateMediaPlayer

CreateMediaPlayer
~~~cpp
blink::WebMediaPlayer* CreateMediaPlayer(
      const blink::WebMediaPlayerSource& source,
      blink::WebMediaPlayerClient* client,
      blink::MediaInspectorContext* inspector_context,
      blink::WebMediaPlayerEncryptedMediaClient* encrypted_client,
      blink::WebContentDecryptionModule* initial_cdm,
      const blink::WebString& sink_id,
      const cc::LayerTreeSettings* settings,
      scoped_refptr<base::TaskRunner> compositor_worker_task_runner) override;
~~~

### CreateWorkerContentSettingsClient

CreateWorkerContentSettingsClient
~~~cpp
std::unique_ptr<blink::WebContentSettingsClient>
  CreateWorkerContentSettingsClient() override;
~~~

### CreateSpeechRecognitionClient

CreateSpeechRecognitionClient
~~~cpp
std::unique_ptr<media::SpeechRecognitionClient> CreateSpeechRecognitionClient(
      media::SpeechRecognitionClient::OnReadyCallback callback) override;
~~~

### CreateWorkerFetchContext

CreateWorkerFetchContext
~~~cpp
scoped_refptr<blink::WebWorkerFetchContext> CreateWorkerFetchContext()
      override;
~~~

### CreateWorkerFetchContextForPlzDedicatedWorker

CreateWorkerFetchContextForPlzDedicatedWorker
~~~cpp
scoped_refptr<blink::WebWorkerFetchContext>
  CreateWorkerFetchContextForPlzDedicatedWorker(
      blink::WebDedicatedWorkerHostFactoryClient* factory_client) override;
~~~

### CreatePrescientNetworking

CreatePrescientNetworking
~~~cpp
std::unique_ptr<blink::WebPrescientNetworking> CreatePrescientNetworking()
      override;
~~~

### CreateResourceLoadInfoNotifierWrapper

CreateResourceLoadInfoNotifierWrapper
~~~cpp
std::unique_ptr<blink::ResourceLoadInfoNotifierWrapper>
  CreateResourceLoadInfoNotifierWrapper() override;
~~~

### CreateServiceWorkerProvider

CreateServiceWorkerProvider
~~~cpp
std::unique_ptr<blink::WebServiceWorkerProvider> CreateServiceWorkerProvider()
      override;
~~~

### GetRemoteNavigationAssociatedInterfaces

GetRemoteNavigationAssociatedInterfaces
~~~cpp
blink::AssociatedInterfaceProvider* GetRemoteNavigationAssociatedInterfaces()
      override;
~~~

### CreateChildFrame

CreateChildFrame
~~~cpp
blink::WebLocalFrame* CreateChildFrame(
      blink::mojom::TreeScopeType scope,
      const blink::WebString& name,
      const blink::WebString& fallback_name,
      const blink::FramePolicy& frame_policy,
      const blink::WebFrameOwnerProperties& frame_owner_properties,
      blink::FrameOwnerElementType frame_owner_element_type,
      blink::WebPolicyContainerBindParams policy_container_bind_params,
      ukm::SourceId document_ukm_source_id,
      FinishChildFrameCreationFn finish_creation) override;
~~~

### DidCreateFencedFrame

DidCreateFencedFrame
~~~cpp
void DidCreateFencedFrame(
      const blink::RemoteFrameToken& frame_token) override;
~~~

### FindFrame

FindFrame
~~~cpp
blink::WebFrame* FindFrame(const blink::WebString& name) override;
~~~

### WillSwap

WillSwap
~~~cpp
void WillSwap() override;
~~~

### WillDetach

WillDetach
~~~cpp
void WillDetach() override;
~~~

### FrameDetached

FrameDetached
~~~cpp
void FrameDetached() override;
~~~

### DidChangeName

DidChangeName
~~~cpp
void DidChangeName(const blink::WebString& name) override;
~~~

### DidMatchCSS

DidMatchCSS
~~~cpp
void DidMatchCSS(
      const blink::WebVector<blink::WebString>& newly_matching_selectors,
      const blink::WebVector<blink::WebString>& stopped_matching_selectors)
      override;
~~~

### ShouldReportDetailedMessageForSourceAndSeverity

ShouldReportDetailedMessageForSourceAndSeverity
~~~cpp
bool ShouldReportDetailedMessageForSourceAndSeverity(
      blink::mojom::ConsoleMessageLevel log_level,
      const blink::WebString& source) override;
~~~

### DidAddMessageToConsole

DidAddMessageToConsole
~~~cpp
void DidAddMessageToConsole(const blink::WebConsoleMessage& message,
                              const blink::WebString& source_name,
                              unsigned source_line,
                              const blink::WebString& stack_trace) override;
~~~

### BeginNavigation

BeginNavigation
~~~cpp
void BeginNavigation(std::unique_ptr<blink::WebNavigationInfo> info) override;
~~~

### DidCreateDocumentLoader

DidCreateDocumentLoader
~~~cpp
void DidCreateDocumentLoader(
      blink::WebDocumentLoader* document_loader) override;
~~~

### SwapIn

SwapIn
~~~cpp
bool SwapIn(blink::WebFrame* previous_web_frame) override;
~~~

### DidCommitNavigation

DidCommitNavigation
~~~cpp
void DidCommitNavigation(
      blink::WebHistoryCommitType commit_type,
      bool should_reset_browser_interface_broker,
      const blink::ParsedPermissionsPolicy& permissions_policy_header,
      const blink::DocumentPolicyFeatureState& document_policy_header) override;
~~~

### DidCommitDocumentReplacementNavigation

DidCommitDocumentReplacementNavigation
~~~cpp
void DidCommitDocumentReplacementNavigation(
      blink::WebDocumentLoader* document_loader) override;
~~~

### DidClearWindowObject

DidClearWindowObject
~~~cpp
void DidClearWindowObject() override;
~~~

### DidCreateDocumentElement

DidCreateDocumentElement
~~~cpp
void DidCreateDocumentElement() override;
~~~

### RunScriptsAtDocumentElementAvailable

RunScriptsAtDocumentElementAvailable
~~~cpp
void RunScriptsAtDocumentElementAvailable() override;
~~~

### DidReceiveTitle

DidReceiveTitle
~~~cpp
void DidReceiveTitle(const blink::WebString& title) override;
~~~

### DidDispatchDOMContentLoadedEvent

DidDispatchDOMContentLoadedEvent
~~~cpp
void DidDispatchDOMContentLoadedEvent() override;
~~~

### RunScriptsAtDocumentReady

RunScriptsAtDocumentReady
~~~cpp
void RunScriptsAtDocumentReady() override;
~~~

### RunScriptsAtDocumentIdle

RunScriptsAtDocumentIdle
~~~cpp
void RunScriptsAtDocumentIdle() override;
~~~

### DidHandleOnloadEvents

DidHandleOnloadEvents
~~~cpp
void DidHandleOnloadEvents() override;
~~~

### DidFinishLoad

DidFinishLoad
~~~cpp
void DidFinishLoad() override;
~~~

### DidFinishLoadForPrinting

DidFinishLoadForPrinting
~~~cpp
void DidFinishLoadForPrinting() override;
~~~

### DidFinishSameDocumentNavigation

DidFinishSameDocumentNavigation
~~~cpp
void DidFinishSameDocumentNavigation(
      blink::WebHistoryCommitType commit_type,
      bool is_synchronously_committed,
      blink::mojom::SameDocumentNavigationType same_document_navigation_type,
      bool is_client_redirect) override;
~~~

### WillFreezePage

WillFreezePage
~~~cpp
void WillFreezePage() override;
~~~

### DidOpenDocumentInputStream

DidOpenDocumentInputStream
~~~cpp
void DidOpenDocumentInputStream(const blink::WebURL& url) override;
~~~

### DidSetPageLifecycleState

DidSetPageLifecycleState
~~~cpp
void DidSetPageLifecycleState() override;
~~~

### NotifyCurrentHistoryItemChanged

NotifyCurrentHistoryItemChanged
~~~cpp
void NotifyCurrentHistoryItemChanged() override;
~~~

### DidUpdateCurrentHistoryItem

DidUpdateCurrentHistoryItem
~~~cpp
void DidUpdateCurrentHistoryItem() override;
~~~

### GetDevToolsFrameToken

GetDevToolsFrameToken
~~~cpp
base::UnguessableToken GetDevToolsFrameToken() override;
~~~

### AbortClientNavigation

AbortClientNavigation
~~~cpp
void AbortClientNavigation() override;
~~~

### DidChangeSelection

DidChangeSelection
~~~cpp
void DidChangeSelection(bool is_empty_selection,
                          blink::SyncCondition force_sync) override;
~~~

### FocusedElementChanged

FocusedElementChanged
~~~cpp
void FocusedElementChanged(const blink::WebElement& element) override;
~~~

### OnMainFrameIntersectionChanged

OnMainFrameIntersectionChanged
~~~cpp
void OnMainFrameIntersectionChanged(
      const gfx::Rect& main_frame_intersection_rect) override;
~~~

### OnMainFrameViewportRectangleChanged

OnMainFrameViewportRectangleChanged
~~~cpp
void OnMainFrameViewportRectangleChanged(
      const gfx::Rect& main_frame_viewport_rect) override;
~~~

### OnMainFrameImageAdRectangleChanged

OnMainFrameImageAdRectangleChanged
~~~cpp
void OnMainFrameImageAdRectangleChanged(
      int element_id,
      const gfx::Rect& image_ad_rect) override;
~~~

### WillSendRequest

WillSendRequest
~~~cpp
void WillSendRequest(blink::WebURLRequest& request,
                       ForRedirect for_redirect) override;
~~~

### OnOverlayPopupAdDetected

OnOverlayPopupAdDetected
~~~cpp
void OnOverlayPopupAdDetected() override;
~~~

### OnLargeStickyAdDetected

OnLargeStickyAdDetected
~~~cpp
void OnLargeStickyAdDetected() override;
~~~

### DidLoadResourceFromMemoryCache

DidLoadResourceFromMemoryCache
~~~cpp
void DidLoadResourceFromMemoryCache(
      const blink::WebURLRequest& request,
      const blink::WebURLResponse& response) override;
~~~

### DidChangePerformanceTiming

DidChangePerformanceTiming
~~~cpp
void DidChangePerformanceTiming() override;
~~~

### DidObserveInputDelay

DidObserveInputDelay
~~~cpp
void DidObserveInputDelay(base::TimeDelta input_delay) override;
~~~

### DidObserveUserInteraction

DidObserveUserInteraction
~~~cpp
void DidObserveUserInteraction(
      base::TimeDelta max_event_duration,
      blink::UserInteractionType interaction_type) override;
~~~

### DidChangeCpuTiming

DidChangeCpuTiming
~~~cpp
void DidChangeCpuTiming(base::TimeDelta time) override;
~~~

### DidObserveLoadingBehavior

DidObserveLoadingBehavior
~~~cpp
void DidObserveLoadingBehavior(blink::LoadingBehaviorFlag behavior) override;
~~~

### DidObserveSubresourceLoad

DidObserveSubresourceLoad
~~~cpp
void DidObserveSubresourceLoad(
      uint32_t number_of_subresources_loaded,
      uint32_t number_of_subresource_loads_handled_by_service_worker,
      bool pervasive_payload_requested,
      int64_t pervasive_bytes_fetched,
      int64_t total_bytes_fetched) override;
~~~

### DidObserveNewFeatureUsage

DidObserveNewFeatureUsage
~~~cpp
void DidObserveNewFeatureUsage(
      const blink::UseCounterFeature& feature) override;
~~~

### DidObserveSoftNavigation

DidObserveSoftNavigation
~~~cpp
void DidObserveSoftNavigation(uint32_t count) override;
~~~

### DidObserveLayoutShift

DidObserveLayoutShift
~~~cpp
void DidObserveLayoutShift(double score, bool after_input_or_scroll) override;
~~~

### DidCreateScriptContext

DidCreateScriptContext
~~~cpp
void DidCreateScriptContext(v8::Local<v8::Context> context,
                              int world_id) override;
~~~

### WillReleaseScriptContext

WillReleaseScriptContext
~~~cpp
void WillReleaseScriptContext(v8::Local<v8::Context> context,
                                int world_id) override;
~~~

### DidChangeScrollOffset

DidChangeScrollOffset
~~~cpp
void DidChangeScrollOffset() override;
~~~

### PreloadSubresourceOptimizationsForOrigins

PreloadSubresourceOptimizationsForOrigins
~~~cpp
void PreloadSubresourceOptimizationsForOrigins(
      const std::vector<blink::WebSecurityOrigin>& origins) override;
~~~

### MediaStreamDeviceObserver

MediaStreamDeviceObserver
~~~cpp
blink::WebMediaStreamDeviceObserver* MediaStreamDeviceObserver() override;
~~~

### EncryptedMediaClient

EncryptedMediaClient
~~~cpp
blink::WebEncryptedMediaClient* EncryptedMediaClient() override;
~~~

### UserAgentOverride

UserAgentOverride
~~~cpp
blink::WebString UserAgentOverride() override;
~~~

### UserAgentMetadataOverride

UserAgentMetadataOverride
~~~cpp
absl::optional<blink::UserAgentMetadata> UserAgentMetadataOverride() override;
~~~

### GetAudioInputStreamFactory

GetAudioInputStreamFactory
~~~cpp
blink::mojom::RendererAudioInputStreamFactory* GetAudioInputStreamFactory();
~~~

### AllowContentInitiatedDataUrlNavigations

AllowContentInitiatedDataUrlNavigations
~~~cpp
bool AllowContentInitiatedDataUrlNavigations(
      const blink::WebURL& url) override;
~~~

### PostAccessibilityEvent

PostAccessibilityEvent
~~~cpp
void PostAccessibilityEvent(const ui::AXEvent& event) override;
~~~

### NotifyWebAXObjectMarkedDirty

NotifyWebAXObjectMarkedDirty
~~~cpp
void NotifyWebAXObjectMarkedDirty(const blink::WebAXObject& object) override;
~~~

### AXReadyCallback

AXReadyCallback
~~~cpp
void AXReadyCallback() override;
~~~

### CheckIfAudioSinkExistsAndIsAuthorized

CheckIfAudioSinkExistsAndIsAuthorized
~~~cpp
void CheckIfAudioSinkExistsAndIsAuthorized(
      const blink::WebString& sink_id,
      blink::WebSetSinkIdCompleteCallback callback) override;
~~~

### GetURLLoaderFactory

GetURLLoaderFactory
~~~cpp
scoped_refptr<network::SharedURLLoaderFactory> GetURLLoaderFactory() override;
~~~

### OnStopLoading

OnStopLoading
~~~cpp
void OnStopLoading() override;
~~~

### DraggableRegionsChanged

DraggableRegionsChanged
~~~cpp
void DraggableRegionsChanged() override;
~~~

### GetBrowserInterfaceBroker

GetBrowserInterfaceBroker
~~~cpp
blink::BrowserInterfaceBrokerProxy* GetBrowserInterfaceBroker() override;
~~~

### CreateNewWindow

CreateNewWindow
~~~cpp
blink::WebView* CreateNewWindow(
      const blink::WebURLRequest& request,
      const blink::WebWindowFeatures& features,
      const blink::WebString& frame_name,
      blink::WebNavigationPolicy policy,
      network::mojom::WebSandboxFlags sandbox_flags,
      const blink::SessionStorageNamespaceId& session_storage_namespace_id,
      bool& consumed_user_gesture,
      const absl::optional<blink::Impression>& impression,
      const absl::optional<blink::WebPictureInPictureWindowOptions>&
          pip_options) override;
~~~

### SyncSelectionIfRequired

SyncSelectionIfRequired
~~~cpp
void SyncSelectionIfRequired(blink::SyncCondition force_sync) override;
~~~
 Dispatches the current state of selection on the webpage to the browser if
 it has changed or if the forced flag is passed. The forced flag is used
 when the browser selection may be out of sync with the renderer due to
 incorrect prediction.

### CreateAudioInputStream

CreateAudioInputStream
~~~cpp
void CreateAudioInputStream(
      blink::CrossVariantMojoRemote<
          blink::mojom::RendererAudioInputStreamFactoryClientInterfaceBase>
          client,
      const base::UnguessableToken& session_id,
      const media::AudioParameters& params,
      bool automatic_gain_control,
      uint32_t shared_memory_count,
      blink::CrossVariantMojoReceiver<
          media::mojom::AudioProcessorControlsInterfaceBase> controls_receiver,
      const media::AudioProcessingSettings* settings) override;
~~~

### AssociateInputAndOutputForAec

AssociateInputAndOutputForAec
~~~cpp
void AssociateInputAndOutputForAec(
      const base::UnguessableToken& input_stream_id,
      const std::string& output_device_id) override;
~~~

### DidMeaningfulLayout

DidMeaningfulLayout
~~~cpp
void DidMeaningfulLayout(blink::WebMeaningfulLayout layout_type) override;
~~~

### DidCommitAndDrawCompositorFrame

DidCommitAndDrawCompositorFrame
~~~cpp
void DidCommitAndDrawCompositorFrame() override;
~~~

### WasHidden

WasHidden
~~~cpp
void WasHidden() override;
~~~

### WasShown

WasShown
~~~cpp
void WasShown() override;
~~~

### SetUpSharedMemoryForSmoothness

SetUpSharedMemoryForSmoothness
~~~cpp
void SetUpSharedMemoryForSmoothness(
      base::ReadOnlySharedMemoryRegion shared_memory) override;
~~~

### LastCommittedUrlForUKM

LastCommittedUrlForUKM
~~~cpp
blink::WebURL LastCommittedUrlForUKM() override;
~~~

### ScriptedPrint

ScriptedPrint
~~~cpp
void ScriptedPrint() override;
~~~

### DeferMediaLoad

DeferMediaLoad
~~~cpp
bool DeferMediaLoad(bool has_played_media_before, base::OnceClosure closure);
~~~
 Possibly defers the loading of media resources.


 This function defers in two cases:
 - In the normal case, it calls ContentRendererClient::DeferMediaLoad()
   to give the embedder a chance to defer.

 - If the frame is prerendering, this function defers the load. It
   calls ContentRendererClient::DeferMediaLoad() once activation
   occurs.


 `closure` is run when loading should proceed. Returns true if running
 of |closure| is deferred; false if run immediately.


 If `has_played_media_before` is true, the render frame has previously
 started media playback (i.e. played audio and video).

### BindMhtmlFileWriter

BindMhtmlFileWriter
~~~cpp
void BindMhtmlFileWriter(
      mojo::PendingAssociatedReceiver<mojom::MhtmlFileWriter> receiver);
~~~
 Binds to the MHTML file generation service in the browser.

### BindAutoplayConfiguration

BindAutoplayConfiguration
~~~cpp
void BindAutoplayConfiguration(
      mojo::PendingAssociatedReceiver<blink::mojom::AutoplayConfigurationClient>
          receiver);
~~~
 Binds to the autoplay configuration service in the browser.

### BindFrameBindingsControl

BindFrameBindingsControl
~~~cpp
void BindFrameBindingsControl(
      mojo::PendingAssociatedReceiver<mojom::FrameBindingsControl> receiver);
~~~

### BindNavigationClient

BindNavigationClient
~~~cpp
void BindNavigationClient(
      mojo::PendingAssociatedReceiver<mojom::NavigationClient> receiver);
~~~

### GetFrameHost

GetFrameHost
~~~cpp
virtual mojom::FrameHost* GetFrameHost();
~~~
 Virtual so that a TestRenderFrame can mock out the interface.

### GetMediaPermission

GetMediaPermission
~~~cpp
media::MediaPermission* GetMediaPermission();
~~~

### SendUpdateState

SendUpdateState
~~~cpp
void SendUpdateState();
~~~
 Sends the current frame's navigation state to the browser.

### MaybeEnableMojoBindings

MaybeEnableMojoBindings
~~~cpp
void MaybeEnableMojoBindings();
~~~
 Creates a MojoBindingsController if Mojo bindings have been enabled for
 this frame. For WebUI, this allows the page to communicate with the browser
 process; for layout tests, this allows the test to mock out services at
 the Mojo IPC layer.

### NotifyObserversOfFailedProvisionalLoad

NotifyObserversOfFailedProvisionalLoad
~~~cpp
void NotifyObserversOfFailedProvisionalLoad();
~~~

### focused_pepper_plugin

focused_pepper_plugin
~~~cpp
PepperPluginInstanceImpl* focused_pepper_plugin() {
    return focused_pepper_plugin_;
  }
~~~

### PepperInstanceCreated

PepperInstanceCreated
~~~cpp
void PepperInstanceCreated(
      PepperPluginInstanceImpl* instance,
      mojo::PendingAssociatedRemote<mojom::PepperPluginInstance> mojo_instance,
      mojo::PendingAssociatedReceiver<mojom::PepperPluginInstanceHost>
          mojo_host);
~~~
 Indicates that the given instance has been created.

### PepperInstanceDeleted

PepperInstanceDeleted
~~~cpp
void PepperInstanceDeleted(PepperPluginInstanceImpl* instance);
~~~
 Indicates that the given instance is being destroyed. This is called from
 the destructor, so it's important that the instance is not dereferenced
 from this call.

### PepperFocusChanged

PepperFocusChanged
~~~cpp
void PepperFocusChanged(PepperPluginInstanceImpl* instance, bool focused);
~~~
 Notification that the given plugin is focused or unfocused.

### OnSetPepperVolume

OnSetPepperVolume
~~~cpp
void OnSetPepperVolume(int32_t pp_instance, double volume);
~~~

### GetRendererPreferences

GetRendererPreferences
~~~cpp
const blink::RendererPreferences& GetRendererPreferences() const;
~~~
 BUILDFLAG(ENABLE_PPAPI)
### OnDroppedNavigation

OnDroppedNavigation
~~~cpp
void OnDroppedNavigation();
~~~
 Called when an ongoing renderer-initiated navigation was dropped by the
 browser.

### DidStartResponse

DidStartResponse
~~~cpp
void DidStartResponse(const url::SchemeHostPort& final_response_url,
                        int request_id,
                        network::mojom::URLResponseHeadPtr response_head,
                        network::mojom::RequestDestination request_destination);
~~~

### DidCompleteResponse

DidCompleteResponse
~~~cpp
void DidCompleteResponse(int request_id,
                           const network::URLLoaderCompletionStatus& status);
~~~

### DidCancelResponse

DidCancelResponse
~~~cpp
void DidCancelResponse(int request_id);
~~~

### DidReceiveTransferSizeUpdate

DidReceiveTransferSizeUpdate
~~~cpp
void DidReceiveTransferSizeUpdate(int request_id, int received_data_length);
~~~

### GetCaretBoundsFromFocusedPlugin

GetCaretBoundsFromFocusedPlugin
~~~cpp
bool GetCaretBoundsFromFocusedPlugin(gfx::Rect& rect) override;
~~~

### SetURLLoaderFactoryOverrideForTest

SetURLLoaderFactoryOverrideForTest
~~~cpp
void SetURLLoaderFactoryOverrideForTest(
      scoped_refptr<network::SharedURLLoaderFactory> factory);
~~~
 Used in tests to install a fake URLLoaderFactory via
 RenderViewTest::CreateFakeURLLoaderFactory().

### CloneLoaderFactories

CloneLoaderFactories
~~~cpp
scoped_refptr<blink::ChildURLLoaderFactoryBundle> CloneLoaderFactories();
~~~
 Clones and returns `this` frame's blink::ChildURLLoaderFactoryBundle.

### GetSecurityOriginOfTopFrame

GetSecurityOriginOfTopFrame
~~~cpp
url::Origin GetSecurityOriginOfTopFrame();
~~~

### set_send_content_state_immediately

set_send_content_state_immediately
~~~cpp
void set_send_content_state_immediately(bool value) {
    send_content_state_immediately_ = value;
  }
~~~

### GetMediaDecoderFactory

GetMediaDecoderFactory
~~~cpp
base::WeakPtr<media::DecoderFactory> GetMediaDecoderFactory();
~~~

### IsLocalRoot

IsLocalRoot
~~~cpp
bool IsLocalRoot() const;
~~~

### GetLocalRoot

GetLocalRoot
~~~cpp
const RenderFrameImpl* GetLocalRoot() const;
~~~

### Create

Create
~~~cpp
static RenderFrameImpl* Create(
      AgentSchedulingGroup& agent_scheduling_group,
      int32_t routing_id,
      mojo::PendingAssociatedReceiver<mojom::Frame> frame_receiver,
      mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker>
          browser_interface_broker,
      mojo::PendingAssociatedRemote<blink::mojom::AssociatedInterfaceProvider>
          associated_interface_provider,
      const base::UnguessableToken& devtools_frame_token);
~~~
 Creates a new RenderFrame. |browser_interface_broker| is the
 RenderFrameHost's BrowserInterfaceBroker through which services are exposed
 to the RenderFrame.

### AddObserver

AddObserver
~~~cpp
void AddObserver(RenderFrameObserver* observer);
~~~
 Functions to add and remove observers for this object.

### RemoveObserver

RemoveObserver
~~~cpp
void RemoveObserver(RenderFrameObserver* observer);
~~~

### IsAccessibilityEnabled

IsAccessibilityEnabled
~~~cpp
bool IsAccessibilityEnabled() const;
~~~
 Checks whether accessibility support for this frame is currently enabled.

### CommitSameDocumentNavigation

CommitSameDocumentNavigation
~~~cpp
void CommitSameDocumentNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      CommitSameDocumentNavigationCallback callback) override;
~~~
 mojom::Frame implementation:
### UpdateSubresourceLoaderFactories

UpdateSubresourceLoaderFactories
~~~cpp
void UpdateSubresourceLoaderFactories(
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
          subresource_loader_factories) override;
~~~

### SetWantErrorMessageStackTrace

SetWantErrorMessageStackTrace
~~~cpp
void SetWantErrorMessageStackTrace() override;
~~~

### Unload

Unload
~~~cpp
void Unload(
      bool is_loading,
      blink::mojom::FrameReplicationStatePtr replicated_frame_state,
      const blink::RemoteFrameToken& frame_token,
      blink::mojom::RemoteFrameInterfacesFromBrowserPtr remote_frame_interfaces,
      blink::mojom::RemoteMainFrameInterfacesPtr remote_main_frame_interfaces)
      override;
~~~

### Delete

Delete
~~~cpp
void Delete(mojom::FrameDeleteIntention intent) override;
~~~

### UndoCommitNavigation

UndoCommitNavigation
~~~cpp
void UndoCommitNavigation(
      bool is_loading,
      blink::mojom::FrameReplicationStatePtr replicated_frame_state,
      const blink::RemoteFrameToken& frame_token,
      blink::mojom::RemoteFrameInterfacesFromBrowserPtr remote_frame_interfaces,
      blink::mojom::RemoteMainFrameInterfacesPtr remote_main_frame_interfaces)
      override;
~~~

### BlockRequests

BlockRequests
~~~cpp
void BlockRequests() override;
~~~

### ResumeBlockedRequests

ResumeBlockedRequests
~~~cpp
void ResumeBlockedRequests() override;
~~~

### GetInterfaceProvider

GetInterfaceProvider
~~~cpp
void GetInterfaceProvider(
      mojo::PendingReceiver<service_manager::mojom::InterfaceProvider> receiver)
      override;
~~~

### SnapshotAccessibilityTree

SnapshotAccessibilityTree
~~~cpp
void SnapshotAccessibilityTree(
      mojom::SnapshotAccessibilityTreeParamsPtr params,
      SnapshotAccessibilityTreeCallback callback) override;
~~~

### GetSerializedHtmlWithLocalLinks

GetSerializedHtmlWithLocalLinks
~~~cpp
void GetSerializedHtmlWithLocalLinks(
      const base::flat_map<GURL, base::FilePath>& url_map,
      const base::flat_map<blink::FrameToken, base::FilePath>& frame_token_map,
      bool save_with_empty_url,
      mojo::PendingRemote<mojom::FrameHTMLSerializerHandler> handler_remote)
      override;
~~~

### SetResourceCache

SetResourceCache
~~~cpp
void SetResourceCache(
      mojo::PendingRemote<blink::mojom::ResourceCache> remote) override;
~~~

### OnWriteMHTMLComplete

OnWriteMHTMLComplete
~~~cpp
void OnWriteMHTMLComplete(
      SerializeAsMHTMLCallback callback,
      std::unordered_set<std::string> serialized_resources_uri_digests,
      base::TimeDelta main_thread_use_time,
      mojom::MhtmlSaveStatus save_status);
~~~
 Callback scheduled from SerializeAsMHTML for when writing serialized
 MHTML to the handle has been completed in the file thread.

### OpenURL

OpenURL
~~~cpp
void OpenURL(std::unique_ptr<blink::WebNavigationInfo> info);
~~~
 Requests that the browser process navigates to |url|.

### GetLoaderFactoryBundle

GetLoaderFactoryBundle
~~~cpp
blink::ChildURLLoaderFactoryBundle* GetLoaderFactoryBundle();
~~~
 Returns a blink::ChildURLLoaderFactoryBundle which can be used to request
 subresources for this frame.


 The returned bundle was typically sent by the browser process when
 committing a navigation, but in some cases (about:srcdoc, initial empty
 document) it may be inherited from the parent or opener.

### CreateLoaderFactoryBundle

CreateLoaderFactoryBundle
~~~cpp
scoped_refptr<blink::ChildURLLoaderFactoryBundle> CreateLoaderFactoryBundle(
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle> info,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          prefetch_loader_factory,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          topics_loader_factory);
~~~

### UpdateEncoding

UpdateEncoding
~~~cpp
void UpdateEncoding(blink::WebFrame* frame, const std::string& encoding_name);
~~~
 Update current main frame's encoding and send it to browser window.

 Since we want to let users see the right encoding info from menu
 before finishing loading, we call the UpdateEncoding in
 a) function:DidCommitLoadForFrame. When this function is called,
 that means we have got first data. In here we try to get encoding
 of page if it has been specified in http header.

 b) function:DidReceiveTitle. When this function is called,
 that means we have got specified title. Because in most of webpages,
 title tags will follow meta tags. In here we try to get encoding of
 page if it has been specified in meta tag.

 c) function:DidDispatchDOMContentLoadedEvent. When this function is
 called, that means we have got whole html page. In here we should
 finally get right encoding of page.

### InitializeMediaStreamDeviceObserver

InitializeMediaStreamDeviceObserver
~~~cpp
void InitializeMediaStreamDeviceObserver();
~~~

### BeginNavigationInternal

BeginNavigationInternal
~~~cpp
void BeginNavigationInternal(std::unique_ptr<blink::WebNavigationInfo> info,
                               bool is_history_navigation_in_new_child_frame,
                               base::TimeTicks renderer_before_unload_start,
                               base::TimeTicks renderer_before_unload_end);
~~~
 Sends a FrameHostMsg_BeginNavigation to the browser
### SynchronouslyCommitAboutBlankForBug778318

SynchronouslyCommitAboutBlankForBug778318
~~~cpp
void SynchronouslyCommitAboutBlankForBug778318(
      std::unique_ptr<blink::WebNavigationInfo> info);
~~~
 TODO(https://crbug.com/778318): When creating a new browsing context, Blink
 always populates it with an initial empty document synchronously, as
 required by the HTML spec. However, for both iframe and window creation,
 there is an additional special case that currently requires completing an
 about:blank navigation synchronously.


 1. Inserting an <iframe> into the active document with no src and no
    srcdoc or with src = "about:blank".

 2. Opening a new window with no specified URL or with URL = "about:blank".


 In both cases, Blink will initialize the new browsing context, and then
 immediately re-navigate to "about:blank". This leads to a number of odd
 situations throughout the navigation stack, and it is spec-incompliant.


 For a new <iframe>, the re-navigation to "about:blank" should be a regular
 asynchronous navigation.


 For a new window, there should be no navigation at all: the standard states
 that a load event should be dispatched, and nothing else.


 See also:
 - https://chromium-review.googlesource.com/c/chromium/src/+/804797
 - https://github.com/whatwg/html/issues/3267
### CommitNavigationWithParams

CommitNavigationWithParams
~~~cpp
void CommitNavigationWithParams(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle>
          subresource_loader_factories,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      blink::mojom::ControllerServiceWorkerInfoPtr
          controller_service_worker_info,
      blink::mojom::ServiceWorkerContainerInfoForClientPtr container_info,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          prefetch_loader_factory,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          topics_loader_factory,
      mojo::PendingRemote<blink::mojom::CodeCacheHost> code_cache_host,
      mojom::CookieManagerInfoPtr cookie_manager_info,
      mojom::StorageInfoPtr storage_info,
      std::unique_ptr<DocumentState> document_state,
      std::unique_ptr<blink::WebNavigationParams> navigation_params);
~~~
 Commit navigation with |navigation_params| prepared.

### DecodeDataURL

DecodeDataURL
~~~cpp
void DecodeDataURL(const blink::mojom::CommonNavigationParams& common_params,
                     const blink::mojom::CommitNavigationParams& commit_params,
                     std::string* mime_type,
                     std::string* charset,
                     std::string* data,
                     GURL* base_url);
~~~
 Decodes a data url for navigation commit.

### WillSendRequestInternal

WillSendRequestInternal
~~~cpp
void WillSendRequestInternal(blink::WebURLRequest& request,
                               bool for_outermost_main_frame,
                               ui::PageTransition transition_type,
                               ForRedirect for_redirect);
~~~
 |transition_type| corresponds to the document which triggered this request.

### GetLoadingUrl

GetLoadingUrl
~~~cpp
GURL GetLoadingUrl() const;
~~~
 Returns the URL being loaded by the |frame_|'s request.

### RegisterMojoInterfaces

RegisterMojoInterfaces
~~~cpp
void RegisterMojoInterfaces();
~~~

### GetInterface

GetInterface
~~~cpp
void GetInterface(const std::string& interface_name,
                    mojo::ScopedMessagePipeHandle interface_pipe) override;
~~~
 service_manager::mojom::InterfaceProvider:
### RequestOverlayRoutingToken

RequestOverlayRoutingToken
~~~cpp
void RequestOverlayRoutingToken(media::RoutingTokenCallback callback);
~~~
 Send |callback| our AndroidOverlay routing token.

### BindWebUIReceiver

BindWebUIReceiver
~~~cpp
void BindWebUIReceiver(mojo::PendingReceiver<mojom::WebUI> receiver);
~~~

### MakeDidCommitProvisionalLoadParams

MakeDidCommitProvisionalLoadParams
~~~cpp
mojom::DidCommitProvisionalLoadParamsPtr MakeDidCommitProvisionalLoadParams(
      blink::WebHistoryCommitType commit_type,
      ui::PageTransition transition,
      const blink::ParsedPermissionsPolicy& permissions_policy_header,
      const blink::DocumentPolicyFeatureState& document_policy_header,
      const absl::optional<base::UnguessableToken>& embedding_token);
~~~
 Build DidCommitProvisionalLoadParams based on the frame internal state.

### UpdateNavigationHistory

UpdateNavigationHistory
~~~cpp
void UpdateNavigationHistory(blink::WebHistoryCommitType commit_type);
~~~
 Updates the navigation history depending on the passed parameters.

 This could result either in the creation of a new entry or a modification
 of the current entry or nothing. If a new entry was created,
 returns true, false otherwise.

### NotifyObserversOfNavigationCommit

NotifyObserversOfNavigationCommit
~~~cpp
void NotifyObserversOfNavigationCommit(ui::PageTransition transition);
~~~
 Notify render_view_ observers that a commit happened.

### UpdateStateForCommit

UpdateStateForCommit
~~~cpp
void UpdateStateForCommit(blink::WebHistoryCommitType commit_type,
                            ui::PageTransition transition);
~~~
 Updates the internal state following a navigation commit. This should be
 called before notifying the FrameHost of the commit.

### DidCommitNavigationInternal

DidCommitNavigationInternal
~~~cpp
void DidCommitNavigationInternal(
      blink::WebHistoryCommitType commit_type,
      ui::PageTransition transition,
      const blink::ParsedPermissionsPolicy& permissions_policy_header,
      const blink::DocumentPolicyFeatureState& document_policy_header,
      mojom::DidCommitProvisionalLoadInterfaceParamsPtr interface_params,
      mojom::DidCommitSameDocumentNavigationParamsPtr same_document_params,
      const absl::optional<base::UnguessableToken>& embedding_token);
~~~
 Internal function used by same document navigation as well as cross
 document navigation that updates the state of the RenderFrameImpl and sends
 a commit message to the browser process.

### GetOrCreateWebComputedAXTree

GetOrCreateWebComputedAXTree
~~~cpp
blink::WebComputedAXTree* GetOrCreateWebComputedAXTree() override;
~~~

### CreateWebSocketHandshakeThrottle

CreateWebSocketHandshakeThrottle
~~~cpp
std::unique_ptr<blink::WebSocketHandshakeThrottle>
  CreateWebSocketHandshakeThrottle() override;
~~~

### IsPluginHandledExternally

IsPluginHandledExternally
~~~cpp
bool IsPluginHandledExternally(
      const blink::WebElement& plugin_element,
      const blink::WebURL& url,
      const blink::WebString& suggested_mime_type) override;
~~~

### GetScriptableObject

GetScriptableObject
~~~cpp
v8::Local<v8::Object> GetScriptableObject(
      const blink::WebElement& plugin_element,
      v8::Isolate* isolate) override;
~~~

### UpdateSubresourceFactory

UpdateSubresourceFactory
~~~cpp
void UpdateSubresourceFactory(
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle> info) override;
~~~

### PrepareFrameForCommit

PrepareFrameForCommit
~~~cpp
void PrepareFrameForCommit(
      const GURL& url,
      const blink::mojom::CommitNavigationParams& commit_params);
~~~
 Updates the state of this frame when asked to commit a navigation.

### ShouldUseUserAgentOverride

ShouldUseUserAgentOverride
~~~cpp
bool ShouldUseUserAgentOverride() const;
~~~
 Returns true if UA (and UA client hints) overrides in renderer preferences
 should be used.

### SetOldPageLifecycleStateFromNewPageCommitIfNeeded

SetOldPageLifecycleStateFromNewPageCommitIfNeeded
~~~cpp
void SetOldPageLifecycleStateFromNewPageCommitIfNeeded(
      const blink::mojom::OldPageInfo* old_page_info,
      const GURL& new_page_url);
~~~
 Sets the PageLifecycleState and runs pagehide and visibilitychange handlers
 of the old page before committing this RenderFrame. Should only be called
 for main-frame same-site navigations where we did a proactive
 BrowsingInstance swap and we're reusing the old page's process. This is
 needed to ensure consistency with other same-site main frame navigations.

 Note that we will set the page's visibility to hidden, but not run the
 unload handlers of the old page, nor actually unload/freeze the page here.

 That needs a more complicated support on the browser side which will be
 implemented later.

 TODO(crbug.com/1110744): Support unload-in-commit.

### PrepareForHistoryNavigationCommit

PrepareForHistoryNavigationCommit
~~~cpp
blink::mojom::CommitResult PrepareForHistoryNavigationCommit(
      const blink::mojom::CommonNavigationParams& common_params,
      const blink::mojom::CommitNavigationParams& commit_params,
      blink::WebHistoryItem* item_for_history_navigation,
      blink::WebFrameLoadType* load_type);
~~~
 Updates the state when asked to commit a history navigation.  Sets
 |item_for_history_navigation| and |load_type| to the appropriate values for
 commit.


 The function will also return whether to proceed with the commit of a
 history navigation or not.  This can return false when the state of the
 history in the browser process goes out of sync with the renderer process.

 This can happen in the following scenario:
   * this RenderFrame has a document with URL foo, which does a push state
     to foo#bar.

   * the user starts a navigation to foo/bar.

   * the browser process asks this renderer process to commit the navigation
     to foo/bar.

   * the browser process starts a navigation back to foo, which it
     considers same-document since the navigation to foo/bar hasn't
     committed yet. It asks the RenderFrame to commit the same-document
     navigation to foo#bar.

   * by the time the RenderFrame receives the call to commit the
     same-document back navigation, the navigation to foo/bar has committed.

     A back navigation to foo is no longer same-document with the current
     document of the RenderFrame (foo/bar). Therefore, the navigation cannot
     be committed as a same-document navigation.

 When this happens, the navigation will be sent back to the browser process
 so that it can be performed in cross-document fashion.

### AddMessageToConsoleImpl

AddMessageToConsoleImpl
~~~cpp
void AddMessageToConsoleImpl(blink::mojom::ConsoleMessageLevel level,
                               const std::string& message,
                               bool discard_duplicates);
~~~
 Implements AddMessageToConsole().

### StartDelayedSyncTimer

StartDelayedSyncTimer
~~~cpp
void StartDelayedSyncTimer();
~~~
 Start a delayed timer to update the frame sync state to the browser.

 Debounces many updates in quick succession.

### SwapOutAndDeleteThis

SwapOutAndDeleteThis
~~~cpp
bool SwapOutAndDeleteThis(
      bool is_loading,
      blink::mojom::FrameReplicationStatePtr replicated_frame_state,
      const blink::RemoteFrameToken& frame_token,
      blink::mojom::RemoteFrameInterfacesFromBrowserPtr remote_frame_interfaces,
      blink::mojom::RemoteMainFrameInterfacesPtr remote_main_frame_interfaces);
~~~
 Swaps out the RenderFrame, creating a new `blink::RemoteFrame` and then
 swapping it into the frame tree to replace `this`. Returns false if
 swapping out `this` ends up detaching this frame instead when running the
 unload handlers, and true otherwise.


 Important: after this method returns, `this` has been deleted.

### ResetMembersUsedForDurationOfCommit

ResetMembersUsedForDurationOfCommit
~~~cpp
void ResetMembersUsedForDurationOfCommit();
~~~
 Resets membmers that are needed for the duration of commit (time between
 CommitNavigation() and DidCommitNavigation().

### rtNavigationCommits(Ren

rtNavigationCommits(Ren
~~~cpp
rtNavigationCommits(RenderFrameImpl* frame,
                                     MayReplaceInitialEmptyDocumentTag);

 
~~~

### ertNavigationCommits();


ertNavigationCommits();

~~~cpp
ertNavigationCommits();

 
~~~

## struct CreateParams
 Constructor parameters are bundled into a struct.

### CreateParams

CreateParams::CreateParams
~~~cpp
CreateParams(
        AgentSchedulingGroup& agent_scheduling_group,
        int32_t routing_id,
        mojo::PendingAssociatedReceiver<mojom::Frame> frame_receiver,
        mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker>
            browser_interface_broker,
        mojo::PendingAssociatedRemote<blink::mojom::AssociatedInterfaceProvider>
            associated_interface_provider,
        const base::UnguessableToken& devtools_frame_token);
    ~CreateParams();

    CreateParams(CreateParams&&);
    CreateParams& operator=(CreateParams&&)
~~~

## class AutoResetMember

### AutoResetMember

AutoResetMember
~~~cpp
AutoResetMember(RenderFrameImpl* frame,
                    T RenderFrameImpl::*member,
                    T new_value)
        : weak_frame_(frame->weak_factory_.GetWeakPtr()),
          scoped_variable_(&(frame->*member)),
          original_value_(*scoped_variable_) {
      *scoped_variable_ = new_value;
    }
~~~

### ~AutoResetMember

~AutoResetMember
~~~cpp
~AutoResetMember() {
      if (weak_frame_)
        *scoped_variable_ = original_value_;
    }
~~~

### weak_frame_



~~~cpp

base::WeakPtr<RenderFrameImpl> weak_frame_;

~~~


### field error



~~~cpp

T* scoped_variable_;

~~~


### original_value_



~~~cpp

T original_value_;

~~~


### AutoResetMember

AutoResetMember
~~~cpp
AutoResetMember(RenderFrameImpl* frame,
                    T RenderFrameImpl::*member,
                    T new_value)
        : weak_frame_(frame->weak_factory_.GetWeakPtr()),
          scoped_variable_(&(frame->*member)),
          original_value_(*scoped_variable_) {
      *scoped_variable_ = new_value;
    }
~~~

### ~AutoResetMember

~AutoResetMember
~~~cpp
~AutoResetMember() {
      if (weak_frame_)
        *scoped_variable_ = original_value_;
    }
~~~

### weak_frame_



~~~cpp

base::WeakPtr<RenderFrameImpl> weak_frame_;

~~~


### field error



~~~cpp

T* scoped_variable_;

~~~


### original_value_



~~~cpp

T original_value_;

~~~


## class UniqueNameFrameAdapter

### UniqueNameFrameAdapter

UniqueNameFrameAdapter::UniqueNameFrameAdapter
~~~cpp
explicit UniqueNameFrameAdapter(RenderFrameImpl* render_frame);
~~~

### ~UniqueNameFrameAdapter

UniqueNameFrameAdapter::~UniqueNameFrameAdapter
~~~cpp
~UniqueNameFrameAdapter() override;
~~~

### IsMainFrame

UniqueNameFrameAdapter::IsMainFrame
~~~cpp
bool IsMainFrame() const override;
~~~
 FrameAdapter overrides:
### IsCandidateUnique

UniqueNameFrameAdapter::IsCandidateUnique
~~~cpp
bool IsCandidateUnique(base::StringPiece name) const override;
~~~

### GetSiblingCount

UniqueNameFrameAdapter::GetSiblingCount
~~~cpp
int GetSiblingCount() const override;
~~~

### GetChildCount

UniqueNameFrameAdapter::GetChildCount
~~~cpp
int GetChildCount() const override;
~~~

### CollectAncestorNames

UniqueNameFrameAdapter::CollectAncestorNames
~~~cpp
std::vector<std::string> CollectAncestorNames(
        BeginPoint begin_point,
        bool (*should_stop)(base::StringPiece)) const override;
~~~

### GetFramePosition

UniqueNameFrameAdapter::GetFramePosition
~~~cpp
std::vector<int> GetFramePosition(BeginPoint begin_point) const override;
~~~

### GetWebFrame

UniqueNameFrameAdapter::GetWebFrame
~~~cpp
blink::WebLocalFrame* GetWebFrame() const;
~~~

### field error



~~~cpp

RenderFrameImpl* render_frame_;

~~~


### IsMainFrame

UniqueNameFrameAdapter::IsMainFrame
~~~cpp
bool IsMainFrame() const override;
~~~
 FrameAdapter overrides:
### IsCandidateUnique

UniqueNameFrameAdapter::IsCandidateUnique
~~~cpp
bool IsCandidateUnique(base::StringPiece name) const override;
~~~

### GetSiblingCount

UniqueNameFrameAdapter::GetSiblingCount
~~~cpp
int GetSiblingCount() const override;
~~~

### GetChildCount

UniqueNameFrameAdapter::GetChildCount
~~~cpp
int GetChildCount() const override;
~~~

### CollectAncestorNames

UniqueNameFrameAdapter::CollectAncestorNames
~~~cpp
std::vector<std::string> CollectAncestorNames(
        BeginPoint begin_point,
        bool (*should_stop)(base::StringPiece)) const override;
~~~

### GetFramePosition

UniqueNameFrameAdapter::GetFramePosition
~~~cpp
std::vector<int> GetFramePosition(BeginPoint begin_point) const override;
~~~

### GetWebFrame

UniqueNameFrameAdapter::GetWebFrame
~~~cpp
blink::WebLocalFrame* GetWebFrame() const;
~~~

### field error



~~~cpp

RenderFrameImpl* render_frame_;

~~~

