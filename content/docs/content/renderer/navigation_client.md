
## class NavigationClient

### NavigationClient

NavigationClient::NavigationClient
~~~cpp
NavigationClient(RenderFrameImpl* render_frame)
~~~

### ~NavigationClient

NavigationClient::~NavigationClient
~~~cpp
~NavigationClient() override
~~~

### CommitNavigation

NavigationClient::CommitNavigation
~~~cpp
CommitNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle> subresource_loaders,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      blink::mojom::ControllerServiceWorkerInfoPtr
          controller_service_worker_info,
      blink::mojom::ServiceWorkerContainerInfoForClientPtr container_info,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          prefetch_loader_factory,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          topics_loader_factory,
      const blink::DocumentToken& document_token,
      const base::UnguessableToken& devtools_navigation_token,
      const absl::optional<blink::ParsedPermissionsPolicy>& permissions_policy,
      blink::mojom::PolicyContainerPtr policy_container,
      mojo::PendingRemote<blink::mojom::CodeCacheHost> code_cache_host,
      mojom::CookieManagerInfoPtr cookie_manager_info,
      mojom::StorageInfoPtr storage_info,
      CommitNavigationCallback callback) override
~~~

### CommitFailedNavigation

NavigationClient::CommitFailedNavigation
~~~cpp
CommitFailedNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool has_stale_copy_in_cache,
      int error_code,
      int extended_error_code,
      const net::ResolveErrorInfo& resolve_error_info,
      const absl::optional<std::string>& error_page_content,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle> subresource_loaders,
      const blink::DocumentToken& document_token,
      blink::mojom::PolicyContainerPtr policy_container,
      mojom::AlternativeErrorPageOverrideInfoPtr alternative_error_page_info,
      CommitFailedNavigationCallback callback) override
~~~

### Bind

NavigationClient::Bind
~~~cpp
Bind(mojo::PendingAssociatedReceiver<mojom::NavigationClient> receiver)
~~~

### was_initiated_in_this_frame

was_initiated_in_this_frame
~~~cpp
bool was_initiated_in_this_frame() const {
    return was_initiated_in_this_frame_;
  }
~~~
 See NavigationState::was_initiated_in_this_frame for details.

### SetUpRendererInitiatedNavigation

NavigationClient::SetUpRendererInitiatedNavigation
~~~cpp
SetUpRendererInitiatedNavigation(
      mojo::PendingRemote<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener_remote)
~~~

### ResetWithoutCancelling

NavigationClient::ResetWithoutCancelling
~~~cpp
ResetWithoutCancelling()
~~~

### OnDroppedNavigation

NavigationClient::OnDroppedNavigation
~~~cpp
OnDroppedNavigation()
~~~

### SetDisconnectionHandler

NavigationClient::SetDisconnectionHandler
~~~cpp
SetDisconnectionHandler()
~~~

### ResetDisconnectionHandler

NavigationClient::ResetDisconnectionHandler
~~~cpp
ResetDisconnectionHandler()
~~~

### NotifyNavigationCancellationWindowEnded

NavigationClient::NotifyNavigationCancellationWindowEnded
~~~cpp
NotifyNavigationCancellationWindowEnded()
~~~

### navigation_client_receiver_

~~~cpp

mojo::AssociatedReceiver<mojom::NavigationClient> navigation_client_receiver_{
      this};

~~~


### renderer_cancellation_listener_remote_

~~~cpp

mojo::Remote<mojom::NavigationRendererCancellationListener>
      renderer_cancellation_listener_remote_;

~~~


### render_frame_

~~~cpp

RenderFrameImpl* render_frame_;

~~~


### was_initiated_in_this_frame_

~~~cpp

bool was_initiated_in_this_frame_ = false;

~~~

 See NavigationState::was_initiated_in_this_frame for details.

### weak_ptr_factory_

~~~cpp

base::WeakPtrFactory<NavigationClient> weak_ptr_factory_{this};

~~~


### was_initiated_in_this_frame

was_initiated_in_this_frame
~~~cpp
bool was_initiated_in_this_frame() const {
    return was_initiated_in_this_frame_;
  }
~~~
 See NavigationState::was_initiated_in_this_frame for details.

### CommitNavigation

NavigationClient::CommitNavigation
~~~cpp
CommitNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle> subresource_loaders,
      absl::optional<std::vector<blink::mojom::TransferrableURLLoaderPtr>>
          subresource_overrides,
      blink::mojom::ControllerServiceWorkerInfoPtr
          controller_service_worker_info,
      blink::mojom::ServiceWorkerContainerInfoForClientPtr container_info,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          prefetch_loader_factory,
      mojo::PendingRemote<network::mojom::URLLoaderFactory>
          topics_loader_factory,
      const blink::DocumentToken& document_token,
      const base::UnguessableToken& devtools_navigation_token,
      const absl::optional<blink::ParsedPermissionsPolicy>& permissions_policy,
      blink::mojom::PolicyContainerPtr policy_container,
      mojo::PendingRemote<blink::mojom::CodeCacheHost> code_cache_host,
      mojom::CookieManagerInfoPtr cookie_manager_info,
      mojom::StorageInfoPtr storage_info,
      CommitNavigationCallback callback) override
~~~

### CommitFailedNavigation

NavigationClient::CommitFailedNavigation
~~~cpp
CommitFailedNavigation(
      blink::mojom::CommonNavigationParamsPtr common_params,
      blink::mojom::CommitNavigationParamsPtr commit_params,
      bool has_stale_copy_in_cache,
      int error_code,
      int extended_error_code,
      const net::ResolveErrorInfo& resolve_error_info,
      const absl::optional<std::string>& error_page_content,
      std::unique_ptr<blink::PendingURLLoaderFactoryBundle> subresource_loaders,
      const blink::DocumentToken& document_token,
      blink::mojom::PolicyContainerPtr policy_container,
      mojom::AlternativeErrorPageOverrideInfoPtr alternative_error_page_info,
      CommitFailedNavigationCallback callback) override
~~~

### Bind

NavigationClient::Bind
~~~cpp
Bind(mojo::PendingAssociatedReceiver<mojom::NavigationClient> receiver)
~~~

### SetUpRendererInitiatedNavigation

NavigationClient::SetUpRendererInitiatedNavigation
~~~cpp
SetUpRendererInitiatedNavigation(
      mojo::PendingRemote<mojom::NavigationRendererCancellationListener>
          renderer_cancellation_listener_remote)
~~~

### ResetWithoutCancelling

NavigationClient::ResetWithoutCancelling
~~~cpp
ResetWithoutCancelling()
~~~

### OnDroppedNavigation

NavigationClient::OnDroppedNavigation
~~~cpp
OnDroppedNavigation()
~~~

### SetDisconnectionHandler

NavigationClient::SetDisconnectionHandler
~~~cpp
SetDisconnectionHandler()
~~~

### ResetDisconnectionHandler

NavigationClient::ResetDisconnectionHandler
~~~cpp
ResetDisconnectionHandler()
~~~

### NotifyNavigationCancellationWindowEnded

NavigationClient::NotifyNavigationCancellationWindowEnded
~~~cpp
NotifyNavigationCancellationWindowEnded()
~~~

### navigation_client_receiver_

~~~cpp

mojo::AssociatedReceiver<mojom::NavigationClient> navigation_client_receiver_{
      this};

~~~


### renderer_cancellation_listener_remote_

~~~cpp

mojo::Remote<mojom::NavigationRendererCancellationListener>
      renderer_cancellation_listener_remote_;

~~~


### render_frame_

~~~cpp

RenderFrameImpl* render_frame_;

~~~


### was_initiated_in_this_frame_

~~~cpp

bool was_initiated_in_this_frame_ = false;

~~~

 See NavigationState::was_initiated_in_this_frame for details.

### weak_ptr_factory_

~~~cpp

base::WeakPtrFactory<NavigationClient> weak_ptr_factory_{this};

~~~

