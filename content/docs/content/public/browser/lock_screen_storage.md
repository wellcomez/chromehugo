
## class LockScreenStorage
 Global storage for lock screen data stored by websites. This isn't
 BrowserContext keyed because there can only ever be one lock screen for the
 entire browser (the primary user's BrowserContext).

### Init

LockScreenStorage::Init
~~~cpp
virtual void Init(BrowserContext* browser_context,
                    const base::FilePath& base_path) = 0;
~~~
 LockScreenStorage must be initialized before any data is written to it or
 read from it by the Lock Screen API. The BrowserContext associated with the
 lock screen and the base path where data will be stored should be passed
 in. There can be up to one lock screen for the entire browser, so
 this can be called only once with the BrowserContext associated with the
 lock screen.
