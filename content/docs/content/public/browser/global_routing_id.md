### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
inline std::ostream& operator<<(std::ostream& os, const GlobalRoutingID& id) {
  os << "GlobalRoutingID(" << id.child_id << ", " << id.route_id << ")";
  return os;
}
~~~

### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
inline std::ostream& operator<<(std::ostream& os,
                                const GlobalRenderFrameHostId& id) {
  os << "GlobalRenderFrameHostId(" << id.child_id << ", " << id.frame_routing_id
     << ")";
  return os;
}
~~~

## class ;
 Uniquely identifies a target that legacy IPCs can be routed to.


 These IDs can be considered to be unique for the lifetime of the browser
 process. While they are finite and thus must eventually roll over, this case
 may be considered sufficiently rare as to be ignorable.

### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
inline std::ostream& operator<<(std::ostream& os, const GlobalRoutingID& id) {
  os << "GlobalRoutingID(" << id.child_id << ", " << id.route_id << ")";
  return os;
}
~~~

## struct GlobalRenderFrameHostId
 Same as GlobalRoutingID except the route_id must be a RenderFrameHost routing
 id.


 These IDs can be considered to be unique for the lifetime of the browser
 process. While they are finite and thus must eventually roll over, this case
 may be considered sufficiently rare as to be ignorable.

### GlobalRenderFrameHostId

GlobalRenderFrameHostId
~~~cpp
GlobalRenderFrameHostId() = default;
~~~

### GlobalRenderFrameHostId

GlobalRenderFrameHostId
~~~cpp
GlobalRenderFrameHostId(int child_id, int frame_routing_id)
      : child_id(child_id), frame_routing_id(frame_routing_id) {}
~~~

### GlobalRenderFrameHostId

GlobalRenderFrameHostId
~~~cpp
GlobalRenderFrameHostId(const GlobalRenderFrameHostId&) = default;
~~~
 GlobalRenderFrameHostId is copyable.

### operator=

GlobalRenderFrameHostId::operator=
~~~cpp
GlobalRenderFrameHostId& operator=(const GlobalRenderFrameHostId&) = default;
~~~

### operator&lt;

operator&lt;
~~~cpp
bool operator<(const GlobalRenderFrameHostId& other) const {
    return std::tie(child_id, frame_routing_id) <
           std::tie(other.child_id, other.frame_routing_id);
  }
~~~

### operator==

operator==
~~~cpp
bool operator==(const GlobalRenderFrameHostId& other) const {
    return child_id == other.child_id &&
           frame_routing_id == other.frame_routing_id;
  }
~~~

### operator!=

operator!=
~~~cpp
bool operator!=(const GlobalRenderFrameHostId& other) const {
    return !(*this == other);
  }
~~~

### 


~~~cpp
explicit operator bool() const {
    return frame_routing_id != MSG_ROUTING_NONE;
  }
~~~

### WriteIntoTrace

GlobalRenderFrameHostId::WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedProto<TraceProto> proto) const;
~~~
 Write a representation of this object into proto.
