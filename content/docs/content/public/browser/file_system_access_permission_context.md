
## class FileSystemAccessPermissionContext
 Entry point to an embedder implemented permission context for the File System
 Access API. Instances of this class can be retrieved via a BrowserContext.

 All these methods must always be called on the UI thread.

### enum class

~~~cpp
enum class UserAction {
    // The path for which a permission grant is requested was the result of a
    // "open" dialog. As such, only read access to files should be automatically
    // granted, but read access to directories as well as write access to files
    // or directories should not be granted without needing to request it.
    kOpen,
    // The path for which a permission grant is requested was the result of a
    // "save" dialog, and as such it could make sense to return a grant that
    // immediately allows write access without needing to request it.
    kSave,
    // The path for which a permission grant is requested was the result of
    // loading a handle from storage. As such the grant should not start out
    // as granted, even for read access.
    kLoadFromStorage,
    // The path for which a permission grant is requested was the result of a
    // drag&drop operation. Read access should start out granted, but write
    // access will require a prompt.
    kDragAndDrop,
    // The path for which a permission grant is requested was not the result of
    // a user action. This is used for checking additional blocklist check of
    // a path when obtaining a handle, therefore no prompt needs to be shown.
    kNone,
  }
~~~
### enum class

~~~cpp
enum class HandleType { kFile, kDirectory }
~~~
### enum class

~~~cpp
enum class PathType {
    // A path on the local file system. Files with these paths can be operated
    // on by base::File.
    kLocal = 0,

    // A path on an "external" file system. These paths can only be accessed via
    // the filesystem abstraction in //storage/browser/file_system, and a
    // storage::FileSystemURL of type storage::kFileSystemTypeExternal.
    // This path type should be used for paths retrieved via the `virtual_path`
    // member of a ui::SelectedFileInfo struct.
    kExternal = 1
  }
~~~
### field error



~~~cpp

struct PathInfo {
    PathType type = PathType::kLocal;
    base::FilePath path;
  };

~~~


### GetReadPermissionGrant

GetReadPermissionGrant
~~~cpp
virtual scoped_refptr<FileSystemAccessPermissionGrant> GetReadPermissionGrant(
      const url::Origin& origin,
      const base::FilePath& path,
      HandleType handle_type,
      UserAction user_action) = 0;
~~~
 Returns the read permission grant to use for a particular path.

### GetWritePermissionGrant

GetWritePermissionGrant
~~~cpp
virtual scoped_refptr<FileSystemAccessPermissionGrant>
  GetWritePermissionGrant(const url::Origin& origin,
                          const base::FilePath& path,
                          HandleType handle_type,
                          UserAction user_action) = 0;
~~~
 Returns the permission grant to use for a particular path. This could be a
 grant that applies to more than just the path passed in, for example if a
 user has already granted write access to a directory, this method could
 return that existing grant when figuring the grant to use for a file in
 that directory.

### enum class

~~~cpp
enum class SensitiveEntryResult {
    kAllowed = 0,   // Access to entry is okay.
    kTryAgain = 1,  // User should pick a different entry.
    kAbort = 2,     // Abandon entirely, as if picking was cancelled.
    kMaxValue = kAbort
  }
~~~
### ConfirmSensitiveEntryAccess

ConfirmSensitiveEntryAccess
~~~cpp
virtual void ConfirmSensitiveEntryAccess(
      const url::Origin& origin,
      PathType path_type,
      const base::FilePath& path,
      HandleType handle_type,
      UserAction user_action,
      GlobalRenderFrameHostId frame_id,
      base::OnceCallback<void(SensitiveEntryResult)> callback) = 0;
~~~
 Checks if access to the given `path` should be allowed or blocked. This is
 used to implement blocks for certain sensitive directories such as the
 "Windows" system directory, as well as the root of the "home" directory.

 For downloads ("Save as") it also checks the file extension. Calls
 `callback` with the result of the check, after potentially showing some UI
 to the user if the path is dangerous or should not be accessed.

### enum class

~~~cpp
enum class AfterWriteCheckResult { kAllow, kBlock }
~~~
### PerformAfterWriteChecks

PerformAfterWriteChecks
~~~cpp
virtual void PerformAfterWriteChecks(
      std::unique_ptr<FileSystemAccessWriteItem> item,
      GlobalRenderFrameHostId frame_id,
      base::OnceCallback<void(AfterWriteCheckResult)> callback) = 0;
~~~
 Runs a recently finished write operation through checks such as malware
 or other security checks to determine if the write should be allowed.

### CanObtainReadPermission

CanObtainReadPermission
~~~cpp
virtual bool CanObtainReadPermission(const url::Origin& origin) = 0;
~~~
 Returns whether the give |origin| already allows read permission, or it is
 possible to request one. This is used to block file dialogs from being
 shown if permission won't be granted anyway.

### CanObtainWritePermission

CanObtainWritePermission
~~~cpp
virtual bool CanObtainWritePermission(const url::Origin& origin) = 0;
~~~
 Returns whether the give |origin| already allows write permission, or it is
 possible to request one. This is used to block save file dialogs from being
 shown if there is no need to ask for it.

### SetLastPickedDirectory

SetLastPickedDirectory
~~~cpp
virtual void SetLastPickedDirectory(const url::Origin& origin,
                                      const std::string& id,
                                      const base::FilePath& path,
                                      const PathType type) = 0;
~~~
 Store the directory recently chosen by a file picker. This can later be
 retrieved via a call to |GetLastPickedDirectory| with the corresponding
 |origin| and |id|.

### GetLastPickedDirectory

GetLastPickedDirectory
~~~cpp
virtual PathInfo GetLastPickedDirectory(const url::Origin& origin,
                                          const std::string& id) = 0;
~~~
 Returns the directory recently chosen by a file picker for a given
 |origin| and |id|.

### GetWellKnownDirectoryPath

GetWellKnownDirectoryPath
~~~cpp
virtual base::FilePath GetWellKnownDirectoryPath(
      blink::mojom::WellKnownDirectory directory,
      const url::Origin& origin) = 0;
~~~
 Return the path associated with well-known directories such as "desktop"
 and "music", or a default path if the |directory| cannot be matched to a
 well-known directory. When |directory| is WellKnownDirectory.DIR_DOWNLOADS,
 |origin| is used to determine if browser-specified download directory
 should be returned instead of OS default download directory.

### GetPickerTitle

GetPickerTitle
~~~cpp
virtual std::u16string GetPickerTitle(
      const blink::mojom::FilePickerOptionsPtr& options) = 0;
~~~
 Return the desired title of the file picker for the given `options`.

### NotifyEntryMoved

NotifyEntryMoved
~~~cpp
virtual void NotifyEntryMoved(const url::Origin& origin,
                                const base::FilePath& old_path,
                                const base::FilePath& new_path) = 0;
~~~
 Notifies that the underlying file or directory has been moved and updates
 permission grants accordingly.

### ~FileSystemAccessPermissionContext

~FileSystemAccessPermissionContext
~~~cpp
virtual ~FileSystemAccessPermissionContext() = default;
~~~

### GetReadPermissionGrant

GetReadPermissionGrant
~~~cpp
virtual scoped_refptr<FileSystemAccessPermissionGrant> GetReadPermissionGrant(
      const url::Origin& origin,
      const base::FilePath& path,
      HandleType handle_type,
      UserAction user_action) = 0;
~~~
 Returns the read permission grant to use for a particular path.

### GetWritePermissionGrant

GetWritePermissionGrant
~~~cpp
virtual scoped_refptr<FileSystemAccessPermissionGrant>
  GetWritePermissionGrant(const url::Origin& origin,
                          const base::FilePath& path,
                          HandleType handle_type,
                          UserAction user_action) = 0;
~~~
 Returns the permission grant to use for a particular path. This could be a
 grant that applies to more than just the path passed in, for example if a
 user has already granted write access to a directory, this method could
 return that existing grant when figuring the grant to use for a file in
 that directory.

### ConfirmSensitiveEntryAccess

ConfirmSensitiveEntryAccess
~~~cpp
virtual void ConfirmSensitiveEntryAccess(
      const url::Origin& origin,
      PathType path_type,
      const base::FilePath& path,
      HandleType handle_type,
      UserAction user_action,
      GlobalRenderFrameHostId frame_id,
      base::OnceCallback<void(SensitiveEntryResult)> callback) = 0;
~~~
 Checks if access to the given `path` should be allowed or blocked. This is
 used to implement blocks for certain sensitive directories such as the
 "Windows" system directory, as well as the root of the "home" directory.

 For downloads ("Save as") it also checks the file extension. Calls
 `callback` with the result of the check, after potentially showing some UI
 to the user if the path is dangerous or should not be accessed.

### PerformAfterWriteChecks

PerformAfterWriteChecks
~~~cpp
virtual void PerformAfterWriteChecks(
      std::unique_ptr<FileSystemAccessWriteItem> item,
      GlobalRenderFrameHostId frame_id,
      base::OnceCallback<void(AfterWriteCheckResult)> callback) = 0;
~~~
 Runs a recently finished write operation through checks such as malware
 or other security checks to determine if the write should be allowed.

### CanObtainReadPermission

CanObtainReadPermission
~~~cpp
virtual bool CanObtainReadPermission(const url::Origin& origin) = 0;
~~~
 Returns whether the give |origin| already allows read permission, or it is
 possible to request one. This is used to block file dialogs from being
 shown if permission won't be granted anyway.

### CanObtainWritePermission

CanObtainWritePermission
~~~cpp
virtual bool CanObtainWritePermission(const url::Origin& origin) = 0;
~~~
 Returns whether the give |origin| already allows write permission, or it is
 possible to request one. This is used to block save file dialogs from being
 shown if there is no need to ask for it.

### SetLastPickedDirectory

SetLastPickedDirectory
~~~cpp
virtual void SetLastPickedDirectory(const url::Origin& origin,
                                      const std::string& id,
                                      const base::FilePath& path,
                                      const PathType type) = 0;
~~~
 Store the directory recently chosen by a file picker. This can later be
 retrieved via a call to |GetLastPickedDirectory| with the corresponding
 |origin| and |id|.

### GetLastPickedDirectory

GetLastPickedDirectory
~~~cpp
virtual PathInfo GetLastPickedDirectory(const url::Origin& origin,
                                          const std::string& id) = 0;
~~~
 Returns the directory recently chosen by a file picker for a given
 |origin| and |id|.

### GetWellKnownDirectoryPath

GetWellKnownDirectoryPath
~~~cpp
virtual base::FilePath GetWellKnownDirectoryPath(
      blink::mojom::WellKnownDirectory directory,
      const url::Origin& origin) = 0;
~~~
 Return the path associated with well-known directories such as "desktop"
 and "music", or a default path if the |directory| cannot be matched to a
 well-known directory. When |directory| is WellKnownDirectory.DIR_DOWNLOADS,
 |origin| is used to determine if browser-specified download directory
 should be returned instead of OS default download directory.

### GetPickerTitle

GetPickerTitle
~~~cpp
virtual std::u16string GetPickerTitle(
      const blink::mojom::FilePickerOptionsPtr& options) = 0;
~~~
 Return the desired title of the file picker for the given `options`.

### NotifyEntryMoved

NotifyEntryMoved
~~~cpp
virtual void NotifyEntryMoved(const url::Origin& origin,
                                const base::FilePath& old_path,
                                const base::FilePath& new_path) = 0;
~~~
 Notifies that the underlying file or directory has been moved and updates
 permission grants accordingly.

### ~FileSystemAccessPermissionContext

~FileSystemAccessPermissionContext
~~~cpp
virtual ~FileSystemAccessPermissionContext() = default;
~~~

### enum class
 The type of action a user took that resulted in needing a permission grant
 for a particular path. This is used to signal to the permission context if
 the path was the result of a "save" operation, which an implementation can
 use to automatically grant write access to the path.

~~~cpp
enum class UserAction {
    // The path for which a permission grant is requested was the result of a
    // "open" dialog. As such, only read access to files should be automatically
    // granted, but read access to directories as well as write access to files
    // or directories should not be granted without needing to request it.
    kOpen,
    // The path for which a permission grant is requested was the result of a
    // "save" dialog, and as such it could make sense to return a grant that
    // immediately allows write access without needing to request it.
    kSave,
    // The path for which a permission grant is requested was the result of
    // loading a handle from storage. As such the grant should not start out
    // as granted, even for read access.
    kLoadFromStorage,
    // The path for which a permission grant is requested was the result of a
    // drag&drop operation. Read access should start out granted, but write
    // access will require a prompt.
    kDragAndDrop,
    // The path for which a permission grant is requested was not the result of
    // a user action. This is used for checking additional blocklist check of
    // a path when obtaining a handle, therefore no prompt needs to be shown.
    kNone,
  };
~~~
### enum class
 This enum helps distinguish between file or directory File System Access
 handles.

~~~cpp
enum class HandleType { kFile, kDirectory };
~~~
### enum class
 These values are used in json serialization. Entries should not be
 renumbered and numeric values should never be reused.

~~~cpp
enum class PathType {
    // A path on the local file system. Files with these paths can be operated
    // on by base::File.
    kLocal = 0,

    // A path on an "external" file system. These paths can only be accessed via
    // the filesystem abstraction in //storage/browser/file_system, and a
    // storage::FileSystemURL of type storage::kFileSystemTypeExternal.
    // This path type should be used for paths retrieved via the `virtual_path`
    // member of a ui::SelectedFileInfo struct.
    kExternal = 1
  };
~~~
### field error



~~~cpp

struct PathInfo {
    PathType type = PathType::kLocal;
    base::FilePath path;
  };

~~~


### enum class
 These values are persisted to logs. Entries should not be renumbered and
 numeric values should never be reused.

~~~cpp
enum class SensitiveEntryResult {
    kAllowed = 0,   // Access to entry is okay.
    kTryAgain = 1,  // User should pick a different entry.
    kAbort = 2,     // Abandon entirely, as if picking was cancelled.
    kMaxValue = kAbort
  };
~~~
### enum class

~~~cpp
enum class AfterWriteCheckResult { kAllow, kBlock };
~~~