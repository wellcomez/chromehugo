
## class GpuDataManagerObserver
 Observers can register themselves via GpuDataManager::AddObserver, and
 can un-register with GpuDataManager::RemoveObserver.

### OnGpuExtraInfoUpdate

OnGpuExtraInfoUpdate
~~~cpp
virtual void OnGpuExtraInfoUpdate() {}
~~~
 Called for any observers whenever there is a GpuExtraInfo update.

### OnGpuProcessCrashed

OnGpuProcessCrashed
~~~cpp
virtual void OnGpuProcessCrashed() {}
~~~
 Called for any observer when the GPU process crashed.
