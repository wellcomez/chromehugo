### ReportBadMessage

ReportBadMessage
~~~cpp
NOT_TAIL_CALLED void ReportBadMessage(const std::string& message) {
    receivers_.ReportBadMessage(message);
  }
~~~
 Reports the currently dispatching Message as bad and closes+removes the
 receiver which received the message. Prefer this over the global
 `mojo::ReportBadMessage()` function, since calling this method promptly
 disconnects the receiver, preventing further (potentially bad) messages
 from being processed.


 Important: this method must only be called while the incoming message is
 being dispatched on the stack. To report a bad message after asynchronous
 processing (e.g. posting a task that then reports a the bad message), use
 `GetMessageCallback()` and pass the returned callback to the async task
 that needs to report the message as bad.

### GetBadMessageCallback

GetBadMessageCallback
~~~cpp
mojo::ReportBadMessageCallback GetBadMessageCallback() {
    return receivers_.GetBadMessageCallback();
  }
~~~
 Creates a callback which, when run, reports the currently dispatching
 Message as bad and closes+removes the receiver which received the message.

 Prefer this over the global `mojo::GetBadMessageCallback()` function,
 since running the callback promptly disconnects the receiver, preventing
 further (potentially bad) messages from being processed.


 Important: like `ReportBadMessage()`, this method must only be called while
 the incoming message is being dispatched on the stack. However, unlike
 `ReportBadMessage()`, the returned callback may be called even if the
 original message is no longer being dispatched on the stack.


 Sequence safety: the returned callback must be called on the sequence that
 owns `this` (i.e. the UI thread).

### SetCurrentTargetFrameForTesting

SetCurrentTargetFrameForTesting
~~~cpp
void SetCurrentTargetFrameForTesting(RenderFrameHost* render_frame_host) {
    current_target_frame_for_testing_ = render_frame_host;
  }
~~~

### SwapImplForTesting

SwapImplForTesting
~~~cpp
[[nodiscard]] ImplPointerType SwapImplForTesting(ImplPointerType new_impl) {
    ImplPointerType old_impl = impl_;
    impl_ = new_impl;

    for (const auto& it : frame_to_receivers_map_) {
      const std::vector<mojo::ReceiverId>& receiver_ids = it.second;
      for (const mojo::ReceiverId& id : receiver_ids) {
        // RenderFrameHostReceiverSet only allows all-or=nothing swaps, so
        // all the old impls are expected to be equal to `this`'s old impl_.
        CHECK_EQ(old_impl, receivers_.SwapImplForTesting(id, new_impl));
      }
    }

    return old_impl;
  }
~~~
 Allows test code to swap the interface implementation.


 Returns the existing interface implementation to the caller.


 The caller needs to guarantee that `new_impl` will live longer than
 `this` Receiver.  One way to achieve this is to store the returned
 `old_impl` and swap it back in when `new_impl` is getting destroyed.

 Test code should prefer using `mojo::test::ScopedSwapImplForTesting` if
 possible.

## class RenderFrameHostReceiverSet

### RenderFrameHostReceiverSet

RenderFrameHostReceiverSet
~~~cpp
RenderFrameHostReceiverSet(WebContents* web_contents, Interface* impl)
      : WebContentsObserver(web_contents), impl_(impl) {}
~~~

### ~RenderFrameHostReceiverSet

~RenderFrameHostReceiverSet
~~~cpp
~RenderFrameHostReceiverSet() override = default;
~~~

### RenderFrameHostReceiverSet

RenderFrameHostReceiverSet
~~~cpp
RenderFrameHostReceiverSet(const RenderFrameHostReceiverSet&) = delete;
~~~

### operator=

RenderFrameHostReceiverSet::operator=
~~~cpp
RenderFrameHostReceiverSet& operator=(const RenderFrameHostReceiverSet&) =
      delete;
~~~

### Bind

Bind
~~~cpp
void Bind(RenderFrameHost* render_frame_host,
            mojo::PendingAssociatedReceiver<Interface> pending_receiver) {
    // If the RenderFrameHost does not have a live RenderFrame:
    // 1. There is no point in binding receivers, as the renderer should not be
    //    doing anything with this RenderFrameHost.
    // 2. More problematic, `RenderFrameDeleted()` might not be called again
    //    for `render_frame_host`, potentially leaving dangling pointers to the
    //    RenderFrameHost (or other related objects) after the RenderFrameHost
    //    itself is later deleted.
    if (!render_frame_host->IsRenderFrameLive()) {
      return;
    }

    // Inject the ActiveUrlMessageFilter to improve crash reporting. This filter
    // sets the correct URL crash keys based on the target RFH that is
    // processing a message.
    mojo::ReceiverId id = receivers_.Add(
        impl_, std::move(pending_receiver), render_frame_host,
        std::make_unique<internal::ActiveUrlMessageFilter>(render_frame_host));
    frame_to_receivers_map_[render_frame_host].push_back(id);
  }
~~~
