
## class BluetoothChooser
 Represents a way to ask the user to select a Bluetooth device from a list of
 options.

### BluetoothChooser

BluetoothChooser
~~~cpp
BluetoothChooser() {}
~~~

### ~BluetoothChooser

BluetoothChooser::~BluetoothChooser
~~~cpp
~BluetoothChooser()
~~~

### CanAskForScanningPermission

BluetoothChooser::CanAskForScanningPermission
~~~cpp
virtual bool CanAskForScanningPermission();
~~~
 Some platforms (especially Android) require Chromium to have permission
 from the user before it can scan for Bluetooth devices. This function
 returns false if Chromium isn't even allowed to ask. It defaults to true.

### SetAdapterPresence

SetAdapterPresence
~~~cpp
virtual void SetAdapterPresence(AdapterPresence presence) {}
~~~

### ShowDiscoveryState

ShowDiscoveryState
~~~cpp
virtual void ShowDiscoveryState(DiscoveryState state) {}
~~~

### AddOrUpdateDevice

AddOrUpdateDevice
~~~cpp
virtual void AddOrUpdateDevice(const std::string& device_id,
                                 bool should_update_name,
                                 const std::u16string& device_name,
                                 bool is_gatt_connected,
                                 bool is_paired,
                                 int signal_strength_level) {}
~~~
 Adds a new device to the chooser or updates the information of an existing
 device.


 Sometimes when a Bluetooth device stops advertising, the |device_name| can
 be invalid, and in that case |should_update_name| will be set false.


 The range of |signal_strength_level| is -1 to 4 inclusively.

 -1 means that the device doesn't have RSSI which happens when the device
 is already connected.
