
## class WebUIMessageHandler
 Messages sent from the DOM are forwarded via the WebUI to handler
 classes. These objects are owned by WebUI and destroyed when the
 host is destroyed.

### ~WebUIMessageHandler

~WebUIMessageHandler
~~~cpp
virtual ~WebUIMessageHandler() = default;
~~~

### DisallowJavascript

WebUIMessageHandler::DisallowJavascript
~~~cpp
void DisallowJavascript();
~~~
 Call this when a page should not receive JavaScript messages.

### AllowJavascriptForTesting

WebUIMessageHandler::AllowJavascriptForTesting
~~~cpp
void AllowJavascriptForTesting();
~~~
 Called from tests to toggle JavaScript to catch bugs. If AllowJavascript()
 is needed from production code, just publicize AllowJavascript() instead.

### IsJavascriptAllowed

WebUIMessageHandler::IsJavascriptAllowed
~~~cpp
bool IsJavascriptAllowed();
~~~

### RegisterMessages

WebUIMessageHandler::RegisterMessages
~~~cpp
virtual void RegisterMessages() = 0;
~~~
 This is where subclasses specify which messages they'd like to handle and
 perform any additional initialization.. At this point web_ui() will return
 the associated WebUI object.

### OnJavascriptAllowed

OnJavascriptAllowed
~~~cpp
virtual void OnJavascriptAllowed() {}
~~~
 Will be called whenever JavaScript from this handler becomes allowed from
 the disallowed state. Subclasses should override this method to register
 observers that push JavaScript calls to the page.

### OnJavascriptDisallowed

OnJavascriptDisallowed
~~~cpp
virtual void OnJavascriptDisallowed() {}
~~~
 Will be called whenever JavaScript from this handler becomes disallowed
 from the allowed state. This will never be called before
 OnJavascriptAllowed has been called. Subclasses should override this method
 to deregister or disabled observers that push JavaScript calls to the page.

 Any WeakPtrFactory used for async tasks that would talk to the renderer
 must by invalidated by the subclass when this is called.

### ResolveJavascriptCallback

WebUIMessageHandler::ResolveJavascriptCallback
~~~cpp
void ResolveJavascriptCallback(const base::ValueView callback_id,
                                 const base::ValueView response);
~~~
 Helper method for responding to Javascript requests initiated with
 cr.sendWithPromise() (defined in cr.js) for the case where the returned
 promise should be resolved (request succeeded).

### RejectJavascriptCallback

WebUIMessageHandler::RejectJavascriptCallback
~~~cpp
void RejectJavascriptCallback(const base::ValueView callback_id,
                                const base::ValueView response);
~~~
 Helper method for responding to Javascript requests initiated with
 cr.sendWithPromise() (defined in cr.js), for the case where the returned
 promise should be rejected (request failed).

### FireWebUIListener

FireWebUIListener
~~~cpp
void FireWebUIListener(base::StringPiece event_name,
                         const Values&... values) {
    // cr.webUIListenerCallback is a global JS function exposed from cr.js.
    CallJavascriptFunction("cr.webUIListenerCallback", base::Value(event_name),
                           values...);
  }
~~~

### CallJavascriptFunction

CallJavascriptFunction
~~~cpp
void CallJavascriptFunction(base::StringPiece function_name,
                              const Values&... values) {
    CHECK(IsJavascriptAllowed()) << "Cannot CallJavascriptFunction before "
                                    "explicitly allowing JavaScript.";

    // The CHECK above makes this call safe.
    web_ui()->CallJavascriptFunctionUnsafe(function_name, values...);
  }
~~~

### web_ui

web_ui
~~~cpp
WebUI* web_ui() { return web_ui_; }
~~~
 Returns the attached WebUI for this handler.

### set_web_ui

set_web_ui
~~~cpp
void set_web_ui(WebUI* web_ui) { web_ui_ = web_ui; }
~~~
 Sets the attached WebUI - exposed to subclasses for testing purposes.
