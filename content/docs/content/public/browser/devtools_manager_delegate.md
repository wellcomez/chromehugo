
## class DevToolsManagerDelegate

### GetTargetType

DevToolsManagerDelegate::GetTargetType
~~~cpp
virtual std::string GetTargetType(WebContents* web_contents);
~~~
 Returns DevToolsAgentHost type to use for given |web_contents| target.

### GetTargetTitle

DevToolsManagerDelegate::GetTargetTitle
~~~cpp
virtual std::string GetTargetTitle(WebContents* web_contents);
~~~
 Returns DevToolsAgentHost title to use for given |web_contents| target.

### GetTargetDescription

DevToolsManagerDelegate::GetTargetDescription
~~~cpp
virtual std::string GetTargetDescription(WebContents* web_contents);
~~~
 Returns DevToolsAgentHost title to use for given |web_contents| target.

### AllowInspectingRenderFrameHost

DevToolsManagerDelegate::AllowInspectingRenderFrameHost
~~~cpp
virtual bool AllowInspectingRenderFrameHost(RenderFrameHost* rfh);
~~~
 Returns whether embedder allows to inspect given |rfh|.

### RemoteDebuggingTargets

DevToolsManagerDelegate::RemoteDebuggingTargets
~~~cpp
virtual DevToolsAgentHost::List RemoteDebuggingTargets();
~~~
 Returns all targets embedder would like to report as debuggable
 remotely.

### CreateNewTarget

DevToolsManagerDelegate::CreateNewTarget
~~~cpp
virtual scoped_refptr<DevToolsAgentHost> CreateNewTarget(const GURL& url,
                                                           bool for_tab);
~~~
 Creates new inspectable target given the |url|.

 If |for_tab| is true, creates a tab target, otherwise creates a frame
 target for the topmost frame. The difference is important in presence of
 prerender and other MPArch features, where there could be multiple topmost
 frames per tab. For details see
 https://docs.google.com/document/d/14aeiC_zga2SS0OXJd6eIFj8N0o5LGwUpuqa4L8NKoR4/
### GetBrowserContexts

DevToolsManagerDelegate::GetBrowserContexts
~~~cpp
virtual std::vector<BrowserContext*> GetBrowserContexts();
~~~
 Get all live browser contexts created by CreateBrowserContext() method.

### GetDefaultBrowserContext

DevToolsManagerDelegate::GetDefaultBrowserContext
~~~cpp
virtual BrowserContext* GetDefaultBrowserContext();
~~~
 Get default browser context. May return null if not supported.

### CreateBrowserContext

DevToolsManagerDelegate::CreateBrowserContext
~~~cpp
virtual BrowserContext* CreateBrowserContext();
~~~
 Create new browser context. May return null if not supported or not
 possible. Delegate must take ownership of the created browser context, and
 may destroy it at will.

### DisposeBrowserContext

DevToolsManagerDelegate::DisposeBrowserContext
~~~cpp
virtual void DisposeBrowserContext(BrowserContext* context,
                                     DisposeCallback callback);
~~~

### ClientAttached

DevToolsManagerDelegate::ClientAttached
~~~cpp
virtual void ClientAttached(DevToolsAgentHostClientChannel* channel);
~~~
 Called when a new client is attached/detached.

### ClientDetached

DevToolsManagerDelegate::ClientDetached
~~~cpp
virtual void ClientDetached(DevToolsAgentHostClientChannel* channel);
~~~

### HandleCommand

DevToolsManagerDelegate::HandleCommand
~~~cpp
virtual void HandleCommand(DevToolsAgentHostClientChannel* channel,
                             base::span<const uint8_t> message,
                             NotHandledCallback callback);
~~~

### GetDiscoveryPageHTML

DevToolsManagerDelegate::GetDiscoveryPageHTML
~~~cpp
virtual std::string GetDiscoveryPageHTML();
~~~
 Should return discovery page HTML that should list available tabs
 and provide attach links.

### HasBundledFrontendResources

DevToolsManagerDelegate::HasBundledFrontendResources
~~~cpp
virtual bool HasBundledFrontendResources();
~~~
 Returns whether frontend resources are bundled within the binary.

### IsBrowserTargetDiscoverable

DevToolsManagerDelegate::IsBrowserTargetDiscoverable
~~~cpp
virtual bool IsBrowserTargetDiscoverable();
~~~
 Makes browser target easily discoverable for remote debugging.

 This should only return true when remote debugging endpoint is not
 accessible by the web (for example in Chrome for Android where it is
 exposed via UNIX named socket) or when content/ embedder is built for
 running in the controlled environment (for example a special build for
 the Lab testing). If you want to return true here, please get security
 clearance from the devtools owners.

### ~DevToolsManagerDelegate

DevToolsManagerDelegate::~DevToolsManagerDelegate
~~~cpp
~DevToolsManagerDelegate()
~~~
