
## class CompositorClient

### operator=

CompositorClient::operator=
~~~cpp
CompositorClient& operator=(const CompositorClient&) = delete;
~~~

### RecreateSurface

RecreateSurface
~~~cpp
virtual void RecreateSurface() {}
~~~
 Compositor is requesting client to create a new surface and call
 SetSurface again. The existing surface if any is cleared from the
 compositor before this call.

### UpdateLayerTreeHost

UpdateLayerTreeHost
~~~cpp
virtual void UpdateLayerTreeHost() {}
~~~
 Gives the client a chance to update the layer tree host before compositing.

### DidSwapFrame

DidSwapFrame
~~~cpp
virtual void DidSwapFrame(int pending_frames) {}
~~~
 The compositor has completed swapping a frame. This is a subset of
 DidSwapBuffers and corresponds only to frames where Compositor submits a
 new frame.

### DidSwapBuffers

DidSwapBuffers
~~~cpp
virtual void DidSwapBuffers(const gfx::Size& swap_size) {}
~~~
 This is called on all swap buffers, regardless of cause.

### ~CompositorClient

~CompositorClient
~~~cpp
virtual ~CompositorClient() {}
~~~
