### BrowserContextFromJavaHandle

BrowserContextFromJavaHandle
~~~cpp
CONTENT_EXPORT content::BrowserContext* BrowserContextFromJavaHandle(
    const base::android::JavaRef<jobject>& jhandle);
~~~
 Returns a pointer to the native BrowserContext wrapped by the given Java
 BrowserContextHandle reference.

