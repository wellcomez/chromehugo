
## class SynchronousCompositorClient

### SynchronousCompositorClient

SynchronousCompositorClient
~~~cpp
SynchronousCompositorClient(const SynchronousCompositorClient&) = delete;
~~~

### operator=

operator=
~~~cpp
SynchronousCompositorClient& operator=(const SynchronousCompositorClient&) =
      delete;
~~~

### DidInitializeCompositor

DidInitializeCompositor
~~~cpp
virtual void DidInitializeCompositor(SynchronousCompositor* compositor,
                                       const viz::FrameSinkId& id) = 0;
~~~
 Indication to the client that |compositor| is now initialized on the
 compositor thread, and open for business. |process_id| and |routing_id|
 belong to the RVH that owns the compositor.

### DidDestroyCompositor

DidDestroyCompositor
~~~cpp
virtual void DidDestroyCompositor(SynchronousCompositor* compositor,
                                    const viz::FrameSinkId& id) = 0;
~~~
 Indication to the client that |compositor| is going out of scope, and
 must not be accessed within or after this call.

 NOTE if the client goes away before the compositor it must call
 SynchronousCompositor::SetClient(nullptr) to release the back pointer.

### UpdateRootLayerState

UpdateRootLayerState
~~~cpp
virtual void UpdateRootLayerState(SynchronousCompositor* compositor,
                                    const gfx::PointF& total_scroll_offset,
                                    const gfx::PointF& max_scroll_offset,
                                    const gfx::SizeF& scrollable_size,
                                    float page_scale_factor,
                                    float min_page_scale_factor,
                                    float max_page_scale_factor) = 0;
~~~

### DidOverscroll

DidOverscroll
~~~cpp
virtual void DidOverscroll(SynchronousCompositor* compositor,
                             const gfx::Vector2dF& accumulated_overscroll,
                             const gfx::Vector2dF& latest_overscroll_delta,
                             const gfx::Vector2dF& current_fling_velocity) = 0;
~~~

### PostInvalidate

PostInvalidate
~~~cpp
virtual void PostInvalidate(SynchronousCompositor* compositor) = 0;
~~~

### DidUpdateContent

DidUpdateContent
~~~cpp
virtual void DidUpdateContent(SynchronousCompositor* compositor) = 0;
~~~

### CreateDrawable

CreateDrawable
~~~cpp
virtual ui::TouchHandleDrawable* CreateDrawable() = 0;
~~~

### CopyOutput

CopyOutput
~~~cpp
virtual void CopyOutput(
      SynchronousCompositor* compositor,
      std::unique_ptr<viz::CopyOutputRequest> copy_request) = 0;
~~~

### AddBeginFrameCompletionCallback

AddBeginFrameCompletionCallback
~~~cpp
virtual void AddBeginFrameCompletionCallback(base::OnceClosure callback) = 0;
~~~

### SynchronousCompositorClient

SynchronousCompositorClient
~~~cpp
SynchronousCompositorClient() {}
~~~

### ~SynchronousCompositorClient

~SynchronousCompositorClient
~~~cpp
virtual ~SynchronousCompositorClient() {}
~~~

### SynchronousCompositorClient

SynchronousCompositorClient
~~~cpp
SynchronousCompositorClient(const SynchronousCompositorClient&) = delete;
~~~

### operator=

operator=
~~~cpp
SynchronousCompositorClient& operator=(const SynchronousCompositorClient&) =
      delete;
~~~

### DidInitializeCompositor

DidInitializeCompositor
~~~cpp
virtual void DidInitializeCompositor(SynchronousCompositor* compositor,
                                       const viz::FrameSinkId& id) = 0;
~~~
 Indication to the client that |compositor| is now initialized on the
 compositor thread, and open for business. |process_id| and |routing_id|
 belong to the RVH that owns the compositor.

### DidDestroyCompositor

DidDestroyCompositor
~~~cpp
virtual void DidDestroyCompositor(SynchronousCompositor* compositor,
                                    const viz::FrameSinkId& id) = 0;
~~~
 Indication to the client that |compositor| is going out of scope, and
 must not be accessed within or after this call.

 NOTE if the client goes away before the compositor it must call
 SynchronousCompositor::SetClient(nullptr) to release the back pointer.

### UpdateRootLayerState

UpdateRootLayerState
~~~cpp
virtual void UpdateRootLayerState(SynchronousCompositor* compositor,
                                    const gfx::PointF& total_scroll_offset,
                                    const gfx::PointF& max_scroll_offset,
                                    const gfx::SizeF& scrollable_size,
                                    float page_scale_factor,
                                    float min_page_scale_factor,
                                    float max_page_scale_factor) = 0;
~~~

### DidOverscroll

DidOverscroll
~~~cpp
virtual void DidOverscroll(SynchronousCompositor* compositor,
                             const gfx::Vector2dF& accumulated_overscroll,
                             const gfx::Vector2dF& latest_overscroll_delta,
                             const gfx::Vector2dF& current_fling_velocity) = 0;
~~~

### PostInvalidate

PostInvalidate
~~~cpp
virtual void PostInvalidate(SynchronousCompositor* compositor) = 0;
~~~

### DidUpdateContent

DidUpdateContent
~~~cpp
virtual void DidUpdateContent(SynchronousCompositor* compositor) = 0;
~~~

### CreateDrawable

CreateDrawable
~~~cpp
virtual ui::TouchHandleDrawable* CreateDrawable() = 0;
~~~

### CopyOutput

CopyOutput
~~~cpp
virtual void CopyOutput(
      SynchronousCompositor* compositor,
      std::unique_ptr<viz::CopyOutputRequest> copy_request) = 0;
~~~

### AddBeginFrameCompletionCallback

AddBeginFrameCompletionCallback
~~~cpp
virtual void AddBeginFrameCompletionCallback(base::OnceClosure callback) = 0;
~~~

### SynchronousCompositorClient

SynchronousCompositorClient
~~~cpp
SynchronousCompositorClient() {}
~~~

### ~SynchronousCompositorClient

~SynchronousCompositorClient
~~~cpp
virtual ~SynchronousCompositorClient() {}
~~~
