### CreateGpuVideoAcceleratorFactories

CreateGpuVideoAcceleratorFactories
~~~cpp
CONTENT_EXPORT
void CreateGpuVideoAcceleratorFactories(
    GpuVideoAcceleratorFactoriesCallback callback);
~~~
 Provides hardware video decoding contexts in the browser process.

