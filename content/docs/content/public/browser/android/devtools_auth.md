### CanUserConnectToDevTools

CanUserConnectToDevTools
~~~cpp
CONTENT_EXPORT bool CanUserConnectToDevTools(
    const net::UnixDomainServerSocket::Credentials& credentials);
~~~
 Returns true if the given peer identified by the credentials is authorized
 to connect to the devtools server, false if not.

