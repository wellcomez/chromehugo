
## class AndroidOverlayProvider

### AreOverlaysSupported

AndroidOverlayProvider::AreOverlaysSupported
~~~cpp
virtual bool AreOverlaysSupported() = 0;
~~~
**
   * Return true if this overlays are supported on this device.
   */