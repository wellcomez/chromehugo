
## class ;::SynchronousCompositor::FrameFuture

### FrameFuture

FrameFuture::;::SynchronousCompositor::FrameFuture
~~~cpp
FrameFuture();
~~~

### SetFrame

FrameFuture::;::SynchronousCompositor::SetFrame
~~~cpp
void SetFrame(std::unique_ptr<Frame> frame);
~~~

### GetFrame

FrameFuture::;::SynchronousCompositor::GetFrame
~~~cpp
std::unique_ptr<Frame> GetFrame();
~~~

### ~FrameFuture

FrameFuture::;::SynchronousCompositor::~FrameFuture
~~~cpp
~FrameFuture();
~~~

### waitable_event_



~~~cpp

base::WaitableEvent waitable_event_;

~~~


### frame_



~~~cpp

std::unique_ptr<Frame> frame_;

~~~


### waited_



~~~cpp

bool waited_ = false;

~~~


### SetFrame

FrameFuture::;::SynchronousCompositor::SetFrame
~~~cpp
void SetFrame(std::unique_ptr<Frame> frame);
~~~

### GetFrame

FrameFuture::;::SynchronousCompositor::GetFrame
~~~cpp
std::unique_ptr<Frame> GetFrame();
~~~

### waitable_event_



~~~cpp

base::WaitableEvent waitable_event_;

~~~


### frame_



~~~cpp

std::unique_ptr<Frame> frame_;

~~~


## class SynchronousCompositor::FrameFuture

### FrameFuture

FrameFuture::SynchronousCompositor::FrameFuture
~~~cpp
FrameFuture();
~~~

### SetFrame

FrameFuture::SynchronousCompositor::SetFrame
~~~cpp
void SetFrame(std::unique_ptr<Frame> frame);
~~~

### GetFrame

FrameFuture::SynchronousCompositor::GetFrame
~~~cpp
std::unique_ptr<Frame> GetFrame();
~~~

### ~FrameFuture

FrameFuture::SynchronousCompositor::~FrameFuture
~~~cpp
~FrameFuture();
~~~

### waitable_event_



~~~cpp

base::WaitableEvent waitable_event_;

~~~


### frame_



~~~cpp

std::unique_ptr<Frame> frame_;

~~~


### waited_



~~~cpp

bool waited_ = false;

~~~


### SetFrame

FrameFuture::SynchronousCompositor::SetFrame
~~~cpp
void SetFrame(std::unique_ptr<Frame> frame);
~~~

### GetFrame

FrameFuture::SynchronousCompositor::GetFrame
~~~cpp
std::unique_ptr<Frame> GetFrame();
~~~

### waitable_event_



~~~cpp

base::WaitableEvent waitable_event_;

~~~


### frame_



~~~cpp

std::unique_ptr<Frame> frame_;

~~~


## class SynchronousCompositor
 Interface for embedders that wish to direct compositing operations
 synchronously under their own control. Only meaningful when the
 kEnableSyncrhonousRendererCompositor flag is specified.

### DemandDrawHwAsync

SynchronousCompositor::DemandDrawHwAsync
~~~cpp
virtual scoped_refptr<FrameFuture> DemandDrawHwAsync(
      const gfx::Size& viewport_size,
      const gfx::Rect& viewport_rect_for_tile_priority,
      const gfx::Transform& transform_for_tile_priority) = 0;
~~~
 "On demand" hardware draw. Parameters are used by compositor for this draw.

 |viewport_size| is the current size to improve results during resize.

 |viewport_rect_for_tile_priority| and |transform_for_tile_priority| are
 used to customize the tiling decisions of compositor.

### ReturnResources

SynchronousCompositor::ReturnResources
~~~cpp
virtual void ReturnResources(
      uint32_t layer_tree_frame_sink_id,
      std::vector<viz::ReturnedResource> resources) = 0;
~~~
 For delegated rendering, return resources from parent compositor to this.

 Note that all resources must be returned before ReleaseHwDraw.

### OnCompositorFrameTransitionDirectiveProcessed

SynchronousCompositor::OnCompositorFrameTransitionDirectiveProcessed
~~~cpp
virtual void OnCompositorFrameTransitionDirectiveProcessed(
      uint32_t layer_tree_frame_sink_id,
      uint32_t sequence_id) = 0;
~~~
 Notifies the client when a directive for ViewTransition, submitted in
 a previous CompositorFrame, has finished executing.

### DidPresentCompositorFrames

SynchronousCompositor::DidPresentCompositorFrames
~~~cpp
virtual void DidPresentCompositorFrames(
      viz::FrameTimingDetailsMap timing_details,
      uint32_t frame_token) = 0;
~~~

### DemandDrawSw

SynchronousCompositor::DemandDrawSw
~~~cpp
virtual bool DemandDrawSw(SkCanvas* canvas, bool software_canvas) = 0;
~~~
 "On demand" SW draw, into the supplied canvas (observing the transform
 and clip set there-in).

 `software canvas` being true means drawing happens immediately instead
 of being cached, which allows more efficient drawing.

### SetMemoryPolicy

SynchronousCompositor::SetMemoryPolicy
~~~cpp
virtual void SetMemoryPolicy(size_t bytes_limit) = 0;
~~~
 Set the memory limit policy of this compositor.

### DidBecomeActive

SynchronousCompositor::DidBecomeActive
~~~cpp
virtual void DidBecomeActive() = 0;
~~~
 Called during renderer swap. Should push any relevant up to
 SynchronousCompositorClient.

### DidChangeRootLayerScrollOffset

SynchronousCompositor::DidChangeRootLayerScrollOffset
~~~cpp
virtual void DidChangeRootLayerScrollOffset(
      const gfx::PointF& root_offset) = 0;
~~~
 Should be called by the embedder after the embedder had modified the
 scroll offset of the root layer. |root_offset| must be in physical pixel
 scale.

### SynchronouslyZoomBy

SynchronousCompositor::SynchronouslyZoomBy
~~~cpp
virtual void SynchronouslyZoomBy(float zoom_delta,
                                   const gfx::Point& anchor) = 0;
~~~
 Allows embedder to synchronously update the zoom level, ie page scale
 factor, around the anchor point.

### OnComputeScroll

SynchronousCompositor::OnComputeScroll
~~~cpp
virtual void OnComputeScroll(base::TimeTicks animation_time) = 0;
~~~
 Called by the embedder to notify that the OnComputeScroll step is happening
 and if any input animation is active, it should tick now.

### SetBeginFrameSource

SynchronousCompositor::SetBeginFrameSource
~~~cpp
virtual void SetBeginFrameSource(
      viz::BeginFrameSource* begin_frame_source) = 0;
~~~
 Sets BeginFrameSource to use
### DidInvalidate

SynchronousCompositor::DidInvalidate
~~~cpp
virtual void DidInvalidate() = 0;
~~~
 Called when client invalidated because it was necessary for drawing sub
 clients. Used with viz for webview only.

### WasEvicted

SynchronousCompositor::WasEvicted
~~~cpp
virtual void WasEvicted() = 0;
~~~
 Called when embedder has evicted the previous compositor frame. So renderer
 needs to submit next frame with new LocalSurfaceId.
