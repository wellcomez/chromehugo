### RegisterMediaUrlInterceptor

RegisterMediaUrlInterceptor
~~~cpp
CONTENT_EXPORT void RegisterMediaUrlInterceptor(
    media::MediaUrlInterceptor* media_url_interceptor);
~~~
 Permits embedders to handle custom urls.

