### base::android::ScopedJavaLocalRef&lt;jobjectArray&gt;
    CreateJavaMessagePort

base::android::ScopedJavaLocalRef&lt;jobjectArray&gt;
    CreateJavaMessagePort
~~~cpp
CONTENT_EXPORT base::android::ScopedJavaLocalRef<jobjectArray>
    CreateJavaMessagePort(std::vector<blink::MessagePortDescriptor>);
~~~
 Helper function for converting between arrays of
 `blink::MessagePortDescriptors` (the way message ports are passed around in
 C++) and Java arrays of MessagePorts (the way they are passed around
 in Java).

 Take the ownership of `MessagePortDescriptor`s, and create a java array of
 `org.chromium.content_public.browser.MessagePort` to wrap them.

