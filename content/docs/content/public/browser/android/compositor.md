
## class Compositor
 An interface to the browser-side compositor.

### Initialize

Compositor::Initialize
~~~cpp
static void Initialize();
~~~
 Performs the global initialization needed before any compositor
 instance can be used. This should be called only once.

### CreateContextProvider

Compositor::CreateContextProvider
~~~cpp
static void CreateContextProvider(
      gpu::SurfaceHandle handle,
      gpu::ContextCreationAttribs attributes,
      gpu::SharedMemoryLimits shared_memory_limits,
      ContextProviderCallback callback);
~~~

### Create

Compositor::Create
~~~cpp
static Compositor* Create(CompositorClient* client,
                            gfx::NativeWindow root_window);
~~~
 Creates and returns a compositor instance.  |root_window| needs to outlive
 the compositor as it manages callbacks on the compositor.

### SetRootWindow

Compositor::SetRootWindow
~~~cpp
virtual void SetRootWindow(gfx::NativeWindow root_window) = 0;
~~~

### SetRootLayer

Compositor::SetRootLayer
~~~cpp
virtual void SetRootLayer(scoped_refptr<cc::slim::Layer> root) = 0;
~~~
 Attaches the layer tree.

### SetWindowBounds

Compositor::SetWindowBounds
~~~cpp
virtual void SetWindowBounds(const gfx::Size& size) = 0;
~~~
 Set the output surface bounds.

### GetWindowBounds

Compositor::GetWindowBounds
~~~cpp
virtual const gfx::Size& GetWindowBounds() = 0;
~~~
 Return the last size set with |SetWindowBounds|.

### SetSurface

Compositor::SetSurface
~~~cpp
virtual void SetSurface(const base::android::JavaRef<jobject>& surface,
                          bool can_be_used_with_surface_control) = 0;
~~~
 Set the output surface which the compositor renders into.

### SetBackgroundColor

Compositor::SetBackgroundColor
~~~cpp
virtual void SetBackgroundColor(int color) = 0;
~~~
 Set the background color used by the layer tree host.

### SetRequiresAlphaChannel

Compositor::SetRequiresAlphaChannel
~~~cpp
virtual void SetRequiresAlphaChannel(bool flag) = 0;
~~~
 Tells the compositor to allocate an alpha channel.  This won't take effect
 until the compositor selects a new egl config, usually when the underlying
 Android Surface changes format.

### SetNeedsComposite

Compositor::SetNeedsComposite
~~~cpp
virtual void SetNeedsComposite() = 0;
~~~
 Request layout and draw. You only need to call this if you need to trigger
 Composite *without* having modified the layer tree.

### SetNeedsRedraw

Compositor::SetNeedsRedraw
~~~cpp
virtual void SetNeedsRedraw() = 0;
~~~
 Request a draw and swap even if there is no change to the layer tree.

### GetUIResourceProvider

Compositor::GetUIResourceProvider
~~~cpp
virtual base::WeakPtr<ui::UIResourceProvider> GetUIResourceProvider() = 0;
~~~
 Returns the UI resource provider associated with the compositor.

### GetResourceManager

Compositor::GetResourceManager
~~~cpp
virtual ui::ResourceManager& GetResourceManager() = 0;
~~~
 Returns the resource manager associated with the compositor.

### CacheBackBufferForCurrentSurface

Compositor::CacheBackBufferForCurrentSurface
~~~cpp
virtual void CacheBackBufferForCurrentSurface() = 0;
~~~
 Caches the back buffer associated with the current surface, if any. The
 client is responsible for evicting this cache entry before destroying the
 associated window.

### EvictCachedBackBuffer

Compositor::EvictCachedBackBuffer
~~~cpp
virtual void EvictCachedBackBuffer() = 0;
~~~
 Evicts the cache entry created from the cached call above.

### PreserveChildSurfaceControls

Compositor::PreserveChildSurfaceControls
~~~cpp
virtual void PreserveChildSurfaceControls() = 0;
~~~
 Notifies associated Display to not detach child surface controls during
 destruction.

### RequestPresentationTimeForNextFrame

Compositor::RequestPresentationTimeForNextFrame
~~~cpp
virtual void RequestPresentationTimeForNextFrame(
      PresentationTimeCallback callback) = 0;
~~~

### RequestSuccessfulPresentationTimeForNextFrame

Compositor::RequestSuccessfulPresentationTimeForNextFrame
~~~cpp
virtual void RequestSuccessfulPresentationTimeForNextFrame(
      SuccessfulPresentationTimeCallback callback) = 0;
~~~

### SetDidSwapBuffersCallbackEnabled

Compositor::SetDidSwapBuffersCallbackEnabled
~~~cpp
virtual void SetDidSwapBuffersCallbackEnabled(bool enable) = 0;
~~~
 Control whether `CompositorClient::DidSwapBuffers` should be called. The
 default is false. Note this is asynchronous. Any pending callbacks may
 immediately after enabling may still be missed; best way to avoid this is
 to call this before calling `SetNeedsComposite` or `SetNeedsRedraw`. Also
 there may be trailing calls to `DidSwapBuffers` after unsetting this.
