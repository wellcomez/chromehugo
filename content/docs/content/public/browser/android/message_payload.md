### base::android::ScopedJavaLocalRef&lt;jobject&gt;
ConvertWebMessagePayloadToJava

base::android::ScopedJavaLocalRef&lt;jobject&gt;
ConvertWebMessagePayloadToJava
~~~cpp
CONTENT_EXPORT base::android::ScopedJavaLocalRef<jobject>
ConvertWebMessagePayloadToJava(const blink::WebMessagePayload& payload);
~~~
 Helper methods to convert between java
 `org.chromium.content_public.browser.MessagePayload` and
 `blink::WebMessagePayload`.

 Construct Java `org.chromium.content_public.browser.MessagePayload` from
 `blink::WebMessagePayload`.

### blink::WebMessagePayload ConvertToWebMessagePayloadFromJava

blink::WebMessagePayload ConvertToWebMessagePayloadFromJava
~~~cpp
CONTENT_EXPORT blink::WebMessagePayload ConvertToWebMessagePayloadFromJava(
    const base::android::ScopedJavaLocalRef<
        jobject>& /* org.chromium.content_public.browser.MessagePayload */);
~~~

