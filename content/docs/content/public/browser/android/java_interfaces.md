### GetGlobalJavaInterfaces

GetGlobalJavaInterfaces
~~~cpp
CONTENT_EXPORT service_manager::InterfaceProvider* GetGlobalJavaInterfaces();
~~~
 Returns an InterfaceProvider for global Java-implemented interfaces.

 This provides access to interfaces implemented in Java in the browser process
 to C++ code in the browser process. This and the returned InterfaceProvider
 may only be used on the UI thread.

