### OverrideVideoCaptureServiceForTesting

OverrideVideoCaptureServiceForTesting
~~~cpp
CONTENT_EXPORT void OverrideVideoCaptureServiceForTesting(
    video_capture::mojom::VideoCaptureService* service);
~~~
 Provides an override for the reference returned by
 |GetVideoCaptureService()|. Call again with null to cancel the override
 before |service| is destroyed.

