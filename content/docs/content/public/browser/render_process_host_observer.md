
## class RenderProcessHostObserver : public
 An observer API implemented by classes which are interested
 in RenderProcessHost lifecycle events. Note that this does not allow
 observing the creation of a RenderProcessHost. There is a separate observer
 for that: RenderProcessHostCreationObserver.

### RenderProcessExited

RenderProcessExited
~~~cpp
virtual void RenderProcessExited(RenderProcessHost* host,
                                   const ChildProcessTerminationInfo& info) {}
~~~
 This method is invoked when the process of the observed RenderProcessHost
 exits (either normally or with a crash). To determine if the process closed
 normally or crashed, examine the |status| parameter.


 A new render process may be spawned for this RenderProcessHost, but there
 are no guarantees (e.g. if shutdown is occurring, the HostDestroyed
 callback will happen soon and that will be it, but if the renderer crashed
 and the user clicks 'reload', a new render process will be spawned).


 This will cause a call to WebContentsObserver::RenderProcessGone() for the
 active renderer process for the top-level frame; for code that needs to be
 a WebContentsObserver anyway, consider whether that API might be a better
 choice.


 This is not called in --single-process mode.

### RenderProcessHostDestroyed

RenderProcessHostDestroyed
~~~cpp
virtual void RenderProcessHostDestroyed(RenderProcessHost* host) {}
~~~
 This method is invoked when the observed RenderProcessHost itself is
 destroyed. This is guaranteed to be the last call made to the observer, so
 if the observer is tied to the observed RenderProcessHost, it is safe to
 delete it.
