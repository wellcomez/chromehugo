
## class WebAuthenticationDelegate
 WebAuthenticationDelegate is an interface that lets the //content layer
 provide embedder specific configuration for handling Web Authentication API
 (https://www.w3.org/TR/webauthn/) requests.


 Instances can be obtained via
 ContentBrowserClient::GetWebAuthenticationDelegate().

### ~WebAuthenticationDelegate

WebAuthenticationDelegate::~WebAuthenticationDelegate
~~~cpp
~WebAuthenticationDelegate()
~~~

### OverrideCallerOriginAndRelyingPartyIdValidation

WebAuthenticationDelegate::OverrideCallerOriginAndRelyingPartyIdValidation
~~~cpp
virtual bool OverrideCallerOriginAndRelyingPartyIdValidation(
      BrowserContext* browser_context,
      const url::Origin& caller_origin,
      const std::string& relying_party_id);
~~~
 Returns true if `caller_origin` should be able to claim the given Relying
 Party ID outside of regular processing. Otherwise, standard WebAuthn RP ID
 security checks are performed by `WebAuthRequestSecurityChecker`.

 (https://www.w3.org/TR/2021/REC-webauthn-2-20210408/#relying-party-identifier).


 This is an access-control decision: RP IDs are used to control access to
 credentials so thought is required before allowing an origin to assert an
 RP ID.

### OriginMayUseRemoteDesktopClientOverride

WebAuthenticationDelegate::OriginMayUseRemoteDesktopClientOverride
~~~cpp
virtual bool OriginMayUseRemoteDesktopClientOverride(
      BrowserContext* browser_context,
      const url::Origin& caller_origin);
~~~
 Returns whether |caller_origin| is permitted to use the
 RemoteDesktopClientOverride extension.


 This is an access control decision: RP IDs are used to control access to
 credentials. If this method returns true, the respective origin is able to
 claim any RP ID.

### IsSecurityLevelAcceptableForWebAuthn

WebAuthenticationDelegate::IsSecurityLevelAcceptableForWebAuthn
~~~cpp
virtual bool IsSecurityLevelAcceptableForWebAuthn(
      content::RenderFrameHost* rfh,
      const url::Origin& caller_origin);
~~~
 Returns true if the tab security level is acceptable to allow WebAuthn
 requests, false otherwise.

### MaybeGetRelyingPartyIdOverride

WebAuthenticationDelegate::MaybeGetRelyingPartyIdOverride
~~~cpp
virtual absl::optional<std::string> MaybeGetRelyingPartyIdOverride(
      const std::string& claimed_relying_party_id,
      const url::Origin& caller_origin);
~~~
 Permits the embedder to override the Relying Party ID for a WebAuthn call,
 given the claimed relying party ID and the origin of the caller.


 This is an access-control decision: RP IDs are used to control access to
 credentials so thought is required before allowing an origin to assert an
 RP ID. RP ID strings may be stored on authenticators and may later appear
 in management UI.

### ShouldPermitIndividualAttestation

WebAuthenticationDelegate::ShouldPermitIndividualAttestation
~~~cpp
virtual bool ShouldPermitIndividualAttestation(
      BrowserContext* browser_context,
      const url::Origin& caller_origin,
      const std::string& relying_party_id);
~~~
 Returns true if the given relying party ID is permitted to receive
 individual attestation certificates. This:
  a) triggers a signal to the security key that returning individual
     attestation certificates is permitted, and
  b) skips any permission prompt for attestation.

### SupportsResidentKeys

WebAuthenticationDelegate::SupportsResidentKeys
~~~cpp
virtual bool SupportsResidentKeys(RenderFrameHost* render_frame_host);
~~~
 SupportsResidentKeys returns true if this implementation of
 |AuthenticatorRequestClientDelegate| supports resident keys for WebAuthn
 requests originating from |render_frame_host|. If false then requests to
 create or get assertions will be immediately rejected.

### IsFocused

WebAuthenticationDelegate::IsFocused
~~~cpp
virtual bool IsFocused(WebContents* web_contents);
~~~
 Returns whether |web_contents| is the active tab in the focused window. We
 do not want to allow authenticatorMakeCredential operations to be triggered
 by background tabs.


 Note that the default implementation of this function, and the
 implementation in ChromeContentBrowserClient for Android, return |true| so
 that testing is possible.

### IsUserVerifyingPlatformAuthenticatorAvailableOverride

WebAuthenticationDelegate::IsUserVerifyingPlatformAuthenticatorAvailableOverride
~~~cpp
virtual absl::optional<bool>
  IsUserVerifyingPlatformAuthenticatorAvailableOverride(
      RenderFrameHost* render_frame_host);
~~~
 Returns a bool if the result of the isUserVerifyingPlatformAuthenticator
 API call originating from |render_frame_host| should be overridden with
 that value, or absl::nullopt otherwise.

### MaybeGetRequestProxy

WebAuthenticationDelegate::MaybeGetRequestProxy
~~~cpp
virtual WebAuthenticationRequestProxy* MaybeGetRequestProxy(
      BrowserContext* browser_context,
      const url::Origin& caller_origin);
~~~
 Returns the active WebAuthenticationRequestProxy for WebAuthn requests
 originating from `caller_origin` in `browser_context`.


 If this method returns a proxy, the caller is expected to hand off WebAuthn
 request handling to this proxy instance.

### OperationSucceeded

WebAuthenticationDelegate::OperationSucceeded
~~~cpp
virtual void OperationSucceeded(BrowserContext* browser_context,
                                  bool used_win_api);
~~~
 OperationSucceeded is called when a registration or assertion operation
 succeeded. It communicates whether the Windows API was used or not. The
 implementation may wish to use this information to guide the UI for future
 operations towards the types of security keys that the user tends to use.

### GetTouchIdAuthenticatorConfig

WebAuthenticationDelegate::GetTouchIdAuthenticatorConfig
~~~cpp
virtual absl::optional<TouchIdAuthenticatorConfig>
  GetTouchIdAuthenticatorConfig(BrowserContext* browser_context);
~~~
 Returns configuration data for the built-in Touch ID platform
 authenticator. May return nullopt if the authenticator is not available in
 the current context, in which case the Touch ID authenticator will be
 unavailable.

### GetGenerateRequestIdCallback

WebAuthenticationDelegate::GetGenerateRequestIdCallback
~~~cpp
virtual ChromeOSGenerateRequestIdCallback GetGenerateRequestIdCallback(
      RenderFrameHost* render_frame_host);
~~~
 Returns a callback to generate a request id for a WebAuthn request
 originating from |RenderFrameHost|. The request id has two purposes: 1.

 ChromeOS UI will use the request id to find the source window and show a
 dialog accordingly; 2. The authenticator will include the request id when
 asking ChromeOS platform to cancel the request.

### GetIntentSender

WebAuthenticationDelegate::GetIntentSender
~~~cpp
virtual base::android::ScopedJavaLocalRef<jobject> GetIntentSender(
      WebContents* web_contents);
~~~
 GetIntentSender returns a Java object that implements
 `WebAuthenticationDelegate.IntentSender` from
 WebAuthenticationDelegate.java. See the comments in that file for details.

### GetSupportLevel

WebAuthenticationDelegate::GetSupportLevel
~~~cpp
virtual int GetSupportLevel(WebContents* web_contents);
~~~
 GetSupportLevel returns one of:
   0 -> No WebAuthn support for this `WebContents`.

   1 -> WebAuthn should be implemented like an app.

   2 -> WebAuthn should be implemented like a browser.


 The difference between app and browser is meaningful on Android because
 there is a different, privileged interface for browsers.


 The return value is an `int` rather than an enum because it's bounced
 access JNI boundaries multiple times and so it's only converted to an
 enum at the very end.

## class AuthenticatorRequestClientDelegate
    : public
 AuthenticatorRequestClientDelegate is an interface that lets embedders
 customize the lifetime of a single WebAuthn API request in the //content
 layer. In particular, the Authenticator mojo service uses
 AuthenticatorRequestClientDelegate to show WebAuthn request UI.

### AuthenticatorRequestClientDelegate

AuthenticatorRequestClientDelegate
~~~cpp
AuthenticatorRequestClientDelegate(
      const AuthenticatorRequestClientDelegate&) = delete;
~~~

### operator=

AuthenticatorRequestClientDelegate
    : public::operator=
~~~cpp
AuthenticatorRequestClientDelegate& operator=(
      const AuthenticatorRequestClientDelegate&) = delete;
~~~

### ~AuthenticatorRequestClientDelegate

AuthenticatorRequestClientDelegate
    : public::~AuthenticatorRequestClientDelegate
~~~cpp
~AuthenticatorRequestClientDelegate() override
~~~

### SetRelyingPartyId

AuthenticatorRequestClientDelegate
    : public::SetRelyingPartyId
~~~cpp
virtual void SetRelyingPartyId(const std::string& rp_id);
~~~
 SetRelyingPartyId sets the RP ID for this request. This is called after
 |WebAuthenticationDelegate::MaybeGetRelyingPartyIdOverride| is given the
 opportunity to affect this value. For typical origins, the RP ID is just a
 domain name, but
 |WebAuthenticationDelegate::MaybeGetRelyingPartyIdOverride| may return
 other forms of strings.

### DoesBlockRequestOnFailure

AuthenticatorRequestClientDelegate
    : public::DoesBlockRequestOnFailure
~~~cpp
virtual bool DoesBlockRequestOnFailure(InterestingFailureReason reason);
~~~
 Called when the request fails for the given |reason|.


 Embedders may return true if they want AuthenticatorImpl to hold off from
 resolving the WebAuthn request with an error, e.g. because they want the
 user to dismiss an error dialog first. In this case, embedders *must*
 eventually invoke the FidoRequestHandlerBase::CancelCallback in order to
 resolve the request. Returning false causes AuthenticatorImpl to resolve
 the request with the error right away.

### RegisterActionCallbacks

AuthenticatorRequestClientDelegate
    : public::RegisterActionCallbacks
~~~cpp
virtual void RegisterActionCallbacks(
      base::OnceClosure cancel_callback,
      base::RepeatingClosure start_over_callback,
      AccountPreselectedCallback account_preselected_callback,
      device::FidoRequestHandlerBase::RequestCallback request_callback,
      base::RepeatingClosure bluetooth_adapter_power_on_callback);
~~~
 Supplies callbacks that the embedder can invoke to initiate certain
 actions, namely: cancel the request, start the request over, preselect an
 account, dispatch request to connected authenticators, and power on the
 bluetooth adapter.

### ShouldReturnAttestation

AuthenticatorRequestClientDelegate
    : public::ShouldReturnAttestation
~~~cpp
virtual void ShouldReturnAttestation(
      const std::string& relying_party_id,
      const device::FidoAuthenticator* authenticator,
      bool is_enterprise_attestation,
      base::OnceCallback<void(bool)> callback);
~~~
 Invokes |callback| with |true| if the given relying party ID is permitted
 to receive attestation certificates from the provided FidoAuthenticator.

 Otherwise invokes |callback| with |false|.


 If |is_enterprise_attestation| is true then that authenticator has asserted
 that |relying_party_id| is known to it and the attesation has no
 expectations of privacy.


 Since these certificates may uniquely identify the authenticator, the
 embedder may choose to show a permissions prompt to the user, and only
 invoke |callback| afterwards. This may hairpin |callback|.

### ConfigureCable

AuthenticatorRequestClientDelegate
    : public::ConfigureCable
~~~cpp
virtual void ConfigureCable(
      const url::Origin& origin,
      device::CableRequestType request_type,
      absl::optional<device::ResidentKeyRequirement> resident_key_requirement,
      base::span<const device::CableDiscoveryData> pairings_from_extension,
      device::FidoDiscoveryFactory* fido_discovery_factory);
~~~
 ConfigureCable optionally configures Cloud-assisted Bluetooth Low Energy
 transports. |origin| is the origin of the calling site and
 |pairings_from_extension| are caBLEv1 pairings that have been provided in
 an extension to the WebAuthn get() call. |resident_key_requirement| is only
 set when provided (i.e. for makeCredential calls) and reflects the value
 requested by the site. If the embedder wishes, it may use this to configure
 caBLE on the |FidoDiscoveryFactory| for use in this request.

### SelectAccount

AuthenticatorRequestClientDelegate
    : public::SelectAccount
~~~cpp
virtual void SelectAccount(
      std::vector<device::AuthenticatorGetAssertionResponse> responses,
      base::OnceCallback<void(device::AuthenticatorGetAssertionResponse)>
          callback);
~~~
 SelectAccount is called to allow the embedder to select between one or more
 accounts. This is triggered when the web page requests an unspecified
 credential (by passing an empty allow-list). In this case, any accounts
 will come from the authenticator's storage and the user should confirm the
 use of any specific account before it is returned. The callback takes the
 selected account, or else |cancel_callback| can be called.


 This is only called if |WebAuthenticationDelegate::SupportsResidentKeys|
 returns true.

### DisableUI

AuthenticatorRequestClientDelegate
    : public::DisableUI
~~~cpp
virtual void DisableUI();
~~~
 Disables the WebAuthn request modal dialog UI.

### IsWebAuthnUIEnabled

AuthenticatorRequestClientDelegate
    : public::IsWebAuthnUIEnabled
~~~cpp
virtual bool IsWebAuthnUIEnabled();
~~~

### SetVirtualEnvironment

AuthenticatorRequestClientDelegate
    : public::SetVirtualEnvironment
~~~cpp
void SetVirtualEnvironment(bool virtual_environment);
~~~
 Configures whether a virtual authenticator environment is enabled. The
 embedder might choose to e.g. automate account selection under a virtual
 environment.

### IsVirtualEnvironmentEnabled

AuthenticatorRequestClientDelegate
    : public::IsVirtualEnvironmentEnabled
~~~cpp
bool IsVirtualEnvironmentEnabled();
~~~

### SetConditionalRequest

AuthenticatorRequestClientDelegate
    : public::SetConditionalRequest
~~~cpp
virtual void SetConditionalRequest(bool is_conditional);
~~~
 Set to true to enable a mode where a priori discovered credentials are
 shown alongside autofilled passwords, instead of the modal flow.

### SetCredentialIdFilter

AuthenticatorRequestClientDelegate
    : public::SetCredentialIdFilter
~~~cpp
virtual void SetCredentialIdFilter(
      std::vector<device::PublicKeyCredentialDescriptor> credential_list);
~~~
 Sets a credential filter for conditional mediation requests, which will
 only allow passkeys with matching credential IDs to be displayed to the
 user.

### SetUserEntityForMakeCredentialRequest

AuthenticatorRequestClientDelegate
    : public::SetUserEntityForMakeCredentialRequest
~~~cpp
virtual void SetUserEntityForMakeCredentialRequest(
      const device::PublicKeyCredentialUserEntity& user_entity);
~~~
 Optionally configures the user entity passed for a makeCredential request.

### OnTransportAvailabilityEnumerated

AuthenticatorRequestClientDelegate
    : public::OnTransportAvailabilityEnumerated
~~~cpp
void OnTransportAvailabilityEnumerated(
      device::FidoRequestHandlerBase::TransportAvailabilityInfo data) override;
~~~
 device::FidoRequestHandlerBase::Observer:
### EmbedderControlsAuthenticatorDispatch

AuthenticatorRequestClientDelegate
    : public::EmbedderControlsAuthenticatorDispatch
~~~cpp
bool EmbedderControlsAuthenticatorDispatch(
      const device::FidoAuthenticator& authenticator) override;
~~~
 If true, the request handler will defer dispatch of its request onto the
 given authenticator to the embedder. The embedder needs to call
 |StartAuthenticatorRequest| when it wants to initiate request dispatch.


 This method is invoked before |FidoAuthenticatorAdded|, and may be
 invoked multiple times for the same authenticator. Depending on the
 result, the request handler might decide not to make the authenticator
 available, in which case it never gets passed to
 |FidoAuthenticatorAdded|.

### BluetoothAdapterPowerChanged

AuthenticatorRequestClientDelegate
    : public::BluetoothAdapterPowerChanged
~~~cpp
void BluetoothAdapterPowerChanged(bool is_powered_on) override;
~~~

### FidoAuthenticatorAdded

AuthenticatorRequestClientDelegate
    : public::FidoAuthenticatorAdded
~~~cpp
void FidoAuthenticatorAdded(
      const device::FidoAuthenticator& authenticator) override;
~~~

### FidoAuthenticatorRemoved

AuthenticatorRequestClientDelegate
    : public::FidoAuthenticatorRemoved
~~~cpp
void FidoAuthenticatorRemoved(base::StringPiece device_id) override;
~~~

### SupportsPIN

AuthenticatorRequestClientDelegate
    : public::SupportsPIN
~~~cpp
bool SupportsPIN() const override;
~~~

### CollectPIN

AuthenticatorRequestClientDelegate
    : public::CollectPIN
~~~cpp
void CollectPIN(
      CollectPINOptions options,
      base::OnceCallback<void(std::u16string)> provide_pin_cb) override;
~~~

### StartBioEnrollment

AuthenticatorRequestClientDelegate
    : public::StartBioEnrollment
~~~cpp
void StartBioEnrollment(base::OnceClosure next_callback) override;
~~~

### OnSampleCollected

AuthenticatorRequestClientDelegate
    : public::OnSampleCollected
~~~cpp
void OnSampleCollected(int bio_samples_remaining) override;
~~~

### FinishCollectToken

AuthenticatorRequestClientDelegate
    : public::FinishCollectToken
~~~cpp
void FinishCollectToken() override;
~~~

### OnRetryUserVerification

AuthenticatorRequestClientDelegate
    : public::OnRetryUserVerification
~~~cpp
void OnRetryUserVerification(int attempts) override;
~~~
