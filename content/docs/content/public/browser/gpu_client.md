### std::unique_ptr&lt;viz::GpuClient, base::OnTaskRunnerDeleter&gt; CreateGpuClient

std::unique_ptr&lt;viz::GpuClient, base::OnTaskRunnerDeleter&gt; CreateGpuClient
~~~cpp
CONTENT_EXPORT
std::unique_ptr<viz::GpuClient, base::OnTaskRunnerDeleter> CreateGpuClient(
    mojo::PendingReceiver<viz::mojom::Gpu> receiver,
    viz::GpuClient::ConnectionErrorHandlerClosure connection_error_handler);
~~~

