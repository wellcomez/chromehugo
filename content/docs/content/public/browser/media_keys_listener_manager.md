
## class MediaKeysListenerManager
 The browser listens for media keys and uses them to control the active media
 session. However, there are cases where this behavior is undesirable, for
 example when an extension wants to handle media key presses instead. This
 class provides an interface for code outside of content (e.g. extensions) to
 receive global media key input instead of allowing the browser to use it to
 control the active session.

### GetInstance

MediaKeysListenerManager::GetInstance
~~~cpp
CONTENT_EXPORT static MediaKeysListenerManager* GetInstance();
~~~
 Returns the singleton instance.

### IsMediaKeysListenerManagerEnabled

MediaKeysListenerManager::IsMediaKeysListenerManagerEnabled
~~~cpp
CONTENT_EXPORT static bool IsMediaKeysListenerManagerEnabled();
~~~
 Returns true if the MediaKeysListenerManager is enabled, taking OS and
 feature flags into account.

### StartWatchingMediaKey

StartWatchingMediaKey
~~~cpp
virtual bool StartWatchingMediaKey(
      ui::KeyboardCode key_code,
      ui::MediaKeysListener::Delegate* delegate) = 0;
~~~
 Start listening for a given media key. Returns true if the listener
 successfully started listening for the key. This will prevent the
 HardwareKeyMediaController from also handling the specified key.

 Note: Delegates *must* call |StopWatchingMediaKey()| for each key code
 they're listening to before destruction in order to prevent the
 MediaKeysListenerManager from holding invalid pointers.

### StopWatchingMediaKey

StopWatchingMediaKey
~~~cpp
virtual void StopWatchingMediaKey(
      ui::KeyboardCode key_code,
      ui::MediaKeysListener::Delegate* delegate) = 0;
~~~
 Stop listening for a given media key. This will free the key to be handled
 by the HardwareKeyMediaController. Delegates must stop watching all keys
 before they are destroyed in order to prevent the MediaKeysListenerManager
 from holding invalid pointers.

### DisableInternalMediaKeyHandling

DisableInternalMediaKeyHandling
~~~cpp
virtual void DisableInternalMediaKeyHandling() = 0;
~~~
 Prevent the browser from using media key presses to control the active
 media session. This allows a caller to prevent the media key handling
 without registering to receive the key events.  Note that this does not
 prevent callers of |StartWatchingMediaKey()| from receiving media key
 events. This does not need to be called if |StartWatchingMediaKey()| is
 used, since |StartWatchingMediaKey()| will automatically prevent the
 browser from using the media key presses.

### EnableInternalMediaKeyHandling

EnableInternalMediaKeyHandling
~~~cpp
virtual void EnableInternalMediaKeyHandling() = 0;
~~~
 Allows the browser to use media key presses to control the active media.

 Only needs to be called if a call to |DisableInternalMediaKeyHandling()|
 has been made.

### ~MediaKeysListenerManager

MediaKeysListenerManager::~MediaKeysListenerManager
~~~cpp
virtual ~MediaKeysListenerManager();
~~~

### StartWatchingMediaKey

StartWatchingMediaKey
~~~cpp
virtual bool StartWatchingMediaKey(
      ui::KeyboardCode key_code,
      ui::MediaKeysListener::Delegate* delegate) = 0;
~~~
 Start listening for a given media key. Returns true if the listener
 successfully started listening for the key. This will prevent the
 HardwareKeyMediaController from also handling the specified key.

 Note: Delegates *must* call |StopWatchingMediaKey()| for each key code
 they're listening to before destruction in order to prevent the
 MediaKeysListenerManager from holding invalid pointers.

### StopWatchingMediaKey

StopWatchingMediaKey
~~~cpp
virtual void StopWatchingMediaKey(
      ui::KeyboardCode key_code,
      ui::MediaKeysListener::Delegate* delegate) = 0;
~~~
 Stop listening for a given media key. This will free the key to be handled
 by the HardwareKeyMediaController. Delegates must stop watching all keys
 before they are destroyed in order to prevent the MediaKeysListenerManager
 from holding invalid pointers.

### DisableInternalMediaKeyHandling

DisableInternalMediaKeyHandling
~~~cpp
virtual void DisableInternalMediaKeyHandling() = 0;
~~~
 Prevent the browser from using media key presses to control the active
 media session. This allows a caller to prevent the media key handling
 without registering to receive the key events.  Note that this does not
 prevent callers of |StartWatchingMediaKey()| from receiving media key
 events. This does not need to be called if |StartWatchingMediaKey()| is
 used, since |StartWatchingMediaKey()| will automatically prevent the
 browser from using the media key presses.

### EnableInternalMediaKeyHandling

EnableInternalMediaKeyHandling
~~~cpp
virtual void EnableInternalMediaKeyHandling() = 0;
~~~
 Allows the browser to use media key presses to control the active media.

 Only needs to be called if a call to |DisableInternalMediaKeyHandling()|
 has been made.

### GetInstance

MediaKeysListenerManager::GetInstance
~~~cpp
CONTENT_EXPORT static MediaKeysListenerManager* GetInstance();
~~~
 Returns the singleton instance.

### IsMediaKeysListenerManagerEnabled

MediaKeysListenerManager::IsMediaKeysListenerManagerEnabled
~~~cpp
CONTENT_EXPORT static bool IsMediaKeysListenerManagerEnabled();
~~~
 Returns true if the MediaKeysListenerManager is enabled, taking OS and
 feature flags into account.
