
## class BackgroundSyncContext
 One instance of this exists per StoragePartition, and services multiple child
 processes/origins. It contains the context for processing Background Sync
 registrations, and delegates most of this processing to owned instances of
 other components.

### FireBackgroundSyncEventsAcrossPartitions

BackgroundSyncContext::FireBackgroundSyncEventsAcrossPartitions
~~~cpp
static void FireBackgroundSyncEventsAcrossPartitions(
      BrowserContext* browser_context,
      blink::mojom::BackgroundSyncType sync_type,
      const base::android::JavaParamRef<jobject>& j_runnable);
~~~
 Processes pending Background Sync registrations of |sync_type| for all the
 storage partitions in |browser_context|, and then runs  the |j_runnable|
 when done.

### BackgroundSyncContext

BackgroundSyncContext
~~~cpp
BackgroundSyncContext() = default;
~~~

### BackgroundSyncContext

BackgroundSyncContext
~~~cpp
BackgroundSyncContext(const BackgroundSyncContext&) = delete;
~~~

### operator=

BackgroundSyncContext::operator=
~~~cpp
BackgroundSyncContext& operator=(const BackgroundSyncContext&) = delete;
~~~

### FireBackgroundSyncEvents

BackgroundSyncContext::FireBackgroundSyncEvents
~~~cpp
virtual void FireBackgroundSyncEvents(
      blink::mojom::BackgroundSyncType sync_type,
      base::OnceClosure done_closure) = 0;
~~~
 Process any pending Background Sync registrations.

 This involves firing any sync events ready to be fired, and optionally
 scheduling a job to wake up the browser when the next event needs to be
 fired.

### RevivePeriodicBackgroundSyncRegistrations

BackgroundSyncContext::RevivePeriodicBackgroundSyncRegistrations
~~~cpp
virtual void RevivePeriodicBackgroundSyncRegistrations(
      url::Origin origin) = 0;
~~~
 Revives any suspended periodic Background Sync registrations for |origin|.

### UnregisterPeriodicSyncForOrigin

BackgroundSyncContext::UnregisterPeriodicSyncForOrigin
~~~cpp
virtual void UnregisterPeriodicSyncForOrigin(url::Origin origin) = 0;
~~~
 Unregisters any periodic Background Sync registrations for |origin|.
