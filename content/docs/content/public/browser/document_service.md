
## class DocumentService

### DocumentService

DocumentService
~~~cpp
DocumentService(RenderFrameHost& render_frame_host,
                  mojo::PendingReceiver<Interface> pending_receiver)
      : DocumentServiceBase(render_frame_host),
        receiver_(this, std::move(pending_receiver)) {
    // |this| owns |receiver_|, so base::Unretained is safe.
    receiver_.set_disconnect_handler(base::BindOnce(
        [](DocumentService* document_service) {
          document_service->WillBeDestroyed(
              DocumentServiceDestructionReason::kConnectionTerminated);
          document_service->ResetAndDeleteThis();
        },
        base::Unretained(this)));
  }
~~~

### ~DocumentService

~DocumentService
~~~cpp
~DocumentService() override {
    // To avoid potential destruction order issues, implementations must use one
    // of the *AndDeleteThis() methods below instead of writing `delete this`.
    DCHECK(!receiver_.is_bound());
  }
~~~

### ResetAndDeleteThis

ResetAndDeleteThis
~~~cpp
void ResetAndDeleteThis() final {
    receiver_.reset();
    delete this;
  }
~~~
 Subclasses may end their lifetime early by calling this method; `delete
 this` is not permitted for a `DocumentService` and will trigger the
 `DCHECK` in the destructor above.


 If there is a specific reason for self-deletion, one of the following may
 be more appropriate instead:

 - To report a failure when validating inputs received over IPC (e.g. the
   sender is malicious or buggy), use `ReportBadMessageAndDeleteThis()`.


 - Otherwise, to attach a specific numeric code to the `mojo::Receiver`
   reset, which will be passed to the other endpoint's disconnect with
   reason handler (if any), use `ResetWithReasonAndDeleteThis()`.


 The ordering of events is important: by resetting the mojo::Receiver before
 invoking the destructor, any pending Mojo reply callbacks can simply be
 dropped by an interface implementation, without forcing the implementation
 to (pointlessly) first run those reply callbacks.


 Marked final because there should be no real reason for a subclass to
 customize this behavior, and it allows for most `ResetAndDeleteThis()`
 calls to be devirtualized.

### origin

origin
~~~cpp
const url::Origin& origin() const {
    return render_frame_host().GetLastCommittedOrigin();
  }
~~~
 `this` is promptly deleted if `render_frame_host_` commits a cross-document
 navigation, so it is always safe to simply call `GetLastCommittedOrigin()`
 and `GetLastCommittedURL()` directly.

### ReportBadMessageAndDeleteThis

ReportBadMessageAndDeleteThis
~~~cpp
NOT_TAIL_CALLED void ReportBadMessageAndDeleteThis(base::StringPiece error) {
    receiver_.ReportBadMessage(error);
    delete this;
  }
~~~
 Reports a bad message and deletes `this`.


 Prefer over `mojo::ReportBadMessage()`, since using this method avoids the
 need to run any pending reply callbacks with placeholder arguments.

### ResetWithReasonAndDeleteThis

ResetWithReasonAndDeleteThis
~~~cpp
void ResetWithReasonAndDeleteThis(uint32_t reason,
                                    base::StringPiece description) {
    receiver_.ResetWithReason(reason, description);
    delete this;
  }
~~~
 Resets the `mojo::Receiver` with a `reason` and `description` and deletes
 `this`.

### THREAD_CHECKER

DocumentService::THREAD_CHECKER
~~~cpp
THREAD_CHECKER(thread_checker_);
~~~
 Subclasses can use this to check thread safety.

 For example: DCHECK_CALLED_ON_VALID_THREAD(thread_checker_);
### receiver_



~~~cpp

mojo::Receiver<Interface> receiver_;

~~~

 Note: `receiver_` is intentionally not exposed to implementations, since it
 is otherwise easy to write bugs that leak `this` by resetting the receiver
 without deleting `this`.

### DocumentService

DocumentService
~~~cpp
DocumentService(RenderFrameHost& render_frame_host,
                  mojo::PendingReceiver<Interface> pending_receiver)
      : DocumentServiceBase(render_frame_host),
        receiver_(this, std::move(pending_receiver)) {
    // |this| owns |receiver_|, so base::Unretained is safe.
    receiver_.set_disconnect_handler(base::BindOnce(
        [](DocumentService* document_service) {
          document_service->WillBeDestroyed(
              DocumentServiceDestructionReason::kConnectionTerminated);
          document_service->ResetAndDeleteThis();
        },
        base::Unretained(this)));
  }
~~~

### ~DocumentService

~DocumentService
~~~cpp
~DocumentService() override {
    // To avoid potential destruction order issues, implementations must use one
    // of the *AndDeleteThis() methods below instead of writing `delete this`.
    DCHECK(!receiver_.is_bound());
  }
~~~

### ResetAndDeleteThis

ResetAndDeleteThis
~~~cpp
void ResetAndDeleteThis() final {
    receiver_.reset();
    delete this;
  }
~~~
 Subclasses may end their lifetime early by calling this method; `delete
 this` is not permitted for a `DocumentService` and will trigger the
 `DCHECK` in the destructor above.


 If there is a specific reason for self-deletion, one of the following may
 be more appropriate instead:

 - To report a failure when validating inputs received over IPC (e.g. the
   sender is malicious or buggy), use `ReportBadMessageAndDeleteThis()`.


 - Otherwise, to attach a specific numeric code to the `mojo::Receiver`
   reset, which will be passed to the other endpoint's disconnect with
   reason handler (if any), use `ResetWithReasonAndDeleteThis()`.


 The ordering of events is important: by resetting the mojo::Receiver before
 invoking the destructor, any pending Mojo reply callbacks can simply be
 dropped by an interface implementation, without forcing the implementation
 to (pointlessly) first run those reply callbacks.


 Marked final because there should be no real reason for a subclass to
 customize this behavior, and it allows for most `ResetAndDeleteThis()`
 calls to be devirtualized.

### origin

origin
~~~cpp
const url::Origin& origin() const {
    return render_frame_host().GetLastCommittedOrigin();
  }
~~~
 `this` is promptly deleted if `render_frame_host_` commits a cross-document
 navigation, so it is always safe to simply call `GetLastCommittedOrigin()`
 and `GetLastCommittedURL()` directly.

### ReportBadMessageAndDeleteThis

ReportBadMessageAndDeleteThis
~~~cpp
NOT_TAIL_CALLED void ReportBadMessageAndDeleteThis(base::StringPiece error) {
    receiver_.ReportBadMessage(error);
    delete this;
  }
~~~
 Reports a bad message and deletes `this`.


 Prefer over `mojo::ReportBadMessage()`, since using this method avoids the
 need to run any pending reply callbacks with placeholder arguments.

### ResetWithReasonAndDeleteThis

ResetWithReasonAndDeleteThis
~~~cpp
void ResetWithReasonAndDeleteThis(uint32_t reason,
                                    base::StringPiece description) {
    receiver_.ResetWithReason(reason, description);
    delete this;
  }
~~~
 Resets the `mojo::Receiver` with a `reason` and `description` and deletes
 `this`.

### receiver_



~~~cpp

mojo::Receiver<Interface> receiver_;

~~~

 Note: `receiver_` is intentionally not exposed to implementations, since it
 is otherwise easy to write bugs that leak `this` by resetting the receiver
 without deleting `this`.
