
## class ChildProcessHostDelegate
 Interface that all users of ChildProcessHost need to provide.

### ~ChildProcessHostDelegate

~ChildProcessHostDelegate
~~~cpp
~ChildProcessHostDelegate() override {}
~~~

### OnChannelInitialized

OnChannelInitialized
~~~cpp
virtual void OnChannelInitialized(IPC::Channel* channel) {}
~~~
 Called when the IPC channel for the child process is initialized.

### OnChildDisconnected

OnChildDisconnected
~~~cpp
virtual void OnChildDisconnected() {}
~~~
 Called when the child process unexpected closes the IPC channel. Delegates
 would normally delete the object in this case.

### GetProcess

GetProcess
~~~cpp
virtual const base::Process& GetProcess() = 0;
~~~
 Returns a reference to the child process. This can be called only after
 OnProcessLaunched is called or it will be invalid and may crash.

### BindHostReceiver

BindHostReceiver
~~~cpp
virtual void BindHostReceiver(mojo::GenericPendingReceiver receiver) {}
~~~
 Binds an interface receiver in the host process, as requested by the child
 process.

### ~ChildProcessHostDelegate

~ChildProcessHostDelegate
~~~cpp
~ChildProcessHostDelegate() override {}
~~~

### OnChannelInitialized

OnChannelInitialized
~~~cpp
virtual void OnChannelInitialized(IPC::Channel* channel) {}
~~~
 Called when the IPC channel for the child process is initialized.

### OnChildDisconnected

OnChildDisconnected
~~~cpp
virtual void OnChildDisconnected() {}
~~~
 Called when the child process unexpected closes the IPC channel. Delegates
 would normally delete the object in this case.

### GetProcess

GetProcess
~~~cpp
virtual const base::Process& GetProcess() = 0;
~~~
 Returns a reference to the child process. This can be called only after
 OnProcessLaunched is called or it will be invalid and may crash.

### BindHostReceiver

BindHostReceiver
~~~cpp
virtual void BindHostReceiver(mojo::GenericPendingReceiver receiver) {}
~~~
 Binds an interface receiver in the host process, as requested by the child
 process.
