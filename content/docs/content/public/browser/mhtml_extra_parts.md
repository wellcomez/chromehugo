
## class MHTMLExtraParts : public
 Interface class for adding an extra part to MHTML when doing MHTML
 generation.  To use it, get it from the web contents with FromWebContents,
 then call AddExtraMHTMLPart to add a new part.

### AddExtraMHTMLPart

MHTMLExtraParts : public::AddExtraMHTMLPart
~~~cpp
virtual void AddExtraMHTMLPart(const std::string& content_type,
                                 const std::string& content_location,
                                 const std::string& extra_headers,
                                 const std::string& body) = 0;
~~~
 Add an extra MHTML part to the data structure stored by the WebContents.

 This will take care of generating the boundary line.  This will also set
 the content-type and content-location headers to the values provided, and
 use the body provided.

### size

MHTMLExtraParts : public::size
~~~cpp
virtual int64_t size() = 0;
~~~
 Returns the number of extra parts added.
