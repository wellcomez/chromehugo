### ClearSiteData

ClearSiteData
~~~cpp
CONTENT_EXPORT void ClearSiteData(
    const base::RepeatingCallback<BrowserContext*()>& browser_context_getter,
    const url::Origin& origin,
    bool clear_cookies,
    bool clear_storage,
    bool clear_cache,
    const std::set<std::string>& storage_buckets_to_remove,
    bool avoid_closing_connections,
    const absl::optional<net::CookiePartitionKey>& cookie_partition_key,
    const absl::optional<blink::StorageKey>& storage_key,
    bool partitioned_state_allowed_only,
    base::OnceClosure callback);
~~~
 Removes browsing data associated with |origin|. Used when the Clear-Site-Data
 header is sent.

 Has to be called on the UI thread and will execute |callback| on the UI
 thread when done.

