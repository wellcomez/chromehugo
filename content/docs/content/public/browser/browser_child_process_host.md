
## class BrowserChildProcessHost : public
 This represents child processes of the browser process, i.e. plugins. They
 will get terminated at browser shutdown.

### FromID

BrowserChildProcessHost : public::FromID
~~~cpp
static BrowserChildProcessHost* FromID(int child_process_id);
~~~
 Returns the child process host with unique id |child_process_id|, or
 nullptr if it doesn't exist. |child_process_id| is NOT the process ID, but
 is the same unique ID as |ChildProcessData::id|.

### ~BrowserChildProcessHost

~BrowserChildProcessHost
~~~cpp
~BrowserChildProcessHost() override {}
~~~

### Launch

BrowserChildProcessHost : public::Launch
~~~cpp
virtual void Launch(
      std::unique_ptr<SandboxedProcessLauncherDelegate> delegate,
      std::unique_ptr<base::CommandLine> cmd_line,
      bool terminate_on_shutdown) = 0;
~~~
 Derived classes call this to launch the child process asynchronously.

### GetData

BrowserChildProcessHost : public::GetData
~~~cpp
virtual const ChildProcessData& GetData() = 0;
~~~

### GetHost

BrowserChildProcessHost : public::GetHost
~~~cpp
virtual ChildProcessHost* GetHost() = 0;
~~~
 Returns the ChildProcessHost object used by this object.

### GetTerminationInfo

BrowserChildProcessHost : public::GetTerminationInfo
~~~cpp
virtual ChildProcessTerminationInfo GetTerminationInfo(bool known_dead) = 0;
~~~
 Returns the termination info of a child.

 |known_dead| indicates that the child is already dead. On Linux, this
 information is necessary to retrieve accurate information. See
 ChildProcessLauncher::GetChildTerminationInfo() for more details.

### TakeMetricsAllocator

BrowserChildProcessHost : public::TakeMetricsAllocator
~~~cpp
virtual std::unique_ptr<base::PersistentMemoryAllocator>
  TakeMetricsAllocator() = 0;
~~~
 Take ownership of a "shared" metrics allocator (if one exists).

### SetName

BrowserChildProcessHost : public::SetName
~~~cpp
virtual void SetName(const std::u16string& name) = 0;
~~~
 Sets the user-visible name of the process.

### SetMetricsName

BrowserChildProcessHost : public::SetMetricsName
~~~cpp
virtual void SetMetricsName(const std::string& metrics_name) = 0;
~~~
 Sets the name of the process used for metrics reporting.

### SetProcess

BrowserChildProcessHost : public::SetProcess
~~~cpp
virtual void SetProcess(base::Process process) = 0;
~~~
 Set the process. BrowserChildProcessHost will do this when the Launch
 method is used to start the process. However if the owner of this object
 doesn't call Launch and starts the process in another way, they need to
 call this method so that the process is associated with this object.

### GetPortProvider

BrowserChildProcessHost : public::GetPortProvider
~~~cpp
static base::PortProvider* GetPortProvider();
~~~
 Returns a PortProvider used to get the task port for child processes.

### InterceptBindHostReceiverForTesting

BrowserChildProcessHost : public::InterceptBindHostReceiverForTesting
~~~cpp
static void InterceptBindHostReceiverForTesting(
      BindHostReceiverInterceptor callback);
~~~
