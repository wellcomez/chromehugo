
## class MediaObserver
 An embedder may implement MediaObserver and return it from
 ContentBrowserClient to receive callbacks as media events occur.

### OnAudioCaptureDevicesChanged

OnAudioCaptureDevicesChanged
~~~cpp
virtual void OnAudioCaptureDevicesChanged() = 0;
~~~
 Called when a audio capture device is plugged in or unplugged.

### OnVideoCaptureDevicesChanged

OnVideoCaptureDevicesChanged
~~~cpp
virtual void OnVideoCaptureDevicesChanged() = 0;
~~~
 Called when a video capture device is plugged in or unplugged.

### OnMediaRequestStateChanged

OnMediaRequestStateChanged
~~~cpp
virtual void OnMediaRequestStateChanged(
      int render_process_id,
      int render_frame_id,
      int page_request_id,
      const GURL& security_origin,
      blink::mojom::MediaStreamType stream_type,
      MediaRequestState state) = 0;
~~~
 Called when a media request changes state.

### OnCreatingAudioStream

OnCreatingAudioStream
~~~cpp
virtual void OnCreatingAudioStream(int render_process_id,
                                     int render_frame_id) = 0;
~~~
 Called when an audio stream is being created.

### OnSetCapturingLinkSecured

OnSetCapturingLinkSecured
~~~cpp
virtual void OnSetCapturingLinkSecured(
      int render_process_id,
      int render_frame_id,
      int page_request_id,
      blink::mojom::MediaStreamType stream_type,
      bool is_secure) = 0;
~~~
 Called when the secure display link status of one or more consumers of this
 media stream has changed.

### ~MediaObserver

~MediaObserver
~~~cpp
virtual ~MediaObserver() {}
~~~

### OnAudioCaptureDevicesChanged

OnAudioCaptureDevicesChanged
~~~cpp
virtual void OnAudioCaptureDevicesChanged() = 0;
~~~
 Called when a audio capture device is plugged in or unplugged.

### OnVideoCaptureDevicesChanged

OnVideoCaptureDevicesChanged
~~~cpp
virtual void OnVideoCaptureDevicesChanged() = 0;
~~~
 Called when a video capture device is plugged in or unplugged.

### OnMediaRequestStateChanged

OnMediaRequestStateChanged
~~~cpp
virtual void OnMediaRequestStateChanged(
      int render_process_id,
      int render_frame_id,
      int page_request_id,
      const GURL& security_origin,
      blink::mojom::MediaStreamType stream_type,
      MediaRequestState state) = 0;
~~~
 Called when a media request changes state.

### OnCreatingAudioStream

OnCreatingAudioStream
~~~cpp
virtual void OnCreatingAudioStream(int render_process_id,
                                     int render_frame_id) = 0;
~~~
 Called when an audio stream is being created.

### OnSetCapturingLinkSecured

OnSetCapturingLinkSecured
~~~cpp
virtual void OnSetCapturingLinkSecured(
      int render_process_id,
      int render_frame_id,
      int page_request_id,
      blink::mojom::MediaStreamType stream_type,
      bool is_secure) = 0;
~~~
 Called when the secure display link status of one or more consumers of this
 media stream has changed.

### ~MediaObserver

~MediaObserver
~~~cpp
virtual ~MediaObserver() {}
~~~
