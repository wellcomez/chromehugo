
## struct MediaPlayerId

### CreateMediaPlayerIdForTests

MediaPlayerId::CreateMediaPlayerIdForTests
~~~cpp
static MediaPlayerId CreateMediaPlayerIdForTests();
~~~

### MediaPlayerId

MediaPlayerId
~~~cpp
MediaPlayerId() = delete;
~~~

### MediaPlayerId

MediaPlayerId::MediaPlayerId
~~~cpp
MediaPlayerId(GlobalRenderFrameHostId routing_id, int delegate_id)
~~~

### operator==

MediaPlayerId::operator==
~~~cpp
bool operator==(const MediaPlayerId&) const;
~~~

### operator!=

MediaPlayerId::operator!=
~~~cpp
bool operator!=(const MediaPlayerId&) const;
~~~

### operator&lt;

MediaPlayerId::operator&lt;
~~~cpp
bool operator<(const MediaPlayerId&) const;
~~~
