### scoped_refptr&lt;TraceDataEndpoint&gt; CreateStringEndpoint

scoped_refptr&lt;TraceDataEndpoint&gt; CreateStringEndpoint
~~~cpp
CONTENT_EXPORT static scoped_refptr<TraceDataEndpoint> CreateStringEndpoint(
      CompletionCallback callback);
~~~

### scoped_refptr&lt;TraceDataEndpoint&gt; CreateFileEndpoint

scoped_refptr&lt;TraceDataEndpoint&gt; CreateFileEndpoint
~~~cpp
CONTENT_EXPORT static scoped_refptr<TraceDataEndpoint> CreateFileEndpoint(
      const base::FilePath& file_path,
      base::OnceClosure callback,
      base::TaskPriority write_priority = base::TaskPriority::BEST_EFFORT);
~~~
 Create a trace endpoint that may be supplied to StopTracing
 to dump the trace data to a file.

### GetCategories

GetCategories
~~~cpp
virtual bool GetCategories(GetCategoriesDoneCallback callback) = 0;
~~~

### StartTracing

StartTracing
~~~cpp
virtual bool StartTracing(const base::trace_event::TraceConfig& trace_config,
                            StartTracingDoneCallback callback) = 0;
~~~

### StopTracing

StopTracing
~~~cpp
virtual bool StopTracing(
      const scoped_refptr<TraceDataEndpoint>& trace_data_endpoint) = 0;
~~~
 Stop tracing (recording traces) on all processes.


 Child processes typically are caching trace data and only rarely flush
 and send trace data back to the browser process. That is because it may be
 an expensive operation to send the trace data over IPC, and we would like
 to avoid much runtime overhead of tracing. So, to end tracing, we must
 asynchronously ask all child processes to flush any pending trace data.


 Once all child processes have acked to the StopTracing request,
 TracingFileResultCallback will be called back with a file that contains
 the traced data.


 If |trace_data_endpoint| is not null, it will receive chunks of trace data
 as JSON-stringified events, followed by a notification that the trace
 collection is finished.


### StopTracing

StopTracing
~~~cpp
virtual bool StopTracing(
      const scoped_refptr<TraceDataEndpoint>& trace_data_endpoint,
      const std::string& agent_label,
      bool privacy_filtering_enabled = false) = 0;
~~~

### GetTraceBufferUsage

GetTraceBufferUsage
~~~cpp
virtual bool GetTraceBufferUsage(GetTraceBufferUsageCallback callback) = 0;
~~~

### IsTracing

IsTracing
~~~cpp
virtual bool IsTracing() = 0;
~~~
 Check if the tracing system is tracing
## class TracingController
 TracingController is used on the browser processes to enable/disable
 tracing status and collect trace data. Only the browser UI thread is allowed
 to interact with the TracingController object. All callbacks are called on
 the UI thread.

### GetInstance

TracingController::GetInstance
~~~cpp
CONTENT_EXPORT static TracingController* GetInstance();
~~~

###  ReceiveTraceChunk

 An interface for trace data consumer. An implementation of this interface
 is passed to StopTracing() and receives the trace data followed by a
 notification that all child processes have completed tracing and the data
 collection is over. All methods are called on the UI thread.

 ReceiveTraceFinalContents method will be called exactly once and no methods
 will be called after that.

~~~cpp
class CONTENT_EXPORT TraceDataEndpoint
      : public base::RefCountedThreadSafe<TraceDataEndpoint> {
   public:
    virtual void ReceiveTraceChunk(std::unique_ptr<std::string> chunk) = 0;
~~~
### ReceivedTraceFinalContents

ReceivedTraceFinalContents
~~~cpp
virtual void ReceivedTraceFinalContents() = 0;
~~~

### ~TraceDataEndpoint

~TraceDataEndpoint
~~~cpp
virtual ~TraceDataEndpoint() {}
~~~

### ReceivedTraceFinalContents

ReceivedTraceFinalContents
~~~cpp
virtual void ReceivedTraceFinalContents() = 0;
~~~

### ~TraceDataEndpoint

~TraceDataEndpoint
~~~cpp
virtual ~TraceDataEndpoint() {}
~~~

### GetInstance

TracingController::GetInstance
~~~cpp
CONTENT_EXPORT static TracingController* GetInstance();
~~~

###  ReceiveTraceChunk

 An interface for trace data consumer. An implementation of this interface
 is passed to StopTracing() and receives the trace data followed by a
 notification that all child processes have completed tracing and the data
 collection is over. All methods are called on the UI thread.

 ReceiveTraceFinalContents method will be called exactly once and no methods
 will be called after that.

~~~cpp
class CONTENT_EXPORT TraceDataEndpoint
      : public base::RefCountedThreadSafe<TraceDataEndpoint> {
   public:
    virtual void ReceiveTraceChunk(std::unique_ptr<std::string> chunk) = 0;
~~~