### InstallabilityError

InstallabilityError
~~~cpp
InstallabilityError(std::string error_id)
~~~

### InstallabilityError

InstallabilityError
~~~cpp
InstallabilityError(const InstallabilityError& other)
~~~

### InstallabilityError

InstallabilityError
~~~cpp
InstallabilityError(InstallabilityError&& other)
~~~

### operator==

operator==
~~~cpp
operator==(const InstallabilityError& other) const
~~~

### ~InstallabilityError

~InstallabilityError
~~~cpp
~InstallabilityError()
~~~

### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
operator<<(std::ostream& os,
                                        const InstallabilityError& error)
~~~

## struct InstallabilityErrorArgument

### InstallabilityErrorArgument

InstallabilityErrorArgument
~~~cpp
InstallabilityErrorArgument() = default;
~~~

### InstallabilityErrorArgument

InstallabilityErrorArgument::InstallabilityErrorArgument
~~~cpp
InstallabilityErrorArgument(std::string name, std::string value)
~~~

### operator==

InstallabilityErrorArgument::operator==
~~~cpp
bool operator==(const InstallabilityErrorArgument& other) const;
~~~

### ~InstallabilityErrorArgument

InstallabilityErrorArgument::~InstallabilityErrorArgument
~~~cpp
~InstallabilityErrorArgument()
~~~
