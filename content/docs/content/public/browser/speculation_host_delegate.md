
## class SpeculationHostDelegate
 Allow embedders to handle speculation candidates with their own strategies.

 See third_party/blink/renderer/core/speculation_rules/README.md for more
 context.

### ProcessCandidates

SpeculationHostDelegate::ProcessCandidates
~~~cpp
virtual void ProcessCandidates(
      std::vector<blink::mojom::SpeculationCandidatePtr>& candidates) = 0;
~~~
 Called when the caller has encountered the given speculation candidates
 and gives this delegate a chance to take action on them. The caller may
 take action on `candidates` after this function returns. Therefore, the
 delegate should remove elements that it decided to take an action on.
