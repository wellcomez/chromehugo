### std::string GetHMACForMediaDeviceID

std::string GetHMACForMediaDeviceID
~~~cpp
CONTENT_EXPORT std::string GetHMACForMediaDeviceID(
    const std::string& salt,
    const url::Origin& security_origin,
    const std::string& raw_unique_id);
~~~
 Generates a one-way hash of a device's unique ID usable by one
 particular security origin.

### DoesMediaDeviceIDMatchHMAC

DoesMediaDeviceIDMatchHMAC
~~~cpp
CONTENT_EXPORT bool DoesMediaDeviceIDMatchHMAC(
    const std::string& salt,
    const url::Origin& security_origin,
    const std::string& device_guid,
    const std::string& raw_unique_id);
~~~
 Convenience method to check if |device_guid| is an HMAC of
 |raw_device_id| for |security_origin|.

### GetMediaDeviceIDForHMAC

GetMediaDeviceIDForHMAC
~~~cpp
CONTENT_EXPORT void GetMediaDeviceIDForHMAC(
    blink::mojom::MediaStreamType stream_type,
    std::string salt,
    url::Origin security_origin,
    std::string hmac_device_id,
    scoped_refptr<base::SequencedTaskRunner> task_runner,
    base::OnceCallback<void(const absl::optional<std::string>&)> callback);
~~~
 Returns the raw device ID for the given HMAC |hmac_device_id| for the given
 |security_origin| and |salt|. The result is passed via |callback| on the
 task runner where this function is called. If |hmac_device_id| is not a
 valid device ID nullopt is returned.

 The |callback| will be posted on the given |task_runner|.

### IsValidDeviceId

IsValidDeviceId
~~~cpp
CONTENT_EXPORT bool IsValidDeviceId(const std::string& device_id);
~~~

