
## class ScreenOrientationDelegate
 Can be implemented to provide platform specific functionality for
 ScreenOrientationProvider.

### ScreenOrientationDelegate

ScreenOrientationDelegate
~~~cpp
ScreenOrientationDelegate(const ScreenOrientationDelegate&) = delete;
~~~

### operator=

ScreenOrientationDelegate::operator=
~~~cpp
ScreenOrientationDelegate& operator=(const ScreenOrientationDelegate&) =
      delete;
~~~

### ~ScreenOrientationDelegate

~ScreenOrientationDelegate
~~~cpp
virtual ~ScreenOrientationDelegate() {}
~~~

### FullScreenRequired

ScreenOrientationDelegate::FullScreenRequired
~~~cpp
virtual bool FullScreenRequired(WebContents* web_contents) = 0;
~~~
 Returns true if the provided `web_contents` must be fullscreen in order for
 ScreenOrientationProvider to respond to requests.

### Lock

ScreenOrientationDelegate::Lock
~~~cpp
virtual void Lock(
      WebContents* web_contents,
      device::mojom::ScreenOrientationLockType lock_orientation) = 0;
~~~
 Lock the display with the provided `web_contents` to the given orientation.

### ScreenOrientationProviderSupported

ScreenOrientationDelegate::ScreenOrientationProviderSupported
~~~cpp
virtual bool ScreenOrientationProviderSupported(
      WebContents* web_contents) = 0;
~~~
 Returns true if `Lock()` above can be called for the specified
 `web_contents`.

### Unlock

ScreenOrientationDelegate::Unlock
~~~cpp
virtual void Unlock(WebContents* web_contents) = 0;
~~~
 Unlocks the display with the provided `web_contents`, allowing hardware
 rotation to resume.
