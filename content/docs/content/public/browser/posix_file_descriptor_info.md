
## class PosixFileDescriptorInfo
 PoxisFileDescriptorInfo is a collection of file descriptors which is needed
 to launch a process. You should tell PosixFileDescriptorInfo which FDs
 should be closed and which shouldn't so that it can take care of the
 lifetime of FDs.


 See base/process/launcher.h for more details about launching a process.

### ~PosixFileDescriptorInfo

~PosixFileDescriptorInfo
~~~cpp
virtual ~PosixFileDescriptorInfo() {}
~~~

### Share

Share
~~~cpp
virtual void Share(int id, base::PlatformFile fd) = 0;
~~~
 Adds an FD associated with an ID, without delegating the ownerhip of ID.

### ShareWithRegion

ShareWithRegion
~~~cpp
virtual void ShareWithRegion(
      int id,
      base::PlatformFile fd,
      const base::MemoryMappedFile::Region& region) = 0;
~~~
 Similar to Share but also provides a region in that file that should be
 read in the launched process (accessible with GetRegionAt()).

### Transfer

Transfer
~~~cpp
virtual void Transfer(int id, base::ScopedFD fd) = 0;
~~~
 Adds an FD associated with an ID, passing the FD ownership to
 FileDescriptorInfo.

### GetMapping

GetMapping
~~~cpp
virtual const base::FileHandleMappingVector& GetMapping() = 0;
~~~
 A vector backed map of registered ID-FD pairs.

### GetMappingWithIDAdjustment

GetMappingWithIDAdjustment
~~~cpp
virtual base::FileHandleMappingVector GetMappingWithIDAdjustment(
      int delta) = 0;
~~~
 A GetMapping() variant that adjusts the ID value by |delta|.

 Some environments need this trick.

### GetFDAt

GetFDAt
~~~cpp
virtual base::PlatformFile GetFDAt(size_t i) = 0;
~~~
 API for iterating over the registered ID-FD pairs.

### GetIDAt

GetIDAt
~~~cpp
virtual int GetIDAt(size_t i) = 0;
~~~

### GetRegionAt

GetRegionAt
~~~cpp
virtual const base::MemoryMappedFile::Region& GetRegionAt(size_t i) = 0;
~~~

### GetMappingSize

GetMappingSize
~~~cpp
virtual size_t GetMappingSize() = 0;
~~~

### OwnsFD

OwnsFD
~~~cpp
virtual bool OwnsFD(base::PlatformFile file) = 0;
~~~
 Returns true if |this| has ownership of |file|.

### ReleaseFD

ReleaseFD
~~~cpp
virtual base::ScopedFD ReleaseFD(base::PlatformFile file) = 0;
~~~
 Assuming |OwnsFD(file)|, releases the ownership.

### ~PosixFileDescriptorInfo

~PosixFileDescriptorInfo
~~~cpp
virtual ~PosixFileDescriptorInfo() {}
~~~

### Share

Share
~~~cpp
virtual void Share(int id, base::PlatformFile fd) = 0;
~~~
 Adds an FD associated with an ID, without delegating the ownerhip of ID.

### ShareWithRegion

ShareWithRegion
~~~cpp
virtual void ShareWithRegion(
      int id,
      base::PlatformFile fd,
      const base::MemoryMappedFile::Region& region) = 0;
~~~
 Similar to Share but also provides a region in that file that should be
 read in the launched process (accessible with GetRegionAt()).

### Transfer

Transfer
~~~cpp
virtual void Transfer(int id, base::ScopedFD fd) = 0;
~~~
 Adds an FD associated with an ID, passing the FD ownership to
 FileDescriptorInfo.

### GetMapping

GetMapping
~~~cpp
virtual const base::FileHandleMappingVector& GetMapping() = 0;
~~~
 A vector backed map of registered ID-FD pairs.

### GetMappingWithIDAdjustment

GetMappingWithIDAdjustment
~~~cpp
virtual base::FileHandleMappingVector GetMappingWithIDAdjustment(
      int delta) = 0;
~~~
 A GetMapping() variant that adjusts the ID value by |delta|.

 Some environments need this trick.

### GetFDAt

GetFDAt
~~~cpp
virtual base::PlatformFile GetFDAt(size_t i) = 0;
~~~
 API for iterating over the registered ID-FD pairs.

### GetIDAt

GetIDAt
~~~cpp
virtual int GetIDAt(size_t i) = 0;
~~~

### GetRegionAt

GetRegionAt
~~~cpp
virtual const base::MemoryMappedFile::Region& GetRegionAt(size_t i) = 0;
~~~

### GetMappingSize

GetMappingSize
~~~cpp
virtual size_t GetMappingSize() = 0;
~~~

### OwnsFD

OwnsFD
~~~cpp
virtual bool OwnsFD(base::PlatformFile file) = 0;
~~~
 Returns true if |this| has ownership of |file|.

### ReleaseFD

ReleaseFD
~~~cpp
virtual base::ScopedFD ReleaseFD(base::PlatformFile file) = 0;
~~~
 Assuming |OwnsFD(file)|, releases the ownership.
