
## class HostZoomMap
 Maps hostnames to custom zoom levels.  Written on the UI thread and read on
 any thread.  One instance per browser context. Must be created on the UI
 thread, and it'll delete itself on the UI thread as well.

 Zoom can be defined at three levels: default zoom, zoom for host, and zoom
 for host with specific scheme. Setting any of the levels leaves settings
 for other settings intact. Getting the zoom level starts at the most
 specific setting and progresses to the less specific: first the zoom for the
 host and scheme pair is checked, secondly the zoom for the host only and
 lastly default zoom.

### enum ZoomLevelChangeMode

~~~cpp
enum ZoomLevelChangeMode {
    ZOOM_CHANGED_FOR_HOST,             // Zoom level changed for host.
    ZOOM_CHANGED_FOR_SCHEME_AND_HOST,  // Zoom level changed for scheme/host
                                       // pair.
    ZOOM_CHANGED_TEMPORARY_ZOOM,       // Temporary zoom change for specific
                                       // renderer, no scheme/host is specified.
  }
~~~
### field error



~~~cpp

struct ZoomLevelChange {
    ZoomLevelChangeMode mode;
    std::string host;
    std::string scheme;
    double zoom_level;
    base::Time last_modified;
  };

~~~

 Structure used to notify about zoom changes. Host and/or scheme are empty
 if not applicable to |mode|.

### GetURLFromEntry

HostZoomMap::GetURLFromEntry
~~~cpp
CONTENT_EXPORT static GURL GetURLFromEntry(NavigationEntry* entry);
~~~
 Extracts the URL from NavigationEntry, substituting the error page
 URL in the event that the error page is showing.

### GetDefaultForBrowserContext

HostZoomMap::GetDefaultForBrowserContext
~~~cpp
CONTENT_EXPORT static HostZoomMap* GetDefaultForBrowserContext(
      BrowserContext* browser_context);
~~~

### Get

HostZoomMap::Get
~~~cpp
CONTENT_EXPORT static HostZoomMap* Get(SiteInstance* instance);
~~~
 Returns the HostZoomMap associated with this SiteInstance. The SiteInstance
 may serve multiple WebContents, and the HostZoomMap is the same for all of
 these WebContents.

### GetForWebContents

HostZoomMap::GetForWebContents
~~~cpp
CONTENT_EXPORT static HostZoomMap* GetForWebContents(WebContents* contents);
~~~
 Returns the HostZoomMap associated with this WebContent's main frame. If
 multiple WebContents share the same SiteInstance, then they share a single
 HostZoomMap.

### GetZoomLevel

HostZoomMap::GetZoomLevel
~~~cpp
CONTENT_EXPORT static double GetZoomLevel(WebContents* web_contents);
~~~
 Returns the current zoom level for the specified WebContents. May be
 temporary or host-specific.

### SetZoomLevel

HostZoomMap::SetZoomLevel
~~~cpp
CONTENT_EXPORT static void SetZoomLevel(WebContents* web_contents,
                                          double level);
~~~
 Sets the current zoom level for the specified WebContents. The level may
 be temporary or host-specific depending on the particular WebContents.

### SendErrorPageZoomLevelRefresh

HostZoomMap::SendErrorPageZoomLevelRefresh
~~~cpp
CONTENT_EXPORT static void SendErrorPageZoomLevelRefresh(
      WebContents* web_contents);
~~~
 Send an IPC to refresh any displayed error page's zoom levels. Needs to
 be called since error pages don't get loaded via the normal channel.

### CopyFrom

CopyFrom
~~~cpp
virtual void CopyFrom(HostZoomMap* copy) = 0;
~~~
 Copy the zoom levels from the given map. Can only be called on the UI
 thread.

### GetZoomLevelForHostAndScheme

GetZoomLevelForHostAndScheme
~~~cpp
virtual double GetZoomLevelForHostAndScheme(const std::string& scheme,
                                              const std::string& host) = 0;
~~~
 Here |host| is the host portion of URL, or (in the absence of a host)
 the complete spec of the URL.

 Returns the zoom for the specified |scheme| and |host|. See class
 description for details.


 This may be called on any thread.

### HasZoomLevel

HasZoomLevel
~~~cpp
virtual bool HasZoomLevel(const std::string& scheme,
                            const std::string& host) = 0;
~~~
 Returns true if the specified |scheme| and/or |host| has a zoom level
 currently set.


 This may be called on any thread.

### GetAllZoomLevels

GetAllZoomLevels
~~~cpp
virtual ZoomLevelVector GetAllZoomLevels() = 0;
~~~
 Returns all non-temporary zoom levels. Can be called on any thread.

### SetZoomLevelForHost

SetZoomLevelForHost
~~~cpp
virtual void SetZoomLevelForHost(const std::string& host, double level) = 0;
~~~
 Here |host| is the host portion of URL, or (in the absence of a host)
 the complete spec of the URL.

 Sets the zoom level for the |host| to |level|.  If the level matches the
 current default zoom level, the host is erased from the saved preferences;
 otherwise the new value is written out.

 Zoom levels specified for both scheme and host are not affected.


 This should only be called on the UI thread.

### InitializeZoomLevelForHost

InitializeZoomLevelForHost
~~~cpp
virtual void InitializeZoomLevelForHost(const std::string& host,
                                          double level,
                                          base::Time last_modified) = 0;
~~~
 Sets the zoom level for the |host| to |level| with a given |last_modified|
 timestamp. Should only be used for initialization.

### SetZoomLevelForHostAndScheme

SetZoomLevelForHostAndScheme
~~~cpp
virtual void SetZoomLevelForHostAndScheme(const std::string& scheme,
                                            const std::string& host,
                                            double level) = 0;
~~~
 Here |host| is the host portion of URL, or (in the absence of a host)
 the complete spec of the URL.

 Sets the zoom level for the |scheme|/|host| pair to |level|. No values
 will be erased during this operation, and this value will not be stored in
 the preferences.


 This should only be called on the UI thread.

### UsesTemporaryZoomLevel

UsesTemporaryZoomLevel
~~~cpp
virtual bool UsesTemporaryZoomLevel(
      const GlobalRenderFrameHostId& rfh_id) = 0;
~~~
 Returns whether the frame manages its zoom level independently of other
 frames from the same host.

### SetTemporaryZoomLevel

SetTemporaryZoomLevel
~~~cpp
virtual void SetTemporaryZoomLevel(const GlobalRenderFrameHostId& rfh_id,
                                     double level) = 0;
~~~
 Sets the temporary zoom level that's only valid for the lifetime of this
 RenderFrameHost.


 This should only be called on the UI thread.

### ClearZoomLevels

ClearZoomLevels
~~~cpp
virtual void ClearZoomLevels(base::Time delete_begin,
                               base::Time delete_end) = 0;
~~~
 Clear zoom levels with a modification date greater than or equal
 to |delete_begin| and less than |delete_end|. If |delete_end| is null,
 all entries after |delete_begin| will be deleted.

### ClearTemporaryZoomLevel

ClearTemporaryZoomLevel
~~~cpp
virtual void ClearTemporaryZoomLevel(
      const GlobalRenderFrameHostId& rfh_id) = 0;
~~~
 Clears the temporary zoom level stored for this RenderFrameHost.


 This should only be called on the UI thread.

### GetDefaultZoomLevel

GetDefaultZoomLevel
~~~cpp
virtual double GetDefaultZoomLevel() = 0;
~~~
 Get/Set the default zoom level for pages that don't override it.

### SetDefaultZoomLevel

SetDefaultZoomLevel
~~~cpp
virtual void SetDefaultZoomLevel(double level) = 0;
~~~

### AddZoomLevelChangedCallback

AddZoomLevelChangedCallback
~~~cpp
virtual base::CallbackListSubscription AddZoomLevelChangedCallback(
      ZoomLevelChangedCallback callback) = 0;
~~~
 Add and remove zoom level changed callbacks.

### SetClockForTesting

SetClockForTesting
~~~cpp
virtual void SetClockForTesting(base::Clock* clock) = 0;
~~~

### SetDefaultZoomLevelPrefCallback

SetDefaultZoomLevelPrefCallback
~~~cpp
virtual void SetDefaultZoomLevelPrefCallback(
      DefaultZoomChangedCallback callback) = 0;
~~~

### GetZoomLevelForHostAndScheme

GetZoomLevelForHostAndScheme
~~~cpp
virtual double GetZoomLevelForHostAndScheme(
      const std::string& scheme,
      const std::string& host,
      bool is_overriding_user_agent) = 0;
~~~
 TODO(crbug.com/1424904): Make an Android-specific impl of host_zoom_map, or
                          combine method with GetZoomLevelForHostAndScheme.

### ~HostZoomMap

~HostZoomMap
~~~cpp
virtual ~HostZoomMap() {}
~~~

### CopyFrom

CopyFrom
~~~cpp
virtual void CopyFrom(HostZoomMap* copy) = 0;
~~~
 Copy the zoom levels from the given map. Can only be called on the UI
 thread.

### GetZoomLevelForHostAndScheme

GetZoomLevelForHostAndScheme
~~~cpp
virtual double GetZoomLevelForHostAndScheme(const std::string& scheme,
                                              const std::string& host) = 0;
~~~
 Here |host| is the host portion of URL, or (in the absence of a host)
 the complete spec of the URL.

 Returns the zoom for the specified |scheme| and |host|. See class
 description for details.


 This may be called on any thread.

### HasZoomLevel

HasZoomLevel
~~~cpp
virtual bool HasZoomLevel(const std::string& scheme,
                            const std::string& host) = 0;
~~~
 Returns true if the specified |scheme| and/or |host| has a zoom level
 currently set.


 This may be called on any thread.

### GetAllZoomLevels

GetAllZoomLevels
~~~cpp
virtual ZoomLevelVector GetAllZoomLevels() = 0;
~~~
 Returns all non-temporary zoom levels. Can be called on any thread.

### SetZoomLevelForHost

SetZoomLevelForHost
~~~cpp
virtual void SetZoomLevelForHost(const std::string& host, double level) = 0;
~~~
 Here |host| is the host portion of URL, or (in the absence of a host)
 the complete spec of the URL.

 Sets the zoom level for the |host| to |level|.  If the level matches the
 current default zoom level, the host is erased from the saved preferences;
 otherwise the new value is written out.

 Zoom levels specified for both scheme and host are not affected.


 This should only be called on the UI thread.

### InitializeZoomLevelForHost

InitializeZoomLevelForHost
~~~cpp
virtual void InitializeZoomLevelForHost(const std::string& host,
                                          double level,
                                          base::Time last_modified) = 0;
~~~
 Sets the zoom level for the |host| to |level| with a given |last_modified|
 timestamp. Should only be used for initialization.

### SetZoomLevelForHostAndScheme

SetZoomLevelForHostAndScheme
~~~cpp
virtual void SetZoomLevelForHostAndScheme(const std::string& scheme,
                                            const std::string& host,
                                            double level) = 0;
~~~
 Here |host| is the host portion of URL, or (in the absence of a host)
 the complete spec of the URL.

 Sets the zoom level for the |scheme|/|host| pair to |level|. No values
 will be erased during this operation, and this value will not be stored in
 the preferences.


 This should only be called on the UI thread.

### UsesTemporaryZoomLevel

UsesTemporaryZoomLevel
~~~cpp
virtual bool UsesTemporaryZoomLevel(
      const GlobalRenderFrameHostId& rfh_id) = 0;
~~~
 Returns whether the frame manages its zoom level independently of other
 frames from the same host.

### SetTemporaryZoomLevel

SetTemporaryZoomLevel
~~~cpp
virtual void SetTemporaryZoomLevel(const GlobalRenderFrameHostId& rfh_id,
                                     double level) = 0;
~~~
 Sets the temporary zoom level that's only valid for the lifetime of this
 RenderFrameHost.


 This should only be called on the UI thread.

### ClearZoomLevels

ClearZoomLevels
~~~cpp
virtual void ClearZoomLevels(base::Time delete_begin,
                               base::Time delete_end) = 0;
~~~
 Clear zoom levels with a modification date greater than or equal
 to |delete_begin| and less than |delete_end|. If |delete_end| is null,
 all entries after |delete_begin| will be deleted.

### ClearTemporaryZoomLevel

ClearTemporaryZoomLevel
~~~cpp
virtual void ClearTemporaryZoomLevel(
      const GlobalRenderFrameHostId& rfh_id) = 0;
~~~
 Clears the temporary zoom level stored for this RenderFrameHost.


 This should only be called on the UI thread.

### GetDefaultZoomLevel

GetDefaultZoomLevel
~~~cpp
virtual double GetDefaultZoomLevel() = 0;
~~~
 Get/Set the default zoom level for pages that don't override it.

### SetDefaultZoomLevel

SetDefaultZoomLevel
~~~cpp
virtual void SetDefaultZoomLevel(double level) = 0;
~~~

### AddZoomLevelChangedCallback

AddZoomLevelChangedCallback
~~~cpp
virtual base::CallbackListSubscription AddZoomLevelChangedCallback(
      ZoomLevelChangedCallback callback) = 0;
~~~
 Add and remove zoom level changed callbacks.

### SetClockForTesting

SetClockForTesting
~~~cpp
virtual void SetClockForTesting(base::Clock* clock) = 0;
~~~

### ~HostZoomMap

~HostZoomMap
~~~cpp
virtual ~HostZoomMap() {}
~~~

### enum ZoomLevelChangeMode
 Enum that indicates what was the scope of zoom level change.

~~~cpp
enum ZoomLevelChangeMode {
    ZOOM_CHANGED_FOR_HOST,             // Zoom level changed for host.
    ZOOM_CHANGED_FOR_SCHEME_AND_HOST,  // Zoom level changed for scheme/host
                                       // pair.
    ZOOM_CHANGED_TEMPORARY_ZOOM,       // Temporary zoom change for specific
                                       // renderer, no scheme/host is specified.
  };
~~~
### field error



~~~cpp

struct ZoomLevelChange {
    ZoomLevelChangeMode mode;
    std::string host;
    std::string scheme;
    double zoom_level;
    base::Time last_modified;
  };

~~~

 Structure used to notify about zoom changes. Host and/or scheme are empty
 if not applicable to |mode|.

### GetURLFromEntry

HostZoomMap::GetURLFromEntry
~~~cpp
CONTENT_EXPORT static GURL GetURLFromEntry(NavigationEntry* entry);
~~~
 Extracts the URL from NavigationEntry, substituting the error page
 URL in the event that the error page is showing.

### GetDefaultForBrowserContext

HostZoomMap::GetDefaultForBrowserContext
~~~cpp
CONTENT_EXPORT static HostZoomMap* GetDefaultForBrowserContext(
      BrowserContext* browser_context);
~~~

### Get

HostZoomMap::Get
~~~cpp
CONTENT_EXPORT static HostZoomMap* Get(SiteInstance* instance);
~~~
 Returns the HostZoomMap associated with this SiteInstance. The SiteInstance
 may serve multiple WebContents, and the HostZoomMap is the same for all of
 these WebContents.

### GetForWebContents

HostZoomMap::GetForWebContents
~~~cpp
CONTENT_EXPORT static HostZoomMap* GetForWebContents(WebContents* contents);
~~~
 Returns the HostZoomMap associated with this WebContent's main frame. If
 multiple WebContents share the same SiteInstance, then they share a single
 HostZoomMap.

### GetZoomLevel

HostZoomMap::GetZoomLevel
~~~cpp
CONTENT_EXPORT static double GetZoomLevel(WebContents* web_contents);
~~~
 Returns the current zoom level for the specified WebContents. May be
 temporary or host-specific.

### SetZoomLevel

HostZoomMap::SetZoomLevel
~~~cpp
CONTENT_EXPORT static void SetZoomLevel(WebContents* web_contents,
                                          double level);
~~~
 Sets the current zoom level for the specified WebContents. The level may
 be temporary or host-specific depending on the particular WebContents.

### SendErrorPageZoomLevelRefresh

HostZoomMap::SendErrorPageZoomLevelRefresh
~~~cpp
CONTENT_EXPORT static void SendErrorPageZoomLevelRefresh(
      WebContents* web_contents);
~~~
 Send an IPC to refresh any displayed error page's zoom levels. Needs to
 be called since error pages don't get loaded via the normal channel.
