
## class DevToolsSocketFactory
 Factory of net::ServerSocket. This is to separate instantiating dev tools
 and instantiating server sockets.

 All methods including destructor are called on a separate thread
 different from any BrowserThread instance.

### ~DevToolsSocketFactory

~DevToolsSocketFactory
~~~cpp
virtual ~DevToolsSocketFactory() {}
~~~

### CreateForHttpServer

CreateForHttpServer
~~~cpp
virtual std::unique_ptr<net::ServerSocket> CreateForHttpServer() = 0;
~~~
 Returns a new instance of ServerSocket or nullptr if an error occurred.

### CreateForTethering

CreateForTethering
~~~cpp
virtual std::unique_ptr<net::ServerSocket> CreateForTethering(
      std::string* out_name) = 0;
~~~
 Creates a named socket for reversed tethering implementation (used with
 remote debugging, primarily for mobile).

### ~DevToolsSocketFactory

~DevToolsSocketFactory
~~~cpp
virtual ~DevToolsSocketFactory() {}
~~~

### CreateForHttpServer

CreateForHttpServer
~~~cpp
virtual std::unique_ptr<net::ServerSocket> CreateForHttpServer() = 0;
~~~
 Returns a new instance of ServerSocket or nullptr if an error occurred.

### CreateForTethering

CreateForTethering
~~~cpp
virtual std::unique_ptr<net::ServerSocket> CreateForTethering(
      std::string* out_name) = 0;
~~~
 Creates a named socket for reversed tethering implementation (used with
 remote debugging, primarily for mobile).
