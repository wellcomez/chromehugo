
## class PreloadingPredictor
 Defines various triggering mechanisms which triggers different preloading
 operations mentioned in preloading.h. The integer portion is used for UKM
 logging, and the string portion is used to dynamically compose UMA histogram
 names. Embedders are allowed to define more predictors.

### ukm_value

ukm_value
~~~cpp
int64_t ukm_value() const { return ukm_value_; }
~~~

### name

name
~~~cpp
base::StringPiece name() const { return name_; }
~~~
