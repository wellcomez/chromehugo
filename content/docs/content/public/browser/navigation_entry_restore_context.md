
## class NavigationEntryRestoreContext
 A NavigationEntryRestoreContext is an opaque structure passed to
 NavigationEntry::SetPageState() when restoring a vector of NavigationEntries.

 It tracks the item sequence number (ISN) associated with each session
 history item, and maintains a mapping of ISNs to session history items to
 ensure items are de-duplicated if they appear in multiple NavigationEntries.

### ~NavigationEntryRestoreContext

~NavigationEntryRestoreContext
~~~cpp
virtual ~NavigationEntryRestoreContext() = default;
~~~

###  Create


~~~cpp
CONTENT_EXPORT static std::unique_ptr<NavigationEntryRestoreContext> Create();
~~~
### ~NavigationEntryRestoreContext

~NavigationEntryRestoreContext
~~~cpp
virtual ~NavigationEntryRestoreContext() = default;
~~~

###  Create


~~~cpp
CONTENT_EXPORT static std::unique_ptr<NavigationEntryRestoreContext> Create();
~~~