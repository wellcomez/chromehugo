
## class WebUIDataSource
 A data source that can help with implementing the common operations needed by
 WebUI pages.

### ~WebUIDataSource

~WebUIDataSource
~~~cpp
virtual ~WebUIDataSource() {}
~~~

### CreateAndAdd

WebUIDataSource::CreateAndAdd
~~~cpp
CONTENT_EXPORT static WebUIDataSource* CreateAndAdd(
      BrowserContext* browser_context,
      const std::string& source_name);
~~~
 Creates a WebUIDataSource and adds it to the BrowserContext, which owns it.

 Callers just get a raw pointer, which they don't own.

### Update

WebUIDataSource::Update
~~~cpp
CONTENT_EXPORT static void Update(BrowserContext* browser_context,
                                    const std::string& source_name,
                                    const base::Value::Dict& update);
~~~

### AddString

AddString
~~~cpp
virtual void AddString(base::StringPiece name,
                         const std::u16string& value) = 0;
~~~
 Adds a string keyed to its name to our dictionary.

### AddString

AddString
~~~cpp
virtual void AddString(base::StringPiece name, const std::string& value) = 0;
~~~
 Adds a string keyed to its name to our dictionary.

### AddLocalizedString

AddLocalizedString
~~~cpp
virtual void AddLocalizedString(base::StringPiece name, int ids) = 0;
~~~
 Adds a localized string with resource |ids| keyed to its name to our
 dictionary.

### AddLocalizedStrings

AddLocalizedStrings
~~~cpp
virtual void AddLocalizedStrings(
      base::span<const webui::LocalizedString> strings) = 0;
~~~
 Calls AddLocalizedString() in a for-loop for |strings|. Reduces code size
 vs. reimplementing the same for-loop.

### AddLocalizedStrings

AddLocalizedStrings
~~~cpp
virtual void AddLocalizedStrings(
      const base::Value::Dict& localized_strings) = 0;
~~~
 Add strings from `localized_strings` to our dictionary.

### AddBoolean

AddBoolean
~~~cpp
virtual void AddBoolean(base::StringPiece name, bool value) = 0;
~~~
 Adds a boolean keyed to its name to our dictionary.

### AddInteger

AddInteger
~~~cpp
virtual void AddInteger(base::StringPiece name, int32_t value) = 0;
~~~
 Adds a signed 32-bit integer keyed to its name to our dictionary. Larger
 integers may not be exactly representable in JavaScript. See
 MAX_SAFE_INTEGER in /v8/src/globals.h.

### AddDouble

AddDouble
~~~cpp
virtual void AddDouble(base::StringPiece name, double value) = 0;
~~~
 Adds a double keyed to its name  to our dictionary.

### UseStringsJs

UseStringsJs
~~~cpp
virtual void UseStringsJs() = 0;
~~~
 Call this to enable a virtual "strings.js" (or "strings.m.js" for modules)
 URL that provides translations and dynamic data when requested.

### AddResourcePath

AddResourcePath
~~~cpp
virtual void AddResourcePath(base::StringPiece path, int resource_id) = 0;
~~~
 Adds a mapping between a path name and a resource to return.

### AddResourcePaths

AddResourcePaths
~~~cpp
virtual void AddResourcePaths(
      base::span<const webui::ResourcePath> paths) = 0;
~~~
 Calls AddResourcePath() in a for-loop for |paths|. Reduces code size vs.

 reimplementing the same for-loop.

### SetDefaultResource

SetDefaultResource
~~~cpp
virtual void SetDefaultResource(int resource_id) = 0;
~~~
 Sets the resource to returned when no other paths match.

### SetRequestFilter

SetRequestFilter
~~~cpp
virtual void SetRequestFilter(
      const ShouldHandleRequestCallback& should_handle_request_callback,
      const HandleRequestCallback& handle_request_callback) = 0;
~~~
 Allows a caller to add a filter for URL requests.

### DisableReplaceExistingSource

DisableReplaceExistingSource
~~~cpp
virtual void DisableReplaceExistingSource() = 0;
~~~
 The following map to methods on URLDataSource. See the documentation there.

 NOTE: it's not acceptable to call DisableContentSecurityPolicy for new
 pages, see URLDataSource::ShouldAddContentSecurityPolicy and talk to
 tsepez.

 Currently only used by embedders for WebUIs with multiple instances.

### DisableContentSecurityPolicy

DisableContentSecurityPolicy
~~~cpp
virtual void DisableContentSecurityPolicy() = 0;
~~~

### OverrideContentSecurityPolicy

OverrideContentSecurityPolicy
~~~cpp
virtual void OverrideContentSecurityPolicy(
      network::mojom::CSPDirectiveName directive,
      const std::string& value) = 0;
~~~
 Overrides the content security policy for a certain directive.

### OverrideCrossOriginOpenerPolicy

OverrideCrossOriginOpenerPolicy
~~~cpp
virtual void OverrideCrossOriginOpenerPolicy(const std::string& value) = 0;
~~~
 Adds cross origin opener, embedder, and resource policy headers.

### OverrideCrossOriginEmbedderPolicy

OverrideCrossOriginEmbedderPolicy
~~~cpp
virtual void OverrideCrossOriginEmbedderPolicy(const std::string& value) = 0;
~~~

### OverrideCrossOriginResourcePolicy

OverrideCrossOriginResourcePolicy
~~~cpp
virtual void OverrideCrossOriginResourcePolicy(const std::string& value) = 0;
~~~

### DisableTrustedTypesCSP

DisableTrustedTypesCSP
~~~cpp
virtual void DisableTrustedTypesCSP() = 0;
~~~
 Removes directives related to Trusted Types from the CSP header.

### DisableDenyXFrameOptions

DisableDenyXFrameOptions
~~~cpp
virtual void DisableDenyXFrameOptions() = 0;
~~~
 This method is deprecated and AddFrameAncestors should be used instead.

### AddFrameAncestor

AddFrameAncestor
~~~cpp
virtual void AddFrameAncestor(const GURL& frame_ancestor) = 0;
~~~

### EnableReplaceI18nInJS

EnableReplaceI18nInJS
~~~cpp
virtual void EnableReplaceI18nInJS() = 0;
~~~
 Replace i18n template strings in JS files. Needed for Web UIs that are
 using Polymer 3.

### GetSource

GetSource
~~~cpp
virtual std::string GetSource() = 0;
~~~
 The |source_name| this WebUIDataSource was created with.

### ~WebUIDataSource

~WebUIDataSource
~~~cpp
virtual ~WebUIDataSource() {}
~~~

### AddString

AddString
~~~cpp
virtual void AddString(base::StringPiece name,
                         const std::u16string& value) = 0;
~~~
 Adds a string keyed to its name to our dictionary.

### AddString

AddString
~~~cpp
virtual void AddString(base::StringPiece name, const std::string& value) = 0;
~~~
 Adds a string keyed to its name to our dictionary.

### AddLocalizedString

AddLocalizedString
~~~cpp
virtual void AddLocalizedString(base::StringPiece name, int ids) = 0;
~~~
 Adds a localized string with resource |ids| keyed to its name to our
 dictionary.

### AddLocalizedStrings

AddLocalizedStrings
~~~cpp
virtual void AddLocalizedStrings(
      base::span<const webui::LocalizedString> strings) = 0;
~~~
 Calls AddLocalizedString() in a for-loop for |strings|. Reduces code size
 vs. reimplementing the same for-loop.

### AddLocalizedStrings

AddLocalizedStrings
~~~cpp
virtual void AddLocalizedStrings(
      const base::Value::Dict& localized_strings) = 0;
~~~
 Add strings from `localized_strings` to our dictionary.

### AddBoolean

AddBoolean
~~~cpp
virtual void AddBoolean(base::StringPiece name, bool value) = 0;
~~~
 Adds a boolean keyed to its name to our dictionary.

### AddInteger

AddInteger
~~~cpp
virtual void AddInteger(base::StringPiece name, int32_t value) = 0;
~~~
 Adds a signed 32-bit integer keyed to its name to our dictionary. Larger
 integers may not be exactly representable in JavaScript. See
 MAX_SAFE_INTEGER in /v8/src/globals.h.

### AddDouble

AddDouble
~~~cpp
virtual void AddDouble(base::StringPiece name, double value) = 0;
~~~
 Adds a double keyed to its name  to our dictionary.

### UseStringsJs

UseStringsJs
~~~cpp
virtual void UseStringsJs() = 0;
~~~
 Call this to enable a virtual "strings.js" (or "strings.m.js" for modules)
 URL that provides translations and dynamic data when requested.

### AddResourcePath

AddResourcePath
~~~cpp
virtual void AddResourcePath(base::StringPiece path, int resource_id) = 0;
~~~
 Adds a mapping between a path name and a resource to return.

### AddResourcePaths

AddResourcePaths
~~~cpp
virtual void AddResourcePaths(
      base::span<const webui::ResourcePath> paths) = 0;
~~~
 Calls AddResourcePath() in a for-loop for |paths|. Reduces code size vs.

 reimplementing the same for-loop.

### SetDefaultResource

SetDefaultResource
~~~cpp
virtual void SetDefaultResource(int resource_id) = 0;
~~~
 Sets the resource to returned when no other paths match.

### SetRequestFilter

SetRequestFilter
~~~cpp
virtual void SetRequestFilter(
      const ShouldHandleRequestCallback& should_handle_request_callback,
      const HandleRequestCallback& handle_request_callback) = 0;
~~~
 Allows a caller to add a filter for URL requests.

### DisableReplaceExistingSource

DisableReplaceExistingSource
~~~cpp
virtual void DisableReplaceExistingSource() = 0;
~~~
 The following map to methods on URLDataSource. See the documentation there.

 NOTE: it's not acceptable to call DisableContentSecurityPolicy for new
 pages, see URLDataSource::ShouldAddContentSecurityPolicy and talk to
 tsepez.

 Currently only used by embedders for WebUIs with multiple instances.

### DisableContentSecurityPolicy

DisableContentSecurityPolicy
~~~cpp
virtual void DisableContentSecurityPolicy() = 0;
~~~

### OverrideContentSecurityPolicy

OverrideContentSecurityPolicy
~~~cpp
virtual void OverrideContentSecurityPolicy(
      network::mojom::CSPDirectiveName directive,
      const std::string& value) = 0;
~~~
 Overrides the content security policy for a certain directive.

### OverrideCrossOriginOpenerPolicy

OverrideCrossOriginOpenerPolicy
~~~cpp
virtual void OverrideCrossOriginOpenerPolicy(const std::string& value) = 0;
~~~
 Adds cross origin opener, embedder, and resource policy headers.

### OverrideCrossOriginEmbedderPolicy

OverrideCrossOriginEmbedderPolicy
~~~cpp
virtual void OverrideCrossOriginEmbedderPolicy(const std::string& value) = 0;
~~~

### OverrideCrossOriginResourcePolicy

OverrideCrossOriginResourcePolicy
~~~cpp
virtual void OverrideCrossOriginResourcePolicy(const std::string& value) = 0;
~~~

### DisableTrustedTypesCSP

DisableTrustedTypesCSP
~~~cpp
virtual void DisableTrustedTypesCSP() = 0;
~~~
 Removes directives related to Trusted Types from the CSP header.

### DisableDenyXFrameOptions

DisableDenyXFrameOptions
~~~cpp
virtual void DisableDenyXFrameOptions() = 0;
~~~
 This method is deprecated and AddFrameAncestors should be used instead.

### AddFrameAncestor

AddFrameAncestor
~~~cpp
virtual void AddFrameAncestor(const GURL& frame_ancestor) = 0;
~~~

### EnableReplaceI18nInJS

EnableReplaceI18nInJS
~~~cpp
virtual void EnableReplaceI18nInJS() = 0;
~~~
 Replace i18n template strings in JS files. Needed for Web UIs that are
 using Polymer 3.

### GetSource

GetSource
~~~cpp
virtual std::string GetSource() = 0;
~~~
 The |source_name| this WebUIDataSource was created with.

### CreateAndAdd

WebUIDataSource::CreateAndAdd
~~~cpp
CONTENT_EXPORT static WebUIDataSource* CreateAndAdd(
      BrowserContext* browser_context,
      const std::string& source_name);
~~~
 Creates a WebUIDataSource and adds it to the BrowserContext, which owns it.

 Callers just get a raw pointer, which they don't own.

### Update

WebUIDataSource::Update
~~~cpp
CONTENT_EXPORT static void Update(BrowserContext* browser_context,
                                    const std::string& source_name,
                                    const base::Value::Dict& update);
~~~
