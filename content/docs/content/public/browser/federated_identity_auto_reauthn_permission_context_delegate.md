
## class FederatedIdentityAutoReauthnPermissionContextDelegate
 Delegate interface for the FedCM implementation to query whether the FedCM
 API's auto re-authn is enabled in Site Settings.

### ~FederatedIdentityAutoReauthnPermissionContextDelegate

~FederatedIdentityAutoReauthnPermissionContextDelegate
~~~cpp
virtual ~FederatedIdentityAutoReauthnPermissionContextDelegate() = default;
~~~

### HasAutoReauthnContentSetting

FederatedIdentityAutoReauthnPermissionContextDelegate::HasAutoReauthnContentSetting
~~~cpp
virtual bool HasAutoReauthnContentSetting() = 0;
~~~
 Returns whether the FedCM API's auto re-authn is unblocked based on content
 settings. A caller should also use `IsAutoReauthnEmbargoed()` to determine
 whether auto re-authn is allowed or not.

### IsAutoReauthnEmbargoed

FederatedIdentityAutoReauthnPermissionContextDelegate::IsAutoReauthnEmbargoed
~~~cpp
virtual bool IsAutoReauthnEmbargoed(
      const url::Origin& relying_party_embedder) = 0;
~~~
 Returns whether the FedCM API's auto re-authn feature is embargoed for the
 passed-in |relying_party_embedder|. A caller should also use
 `HasAutoReauthnContentSetting()` to determine whether auto re-authn is
 allowed or not.

### GetAutoReauthnEmbargoStartTime

FederatedIdentityAutoReauthnPermissionContextDelegate::GetAutoReauthnEmbargoStartTime
~~~cpp
virtual base::Time GetAutoReauthnEmbargoStartTime(
      const url::Origin& relying_party_embedder) = 0;
~~~
 Returns the most recent recorded time an auto-reauthn embargo was started
 with the given |relying_party_embedder|. Returns base::Time() if no record
 is found.

### RecordDisplayAndEmbargo

FederatedIdentityAutoReauthnPermissionContextDelegate::RecordDisplayAndEmbargo
~~~cpp
virtual void RecordDisplayAndEmbargo(
      const url::Origin& relying_party_embedder) = 0;
~~~
 Records that an auto re-authn prompt was displayed to the user and places
 the permission under embargo for the passed-in |relying_party_embedder|.
