
## class DevToolsAgentHostClientChannel
 A channel, that is, an open session/connection for sending messages to a
 DevToolsAgentHostClient.


 The channel expects CBOR (binary format) encoded inputs.


 It transcodes to JSON if the underlying DevToolsAgentHostClient specifies
 DevToolsAgentHostClient::UsesBinaryProtocol() == false.


 It inserts the session id, if the underlying session is a child session
 in flatten mode. See also the documentation for the DevTools protocol
 methods Target.attachToTarget and Target.setAutoAttach.


 To obtain a client channel, embedders override
 DevToolsManagerDelegate::ClientAttached.

### GetAgentHost

DevToolsAgentHostClientChannel::GetAgentHost
~~~cpp
virtual DevToolsAgentHost* GetAgentHost() = 0;
~~~
 The agent host which will be identified to the client as the sender.

### GetClient

DevToolsAgentHostClientChannel::GetClient
~~~cpp
virtual DevToolsAgentHostClient* GetClient() = 0;
~~~
 The client to which the messages will be dispatched.

### ~DevToolsAgentHostClientChannel

~DevToolsAgentHostClientChannel
~~~cpp
virtual ~DevToolsAgentHostClientChannel() = default;
~~~
