### BindInterfaceInGpuProcess

BindInterfaceInGpuProcess
~~~cpp
CONTENT_EXPORT void BindInterfaceInGpuProcess(
    const std::string& interface_name,
    mojo::ScopedMessagePipeHandle interface_pipe);
~~~

### BindInterfaceInGpuProcess

BindInterfaceInGpuProcess
~~~cpp
void BindInterfaceInGpuProcess(mojo::PendingReceiver<Interface> receiver) {
  using ProvidedSandboxType = decltype(Interface::kServiceSandbox);
  static_assert(
      std::is_same<ProvidedSandboxType, const sandbox::mojom::Sandbox>::value,
      "This interface does not declare a proper ServiceSandbox attribute. "
      "See //docs/mojo_and_services.md (Specifying a sandbox).");
  static_assert(Interface::kServiceSandbox == sandbox::mojom::Sandbox::kGpu,
                "This interface must have [ServiceSandbox=kGpu].");

  BindInterfaceInGpuProcess(Interface::Name_, receiver.PassPipe());
}
~~~

