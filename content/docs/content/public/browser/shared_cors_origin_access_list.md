
## class SharedCorsOriginAccessList
    : public
 A public interface to manage CORS origin access lists on the UI thread.

 The shared network::cors::OriginAccessList instance can only be accessed on
 the IO thread if NetworkService is not enabled. Callers on UI thread must use
 this wrapper class to make it work with and without NetworkService until
 NetworkService is fully enabled. If NetworkService is enabled,
 network::cors::OriginAccessList is accessed only on the UI thread, and all
 calls can be finished synchronously. This is used for remembering per-profile
 access lists in the browser process.

 TODO(toyoshim): Remove this class, and use network::cors::OriginAccessList
 directly once NetworkService is fully enabled.

### SharedCorsOriginAccessList

SharedCorsOriginAccessList
~~~cpp
SharedCorsOriginAccessList() = default;
~~~

### SharedCorsOriginAccessList

SharedCorsOriginAccessList
~~~cpp
SharedCorsOriginAccessList(const SharedCorsOriginAccessList&) = delete;
~~~

### operator=

SharedCorsOriginAccessList
    : public::operator=
~~~cpp
SharedCorsOriginAccessList& operator=(const SharedCorsOriginAccessList&) =
      delete;
~~~

### SetForOrigin

SharedCorsOriginAccessList
    : public::SetForOrigin
~~~cpp
virtual void SetForOrigin(
      const url::Origin& source_origin,
      std::vector<network::mojom::CorsOriginPatternPtr> allow_patterns,
      std::vector<network::mojom::CorsOriginPatternPtr> block_patterns,
      base::OnceClosure closure) = 0;
~~~
 Sets the access list to an internal network::cors::OriginAccessList
 instance so that its IsAllowed() method works for all users that refer the
 shared network::cors::OriginAccessList instance returned by
 origin_access_list() below. |allow_patterns| and |block_patterns| will be
 moved so to pass the lists to the IO thread if NetworkService is disabled.

 Should be called on the UI thread, and |closure| runs on the UI thread too.

### GetOriginAccessList

SharedCorsOriginAccessList
    : public::GetOriginAccessList
~~~cpp
virtual const network::cors::OriginAccessList& GetOriginAccessList() = 0;
~~~
 Gets a shared OriginAccessList instance pointer. |this| should outlives
 callers' OriginAccessList instance uses. Should be called on the IO thread.
