
## class VideoOverlayWindow
 This window will always float above other windows. The intention is to show
 content perpetually while the user is still interacting with the other
 browser windows.

### enum PlaybackState

~~~cpp
enum PlaybackState {
    kPlaying = 0,
    kPaused,
    kEndOfVideo,
  }
~~~
### VideoOverlayWindow

VideoOverlayWindow
~~~cpp
VideoOverlayWindow() = default;
~~~

### VideoOverlayWindow

VideoOverlayWindow
~~~cpp
VideoOverlayWindow(const VideoOverlayWindow&) = delete;
~~~

### operator=

operator=
~~~cpp
VideoOverlayWindow& operator=(const VideoOverlayWindow&) = delete;
~~~

### Create

VideoOverlayWindow::Create
~~~cpp
static std::unique_ptr<VideoOverlayWindow> Create(
      VideoPictureInPictureWindowController* controller);
~~~
 Returns a created VideoOverlayWindow. This is defined in the
 platform-specific implementation for the class.

### ~VideoOverlayWindow

~VideoOverlayWindow
~~~cpp
virtual ~VideoOverlayWindow() = default;
~~~

### IsActive

IsActive
~~~cpp
virtual bool IsActive() const = 0;
~~~

### Close

Close
~~~cpp
virtual void Close() = 0;
~~~

### ShowInactive

ShowInactive
~~~cpp
virtual void ShowInactive() = 0;
~~~

### Hide

Hide
~~~cpp
virtual void Hide() = 0;
~~~

### IsVisible

IsVisible
~~~cpp
virtual bool IsVisible() const = 0;
~~~

### GetBounds

GetBounds
~~~cpp
virtual gfx::Rect GetBounds() = 0;
~~~
 Retrieves the window's current bounds, including its window.

### UpdateNaturalSize

UpdateNaturalSize
~~~cpp
virtual void UpdateNaturalSize(const gfx::Size& natural_size) = 0;
~~~

### SetPlaybackState

SetPlaybackState
~~~cpp
virtual void SetPlaybackState(PlaybackState playback_state) = 0;
~~~

### SetPlayPauseButtonVisibility

SetPlayPauseButtonVisibility
~~~cpp
virtual void SetPlayPauseButtonVisibility(bool is_visible) = 0;
~~~

### SetSkipAdButtonVisibility

SetSkipAdButtonVisibility
~~~cpp
virtual void SetSkipAdButtonVisibility(bool is_visible) = 0;
~~~

### SetNextTrackButtonVisibility

SetNextTrackButtonVisibility
~~~cpp
virtual void SetNextTrackButtonVisibility(bool is_visible) = 0;
~~~

### SetPreviousTrackButtonVisibility

SetPreviousTrackButtonVisibility
~~~cpp
virtual void SetPreviousTrackButtonVisibility(bool is_visible) = 0;
~~~

### SetMicrophoneMuted

SetMicrophoneMuted
~~~cpp
virtual void SetMicrophoneMuted(bool muted) = 0;
~~~

### SetCameraState

SetCameraState
~~~cpp
virtual void SetCameraState(bool turned_on) = 0;
~~~

### SetToggleMicrophoneButtonVisibility

SetToggleMicrophoneButtonVisibility
~~~cpp
virtual void SetToggleMicrophoneButtonVisibility(bool is_visible) = 0;
~~~

### SetToggleCameraButtonVisibility

SetToggleCameraButtonVisibility
~~~cpp
virtual void SetToggleCameraButtonVisibility(bool is_visible) = 0;
~~~

### SetHangUpButtonVisibility

SetHangUpButtonVisibility
~~~cpp
virtual void SetHangUpButtonVisibility(bool is_visible) = 0;
~~~

### SetNextSlideButtonVisibility

SetNextSlideButtonVisibility
~~~cpp
virtual void SetNextSlideButtonVisibility(bool is_visible) = 0;
~~~

### SetPreviousSlideButtonVisibility

SetPreviousSlideButtonVisibility
~~~cpp
virtual void SetPreviousSlideButtonVisibility(bool is_visible) = 0;
~~~

### SetSurfaceId

SetSurfaceId
~~~cpp
virtual void SetSurfaceId(const viz::SurfaceId& surface_id) = 0;
~~~

### VideoOverlayWindow

VideoOverlayWindow
~~~cpp
VideoOverlayWindow() = default;
~~~

### VideoOverlayWindow

VideoOverlayWindow
~~~cpp
VideoOverlayWindow(const VideoOverlayWindow&) = delete;
~~~

### operator=

operator=
~~~cpp
VideoOverlayWindow& operator=(const VideoOverlayWindow&) = delete;
~~~

### ~VideoOverlayWindow

~VideoOverlayWindow
~~~cpp
virtual ~VideoOverlayWindow() = default;
~~~

### IsActive

IsActive
~~~cpp
virtual bool IsActive() const = 0;
~~~

### Close

Close
~~~cpp
virtual void Close() = 0;
~~~

### ShowInactive

ShowInactive
~~~cpp
virtual void ShowInactive() = 0;
~~~

### Hide

Hide
~~~cpp
virtual void Hide() = 0;
~~~

### IsVisible

IsVisible
~~~cpp
virtual bool IsVisible() const = 0;
~~~

### GetBounds

GetBounds
~~~cpp
virtual gfx::Rect GetBounds() = 0;
~~~
 Retrieves the window's current bounds, including its window.

### UpdateNaturalSize

UpdateNaturalSize
~~~cpp
virtual void UpdateNaturalSize(const gfx::Size& natural_size) = 0;
~~~

### SetPlaybackState

SetPlaybackState
~~~cpp
virtual void SetPlaybackState(PlaybackState playback_state) = 0;
~~~

### SetPlayPauseButtonVisibility

SetPlayPauseButtonVisibility
~~~cpp
virtual void SetPlayPauseButtonVisibility(bool is_visible) = 0;
~~~

### SetSkipAdButtonVisibility

SetSkipAdButtonVisibility
~~~cpp
virtual void SetSkipAdButtonVisibility(bool is_visible) = 0;
~~~

### SetNextTrackButtonVisibility

SetNextTrackButtonVisibility
~~~cpp
virtual void SetNextTrackButtonVisibility(bool is_visible) = 0;
~~~

### SetPreviousTrackButtonVisibility

SetPreviousTrackButtonVisibility
~~~cpp
virtual void SetPreviousTrackButtonVisibility(bool is_visible) = 0;
~~~

### SetMicrophoneMuted

SetMicrophoneMuted
~~~cpp
virtual void SetMicrophoneMuted(bool muted) = 0;
~~~

### SetCameraState

SetCameraState
~~~cpp
virtual void SetCameraState(bool turned_on) = 0;
~~~

### SetToggleMicrophoneButtonVisibility

SetToggleMicrophoneButtonVisibility
~~~cpp
virtual void SetToggleMicrophoneButtonVisibility(bool is_visible) = 0;
~~~

### SetToggleCameraButtonVisibility

SetToggleCameraButtonVisibility
~~~cpp
virtual void SetToggleCameraButtonVisibility(bool is_visible) = 0;
~~~

### SetHangUpButtonVisibility

SetHangUpButtonVisibility
~~~cpp
virtual void SetHangUpButtonVisibility(bool is_visible) = 0;
~~~

### SetNextSlideButtonVisibility

SetNextSlideButtonVisibility
~~~cpp
virtual void SetNextSlideButtonVisibility(bool is_visible) = 0;
~~~

### SetPreviousSlideButtonVisibility

SetPreviousSlideButtonVisibility
~~~cpp
virtual void SetPreviousSlideButtonVisibility(bool is_visible) = 0;
~~~

### SetSurfaceId

SetSurfaceId
~~~cpp
virtual void SetSurfaceId(const viz::SurfaceId& surface_id) = 0;
~~~

### enum PlaybackState
 GENERATED_JAVA_ENUM_PACKAGE:(
   org.chromium.content_public.browser.overlay_window)
~~~cpp
enum PlaybackState {
    kPlaying = 0,
    kPaused,
    kEndOfVideo,
  };
~~~
### Create

VideoOverlayWindow::Create
~~~cpp
static std::unique_ptr<VideoOverlayWindow> Create(
      VideoPictureInPictureWindowController* controller);
~~~
 Returns a created VideoOverlayWindow. This is defined in the
 platform-specific implementation for the class.
