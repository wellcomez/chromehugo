
## class PictureInPictureWindowController
 Interface for Picture in Picture window controllers. This is currently tied
 to a WebContents |web_contents| and created when a Picture in Picture window
 is to be shown. This allows creation of a single window for the WebContents
 WebContents.

### GetOrCreateVideoPictureInPictureController

PictureInPictureWindowController::GetOrCreateVideoPictureInPictureController
~~~cpp
CONTENT_EXPORT static VideoPictureInPictureWindowController*
  GetOrCreateVideoPictureInPictureController(WebContents* web_contents);
~~~
 Gets a reference to the controller of the appropriate type associated with
 |web_contents| and creates one if it does not exist. If there is an
 existing controller, it is reused if it's of the correct type, but is
 recreated if the existing instance was for a different type. The returned
 pointer is guaranteed to be non-null.

### GetOrCreateDocumentPictureInPictureController

PictureInPictureWindowController::GetOrCreateDocumentPictureInPictureController
~~~cpp
CONTENT_EXPORT static DocumentPictureInPictureWindowController*
  GetOrCreateDocumentPictureInPictureController(WebContents* web_contents);
~~~

### ~PictureInPictureWindowController

~PictureInPictureWindowController
~~~cpp
virtual ~PictureInPictureWindowController() = default;
~~~
 !BUILDFLAG(IS_ANDROID)
### Show

Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows the Picture-in-Picture window.

### FocusInitiator

FocusInitiator
~~~cpp
virtual void FocusInitiator() = 0;
~~~
 Called to notify the controller that initiator should be focused.

### Close

Close
~~~cpp
virtual void Close(bool should_pause_video) = 0;
~~~
 Called to notify the controller that the window was requested to be closed
 by the user or the content.

### CloseAndFocusInitiator

CloseAndFocusInitiator
~~~cpp
virtual void CloseAndFocusInitiator() = 0;
~~~
 Called to notify the controller that the window was requested to be closed
 by the content and that initiator should be focused.

### OnWindowDestroyed

OnWindowDestroyed
~~~cpp
virtual void OnWindowDestroyed(bool should_pause_video) = 0;
~~~
 Called by the window implementation to notify the controller that the
 window was requested to be closed and destroyed by the system.

### GetWebContents

GetWebContents
~~~cpp
virtual WebContents* GetWebContents() = 0;
~~~
 Called to get the opener web contents for video or document PiP.

### GetWindowBounds

GetWindowBounds
~~~cpp
virtual absl::optional<gfx::Rect> GetWindowBounds() = 0;
~~~
 Called to get the Picture-in-Picture window bounds.

### GetChildWebContents

GetChildWebContents
~~~cpp
virtual WebContents* GetChildWebContents() = 0;
~~~
 Called to get the child web contents to be PiP for document PiP. This will
 be null for video PiP.

### PictureInPictureWindowController

PictureInPictureWindowController
~~~cpp
PictureInPictureWindowController() = default;
~~~
 Use PictureInPictureWindowController::GetOrCreateForWebContents() to
 create an instance.

### ~PictureInPictureWindowController

~PictureInPictureWindowController
~~~cpp
virtual ~PictureInPictureWindowController() = default;
~~~
 !BUILDFLAG(IS_ANDROID)
### Show

Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows the Picture-in-Picture window.

### FocusInitiator

FocusInitiator
~~~cpp
virtual void FocusInitiator() = 0;
~~~
 Called to notify the controller that initiator should be focused.

### Close

Close
~~~cpp
virtual void Close(bool should_pause_video) = 0;
~~~
 Called to notify the controller that the window was requested to be closed
 by the user or the content.

### CloseAndFocusInitiator

CloseAndFocusInitiator
~~~cpp
virtual void CloseAndFocusInitiator() = 0;
~~~
 Called to notify the controller that the window was requested to be closed
 by the content and that initiator should be focused.

### OnWindowDestroyed

OnWindowDestroyed
~~~cpp
virtual void OnWindowDestroyed(bool should_pause_video) = 0;
~~~
 Called by the window implementation to notify the controller that the
 window was requested to be closed and destroyed by the system.

### GetWebContents

GetWebContents
~~~cpp
virtual WebContents* GetWebContents() = 0;
~~~
 Called to get the opener web contents for video or document PiP.

### GetWindowBounds

GetWindowBounds
~~~cpp
virtual absl::optional<gfx::Rect> GetWindowBounds() = 0;
~~~
 Called to get the Picture-in-Picture window bounds.

### GetChildWebContents

GetChildWebContents
~~~cpp
virtual WebContents* GetChildWebContents() = 0;
~~~
 Called to get the child web contents to be PiP for document PiP. This will
 be null for video PiP.

### PictureInPictureWindowController

PictureInPictureWindowController
~~~cpp
PictureInPictureWindowController() = default;
~~~
 Use PictureInPictureWindowController::GetOrCreateForWebContents() to
 create an instance.

### GetOrCreateVideoPictureInPictureController

PictureInPictureWindowController::GetOrCreateVideoPictureInPictureController
~~~cpp
CONTENT_EXPORT static VideoPictureInPictureWindowController*
  GetOrCreateVideoPictureInPictureController(WebContents* web_contents);
~~~
 Gets a reference to the controller of the appropriate type associated with
 |web_contents| and creates one if it does not exist. If there is an
 existing controller, it is reused if it's of the correct type, but is
 recreated if the existing instance was for a different type. The returned
 pointer is guaranteed to be non-null.
