### GetNetworkService

GetNetworkService
~~~cpp
CONTENT_EXPORT network::mojom::NetworkService* GetNetworkService();
~~~
 Returns a pointer to the NetworkService, creating / re-creating it as needed.

 NetworkService will be running in-process if
   1) kNetworkService feature is disabled, or
   2) kNetworkService and kNetworkServiceInProcess are enabled
 Otherwise it runs out of process.

 This method can only be called on the UI thread.

### GetNetworkChangeNotifier

GetNetworkChangeNotifier
~~~cpp
CONTENT_EXPORT net::NetworkChangeNotifier* GetNetworkChangeNotifier();
~~~
 Returns the global NetworkChangeNotifier instance.

### FlushNetworkServiceInstanceForTesting

FlushNetworkServiceInstanceForTesting
~~~cpp
CONTENT_EXPORT void FlushNetworkServiceInstanceForTesting();
~~~
 Call |FlushForTesting()| on cached |mojo::Remote<NetworkService>|. For
 testing only. Must only be called on the UI thread.

### GetNetworkConnectionTracker

GetNetworkConnectionTracker
~~~cpp
CONTENT_EXPORT network::NetworkConnectionTracker* GetNetworkConnectionTracker();
~~~
 Returns a NetworkConnectionTracker that can be used to subscribe for
 network change events.

 Must only be called on the UI thread.

### GetNetworkConnectionTrackerFromUIThread

GetNetworkConnectionTrackerFromUIThread
~~~cpp
CONTENT_EXPORT void GetNetworkConnectionTrackerFromUIThread(
    base::OnceCallback<void(network::NetworkConnectionTracker*)> callback);
~~~
 Asynchronously calls the given callback with a NetworkConnectionTracker that
 can be used to subscribe to network change events.


 This is a helper method for classes that can't easily call
 GetNetworkConnectionTracker from the UI thread.

### network::NetworkConnectionTrackerAsyncGetter
CreateNetworkConnectionTrackerAsyncGetter

network::NetworkConnectionTrackerAsyncGetter
CreateNetworkConnectionTrackerAsyncGetter
~~~cpp
CONTENT_EXPORT network::NetworkConnectionTrackerAsyncGetter
CreateNetworkConnectionTrackerAsyncGetter();
~~~
 Helper method to create a NetworkConnectionTrackerAsyncGetter.

### SetNetworkConnectionTrackerForTesting

SetNetworkConnectionTrackerForTesting
~~~cpp
CONTENT_EXPORT void SetNetworkConnectionTrackerForTesting(
    network::NetworkConnectionTracker* network_connection_tracker);
~~~
 Sets the NetworkConnectionTracker instance to use. For testing only.

 Must be called on the UI thread. Must be called before the first call to
 GetNetworkConnectionTracker.

### GetNetworkTaskRunner

GetNetworkTaskRunner
~~~cpp
CONTENT_EXPORT const scoped_refptr<base::SequencedTaskRunner>&
GetNetworkTaskRunner();
~~~
 Gets the task runner for the thread the network service will be running on
 when running in-process. Can only be called when network service is in
 process.

### network::mojom::CertVerifierServiceRemoteParamsPtr
GetCertVerifierParams

network::mojom::CertVerifierServiceRemoteParamsPtr
GetCertVerifierParams
~~~cpp
CONTENT_EXPORT network::mojom::CertVerifierServiceRemoteParamsPtr
GetCertVerifierParams(cert_verifier::mojom::CertVerifierCreationParamsPtr
                          cert_verifier_creation_params);
~~~
 Returns a CertVerifierParams that can be placed into a new
 network::mojom::NetworkContextParams.


 If the CertVerifierService feature is enabled, the
 |cert_verifier_creation_params| will be used to configure a new
 CertVerifierService, and a pipe to the new CertVerifierService will be placed
 in the CertVerifierParams.


 Otherwise, |cert_verifier_creation_params| will just be placed directly into
 the CertVerifierParams to configure an in-network-service CertVerifier.

### SetCertVerifierServiceFactoryForTesting

SetCertVerifierServiceFactoryForTesting
~~~cpp
CONTENT_EXPORT void SetCertVerifierServiceFactoryForTesting(
    cert_verifier::mojom::CertVerifierServiceFactory* service_factory);
~~~
 Sets the CertVerifierServiceFactory used to instantiate
 CertVerifierServices.

### GetCertVerifierServiceFactory

GetCertVerifierServiceFactory
~~~cpp
CONTENT_EXPORT cert_verifier::mojom::CertVerifierServiceFactory*
GetCertVerifierServiceFactory();
~~~
 Returns a pointer to the CertVerifierServiceFactory, creating / re-creating
 it as needed.


 This method can only be called on the UI thread.

### CreateNetworkContextInNetworkService

CreateNetworkContextInNetworkService
~~~cpp
CONTENT_EXPORT void CreateNetworkContextInNetworkService(
    mojo::PendingReceiver<network::mojom::NetworkContext> context,
    network::mojom::NetworkContextParamsPtr params);
~~~
 Convenience function to create a NetworkContext from the given set of
 |params|. Any creation of network contexts should be done through this
 function.

 This must be called on the UI thread.

