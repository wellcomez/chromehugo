
## class InterfaceRegistrationHelper

### InterfaceRegistrationHelper

InterfaceRegistrationHelper
~~~cpp
explicit InterfaceRegistrationHelper(
      std::vector<BinderInitializer>* binder_initializers)
      : binder_initializers_(binder_initializers) {}
~~~

### ~InterfaceRegistrationHelper

~InterfaceRegistrationHelper
~~~cpp
~InterfaceRegistrationHelper() = default;
~~~

### Add

Add
~~~cpp
InterfaceRegistrationHelper<ControllerType>& Add() {
    // Insert a function that populates an interface broker instance's
    // binder_map.
    binder_initializers_->push_back(
        base::BindRepeating([](WebUIBinderMap* binder_map) {
          binder_map->Add<Interface>(base::BindRepeating(
              [](WebUIController* controller,
                 mojo::PendingReceiver<Interface> receiver) {
                auto* concrete_controller = controller->GetAs<ControllerType>();

                DCHECK(concrete_controller)
                    << "The requesting WebUIController is of a different type.";

                concrete_controller->BindInterface(std::move(receiver));
              }));
        }));
    return *this;
  }
~~~

### binder_initializers_



~~~cpp

raw_ptr<std::vector<BinderInitializer>> binder_initializers_;

~~~


### InterfaceRegistrationHelper

InterfaceRegistrationHelper
~~~cpp
explicit InterfaceRegistrationHelper(
      std::vector<BinderInitializer>* binder_initializers)
      : binder_initializers_(binder_initializers) {}
~~~

### ~InterfaceRegistrationHelper

~InterfaceRegistrationHelper
~~~cpp
~InterfaceRegistrationHelper() = default;
~~~

### binder_initializers_



~~~cpp

raw_ptr<std::vector<BinderInitializer>> binder_initializers_;

~~~


## class WebUIBrowserInterfaceBrokerRegistry
 Maintains a mapping from WebUIController::Type to a list of interfaces
 exposed to MojoJS, and provides methods to set up an interface broker that
 only brokers the registered interfaces for the WebUIController.


 To register interfaces for WebUI, use the following code:

 registry.ForWebUI<ControllerType>
    .Add<Interface1>()
    .Add<Interface2>();
### ~WebUIBrowserInterfaceBrokerRegistry

WebUIBrowserInterfaceBrokerRegistry::~WebUIBrowserInterfaceBrokerRegistry
~~~cpp
~WebUIBrowserInterfaceBrokerRegistry()
~~~

### WebUIBrowserInterfaceBrokerRegistry

WebUIBrowserInterfaceBrokerRegistry
~~~cpp
WebUIBrowserInterfaceBrokerRegistry(
      const WebUIBrowserInterfaceBrokerRegistry&) = delete;
~~~

### operator=

WebUIBrowserInterfaceBrokerRegistry::operator=
~~~cpp
WebUIBrowserInterfaceBrokerRegistry& operator=(
      const WebUIBrowserInterfaceBrokerRegistry&) = delete;
~~~

### ForWebUI

ForWebUI
~~~cpp
InterfaceRegistrationHelper<ControllerType> ForWebUI() {
    // WebUIController::GetType() requires a instantiated WebUIController
    // (because it's a virtual method and can't be static). Here we only have
    // type information, so we need to figure out the type from the controller's
    // class declaration.
    WebUIController::Type type = &ControllerType::kWebUIControllerType;

    DCHECK(binder_initializers_.count(type) == 0)
        << "Interfaces for a WebUI should be registered together.";

    return InterfaceRegistrationHelper<ControllerType>(
        &binder_initializers_[type]);
  }
~~~

### CreateInterfaceBroker

WebUIBrowserInterfaceBrokerRegistry::CreateInterfaceBroker
~~~cpp
std::unique_ptr<PerWebUIBrowserInterfaceBroker> CreateInterfaceBroker(
      WebUIController& controller);
~~~
 Creates an unbounded interface broker for |controller|. Caller should call
 Bind() method on the returned broker with a PendingReceiver that receives
 MojoJS.bindInterface requests from the renderer. Returns nullptr if
 controller doesn't have registered interface broker initializers.

### AddBinderForTesting

AddBinderForTesting
~~~cpp
void AddBinderForTesting(
      base::RepeatingCallback<void(WebUIController*,
                                   mojo::PendingReceiver<Interface>)> binder) {
    for (auto& it : binder_initializers_) {
      it.second.push_back(base::BindRepeating(
          [](decltype(binder) binder, WebUIBinderMap* binder_map) {
            binder_map->Add<Interface>(binder);
          },
          binder));
    }
  }
~~~
