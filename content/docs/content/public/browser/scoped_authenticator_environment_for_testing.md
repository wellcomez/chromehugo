
## class ScopedAuthenticatorEnvironmentForTesting
 Allows replacing the default FidoDiscoveryFactory to support injecting
 virtual authenticators. These objects cannot be nested.

### ~ScopedAuthenticatorEnvironmentForTesting

ScopedAuthenticatorEnvironmentForTesting::~ScopedAuthenticatorEnvironmentForTesting
~~~cpp
~ScopedAuthenticatorEnvironmentForTesting()
~~~

### ScopedAuthenticatorEnvironmentForTesting

ScopedAuthenticatorEnvironmentForTesting
~~~cpp
ScopedAuthenticatorEnvironmentForTesting(
      const ScopedAuthenticatorEnvironmentForTesting&) = delete;
~~~

### ScopedAuthenticatorEnvironmentForTesting

ScopedAuthenticatorEnvironmentForTesting
~~~cpp
ScopedAuthenticatorEnvironmentForTesting(
      ScopedAuthenticatorEnvironmentForTesting&&) = delete;
~~~

### operator=

ScopedAuthenticatorEnvironmentForTesting::operator=
~~~cpp
ScopedAuthenticatorEnvironmentForTesting& operator=(
      const ScopedAuthenticatorEnvironmentForTesting&) = delete;
~~~

### operator=

ScopedAuthenticatorEnvironmentForTesting::operator=
~~~cpp
ScopedAuthenticatorEnvironmentForTesting& operator=(
      const ScopedAuthenticatorEnvironmentForTesting&&) = delete;
~~~
