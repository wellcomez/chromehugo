### std::unique_ptr&lt;net::CookieStore&gt; CreateCookieStore

std::unique_ptr&lt;net::CookieStore&gt; CreateCookieStore
~~~cpp
CONTENT_EXPORT std::unique_ptr<net::CookieStore> CreateCookieStore(
    const CookieStoreConfig& config,
    net::NetLog* net_log);
~~~

## struct CookieStoreConfig

### CookieStoreConfig

CookieStoreConfig::CookieStoreConfig
~~~cpp
CookieStoreConfig(const base::FilePath& path,
                    bool restore_old_session_cookies,
                    bool persist_session_cookies)
~~~

### ~CookieStoreConfig

CookieStoreConfig::~CookieStoreConfig
~~~cpp
~CookieStoreConfig()
~~~
