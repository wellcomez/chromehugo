
## class NavigationHandleUserData

### CreateForNavigationHandle

CreateForNavigationHandle
~~~cpp
static void CreateForNavigationHandle(NavigationHandle& navigation_handle,
                                        Args&&... args) {
    if (!GetForNavigationHandle(navigation_handle)) {
      navigation_handle.SetUserData(
          UserDataKey(), base::WrapUnique(new T(navigation_handle,
                                                std::forward<Args>(args)...)));
    }
  }
~~~

### GetForNavigationHandle

GetForNavigationHandle
~~~cpp
static T* GetForNavigationHandle(NavigationHandle& navigation_handle) {
    return static_cast<T*>(navigation_handle.GetUserData(UserDataKey()));
  }
~~~

### GetOrCreateForNavigationHandle

GetOrCreateForNavigationHandle
~~~cpp
static T* GetOrCreateForNavigationHandle(
      NavigationHandle& navigation_handle) {
    if (!GetForNavigationHandle(navigation_handle)) {
      CreateForNavigationHandle(navigation_handle);
    }
    return GetForNavigationHandle(navigation_handle);
  }
~~~

### DeleteForNavigationHandle

DeleteForNavigationHandle
~~~cpp
static void DeleteForNavigationHandle(NavigationHandle& navigation_handle) {
    DCHECK(GetForNavigationHandle(navigation_handle));
    navigation_handle.RemoveUserData(UserDataKey());
  }
~~~

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### GetForNavigationHandle

GetForNavigationHandle
~~~cpp
static T* GetForNavigationHandle(NavigationHandle& navigation_handle) {
    return static_cast<T*>(navigation_handle.GetUserData(UserDataKey()));
  }
~~~

### GetOrCreateForNavigationHandle

GetOrCreateForNavigationHandle
~~~cpp
static T* GetOrCreateForNavigationHandle(
      NavigationHandle& navigation_handle) {
    if (!GetForNavigationHandle(navigation_handle)) {
      CreateForNavigationHandle(navigation_handle);
    }
    return GetForNavigationHandle(navigation_handle);
  }
~~~

### DeleteForNavigationHandle

DeleteForNavigationHandle
~~~cpp
static void DeleteForNavigationHandle(NavigationHandle& navigation_handle) {
    DCHECK(GetForNavigationHandle(navigation_handle));
    navigation_handle.RemoveUserData(UserDataKey());
  }
~~~

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~
