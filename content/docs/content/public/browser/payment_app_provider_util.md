
## class PaymentAppProviderUtil

### GetSourceIdForPaymentAppFromScope

PaymentAppProviderUtil::GetSourceIdForPaymentAppFromScope
~~~cpp
GetSourceIdForPaymentAppFromScope(const GURL& sw_scope)
~~~

### IsValidInstallablePaymentApp

PaymentAppProviderUtil::IsValidInstallablePaymentApp
~~~cpp
static bool IsValidInstallablePaymentApp(const GURL& manifest_url,
                                           const GURL& sw_js_url,
                                           const GURL& sw_scope,
                                           std::string* error_message);
~~~
 Check whether given |sw_js_url| from |manifest_url| is allowed to register
 with |sw_scope|.

### CreateBlankCanMakePaymentResponse

PaymentAppProviderUtil::CreateBlankCanMakePaymentResponse
~~~cpp
static payments::mojom::CanMakePaymentResponsePtr
  CreateBlankCanMakePaymentResponse(
      payments::mojom::CanMakePaymentEventResponseType response_type);
~~~
 Create blank struct for response to "can make payment".

### CreateBlankPaymentHandlerResponse

PaymentAppProviderUtil::CreateBlankPaymentHandlerResponse
~~~cpp
static payments::mojom::PaymentHandlerResponsePtr
  CreateBlankPaymentHandlerResponse(
      payments::mojom::PaymentEventResponseType response_type);
~~~
 Create blank struct for receipt payment app response from render side.
