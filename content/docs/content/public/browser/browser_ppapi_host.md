
## class BrowserPpapiHost
 Interface that allows components in the embedder app to talk to the
 PpapiHost in the browser process.


 There will be one of these objects in the browser per plugin process. It
 lives entirely on the I/O thread.

### ~BrowserPpapiHost

~BrowserPpapiHost
~~~cpp
virtual ~BrowserPpapiHost() {}
~~~

### GetPpapiHost

BrowserPpapiHost::GetPpapiHost
~~~cpp
virtual ppapi::host::PpapiHost* GetPpapiHost() = 0;
~~~
 Returns the PpapiHost object.

### GetPluginProcess

BrowserPpapiHost::GetPluginProcess
~~~cpp
virtual const base::Process& GetPluginProcess() = 0;
~~~
 Returns a reference to the plugin process.

### IsValidInstance

BrowserPpapiHost::IsValidInstance
~~~cpp
virtual bool IsValidInstance(PP_Instance instance) = 0;
~~~
 Returns true if the given PP_Instance is valid.

### GetRenderFrameIDsForInstance

BrowserPpapiHost::GetRenderFrameIDsForInstance
~~~cpp
virtual bool GetRenderFrameIDsForInstance(PP_Instance instance,
                                            int* render_process_id,
                                            int* render_frame_id) = 0;
~~~
 Retrieves the process/frame Ids associated with the RenderFrame containing
 the given instance and returns true on success. If the instance is
 invalid, the ids will be 0 and false will be returned.


 When a resource is created, the PP_Instance should already have been
 validated, and the resource hosts will be deleted when the resource is
 destroyed. So it should not generally be necessary to check for errors
 from this function except as a last-minute sanity check if you convert the
 IDs to a RenderFrame/ProcessHost on the UI thread.

### GetPluginName

BrowserPpapiHost::GetPluginName
~~~cpp
virtual const std::string& GetPluginName() = 0;
~~~
 Returns the name of the plugin.

### GetPluginPath

BrowserPpapiHost::GetPluginPath
~~~cpp
virtual const base::FilePath& GetPluginPath() = 0;
~~~
 Returns the path of the plugin.

### GetProfileDataDirectory

BrowserPpapiHost::GetProfileDataDirectory
~~~cpp
virtual const base::FilePath& GetProfileDataDirectory() = 0;
~~~
 Returns the user's profile data directory.

### GetDocumentURLForInstance

BrowserPpapiHost::GetDocumentURLForInstance
~~~cpp
virtual GURL GetDocumentURLForInstance(PP_Instance instance) = 0;
~~~
 Get the Document/Plugin URLs for the given PP_Instance.

### GetPluginURLForInstance

BrowserPpapiHost::GetPluginURLForInstance
~~~cpp
virtual GURL GetPluginURLForInstance(PP_Instance instance) = 0;
~~~
