### RenderFrameMetadataProvider

RenderFrameMetadataProvider
~~~cpp
RenderFrameMetadataProvider() = default;
~~~

### RenderFrameMetadataProvider

RenderFrameMetadataProvider
~~~cpp
RenderFrameMetadataProvider(const RenderFrameMetadataProvider&) = delete;
~~~

### operator=

operator=
~~~cpp
RenderFrameMetadataProvider& operator=(const RenderFrameMetadataProvider&) =
      delete;
~~~

### ~RenderFrameMetadataProvider

~RenderFrameMetadataProvider
~~~cpp
virtual ~RenderFrameMetadataProvider() = default;
~~~

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(Observer* observer) = 0;
~~~

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(Observer* observer) = 0;
~~~

### LastRenderFrameMetadata

LastRenderFrameMetadata
~~~cpp
virtual const cc::RenderFrameMetadata& LastRenderFrameMetadata() = 0;
~~~

## class RenderFrameMetadataProvider
 Notifies all Observer of the submission of CompositorFrames which cause a
 change in RenderFrameMetadata.


 When ReportAllFrameSubmissionsForTesting(true) is called, this will be
 notified of all frame submissions.


 An Observer is provided, so that multiple sources can all observe the
 metadata for a given RenderWidgetHost.

### OnRenderFrameMetadataChangedAfterActivation

RenderFrameMetadataProvider::OnRenderFrameMetadataChangedAfterActivation
~~~cpp
virtual void OnRenderFrameMetadataChangedAfterActivation(
        base::TimeTicks activation_time) = 0;
~~~

### OnRenderFrameSubmission

RenderFrameMetadataProvider::OnRenderFrameSubmission
~~~cpp
virtual void OnRenderFrameSubmission() = 0;
~~~

### OnLocalSurfaceIdChanged

RenderFrameMetadataProvider::OnLocalSurfaceIdChanged
~~~cpp
virtual void OnLocalSurfaceIdChanged(
        const cc::RenderFrameMetadata& metadata) = 0;
~~~
 Called to indicate that the viz::LocalSurfaceId within the
 RenderFrameMetadata has changed. Note that this is called as
 soon as |metadata| arrives and does not wait for the frame token
 to pass in Viz.

### OnRootScrollOffsetChanged

OnRootScrollOffsetChanged
~~~cpp
virtual void OnRootScrollOffsetChanged(
        const gfx::PointF& root_scroll_offset) {}
~~~
