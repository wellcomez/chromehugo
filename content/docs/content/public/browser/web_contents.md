
## struct WebContents::CreateParams

### CreateParams

CreateParams::WebContents::CreateParams
~~~cpp
CreateParams(
        BrowserContext* context,
        base::Location creator_location = base::Location::Current())
~~~

### CreateParams

CreateParams::WebContents::CreateParams
~~~cpp
CreateParams(BrowserContext* context,
                 scoped_refptr<SiteInstance> site,
                 base::Location creator_location = base::Location::Current())
~~~

### CreateParams

CreateParams::WebContents::CreateParams
~~~cpp
CreateParams(const CreateParams& other)
~~~

### ~CreateParams

CreateParams::WebContents::~CreateParams
~~~cpp
~CreateParams()
~~~

## struct WebContents::CreateParams

### CreateParams

CreateParams::WebContents::CreateParams
~~~cpp
CreateParams(
        BrowserContext* context,
        base::Location creator_location = base::Location::Current())
~~~

### CreateParams

CreateParams::WebContents::CreateParams
~~~cpp
CreateParams(BrowserContext* context,
                 scoped_refptr<SiteInstance> site,
                 base::Location creator_location = base::Location::Current())
~~~

### CreateParams

CreateParams::WebContents::CreateParams
~~~cpp
CreateParams(const CreateParams& other)
~~~

### ~CreateParams

CreateParams::WebContents::~CreateParams
~~~cpp
~CreateParams()
~~~

## class WebContents
 WebContents is the core class in content/. A WebContents renders web content
 (usually HTML) in a rectangular area.

~~~cpp
 Instantiating one is simple:
   std::unique_ptr<content::WebContents> web_contents(
       content::WebContents::Create(
           content::WebContents::CreateParams(browser_context)));
   gfx::NativeView view = web_contents->GetNativeView();
   // |view| is an HWND, NSView*, etc.; insert it into the view hierarchy
   // wherever it needs to go.
~~~

 That's it; go to your kitchen, grab a scone, and chill. WebContents will do
 all the multi-process stuff behind the scenes. More details are at
 https://www.chromium.org/developers/design-documents/multi-process-architecture
 .
[multi-process-resource-loading](/chromehugo/docs/note/multi-process-resource-loading)

 The owner of `std::unique_ptr<content::WebContents> web_contents` is
 responsible for ensuring that `web_contents` are destroyed (e.g. closed)
 *before* the corresponding `browser_context` is destroyed.


 Each WebContents has a `NavigationController`, which can be obtained from
 `GetController()`, and is used to load URLs into the WebContents, navigate
 it backwards/forwards, etc.

 See navigation_controller.h for more details.


###  Create

 Creates a new WebContents.


 The caller is responsible for ensuring that the returned WebContents is
 destroyed (e.g. closed) *before* the BrowserContext associated with
 `params` is destroyed.  It is a security bug if WebContents haven't been
 destroyed when the destructor of BrowserContext starts running.  It is not
 necessarily a bug if WebContents haven't been destroyed when
 BrowserContext::NotifyWillBeDestroyed starts running.


 Best practices for managing the lifetime of `WebContents` and
 `BrowserContext` will vary across different //content embedders.  For
 example, for information specific to the //chrome layer, please see the
 "Managing lifetime of a Profile" section in
.
 
 [ //chrome/browser/profiles/README.md](/chromehugo/docs/chrome/browser/profiles/README)
~~~cpp
CONTENT_EXPORT static std::unique_ptr<WebContents> Create(
      const CreateParams& params);
~~~
###  CreateWithSessionStorage

 Similar to Create() above but should be used when you need to prepopulate
 the SessionStorageNamespaceMap of the WebContents. This can happen if
 you duplicate a WebContents, try to reconstitute it from a saved state,
 or when you create a new WebContents based on another one (eg., when
 servicing a window.open() call).


 You do not want to call this. If you think you do, make sure you completely
 understand when SessionStorageNamespace objects should be cloned, why
 they should not be shared by multiple WebContents, and what bad things
 can happen if you share the object.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<WebContents> CreateWithSessionStorage(
      const CreateParams& params,
      const SessionStorageNamespaceMap& session_storage_namespace_map);
~~~
### FromRenderViewHost

WebContents::FromRenderViewHost
~~~cpp
CONTENT_EXPORT static WebContents* FromRenderViewHost(RenderViewHost* rvh);
~~~
 Returns the WebContents that owns the RenderViewHost.


 WARNING: `rvh` may belong to a prerendered page, a page in the back/forward
 cache, or a pending deletion page, so it might be inappropriate for it to
 to trigger changes to the WebContents. See also the below comments for
 FromRenderFrameHost().

### FromRenderFrameHost

[render_frame_host](/chromehugo/docs/content/public/browser/render_frame_host)

WebContents::FromRenderFrameHost
~~~cpp
CONTENT_EXPORT static WebContents* FromRenderFrameHost(RenderFrameHost* rfh);
~~~
 Returns the WebContents for the RenderFrameHost. It is unsafe to call this
 function with an invalid (e.g. destructed) `rfh`.


 WARNING: It might be inappropriate for `rfh` to trigger changes to the
 WebContents, so be careful when calling this. Some cases to be aware of
 are:
 * Pages/documents which are not active are not observable by the user
   and therefore should not show UI elements (e.g., a colour picker). These
   features should use `rfh->IsActive()` to determine whether `rfh` is
   active. See the comments there for more information.

 * Pages/documents which are not primary generally should not update
   per-WebContents state (e.g., theme colour). Use
   `rfh->GetPage().IsPrimary()` to check for primary. Fenced frames are
   one case where a RenderFrameHost can be active but not primary.

### FromFrameTreeNodeId

WebContents::FromFrameTreeNodeId
~~~cpp
CONTENT_EXPORT static WebContents* FromFrameTreeNodeId(
      int frame_tree_node_id);
~~~
 Returns the WebContents associated with the |frame_tree_node_id|. This may
 return nullptr if the RenderFrameHost is shutting down.

### SetScreenOrientationDelegate

WebContents::SetScreenOrientationDelegate
~~~cpp
CONTENT_EXPORT static void SetScreenOrientationDelegate(
      ScreenOrientationDelegate* delegate);
~~~
 Sets delegate for platform specific screen orientation functionality.

### ~WebContents

~WebContents
~~~cpp
~WebContents() override = default;
~~~

### GetDelegate

GetDelegate
~~~cpp
virtual WebContentsDelegate* GetDelegate() = 0;
~~~
###  Intrinsic tab state 
-------------------------------------------------------
 Gets/Sets the delegate.

### SetDelegate

SetDelegate
~~~cpp
virtual void SetDelegate(WebContentsDelegate* delegate) = 0;
~~~

### GetController

GetController
~~~cpp
virtual NavigationController& GetController() = 0;
~~~
 Gets the NavigationController for primary frame tree of this WebContents.

 See comments on NavigationController for more details.

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual content::BrowserContext* GetBrowserContext() = 0;
~~~
 Returns the user browser context associated with this WebContents (via the
 NavigationController).

### GetWeakPtr

GetWeakPtr
~~~cpp
virtual base::WeakPtr<WebContents> GetWeakPtr() = 0;
~~~
 Returns a weak pointer.

### GetURL

GetURL
~~~cpp
virtual const GURL& GetURL() = 0;
~~~
 Gets the URL that is currently being displayed, if there is one.

 This method is deprecated. DO NOT USE! Pick either |GetVisibleURL| or
 |GetLastCommittedURL| as appropriate.

### GetVisibleURL

GetVisibleURL
~~~cpp
virtual const GURL& GetVisibleURL() = 0;
~~~
 Gets the virtual URL currently being displayed in the URL bar, if there is
 one. This URL might be a pending navigation that hasn't committed yet, so
 it is not guaranteed to match the current page in this WebContents.

### GetLastCommittedURL

GetLastCommittedURL
~~~cpp
virtual const GURL& GetLastCommittedURL() = 0;
~~~
 Gets the virtual URL of the last committed page in this WebContents.

 Virtual URLs are meant to be displayed to the user (e.g., they include the
 "view-source:" prefix for view source URLs, unlike NavigationEntry::GetURL
 and NavigationHandle::GetURL). The last committed page is the current
 security context and the content that is actually displayed within the tab.

 See also GetVisibleURL above, which may differ from this URL. Note that
 this might return an empty GURL if no navigation has committed in the
 WebContents' main frame.

### GetPrimaryMainFrame

GetPrimaryMainFrame
~~~cpp
virtual RenderFrameHost* GetPrimaryMainFrame() = 0;
~~~
 Returns the primary main frame for the currently active page. Always
 non-null except during WebContents destruction. This WebContents may
 have additional main frames for prerendered pages, bfcached pages, etc.

 See docs/frame_trees.md for more details.

### GetPrimaryPage

GetPrimaryPage
~~~cpp
virtual Page& GetPrimaryPage() = 0;
~~~
 Returns the current page in the primary frame tree of this WebContents.

 If this WebContents is associated with an omnibox, usually the URL of the
 main document of this page will be displayed in it.


 Primary page can change as a result of a navigation, both to a new page
 (navigation loading a new main document) and an existing one (when
 restoring the page from back/forward cache or activating a prerendering
 page). This change can be observed using
 WebContentsObserver::PrimaryPageChanged, see the comments there for more
 details.


 The primary page's lifetime corresponds to its main document's lifetime
 and may differ from a RenderFrameHost's lifetime (for cross-document same
 RenderFrameHost navigations).


 Apart from the primary page, additional pages might be associated with this
 WebContents:
 - Pending commit pages (which will become primary after-and-if the ongoing
   main frame navigation successfully commits).

 - Pending deletion pages (pages the user has navigated from, but which are
   still alive as they are running unload handlers in background).

 - Pages in back/forward cache (which can be navigated to later).

 - Prerendered pages (pages which are loading in the background in
   anticipation of user navigating to them).


 Given the existence of multiple pages, in many cases (especially when
 handling IPCs from the renderer process), calling GetPrimaryPage would not
 be appropriate as it might return a wrong page. If the code already has a
 reference to RenderFrameHost or a Page (e.g. each IPC from the renderer
 process should be associated with a particular RenderFrameHost), it should
 be used instead of getting the primary page from the WebContents.

 See docs/frame_trees.md for more details.

### GetFocusedFrame

GetFocusedFrame
~~~cpp
virtual RenderFrameHost* GetFocusedFrame() = 0;
~~~
 Returns the focused frame for the primary page or an inner page thereof.

 Might be nullptr if nothing is focused.

### IsPrerenderedFrame

IsPrerenderedFrame
~~~cpp
virtual bool IsPrerenderedFrame(int frame_tree_node_id) = 0;
~~~
 Returns true if |frame_tree_node_id| refers to a frame in a prerendered
 page.

 TODO(1196715, 1232528): This will be extended to also return true if it is
 in an inner page of a prerendered page.

### UnsafeFindFrameByFrameTreeNodeId

UnsafeFindFrameByFrameTreeNodeId
~~~cpp
virtual RenderFrameHost* UnsafeFindFrameByFrameTreeNodeId(
      int frame_tree_node_id) = 0;
~~~
 NOTE: This is generally unsafe to use. A frame's RenderFrameHost may
 change over its lifetime, such as during cross-process navigation (and
 thus privilege change). Use RenderFrameHost::FromID instead wherever
 possible.


 Given a FrameTreeNode ID that belongs to this WebContents, returns the
 current RenderFrameHost regardless of which FrameTree it is in.


 See RenderFrameHost::GetFrameTreeNodeId for documentation on this ID.

### ForEachRenderFrameHostWithAction

ForEachRenderFrameHostWithAction
~~~cpp
virtual void ForEachRenderFrameHostWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHost*)> on_frame) = 0;
~~~

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
virtual void ForEachRenderFrameHost(
      base::FunctionRef<void(RenderFrameHost*)> on_frame) = 0;
~~~

### GetRenderViewHost

GetRenderViewHost
~~~cpp
virtual RenderViewHost* GetRenderViewHost() = 0;
~~~
 Gets the current RenderViewHost for this tab.

### GetRenderWidgetHostView

GetRenderWidgetHostView
~~~cpp
virtual RenderWidgetHostView* GetRenderWidgetHostView() = 0;
~~~
 Returns the currently active RenderWidgetHostView. This may change over
 time and can be nullptr (during setup and teardown).

### GetTopLevelRenderWidgetHostView

GetTopLevelRenderWidgetHostView
~~~cpp
virtual RenderWidgetHostView* GetTopLevelRenderWidgetHostView() = 0;
~~~
 Returns the outermost RenderWidgetHostView. This will return the platform
 specific RenderWidgetHostView (as opposed to
 RenderWidgetHostViewChildFrame), which can be used to create context
 menus.

### RequestAXTreeSnapshot

RequestAXTreeSnapshot
~~~cpp
virtual void RequestAXTreeSnapshot(AXTreeSnapshotCallback callback,
                                     ui::AXMode ax_mode,
                                     bool exclude_offscreen,
                                     size_t max_nodes,
                                     base::TimeDelta timeout) = 0;
~~~

### ClosePage

ClosePage
~~~cpp
virtual void ClosePage() = 0;
~~~
 Causes the current page to be closed, including running its onunload event
 handler.

### GetThemeColor

GetThemeColor
~~~cpp
virtual absl::optional<SkColor> GetThemeColor() = 0;
~~~
 Returns the theme color for the underlying content as set by the
 theme-color meta tag if any.

### GetBackgroundColor

GetBackgroundColor
~~~cpp
virtual absl::optional<SkColor> GetBackgroundColor() = 0;
~~~
 Returns the background color for the underlying content as set by CSS if
 any.

### SetPageBaseBackgroundColor

SetPageBaseBackgroundColor
~~~cpp
virtual void SetPageBaseBackgroundColor(absl::optional<SkColor> color) = 0;
~~~
 Sets the renderer-side default background color of the page. This is used
 when the page has not loaded enough to know a background color or if the
 page does not set a background color.

 Pass in nullopt to reset back to the default.

 Note there are situations where the base background color is not used, such
 as fullscreen.

 Note currently this is sent directly to the renderer, so does not interact
 directly with `RenderWidgetHostView::SetBackgroundColor`. There is pending
 refactor to remove `RenderWidgetHostView::SetBackgroundColor` and merge its
 functionality here, which will be more consistent and simpler to
 understand.

### SetColorProviderSource

SetColorProviderSource
~~~cpp
virtual void SetColorProviderSource(ui::ColorProviderSource* source) = 0;
~~~
 Sets the ColorProviderSource for the WebContents. The WebContents will
 maintain an observation of `source` until a new source is set or the
 current source is destroyed. WebContents will receive updates when the
 source's ColorProvider changes.

### GetColorProvider

GetColorProvider
~~~cpp
virtual const ui::ColorProvider& GetColorProvider() const = 0;
~~~
 Returns the ColorProvider instance for this WebContents object. This will
 always return a valid ColorProvider instance.

### GetWebUI

GetWebUI
~~~cpp
virtual WebUI* GetWebUI() = 0;
~~~
 Returns the committed WebUI if one exists.

### SetUserAgentOverride

SetUserAgentOverride
~~~cpp
virtual void SetUserAgentOverride(const blink::UserAgentOverride& ua_override,
                                    bool override_in_new_tabs) = 0;
~~~
 Sets the user-agent that may be used for navigations in this WebContents.

 The user-agent is *only* used when
 NavigationEntry::SetIsOverridingUserAgent(true) is used (the value of
 is-overriding-user-agent may be specified in LoadURLParams). If
 |override_in_new_tabs| is true, and the first navigation in the tab is
 renderer initiated, then is-overriding-user-agent is set to true for the
 NavigationEntry. See SetRendererInitiatedUserAgentOverrideOption() for
 details on how renderer initiated navigations are configured.


 If nonempty, |ua_override|'s value must not contain '\0', '\r', or '\n' (in
 other words, it must be a valid HTTP header value).

### SetRendererInitiatedUserAgentOverrideOption

SetRendererInitiatedUserAgentOverrideOption
~~~cpp
virtual void SetRendererInitiatedUserAgentOverrideOption(
      NavigationController::UserAgentOverrideOption option) = 0;
~~~
 Configures the value of is-overriding-user-agent for renderer initiated
 navigations. The default is UA_OVERRIDE_INHERIT. This value does not apply
 to the first renderer initiated navigation if the tab has no navigations.

 See SetUserAgentOverride() for details on that.

### GetUserAgentOverride

GetUserAgentOverride
~~~cpp
virtual const blink::UserAgentOverride& GetUserAgentOverride() = 0;
~~~

### EnableWebContentsOnlyAccessibilityMode

EnableWebContentsOnlyAccessibilityMode
~~~cpp
virtual void EnableWebContentsOnlyAccessibilityMode() = 0;
~~~
 Set the accessibility mode so that accessibility events are forwarded
 to each WebContentsObserver.

### IsWebContentsOnlyAccessibilityModeForTesting

IsWebContentsOnlyAccessibilityModeForTesting
~~~cpp
virtual bool IsWebContentsOnlyAccessibilityModeForTesting() = 0;
~~~
 Returns true only if the WebContentsObserver accessibility mode is
 enabled.

### IsFullAccessibilityModeForTesting

IsFullAccessibilityModeForTesting
~~~cpp
virtual bool IsFullAccessibilityModeForTesting() = 0;
~~~
 Returns true only if complete accessibility mode is on, meaning there's
 both renderer accessibility, and a native browser accessibility tree.

### GetAccessibilityMode

GetAccessibilityMode
~~~cpp
virtual ui::AXMode GetAccessibilityMode() = 0;
~~~

### SetAccessibilityMode

SetAccessibilityMode
~~~cpp
virtual void SetAccessibilityMode(ui::AXMode mode) = 0;
~~~

### DumpAccessibilityTree

DumpAccessibilityTree
~~~cpp
virtual std::string DumpAccessibilityTree(
      bool internal,
      std::vector<ui::AXPropertyFilter> property_filters) = 0;
~~~

### RecordAccessibilityEvents

RecordAccessibilityEvents
~~~cpp
virtual void RecordAccessibilityEvents(
      bool start_recording,
      absl::optional<AccessibilityEventCallback> callback) = 0;
~~~
 Starts or stops recording accessibility events. |start_recording| is true
 when recording should start and false when recording should stop.

 |callback| is an optional function which is called when an accessibility
 event is received while accessibility events are being recorded. When
 |start_recording| is true, it is expected that |callback| has a value; when
 |start_recording| is false, it is expected that |callback| does not.

### GetTitle

GetTitle
~~~cpp
virtual const std::u16string& GetTitle() = 0;
~~~
###  Tab navigation state 
------------------------------------------------------
 Returns the current navigation properties, which if a navigation is
 pending may be provisional (e.g., the navigation could result in a
 download, in which case the URL would revert to what it was previously).

### UpdateTitleForEntry

UpdateTitleForEntry
~~~cpp
virtual void UpdateTitleForEntry(NavigationEntry* entry,
                                   const std::u16string& title) = 0;
~~~
 Saves the given title to the navigation entry and does associated work. It
 will update history and the view with the new title, and also synthesize
 titles for file URLs that have none. Thus |entry| must have a URL set.

### GetSiteInstance

GetSiteInstance
~~~cpp
virtual SiteInstance* GetSiteInstance() = 0;
~~~
 Returns the SiteInstance associated with the current page.

### IsLoading

IsLoading
~~~cpp
virtual bool IsLoading() = 0;
~~~
 Returns whether this WebContents is loading a resource.

### GetLoadProgress

GetLoadProgress
~~~cpp
virtual double GetLoadProgress() = 0;
~~~
 Returns the current load progress.

### ShouldShowLoadingUI

ShouldShowLoadingUI
~~~cpp
virtual bool ShouldShowLoadingUI() = 0;
~~~
 Returns whether a navigation is currently in progress that should show
 loading UI if such UI exists (progress bar, loading spinner, stop button,
 etc.) True for different-document navigations and the navigation API's
 intercept(). This being true implies that IsLoading() is also true.

### IsDocumentOnLoadCompletedInPrimaryMainFrame

IsDocumentOnLoadCompletedInPrimaryMainFrame
~~~cpp
virtual bool IsDocumentOnLoadCompletedInPrimaryMainFrame() = 0;
~~~
 Returns whether the current primary main document has reached and finished
 executing its onload() handler. Corresponds to
 WebContentsObserver::DocumentOnLoadCompletedInPrimaryMainFrame() and see
 comments there for more details.

### IsWaitingForResponse

IsWaitingForResponse
~~~cpp
virtual bool IsWaitingForResponse() = 0;
~~~
 Returns whether this WebContents is waiting for a first-response for the
 main resource of the page.

### GetLoadState

GetLoadState
~~~cpp
virtual const net::LoadStateWithParam& GetLoadState() = 0;
~~~
 Returns the current load state and the URL associated with it.

 The load state is only updated while IsLoading() is true.

### GetLoadStateHost

GetLoadStateHost
~~~cpp
virtual const std::u16string& GetLoadStateHost() = 0;
~~~

### GetUploadSize

GetUploadSize
~~~cpp
virtual uint64_t GetUploadSize() = 0;
~~~
 Returns the upload progress.

### GetUploadPosition

GetUploadPosition
~~~cpp
virtual uint64_t GetUploadPosition() = 0;
~~~

### GetEncoding

GetEncoding
~~~cpp
virtual const std::string& GetEncoding() = 0;
~~~
 Returns the character encoding of the page.

### WasDiscarded

WasDiscarded
~~~cpp
virtual bool WasDiscarded() = 0;
~~~
 Indicates that the tab was previously discarded.

 wasDiscarded is exposed on Document after discard, see:
 https://github.com/WICG/web-lifecycle
 When a tab is discarded, WebContents sets was_discarded on its
 root FrameTreeNode.

 In addition, when a child frame is created, this bit is passed on from
 parent to child.

 When a navigation request is created, was_discarded is passed on to the
 request and reset to false in FrameTreeNode.

### SetWasDiscarded

SetWasDiscarded
~~~cpp
virtual void SetWasDiscarded(bool was_discarded) = 0;
~~~

### AboutToBeDiscarded

AboutToBeDiscarded
~~~cpp
virtual void AboutToBeDiscarded(WebContents* new_contents) = 0;
~~~
 Notifies observers that this WebContents is about to be discarded, and
 replaced with `new_contents`. See the comment on
 WebContentsObserver::AboutToBeDiscarded.

### IncrementCapturerCount

IncrementCapturerCount
~~~cpp
[[nodiscard]] virtual base::ScopedClosureRunner IncrementCapturerCount(
      const gfx::Size& capture_size,
      bool stay_hidden,
      bool stay_awake,
      bool is_activity = true) = 0;
~~~
###  Internal state 
------------------------------------------------------------
 Indicates whether the WebContents is being captured (e.g., for screenshots,
 or mirroring video and/or audio). Each IncrementCapturerCount() call must
 be balanced with a corresponding DecrementCapturerCount() call.


 Both internal-to-content and embedders must increment the capturer count
 while capturing to ensure "hidden rendering" optimizations are disabled.

 For example, renderers will be configured to produce compositor frames
 regardless of their "backgrounded" or on-screen occlusion state.


 Embedders can detect whether a WebContents is being captured (see
 IsBeingCaptured() below) and use this, for example, to provide an
 alternative user interface. So, developers should be careful to understand
 the side-effects from using or changing these APIs, both upstream and
 downstream of this API layer.


 Callers must hold onto the returned base::ScopedClosureRunner until they
 are done capturing.


 |capture_size| is only used in the case of mirroring (i.e., screen capture
 video); otherwise, an empty gfx::Size should be provided. This specifies
 the capturer's target video resolution, but can be empty to mean
 "unspecified." This becomes a temporary override to GetPreferredSize(),
 allowing embedders to size the WebContents on-screen views for optimal
 capture quality.


 |stay_hidden| affects the page visibility state of the renderers (i.e., a
 web page can be made aware of whether it is actually user-visible). If
 true, the show/hide state of the WebContents will be passed to the
 renderers, like normal. If false, the renderers will always be told they
 are user-visible while being captured.


 |stay_awake| will cause a WakeLock to be held which prevents system sleep.


 |is_activity| means the capture will cause the last active time to be
 updated.

### GetCaptureHandleConfig

GetCaptureHandleConfig
~~~cpp
virtual const blink::mojom::CaptureHandleConfig& GetCaptureHandleConfig() = 0;
~~~
 Getter for the capture handle, which allows a captured application to
 opt-in to exposing information to its capturer(s).

### IsBeingCaptured

IsBeingCaptured
~~~cpp
virtual bool IsBeingCaptured() = 0;
~~~
 Returns true if audio/screenshot/video is being captured by the embedder,
 as indicated by calls to IncrementCapturerCount().

### IsBeingVisiblyCaptured

IsBeingVisiblyCaptured
~~~cpp
virtual bool IsBeingVisiblyCaptured() = 0;
~~~
 Returns true if audio/screenshot/video is being captured by the embedder
 and renderers are being told they are always user-visible, as indicated by
 calls to IncrementCapturerCount().

### IsAudioMuted

IsAudioMuted
~~~cpp
virtual bool IsAudioMuted() = 0;
~~~
 Indicates/Sets whether all audio output from this WebContents is muted.

 This does not affect audio capture, just local/system output.

### SetAudioMuted

SetAudioMuted
~~~cpp
virtual void SetAudioMuted(bool mute) = 0;
~~~

### IsCurrentlyAudible

IsCurrentlyAudible
~~~cpp
virtual bool IsCurrentlyAudible() = 0;
~~~
 Returns true if the audio is currently audible.

### IsConnectedToBluetoothDevice

IsConnectedToBluetoothDevice
~~~cpp
virtual bool IsConnectedToBluetoothDevice() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a Bluetooth
 Device.

### IsScanningForBluetoothDevices

IsScanningForBluetoothDevices
~~~cpp
virtual bool IsScanningForBluetoothDevices() = 0;
~~~
 Indicates whether any frame in the WebContents is scanning for Bluetooth
 devices.

### IsConnectedToSerialPort

IsConnectedToSerialPort
~~~cpp
virtual bool IsConnectedToSerialPort() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a serial
 port.

### IsConnectedToHidDevice

IsConnectedToHidDevice
~~~cpp
virtual bool IsConnectedToHidDevice() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a HID
 device.

### IsConnectedToUsbDevice

IsConnectedToUsbDevice
~~~cpp
virtual bool IsConnectedToUsbDevice() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a USB
 device.

### HasFileSystemAccessHandles

HasFileSystemAccessHandles
~~~cpp
virtual bool HasFileSystemAccessHandles() = 0;
~~~
 Indicates whether any frame in the WebContents has File System Access
 handles.

### HasPictureInPictureVideo

HasPictureInPictureVideo
~~~cpp
virtual bool HasPictureInPictureVideo() = 0;
~~~
 Indicates whether a video is in Picture-in-Picture for |this|.

### HasPictureInPictureDocument

HasPictureInPictureDocument
~~~cpp
virtual bool HasPictureInPictureDocument() = 0;
~~~
 Indicates whether a document is in Picture-in-Picture for |this|.

### IsCrashed

IsCrashed
~~~cpp
virtual bool IsCrashed() = 0;
~~~
 Indicates whether this tab should be considered crashed. This becomes false
 again when the renderer process is recreated after a crash in order to
 recreate the main frame.

### GetCrashedStatus

GetCrashedStatus
~~~cpp
virtual base::TerminationStatus GetCrashedStatus() = 0;
~~~

### GetCrashedErrorCode

GetCrashedErrorCode
~~~cpp
virtual int GetCrashedErrorCode() = 0;
~~~

### IsBeingDestroyed

IsBeingDestroyed
~~~cpp
virtual bool IsBeingDestroyed() = 0;
~~~
 Whether the tab is in the process of being destroyed.

### NotifyNavigationStateChanged

NotifyNavigationStateChanged
~~~cpp
virtual void NotifyNavigationStateChanged(InvalidateTypes changed_flags) = 0;
~~~
 Convenience method for notifying the delegate of a navigation state
 change.

### OnAudioStateChanged

OnAudioStateChanged
~~~cpp
virtual void OnAudioStateChanged() = 0;
~~~
 Notifies the WebContents that audio state has changed. The contents is
 aware of all of its potential sources of audio and needs to poll them
 directly to determine its aggregate audio state.

### GetLastActiveTime

GetLastActiveTime
~~~cpp
virtual base::TimeTicks GetLastActiveTime() = 0;
~~~
 Get/Set the last time that the WebContents was made active (either when it
 was created or shown with WasShown()).

### WasShown

WasShown
~~~cpp
virtual void WasShown() = 0;
~~~
 Invoked when the WebContents becomes shown/hidden. A hidden WebContents
 isn't painted on the screen.

### WasHidden

WasHidden
~~~cpp
virtual void WasHidden() = 0;
~~~

### WasOccluded

WasOccluded
~~~cpp
virtual void WasOccluded() = 0;
~~~
 Invoked when the WebContents becomes occluded. An occluded WebContents
 isn't painted on the screen, except in a window switching feature (e.g.

 Alt-Tab).

### GetVisibility

GetVisibility
~~~cpp
virtual Visibility GetVisibility() = 0;
~~~
 Returns the visibility of the WebContents' view.

### NeedToFireBeforeUnloadOrUnloadEvents

NeedToFireBeforeUnloadOrUnloadEvents
~~~cpp
virtual bool NeedToFireBeforeUnloadOrUnloadEvents() = 0;
~~~
 This function checks *all* frames in this WebContents (not just the main
 frame) and returns true if at least one frame has either a beforeunload or
 an unload/pagehide/visibilitychange handler.


 The value of this may change over time. For example, if true and the
 beforeunload listener is executed and allows the user to exit, then this
 returns false.

### DispatchBeforeUnload

DispatchBeforeUnload
~~~cpp
virtual void DispatchBeforeUnload(bool auto_cancel) = 0;
~~~
 Runs the beforeunload handler for the main frame and all its subframes.

 See also ClosePage in RenderViewHostImpl, which runs the unload handler.

 If |auto_cancel| is true, and the beforeunload handler returns a non-empty
 string (indicating the page wants to present a confirmation dialog), then
 the beforeunload operation will automatically return with |proceed=false|
 and no dialog will be shown to the user. This is used to interrupt a
 potential discard without causing the dialog to appear.

### AttachInnerWebContents

AttachInnerWebContents
~~~cpp
virtual void AttachInnerWebContents(
      std::unique_ptr<WebContents> inner_web_contents,
      RenderFrameHost* render_frame_host,
      mojo::PendingAssociatedRemote<blink::mojom::RemoteFrame> remote_frame,
      mojo::PendingAssociatedReceiver<blink::mojom::RemoteFrameHost>
          remote_frame_host_receiver,
      bool is_full_page) = 0;
~~~
 Attaches |inner_web_contents| to the container frame |render_frame_host|,
 which must be in a FrameTree for this WebContents. This outer WebContents
 takes ownership of |inner_web_contents|.

 Note: |render_frame_host| will be swapped out and destroyed during the
 process. Generally a frame same-process with its parent is the right choice
 but ideally it should be "about:blank" to avoid problems with beforeunload.

 To ensure sane usage of this API users first should call the async API
 RenderFrameHost::PrepareForInnerWebContentsAttach first.

 Note: If |is_full_page| is true, focus will be given to the inner
 WebContents.

### IsInnerWebContentsForGuest

IsInnerWebContentsForGuest
~~~cpp
virtual bool IsInnerWebContentsForGuest() = 0;
~~~
 Returns whether this WebContents is an inner WebContents for a guest.

 Important: please avoid using this in new callsites, and use
 GetOuterWebContents instead.

### IsPortal

IsPortal
~~~cpp
virtual bool IsPortal() = 0;
~~~
 Returns whether this WebContents is a portal. This returns true even when
 this WebContents is not attached to its portal host's WebContents tree.

 This value may change over time due to portal activation and adoption.

### GetPortalHostWebContents

GetPortalHostWebContents
~~~cpp
virtual WebContents* GetPortalHostWebContents() = 0;
~~~
 If |IsPortal()| is true, returns this WebContents' portal host's
 WebContents. Otherwise, returns nullptr.

### GetOuterWebContentsFrame

GetOuterWebContentsFrame
~~~cpp
virtual RenderFrameHost* GetOuterWebContentsFrame() = 0;
~~~
 Returns the outer WebContents frame, the same frame that this WebContents
 was attached in AttachToOuterWebContentsFrame().

### GetOuterWebContents

GetOuterWebContents
~~~cpp
virtual WebContents* GetOuterWebContents() = 0;
~~~
 Returns the outer WebContents of this WebContents if any.

 Otherwise, return nullptr.

### GetOutermostWebContents

GetOutermostWebContents
~~~cpp
virtual WebContents* GetOutermostWebContents() = 0;
~~~
 Returns the root WebContents of the WebContents tree. Always returns
 non-null value.

### GetInnerWebContents

GetInnerWebContents
~~~cpp
virtual std::vector<WebContents*> GetInnerWebContents() = 0;
~~~
 Returns a vector to the inner WebContents within this WebContents.

### GetResponsibleWebContents

GetResponsibleWebContents
~~~cpp
virtual WebContents* GetResponsibleWebContents() = 0;
~~~
 Returns the user-visible WebContents that is responsible for the UI
 activity in the provided WebContents. For example, this delegate may be
 aware that the contents is embedded in some other contents, or hosts
 background activity on behalf of a user-visible tab which should be used to
 display dialogs and similar affordances to the user.


 This may be distinct from the outer web contents (for example, the
 responsible contents may logically "own" a contents but not currently embed
 it for rendering).


 Always returns a non-null value.

### DidChangeVisibleSecurityState

DidChangeVisibleSecurityState
~~~cpp
virtual void DidChangeVisibleSecurityState() = 0;
~~~
 Invoked when visible security state changes.

### SyncRendererPrefs

SyncRendererPrefs
~~~cpp
virtual void SyncRendererPrefs() = 0;
~~~
 Sends the current preferences to all renderer processes for the current
 page.

### Stop

Stop
~~~cpp
virtual void Stop() = 0;
~~~
###  Commands 
------------------------------------------------------------------
 Stop any pending navigation.

### SetPageFrozen

SetPageFrozen
~~~cpp
virtual void SetPageFrozen(bool frozen) = 0;
~~~
 Freezes or unfreezes the current page. A frozen page runs as few tasks as
 possible. This cannot be called when the page is visible. If the page is
 made visible after this is called, it is automatically unfrozen.

### Clone

Clone
~~~cpp
virtual std::unique_ptr<WebContents> Clone() = 0;
~~~
 Creates a new WebContents with the same state as this one. The returned
 heap-allocated pointer is owned by the caller.

### ReloadFocusedFrame

ReloadFocusedFrame
~~~cpp
virtual void ReloadFocusedFrame() = 0;
~~~
 Reloads the focused frame.

### Undo

Undo
~~~cpp
virtual void Undo() = 0;
~~~
###  Editing commands 
----------------------------------------------------------
### Redo

Redo
~~~cpp
virtual void Redo() = 0;
~~~

### Cut

Cut
~~~cpp
virtual void Cut() = 0;
~~~

### Copy

Copy
~~~cpp
virtual void Copy() = 0;
~~~

### CopyToFindPboard

CopyToFindPboard
~~~cpp
virtual void CopyToFindPboard() = 0;
~~~

### CenterSelection

CenterSelection
~~~cpp
virtual void CenterSelection() = 0;
~~~

### Paste

Paste
~~~cpp
virtual void Paste() = 0;
~~~

### PasteAndMatchStyle

PasteAndMatchStyle
~~~cpp
virtual void PasteAndMatchStyle() = 0;
~~~

### Delete

Delete
~~~cpp
virtual void Delete() = 0;
~~~

### SelectAll

SelectAll
~~~cpp
virtual void SelectAll() = 0;
~~~

### CollapseSelection

CollapseSelection
~~~cpp
virtual void CollapseSelection() = 0;
~~~

### ScrollToTopOfDocument

ScrollToTopOfDocument
~~~cpp
virtual void ScrollToTopOfDocument() = 0;
~~~

### ScrollToBottomOfDocument

ScrollToBottomOfDocument
~~~cpp
virtual void ScrollToBottomOfDocument() = 0;
~~~

### AdjustSelectionByCharacterOffset

AdjustSelectionByCharacterOffset
~~~cpp
virtual void AdjustSelectionByCharacterOffset(int start_adjust,
                                                int end_adjust,
                                                bool show_selection_menu) = 0;
~~~
 Adjust the selection starting and ending points in the focused frame by
 the given amounts. A negative amount moves the selection towards the
 beginning of the document, a positive amount moves the selection towards
 the end of the document.

### Replace

Replace
~~~cpp
virtual void Replace(const std::u16string& word) = 0;
~~~
 Replaces the currently selected word or a word around the cursor.

### ReplaceMisspelling

ReplaceMisspelling
~~~cpp
virtual void ReplaceMisspelling(const std::u16string& word) = 0;
~~~
 Replaces the misspelling in the current selection.

### NotifyContextMenuClosed

NotifyContextMenuClosed
~~~cpp
virtual void NotifyContextMenuClosed(const GURL& link_followed) = 0;
~~~
 Let the renderer know that the menu has been closed.

### ExecuteCustomContextMenuCommand

ExecuteCustomContextMenuCommand
~~~cpp
virtual void ExecuteCustomContextMenuCommand(int action,
                                               const GURL& link_followed) = 0;
~~~
 Executes custom context menu action that was provided from Blink.

### GetNativeView

GetNativeView
~~~cpp
virtual gfx::NativeView GetNativeView() = 0;
~~~
###  Views and focus 
-----------------------------------------------------------
 Returns the native widget that contains the contents of the tab.

### GetContentNativeView

GetContentNativeView
~~~cpp
virtual gfx::NativeView GetContentNativeView() = 0;
~~~
 Returns the native widget with the main content of the tab (i.e. the main
 render view host, though there may be many popups in the tab as children of
 the container).

### GetTopLevelNativeWindow

GetTopLevelNativeWindow
~~~cpp
virtual gfx::NativeWindow GetTopLevelNativeWindow() = 0;
~~~
 Returns the outermost native view. This will be used as the parent for
 dialog boxes.

### GetContainerBounds

GetContainerBounds
~~~cpp
virtual gfx::Rect GetContainerBounds() = 0;
~~~
 Computes the rectangle for the native widget that contains the contents of
 the tab in the screen coordinate system.

### GetViewBounds

GetViewBounds
~~~cpp
virtual gfx::Rect GetViewBounds() = 0;
~~~
 Get the bounds of the View, relative to the parent.

### Resize

Resize
~~~cpp
virtual void Resize(const gfx::Rect& new_bounds) = 0;
~~~
 Resize a WebContents to |new_bounds|.

### GetSize

GetSize
~~~cpp
virtual gfx::Size GetSize() = 0;
~~~
 Get the size of a WebContents.

### GetDropData

GetDropData
~~~cpp
virtual DropData* GetDropData() = 0;
~~~
 Returns the current drop data, if any.

### Focus

Focus
~~~cpp
virtual void Focus() = 0;
~~~
 Sets focus to the native widget for this tab.

### SetInitialFocus

SetInitialFocus
~~~cpp
virtual void SetInitialFocus() = 0;
~~~
 Sets focus to the appropriate element when the WebContents is shown the
 first time.

### StoreFocus

StoreFocus
~~~cpp
virtual void StoreFocus() = 0;
~~~
 Stores the currently focused view.

### RestoreFocus

RestoreFocus
~~~cpp
virtual void RestoreFocus() = 0;
~~~
 Restores focus to the last focus view. If StoreFocus has not yet been
 invoked, SetInitialFocus is invoked.

### FocusThroughTabTraversal

FocusThroughTabTraversal
~~~cpp
virtual void FocusThroughTabTraversal(bool reverse) = 0;
~~~
 Focuses the first (last if |reverse| is true) element in the page.

 Invoked when this tab is getting the focus through tab traversal (|reverse|
 is true when using Shift-Tab).

### IsSavable

IsSavable
~~~cpp
virtual bool IsSavable() = 0;
~~~
###  Misc state & callbacks 
----------------------------------------------------
 Check whether we can do the saving page operation this page given its MIME
 type.

### OnSavePage

OnSavePage
~~~cpp
virtual void OnSavePage() = 0;
~~~
 Prepare for saving the current web page to disk.

### SavePage

SavePage
~~~cpp
virtual bool SavePage(const base::FilePath& main_file,
                        const base::FilePath& dir_path,
                        SavePageType save_type) = 0;
~~~
 Save page with the main HTML file path, the directory for saving resources,
 and the save type: HTML only or complete web page. Returns true if the
 saving process has been initiated successfully.

### SaveFrame

SaveFrame
~~~cpp
virtual void SaveFrame(const GURL& url,
                         const Referrer& referrer,
                         RenderFrameHost* rfh) = 0;
~~~
 Saves the given frame's URL to the local filesystem. If `rfh` is provided,
 the saving is performed in its context. For example, the associated
 navigation isolation info will be used for making the network request.

### SaveFrameWithHeaders

SaveFrameWithHeaders
~~~cpp
virtual void SaveFrameWithHeaders(const GURL& url,
                                    const Referrer& referrer,
                                    const std::string& headers,
                                    const std::u16string& suggested_filename,
                                    RenderFrameHost* rfh) = 0;
~~~
 Saves the given frame's URL to the local filesystem. The headers, if
 provided, is used to make a request to the URL rather than using cache.

 Format of |headers| is a new line separated list of key value pairs:
 "<key1>: <value1>\r\n<key2>: <value2>". The saving is performed in the
 context of `rfh`. For example, the associated navigation isolation info
 will be used for making the network request.

### GenerateMHTML

GenerateMHTML
~~~cpp
virtual void GenerateMHTML(
      const MHTMLGenerationParams& params,
      base::OnceCallback<void(int64_t /* file_size */)> callback) = 0;
~~~
 Generate an MHTML representation of the current page conforming to the
 settings provided by |params| and returning final status information via
 the callback. See MHTMLGenerationParams for details on generation settings.

 A resulting |file_size| of -1 represents a failure. Any other value
 represents the size of the successfully generated file.


 TODO(https://crbug.com/915966): GenerateMHTML will eventually be removed
 and GenerateMHTMLWithResult will be renamed to GenerateMHTML to replace it.

 Both GenerateMHTML and GenerateMHTMLWithResult perform the same operation.

 however, GenerateMHTMLWithResult provides a struct as output, that contains
 the file size and more.

### GenerateMHTMLWithResult

GenerateMHTMLWithResult
~~~cpp
virtual void GenerateMHTMLWithResult(
      const MHTMLGenerationParams& params,
      MHTMLGenerationResult::GenerateMHTMLCallback callback) = 0;
~~~

### GetContentsMimeType

GetContentsMimeType
~~~cpp
virtual const std::string& GetContentsMimeType() = 0;
~~~
 Returns the contents MIME type after a navigation.

### GetMutableRendererPrefs

GetMutableRendererPrefs
~~~cpp
virtual blink::RendererPreferences* GetMutableRendererPrefs() = 0;
~~~
 Returns the settings which get passed to the renderer.

### Close

Close
~~~cpp
virtual void Close() = 0;
~~~
 Tells the tab to close now. The tab will take care not to close until it's
 out of nested run loops.

### SetClosedByUserGesture

SetClosedByUserGesture
~~~cpp
virtual void SetClosedByUserGesture(bool value) = 0;
~~~
 Indicates if this tab was explicitly closed by the user (control-w, close
 tab menu item...). This is false for actions that indirectly close the tab,
 such as closing the window.  The setter is maintained by TabStripModel, and
 the getter only useful from within TAB_CLOSED notification
### GetClosedByUserGesture

GetClosedByUserGesture
~~~cpp
virtual bool GetClosedByUserGesture() = 0;
~~~

### GetMinimumZoomPercent

GetMinimumZoomPercent
~~~cpp
virtual int GetMinimumZoomPercent() = 0;
~~~
 Gets the minimum/maximum zoom percent.

### GetMaximumZoomPercent

GetMaximumZoomPercent
~~~cpp
virtual int GetMaximumZoomPercent() = 0;
~~~

### SetPageScale

SetPageScale
~~~cpp
virtual void SetPageScale(float page_scale_factor) = 0;
~~~
 Set the renderer's page scale to the given factor.

### GetPreferredSize

GetPreferredSize
~~~cpp
virtual gfx::Size GetPreferredSize() = 0;
~~~
 Gets the preferred size of the contents.

### GotResponseToLockMouseRequest

GotResponseToLockMouseRequest
~~~cpp
virtual bool GotResponseToLockMouseRequest(
      blink::mojom::PointerLockResult result) = 0;
~~~
 Called when the response to a pending mouse lock request has arrived.

 Returns true if |allowed| is true and the mouse has been successfully
 locked.

### GotLockMousePermissionResponse

GotLockMousePermissionResponse
~~~cpp
virtual void GotLockMousePermissionResponse(bool allowed) = 0;
~~~
 Wrapper around GotResponseToLockMouseRequest to fit into
 ChromeWebViewPermissionHelperDelegate's structure.

### DropMouseLockForTesting

DropMouseLockForTesting
~~~cpp
virtual void DropMouseLockForTesting() = 0;
~~~
 Drop the mouse lock if it is currently locked, or reject an
 outstanding request if it is pending.

### GotResponseToKeyboardLockRequest

GotResponseToKeyboardLockRequest
~~~cpp
virtual bool GotResponseToKeyboardLockRequest(bool allowed) = 0;
~~~
 Called when the response to a keyboard mouse lock request has arrived.

 Returns false if the request is no longer valid, otherwise true.

### DidChooseColorInColorChooser

DidChooseColorInColorChooser
~~~cpp
virtual void DidChooseColorInColorChooser(SkColor color) = 0;
~~~
 Called when the user has selected a color in the color chooser.

### DidEndColorChooser

DidEndColorChooser
~~~cpp
virtual void DidEndColorChooser() = 0;
~~~
 Called when the color chooser has ended.

### FocusLocationBarByDefault

FocusLocationBarByDefault
~~~cpp
virtual bool FocusLocationBarByDefault() = 0;
~~~
 BUILDFLAG(IS_ANDROID) || BUILDFLAG(IS_MAC)
 Returns true if the location bar should be focused by default rather than
 the page contents. The view calls this function when the tab is focused
 to see what it should do.

### HasOpener

HasOpener
~~~cpp
virtual bool HasOpener() = 0;
~~~
 Does this have an opener (corresponding to window.opener in JavaScript)
 associated with it?
### GetOpener

GetOpener
~~~cpp
virtual RenderFrameHost* GetOpener() = 0;
~~~
 Returns the opener if HasOpener() is true, or nullptr otherwise.

### HasLiveOriginalOpenerChain

HasLiveOriginalOpenerChain
~~~cpp
virtual bool HasLiveOriginalOpenerChain() = 0;
~~~
 Returns true if this WebContents was opened by another WebContents, even
 if the opener was suppressed. In contrast to HasOpener/GetOpener, the
 "original opener chain" doesn't reflect window.opener which can be
 suppressed or updated. The "original opener" is the main frame of the
 actual opener of this frame. This traces the all the way back, so if the
 original opener was closed (deleted or severed due to COOP), but _it_ had
 an original opener, this will return the original opener's original opener,
 etc.

### GetFirstWebContentsInLiveOriginalOpenerChain

GetFirstWebContentsInLiveOriginalOpenerChain
~~~cpp
virtual WebContents* GetFirstWebContentsInLiveOriginalOpenerChain() = 0;
~~~
 Returns the "original opener WebContents" if HasLiveOriginalOpenerChain()
 is true, or nullptr otherwise. See the comment for
 `HasLiveOriginalOpenerChain()` for more details.

### GetWakeLockContext

GetWakeLockContext
~~~cpp
virtual device::mojom::WakeLockContext* GetWakeLockContext() = 0;
~~~
 Returns the WakeLockContext accociated with this WebContents.

### DownloadImage

DownloadImage
~~~cpp
virtual int DownloadImage(const GURL& url,
                            bool is_favicon,
                            const gfx::Size& preferred_size,
                            uint32_t max_bitmap_size,
                            bool bypass_cache,
                            ImageDownloadCallback callback) = 0;
~~~
 Sends a request to download the given image |url| and returns the unique
 id of the download request. When the download is finished, |callback| will
 be called with the bitmaps received from the renderer.

 If |is_favicon| is true, the cookies are not sent and not accepted during
 download.

 If there are no bitmap results <= |max_bitmap_size|, the smallest bitmap
 is resized to |max_bitmap_size| and is the only result.

 A |max_bitmap_size| of 0 means unlimited.

 For vector images, |preferred_size| will serve as a viewport into which
 the image will be rendered. This would usually be the dimensions of the
 rectangle where the bitmap will be rendered. If |preferred_size| is empty,
 any existing intrinsic dimensions of the image will be used. If
 |max_bitmap_size| is non-zero it will also impose an upper bound on the
 longest edge of |preferred_size| (|preferred_size| will be scaled down).

 If |bypass_cache| is true, |url| is requested from the server even if it
 is present in the browser cache.

### DownloadImageInFrame

DownloadImageInFrame
~~~cpp
virtual int DownloadImageInFrame(
      const GlobalRenderFrameHostId& initiator_frame_routing_id,
      const GURL& url,
      bool is_favicon,
      const gfx::Size& preferred_size,
      uint32_t max_bitmap_size,
      bool bypass_cache,
      ImageDownloadCallback callback) = 0;
~~~
 Same as DownloadImage(), but uses the ImageDownloader from the specified
 frame instead of the main frame.

### Find

Find
~~~cpp
virtual void Find(int request_id,
                    const std::u16string& search_text,
                    blink::mojom::FindOptionsPtr options,
                    bool skip_delay = false) = 0;
~~~
 Finds text on a page. |search_text| should not be empty. |skip_delay|
 indicates that the find request should be sent to the renderer immediately
 instead of waiting for privacy/performance mitigations.

### StopFinding

StopFinding
~~~cpp
virtual void StopFinding(StopFindAction action) = 0;
~~~
 Notifies the renderer that the user has closed the FindInPage window
 (and what action to take regarding the selection).

### WasEverAudible

WasEverAudible
~~~cpp
virtual bool WasEverAudible() = 0;
~~~
 Returns true if audio has been audible from the WebContents since the last
 navigation.

### IsFullscreen

IsFullscreen
~~~cpp
virtual bool IsFullscreen() = 0;
~~~
 Returns whether the renderer is in fullscreen mode.

### GetOrCreateWebPreferences

GetOrCreateWebPreferences
~~~cpp
virtual const blink::web_pref::WebPreferences&
  GetOrCreateWebPreferences() = 0;
~~~
 Returns a copy of the current WebPreferences associated with this
 WebContents. If it does not exist, this will create one and send the newly
 computed value to all renderers.

 Note that this will not trigger a recomputation of WebPreferences if it
 already exists - this will return the last computed/set value of
 WebPreferences. If we want to guarantee that the value reflects the current
 state of the WebContents, NotifyPreferencesChanged() should be called
 before calling this.

### NotifyPreferencesChanged

NotifyPreferencesChanged
~~~cpp
virtual void NotifyPreferencesChanged() = 0;
~~~
 Notify this WebContents that the preferences have changed, so it needs to
 recompute the current WebPreferences based on the current state of the
 WebContents, etc. This will send an IPC to all the renderer processes
 associated with this WebContents.

 Note that this will do this by creating a new WebPreferences with default
 values, then recomputing some of the attributes based on current states.

 This means if there's any value previously set through SetWebPreferences
 which does not have special recomputation logic in either
 WebContentsImpl::ComputeWebPreferences or
 ContentBrowserClient::OverrideWebkitPrefs, it will return back to its
 default value whenever this function is called.

### SetWebPreferences

SetWebPreferences
~~~cpp
virtual void SetWebPreferences(
      const blink::web_pref::WebPreferences& prefs) = 0;
~~~
 Sets the WebPreferences to |prefs|. This will send an IPC to all the
 renderer processes associated with this WebContents.

 Note that this is different from NotifyPreferencesChanged, which recomputes
 the WebPreferences based on the current state of things. Instead, we're
 setting this to a specific value. This also means that if we trigger a
 recomputation of WebPreferences after this, the WebPreferences value will
 be overridden. if there's any value previously set through
 SetWebPreferences which does not have special recomputation logic in either
 WebContentsImpl::ComputeWebPreferences or
 ContentBrowserClient::OverrideWebkitPrefs, it will return back to its
 default value, which might be different from the value we set it to here.

 If you want to use this function outside of tests, consider adding
 recomputation logic in either of those functions.

 TODO(rakina): Try to make values set through this function stick even after
 recomputations.

### OnWebPreferencesChanged

OnWebPreferencesChanged
~~~cpp
virtual void OnWebPreferencesChanged() = 0;
~~~
 Passes current web preferences to all renderer in this WebContents after
 possibly recomputing them as follows: all "fast" preferences (those not
 requiring slow platform/device polling) are recomputed unconditionally; the
 remaining "slow" ones are recomputed only if they have not been computed
 before.


 This method must be called if any state that affects web preferences has
 changed so that it can be recomputed and sent to the renderer.

### ExitFullscreen

ExitFullscreen
~~~cpp
virtual void ExitFullscreen(bool will_cause_resize) = 0;
~~~
 Requests the renderer to exit fullscreen.

 |will_cause_resize| indicates whether the fullscreen change causes a
 view resize. e.g. This will be false when going from tab fullscreen to
 browser fullscreen.

### ForSecurityDropFullscreen

ForSecurityDropFullscreen
~~~cpp
[[nodiscard]] virtual base::ScopedClosureRunner ForSecurityDropFullscreen(
      int64_t display_id = display::kInvalidDisplayId) = 0;
~~~
 The WebContents is trying to take some action that would cause user
 confusion if taken while in fullscreen. If this WebContents or any outer
 WebContents is in fullscreen, drop it.


 Returns a ScopedClosureRunner, and for the lifetime of that closure, this
 (and other related) WebContentses will not enter fullscreen. If the action
 should cause a one-time dropping of fullscreen (e.g. a UI element not
 attached to the WebContents), invoke RunAndReset() on the returned
 base::ScopedClosureRunner to release the fullscreen block immediately.

 Otherwise, if the action should cause fullscreen to be prohibited for a
 span of time (e.g. a UI element attached to the WebContents), keep the
 closure alive for that duration.


 If |display_id| is valid, only WebContentses on that specific screen will
 exit fullscreen; the scoped prohibition will still apply to all displays.

 This supports sites using cross-screen window placement capabilities to
 retain fullscreen and open or place a window on another screen.

### ResumeLoadingCreatedWebContents

ResumeLoadingCreatedWebContents
~~~cpp
virtual void ResumeLoadingCreatedWebContents() = 0;
~~~
 Unblocks requests from renderer for a newly created window. This is
 used in showCreatedWindow() or sometimes later in cases where
 delegate->ShouldResumeRequestsForCreatedWindow() indicated the requests
 should not yet be resumed. Then the client is responsible for calling this
 as soon as they are ready.

### SetIsOverlayContent

SetIsOverlayContent
~~~cpp
virtual void SetIsOverlayContent(bool is_overlay_content) = 0;
~~~
 Sets whether the WebContents is for overlaying content on a page.

### GetCurrentlyPlayingVideoCount

GetCurrentlyPlayingVideoCount
~~~cpp
virtual int GetCurrentlyPlayingVideoCount() = 0;
~~~

### GetFullscreenVideoSize

GetFullscreenVideoSize
~~~cpp
virtual absl::optional<gfx::Size> GetFullscreenVideoSize() = 0;
~~~

### ClearFocusedElement

ClearFocusedElement
~~~cpp
virtual void ClearFocusedElement() = 0;
~~~
 Tells the renderer to clear the focused element (if any).

### IsFocusedElementEditable

IsFocusedElementEditable
~~~cpp
virtual bool IsFocusedElementEditable() = 0;
~~~
 Returns true if the current focused element is editable.

### IsShowingContextMenu

IsShowingContextMenu
~~~cpp
virtual bool IsShowingContextMenu() = 0;
~~~
 Returns true if a context menu is showing on the page.

### SetShowingContextMenu

SetShowingContextMenu
~~~cpp
virtual void SetShowingContextMenu(bool showing) = 0;
~~~
 Tells the WebContents whether the context menu is showing.

### FromJavaWebContents

WebContents::FromJavaWebContents
~~~cpp
CONTENT_EXPORT static WebContents* FromJavaWebContents(
      const base::android::JavaRef<jobject>& jweb_contents_android);
~~~

### GetJavaWebContents

GetJavaWebContents
~~~cpp
virtual base::android::ScopedJavaLocalRef<jobject> GetJavaWebContents() = 0;
~~~

### GetJavaCreatorLocation

GetJavaCreatorLocation
~~~cpp
virtual base::android::ScopedJavaLocalRef<jthrowable>
  GetJavaCreatorLocation() = 0;
~~~
 Returns the value from CreateParams::java_creator_location.

### ActivateNearestFindResult

ActivateNearestFindResult
~~~cpp
virtual void ActivateNearestFindResult(float x, float y) = 0;
~~~
 Selects and zooms to the find result nearest to the point (x,y) defined in
 find-in-page coordinates.

### RequestFindMatchRects

RequestFindMatchRects
~~~cpp
virtual void RequestFindMatchRects(int current_version) = 0;
~~~
 Requests the rects of the current find matches from the renderer
 process. |current_version| is the version of find rects that the caller
 already knows about. This version will be compared to the current find
 rects version in the renderer process (which is updated whenever the rects
 change), to see which new rect data will need to be sent back.


 TODO(paulmeyer): This process will change slightly once multi-process
 find-in-page is implemented. This comment should be updated at that time.

### GetJavaInterfaces

GetJavaInterfaces
~~~cpp
virtual service_manager::InterfaceProvider* GetJavaInterfaces() = 0;
~~~
 Returns an InterfaceProvider for Java-implemented interfaces that are
 scoped to this WebContents. This provides access to interfaces implemented
 in Java in the browser process to C++ code in the browser process.

### CompletedFirstVisuallyNonEmptyPaint

CompletedFirstVisuallyNonEmptyPaint
~~~cpp
virtual bool CompletedFirstVisuallyNonEmptyPaint() = 0;
~~~
 BUILDFLAG(IS_ANDROID)
 Returns true if the WebContents has completed its first meaningful paint
 since the last navigation.

### HasRecentInteraction

HasRecentInteraction
~~~cpp
virtual bool HasRecentInteraction() = 0;
~~~
 TODO(https://crbug.com/826293): This is a simple mitigation to validate
 that an action that requires a user gesture actually has one in the
 trustworthy browser process, rather than relying on the untrustworthy
 renderer. This should be eventually merged into and accounted for in the
 user activation work: crbug.com/848778
### SetIgnoreInputEvents

SetIgnoreInputEvents
~~~cpp
virtual void SetIgnoreInputEvents(bool ignore_input_events) = 0;
~~~
 Sets a flag that causes the WebContents to ignore input events.

### GetAudioGroupId

GetAudioGroupId
~~~cpp
virtual base::UnguessableToken GetAudioGroupId() = 0;
~~~
 Returns the group id for all audio streams that correspond to a single
 WebContents. This can be used to determine if a AudioOutputStream was
 created from a renderer that originated from this WebContents.

### GetFaviconURLs

GetFaviconURLs
~~~cpp
virtual const std::vector<blink::mojom::FaviconURLPtr>& GetFaviconURLs() = 0;
~~~
 Returns the raw list of favicon candidates as reported to observers via
 WebContentsObserver::DidUpdateFaviconURL() since the last navigation start.

 Consider using FaviconDriver in components/favicon if possible for more
 reliable favicon-related state.

### UpdateWindowControlsOverlay

UpdateWindowControlsOverlay
~~~cpp
virtual void UpdateWindowControlsOverlay(const gfx::Rect& bounding_rect) = 0;
~~~
 Intended for desktop PWAs with manifest entry of window-controls-overlay,
 This sends the available title bar area bounds to the renderer process.

### GetWindowsControlsOverlayRect

GetWindowsControlsOverlayRect
~~~cpp
virtual gfx::Rect GetWindowsControlsOverlayRect() const = 0;
~~~
 Returns the Window Control Overlay rectangle. Only applies to an
 outermost main frame's widget. Other widgets always returns an empty rect.

### HasActiveEffectivelyFullscreenVideo

HasActiveEffectivelyFullscreenVideo
~~~cpp
virtual bool HasActiveEffectivelyFullscreenVideo() = 0;
~~~
 Whether the WebContents has an active player that is effectively
 fullscreen. That means that the video is either fullscreen or it is the
 content of a fullscreen page (in other words, a fullscreen video with
 custom controls).

### WriteIntoTrace

WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(perfetto::TracedValue context) = 0;
~~~
 Serialise this object into a trace.

### GetCreatorLocation

GetCreatorLocation
~~~cpp
virtual const base::Location& GetCreatorLocation() = 0;
~~~
 Returns the value from CreateParams::creator_location.

### GetPictureInPictureOptions

GetPictureInPictureOptions
~~~cpp
virtual const absl::optional<blink::mojom::PictureInPictureWindowOptions>&
  GetPictureInPictureOptions() const = 0;
~~~
 Returns the parameters associated with PictureInPicture WebContents
### UpdateBrowserControlsState

UpdateBrowserControlsState
~~~cpp
virtual void UpdateBrowserControlsState(cc::BrowserControlsState constraints,
                                          cc::BrowserControlsState current,
                                          bool animate) = 0;
~~~
 Hide or show the browser controls for the given WebContents, based on
 allowed states, desired state and whether the transition should be animated
 or not.

### SetTabSwitchStartTime

SetTabSwitchStartTime
~~~cpp
virtual void SetTabSwitchStartTime(base::TimeTicks start_time,
                                     bool destination_is_loaded) = 0;
~~~
 Sets the last time a tab switch made this WebContents visible.

 `start_time` is the timestamp of the input event that triggered the tab
 switch. `destination_is_loaded` is true when
 ResourceCoordinatorTabHelper::IsLoaded() is true for the new tab contents.

 These will be used to record metrics with the latency between the input
 event and the time when the WebContents is painted.

### StartPrerendering

StartPrerendering
~~~cpp
virtual std::unique_ptr<PrerenderHandle> StartPrerendering(
      const GURL& prerendering_url,
      PrerenderTriggerType trigger_type,
      const std::string& embedder_histogram_suffix,
      ui::PageTransition page_transition,
      PreloadingAttempt* preloading_attempt,
      absl::optional<base::RepeatingCallback<bool(const GURL&)>>
          url_match_predicate = absl::nullopt) = 0;
~~~
 Starts an embedder triggered (browser-initiated) prerendering page and
 returns the unique_ptr<PrerenderHandle>, which cancels prerendering on its
 destruction. If the prerendering failed to start (e.g. if prerendering is
 disabled, failure happened or because this URL is already being
 prerendered), this function returns a nullptr.

 PreloadingAttempt helps us to log various metrics associated with
 particular prerendering attempt. `url_match_predicate` allows embedders to
 define their own predicates for matching same-origin URLs during
 prerendering activation; it would be useful if embedders want Prerender2 to
 ignore some parameter mismatches. Note that if the mismatched prerender URL
 will be activated due to the predicate returning true, the last committed
 URL in the prerendered RenderFrameHost will be activated.

### CreateDisallowCustomCursorScope

CreateDisallowCustomCursorScope
~~~cpp
[[nodiscard]] virtual base::ScopedClosureRunner
  CreateDisallowCustomCursorScope() = 0;
~~~
 Returns a scope object that needs to be owned by caller in order to
 disallow custom cursors. Custom cursors are diallowed in this web contents
 for as long as any of the returned |ScopedClosureRunner| objects is alive.

### WebContents

WebContents
~~~cpp
WebContents() = default;
~~~

### ~WebContents

~WebContents
~~~cpp
~WebContents() override = default;
~~~

### GetDelegate

GetDelegate
~~~cpp
virtual WebContentsDelegate* GetDelegate() = 0;
~~~
###  Intrinsic tab state 
-------------------------------------------------------
 Gets/Sets the delegate.

### SetDelegate

SetDelegate
~~~cpp
virtual void SetDelegate(WebContentsDelegate* delegate) = 0;
~~~

### GetController

GetController
~~~cpp
virtual NavigationController& GetController() = 0;
~~~
 Gets the NavigationController for primary frame tree of this WebContents.

 See comments on NavigationController for more details.

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual content::BrowserContext* GetBrowserContext() = 0;
~~~
 Returns the user browser context associated with this WebContents (via the
 NavigationController).

### GetWeakPtr

GetWeakPtr
~~~cpp
virtual base::WeakPtr<WebContents> GetWeakPtr() = 0;
~~~
 Returns a weak pointer.

### GetURL

GetURL
~~~cpp
virtual const GURL& GetURL() = 0;
~~~
 Gets the URL that is currently being displayed, if there is one.

 This method is deprecated. DO NOT USE! Pick either |GetVisibleURL| or
 |GetLastCommittedURL| as appropriate.

### GetVisibleURL

GetVisibleURL
~~~cpp
virtual const GURL& GetVisibleURL() = 0;
~~~
 Gets the virtual URL currently being displayed in the URL bar, if there is
 one. This URL might be a pending navigation that hasn't committed yet, so
 it is not guaranteed to match the current page in this WebContents.

### GetLastCommittedURL

GetLastCommittedURL
~~~cpp
virtual const GURL& GetLastCommittedURL() = 0;
~~~
 Gets the virtual URL of the last committed page in this WebContents.

 Virtual URLs are meant to be displayed to the user (e.g., they include the
 "view-source:" prefix for view source URLs, unlike NavigationEntry::GetURL
 and NavigationHandle::GetURL). The last committed page is the current
 security context and the content that is actually displayed within the tab.

 See also GetVisibleURL above, which may differ from this URL. Note that
 this might return an empty GURL if no navigation has committed in the
 WebContents' main frame.

### GetPrimaryMainFrame

GetPrimaryMainFrame
~~~cpp
virtual RenderFrameHost* GetPrimaryMainFrame() = 0;
~~~
 Returns the primary main frame for the currently active page. Always
 non-null except during WebContents destruction. This WebContents may
 have additional main frames for prerendered pages, bfcached pages, etc.

 See docs/frame_trees.md for more details.

### GetPrimaryPage

GetPrimaryPage
~~~cpp
virtual Page& GetPrimaryPage() = 0;
~~~
 Returns the current page in the primary frame tree of this WebContents.

 If this WebContents is associated with an omnibox, usually the URL of the
 main document of this page will be displayed in it.


 Primary page can change as a result of a navigation, both to a new page
 (navigation loading a new main document) and an existing one (when
 restoring the page from back/forward cache or activating a prerendering
 page). This change can be observed using
 WebContentsObserver::PrimaryPageChanged, see the comments there for more
 details.


 The primary page's lifetime corresponds to its main document's lifetime
 and may differ from a RenderFrameHost's lifetime (for cross-document same
 RenderFrameHost navigations).


 Apart from the primary page, additional pages might be associated with this
 WebContents:
 - Pending commit pages (which will become primary after-and-if the ongoing
   main frame navigation successfully commits).

 - Pending deletion pages (pages the user has navigated from, but which are
   still alive as they are running unload handlers in background).

 - Pages in back/forward cache (which can be navigated to later).

 - Prerendered pages (pages which are loading in the background in
   anticipation of user navigating to them).


 Given the existence of multiple pages, in many cases (especially when
 handling IPCs from the renderer process), calling GetPrimaryPage would not
 be appropriate as it might return a wrong page. If the code already has a
 reference to RenderFrameHost or a Page (e.g. each IPC from the renderer
 process should be associated with a particular RenderFrameHost), it should
 be used instead of getting the primary page from the WebContents.

 See docs/frame_trees.md for more details.

### GetFocusedFrame

GetFocusedFrame
~~~cpp
virtual RenderFrameHost* GetFocusedFrame() = 0;
~~~
 Returns the focused frame for the primary page or an inner page thereof.

 Might be nullptr if nothing is focused.

### IsPrerenderedFrame

IsPrerenderedFrame
~~~cpp
virtual bool IsPrerenderedFrame(int frame_tree_node_id) = 0;
~~~
 Returns true if |frame_tree_node_id| refers to a frame in a prerendered
 page.

 TODO(1196715, 1232528): This will be extended to also return true if it is
 in an inner page of a prerendered page.

### UnsafeFindFrameByFrameTreeNodeId

UnsafeFindFrameByFrameTreeNodeId
~~~cpp
virtual RenderFrameHost* UnsafeFindFrameByFrameTreeNodeId(
      int frame_tree_node_id) = 0;
~~~
 NOTE: This is generally unsafe to use. A frame's RenderFrameHost may
 change over its lifetime, such as during cross-process navigation (and
 thus privilege change). Use RenderFrameHost::FromID instead wherever
 possible.


 Given a FrameTreeNode ID that belongs to this WebContents, returns the
 current RenderFrameHost regardless of which FrameTree it is in.


 See RenderFrameHost::GetFrameTreeNodeId for documentation on this ID.

### ForEachRenderFrameHostWithAction

ForEachRenderFrameHostWithAction
~~~cpp
virtual void ForEachRenderFrameHostWithAction(
      base::FunctionRef<FrameIterationAction(RenderFrameHost*)> on_frame) = 0;
~~~

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
virtual void ForEachRenderFrameHost(
      base::FunctionRef<void(RenderFrameHost*)> on_frame) = 0;
~~~

### GetRenderViewHost

GetRenderViewHost
~~~cpp
virtual RenderViewHost* GetRenderViewHost() = 0;
~~~
 Gets the current RenderViewHost for this tab.

### GetRenderWidgetHostView

GetRenderWidgetHostView
~~~cpp
virtual RenderWidgetHostView* GetRenderWidgetHostView() = 0;
~~~
 Returns the currently active RenderWidgetHostView. This may change over
 time and can be nullptr (during setup and teardown).

### GetTopLevelRenderWidgetHostView

GetTopLevelRenderWidgetHostView
~~~cpp
virtual RenderWidgetHostView* GetTopLevelRenderWidgetHostView() = 0;
~~~
 Returns the outermost RenderWidgetHostView. This will return the platform
 specific RenderWidgetHostView (as opposed to
 RenderWidgetHostViewChildFrame), which can be used to create context
 menus.

### RequestAXTreeSnapshot

RequestAXTreeSnapshot
~~~cpp
virtual void RequestAXTreeSnapshot(AXTreeSnapshotCallback callback,
                                     ui::AXMode ax_mode,
                                     bool exclude_offscreen,
                                     size_t max_nodes,
                                     base::TimeDelta timeout) = 0;
~~~

### ClosePage

ClosePage
~~~cpp
virtual void ClosePage() = 0;
~~~
 Causes the current page to be closed, including running its onunload event
 handler.

### GetThemeColor

GetThemeColor
~~~cpp
virtual absl::optional<SkColor> GetThemeColor() = 0;
~~~
 Returns the theme color for the underlying content as set by the
 theme-color meta tag if any.

### GetBackgroundColor

GetBackgroundColor
~~~cpp
virtual absl::optional<SkColor> GetBackgroundColor() = 0;
~~~
 Returns the background color for the underlying content as set by CSS if
 any.

### SetPageBaseBackgroundColor

SetPageBaseBackgroundColor
~~~cpp
virtual void SetPageBaseBackgroundColor(absl::optional<SkColor> color) = 0;
~~~
 Sets the renderer-side default background color of the page. This is used
 when the page has not loaded enough to know a background color or if the
 page does not set a background color.

 Pass in nullopt to reset back to the default.

 Note there are situations where the base background color is not used, such
 as fullscreen.

 Note currently this is sent directly to the renderer, so does not interact
 directly with `RenderWidgetHostView::SetBackgroundColor`. There is pending
 refactor to remove `RenderWidgetHostView::SetBackgroundColor` and merge its
 functionality here, which will be more consistent and simpler to
 understand.

### SetColorProviderSource

SetColorProviderSource
~~~cpp
virtual void SetColorProviderSource(ui::ColorProviderSource* source) = 0;
~~~
 Sets the ColorProviderSource for the WebContents. The WebContents will
 maintain an observation of `source` until a new source is set or the
 current source is destroyed. WebContents will receive updates when the
 source's ColorProvider changes.

### GetColorProvider

GetColorProvider
~~~cpp
virtual const ui::ColorProvider& GetColorProvider() const = 0;
~~~
 Returns the ColorProvider instance for this WebContents object. This will
 always return a valid ColorProvider instance.

### GetWebUI

GetWebUI
~~~cpp
virtual WebUI* GetWebUI() = 0;
~~~
 Returns the committed WebUI if one exists.

### SetUserAgentOverride

SetUserAgentOverride
~~~cpp
virtual void SetUserAgentOverride(const blink::UserAgentOverride& ua_override,
                                    bool override_in_new_tabs) = 0;
~~~
 Sets the user-agent that may be used for navigations in this WebContents.

 The user-agent is *only* used when
 NavigationEntry::SetIsOverridingUserAgent(true) is used (the value of
 is-overriding-user-agent may be specified in LoadURLParams). If
 |override_in_new_tabs| is true, and the first navigation in the tab is
 renderer initiated, then is-overriding-user-agent is set to true for the
 NavigationEntry. See SetRendererInitiatedUserAgentOverrideOption() for
 details on how renderer initiated navigations are configured.


 If nonempty, |ua_override|'s value must not contain '\0', '\r', or '\n' (in
 other words, it must be a valid HTTP header value).

### SetRendererInitiatedUserAgentOverrideOption

SetRendererInitiatedUserAgentOverrideOption
~~~cpp
virtual void SetRendererInitiatedUserAgentOverrideOption(
      NavigationController::UserAgentOverrideOption option) = 0;
~~~
 Configures the value of is-overriding-user-agent for renderer initiated
 navigations. The default is UA_OVERRIDE_INHERIT. This value does not apply
 to the first renderer initiated navigation if the tab has no navigations.

 See SetUserAgentOverride() for details on that.

### GetUserAgentOverride

GetUserAgentOverride
~~~cpp
virtual const blink::UserAgentOverride& GetUserAgentOverride() = 0;
~~~

### EnableWebContentsOnlyAccessibilityMode

EnableWebContentsOnlyAccessibilityMode
~~~cpp
virtual void EnableWebContentsOnlyAccessibilityMode() = 0;
~~~
 Set the accessibility mode so that accessibility events are forwarded
 to each WebContentsObserver.

### IsWebContentsOnlyAccessibilityModeForTesting

IsWebContentsOnlyAccessibilityModeForTesting
~~~cpp
virtual bool IsWebContentsOnlyAccessibilityModeForTesting() = 0;
~~~
 Returns true only if the WebContentsObserver accessibility mode is
 enabled.

### IsFullAccessibilityModeForTesting

IsFullAccessibilityModeForTesting
~~~cpp
virtual bool IsFullAccessibilityModeForTesting() = 0;
~~~
 Returns true only if complete accessibility mode is on, meaning there's
 both renderer accessibility, and a native browser accessibility tree.

### GetAccessibilityMode

GetAccessibilityMode
~~~cpp
virtual ui::AXMode GetAccessibilityMode() = 0;
~~~

### SetAccessibilityMode

SetAccessibilityMode
~~~cpp
virtual void SetAccessibilityMode(ui::AXMode mode) = 0;
~~~

### DumpAccessibilityTree

DumpAccessibilityTree
~~~cpp
virtual std::string DumpAccessibilityTree(
      bool internal,
      std::vector<ui::AXPropertyFilter> property_filters) = 0;
~~~

### RecordAccessibilityEvents

RecordAccessibilityEvents
~~~cpp
virtual void RecordAccessibilityEvents(
      bool start_recording,
      absl::optional<AccessibilityEventCallback> callback) = 0;
~~~
 Starts or stops recording accessibility events. |start_recording| is true
 when recording should start and false when recording should stop.

 |callback| is an optional function which is called when an accessibility
 event is received while accessibility events are being recorded. When
 |start_recording| is true, it is expected that |callback| has a value; when
 |start_recording| is false, it is expected that |callback| does not.

### GetTitle

GetTitle
~~~cpp
virtual const std::u16string& GetTitle() = 0;
~~~
###  Tab navigation state 
------------------------------------------------------
 Returns the current navigation properties, which if a navigation is
 pending may be provisional (e.g., the navigation could result in a
 download, in which case the URL would revert to what it was previously).

### UpdateTitleForEntry

UpdateTitleForEntry
~~~cpp
virtual void UpdateTitleForEntry(NavigationEntry* entry,
                                   const std::u16string& title) = 0;
~~~
 Saves the given title to the navigation entry and does associated work. It
 will update history and the view with the new title, and also synthesize
 titles for file URLs that have none. Thus |entry| must have a URL set.

### GetSiteInstance

GetSiteInstance
~~~cpp
virtual SiteInstance* GetSiteInstance() = 0;
~~~
 Returns the SiteInstance associated with the current page.

### IsLoading

IsLoading
~~~cpp
virtual bool IsLoading() = 0;
~~~
 Returns whether this WebContents is loading a resource.

### GetLoadProgress

GetLoadProgress
~~~cpp
virtual double GetLoadProgress() = 0;
~~~
 Returns the current load progress.

### ShouldShowLoadingUI

ShouldShowLoadingUI
~~~cpp
virtual bool ShouldShowLoadingUI() = 0;
~~~
 Returns whether a navigation is currently in progress that should show
 loading UI if such UI exists (progress bar, loading spinner, stop button,
 etc.) True for different-document navigations and the navigation API's
 intercept(). This being true implies that IsLoading() is also true.

### IsDocumentOnLoadCompletedInPrimaryMainFrame

IsDocumentOnLoadCompletedInPrimaryMainFrame
~~~cpp
virtual bool IsDocumentOnLoadCompletedInPrimaryMainFrame() = 0;
~~~
 Returns whether the current primary main document has reached and finished
 executing its onload() handler. Corresponds to
 WebContentsObserver::DocumentOnLoadCompletedInPrimaryMainFrame() and see
 comments there for more details.

### IsWaitingForResponse

IsWaitingForResponse
~~~cpp
virtual bool IsWaitingForResponse() = 0;
~~~
 Returns whether this WebContents is waiting for a first-response for the
 main resource of the page.

### GetLoadState

GetLoadState
~~~cpp
virtual const net::LoadStateWithParam& GetLoadState() = 0;
~~~
 Returns the current load state and the URL associated with it.

 The load state is only updated while IsLoading() is true.

### GetLoadStateHost

GetLoadStateHost
~~~cpp
virtual const std::u16string& GetLoadStateHost() = 0;
~~~

### GetUploadSize

GetUploadSize
~~~cpp
virtual uint64_t GetUploadSize() = 0;
~~~
 Returns the upload progress.

### GetUploadPosition

GetUploadPosition
~~~cpp
virtual uint64_t GetUploadPosition() = 0;
~~~

### GetEncoding

GetEncoding
~~~cpp
virtual const std::string& GetEncoding() = 0;
~~~
 Returns the character encoding of the page.

### WasDiscarded

WasDiscarded
~~~cpp
virtual bool WasDiscarded() = 0;
~~~
 Indicates that the tab was previously discarded.

 wasDiscarded is exposed on Document after discard, see:
 https://github.com/WICG/web-lifecycle
 When a tab is discarded, WebContents sets was_discarded on its
 root FrameTreeNode.

 In addition, when a child frame is created, this bit is passed on from
 parent to child.

 When a navigation request is created, was_discarded is passed on to the
 request and reset to false in FrameTreeNode.

### SetWasDiscarded

SetWasDiscarded
~~~cpp
virtual void SetWasDiscarded(bool was_discarded) = 0;
~~~

### AboutToBeDiscarded

AboutToBeDiscarded
~~~cpp
virtual void AboutToBeDiscarded(WebContents* new_contents) = 0;
~~~
 Notifies observers that this WebContents is about to be discarded, and
 replaced with `new_contents`. See the comment on
 WebContentsObserver::AboutToBeDiscarded.

### IncrementCapturerCount

IncrementCapturerCount
~~~cpp
[[nodiscard]] virtual base::ScopedClosureRunner IncrementCapturerCount(
      const gfx::Size& capture_size,
      bool stay_hidden,
      bool stay_awake,
      bool is_activity = true) = 0;
~~~
###  Internal state 
------------------------------------------------------------
 Indicates whether the WebContents is being captured (e.g., for screenshots,
 or mirroring video and/or audio). Each IncrementCapturerCount() call must
 be balanced with a corresponding DecrementCapturerCount() call.


 Both internal-to-content and embedders must increment the capturer count
 while capturing to ensure "hidden rendering" optimizations are disabled.

 For example, renderers will be configured to produce compositor frames
 regardless of their "backgrounded" or on-screen occlusion state.


 Embedders can detect whether a WebContents is being captured (see
 IsBeingCaptured() below) and use this, for example, to provide an
 alternative user interface. So, developers should be careful to understand
 the side-effects from using or changing these APIs, both upstream and
 downstream of this API layer.


 Callers must hold onto the returned base::ScopedClosureRunner until they
 are done capturing.


 |capture_size| is only used in the case of mirroring (i.e., screen capture
 video); otherwise, an empty gfx::Size should be provided. This specifies
 the capturer's target video resolution, but can be empty to mean
 "unspecified." This becomes a temporary override to GetPreferredSize(),
 allowing embedders to size the WebContents on-screen views for optimal
 capture quality.


 |stay_hidden| affects the page visibility state of the renderers (i.e., a
 web page can be made aware of whether it is actually user-visible). If
 true, the show/hide state of the WebContents will be passed to the
 renderers, like normal. If false, the renderers will always be told they
 are user-visible while being captured.


 |stay_awake| will cause a WakeLock to be held which prevents system sleep.


 |is_activity| means the capture will cause the last active time to be
 updated.

### GetCaptureHandleConfig

GetCaptureHandleConfig
~~~cpp
virtual const blink::mojom::CaptureHandleConfig& GetCaptureHandleConfig() = 0;
~~~
 Getter for the capture handle, which allows a captured application to
 opt-in to exposing information to its capturer(s).

### IsBeingCaptured

IsBeingCaptured
~~~cpp
virtual bool IsBeingCaptured() = 0;
~~~
 Returns true if audio/screenshot/video is being captured by the embedder,
 as indicated by calls to IncrementCapturerCount().

### IsBeingVisiblyCaptured

IsBeingVisiblyCaptured
~~~cpp
virtual bool IsBeingVisiblyCaptured() = 0;
~~~
 Returns true if audio/screenshot/video is being captured by the embedder
 and renderers are being told they are always user-visible, as indicated by
 calls to IncrementCapturerCount().

### IsAudioMuted

IsAudioMuted
~~~cpp
virtual bool IsAudioMuted() = 0;
~~~
 Indicates/Sets whether all audio output from this WebContents is muted.

 This does not affect audio capture, just local/system output.

### SetAudioMuted

SetAudioMuted
~~~cpp
virtual void SetAudioMuted(bool mute) = 0;
~~~

### IsCurrentlyAudible

IsCurrentlyAudible
~~~cpp
virtual bool IsCurrentlyAudible() = 0;
~~~
 Returns true if the audio is currently audible.

### IsConnectedToBluetoothDevice

IsConnectedToBluetoothDevice
~~~cpp
virtual bool IsConnectedToBluetoothDevice() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a Bluetooth
 Device.

### IsScanningForBluetoothDevices

IsScanningForBluetoothDevices
~~~cpp
virtual bool IsScanningForBluetoothDevices() = 0;
~~~
 Indicates whether any frame in the WebContents is scanning for Bluetooth
 devices.

### IsConnectedToSerialPort

IsConnectedToSerialPort
~~~cpp
virtual bool IsConnectedToSerialPort() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a serial
 port.

### IsConnectedToHidDevice

IsConnectedToHidDevice
~~~cpp
virtual bool IsConnectedToHidDevice() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a HID
 device.

### IsConnectedToUsbDevice

IsConnectedToUsbDevice
~~~cpp
virtual bool IsConnectedToUsbDevice() = 0;
~~~
 Indicates whether any frame in the WebContents is connected to a USB
 device.

### HasFileSystemAccessHandles

HasFileSystemAccessHandles
~~~cpp
virtual bool HasFileSystemAccessHandles() = 0;
~~~
 Indicates whether any frame in the WebContents has File System Access
 handles.

### HasPictureInPictureVideo

HasPictureInPictureVideo
~~~cpp
virtual bool HasPictureInPictureVideo() = 0;
~~~
 Indicates whether a video is in Picture-in-Picture for |this|.

### HasPictureInPictureDocument

HasPictureInPictureDocument
~~~cpp
virtual bool HasPictureInPictureDocument() = 0;
~~~
 Indicates whether a document is in Picture-in-Picture for |this|.

### IsCrashed

IsCrashed
~~~cpp
virtual bool IsCrashed() = 0;
~~~
 Indicates whether this tab should be considered crashed. This becomes false
 again when the renderer process is recreated after a crash in order to
 recreate the main frame.

### GetCrashedStatus

GetCrashedStatus
~~~cpp
virtual base::TerminationStatus GetCrashedStatus() = 0;
~~~

### GetCrashedErrorCode

GetCrashedErrorCode
~~~cpp
virtual int GetCrashedErrorCode() = 0;
~~~

### IsBeingDestroyed

IsBeingDestroyed
~~~cpp
virtual bool IsBeingDestroyed() = 0;
~~~
 Whether the tab is in the process of being destroyed.

### NotifyNavigationStateChanged

NotifyNavigationStateChanged
~~~cpp
virtual void NotifyNavigationStateChanged(InvalidateTypes changed_flags) = 0;
~~~
 Convenience method for notifying the delegate of a navigation state
 change.

### OnAudioStateChanged

OnAudioStateChanged
~~~cpp
virtual void OnAudioStateChanged() = 0;
~~~
 Notifies the WebContents that audio state has changed. The contents is
 aware of all of its potential sources of audio and needs to poll them
 directly to determine its aggregate audio state.

### GetLastActiveTime

GetLastActiveTime
~~~cpp
virtual base::TimeTicks GetLastActiveTime() = 0;
~~~
 Get/Set the last time that the WebContents was made active (either when it
 was created or shown with WasShown()).

### WasShown

WasShown
~~~cpp
virtual void WasShown() = 0;
~~~
 Invoked when the WebContents becomes shown/hidden. A hidden WebContents
 isn't painted on the screen.

### WasHidden

WasHidden
~~~cpp
virtual void WasHidden() = 0;
~~~

### WasOccluded

WasOccluded
~~~cpp
virtual void WasOccluded() = 0;
~~~
 Invoked when the WebContents becomes occluded. An occluded WebContents
 isn't painted on the screen, except in a window switching feature (e.g.

 Alt-Tab).

### GetVisibility

GetVisibility
~~~cpp
virtual Visibility GetVisibility() = 0;
~~~
 Returns the visibility of the WebContents' view.

### NeedToFireBeforeUnloadOrUnloadEvents

NeedToFireBeforeUnloadOrUnloadEvents
~~~cpp
virtual bool NeedToFireBeforeUnloadOrUnloadEvents() = 0;
~~~
 This function checks *all* frames in this WebContents (not just the main
 frame) and returns true if at least one frame has either a beforeunload or
 an unload/pagehide/visibilitychange handler.


 The value of this may change over time. For example, if true and the
 beforeunload listener is executed and allows the user to exit, then this
 returns false.

### DispatchBeforeUnload

DispatchBeforeUnload
~~~cpp
virtual void DispatchBeforeUnload(bool auto_cancel) = 0;
~~~
 Runs the beforeunload handler for the main frame and all its subframes.

 See also ClosePage in RenderViewHostImpl, which runs the unload handler.

 If |auto_cancel| is true, and the beforeunload handler returns a non-empty
 string (indicating the page wants to present a confirmation dialog), then
 the beforeunload operation will automatically return with |proceed=false|
 and no dialog will be shown to the user. This is used to interrupt a
 potential discard without causing the dialog to appear.

### AttachInnerWebContents

AttachInnerWebContents
~~~cpp
virtual void AttachInnerWebContents(
      std::unique_ptr<WebContents> inner_web_contents,
      RenderFrameHost* render_frame_host,
      mojo::PendingAssociatedRemote<blink::mojom::RemoteFrame> remote_frame,
      mojo::PendingAssociatedReceiver<blink::mojom::RemoteFrameHost>
          remote_frame_host_receiver,
      bool is_full_page) = 0;
~~~
 Attaches |inner_web_contents| to the container frame |render_frame_host|,
 which must be in a FrameTree for this WebContents. This outer WebContents
 takes ownership of |inner_web_contents|.

 Note: |render_frame_host| will be swapped out and destroyed during the
 process. Generally a frame same-process with its parent is the right choice
 but ideally it should be "about:blank" to avoid problems with beforeunload.

 To ensure sane usage of this API users first should call the async API
 RenderFrameHost::PrepareForInnerWebContentsAttach first.

 Note: If |is_full_page| is true, focus will be given to the inner
 WebContents.

### IsInnerWebContentsForGuest

IsInnerWebContentsForGuest
~~~cpp
virtual bool IsInnerWebContentsForGuest() = 0;
~~~
 Returns whether this WebContents is an inner WebContents for a guest.

 Important: please avoid using this in new callsites, and use
 GetOuterWebContents instead.

### IsPortal

IsPortal
~~~cpp
virtual bool IsPortal() = 0;
~~~
 Returns whether this WebContents is a portal. This returns true even when
 this WebContents is not attached to its portal host's WebContents tree.

 This value may change over time due to portal activation and adoption.

### GetPortalHostWebContents

GetPortalHostWebContents
~~~cpp
virtual WebContents* GetPortalHostWebContents() = 0;
~~~
 If |IsPortal()| is true, returns this WebContents' portal host's
 WebContents. Otherwise, returns nullptr.

### GetOuterWebContentsFrame

GetOuterWebContentsFrame
~~~cpp
virtual RenderFrameHost* GetOuterWebContentsFrame() = 0;
~~~
 Returns the outer WebContents frame, the same frame that this WebContents
 was attached in AttachToOuterWebContentsFrame().

### GetOuterWebContents

GetOuterWebContents
~~~cpp
virtual WebContents* GetOuterWebContents() = 0;
~~~
 Returns the outer WebContents of this WebContents if any.

 Otherwise, return nullptr.

### GetOutermostWebContents

GetOutermostWebContents
~~~cpp
virtual WebContents* GetOutermostWebContents() = 0;
~~~
 Returns the root WebContents of the WebContents tree. Always returns
 non-null value.

### GetInnerWebContents

GetInnerWebContents
~~~cpp
virtual std::vector<WebContents*> GetInnerWebContents() = 0;
~~~
 Returns a vector to the inner WebContents within this WebContents.

### GetResponsibleWebContents

GetResponsibleWebContents
~~~cpp
virtual WebContents* GetResponsibleWebContents() = 0;
~~~
 Returns the user-visible WebContents that is responsible for the UI
 activity in the provided WebContents. For example, this delegate may be
 aware that the contents is embedded in some other contents, or hosts
 background activity on behalf of a user-visible tab which should be used to
 display dialogs and similar affordances to the user.


 This may be distinct from the outer web contents (for example, the
 responsible contents may logically "own" a contents but not currently embed
 it for rendering).


 Always returns a non-null value.

### DidChangeVisibleSecurityState

DidChangeVisibleSecurityState
~~~cpp
virtual void DidChangeVisibleSecurityState() = 0;
~~~
 Invoked when visible security state changes.

### SyncRendererPrefs

SyncRendererPrefs
~~~cpp
virtual void SyncRendererPrefs() = 0;
~~~
 Sends the current preferences to all renderer processes for the current
 page.

### Stop

Stop
~~~cpp
virtual void Stop() = 0;
~~~
###  Commands 
------------------------------------------------------------------
 Stop any pending navigation.

### SetPageFrozen

SetPageFrozen
~~~cpp
virtual void SetPageFrozen(bool frozen) = 0;
~~~
 Freezes or unfreezes the current page. A frozen page runs as few tasks as
 possible. This cannot be called when the page is visible. If the page is
 made visible after this is called, it is automatically unfrozen.

### Clone

Clone
~~~cpp
virtual std::unique_ptr<WebContents> Clone() = 0;
~~~
 Creates a new WebContents with the same state as this one. The returned
 heap-allocated pointer is owned by the caller.

### ReloadFocusedFrame

ReloadFocusedFrame
~~~cpp
virtual void ReloadFocusedFrame() = 0;
~~~
 Reloads the focused frame.

### Undo

Undo
~~~cpp
virtual void Undo() = 0;
~~~
###  Editing commands 
----------------------------------------------------------
### Redo

Redo
~~~cpp
virtual void Redo() = 0;
~~~

### Cut

Cut
~~~cpp
virtual void Cut() = 0;
~~~

### Copy

Copy
~~~cpp
virtual void Copy() = 0;
~~~

### CopyToFindPboard

CopyToFindPboard
~~~cpp
virtual void CopyToFindPboard() = 0;
~~~

### CenterSelection

CenterSelection
~~~cpp
virtual void CenterSelection() = 0;
~~~

### Paste

Paste
~~~cpp
virtual void Paste() = 0;
~~~

### PasteAndMatchStyle

PasteAndMatchStyle
~~~cpp
virtual void PasteAndMatchStyle() = 0;
~~~

### Delete

Delete
~~~cpp
virtual void Delete() = 0;
~~~

### SelectAll

SelectAll
~~~cpp
virtual void SelectAll() = 0;
~~~

### CollapseSelection

CollapseSelection
~~~cpp
virtual void CollapseSelection() = 0;
~~~

### ScrollToTopOfDocument

ScrollToTopOfDocument
~~~cpp
virtual void ScrollToTopOfDocument() = 0;
~~~

### ScrollToBottomOfDocument

ScrollToBottomOfDocument
~~~cpp
virtual void ScrollToBottomOfDocument() = 0;
~~~

### AdjustSelectionByCharacterOffset

AdjustSelectionByCharacterOffset
~~~cpp
virtual void AdjustSelectionByCharacterOffset(int start_adjust,
                                                int end_adjust,
                                                bool show_selection_menu) = 0;
~~~
 Adjust the selection starting and ending points in the focused frame by
 the given amounts. A negative amount moves the selection towards the
 beginning of the document, a positive amount moves the selection towards
 the end of the document.

### Replace

Replace
~~~cpp
virtual void Replace(const std::u16string& word) = 0;
~~~
 Replaces the currently selected word or a word around the cursor.

### ReplaceMisspelling

ReplaceMisspelling
~~~cpp
virtual void ReplaceMisspelling(const std::u16string& word) = 0;
~~~
 Replaces the misspelling in the current selection.

### NotifyContextMenuClosed

NotifyContextMenuClosed
~~~cpp
virtual void NotifyContextMenuClosed(const GURL& link_followed) = 0;
~~~
 Let the renderer know that the menu has been closed.

### ExecuteCustomContextMenuCommand

ExecuteCustomContextMenuCommand
~~~cpp
virtual void ExecuteCustomContextMenuCommand(int action,
                                               const GURL& link_followed) = 0;
~~~
 Executes custom context menu action that was provided from Blink.

### GetNativeView

GetNativeView
~~~cpp
virtual gfx::NativeView GetNativeView() = 0;
~~~
###  Views and focus 
-----------------------------------------------------------
 Returns the native widget that contains the contents of the tab.

### GetContentNativeView

GetContentNativeView
~~~cpp
virtual gfx::NativeView GetContentNativeView() = 0;
~~~
 Returns the native widget with the main content of the tab (i.e. the main
 render view host, though there may be many popups in the tab as children of
 the container).

### GetTopLevelNativeWindow

GetTopLevelNativeWindow
~~~cpp
virtual gfx::NativeWindow GetTopLevelNativeWindow() = 0;
~~~
 Returns the outermost native view. This will be used as the parent for
 dialog boxes.

### GetContainerBounds

GetContainerBounds
~~~cpp
virtual gfx::Rect GetContainerBounds() = 0;
~~~
 Computes the rectangle for the native widget that contains the contents of
 the tab in the screen coordinate system.

### GetViewBounds

GetViewBounds
~~~cpp
virtual gfx::Rect GetViewBounds() = 0;
~~~
 Get the bounds of the View, relative to the parent.

### Resize

Resize
~~~cpp
virtual void Resize(const gfx::Rect& new_bounds) = 0;
~~~
 Resize a WebContents to |new_bounds|.

### GetSize

GetSize
~~~cpp
virtual gfx::Size GetSize() = 0;
~~~
 Get the size of a WebContents.

### GetDropData

GetDropData
~~~cpp
virtual DropData* GetDropData() = 0;
~~~
 Returns the current drop data, if any.

### Focus

Focus
~~~cpp
virtual void Focus() = 0;
~~~
 Sets focus to the native widget for this tab.

### SetInitialFocus

SetInitialFocus
~~~cpp
virtual void SetInitialFocus() = 0;
~~~
 Sets focus to the appropriate element when the WebContents is shown the
 first time.

### StoreFocus

StoreFocus
~~~cpp
virtual void StoreFocus() = 0;
~~~
 Stores the currently focused view.

### RestoreFocus

RestoreFocus
~~~cpp
virtual void RestoreFocus() = 0;
~~~
 Restores focus to the last focus view. If StoreFocus has not yet been
 invoked, SetInitialFocus is invoked.

### FocusThroughTabTraversal

FocusThroughTabTraversal
~~~cpp
virtual void FocusThroughTabTraversal(bool reverse) = 0;
~~~
 Focuses the first (last if |reverse| is true) element in the page.

 Invoked when this tab is getting the focus through tab traversal (|reverse|
 is true when using Shift-Tab).

### IsSavable

IsSavable
~~~cpp
virtual bool IsSavable() = 0;
~~~
###  Misc state & callbacks 
----------------------------------------------------
 Check whether we can do the saving page operation this page given its MIME
 type.

### OnSavePage

OnSavePage
~~~cpp
virtual void OnSavePage() = 0;
~~~
 Prepare for saving the current web page to disk.

### SavePage

SavePage
~~~cpp
virtual bool SavePage(const base::FilePath& main_file,
                        const base::FilePath& dir_path,
                        SavePageType save_type) = 0;
~~~
 Save page with the main HTML file path, the directory for saving resources,
 and the save type: HTML only or complete web page. Returns true if the
 saving process has been initiated successfully.

### SaveFrame

SaveFrame
~~~cpp
virtual void SaveFrame(const GURL& url,
                         const Referrer& referrer,
                         RenderFrameHost* rfh) = 0;
~~~
 Saves the given frame's URL to the local filesystem. If `rfh` is provided,
 the saving is performed in its context. For example, the associated
 navigation isolation info will be used for making the network request.

### SaveFrameWithHeaders

SaveFrameWithHeaders
~~~cpp
virtual void SaveFrameWithHeaders(const GURL& url,
                                    const Referrer& referrer,
                                    const std::string& headers,
                                    const std::u16string& suggested_filename,
                                    RenderFrameHost* rfh) = 0;
~~~
 Saves the given frame's URL to the local filesystem. The headers, if
 provided, is used to make a request to the URL rather than using cache.

 Format of |headers| is a new line separated list of key value pairs:
 "<key1>: <value1>\r\n<key2>: <value2>". The saving is performed in the
 context of `rfh`. For example, the associated navigation isolation info
 will be used for making the network request.

### GenerateMHTML

GenerateMHTML
~~~cpp
virtual void GenerateMHTML(
      const MHTMLGenerationParams& params,
      base::OnceCallback<void(int64_t /* file_size */)> callback) = 0;
~~~
 Generate an MHTML representation of the current page conforming to the
 settings provided by |params| and returning final status information via
 the callback. See MHTMLGenerationParams for details on generation settings.

 A resulting |file_size| of -1 represents a failure. Any other value
 represents the size of the successfully generated file.


 TODO(https://crbug.com/915966): GenerateMHTML will eventually be removed
 and GenerateMHTMLWithResult will be renamed to GenerateMHTML to replace it.

 Both GenerateMHTML and GenerateMHTMLWithResult perform the same operation.

 however, GenerateMHTMLWithResult provides a struct as output, that contains
 the file size and more.

### GenerateMHTMLWithResult

GenerateMHTMLWithResult
~~~cpp
virtual void GenerateMHTMLWithResult(
      const MHTMLGenerationParams& params,
      MHTMLGenerationResult::GenerateMHTMLCallback callback) = 0;
~~~

### GetContentsMimeType

GetContentsMimeType
~~~cpp
virtual const std::string& GetContentsMimeType() = 0;
~~~
 Returns the contents MIME type after a navigation.

### GetMutableRendererPrefs

GetMutableRendererPrefs
~~~cpp
virtual blink::RendererPreferences* GetMutableRendererPrefs() = 0;
~~~
 Returns the settings which get passed to the renderer.

### Close

Close
~~~cpp
virtual void Close() = 0;
~~~
 Tells the tab to close now. The tab will take care not to close until it's
 out of nested run loops.

### SetClosedByUserGesture

SetClosedByUserGesture
~~~cpp
virtual void SetClosedByUserGesture(bool value) = 0;
~~~
 Indicates if this tab was explicitly closed by the user (control-w, close
 tab menu item...). This is false for actions that indirectly close the tab,
 such as closing the window.  The setter is maintained by TabStripModel, and
 the getter only useful from within TAB_CLOSED notification
### GetClosedByUserGesture

GetClosedByUserGesture
~~~cpp
virtual bool GetClosedByUserGesture() = 0;
~~~

### GetMinimumZoomPercent

GetMinimumZoomPercent
~~~cpp
virtual int GetMinimumZoomPercent() = 0;
~~~
 Gets the minimum/maximum zoom percent.

### GetMaximumZoomPercent

GetMaximumZoomPercent
~~~cpp
virtual int GetMaximumZoomPercent() = 0;
~~~

### SetPageScale

SetPageScale
~~~cpp
virtual void SetPageScale(float page_scale_factor) = 0;
~~~
 Set the renderer's page scale to the given factor.

### GetPreferredSize

GetPreferredSize
~~~cpp
virtual gfx::Size GetPreferredSize() = 0;
~~~
 Gets the preferred size of the contents.

### GotResponseToLockMouseRequest

GotResponseToLockMouseRequest
~~~cpp
virtual bool GotResponseToLockMouseRequest(
      blink::mojom::PointerLockResult result) = 0;
~~~
 Called when the response to a pending mouse lock request has arrived.

 Returns true if |allowed| is true and the mouse has been successfully
 locked.

### GotLockMousePermissionResponse

GotLockMousePermissionResponse
~~~cpp
virtual void GotLockMousePermissionResponse(bool allowed) = 0;
~~~
 Wrapper around GotResponseToLockMouseRequest to fit into
 ChromeWebViewPermissionHelperDelegate's structure.

### DropMouseLockForTesting

DropMouseLockForTesting
~~~cpp
virtual void DropMouseLockForTesting() = 0;
~~~
 Drop the mouse lock if it is currently locked, or reject an
 outstanding request if it is pending.

### GotResponseToKeyboardLockRequest

GotResponseToKeyboardLockRequest
~~~cpp
virtual bool GotResponseToKeyboardLockRequest(bool allowed) = 0;
~~~
 Called when the response to a keyboard mouse lock request has arrived.

 Returns false if the request is no longer valid, otherwise true.

### FocusLocationBarByDefault

FocusLocationBarByDefault
~~~cpp
virtual bool FocusLocationBarByDefault() = 0;
~~~
 BUILDFLAG(IS_ANDROID) || BUILDFLAG(IS_MAC)
 Returns true if the location bar should be focused by default rather than
 the page contents. The view calls this function when the tab is focused
 to see what it should do.

### HasOpener

HasOpener
~~~cpp
virtual bool HasOpener() = 0;
~~~
 Does this have an opener (corresponding to window.opener in JavaScript)
 associated with it?
### GetOpener

GetOpener
~~~cpp
virtual RenderFrameHost* GetOpener() = 0;
~~~
 Returns the opener if HasOpener() is true, or nullptr otherwise.

### HasLiveOriginalOpenerChain

HasLiveOriginalOpenerChain
~~~cpp
virtual bool HasLiveOriginalOpenerChain() = 0;
~~~
 Returns true if this WebContents was opened by another WebContents, even
 if the opener was suppressed. In contrast to HasOpener/GetOpener, the
 "original opener chain" doesn't reflect window.opener which can be
 suppressed or updated. The "original opener" is the main frame of the
 actual opener of this frame. This traces the all the way back, so if the
 original opener was closed (deleted or severed due to COOP), but _it_ had
 an original opener, this will return the original opener's original opener,
 etc.

### GetFirstWebContentsInLiveOriginalOpenerChain

GetFirstWebContentsInLiveOriginalOpenerChain
~~~cpp
virtual WebContents* GetFirstWebContentsInLiveOriginalOpenerChain() = 0;
~~~
 Returns the "original opener WebContents" if HasLiveOriginalOpenerChain()
 is true, or nullptr otherwise. See the comment for
 `HasLiveOriginalOpenerChain()` for more details.

### GetWakeLockContext

GetWakeLockContext
~~~cpp
virtual device::mojom::WakeLockContext* GetWakeLockContext() = 0;
~~~
 Returns the WakeLockContext accociated with this WebContents.

### DownloadImage

DownloadImage
~~~cpp
virtual int DownloadImage(const GURL& url,
                            bool is_favicon,
                            const gfx::Size& preferred_size,
                            uint32_t max_bitmap_size,
                            bool bypass_cache,
                            ImageDownloadCallback callback) = 0;
~~~
 Sends a request to download the given image |url| and returns the unique
 id of the download request. When the download is finished, |callback| will
 be called with the bitmaps received from the renderer.

 If |is_favicon| is true, the cookies are not sent and not accepted during
 download.

 If there are no bitmap results <= |max_bitmap_size|, the smallest bitmap
 is resized to |max_bitmap_size| and is the only result.

 A |max_bitmap_size| of 0 means unlimited.

 For vector images, |preferred_size| will serve as a viewport into which
 the image will be rendered. This would usually be the dimensions of the
 rectangle where the bitmap will be rendered. If |preferred_size| is empty,
 any existing intrinsic dimensions of the image will be used. If
 |max_bitmap_size| is non-zero it will also impose an upper bound on the
 longest edge of |preferred_size| (|preferred_size| will be scaled down).

 If |bypass_cache| is true, |url| is requested from the server even if it
 is present in the browser cache.

### DownloadImageInFrame

DownloadImageInFrame
~~~cpp
virtual int DownloadImageInFrame(
      const GlobalRenderFrameHostId& initiator_frame_routing_id,
      const GURL& url,
      bool is_favicon,
      const gfx::Size& preferred_size,
      uint32_t max_bitmap_size,
      bool bypass_cache,
      ImageDownloadCallback callback) = 0;
~~~
 Same as DownloadImage(), but uses the ImageDownloader from the specified
 frame instead of the main frame.

### Find

Find
~~~cpp
virtual void Find(int request_id,
                    const std::u16string& search_text,
                    blink::mojom::FindOptionsPtr options,
                    bool skip_delay = false) = 0;
~~~
 Finds text on a page. |search_text| should not be empty. |skip_delay|
 indicates that the find request should be sent to the renderer immediately
 instead of waiting for privacy/performance mitigations.

### StopFinding

StopFinding
~~~cpp
virtual void StopFinding(StopFindAction action) = 0;
~~~
 Notifies the renderer that the user has closed the FindInPage window
 (and what action to take regarding the selection).

### WasEverAudible

WasEverAudible
~~~cpp
virtual bool WasEverAudible() = 0;
~~~
 Returns true if audio has been audible from the WebContents since the last
 navigation.

### IsFullscreen

IsFullscreen
~~~cpp
virtual bool IsFullscreen() = 0;
~~~
 Returns whether the renderer is in fullscreen mode.

### GetOrCreateWebPreferences

GetOrCreateWebPreferences
~~~cpp
virtual const blink::web_pref::WebPreferences&
  GetOrCreateWebPreferences() = 0;
~~~
 Returns a copy of the current WebPreferences associated with this
 WebContents. If it does not exist, this will create one and send the newly
 computed value to all renderers.

 Note that this will not trigger a recomputation of WebPreferences if it
 already exists - this will return the last computed/set value of
 WebPreferences. If we want to guarantee that the value reflects the current
 state of the WebContents, NotifyPreferencesChanged() should be called
 before calling this.

### NotifyPreferencesChanged

NotifyPreferencesChanged
~~~cpp
virtual void NotifyPreferencesChanged() = 0;
~~~
 Notify this WebContents that the preferences have changed, so it needs to
 recompute the current WebPreferences based on the current state of the
 WebContents, etc. This will send an IPC to all the renderer processes
 associated with this WebContents.

 Note that this will do this by creating a new WebPreferences with default
 values, then recomputing some of the attributes based on current states.

 This means if there's any value previously set through SetWebPreferences
 which does not have special recomputation logic in either
 WebContentsImpl::ComputeWebPreferences or
 ContentBrowserClient::OverrideWebkitPrefs, it will return back to its
 default value whenever this function is called.

### SetWebPreferences

SetWebPreferences
~~~cpp
virtual void SetWebPreferences(
      const blink::web_pref::WebPreferences& prefs) = 0;
~~~
 Sets the WebPreferences to |prefs|. This will send an IPC to all the
 renderer processes associated with this WebContents.

 Note that this is different from NotifyPreferencesChanged, which recomputes
 the WebPreferences based on the current state of things. Instead, we're
 setting this to a specific value. This also means that if we trigger a
 recomputation of WebPreferences after this, the WebPreferences value will
 be overridden. if there's any value previously set through
 SetWebPreferences which does not have special recomputation logic in either
 WebContentsImpl::ComputeWebPreferences or
 ContentBrowserClient::OverrideWebkitPrefs, it will return back to its
 default value, which might be different from the value we set it to here.

 If you want to use this function outside of tests, consider adding
 recomputation logic in either of those functions.

 TODO(rakina): Try to make values set through this function stick even after
 recomputations.

### OnWebPreferencesChanged

OnWebPreferencesChanged
~~~cpp
virtual void OnWebPreferencesChanged() = 0;
~~~
 Passes current web preferences to all renderer in this WebContents after
 possibly recomputing them as follows: all "fast" preferences (those not
 requiring slow platform/device polling) are recomputed unconditionally; the
 remaining "slow" ones are recomputed only if they have not been computed
 before.


 This method must be called if any state that affects web preferences has
 changed so that it can be recomputed and sent to the renderer.

### ExitFullscreen

ExitFullscreen
~~~cpp
virtual void ExitFullscreen(bool will_cause_resize) = 0;
~~~
 Requests the renderer to exit fullscreen.

 |will_cause_resize| indicates whether the fullscreen change causes a
 view resize. e.g. This will be false when going from tab fullscreen to
 browser fullscreen.

### ForSecurityDropFullscreen

ForSecurityDropFullscreen
~~~cpp
[[nodiscard]] virtual base::ScopedClosureRunner ForSecurityDropFullscreen(
      int64_t display_id = display::kInvalidDisplayId) = 0;
~~~
 The WebContents is trying to take some action that would cause user
 confusion if taken while in fullscreen. If this WebContents or any outer
 WebContents is in fullscreen, drop it.


 Returns a ScopedClosureRunner, and for the lifetime of that closure, this
 (and other related) WebContentses will not enter fullscreen. If the action
 should cause a one-time dropping of fullscreen (e.g. a UI element not
 attached to the WebContents), invoke RunAndReset() on the returned
 base::ScopedClosureRunner to release the fullscreen block immediately.

 Otherwise, if the action should cause fullscreen to be prohibited for a
 span of time (e.g. a UI element attached to the WebContents), keep the
 closure alive for that duration.


 If |display_id| is valid, only WebContentses on that specific screen will
 exit fullscreen; the scoped prohibition will still apply to all displays.

 This supports sites using cross-screen window placement capabilities to
 retain fullscreen and open or place a window on another screen.

### ResumeLoadingCreatedWebContents

ResumeLoadingCreatedWebContents
~~~cpp
virtual void ResumeLoadingCreatedWebContents() = 0;
~~~
 Unblocks requests from renderer for a newly created window. This is
 used in showCreatedWindow() or sometimes later in cases where
 delegate->ShouldResumeRequestsForCreatedWindow() indicated the requests
 should not yet be resumed. Then the client is responsible for calling this
 as soon as they are ready.

### SetIsOverlayContent

SetIsOverlayContent
~~~cpp
virtual void SetIsOverlayContent(bool is_overlay_content) = 0;
~~~
 Sets whether the WebContents is for overlaying content on a page.

### GetCurrentlyPlayingVideoCount

GetCurrentlyPlayingVideoCount
~~~cpp
virtual int GetCurrentlyPlayingVideoCount() = 0;
~~~

### GetFullscreenVideoSize

GetFullscreenVideoSize
~~~cpp
virtual absl::optional<gfx::Size> GetFullscreenVideoSize() = 0;
~~~

### ClearFocusedElement

ClearFocusedElement
~~~cpp
virtual void ClearFocusedElement() = 0;
~~~
 Tells the renderer to clear the focused element (if any).

### IsFocusedElementEditable

IsFocusedElementEditable
~~~cpp
virtual bool IsFocusedElementEditable() = 0;
~~~
 Returns true if the current focused element is editable.

### IsShowingContextMenu

IsShowingContextMenu
~~~cpp
virtual bool IsShowingContextMenu() = 0;
~~~
 Returns true if a context menu is showing on the page.

### SetShowingContextMenu

SetShowingContextMenu
~~~cpp
virtual void SetShowingContextMenu(bool showing) = 0;
~~~
 Tells the WebContents whether the context menu is showing.

### CompletedFirstVisuallyNonEmptyPaint

CompletedFirstVisuallyNonEmptyPaint
~~~cpp
virtual bool CompletedFirstVisuallyNonEmptyPaint() = 0;
~~~
 BUILDFLAG(IS_ANDROID)
 Returns true if the WebContents has completed its first meaningful paint
 since the last navigation.

### HasRecentInteraction

HasRecentInteraction
~~~cpp
virtual bool HasRecentInteraction() = 0;
~~~
 TODO(https://crbug.com/826293): This is a simple mitigation to validate
 that an action that requires a user gesture actually has one in the
 trustworthy browser process, rather than relying on the untrustworthy
 renderer. This should be eventually merged into and accounted for in the
 user activation work: crbug.com/848778
### SetIgnoreInputEvents

SetIgnoreInputEvents
~~~cpp
virtual void SetIgnoreInputEvents(bool ignore_input_events) = 0;
~~~
 Sets a flag that causes the WebContents to ignore input events.

### GetAudioGroupId

GetAudioGroupId
~~~cpp
virtual base::UnguessableToken GetAudioGroupId() = 0;
~~~
 Returns the group id for all audio streams that correspond to a single
 WebContents. This can be used to determine if a AudioOutputStream was
 created from a renderer that originated from this WebContents.

### GetFaviconURLs

GetFaviconURLs
~~~cpp
virtual const std::vector<blink::mojom::FaviconURLPtr>& GetFaviconURLs() = 0;
~~~
 Returns the raw list of favicon candidates as reported to observers via
 WebContentsObserver::DidUpdateFaviconURL() since the last navigation start.

 Consider using FaviconDriver in components/favicon if possible for more
 reliable favicon-related state.

### UpdateWindowControlsOverlay

UpdateWindowControlsOverlay
~~~cpp
virtual void UpdateWindowControlsOverlay(const gfx::Rect& bounding_rect) = 0;
~~~
 Intended for desktop PWAs with manifest entry of window-controls-overlay,
 This sends the available title bar area bounds to the renderer process.

### GetWindowsControlsOverlayRect

GetWindowsControlsOverlayRect
~~~cpp
virtual gfx::Rect GetWindowsControlsOverlayRect() const = 0;
~~~
 Returns the Window Control Overlay rectangle. Only applies to an
 outermost main frame's widget. Other widgets always returns an empty rect.

### HasActiveEffectivelyFullscreenVideo

HasActiveEffectivelyFullscreenVideo
~~~cpp
virtual bool HasActiveEffectivelyFullscreenVideo() = 0;
~~~
 Whether the WebContents has an active player that is effectively
 fullscreen. That means that the video is either fullscreen or it is the
 content of a fullscreen page (in other words, a fullscreen video with
 custom controls).

### WriteIntoTrace

WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(perfetto::TracedValue context) = 0;
~~~
 Serialise this object into a trace.

### GetCreatorLocation

GetCreatorLocation
~~~cpp
virtual const base::Location& GetCreatorLocation() = 0;
~~~
 Returns the value from CreateParams::creator_location.

### GetPictureInPictureOptions

GetPictureInPictureOptions
~~~cpp
virtual const absl::optional<blink::mojom::PictureInPictureWindowOptions>&
  GetPictureInPictureOptions() const = 0;
~~~
 Returns the parameters associated with PictureInPicture WebContents
### UpdateBrowserControlsState

UpdateBrowserControlsState
~~~cpp
virtual void UpdateBrowserControlsState(cc::BrowserControlsState constraints,
                                          cc::BrowserControlsState current,
                                          bool animate) = 0;
~~~
 Hide or show the browser controls for the given WebContents, based on
 allowed states, desired state and whether the transition should be animated
 or not.

### SetTabSwitchStartTime

SetTabSwitchStartTime
~~~cpp
virtual void SetTabSwitchStartTime(base::TimeTicks start_time,
                                     bool destination_is_loaded) = 0;
~~~
 Sets the last time a tab switch made this WebContents visible.

 `start_time` is the timestamp of the input event that triggered the tab
 switch. `destination_is_loaded` is true when
 ResourceCoordinatorTabHelper::IsLoaded() is true for the new tab contents.

 These will be used to record metrics with the latency between the input
 event and the time when the WebContents is painted.

### StartPrerendering

StartPrerendering
~~~cpp
virtual std::unique_ptr<PrerenderHandle> StartPrerendering(
      const GURL& prerendering_url,
      PrerenderTriggerType trigger_type,
      const std::string& embedder_histogram_suffix,
      ui::PageTransition page_transition,
      PreloadingAttempt* preloading_attempt,
      absl::optional<base::RepeatingCallback<bool(const GURL&)>>
          url_match_predicate = absl::nullopt) = 0;
~~~
 Starts an embedder triggered (browser-initiated) prerendering page and
 returns the unique_ptr<PrerenderHandle>, which cancels prerendering on its
 destruction. If the prerendering failed to start (e.g. if prerendering is
 disabled, failure happened or because this URL is already being
 prerendered), this function returns a nullptr.

 PreloadingAttempt helps us to log various metrics associated with
 particular prerendering attempt. `url_match_predicate` allows embedders to
 define their own predicates for matching same-origin URLs during
 prerendering activation; it would be useful if embedders want Prerender2 to
 ignore some parameter mismatches. Note that if the mismatched prerender URL
 will be activated due to the predicate returning true, the last committed
 URL in the prerendered RenderFrameHost will be activated.

### CreateDisallowCustomCursorScope

CreateDisallowCustomCursorScope
~~~cpp
[[nodiscard]] virtual base::ScopedClosureRunner
  CreateDisallowCustomCursorScope() = 0;
~~~
 Returns a scope object that needs to be owned by caller in order to
 disallow custom cursors. Custom cursors are diallowed in this web contents
 for as long as any of the returned |ScopedClosureRunner| objects is alive.

### WebContents

WebContents
~~~cpp
WebContents() = default;
~~~


###  Create

 Creates a new WebContents.


 The caller is responsible for ensuring that the returned WebContents is
 destroyed (e.g. closed) *before* the BrowserContext associated with
 `params` is destroyed.  It is a security bug if WebContents haven't been
 destroyed when the destructor of BrowserContext starts running.  It is not
 necessarily a bug if WebContents haven't been destroyed when
 BrowserContext::NotifyWillBeDestroyed starts running.


 Best practices for managing the lifetime of `WebContents` and
 `BrowserContext` will vary across different //content embedders.  For
 example, for information specific to the //chrome layer, please see the
 "Managing lifetime of a Profile" section in
 //chrome/browser/profiles/README.md.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<WebContents> Create(
      const CreateParams& params);
~~~
###  CreateWithSessionStorage

 Similar to Create() above but should be used when you need to prepopulate
 the SessionStorageNamespaceMap of the WebContents. This can happen if
 you duplicate a WebContents, try to reconstitute it from a saved state,
 or when you create a new WebContents based on another one (eg., when
 servicing a window.open() call).


 You do not want to call this. If you think you do, make sure you completely
 understand when SessionStorageNamespace objects should be cloned, why
 they should not be shared by multiple WebContents, and what bad things
 can happen if you share the object.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<WebContents> CreateWithSessionStorage(
      const CreateParams& params,
      const SessionStorageNamespaceMap& session_storage_namespace_map);
~~~
### FromRenderViewHost

WebContents::FromRenderViewHost
~~~cpp
CONTENT_EXPORT static WebContents* FromRenderViewHost(RenderViewHost* rvh);
~~~
 Returns the WebContents that owns the RenderViewHost.


 WARNING: `rvh` may belong to a prerendered page, a page in the back/forward
 cache, or a pending deletion page, so it might be inappropriate for it to
 to trigger changes to the WebContents. See also the below comments for
 FromRenderFrameHost().

### FromRenderFrameHost

WebContents::FromRenderFrameHost
~~~cpp
CONTENT_EXPORT static WebContents* FromRenderFrameHost(RenderFrameHost* rfh);
~~~
 Returns the WebContents for the RenderFrameHost. It is unsafe to call this
 function with an invalid (e.g. destructed) `rfh`.


 WARNING: It might be inappropriate for `rfh` to trigger changes to the
 WebContents, so be careful when calling this. Some cases to be aware of
 are:
 * Pages/documents which are not active are not observable by the user
   and therefore should not show UI elements (e.g., a colour picker). These
   features should use `rfh->IsActive()` to determine whether `rfh` is
   active. See the comments there for more information.

 * Pages/documents which are not primary generally should not update
   per-WebContents state (e.g., theme colour). Use
   `rfh->GetPage().IsPrimary()` to check for primary. Fenced frames are
   one case where a RenderFrameHost can be active but not primary.

### FromFrameTreeNodeId

WebContents::FromFrameTreeNodeId
~~~cpp
CONTENT_EXPORT static WebContents* FromFrameTreeNodeId(
      int frame_tree_node_id);
~~~
 Returns the WebContents associated with the |frame_tree_node_id|. This may
 return nullptr if the RenderFrameHost is shutting down.

### SetScreenOrientationDelegate

WebContents::SetScreenOrientationDelegate
~~~cpp
CONTENT_EXPORT static void SetScreenOrientationDelegate(
      ScreenOrientationDelegate* delegate);
~~~
 Sets delegate for platform specific screen orientation functionality.
