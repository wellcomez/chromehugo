
## class GpuFeatureChecker
 GpuFeatureChecker does an asynchronous lookup for a GpuFeatureType. It will
 keep itself alive (via being a refcounted object) until it calls the given
 |callback|. The feature check is not initiated (and a self-reference is not
 taken) until CheckGpuFeatureAvailability() is called. If the feature is
 already available, the |callback| may be called synchronously inside of the
 CheckGpuFeatureAvailability() call.

### field error



~~~cpp

CONTENT_EXPORT static scoped_refptr<GpuFeatureChecker>

~~~


### Create

GpuFeatureChecker::Create
~~~cpp
Create(
      gpu::GpuFeatureType feature,
      FeatureAvailableCallback callback);
~~~

### CheckGpuFeatureAvailability

CheckGpuFeatureAvailability
~~~cpp
virtual void CheckGpuFeatureAvailability() = 0;
~~~
 Checks to see if |feature_| is available on the current GPU. |callback_|
 will be called to indicate the availability of the feature. It may be
 called synchronously if the data is already available, or else will be
 called asynchronously when it becomes available. This method must be called
 from the the UI thread.

 CheckGpuFeatureAvailability() may be called at most once for any given
 GpuFeatureChecker
### ~GpuFeatureChecker

GpuFeatureChecker::~GpuFeatureChecker
~~~cpp
virtual ~GpuFeatureChecker();
~~~

### CheckGpuFeatureAvailability

CheckGpuFeatureAvailability
~~~cpp
virtual void CheckGpuFeatureAvailability() = 0;
~~~
 Checks to see if |feature_| is available on the current GPU. |callback_|
 will be called to indicate the availability of the feature. It may be
 called synchronously if the data is already available, or else will be
 called asynchronously when it becomes available. This method must be called
 from the the UI thread.

 CheckGpuFeatureAvailability() may be called at most once for any given
 GpuFeatureChecker
### field error



~~~cpp

CONTENT_EXPORT static scoped_refptr<GpuFeatureChecker>

~~~

