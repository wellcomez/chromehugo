
## class BrowsingDataFilterBuilder
 A class that builds Origin->bool predicates to filter browsing data. These
 filters can be of two modes - a list of items to delete or a list of items to
 preserve, deleting everything else. The filter entries can be origins or
 registrable domains.


 This class defines interface to build filters for various kinds of browsing
 data. |BuildOriginFilter()| is useful for most browsing data storage backends,
 but some backends, such as website settings and cookies, use other formats of
 filter.

### Create

BrowsingDataFilterBuilder::Create
~~~cpp
static std::unique_ptr<BrowsingDataFilterBuilder> Create(Mode mode);
~~~
 Constructs a filter with the given |mode|: delete or preserve.

 The |OriginMatchingMode| is |kThirdPartiesIncluded| in this case by
 default.

### Create

BrowsingDataFilterBuilder::Create
~~~cpp
static std::unique_ptr<BrowsingDataFilterBuilder> Create(
      Mode mode,
      OriginMatchingMode origin_mode);
~~~
 Same as above, but also allows to choose how origins are matched with
 storage keys.

### ~BrowsingDataFilterBuilder

~BrowsingDataFilterBuilder
~~~cpp
virtual ~BrowsingDataFilterBuilder() = default;
~~~

### AddOrigin

BrowsingDataFilterBuilder::AddOrigin
~~~cpp
virtual void AddOrigin(const url::Origin& origin) = 0;
~~~
 Adds an origin to the filter. Note that this makes it impossible to
 create cookie, channel ID, or plugin filters, as those datatypes are
 scoped more broadly than an origin.

### AddRegisterableDomain

BrowsingDataFilterBuilder::AddRegisterableDomain
~~~cpp
virtual void AddRegisterableDomain(const std::string& registrable_domain) = 0;
~~~
 Adds a registrable domain (e.g. google.com), an internal hostname
 (e.g. localhost), or an IP address (e.g. 127.0.0.1). Other domains, such
 as third and lower level domains (e.g. www.google.com) are not accepted.

 Formally, it must hold that GetDomainAndRegistry(|registrable_domain|, _)
 is |registrable_domain| itself or an empty string for this method
 to accept it.

### SetCookiePartitionKeyCollection

BrowsingDataFilterBuilder::SetCookiePartitionKeyCollection
~~~cpp
virtual void SetCookiePartitionKeyCollection(
      const net::CookiePartitionKeyCollection&
          cookie_partition_key_collection) = 0;
~~~
 Set the CookiePartitionKeyCollection for a CookieDeletionFilter.

 Partitioned cookies will be not be deleted if their partition key is not in
 the keychain. If this method is not invoked, then by default this clears
 all partitioned cookies that match the other criteria.

### IsCrossSiteClearSiteDataForCookies

BrowsingDataFilterBuilder::IsCrossSiteClearSiteDataForCookies
~~~cpp
virtual bool IsCrossSiteClearSiteDataForCookies() const = 0;
~~~
 Returns true if this filter is handling a Clear-Site-Data header sent in a
 cross-site context. Only works for processing the Clear-Site-Data header
 (which means the filter contains a single domain) when partitioned cookies
 are enabled.

 TODO(crbug.com/1416655): Remove this method in favor of
    SetPartitionedStateAllowedOnly.

### SetStorageKey

BrowsingDataFilterBuilder::SetStorageKey
~~~cpp
virtual void SetStorageKey(
      const absl::optional<blink::StorageKey>& storage_key) = 0;
~~~
 Set the StorageKey for the filter.

 If the key is set, then only the StoragePartition that matches the key
 exactly will be deleted. Without the key, all storage that matches the
 other criteria is deleted.

### HasStorageKey

BrowsingDataFilterBuilder::HasStorageKey
~~~cpp
virtual bool HasStorageKey() const = 0;
~~~
 Returns whether the StorageKey is set (e.g. using the method above).

### MatchesWithSavedStorageKey

BrowsingDataFilterBuilder::MatchesWithSavedStorageKey
~~~cpp
virtual bool MatchesWithSavedStorageKey(
      const blink::StorageKey& other_key) const = 0;
~~~
 Returns whether the filter's StorageKey matches the given one.

 Note: the StorageKey in the filter has to be set.

### MatchesAllOriginsAndDomains

BrowsingDataFilterBuilder::MatchesAllOriginsAndDomains
~~~cpp
virtual bool MatchesAllOriginsAndDomains() = 0;
~~~
 Returns true if we're an empty preserve list, where we delete everything.

### SetPartitionedStateAllowedOnly

BrowsingDataFilterBuilder::SetPartitionedStateAllowedOnly
~~~cpp
virtual void SetPartitionedStateAllowedOnly(bool value) = 0;
~~~
 When true, this filter will exclude unpartitioned cookies, i.e. cookies
 whose partition key is null. By default, the value is false.

 Partitioned cookies are unaffected by this setting.

### BuildUrlFilter

BrowsingDataFilterBuilder::BuildUrlFilter
~~~cpp
virtual base::RepeatingCallback<bool(const GURL&)> BuildUrlFilter() = 0;
~~~
 Deprecated: Prefer `BuildOriginFilter()` instead.

 Builds a filter that matches URLs that are in the list to delete, or aren't
 in the list to preserve.

### BuildStorageKeyFilter

BrowsingDataFilterBuilder::BuildStorageKeyFilter
~~~cpp
virtual content::StoragePartition::StorageKeyMatcherFunction
  BuildStorageKeyFilter() = 0;
~~~
 Builds a filter that matches storage keys that are in the list to delete,
 or aren't in the list to preserve. This is preferred to BuildUrlFilter() as
 it preserves storage partitioning information.

### BuildNetworkServiceFilter

BrowsingDataFilterBuilder::BuildNetworkServiceFilter
~~~cpp
virtual network::mojom::ClearDataFilterPtr BuildNetworkServiceFilter() = 0;
~~~
 Builds a filter that can be used with the network service. This uses a Mojo
 struct rather than a predicate function (as used by the rest of the filters
 built by this class) because we need to be able to pass the filter to the
 network service via IPC. Returns nullptr if |IsEmptyPreserveList()| is
 true.

### BuildCookieDeletionFilter

BrowsingDataFilterBuilder::BuildCookieDeletionFilter
~~~cpp
virtual network::mojom::CookieDeletionFilterPtr
  BuildCookieDeletionFilter() = 0;
~~~
 Builds a CookieDeletionInfo object that matches cookies whose sources are
 in the list to delete, or aren't in the list to preserve.

### BuildPluginFilter

BrowsingDataFilterBuilder::BuildPluginFilter
~~~cpp
virtual base::RepeatingCallback<bool(const std::string& site)>
  BuildPluginFilter() = 0;
~~~
 Builds a filter that matches the |site| of a plugin.

### BuildNoopFilter

BrowsingDataFilterBuilder::BuildNoopFilter
~~~cpp
static base::RepeatingCallback<bool(const GURL&)> BuildNoopFilter();
~~~
 A convenience method to produce an empty preserve list, a filter that
 matches everything.

### GetMode

BrowsingDataFilterBuilder::GetMode
~~~cpp
virtual Mode GetMode() = 0;
~~~
 The mode of the filter.

### Copy

BrowsingDataFilterBuilder::Copy
~~~cpp
virtual std::unique_ptr<BrowsingDataFilterBuilder> Copy() = 0;
~~~
 Create a new filter builder with the same set of origins, set of domains,
 and mode.

### operator==

operator==
~~~cpp
bool operator==(const BrowsingDataFilterBuilder& other) const {
    return IsEqual(other);
  }
~~~
 Comparison.
