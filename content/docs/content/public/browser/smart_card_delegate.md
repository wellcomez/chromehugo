
## class SmartCardDelegate
 Interface provided by the content embedder to support the Web Smart Card
 API.

### SmartCardDelegate

SmartCardDelegate
~~~cpp
SmartCardDelegate(SmartCardDelegate&) = delete;
~~~

### operator=

SmartCardDelegate::operator=
~~~cpp
SmartCardDelegate& operator=(SmartCardDelegate&) = delete;
~~~

### ~SmartCardDelegate

~SmartCardDelegate
~~~cpp
virtual ~SmartCardDelegate() = default;
~~~

### GetSmartCardContextFactory

SmartCardDelegate::GetSmartCardContextFactory
~~~cpp
virtual mojo::PendingRemote<device::mojom::SmartCardContextFactory>
  GetSmartCardContextFactory(BrowserContext& browser_context) = 0;
~~~

### SupportsReaderAddedRemovedNotifications

SmartCardDelegate::SupportsReaderAddedRemovedNotifications
~~~cpp
virtual bool SupportsReaderAddedRemovedNotifications() const = 0;
~~~
 Whether the implementation supports notifying when a smart card
 reader device is added or removed from the system.

 Platform dependent.
