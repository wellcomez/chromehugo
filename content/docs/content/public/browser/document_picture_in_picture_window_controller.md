
## class DocumentPictureInPictureWindowController

### SetChildWebContents

SetChildWebContents
~~~cpp
virtual void SetChildWebContents(WebContents* child_contents) = 0;
~~~
 Sets the contents inside the Picture in Picture window.

### DocumentPictureInPictureWindowController

DocumentPictureInPictureWindowController
~~~cpp
DocumentPictureInPictureWindowController() = default;
~~~
 Use PictureInPictureWindowController::GetOrCreateForWebContents() to
 create an instance.

### SetChildWebContents

SetChildWebContents
~~~cpp
virtual void SetChildWebContents(WebContents* child_contents) = 0;
~~~
 Sets the contents inside the Picture in Picture window.

### DocumentPictureInPictureWindowController

DocumentPictureInPictureWindowController
~~~cpp
DocumentPictureInPictureWindowController() = default;
~~~
 Use PictureInPictureWindowController::GetOrCreateForWebContents() to
 create an instance.
