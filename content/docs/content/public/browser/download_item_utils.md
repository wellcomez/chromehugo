
## class DownloadItemUtils
 Helper class to attach WebContents and BrowserContext to a DownloadItem.

### GetWebContents

DownloadItemUtils::GetWebContents
~~~cpp
static WebContents* GetWebContents(
      const download::DownloadItem* downloadItem);
~~~
 Helper method to get the WebContents from a DownloadItem.

### GetRenderFrameHost

DownloadItemUtils::GetRenderFrameHost
~~~cpp
static RenderFrameHost* GetRenderFrameHost(
      const download::DownloadItem* downloadItem);
~~~
 Helper method to get the RenderFrameHost from a DownloadItem.

### AttachInfo

DownloadItemUtils::AttachInfo
~~~cpp
static void AttachInfo(download::DownloadItem* downloadItem,
                         BrowserContext* browser_context,
                         WebContents* web_contents,
                         GlobalRenderFrameHostId id);
~~~
 Attach information to a DownloadItem.

### AttachInfoForTesting

DownloadItemUtils::AttachInfoForTesting
~~~cpp
static void AttachInfoForTesting(download::DownloadItem* downloadItem,
                                   BrowserContext* browser_context,
                                   WebContents* web_contents);
~~~
 Attach information to a DownloadItem.
