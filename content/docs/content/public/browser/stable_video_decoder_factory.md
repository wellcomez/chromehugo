### LaunchStableVideoDecoderFactory

LaunchStableVideoDecoderFactory
~~~cpp
CONTENT_EXPORT void LaunchStableVideoDecoderFactory(
    mojo::PendingReceiver<media::stable::mojom::StableVideoDecoderFactory>
        receiver);
~~~
 Binds a StableVideoDecoderFactory PendingReceiver by either using the crosapi
 (on LaCrOS) or starting a new utility process (on non-LaCrOS). This function
 can be called from any thread.

