
## class PresentationObserver : public
 Observes changes to presentations associated with a WebContents.

### OnDefaultPresentationChanged

OnDefaultPresentationChanged
~~~cpp
virtual void OnDefaultPresentationChanged(
      const content::PresentationRequest* presentation_request) {}
~~~
 `presentation_request` is a nullptr if the default PresentationRequest
 has been removed.
