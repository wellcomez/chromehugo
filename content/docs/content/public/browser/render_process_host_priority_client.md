
## class RenderProcessHostPriorityClient
 Interface for a client that contributes Priority to a RenderProcessHost.

 Clients can call RenderProcessHost::UpdateClientPriority() when their
 Priority changes.

###  BUILDFLAG

 Priority (or on Android, the importance) that a client contributes to a
 RenderProcessHost. E.g. a RenderProcessHost with a visible client has
 higher priority / importance than a RenderProcessHost with hidden clients
 only.

~~~cpp
struct Priority {
    bool is_hidden;
    unsigned int frame_depth;
    bool intersects_viewport;
#if BUILDFLAG(IS_ANDROID)
    ChildProcessImportance importance;
#endif
  };
~~~
### GetPriority

GetPriority
~~~cpp
virtual Priority GetPriority() = 0;
~~~

### ~RenderProcessHostPriorityClient

~RenderProcessHostPriorityClient
~~~cpp
virtual ~RenderProcessHostPriorityClient() = default;
~~~

### GetPriority

GetPriority
~~~cpp
virtual Priority GetPriority() = 0;
~~~

### ~RenderProcessHostPriorityClient

~RenderProcessHostPriorityClient
~~~cpp
virtual ~RenderProcessHostPriorityClient() = default;
~~~

###  BUILDFLAG

 Priority (or on Android, the importance) that a client contributes to a
 RenderProcessHost. E.g. a RenderProcessHost with a visible client has
 higher priority / importance than a RenderProcessHost with hidden clients
 only.

~~~cpp
struct Priority {
    bool is_hidden;
    unsigned int frame_depth;
    bool intersects_viewport;
#if BUILDFLAG(IS_ANDROID)
    ChildProcessImportance importance;
#endif
  };
~~~