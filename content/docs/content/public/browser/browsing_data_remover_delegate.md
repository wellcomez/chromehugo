
## class BrowsingDataRemoverDelegate

### ~BrowsingDataRemoverDelegate

~BrowsingDataRemoverDelegate
~~~cpp
virtual ~BrowsingDataRemoverDelegate() {}
~~~

### GetDomainsForDeferredCookieDeletion

GetDomainsForDeferredCookieDeletion
~~~cpp
virtual std::vector<std::string> GetDomainsForDeferredCookieDeletion(
      uint64_t remove_mask) = 0;
~~~
 The embedder can define domains, for which cookies are only deleted
 after all other deletions are finished.

### GetOriginTypeMatcher

GetOriginTypeMatcher
~~~cpp
virtual EmbedderOriginTypeMatcher GetOriginTypeMatcher() = 0;
~~~
 Returns a MaskMatcherFunction to match embedder's origin types.

 This MaskMatcherFunction will be called with an |origin_type_mask|
 parameter containing ONLY embedder-defined origin types, and must be able
 to handle ALL embedder-defined typed. It must be static and support
 being called on the UI and IO thread.

### MayRemoveDownloadHistory

MayRemoveDownloadHistory
~~~cpp
virtual bool MayRemoveDownloadHistory() = 0;
~~~
 Whether the embedder allows the removal of download history.

### RemoveEmbedderData

RemoveEmbedderData
~~~cpp
virtual void RemoveEmbedderData(
      const base::Time& delete_begin,
      const base::Time& delete_end,
      uint64_t remove_mask,
      BrowsingDataFilterBuilder* filter_builder,
      uint64_t origin_type_mask,
      base::OnceCallback<void(/*failed_data_types=*/uint64_t)> callback) = 0;
~~~
 Removes embedder-specific data. Once done, calls the |callback| and passes
 back any data types for which the deletion failed (which will always be a
 subset of the |remove_mask|).

### OnStartRemoving

OnStartRemoving
~~~cpp
virtual void OnStartRemoving() {}
~~~
 Called when the BrowsingDataRemover starts executing a task.

### OnDoneRemoving

OnDoneRemoving
~~~cpp
virtual void OnDoneRemoving() {}
~~~
 Called when the BrowsingDataRemover is done executing all the tasks in its
 queue.

### ~BrowsingDataRemoverDelegate

~BrowsingDataRemoverDelegate
~~~cpp
virtual ~BrowsingDataRemoverDelegate() {}
~~~

### GetDomainsForDeferredCookieDeletion

GetDomainsForDeferredCookieDeletion
~~~cpp
virtual std::vector<std::string> GetDomainsForDeferredCookieDeletion(
      uint64_t remove_mask) = 0;
~~~
 The embedder can define domains, for which cookies are only deleted
 after all other deletions are finished.

### GetOriginTypeMatcher

GetOriginTypeMatcher
~~~cpp
virtual EmbedderOriginTypeMatcher GetOriginTypeMatcher() = 0;
~~~
 Returns a MaskMatcherFunction to match embedder's origin types.

 This MaskMatcherFunction will be called with an |origin_type_mask|
 parameter containing ONLY embedder-defined origin types, and must be able
 to handle ALL embedder-defined typed. It must be static and support
 being called on the UI and IO thread.

### MayRemoveDownloadHistory

MayRemoveDownloadHistory
~~~cpp
virtual bool MayRemoveDownloadHistory() = 0;
~~~
 Whether the embedder allows the removal of download history.

### RemoveEmbedderData

RemoveEmbedderData
~~~cpp
virtual void RemoveEmbedderData(
      const base::Time& delete_begin,
      const base::Time& delete_end,
      uint64_t remove_mask,
      BrowsingDataFilterBuilder* filter_builder,
      uint64_t origin_type_mask,
      base::OnceCallback<void(/*failed_data_types=*/uint64_t)> callback) = 0;
~~~
 Removes embedder-specific data. Once done, calls the |callback| and passes
 back any data types for which the deletion failed (which will always be a
 subset of the |remove_mask|).

### OnStartRemoving

OnStartRemoving
~~~cpp
virtual void OnStartRemoving() {}
~~~
 Called when the BrowsingDataRemover starts executing a task.

### OnDoneRemoving

OnDoneRemoving
~~~cpp
virtual void OnDoneRemoving() {}
~~~
 Called when the BrowsingDataRemover is done executing all the tasks in its
 queue.
