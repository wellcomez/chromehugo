### std::unique_ptr&lt;media::ProvisionFetcher&gt; CreateProvisionFetcher

std::unique_ptr&lt;media::ProvisionFetcher&gt; CreateProvisionFetcher
~~~cpp
CONTENT_EXPORT
std::unique_ptr<media::ProvisionFetcher> CreateProvisionFetcher(
    scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory);
~~~
 Factory method for media::ProvisionFetcher objects.

