
## struct LoadCommittedDetails
 Provides the details of a committed navigation entry for the
 WebContentsObserver::NavigationEntryCommitted() notification.

### LoadCommittedDetails

LoadCommittedDetails::LoadCommittedDetails
~~~cpp
LoadCommittedDetails(const LoadCommittedDetails&)
~~~

### operator=

LoadCommittedDetails::operator=
~~~cpp
LoadCommittedDetails& operator=(const LoadCommittedDetails&);
~~~

### is_navigation_to_different_page

is_navigation_to_different_page
~~~cpp
bool is_navigation_to_different_page() const {
    return is_main_frame && !is_same_document;
  }
~~~
 Returns whether the main frame navigated to a different page (e.g., not
 scrolling to a fragment inside the current page). We often need this logic
 for showing or hiding something.
