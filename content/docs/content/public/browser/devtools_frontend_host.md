
## class DevToolsFrontendHost
 This class dispatches messages between DevTools frontend and Delegate
 which is implemented by the embedder.

 This allows us to avoid exposing DevTools frontend messages through
 the content public API.

 Note: DevToolsFrontendHost is not supported on Android.

###  Create

 Creates a new DevToolsFrontendHost for RenderFrameHost where DevTools
 frontend is loaded.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<DevToolsFrontendHost> Create(
      RenderFrameHost* frontend_main_frame,
      const HandleMessageCallback& handle_message_callback);
~~~
### SetupExtensionsAPI

DevToolsFrontendHost::SetupExtensionsAPI
~~~cpp
CONTENT_EXPORT static void SetupExtensionsAPI(
      RenderFrameHost* frame,
      const std::string& extension_api);
~~~

### DevToolsFrontendHost

DevToolsFrontendHost
~~~cpp
CONTENT_EXPORT virtual ~DevToolsFrontendHost() {}
~~~

### BadMessageReceived

BadMessageReceived
~~~cpp
CONTENT_EXPORT virtual void BadMessageReceived() {}
~~~

### field error



~~~cpp

CONTENT_EXPORT static scoped_refptr<base::RefCountedMemory>

~~~

 Returns bundled DevTools frontend resource by |path|. Returns null if
 |path| does not correspond to any frontend resource.

### GetFrontendResourceBytes

DevToolsFrontendHost::GetFrontendResourceBytes
~~~cpp
GetFrontendResourceBytes(const std::string& path);
~~~

###  GetFrontendResource

 Convenience wrapper to return GetFrontendResourceBytes() as a string.

~~~cpp
CONTENT_EXPORT static std::string GetFrontendResource(
      const std::string& path);
~~~
### DevToolsFrontendHost

DevToolsFrontendHost
~~~cpp
CONTENT_EXPORT virtual ~DevToolsFrontendHost() {}
~~~

### BadMessageReceived

BadMessageReceived
~~~cpp
CONTENT_EXPORT virtual void BadMessageReceived() {}
~~~

###  Create

 Creates a new DevToolsFrontendHost for RenderFrameHost where DevTools
 frontend is loaded.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<DevToolsFrontendHost> Create(
      RenderFrameHost* frontend_main_frame,
      const HandleMessageCallback& handle_message_callback);
~~~
### SetupExtensionsAPI

DevToolsFrontendHost::SetupExtensionsAPI
~~~cpp
CONTENT_EXPORT static void SetupExtensionsAPI(
      RenderFrameHost* frame,
      const std::string& extension_api);
~~~

### field error



~~~cpp

CONTENT_EXPORT static scoped_refptr<base::RefCountedMemory>

~~~

 Returns bundled DevTools frontend resource by |path|. Returns null if
 |path| does not correspond to any frontend resource.

###  GetFrontendResource

 Convenience wrapper to return GetFrontendResourceBytes() as a string.

~~~cpp
CONTENT_EXPORT static std::string GetFrontendResource(
      const std::string& path);
~~~