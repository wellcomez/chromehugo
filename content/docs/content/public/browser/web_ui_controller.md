
## class WebUIController
 A WebUI page is controlled by the embedder's WebUIController object. It
 manages the data source and message handlers.

### WebUIRenderFrameCreated

WebUIRenderFrameCreated
~~~cpp
explicit WebUIController(WebUI* web_ui);
  virtual ~WebUIController();

  // Allows the controller to override handling all messages from the page.
  // Return true if the message handling was overridden.
  virtual bool OverrideHandleWebUIMessage(const GURL& source_url,
                                          const std::string& message,
                                          const base::Value::List& args);

  // Called when a WebUI RenderFrame is created.  This is *not* called for every
  // page load because in some cases a RenderFrame will be reused, for example
  // when reloading or navigating to a same-site URL.
  // This is deliberately named to differentiate from
  // WebContentsObserver::RenderFrameCreated, as some classes may override both.
  virtual void WebUIRenderFrameCreated(RenderFrameHost* render_frame_host) {}
~~~

### WebUIPrimaryPageChanged

WebUIPrimaryPageChanged
~~~cpp
virtual void WebUIPrimaryPageChanged(Page& page) {}
~~~
 Called when the WebUI's primary page changes. WebUIControllers should reset
 its state if necessary.

### WebUIReadyToCommitNavigation

WebUIController::WebUIReadyToCommitNavigation
~~~cpp
void WebUIReadyToCommitNavigation(RenderFrameHost* render_frame_host);
~~~
 Called when a WebUI page load is about to be committed, even if RenderFrame
 is reused. This sets up MojoJS interface broker.

### web_ui

web_ui
~~~cpp
WebUI* web_ui() const { return web_ui_; }
~~~

### GetAs

GetAs
~~~cpp
T* GetAs() {
    CHECK(GetType())
        << "WebUIController::GetAs() called on subclass which is missing "
           "WEB_UI_CONTROLLER_TYPE_DECL().";

    return GetType() == &T::kWebUIControllerType ? static_cast<T*>(this)
                                                 : nullptr;
  }
~~~

### IsJavascriptErrorReportingEnabled

WebUIController::IsJavascriptErrorReportingEnabled
~~~cpp
virtual bool IsJavascriptErrorReportingEnabled();
~~~
 Controls whether the engineering team receives JavaScript error reports for
 this WebUI. For example, WebUIs may report JavaScript errors and unhandled
 exceptions to an error reporting service if this function isn't called.


 WebUIs may want to override this function if they are reporting errors via
 other channels and don't want duplicates. For instance, a WebUI which uses
 crashReportPrivate to report JS errors might override this function to
 return to false in order to avoid duplicate reports. WebUIs might also
 override this function to return false to avoid noise if the engineering
 team doesn't expect to fix reported errors; for instance, a low-usage
 debugging page might turn off error reports if the owners feel any reported
 bugs would be too low priority to bother with.

### GetType

WebUIController::GetType
~~~cpp
virtual Type GetType();
~~~
 TODO(calamity): Make this abstract once all subclasses implement GetType().

### broker_for_testing

broker_for_testing
~~~cpp
PerWebUIBrowserInterfaceBroker* broker_for_testing() { return broker_.get(); }
~~~
