### SessionStorageNamespaceMap
CreateMapWithDefaultSessionStorageNamespace

SessionStorageNamespaceMap
CreateMapWithDefaultSessionStorageNamespace
~~~cpp
CONTENT_EXPORT SessionStorageNamespaceMap
CreateMapWithDefaultSessionStorageNamespace(
    BrowserContext* browser_context,
    scoped_refptr<SessionStorageNamespace> session_storage_namespace);
~~~
 Helper function that creates a SessionStorageNamespaceMap and assigns
 `session_storage_namespace` to the default StoragePartitionConfig.

## class SessionStorageNamespace
 This is a ref-counted class that represents a SessionStorageNamespace.

 On destruction it ensures that the storage namespace is destroyed.

### id

id
~~~cpp
virtual const std::string& id() = 0;
~~~
 Returns the ID for the |SessionStorageNamespace|. The ID is unique among
 all SessionStorageNamespace objects and across browser runs.

### SetShouldPersist

SetShouldPersist
~~~cpp
virtual void SetShouldPersist(bool should_persist) = 0;
~~~
 For marking that the sessionStorage will be needed or won't be needed by
 session restore.

### should_persist

should_persist
~~~cpp
virtual bool should_persist() = 0;
~~~

### ~SessionStorageNamespace

~SessionStorageNamespace
~~~cpp
virtual ~SessionStorageNamespace() {}
~~~

### id

id
~~~cpp
virtual const std::string& id() = 0;
~~~
 Returns the ID for the |SessionStorageNamespace|. The ID is unique among
 all SessionStorageNamespace objects and across browser runs.

### SetShouldPersist

SetShouldPersist
~~~cpp
virtual void SetShouldPersist(bool should_persist) = 0;
~~~
 For marking that the sessionStorage will be needed or won't be needed by
 session restore.

### should_persist

should_persist
~~~cpp
virtual bool should_persist() = 0;
~~~

### ~SessionStorageNamespace

~SessionStorageNamespace
~~~cpp
virtual ~SessionStorageNamespace() {}
~~~
