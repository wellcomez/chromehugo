
## class XRRuntimeManager
 The XRRuntimeManager is a singleton responsible for managing access to the
 active BrowserXRRuntime instances. An Observer interface is provided in case
 runtimes need to interact with runtimes when they are added (e.g. to notify
 them of any current state that they may need to know about).

### GetInstanceIfCreated

XRRuntimeManager::GetInstanceIfCreated
~~~cpp
static content::XRRuntimeManager* GetInstanceIfCreated();
~~~
 Provides access to the XRRuntimeManager singleton, if it exists.

 This method does not extend the lifetime of the singleton, so you should be
 careful with the lifetime of this reference.

### ExitImmersivePresentation

XRRuntimeManager::ExitImmersivePresentation
~~~cpp
static void ExitImmersivePresentation();
~~~
 Exits any currently presenting immersive session.

### AddObserver

XRRuntimeManager::AddObserver
~~~cpp
static void AddObserver(content::XRRuntimeManager::Observer* observer);
~~~
 Observer registration methods are static so that observers may subscribe
 and unsubscribe independent of the lifecycle of the XRRuntimeManager
 Singleton.

### RemoveObserver

XRRuntimeManager::RemoveObserver
~~~cpp
static void RemoveObserver(content::XRRuntimeManager::Observer* observer);
~~~

### XRRuntimeManager

XRRuntimeManager
~~~cpp
XRRuntimeManager() = default;
~~~

### ~XRRuntimeManager

~XRRuntimeManager
~~~cpp
virtual ~XRRuntimeManager() = default;
~~~

### XRRuntimeManager

XRRuntimeManager
~~~cpp
XRRuntimeManager(const XRRuntimeManager&) = delete;
~~~

### operator=

XRRuntimeManager::operator=
~~~cpp
XRRuntimeManager& operator=(const XRRuntimeManager&) = delete;
~~~

### GetRuntime

XRRuntimeManager::GetRuntime
~~~cpp
virtual content::BrowserXRRuntime* GetRuntime(
      device::mojom::XRDeviceId id) = 0;
~~~
 Provides access to the BrowserXRRuntime corresponding to the given
 XRDeviceId, or nullptr if no such device exists/has been registered.

### ForEachRuntime

XRRuntimeManager::ForEachRuntime
~~~cpp
virtual void ForEachRuntime(
      base::RepeatingCallback<void(content::BrowserXRRuntime*)> fn) = 0;
~~~
 Provides a mechanism for performing operations on/reasoning about all
 currently active runtimes, without exposing the collection.
