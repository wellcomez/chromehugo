
## class ;::;::ServiceWorkerContextObserver

###  ErrorInfo


~~~cpp
struct ErrorInfo {
    ErrorInfo(const std::u16string& message,
              int line,
              int column,
              const GURL& url)
        : error_message(message),
          line_number(line),
          column_number(column),
          source_url(url) {}
    ErrorInfo(const ErrorInfo& info) = default;
    const std::u16string error_message;
    const int line_number;
    const int column_number;
    const GURL source_url;
  };
~~~
### OnRegistrationCompleted

OnRegistrationCompleted
~~~cpp
virtual void OnRegistrationCompleted(const GURL& scope) {}
~~~
 Called when a service worker has been registered with scope |scope|.


 This is called when the ServiceWorkerContainer.register() promise is
 resolved, which happens before the service worker registration is persisted
 to disk.

### OnRegistrationStored

OnRegistrationStored
~~~cpp
virtual void OnRegistrationStored(int64_t registration_id,
                                    const GURL& scope) {}
~~~
 Called after a service worker registration is persisted to storage with
 registration ID |registration_id| and scope |scope|.


 This happens after OnRegistrationCompleted().

### OnVersionActivated

OnVersionActivated
~~~cpp
virtual void OnVersionActivated(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 activated.

### OnVersionRedundant

OnVersionRedundant
~~~cpp
virtual void OnVersionRedundant(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 redundant.

### OnVersionStartedRunning

OnVersionStartedRunning
~~~cpp
virtual void OnVersionStartedRunning(
      int64_t version_id,
      const ServiceWorkerRunningInfo& running_info) {}
~~~
 Called when the service worker with id |version_id| starts or stops
 running.


 These functions are currently only called after a worker finishes
 starting/stopping or the version is destroyed before finishing
 stopping. That is, a worker in the process of starting is not yet
 considered running, even if it's executing JavaScript.


 TODO(minggang): Create a new observer to listen to the events when the
 process of the service worker is allocated/released, instead of using the
 running status of the embedded worker.

### OnVersionStoppedRunning

OnVersionStoppedRunning
~~~cpp
virtual void OnVersionStoppedRunning(int64_t version_id) {}
~~~

### OnControlleeAdded

OnControlleeAdded
~~~cpp
virtual void OnControlleeAdded(int64_t version_id,
                                 const std::string& client_uuid,
                                 const ServiceWorkerClientInfo& client_info) {}
~~~
 Called when a controllee is added/removed for the service worker with id
 |version_id|.

### OnControlleeRemoved

OnControlleeRemoved
~~~cpp
virtual void OnControlleeRemoved(int64_t version_id,
                                   const std::string& client_uuid) {}
~~~

### OnNoControllees

OnNoControllees
~~~cpp
virtual void OnNoControllees(int64_t version_id, const GURL& scope) {}
~~~
 Called when there are no more controllees for the service worker with id
 |version_id|.

### OnControlleeNavigationCommitted

OnControlleeNavigationCommitted
~~~cpp
virtual void OnControlleeNavigationCommitted(
      int64_t version_id,
      const std::string& client_uuid,
      GlobalRenderFrameHostId render_frame_host_id) {}
~~~
 Called when the navigation for a window client commits to a render frame
 host. At this point, if there was a previous controllee attached to that
 RenderFrameHost, it has already been removed and OnControlleeRemoved()
 has been called.

### OnErrorReported

OnErrorReported
~~~cpp
virtual void OnErrorReported(int64_t version_id,
                               const GURL& scope,
                               const ErrorInfo& info) {}
~~~
 Called when an error is reported for the service worker with id
 |version_id|.

### OnReportConsoleMessage

OnReportConsoleMessage
~~~cpp
virtual void OnReportConsoleMessage(int64_t version_id,
                                      const GURL& scope,
                                      const ConsoleMessage& message) {}
~~~
 Called when a console message is reported for the service worker with id
 |version_id|.

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct(ServiceWorkerContext* context) {}
~~~
 Called when |context| is destroyed. Observers must no longer use |context|.

### ~ServiceWorkerContextObserver

~ServiceWorkerContextObserver
~~~cpp
virtual ~ServiceWorkerContextObserver() {}
~~~

### OnRegistrationCompleted

OnRegistrationCompleted
~~~cpp
virtual void OnRegistrationCompleted(const GURL& scope) {}
~~~
 Called when a service worker has been registered with scope |scope|.


 This is called when the ServiceWorkerContainer.register() promise is
 resolved, which happens before the service worker registration is persisted
 to disk.

### OnRegistrationStored

OnRegistrationStored
~~~cpp
virtual void OnRegistrationStored(int64_t registration_id,
                                    const GURL& scope) {}
~~~
 Called after a service worker registration is persisted to storage with
 registration ID |registration_id| and scope |scope|.


 This happens after OnRegistrationCompleted().

### OnVersionActivated

OnVersionActivated
~~~cpp
virtual void OnVersionActivated(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 activated.

### OnVersionRedundant

OnVersionRedundant
~~~cpp
virtual void OnVersionRedundant(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 redundant.

### OnVersionStartedRunning

OnVersionStartedRunning
~~~cpp
virtual void OnVersionStartedRunning(
      int64_t version_id,
      const ServiceWorkerRunningInfo& running_info) {}
~~~
 Called when the service worker with id |version_id| starts or stops
 running.


 These functions are currently only called after a worker finishes
 starting/stopping or the version is destroyed before finishing
 stopping. That is, a worker in the process of starting is not yet
 considered running, even if it's executing JavaScript.


 TODO(minggang): Create a new observer to listen to the events when the
 process of the service worker is allocated/released, instead of using the
 running status of the embedded worker.

### OnVersionStoppedRunning

OnVersionStoppedRunning
~~~cpp
virtual void OnVersionStoppedRunning(int64_t version_id) {}
~~~

### OnControlleeAdded

OnControlleeAdded
~~~cpp
virtual void OnControlleeAdded(int64_t version_id,
                                 const std::string& client_uuid,
                                 const ServiceWorkerClientInfo& client_info) {}
~~~
 Called when a controllee is added/removed for the service worker with id
 |version_id|.

### OnControlleeRemoved

OnControlleeRemoved
~~~cpp
virtual void OnControlleeRemoved(int64_t version_id,
                                   const std::string& client_uuid) {}
~~~

### OnNoControllees

OnNoControllees
~~~cpp
virtual void OnNoControllees(int64_t version_id, const GURL& scope) {}
~~~
 Called when there are no more controllees for the service worker with id
 |version_id|.

### OnControlleeNavigationCommitted

OnControlleeNavigationCommitted
~~~cpp
virtual void OnControlleeNavigationCommitted(
      int64_t version_id,
      const std::string& client_uuid,
      GlobalRenderFrameHostId render_frame_host_id) {}
~~~
 Called when the navigation for a window client commits to a render frame
 host. At this point, if there was a previous controllee attached to that
 RenderFrameHost, it has already been removed and OnControlleeRemoved()
 has been called.

### OnErrorReported

OnErrorReported
~~~cpp
virtual void OnErrorReported(int64_t version_id,
                               const GURL& scope,
                               const ErrorInfo& info) {}
~~~
 Called when an error is reported for the service worker with id
 |version_id|.

### OnReportConsoleMessage

OnReportConsoleMessage
~~~cpp
virtual void OnReportConsoleMessage(int64_t version_id,
                                      const GURL& scope,
                                      const ConsoleMessage& message) {}
~~~
 Called when a console message is reported for the service worker with id
 |version_id|.

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct(ServiceWorkerContext* context) {}
~~~
 Called when |context| is destroyed. Observers must no longer use |context|.

### ~ServiceWorkerContextObserver

~ServiceWorkerContextObserver
~~~cpp
virtual ~ServiceWorkerContextObserver() {}
~~~

###  ErrorInfo


~~~cpp
struct ErrorInfo {
    ErrorInfo(const std::u16string& message,
              int line,
              int column,
              const GURL& url)
        : error_message(message),
          line_number(line),
          column_number(column),
          source_url(url) {}
    ErrorInfo(const ErrorInfo& info) = default;
    const std::u16string error_message;
    const int line_number;
    const int column_number;
    const GURL source_url;
  };
~~~
## class ;::ServiceWorkerContextObserver

###  ErrorInfo


~~~cpp
struct ErrorInfo {
    ErrorInfo(const std::u16string& message,
              int line,
              int column,
              const GURL& url)
        : error_message(message),
          line_number(line),
          column_number(column),
          source_url(url) {}
    ErrorInfo(const ErrorInfo& info) = default;
    const std::u16string error_message;
    const int line_number;
    const int column_number;
    const GURL source_url;
  };
~~~
### OnRegistrationCompleted

OnRegistrationCompleted
~~~cpp
virtual void OnRegistrationCompleted(const GURL& scope) {}
~~~
 Called when a service worker has been registered with scope |scope|.


 This is called when the ServiceWorkerContainer.register() promise is
 resolved, which happens before the service worker registration is persisted
 to disk.

### OnRegistrationStored

OnRegistrationStored
~~~cpp
virtual void OnRegistrationStored(int64_t registration_id,
                                    const GURL& scope) {}
~~~
 Called after a service worker registration is persisted to storage with
 registration ID |registration_id| and scope |scope|.


 This happens after OnRegistrationCompleted().

### OnVersionActivated

OnVersionActivated
~~~cpp
virtual void OnVersionActivated(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 activated.

### OnVersionRedundant

OnVersionRedundant
~~~cpp
virtual void OnVersionRedundant(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 redundant.

### OnVersionStartedRunning

OnVersionStartedRunning
~~~cpp
virtual void OnVersionStartedRunning(
      int64_t version_id,
      const ServiceWorkerRunningInfo& running_info) {}
~~~
 Called when the service worker with id |version_id| starts or stops
 running.


 These functions are currently only called after a worker finishes
 starting/stopping or the version is destroyed before finishing
 stopping. That is, a worker in the process of starting is not yet
 considered running, even if it's executing JavaScript.


 TODO(minggang): Create a new observer to listen to the events when the
 process of the service worker is allocated/released, instead of using the
 running status of the embedded worker.

### OnVersionStoppedRunning

OnVersionStoppedRunning
~~~cpp
virtual void OnVersionStoppedRunning(int64_t version_id) {}
~~~

### OnControlleeAdded

OnControlleeAdded
~~~cpp
virtual void OnControlleeAdded(int64_t version_id,
                                 const std::string& client_uuid,
                                 const ServiceWorkerClientInfo& client_info) {}
~~~
 Called when a controllee is added/removed for the service worker with id
 |version_id|.

### OnControlleeRemoved

OnControlleeRemoved
~~~cpp
virtual void OnControlleeRemoved(int64_t version_id,
                                   const std::string& client_uuid) {}
~~~

### OnNoControllees

OnNoControllees
~~~cpp
virtual void OnNoControllees(int64_t version_id, const GURL& scope) {}
~~~
 Called when there are no more controllees for the service worker with id
 |version_id|.

### OnControlleeNavigationCommitted

OnControlleeNavigationCommitted
~~~cpp
virtual void OnControlleeNavigationCommitted(
      int64_t version_id,
      const std::string& client_uuid,
      GlobalRenderFrameHostId render_frame_host_id) {}
~~~
 Called when the navigation for a window client commits to a render frame
 host. At this point, if there was a previous controllee attached to that
 RenderFrameHost, it has already been removed and OnControlleeRemoved()
 has been called.

### OnErrorReported

OnErrorReported
~~~cpp
virtual void OnErrorReported(int64_t version_id,
                               const GURL& scope,
                               const ErrorInfo& info) {}
~~~
 Called when an error is reported for the service worker with id
 |version_id|.

### OnReportConsoleMessage

OnReportConsoleMessage
~~~cpp
virtual void OnReportConsoleMessage(int64_t version_id,
                                      const GURL& scope,
                                      const ConsoleMessage& message) {}
~~~
 Called when a console message is reported for the service worker with id
 |version_id|.

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct(ServiceWorkerContext* context) {}
~~~
 Called when |context| is destroyed. Observers must no longer use |context|.

### ~ServiceWorkerContextObserver

~ServiceWorkerContextObserver
~~~cpp
virtual ~ServiceWorkerContextObserver() {}
~~~

### OnRegistrationCompleted

OnRegistrationCompleted
~~~cpp
virtual void OnRegistrationCompleted(const GURL& scope) {}
~~~
 Called when a service worker has been registered with scope |scope|.


 This is called when the ServiceWorkerContainer.register() promise is
 resolved, which happens before the service worker registration is persisted
 to disk.

### OnRegistrationStored

OnRegistrationStored
~~~cpp
virtual void OnRegistrationStored(int64_t registration_id,
                                    const GURL& scope) {}
~~~
 Called after a service worker registration is persisted to storage with
 registration ID |registration_id| and scope |scope|.


 This happens after OnRegistrationCompleted().

### OnVersionActivated

OnVersionActivated
~~~cpp
virtual void OnVersionActivated(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 activated.

### OnVersionRedundant

OnVersionRedundant
~~~cpp
virtual void OnVersionRedundant(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 redundant.

### OnVersionStartedRunning

OnVersionStartedRunning
~~~cpp
virtual void OnVersionStartedRunning(
      int64_t version_id,
      const ServiceWorkerRunningInfo& running_info) {}
~~~
 Called when the service worker with id |version_id| starts or stops
 running.


 These functions are currently only called after a worker finishes
 starting/stopping or the version is destroyed before finishing
 stopping. That is, a worker in the process of starting is not yet
 considered running, even if it's executing JavaScript.


 TODO(minggang): Create a new observer to listen to the events when the
 process of the service worker is allocated/released, instead of using the
 running status of the embedded worker.

### OnVersionStoppedRunning

OnVersionStoppedRunning
~~~cpp
virtual void OnVersionStoppedRunning(int64_t version_id) {}
~~~

### OnControlleeAdded

OnControlleeAdded
~~~cpp
virtual void OnControlleeAdded(int64_t version_id,
                                 const std::string& client_uuid,
                                 const ServiceWorkerClientInfo& client_info) {}
~~~
 Called when a controllee is added/removed for the service worker with id
 |version_id|.

### OnControlleeRemoved

OnControlleeRemoved
~~~cpp
virtual void OnControlleeRemoved(int64_t version_id,
                                   const std::string& client_uuid) {}
~~~

### OnNoControllees

OnNoControllees
~~~cpp
virtual void OnNoControllees(int64_t version_id, const GURL& scope) {}
~~~
 Called when there are no more controllees for the service worker with id
 |version_id|.

### OnControlleeNavigationCommitted

OnControlleeNavigationCommitted
~~~cpp
virtual void OnControlleeNavigationCommitted(
      int64_t version_id,
      const std::string& client_uuid,
      GlobalRenderFrameHostId render_frame_host_id) {}
~~~
 Called when the navigation for a window client commits to a render frame
 host. At this point, if there was a previous controllee attached to that
 RenderFrameHost, it has already been removed and OnControlleeRemoved()
 has been called.

### OnErrorReported

OnErrorReported
~~~cpp
virtual void OnErrorReported(int64_t version_id,
                               const GURL& scope,
                               const ErrorInfo& info) {}
~~~
 Called when an error is reported for the service worker with id
 |version_id|.

### OnReportConsoleMessage

OnReportConsoleMessage
~~~cpp
virtual void OnReportConsoleMessage(int64_t version_id,
                                      const GURL& scope,
                                      const ConsoleMessage& message) {}
~~~
 Called when a console message is reported for the service worker with id
 |version_id|.

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct(ServiceWorkerContext* context) {}
~~~
 Called when |context| is destroyed. Observers must no longer use |context|.

### ~ServiceWorkerContextObserver

~ServiceWorkerContextObserver
~~~cpp
virtual ~ServiceWorkerContextObserver() {}
~~~

###  ErrorInfo


~~~cpp
struct ErrorInfo {
    ErrorInfo(const std::u16string& message,
              int line,
              int column,
              const GURL& url)
        : error_message(message),
          line_number(line),
          column_number(column),
          source_url(url) {}
    ErrorInfo(const ErrorInfo& info) = default;
    const std::u16string error_message;
    const int line_number;
    const int column_number;
    const GURL source_url;
  };
~~~
## class ServiceWorkerContextObserver

###  ErrorInfo


~~~cpp
struct ErrorInfo {
    ErrorInfo(const std::u16string& message,
              int line,
              int column,
              const GURL& url)
        : error_message(message),
          line_number(line),
          column_number(column),
          source_url(url) {}
    ErrorInfo(const ErrorInfo& info) = default;
    const std::u16string error_message;
    const int line_number;
    const int column_number;
    const GURL source_url;
  };
~~~
### OnRegistrationCompleted

OnRegistrationCompleted
~~~cpp
virtual void OnRegistrationCompleted(const GURL& scope) {}
~~~
 Called when a service worker has been registered with scope |scope|.


 This is called when the ServiceWorkerContainer.register() promise is
 resolved, which happens before the service worker registration is persisted
 to disk.

### OnRegistrationStored

OnRegistrationStored
~~~cpp
virtual void OnRegistrationStored(int64_t registration_id,
                                    const GURL& scope) {}
~~~
 Called after a service worker registration is persisted to storage with
 registration ID |registration_id| and scope |scope|.


 This happens after OnRegistrationCompleted().

### OnVersionActivated

OnVersionActivated
~~~cpp
virtual void OnVersionActivated(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 activated.

### OnVersionRedundant

OnVersionRedundant
~~~cpp
virtual void OnVersionRedundant(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 redundant.

### OnVersionStartedRunning

OnVersionStartedRunning
~~~cpp
virtual void OnVersionStartedRunning(
      int64_t version_id,
      const ServiceWorkerRunningInfo& running_info) {}
~~~
 Called when the service worker with id |version_id| starts or stops
 running.


 These functions are currently only called after a worker finishes
 starting/stopping or the version is destroyed before finishing
 stopping. That is, a worker in the process of starting is not yet
 considered running, even if it's executing JavaScript.


 TODO(minggang): Create a new observer to listen to the events when the
 process of the service worker is allocated/released, instead of using the
 running status of the embedded worker.

### OnVersionStoppedRunning

OnVersionStoppedRunning
~~~cpp
virtual void OnVersionStoppedRunning(int64_t version_id) {}
~~~

### OnControlleeAdded

OnControlleeAdded
~~~cpp
virtual void OnControlleeAdded(int64_t version_id,
                                 const std::string& client_uuid,
                                 const ServiceWorkerClientInfo& client_info) {}
~~~
 Called when a controllee is added/removed for the service worker with id
 |version_id|.

### OnControlleeRemoved

OnControlleeRemoved
~~~cpp
virtual void OnControlleeRemoved(int64_t version_id,
                                   const std::string& client_uuid) {}
~~~

### OnNoControllees

OnNoControllees
~~~cpp
virtual void OnNoControllees(int64_t version_id, const GURL& scope) {}
~~~
 Called when there are no more controllees for the service worker with id
 |version_id|.

### OnControlleeNavigationCommitted

OnControlleeNavigationCommitted
~~~cpp
virtual void OnControlleeNavigationCommitted(
      int64_t version_id,
      const std::string& client_uuid,
      GlobalRenderFrameHostId render_frame_host_id) {}
~~~
 Called when the navigation for a window client commits to a render frame
 host. At this point, if there was a previous controllee attached to that
 RenderFrameHost, it has already been removed and OnControlleeRemoved()
 has been called.

### OnErrorReported

OnErrorReported
~~~cpp
virtual void OnErrorReported(int64_t version_id,
                               const GURL& scope,
                               const ErrorInfo& info) {}
~~~
 Called when an error is reported for the service worker with id
 |version_id|.

### OnReportConsoleMessage

OnReportConsoleMessage
~~~cpp
virtual void OnReportConsoleMessage(int64_t version_id,
                                      const GURL& scope,
                                      const ConsoleMessage& message) {}
~~~
 Called when a console message is reported for the service worker with id
 |version_id|.

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct(ServiceWorkerContext* context) {}
~~~
 Called when |context| is destroyed. Observers must no longer use |context|.

### ~ServiceWorkerContextObserver

~ServiceWorkerContextObserver
~~~cpp
virtual ~ServiceWorkerContextObserver() {}
~~~

### OnRegistrationCompleted

OnRegistrationCompleted
~~~cpp
virtual void OnRegistrationCompleted(const GURL& scope) {}
~~~
 Called when a service worker has been registered with scope |scope|.


 This is called when the ServiceWorkerContainer.register() promise is
 resolved, which happens before the service worker registration is persisted
 to disk.

### OnRegistrationStored

OnRegistrationStored
~~~cpp
virtual void OnRegistrationStored(int64_t registration_id,
                                    const GURL& scope) {}
~~~
 Called after a service worker registration is persisted to storage with
 registration ID |registration_id| and scope |scope|.


 This happens after OnRegistrationCompleted().

### OnVersionActivated

OnVersionActivated
~~~cpp
virtual void OnVersionActivated(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 activated.

### OnVersionRedundant

OnVersionRedundant
~~~cpp
virtual void OnVersionRedundant(int64_t version_id, const GURL& scope) {}
~~~
 Called when the service worker with id |version_id| changes status to
 redundant.

### OnVersionStartedRunning

OnVersionStartedRunning
~~~cpp
virtual void OnVersionStartedRunning(
      int64_t version_id,
      const ServiceWorkerRunningInfo& running_info) {}
~~~
 Called when the service worker with id |version_id| starts or stops
 running.


 These functions are currently only called after a worker finishes
 starting/stopping or the version is destroyed before finishing
 stopping. That is, a worker in the process of starting is not yet
 considered running, even if it's executing JavaScript.


 TODO(minggang): Create a new observer to listen to the events when the
 process of the service worker is allocated/released, instead of using the
 running status of the embedded worker.

### OnVersionStoppedRunning

OnVersionStoppedRunning
~~~cpp
virtual void OnVersionStoppedRunning(int64_t version_id) {}
~~~

### OnControlleeAdded

OnControlleeAdded
~~~cpp
virtual void OnControlleeAdded(int64_t version_id,
                                 const std::string& client_uuid,
                                 const ServiceWorkerClientInfo& client_info) {}
~~~
 Called when a controllee is added/removed for the service worker with id
 |version_id|.

### OnControlleeRemoved

OnControlleeRemoved
~~~cpp
virtual void OnControlleeRemoved(int64_t version_id,
                                   const std::string& client_uuid) {}
~~~

### OnNoControllees

OnNoControllees
~~~cpp
virtual void OnNoControllees(int64_t version_id, const GURL& scope) {}
~~~
 Called when there are no more controllees for the service worker with id
 |version_id|.

### OnControlleeNavigationCommitted

OnControlleeNavigationCommitted
~~~cpp
virtual void OnControlleeNavigationCommitted(
      int64_t version_id,
      const std::string& client_uuid,
      GlobalRenderFrameHostId render_frame_host_id) {}
~~~
 Called when the navigation for a window client commits to a render frame
 host. At this point, if there was a previous controllee attached to that
 RenderFrameHost, it has already been removed and OnControlleeRemoved()
 has been called.

### OnErrorReported

OnErrorReported
~~~cpp
virtual void OnErrorReported(int64_t version_id,
                               const GURL& scope,
                               const ErrorInfo& info) {}
~~~
 Called when an error is reported for the service worker with id
 |version_id|.

### OnReportConsoleMessage

OnReportConsoleMessage
~~~cpp
virtual void OnReportConsoleMessage(int64_t version_id,
                                      const GURL& scope,
                                      const ConsoleMessage& message) {}
~~~
 Called when a console message is reported for the service worker with id
 |version_id|.

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct(ServiceWorkerContext* context) {}
~~~
 Called when |context| is destroyed. Observers must no longer use |context|.

### ~ServiceWorkerContextObserver

~ServiceWorkerContextObserver
~~~cpp
virtual ~ServiceWorkerContextObserver() {}
~~~

###  ErrorInfo


~~~cpp
struct ErrorInfo {
    ErrorInfo(const std::u16string& message,
              int line,
              int column,
              const GURL& url)
        : error_message(message),
          line_number(line),
          column_number(column),
          source_url(url) {}
    ErrorInfo(const ErrorInfo& info) = default;
    const std::u16string error_message;
    const int line_number;
    const int column_number;
    const GURL source_url;
  };
~~~