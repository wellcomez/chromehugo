
## struct ServiceWorkerRunningInfo
 A struct containing information about a running service worker.

### ServiceWorkerRunningInfo

ServiceWorkerRunningInfo::ServiceWorkerRunningInfo
~~~cpp
ServiceWorkerRunningInfo(const GURL& script_url,
                           const GURL& scope,
                           const blink::StorageKey& key,
                           int64_t render_process_id,
                           const blink::ServiceWorkerToken& token)
~~~

### ServiceWorkerRunningInfo

ServiceWorkerRunningInfo::ServiceWorkerRunningInfo
~~~cpp
ServiceWorkerRunningInfo(ServiceWorkerRunningInfo&& other) noexcept
~~~

### operator=

ServiceWorkerRunningInfo::operator=
~~~cpp
ServiceWorkerRunningInfo& operator=(
      ServiceWorkerRunningInfo&& other) noexcept;
~~~

### ~ServiceWorkerRunningInfo

ServiceWorkerRunningInfo::~ServiceWorkerRunningInfo
~~~cpp
~ServiceWorkerRunningInfo()
~~~
