
## struct MediaStreamRequest
 Represents a request for media streams (audio/video).

 TODO(vrk,justinlin,wjia): Figure out a way to share this code cleanly between
 vanilla WebRTC, Tab Capture, and Pepper Video Capture. Right now there is
 Tab-only stuff and Pepper-only stuff being passed around to all clients,
 which is icky.

### MediaStreamRequest

MediaStreamRequest::MediaStreamRequest
~~~cpp
MediaStreamRequest(int render_process_id,
                     int render_frame_id,
                     int page_request_id,
                     const GURL& security_origin,
                     bool user_gesture,
                     blink::MediaStreamRequestType request_type,
                     const std::string& requested_audio_device_id,
                     const std::string& requested_video_device_id,
                     blink::mojom::MediaStreamType audio_type,
                     blink::mojom::MediaStreamType video_type,
                     bool disable_local_echo,
                     bool request_pan_tilt_zoom_permission)
~~~

### MediaStreamRequest

MediaStreamRequest::MediaStreamRequest
~~~cpp
MediaStreamRequest(const MediaStreamRequest& other)
~~~

### ~MediaStreamRequest

MediaStreamRequest::~MediaStreamRequest
~~~cpp
~MediaStreamRequest()
~~~

## class MediaStreamUI
 Interface used by the content layer to notify chrome about changes in the
 state of a media stream. Instances of this class are passed to content layer
 when MediaStream access is approved using MediaResponseCallback.

### ~MediaStreamUI

~MediaStreamUI
~~~cpp
virtual ~MediaStreamUI() = default;
~~~

### OnStarted

OnStarted
~~~cpp
virtual gfx::NativeViewId OnStarted(
      base::RepeatingClosure stop,
      SourceCallback source,
      const std::string& label,
      std::vector<DesktopMediaID> screen_capture_ids,
      StateChangeCallback state_change) = 0;
~~~
 Called when MediaStream capturing is started. Chrome layer can call |stop|
 to stop the stream, or |source| to change the source of the stream, or
 |state_change| to pause/unpause the stream.

 |stop| is a callback that, once invoked, will stop the stream.

 Stopping a stream is irreversible, so only the first invocation
 will have an effect. |stop| is defined as RepeatingClosure so as
 to allow its duplication upstream, thereby enabling multiple
 potential sources for the stop invocation. (For example, allow
 multiple UX elements that would stop the capture.)
 Returns the platform-dependent window ID for the UI, or 0
 if not applicable.

### OnDeviceStoppedForSourceChange

OnDeviceStoppedForSourceChange
~~~cpp
virtual void OnDeviceStoppedForSourceChange(
      const std::string& label,
      const DesktopMediaID& old_media_id,
      const DesktopMediaID& new_media_id) = 0;
~~~
 Called when the device is stopped because desktop capture identified by
 |label| source is about to be changed from |old_media_id| to
 |new_media_id|. Note that the switch is not necessarily completed.

### OnDeviceStopped

OnDeviceStopped
~~~cpp
virtual void OnDeviceStopped(const std::string& label,
                               const DesktopMediaID& media_id) = 0;
~~~

### OnRegionCaptureRectChanged

OnRegionCaptureRectChanged
~~~cpp
virtual void OnRegionCaptureRectChanged(
      const absl::optional<gfx::Rect>& region_capture_rect) {}
~~~
 Called when Region Capture starts/stops, or when the cropped area changes.

 * A non-empty rect indicates the region which was cropped-to.

 * An empty rect indicates that Region Capture was employed,
   but the cropped-to region ended up having zero pixels.

 * Nullopt indicates that cropping stopped.

### SetFocus

SetFocus
~~~cpp
virtual void SetFocus(const DesktopMediaID& media_id,
                        bool focus,
                        bool is_from_microtask,
                        bool is_from_timer) {}
~~~
 Focuses the display surface represented by |media_id|.


 |is_from_microtask| and |is_from_timer| are used to distinguish:
 a. Explicit calls from the Web-application.

 b. Implicit calls resulting from the focusability-window-closing microtask.

 c. The browser-side timer.

 This distinction is reflected by UMA.

### ~MediaStreamUI

~MediaStreamUI
~~~cpp
virtual ~MediaStreamUI() = default;
~~~

### OnStarted

OnStarted
~~~cpp
virtual gfx::NativeViewId OnStarted(
      base::RepeatingClosure stop,
      SourceCallback source,
      const std::string& label,
      std::vector<DesktopMediaID> screen_capture_ids,
      StateChangeCallback state_change) = 0;
~~~
 Called when MediaStream capturing is started. Chrome layer can call |stop|
 to stop the stream, or |source| to change the source of the stream, or
 |state_change| to pause/unpause the stream.

 |stop| is a callback that, once invoked, will stop the stream.

 Stopping a stream is irreversible, so only the first invocation
 will have an effect. |stop| is defined as RepeatingClosure so as
 to allow its duplication upstream, thereby enabling multiple
 potential sources for the stop invocation. (For example, allow
 multiple UX elements that would stop the capture.)
 Returns the platform-dependent window ID for the UI, or 0
 if not applicable.

### OnDeviceStoppedForSourceChange

OnDeviceStoppedForSourceChange
~~~cpp
virtual void OnDeviceStoppedForSourceChange(
      const std::string& label,
      const DesktopMediaID& old_media_id,
      const DesktopMediaID& new_media_id) = 0;
~~~
 Called when the device is stopped because desktop capture identified by
 |label| source is about to be changed from |old_media_id| to
 |new_media_id|. Note that the switch is not necessarily completed.

### OnDeviceStopped

OnDeviceStopped
~~~cpp
virtual void OnDeviceStopped(const std::string& label,
                               const DesktopMediaID& media_id) = 0;
~~~

### OnRegionCaptureRectChanged

OnRegionCaptureRectChanged
~~~cpp
virtual void OnRegionCaptureRectChanged(
      const absl::optional<gfx::Rect>& region_capture_rect) {}
~~~
 Called when Region Capture starts/stops, or when the cropped area changes.

 * A non-empty rect indicates the region which was cropped-to.

 * An empty rect indicates that Region Capture was employed,
   but the cropped-to region ended up having zero pixels.

 * Nullopt indicates that cropping stopped.
