
## struct SSLStatus
 Collects the SSL information for this NavigationEntry.

### SSLStatus

SSLStatus::SSLStatus
~~~cpp
SSLStatus(const net::SSLInfo& ssl_info)
~~~

### SSLStatus

SSLStatus::SSLStatus
~~~cpp
SSLStatus(const SSLStatus& other)
~~~

### operator=

SSLStatus::operator=
~~~cpp
operator=(SSLStatus other)
~~~

### ~SSLStatus

SSLStatus::~SSLStatus
~~~cpp
~SSLStatus()
~~~
