### EnableIPCLogging

EnableIPCLogging
~~~cpp
CONTENT_EXPORT void EnableIPCLogging(bool enable);
~~~
 Enable or disable IPC logging for the browser, all processes
 derived from ChildProcess (plugin etc), and all
 renderers.

