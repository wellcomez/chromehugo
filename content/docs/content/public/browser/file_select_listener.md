
## class FileSelectListener
 Callback interface to receive results of RunFileChooser() and
 EnumerateDirectory() of WebContentsDelegate.

### FileSelected

FileSelected
~~~cpp
virtual void FileSelected(
      std::vector<blink::mojom::FileChooserFileInfoPtr> files,
      const base::FilePath& base_dir,
      blink::mojom::FileChooserParams::Mode mode) = 0;
~~~
 This function should be called if file selection succeeds.

 |files| - A list of selected files.

 |base_dir| - This has non-empty directory path if |mode| argument is
              kUploadFolder and kUploadFolder is supported by the
              WebContentsDelegate instance. It represents enumeration root
              directory.

              This is an empty FilePath otherwise.

 |mode| - kUploadFolder for WebContentsDelegate::EnumerateDirectory(), and
          |params.mode| for WebContentsDelegate::RunFileChooser().

### FileSelectionCanceled

FileSelectionCanceled
~~~cpp
virtual void FileSelectionCanceled() = 0;
~~~
 This function should be called if a user cancels a file selection
 dialog, or we open no file selection dialog for some reason.

### ~FileSelectListener

~FileSelectListener
~~~cpp
virtual ~FileSelectListener() = default;
~~~

### FileSelected

FileSelected
~~~cpp
virtual void FileSelected(
      std::vector<blink::mojom::FileChooserFileInfoPtr> files,
      const base::FilePath& base_dir,
      blink::mojom::FileChooserParams::Mode mode) = 0;
~~~
 This function should be called if file selection succeeds.

 |files| - A list of selected files.

 |base_dir| - This has non-empty directory path if |mode| argument is
              kUploadFolder and kUploadFolder is supported by the
              WebContentsDelegate instance. It represents enumeration root
              directory.

              This is an empty FilePath otherwise.

 |mode| - kUploadFolder for WebContentsDelegate::EnumerateDirectory(), and
          |params.mode| for WebContentsDelegate::RunFileChooser().

### FileSelectionCanceled

FileSelectionCanceled
~~~cpp
virtual void FileSelectionCanceled() = 0;
~~~
 This function should be called if a user cancels a file selection
 dialog, or we open no file selection dialog for some reason.

### ~FileSelectListener

~FileSelectListener
~~~cpp
virtual ~FileSelectListener() = default;
~~~
