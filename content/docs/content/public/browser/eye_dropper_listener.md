
## class EyeDropperListener
 Callback interface to receive the color selection from the eye dropper.

### ~EyeDropperListener

~EyeDropperListener
~~~cpp
virtual ~EyeDropperListener() = default;
~~~

### ColorSelected

ColorSelected
~~~cpp
virtual void ColorSelected(SkColor color) = 0;
~~~

### ColorSelectionCanceled

ColorSelectionCanceled
~~~cpp
virtual void ColorSelectionCanceled() = 0;
~~~

### ~EyeDropperListener

~EyeDropperListener
~~~cpp
virtual ~EyeDropperListener() = default;
~~~

### ColorSelected

ColorSelected
~~~cpp
virtual void ColorSelected(SkColor color) = 0;
~~~

### ColorSelectionCanceled

ColorSelectionCanceled
~~~cpp
virtual void ColorSelectionCanceled() = 0;
~~~
