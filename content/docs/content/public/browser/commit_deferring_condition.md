
## class CommitDeferringCondition
 Base class allowing clients to defer an activation or a navigation that's
 ready to commit. See commit_deferring_condition_runner.h for more details.

### CommitDeferringCondition

CommitDeferringCondition
~~~cpp
CommitDeferringCondition() = delete;
~~~

### CommitDeferringCondition

CommitDeferringCondition::CommitDeferringCondition
~~~cpp
CommitDeferringCondition(NavigationHandle& navigation_handle)
~~~

### ~CommitDeferringCondition

CommitDeferringCondition::~CommitDeferringCondition
~~~cpp
~CommitDeferringCondition()
~~~

### WillCommitNavigation

CommitDeferringCondition::WillCommitNavigation
~~~cpp
virtual Result WillCommitNavigation(base::OnceClosure resume) = 0;
~~~
 Override to check if the navigation should be allowed to commit or it
 should be deferred. If this method returns true, this condition is
 already satisfied and the navigation should be allowed to commit. If it
 returns false, the condition will call |resume| asynchronously to
 indicate completion.

### GetNavigationHandle

GetNavigationHandle
~~~cpp
NavigationHandle& GetNavigationHandle() const { return *navigation_handle_; }
~~~
