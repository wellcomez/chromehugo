
## class BrowserChildProcessObserver
 An observer API implemented by classes which are interested in browser child
 process events. Note that render processes cannot be observed through this
 interface; use RenderProcessHostObserver instead.

### BrowserChildProcessHostDisconnected

BrowserChildProcessHostDisconnected
~~~cpp
virtual void BrowserChildProcessHostDisconnected(
      const ChildProcessData& data) {}
~~~
 Called after a ChildProcessHost is disconnected from the child process.

### BrowserChildProcessCrashed

BrowserChildProcessCrashed
~~~cpp
virtual void BrowserChildProcessCrashed(
      const ChildProcessData& data,
      const ChildProcessTerminationInfo& info) {}
~~~
 Called when a child process disappears unexpectedly as a result of a crash.

### BrowserChildProcessKilled

BrowserChildProcessKilled
~~~cpp
virtual void BrowserChildProcessKilled(
      const ChildProcessData& data,
      const ChildProcessTerminationInfo& info) {}
~~~
 Called when a child process disappears unexpectedly as a result of being
 killed.

### BrowserChildProcessLaunchFailed

BrowserChildProcessLaunchFailed
~~~cpp
virtual void BrowserChildProcessLaunchFailed(
      const ChildProcessData& data,
      const ChildProcessTerminationInfo& info) {}
~~~
 Called when a child process never launches successfully. In this case,
 info.status will be TERMINATION_STATUS_LAUNCH_FAILED and info.exit_code
 will contain a platform specific launch failure error code.

### BrowserChildProcessExitedNormally

BrowserChildProcessExitedNormally
~~~cpp
virtual void BrowserChildProcessExitedNormally(
      const ChildProcessData& data,
      const ChildProcessTerminationInfo& info) {}
~~~
 Called when a child process exits without crashing or being killed.

### Add

BrowserChildProcessObserver::Add
~~~cpp
static void Add(BrowserChildProcessObserver* observer);
~~~

### Remove

BrowserChildProcessObserver::Remove
~~~cpp
static void Remove(BrowserChildProcessObserver* observer);
~~~
