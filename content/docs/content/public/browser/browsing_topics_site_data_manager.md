
## class BrowsingTopicsSiteDataManager
 Interface for storing and accessing the browsing topics site data used to
 calculate web-visible topics.

### ~BrowsingTopicsSiteDataManager

~BrowsingTopicsSiteDataManager
~~~cpp
virtual ~BrowsingTopicsSiteDataManager() = default;
~~~

### ExpireDataBefore

BrowsingTopicsSiteDataManager::ExpireDataBefore
~~~cpp
virtual void ExpireDataBefore(base::Time time) = 0;
~~~
 Expire all data before the given time.

### ClearContextDomain

BrowsingTopicsSiteDataManager::ClearContextDomain
~~~cpp
virtual void ClearContextDomain(
      const browsing_topics::HashedDomain& hashed_context_domain) = 0;
~~~
 Clear per-context-domain data.

### GetBrowsingTopicsApiUsage

BrowsingTopicsSiteDataManager::GetBrowsingTopicsApiUsage
~~~cpp
virtual void GetBrowsingTopicsApiUsage(
      base::Time begin_time,
      base::Time end_time,
      GetBrowsingTopicsApiUsageCallback callback) = 0;
~~~
 Get all browsing topics `ApiUsageContext` with its `last_usage_time` within
 [`begin_time`, `end_time`). Note that it's possible for a usage to occur
 within the specified time range, and a more recent usage has renewed its
 `last_usage_time`, so that the corresponding context is not retrieved in
 this query. In practice, this method will be called with
 `end_time` being very close to the current time, so the amount of missed
 data should be negligible. This query also deletes all data with
 last_usage_time (non-inclusive) less than `begin_time`.

### OnBrowsingTopicsApiUsed

BrowsingTopicsSiteDataManager::OnBrowsingTopicsApiUsed
~~~cpp
virtual void OnBrowsingTopicsApiUsed(
      const browsing_topics::HashedHost& hashed_main_frame_host,
      const base::flat_set<browsing_topics::HashedDomain>&
          hashed_context_domains,
      base::Time time) = 0;
~~~
 Persist the browsing topics api usage context to storage. Called when the
 usage is detected in a context on a page.
