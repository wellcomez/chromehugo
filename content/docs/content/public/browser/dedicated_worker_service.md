### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(Observer* observer) = 0;
~~~
 Adds/removes an observer.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(Observer* observer) = 0;
~~~

### EnumerateDedicatedWorkers

EnumerateDedicatedWorkers
~~~cpp
virtual void EnumerateDedicatedWorkers(Observer* observer) = 0;
~~~
 Invokes OnWorkerCreated() on |observer| for all existing dedicated workers.


 This function must be invoked in conjunction with AddObserver(). It is
 meant to be used by an observer that dynamically subscribes to the
 DedicatedWorkerService while some workers are already running. It avoids
 receiving a OnBeforeWorkerDestroyed() event without having received the
 corresponding OnWorkerCreated() event.

## class DedicatedWorkerService
 An interface that allows to subscribe to the lifetime of dedicated workers.

 The service is owned by the StoragePartition and lives on the UI thread.

### OnBeforeWorkerDestroyed

DedicatedWorkerService::OnBeforeWorkerDestroyed
~~~cpp
virtual void OnBeforeWorkerDestroyed(
        const blink::DedicatedWorkerToken& worker_token,
        GlobalRenderFrameHostId ancestor_render_frame_host_id) = 0;
~~~

### OnFinalResponseURLDetermined

DedicatedWorkerService::OnFinalResponseURLDetermined
~~~cpp
virtual void OnFinalResponseURLDetermined(
        const blink::DedicatedWorkerToken& worker_token,
        const GURL& url) = 0;
~~~
 Called when the final response URL (the URL after redirects) was
 determined when fetching the worker's script.
