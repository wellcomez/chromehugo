
## class XrInstallHelper
 Interface class to provide the opportunity for runtimes to ensure that any
 necessary installation steps that need to occur from within the browser
 process are kicked off. This is acquired via the |XrInstallHelperFactory|.

 Generally, these steps are specific per runtime, so likely this should be
 implemented for each runtime that has browser-specific installation steps.

 This should be implemented by embedders.

### XrInstallHelper

XrInstallHelper
~~~cpp
XrInstallHelper(const XrInstallHelper&) = delete;
~~~

### operator=

XrInstallHelper::operator=
~~~cpp
XrInstallHelper& operator=(const XrInstallHelper&) = delete;
~~~

### EnsureInstalled

XrInstallHelper::EnsureInstalled
~~~cpp
virtual void EnsureInstalled(
      int render_process_id,
      int render_frame_id,
      base::OnceCallback<void(bool installed)> install_callback) = 0;
~~~
 Triggers checks and appropriate installation requests for any runtime
 dependencies that need to be installed from the browser process.

 render_process_id and render_frame_id are passed in case any tab specific
 UI needs to be shown.

 The callback should be guaranteed to run in the event that that the object
 is destroyed, and should return whether or not the runtime was able to be
 successfully installed (or verified to already be installed).
