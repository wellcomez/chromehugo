
## class FederatedIdentityApiPermissionContextDelegate
 Delegate interface for the FedCM implementation to query whether the FedCM
 API is enabled in Site Settings.

### FederatedIdentityApiPermissionContextDelegate

FederatedIdentityApiPermissionContextDelegate
~~~cpp
FederatedIdentityApiPermissionContextDelegate() = default;
~~~

### ~FederatedIdentityApiPermissionContextDelegate

~FederatedIdentityApiPermissionContextDelegate
~~~cpp
virtual ~FederatedIdentityApiPermissionContextDelegate() = default;
~~~

### GetApiPermissionStatus

FederatedIdentityApiPermissionContextDelegate::GetApiPermissionStatus
~~~cpp
virtual PermissionStatus GetApiPermissionStatus(
      const url::Origin& relying_party_embedder) = 0;
~~~
 Returns the status of the FedCM API for the passed-in
 |relying_party_embedder|.

### RecordDismissAndEmbargo

FederatedIdentityApiPermissionContextDelegate::RecordDismissAndEmbargo
~~~cpp
virtual void RecordDismissAndEmbargo(
      const url::Origin& relying_party_embedder) = 0;
~~~
 Records that the FedCM prompt was explicitly dismissed and places the
 permission under embargo for the passed-in |relying_party_embedder|.

### RemoveEmbargoAndResetCounts

FederatedIdentityApiPermissionContextDelegate::RemoveEmbargoAndResetCounts
~~~cpp
virtual void RemoveEmbargoAndResetCounts(
      const url::Origin& relying_party_embedder) = 0;
~~~
 Clears any existing embargo status for the FEDERATED_IDENTITY_API
 permission for the passed-in |relying_party_embedder|. Clears the dismiss
 and ignore counts.

### ShouldCompleteRequestImmediately

FederatedIdentityApiPermissionContextDelegate::ShouldCompleteRequestImmediately
~~~cpp
virtual bool ShouldCompleteRequestImmediately() const;
~~~
 This function is so we can avoid the delay in tests. It does not really
 belong on this delegate but we don't have a better one and it seems
 wasteful to add one just for this one testing function.
