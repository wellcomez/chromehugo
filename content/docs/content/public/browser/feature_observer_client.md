
## class FeatureObserverClient

### OnStartUsing

FeatureObserverClient::OnStartUsing
~~~cpp
virtual void OnStartUsing(GlobalRenderFrameHostId id,
                            blink::mojom::ObservedFeatureType feature_type) = 0;
~~~
 These functions are invoked after the number of features of |feature_type|
 used by a frame switches between zero to non-zero. There is no guarantee
 that the frame identified by |render_process_id| and |render_frame_id|
 still exists when this is called.

### OnStopUsing

FeatureObserverClient::OnStopUsing
~~~cpp
virtual void OnStopUsing(GlobalRenderFrameHostId id,
                           blink::mojom::ObservedFeatureType feature_type) = 0;
~~~
