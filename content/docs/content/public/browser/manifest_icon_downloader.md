
## class ManifestIconDownloader
 Helper class which downloads the icon located at a specified URL. If the
 icon file contains multiple icons then it attempts to pick the one closest in
 size bigger than or equal to ideal_icon_size_in_px, taking into account the
 density of the device. If a bigger icon is chosen then, the icon is scaled
 down to be equal to ideal_icon_size_in_px. Smaller icons will be chosen down
 to the value specified by |minimum_icon_size_in_px|.

### ManifestIconDownloader

ManifestIconDownloader
~~~cpp
ManifestIconDownloader() = delete;
~~~

### ManifestIconDownloader

ManifestIconDownloader
~~~cpp
ManifestIconDownloader(const ManifestIconDownloader&) = delete;
~~~

### operator=

ManifestIconDownloader::operator=
~~~cpp
ManifestIconDownloader& operator=(const ManifestIconDownloader&) = delete;
~~~

### ~ManifestIconDownloader

~ManifestIconDownloader
~~~cpp
~ManifestIconDownloader() = delete;
~~~

### Download

ManifestIconDownloader::Download
~~~cpp
static bool Download(
      content::WebContents* web_contents,
      const GURL& icon_url,
      int ideal_icon_size_in_px,
      int minimum_icon_size_in_px,
      int maximum_icon_size_in_px,
      IconFetchCallback callback,
      bool square_only = true,
      const GlobalRenderFrameHostId& initiator_frame_routing_id =
          GlobalRenderFrameHostId());
~~~
 Returns whether the download has started.

 It will return false if the current context or information do not allow to
 download the image.

 |global_frame_routing_id| specifies the frame in which to initiate the
 download.

### ScaleIcon

ManifestIconDownloader::ScaleIcon
~~~cpp
static void ScaleIcon(int ideal_icon_width_in_px,
                        int ideal_icon_height_in_px,
                        const SkBitmap& bitmap,
                        IconFetchCallback callback);
~~~

### FindClosestBitmapIndex

ManifestIconDownloader::FindClosestBitmapIndex
~~~cpp
static int FindClosestBitmapIndex(int ideal_icon_size_in_px,
                                    int minimum_icon_size_in_px,
                                    bool square_only,
                                    const std::vector<SkBitmap>& bitmaps);
~~~
