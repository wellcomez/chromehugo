
## class BrowserURLHandler
 We handle some special browser-level URLs (like "about:version")
 before they're handed to a renderer.  This lets us do the URL handling
 on the browser side (which has access to more information than the
 renderers do) as well as sidestep the risk of exposing data to
 random web pages (because from the resource loader's perspective, these
 URL schemes don't exist).

 BrowserURLHandler manages the list of all special URLs and manages
 dispatching the URL handling to registered handlers.

### null_handler

BrowserURLHandler::null_handler
~~~cpp
static URLHandler null_handler();
~~~
 Returns the null handler for use with |AddHandlerPair()|.

### GetInstance

BrowserURLHandler::GetInstance
~~~cpp
static BrowserURLHandler* GetInstance();
~~~
 Returns the singleton instance.

### RewriteURLIfNecessary

BrowserURLHandler::RewriteURLIfNecessary
~~~cpp
virtual void RewriteURLIfNecessary(GURL* url,
                                     BrowserContext* browser_context) = 0;
~~~
 RewriteURLIfNecessary gives all registered URLHandlers a shot at processing
 the given URL, and modifies it in place.

### GetPossibleRewrites

BrowserURLHandler::GetPossibleRewrites
~~~cpp
virtual std::vector<GURL> GetPossibleRewrites(
      const GURL& url,
      BrowserContext* browser_context) = 0;
~~~
 Returns the list of possible rewrites, in order of priority (i.e., index 0
 is the rewrite that would be used in RewriteURLIfNecessary()). Note that
 this only allows for one rewrite per registered URLHandler (and each gets a
 fresh copy of |url|), so it is not necessarily the complete set of all
 possible rewrites.

### AddHandlerPair

BrowserURLHandler::AddHandlerPair
~~~cpp
virtual void AddHandlerPair(URLHandler handler,
                              URLHandler reverse_handler) = 0;
~~~
 Add the specified handler pair to the list of URL handlers.


 Note that normally, the reverse handler is only used if the modified URL is
 not modified, e.g., by adding a hash fragment. To support this behavior,
 register the forward and reverse handlers separately, each with a
 null_handler() for the opposite direction.
