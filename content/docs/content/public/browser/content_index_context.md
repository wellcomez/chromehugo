
## class ContentIndexContext
 Owned by the Storage Partition. This is used by the ContentIndexProvider to
 query auxiliary data for its entries from the right source.

### ContentIndexContext

ContentIndexContext
~~~cpp
ContentIndexContext() = default;
~~~

### ContentIndexContext

ContentIndexContext
~~~cpp
ContentIndexContext(const ContentIndexContext&) = delete;
~~~

### operator=

ContentIndexContext::operator=
~~~cpp
ContentIndexContext& operator=(const ContentIndexContext&) = delete;
~~~

### ~ContentIndexContext

~ContentIndexContext
~~~cpp
virtual ~ContentIndexContext() = default;
~~~

### GetIcons

ContentIndexContext::GetIcons
~~~cpp
virtual void GetIcons(int64_t service_worker_registration_id,
                        const std::string& description_id,
                        GetIconsCallback callback) = 0;
~~~
 Returns all available icons for the entry identified by
 |service_worker_registration_id| and |description_id|.

 The number of icons and the sizes are chosen by the ContentIndexProvider.

 Must be called on the UI thread. |callback| must be invoked on the UI
 the UI thread.

### GetAllEntries

ContentIndexContext::GetAllEntries
~~~cpp
virtual void GetAllEntries(GetAllEntriesCallback callback) = 0;
~~~
 Must be called on the UI thread.

### GetEntry

ContentIndexContext::GetEntry
~~~cpp
virtual void GetEntry(int64_t service_worker_registration_id,
                        const std::string& description_id,
                        GetEntryCallback callback) = 0;
~~~
 Must be called on the UI thread.

### OnUserDeletedItem

ContentIndexContext::OnUserDeletedItem
~~~cpp
virtual void OnUserDeletedItem(int64_t service_worker_registration_id,
                                 const url::Origin& origin,
                                 const std::string& description_id) = 0;
~~~
 Called when a user deleted an item. Must be called on the UI thread.
