### WeakDocumentPtr::WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
inline WeakDocumentPtr::WeakDocumentPtr() = default;
~~~
 [chromium-style] requires these be out of line, but they are small enough to
 inline the defaults.

### WeakDocumentPtr::WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
inline WeakDocumentPtr::WeakDocumentPtr(WeakDocumentPtr&&) = default;
~~~

### WeakDocumentPtr::operator=

WeakDocumentPtr::operator=
~~~cpp
inline WeakDocumentPtr& WeakDocumentPtr::operator=(WeakDocumentPtr&&) = default;
~~~

### WeakDocumentPtr::WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
inline WeakDocumentPtr::WeakDocumentPtr(const WeakDocumentPtr&) = default;
~~~

### WeakDocumentPtr::operator=

WeakDocumentPtr::operator=
~~~cpp
inline WeakDocumentPtr& WeakDocumentPtr::operator=(const WeakDocumentPtr&) =
    default;
~~~

### WeakDocumentPtr::~WeakDocumentPtr

WeakDocumentPtr::~WeakDocumentPtr
~~~cpp
inline WeakDocumentPtr::~WeakDocumentPtr() = default;
~~~

## class WeakDocumentPtr
 Weakly refers to a document.


 This is invalidated at the same time as DocumentUserData. It
 becomes null whenever the RenderFrameHost is deleted or navigates to a
 different document. See also document_user_data.h.


 Note that though this is implemented as a base::WeakPtr<RenderFrameHost>,
 it is different from an ordinary weak pointer to the RenderFrameHost.


 docs/render_document.md will make these equivalent in the future.


 Treat this like you would a base::WeakPtr, because that's essentially what it
 is.

### WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
WeakDocumentPtr();
~~~

### WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
WeakDocumentPtr(WeakDocumentPtr&&);
~~~
 Copyable and movable.

###  operator=


~~~cpp
WeakDocumentPtr& operator=(WeakDocumentPtr&&);
~~~
### WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
WeakDocumentPtr(const WeakDocumentPtr&);
~~~

###  operator=


~~~cpp
WeakDocumentPtr& operator=(const WeakDocumentPtr&);
~~~
### ~WeakDocumentPtr

WeakDocumentPtr::~WeakDocumentPtr
~~~cpp
~WeakDocumentPtr();
~~~

### AsRenderFrameHostIfValid

AsRenderFrameHostIfValid
~~~cpp
RenderFrameHost* AsRenderFrameHostIfValid() const {
    return weak_document_.get();
  }
~~~
 Callers must handle this returning null, in case the frame has been deleted
 or a cross-document navigation has committed in the same RenderFrameHost.

### WeakDocumentPtr

WeakDocumentPtr::WeakDocumentPtr
~~~cpp
explicit WeakDocumentPtr(base::WeakPtr<RenderFrameHost> weak_rfh);
~~~

### weak_document_



~~~cpp

base::WeakPtr<RenderFrameHost> weak_document_;

~~~

 Created from a factory scoped to document, rather than RenderFrameHost,
 lifetime.

### AsRenderFrameHostIfValid

AsRenderFrameHostIfValid
~~~cpp
RenderFrameHost* AsRenderFrameHostIfValid() const {
    return weak_document_.get();
  }
~~~
 Callers must handle this returning null, in case the frame has been deleted
 or a cross-document navigation has committed in the same RenderFrameHost.

###  operator=


~~~cpp
WeakDocumentPtr& operator=(WeakDocumentPtr&&);
~~~
###  operator=


~~~cpp
WeakDocumentPtr& operator=(const WeakDocumentPtr&);
~~~
### weak_document_



~~~cpp

base::WeakPtr<RenderFrameHost> weak_document_;

~~~

 Created from a factory scoped to document, rather than RenderFrameHost,
 lifetime.
