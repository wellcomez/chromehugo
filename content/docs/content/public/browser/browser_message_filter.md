### BrowserMessageFilter

BrowserMessageFilter
~~~cpp
BrowserMessageFilter(uint32_t message_class_to_filter)
~~~

### BrowserMessageFilter

BrowserMessageFilter
~~~cpp
BrowserMessageFilter(const uint32_t* message_classes_to_filter,
                       size_t num_message_classes_to_filter)
~~~

### OnFilterAdded

OnFilterAdded
~~~cpp
virtual void OnFilterAdded(IPC::Channel* channel) {}
~~~
 These match the corresponding IPC::MessageFilter methods and are always
 called on the IO thread.

### OnFilterRemoved

OnFilterRemoved
~~~cpp
virtual void OnFilterRemoved() {}
~~~

### OnChannelClosing

OnChannelClosing
~~~cpp
virtual void OnChannelClosing() {}
~~~

### OnChannelError

OnChannelError
~~~cpp
virtual void OnChannelError() {}
~~~

### OnChannelConnected

OnChannelConnected
~~~cpp
virtual void OnChannelConnected(int32_t peer_pid) {}
~~~

### OnDestruct

OnDestruct
~~~cpp
virtual void OnDestruct() const;
~~~
 Called when the message filter is about to be deleted.  This gives
 derived classes the option of controlling which thread they're deleted
 on etc.

### Send

Send
~~~cpp
bool Send(IPC::Message* message) override;
~~~
 IPC::Sender implementation.  Can be called on any thread.  Can't send sync
 messages (since we don't want to block the browser on any other process).

### OverrideThreadForMessage

OverrideThreadForMessage
~~~cpp
virtual void OverrideThreadForMessage(
      const IPC::Message& message,
      BrowserThread::ID* thread) {}
~~~
 If you want the given message to be dispatched to your OnMessageReceived on
 a different thread, there are two options, either
 OverrideThreadForMessage or OverrideTaskRunnerForMessage.

 If neither is overriden, the message will be dispatched on the IO thread.

 If you want the message to be dispatched on a particular well-known
 browser thread, change |thread| to the id of the target thread
### OverrideTaskRunnerForMessage

OverrideTaskRunnerForMessage
~~~cpp
virtual scoped_refptr<base::SequencedTaskRunner> OverrideTaskRunnerForMessage(
      const IPC::Message& message);
~~~
 If you want the message to be dispatched via the SequencedWorkerPool,
 return a non-null task runner which will target tasks accordingly.

 Note: To target the UI thread, please use OverrideThreadForMessage
 since that has extra checks to avoid deadlocks.

### OnMessageReceived

OnMessageReceived
~~~cpp
virtual bool OnMessageReceived(const IPC::Message& message) = 0;
~~~
 Override this to receive messages.

 Your function will normally be called on the IO thread.  However, if your
 OverrideXForMessage modifies the thread used to dispatch the message,
 your function will be called on the requested thread.

### AddAssociatedInterface

AddAssociatedInterface
~~~cpp
void AddAssociatedInterface(
      const std::string& name,
      const IPC::ChannelProxy::GenericAssociatedInterfaceFactory& factory,
      const base::OnceClosure filter_removed_callback);
~~~
 Adds an associated interface factory to this filter. Must be called before
 RegisterAssociatedInterfaces().


 |filter_removed_callback| is called on the IO thread when this filter is
 removed.

### PeerHandle

PeerHandle
~~~cpp
base::ProcessHandle PeerHandle();
~~~
 Can be called on any thread, after OnChannelConnected is called.

### peer_pid

peer_pid
~~~cpp
base::ProcessId peer_pid() const { return peer_process_.Pid(); }
~~~
 Can be called on any thread, after OnChannelConnected is called.

### set_peer_process_for_testing

set_peer_process_for_testing
~~~cpp
void set_peer_process_for_testing(base::Process peer_process) {
    peer_process_ = std::move(peer_process);
  }
~~~

### ShutdownForBadMessage

ShutdownForBadMessage
~~~cpp
virtual void ShutdownForBadMessage();
~~~
 Called by bad_message.h helpers if a message couldn't be deserialized. This
 kills the renderer.  Can be called on any thread.  This doesn't log the
 error details to UMA, so use the bad_message.h for your module instead.

### message_classes_to_filter

message_classes_to_filter
~~~cpp
const std::vector<uint32_t>& message_classes_to_filter() const {
    return message_classes_to_filter_;
  }
~~~

### GetFilter

GetFilter
~~~cpp
IPC::MessageFilter* GetFilter();
~~~
 These are private because the only classes that need access to them are
 made friends above. These are only guaranteed to be valid to call on
 creation. After that this class could outlive the filter and new interface
 registrations could race with incoming requests.

### RegisterAssociatedInterfaces

RegisterAssociatedInterfaces
~~~cpp
void RegisterAssociatedInterfaces(IPC::ChannelProxy* proxy);
~~~

