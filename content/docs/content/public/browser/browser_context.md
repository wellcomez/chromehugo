
## class BrowserContext : public
 This class holds the context needed for a browsing session.

 It lives on the UI thread. All these methods must only be called on the UI
 thread.

### ~BrowserContext

BrowserContext : public::~BrowserContext
~~~cpp
~BrowserContext() override
~~~

### GetDownloadManager

BrowserContext : public::GetDownloadManager
~~~cpp
DownloadManager* GetDownloadManager();
~~~

### GetMountPoints

BrowserContext : public::GetMountPoints
~~~cpp
storage::ExternalMountPoints* GetMountPoints();
~~~
 Returns BrowserContext specific external mount points. It may return
 nullptr if the context doesn't have any BrowserContext specific external
 mount points. Currently, non-nullptr value is returned only on ChromeOS.

### GetBrowsingDataRemover

BrowserContext : public::GetBrowsingDataRemover
~~~cpp
BrowsingDataRemover* GetBrowsingDataRemover();
~~~
 Returns a BrowsingDataRemover that can schedule data deletion tasks
 for this |context|.

### GetPermissionController

BrowserContext : public::GetPermissionController
~~~cpp
PermissionController* GetPermissionController();
~~~
 Returns the PermissionController associated with this context. There's
 always a PermissionController instance for each BrowserContext.

### GetStoragePartition

BrowserContext : public::GetStoragePartition
~~~cpp
StoragePartition* GetStoragePartition(SiteInstance* site_instance,
                                        bool can_create = true);
~~~
 Returns a StoragePartition for the given SiteInstance. By default this will
 create a new StoragePartition if it doesn't exist, unless |can_create| is
 false.

### GetStoragePartition

BrowserContext : public::GetStoragePartition
~~~cpp
StoragePartition* GetStoragePartition(
      const StoragePartitionConfig& storage_partition_config,
      bool can_create = true);
~~~
 Returns a StoragePartition for the given StoragePartitionConfig. By
 default this will create a new StoragePartition if it doesn't exist,
 unless |can_create| is false.

### GetStoragePartitionForUrl

BrowserContext : public::GetStoragePartitionForUrl
~~~cpp
StoragePartition* GetStoragePartitionForUrl(const GURL& url,
                                              bool can_create = true);
~~~
 Deprecated. Do not add new callers. Use the SiteInstance or
 StoragePartitionConfig methods above instead.

 Returns a StoragePartition for the given URL. By default this will
 create a new StoragePartition if it doesn't exist, unless |can_create| is
 false.

### ForEachLoadedStoragePartition

BrowserContext : public::ForEachLoadedStoragePartition
~~~cpp
void ForEachLoadedStoragePartition(StoragePartitionCallback callback);
~~~

### GetLoadedStoragePartitionCount

BrowserContext : public::GetLoadedStoragePartitionCount
~~~cpp
size_t GetLoadedStoragePartitionCount();
~~~
 Returns the number of loaded StoragePartitions that exist for `this`
 BrowserContext.

 See |ForEachLoadedStoragePartition| for details about loaded
 StoragePartitions.

### AsyncObliterateStoragePartition

BrowserContext : public::AsyncObliterateStoragePartition
~~~cpp
void AsyncObliterateStoragePartition(const std::string& partition_domain,
                                       base::OnceClosure on_gc_required,
                                       base::OnceClosure done_callback);
~~~
 Starts an asynchronous best-effort attempt to delete all on-disk storage
 related to |partition_domain| and synchronously invokes |done_callback|
 once all deletable on-disk storage is deleted. |on_gc_required| will be
 invoked if |partition_domain| corresponds to any StoragePartitions that
 are loaded and can't safely be deleted. In this case the caller should
 attempt to delete the StoragePartition again at next browser launch.

### GarbageCollectStoragePartitions

BrowserContext : public::GarbageCollectStoragePartitions
~~~cpp
void GarbageCollectStoragePartitions(
      std::unordered_set<base::FilePath> active_paths,
      base::OnceClosure done);
~~~
 Examines all on-disk StoragePartitions and removes any entries that are
 not loaded or listed in `active_paths`.


 The `done` closure is executed on the calling thread when garbage
 collection is complete.

### GetDefaultStoragePartition

BrowserContext : public::GetDefaultStoragePartition
~~~cpp
StoragePartition* GetDefaultStoragePartition();
~~~

### CreateMemoryBackedBlob

BrowserContext : public::CreateMemoryBackedBlob
~~~cpp
void CreateMemoryBackedBlob(base::span<const uint8_t> data,
                              const std::string& content_type,
                              BlobCallback callback);
~~~
 This method should be called on UI thread and calls back on UI thread
 as well. Note that retrieving a blob ptr out of BlobHandle can only be
 done on IO. |callback| returns a nullptr on failure.

### GetBlobStorageContext

BrowserContext : public::GetBlobStorageContext
~~~cpp
BlobContextGetter GetBlobStorageContext();
~~~
 Get a BlobStorageContext getter that needs to run on IO thread.

### GetBlobRemote

BrowserContext : public::GetBlobRemote
~~~cpp
mojo::PendingRemote<blink::mojom::Blob> GetBlobRemote(
      const std::string& uuid);
~~~
 Returns a mojom::mojo::PendingRemote<blink::mojom::Blob> for a specific
 blob. If no blob exists with the given UUID, the
 mojo::PendingRemote<blink::mojom::Blob> pipe will close. This method should
 be called on the UI thread.

 TODO(mek): Blob UUIDs should be entirely internal to the blob system, so
 eliminate this method in favor of just passing around the
 mojo::PendingRemote<blink::mojom::Blob> directly.

### DeliverPushMessage

BrowserContext : public::DeliverPushMessage
~~~cpp
void DeliverPushMessage(
      const GURL& origin,
      int64_t service_worker_registration_id,
      const std::string& message_id,
      absl::optional<std::string> payload,
      base::OnceCallback<void(blink::mojom::PushEventStatus)> callback);
~~~
 Delivers a push message with |data| to the Service Worker identified by
 |origin| and |service_worker_registration_id|.

### FirePushSubscriptionChangeEvent

BrowserContext : public::FirePushSubscriptionChangeEvent
~~~cpp
void FirePushSubscriptionChangeEvent(
      const GURL& origin,
      int64_t service_worker_registration_id,
      blink::mojom::PushSubscriptionPtr new_subscription,
      blink::mojom::PushSubscriptionPtr old_subscription,
      base::OnceCallback<void(blink::mojom::PushEventStatus)> callback);
~~~
 Fires a push subscription change event to the Service Worker identified by
 |origin| and |service_worker_registration_id| with |new_subscription| and
 |old_subscription| as event information.

### NotifyWillBeDestroyed

BrowserContext : public::NotifyWillBeDestroyed
~~~cpp
void NotifyWillBeDestroyed();
~~~

### EnsureResourceContextInitialized

BrowserContext : public::EnsureResourceContextInitialized
~~~cpp
void EnsureResourceContextInitialized();
~~~
 Ensures that the corresponding ResourceContext is initialized. Normally the
 BrowserContext initializs the corresponding getters when its objects are
 created, but if the embedder wants to pass the ResourceContext to another
 thread before they use BrowserContext, they should call this to make sure
 that the ResourceContext is ready.

### SaveSessionState

BrowserContext : public::SaveSessionState
~~~cpp
void SaveSessionState();
~~~
 Tells the HTML5 objects on this context to persist their session state
 across the next restart.

### SetDownloadManagerForTesting

BrowserContext : public::SetDownloadManagerForTesting
~~~cpp
void SetDownloadManagerForTesting(
      std::unique_ptr<DownloadManager> download_manager);
~~~

### SetPermissionControllerForTesting

BrowserContext : public::SetPermissionControllerForTesting
~~~cpp
void SetPermissionControllerForTesting(
      std::unique_ptr<PermissionController> permission_controller);
~~~

### GetSharedCorsOriginAccessList

BrowserContext : public::GetSharedCorsOriginAccessList
~~~cpp
SharedCorsOriginAccessList* GetSharedCorsOriginAccessList();
~~~
 The list of CORS exemptions.  This list needs to be 1) replicated when
 creating or re-creating new network::mojom::NetworkContexts (see
 network::mojom::NetworkContextParams::cors_origin_access_list) and 2)
 consulted by CORS-aware factories (e.g. passed when constructing
 FileURLLoaderFactory).

### ShutdownStoragePartitions

BrowserContext : public::ShutdownStoragePartitions
~~~cpp
void ShutdownStoragePartitions();
~~~
 Shuts down the storage partitions associated to this browser context.

 This must be called before the browser context is actually destroyed
 and before a clean-up task for its corresponding IO thread residents (e.g.

 ResourceContext) is posted, so that the classes that hung on
 StoragePartition can have time to do necessary cleanups on IO thread.

### ShutdownStarted

BrowserContext : public::ShutdownStarted
~~~cpp
bool ShutdownStarted();
~~~
 Returns true if shutdown has been initiated via a
 NotifyWillBeDestroyed() call. This is a signal that the object will be
 destroyed soon and no new references to this object should be created.

### UniqueId

BrowserContext : public::UniqueId
~~~cpp
virtual const std::string& UniqueId();
~~~
 Returns a unique string associated with this browser context.

### GetVideoDecodePerfHistory

BrowserContext : public::GetVideoDecodePerfHistory
~~~cpp
media::VideoDecodePerfHistory* GetVideoDecodePerfHistory();
~~~
 Gets media service for storing/retrieving video decoding performance stats.

 Exposed here rather than StoragePartition because all SiteInstances should
 have similar decode performance and stats are not exposed to the web
 directly, so privacy is not compromised.

### GetWebrtcVideoPerfHistory

BrowserContext : public::GetWebrtcVideoPerfHistory
~~~cpp
media::WebrtcVideoPerfHistory* GetWebrtcVideoPerfHistory();
~~~
 Gets media service for storing/retrieving WebRTC video performance stats.

 Exposed here rather than StoragePartition because all SiteInstances should
 have similar encode/decode performance and stats are not exposed to the web
 directly, so privacy is not compromised.

### GetLearningSession

BrowserContext : public::GetLearningSession
~~~cpp
virtual media::learning::LearningSession* GetLearningSession();
~~~
 Returns a LearningSession associated with |this|. Used as the central
 source from which to retrieve LearningTaskControllers for media machine
 learning.

 Exposed here rather than StoragePartition because learnings will cover
 general media trends rather than SiteInstance specific behavior. The
 learnings are not exposed to the web.

### RetrieveInProgressDownloadManager

BrowserContext : public::RetrieveInProgressDownloadManager
~~~cpp
virtual std::unique_ptr<download::InProgressDownloadManager>
  RetrieveInProgressDownloadManager();
~~~
 Retrieves the InProgressDownloadManager associated with this object if
 available
### CreateRandomMediaDeviceIDSalt

BrowserContext : public::CreateRandomMediaDeviceIDSalt
~~~cpp
static std::string CreateRandomMediaDeviceIDSalt();
~~~
 Utility function useful for embedders. Only needs to be called if
 1) The embedder needs to use a new salt, and
 2) The embedder saves its salt across restarts.

### WriteIntoTrace

BrowserContext : public::WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedProto<TraceProto> context) const;
~~~
 Write a representation of this object into tracing proto.

 rvalue ensure that the this method can be called without having access
 to the declaration of ChromeBrowserContext proto.

### CreateZoomLevelDelegate

BrowserContext : public::CreateZoomLevelDelegate
~~~cpp
virtual std::unique_ptr<ZoomLevelDelegate> CreateZoomLevelDelegate(
      const base::FilePath& partition_path) = 0;
~~~

 The //content embedder can override the methods below to change or extend
 how the //content layer interacts with a BrowserContext.


 All the methods below should be virtual.  Most of the methods should be
 pure (i.e. `= 0`) although it may make sense to provide a default
 implementation for some of the methods.


 TODO(https://crbug.com/1179776): Migrate method declarations from this
 section into a separate BrowserContextDelegate class.

 Creates a delegate to initialize a HostZoomMap and persist its information.

 This is called during creation of each StoragePartition.

### GetPath

BrowserContext : public::GetPath
~~~cpp
virtual base::FilePath GetPath() = 0;
~~~
 Returns the path of the directory where this context's data is stored.

### IsOffTheRecord

BrowserContext : public::IsOffTheRecord
~~~cpp
virtual bool IsOffTheRecord() = 0;
~~~
 Return whether this context is off the record. Default is false.

 Note that for Chrome this does not imply Incognito as Guest sessions are
 also off the record.

### GetResourceContext

BrowserContext : public::GetResourceContext
~~~cpp
virtual ResourceContext* GetResourceContext() = 0;
~~~
 Returns the resource context.

### GetDownloadManagerDelegate

BrowserContext : public::GetDownloadManagerDelegate
~~~cpp
virtual DownloadManagerDelegate* GetDownloadManagerDelegate() = 0;
~~~
 Returns the DownloadManagerDelegate for this context. This will be called
 once per context. The embedder owns the delegate and is responsible for
 ensuring that it outlives DownloadManager. Note in particular that it is
 unsafe to destroy the delegate in the destructor of a subclass of
 BrowserContext, since it needs to be alive in ~BrowserContext.

 It's valid to return nullptr.

### GetGuestManager

BrowserContext : public::GetGuestManager
~~~cpp
virtual BrowserPluginGuestManager* GetGuestManager() = 0;
~~~
 Returns the guest manager for this context.

### GetSpecialStoragePolicy

BrowserContext : public::GetSpecialStoragePolicy
~~~cpp
virtual storage::SpecialStoragePolicy* GetSpecialStoragePolicy() = 0;
~~~
 Returns a special storage policy implementation, or nullptr.

### GetPlatformNotificationService

BrowserContext : public::GetPlatformNotificationService
~~~cpp
virtual PlatformNotificationService* GetPlatformNotificationService() = 0;
~~~
 Returns the platform notification service, capable of displaying Web
 Notifications to the user. The embedder can return a nullptr if they don't
 support this functionality. Must be called on the UI thread.

### GetPushMessagingService

BrowserContext : public::GetPushMessagingService
~~~cpp
virtual PushMessagingService* GetPushMessagingService() = 0;
~~~
 Returns a push messaging service. The embedder owns the service, and is
 responsible for ensuring that it outlives RenderProcessHost. It's valid to
 return nullptr.

### GetStorageNotificationService

BrowserContext : public::GetStorageNotificationService
~~~cpp
virtual StorageNotificationService* GetStorageNotificationService() = 0;
~~~
 Returns a storage notification service associated with that context,
 nullptr otherwise. In the case that nullptr is returned, QuotaManager
 and the rest of the storage layer will have no connection to the Chrome
 layer for UI purposes.

### GetSSLHostStateDelegate

BrowserContext : public::GetSSLHostStateDelegate
~~~cpp
virtual SSLHostStateDelegate* GetSSLHostStateDelegate() = 0;
~~~
 Returns the SSL host state decisions for this context. The context may
 return nullptr, implementing the default exception storage strategy.

### GetPermissionControllerDelegate

BrowserContext : public::GetPermissionControllerDelegate
~~~cpp
virtual PermissionControllerDelegate* GetPermissionControllerDelegate() = 0;
~~~
 Returns the PermissionControllerDelegate associated with this context if
 any, nullptr otherwise.


 Note: if you want to check a permission status, you probably need
 BrowserContext::GetPermissionController() instead.

### GetReduceAcceptLanguageControllerDelegate

BrowserContext : public::GetReduceAcceptLanguageControllerDelegate
~~~cpp
virtual ReduceAcceptLanguageControllerDelegate*
  GetReduceAcceptLanguageControllerDelegate() = 0;
~~~
 Returns the ReduceAcceptLanguageControllerDelegate associated with that
 context if any, nullptr otherwise.

### GetClientHintsControllerDelegate

BrowserContext : public::GetClientHintsControllerDelegate
~~~cpp
virtual ClientHintsControllerDelegate* GetClientHintsControllerDelegate() = 0;
~~~
 Returns the ClientHintsControllerDelegate associated with that context if
 any, nullptr otherwise.

### GetBackgroundFetchDelegate

BrowserContext : public::GetBackgroundFetchDelegate
~~~cpp
virtual BackgroundFetchDelegate* GetBackgroundFetchDelegate() = 0;
~~~
 Returns the BackgroundFetchDelegate associated with that context if any,
 nullptr otherwise.

### GetBackgroundSyncController

BrowserContext : public::GetBackgroundSyncController
~~~cpp
virtual BackgroundSyncController* GetBackgroundSyncController() = 0;
~~~
 Returns the BackgroundSyncController associated with that context if any,
 nullptr otherwise.

### GetBrowsingDataRemoverDelegate

BrowserContext : public::GetBrowsingDataRemoverDelegate
~~~cpp
virtual BrowsingDataRemoverDelegate* GetBrowsingDataRemoverDelegate() = 0;
~~~
 Returns the BrowsingDataRemoverDelegate for this context. This will be
 called once per context. It's valid to return nullptr.

### GetMediaDeviceIDSalt

BrowserContext : public::GetMediaDeviceIDSalt
~~~cpp
virtual std::string GetMediaDeviceIDSalt();
~~~
 Returns a random salt string that is used for creating media device IDs.

 Default implementation uses the BrowserContext's UniqueId.

### GetFileSystemAccessPermissionContext

BrowserContext : public::GetFileSystemAccessPermissionContext
~~~cpp
virtual FileSystemAccessPermissionContext*
  GetFileSystemAccessPermissionContext();
~~~
 Returns the FileSystemAccessPermissionContext associated with this context
 if any, nullptr otherwise.

### GetContentIndexProvider

BrowserContext : public::GetContentIndexProvider
~~~cpp
virtual ContentIndexProvider* GetContentIndexProvider();
~~~
 Returns the ContentIndexProvider associated with that context if any,
 nullptr otherwise.

### CanUseDiskWhenOffTheRecord

BrowserContext : public::CanUseDiskWhenOffTheRecord
~~~cpp
virtual bool CanUseDiskWhenOffTheRecord();
~~~
 Returns true iff the sandboxed file system implementation should be disk
 backed, even if this browser context is off the record. By default this
 returns false, an embedded could override this to return true if for
 example the off-the-record browser context is stored in a in-memory file
 system anyway, in which case using the disk backed sandboxed file system
 API implementation can give some benefits over the in-memory
 implementation.

### GetVariationsClient

BrowserContext : public::GetVariationsClient
~~~cpp
virtual variations::VariationsClient* GetVariationsClient();
~~~
 Returns the VariationsClient associated with the context if any, or
 nullptr if there isn't one.

### CreateVideoDecodePerfHistory

BrowserContext : public::CreateVideoDecodePerfHistory
~~~cpp
virtual std::unique_ptr<media::VideoDecodePerfHistory>
  CreateVideoDecodePerfHistory();
~~~
 Creates the media service for storing/retrieving video decoding performance
 stats.  Exposed here rather than StoragePartition because all SiteInstances
 should have similar decode performance and stats are not exposed to the web
 directly, so privacy is not compromised.

### GetFederatedIdentityApiPermissionContext

BrowserContext : public::GetFederatedIdentityApiPermissionContext
~~~cpp
virtual FederatedIdentityApiPermissionContextDelegate*
  GetFederatedIdentityApiPermissionContext();
~~~
 Gets the permission context for determining whether the FedCM API is
 enabled in site settings.

### GetFederatedIdentityAutoReauthnPermissionContext

BrowserContext : public::GetFederatedIdentityAutoReauthnPermissionContext
~~~cpp
virtual FederatedIdentityAutoReauthnPermissionContextDelegate*
  GetFederatedIdentityAutoReauthnPermissionContext();
~~~
 Gets the permission context for determining whether the FedCM API's auto
 re-authentication feature is enabled in site settings.

### GetFederatedIdentityPermissionContext

BrowserContext : public::GetFederatedIdentityPermissionContext
~~~cpp
virtual FederatedIdentityPermissionContextDelegate*
  GetFederatedIdentityPermissionContext();
~~~
 Gets the permission context for allowing session management capabilities
 between an identity provider and a relying party if one exists, or
 nullptr otherwise.

### GetKAnonymityServiceDelegate

BrowserContext : public::GetKAnonymityServiceDelegate
~~~cpp
virtual KAnonymityServiceDelegate* GetKAnonymityServiceDelegate();
~~~
 Gets the KAnonymityServiceDelegate if supported. Returns nullptr if
 unavailable.

### GetOriginTrialsControllerDelegate

BrowserContext : public::GetOriginTrialsControllerDelegate
~~~cpp
virtual OriginTrialsControllerDelegate* GetOriginTrialsControllerDelegate();
~~~
 Returns the OriginTrialsControllerDelegate associated with the context if
 any, nullptr otherwise.

### impl

impl
~~~cpp
BrowserContextImpl* impl() { return impl_.get(); }
~~~

### impl

impl
~~~cpp
const BrowserContextImpl* impl() const { return impl_.get(); }
~~~
