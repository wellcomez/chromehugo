
## class WebRtcLog

### WebRtcLog

WebRtcLog
~~~cpp
WebRtcLog(const WebRtcLog&) = delete;
~~~

### operator=

WebRtcLog::operator=
~~~cpp
WebRtcLog& operator=(const WebRtcLog&) = delete;
~~~

### SetLogMessageCallback

WebRtcLog::SetLogMessageCallback
~~~cpp
static void SetLogMessageCallback(
      int render_process_id,
      base::RepeatingCallback<void(const std::string&)> callback);
~~~
 When set, |callback| receives log messages regarding, for example, media
 devices (webcams, mics, etc) that were initially requested in the render
 process associated with the RenderProcessHost with |render_process_id|.

### ClearLogMessageCallback

WebRtcLog::ClearLogMessageCallback
~~~cpp
static void ClearLogMessageCallback(int render_process_id);
~~~
