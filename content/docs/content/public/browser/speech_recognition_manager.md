
## class SpeechRecognitionManager
 The SpeechRecognitionManager (SRM) is a singleton class that handles SR
 functionalities within Chrome. Everyone that needs to perform SR should
 interface exclusively with the SRM, receiving events through the callback
 interface SpeechRecognitionEventListener.

 Since many different sources can use SR in different times (some overlapping
 is allowed while waiting for results), the SRM has the further responsibility
 of handling separately and reliably (taking into account also call sequences
 that might not make sense, e.g., two subsequent AbortSession calls).

 In this sense a session, within the SRM, models the ongoing evolution of a
 SR request from the viewpoint of the end-user, abstracting all the concrete
 operations that must be carried out, that will be handled by inner classes.

### enum { kSessionIDInvalid = 0 }

~~~cpp
enum { kSessionIDInvalid = 0 }
~~~
### GetInstance

SpeechRecognitionManager::GetInstance
~~~cpp
static CONTENT_EXPORT SpeechRecognitionManager* GetInstance();
~~~
 Returns the singleton instance.

### CONTENT_EXPORT



~~~cpp

static void CONTENT_EXPORT

~~~

 Singleton manager setter useful for tests.

### SetManagerForTesting

SpeechRecognitionManager::SetManagerForTesting
~~~cpp
SetManagerForTesting(
      SpeechRecognitionManager* manager);
~~~

### CreateSession

CreateSession
~~~cpp
virtual int CreateSession(const SpeechRecognitionSessionConfig& config) = 0;
~~~
 Creates a new recognition session.

### StartSession

StartSession
~~~cpp
virtual void StartSession(int session_id) = 0;
~~~
 Starts/restarts recognition for an existing session, after performing a
 preliminary check on the delegate (CheckRecognitionIsAllowed).

### AbortSession

AbortSession
~~~cpp
virtual void AbortSession(int session_id) = 0;
~~~
 Aborts recognition for an existing session, without providing any result.

### AbortAllSessionsForRenderFrame

AbortAllSessionsForRenderFrame
~~~cpp
virtual void AbortAllSessionsForRenderFrame(int render_process_id,
                                              int render_frame_id) = 0;
~~~
 Aborts all sessions for a given RenderFrame, without providing any result.

### StopAudioCaptureForSession

StopAudioCaptureForSession
~~~cpp
virtual void StopAudioCaptureForSession(int session_id) = 0;
~~~
 Stops audio capture for an existing session. The audio captured before the
 call will be processed, possibly ending up with a result.

### GetSessionConfig

GetSessionConfig
~~~cpp
virtual const SpeechRecognitionSessionConfig& GetSessionConfig(
      int session_id) = 0;
~~~
 Retrieves the configuration of a session, as provided by the caller
 upon CreateSession.

### GetSessionContext

GetSessionContext
~~~cpp
virtual SpeechRecognitionSessionContext GetSessionContext(int session_id) = 0;
~~~
 Retrieves the context associated to a session.

### ~SpeechRecognitionManager

~SpeechRecognitionManager
~~~cpp
virtual ~SpeechRecognitionManager() {}
~~~

### field error



~~~cpp

static SpeechRecognitionManager* manager_for_tests_;

~~~


### CreateSession

CreateSession
~~~cpp
virtual int CreateSession(const SpeechRecognitionSessionConfig& config) = 0;
~~~
 Creates a new recognition session.

### StartSession

StartSession
~~~cpp
virtual void StartSession(int session_id) = 0;
~~~
 Starts/restarts recognition for an existing session, after performing a
 preliminary check on the delegate (CheckRecognitionIsAllowed).

### AbortSession

AbortSession
~~~cpp
virtual void AbortSession(int session_id) = 0;
~~~
 Aborts recognition for an existing session, without providing any result.

### AbortAllSessionsForRenderFrame

AbortAllSessionsForRenderFrame
~~~cpp
virtual void AbortAllSessionsForRenderFrame(int render_process_id,
                                              int render_frame_id) = 0;
~~~
 Aborts all sessions for a given RenderFrame, without providing any result.

### StopAudioCaptureForSession

StopAudioCaptureForSession
~~~cpp
virtual void StopAudioCaptureForSession(int session_id) = 0;
~~~
 Stops audio capture for an existing session. The audio captured before the
 call will be processed, possibly ending up with a result.

### GetSessionConfig

GetSessionConfig
~~~cpp
virtual const SpeechRecognitionSessionConfig& GetSessionConfig(
      int session_id) = 0;
~~~
 Retrieves the configuration of a session, as provided by the caller
 upon CreateSession.

### GetSessionContext

GetSessionContext
~~~cpp
virtual SpeechRecognitionSessionContext GetSessionContext(int session_id) = 0;
~~~
 Retrieves the context associated to a session.

### ~SpeechRecognitionManager

~SpeechRecognitionManager
~~~cpp
virtual ~SpeechRecognitionManager() {}
~~~

### enum { kSessionIDInvalid = 0 }

~~~cpp
enum { kSessionIDInvalid = 0 };
~~~
### GetInstance

SpeechRecognitionManager::GetInstance
~~~cpp
static CONTENT_EXPORT SpeechRecognitionManager* GetInstance();
~~~
 Returns the singleton instance.

### CONTENT_EXPORT



~~~cpp

static void CONTENT_EXPORT

~~~

 Singleton manager setter useful for tests.

### field error



~~~cpp

static SpeechRecognitionManager* manager_for_tests_;

~~~

