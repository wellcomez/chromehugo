
## class PermissionController
    : public
 This class allows the content layer to manipulate permissions. It's behavior
 is defined by the embedder via PermissionControllerDelegate implementation.

 TODO(crbug.com/1312212): Use url::Origin instead of GURL.

### ~PermissionController

~PermissionController
~~~cpp
~PermissionController() override {}
~~~

### GetPermissionStatusForWorker

PermissionController
    : public::GetPermissionStatusForWorker
~~~cpp
virtual blink::mojom::PermissionStatus GetPermissionStatusForWorker(
      blink::PermissionType permission,
      RenderProcessHost* render_process_host,
      const url::Origin& worker_origin) = 0;
~~~
 Returns the status of the given |permission| for a worker on
 |worker_origin| running in the renderer corresponding to
 |render_process_host|.

### GetPermissionStatusForCurrentDocument

PermissionController
    : public::GetPermissionStatusForCurrentDocument
~~~cpp
virtual blink::mojom::PermissionStatus GetPermissionStatusForCurrentDocument(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host) = 0;
~~~
 Returns the permission status for the current document in the given
 RenderFrameHost. This API takes into account the lifecycle state of a given
 document (i.e. whether it's in back-forward cache or being prerendered) in
 addition to its origin.

### GetPermissionResultForCurrentDocument

PermissionController
    : public::GetPermissionResultForCurrentDocument
~~~cpp
virtual PermissionResult GetPermissionResultForCurrentDocument(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host) = 0;
~~~
 The method does the same as `GetPermissionStatusForCurrentDocument` but
 additionally returns a source or reason for the permission status.

### GetPermissionResultForOriginWithoutContext

PermissionController
    : public::GetPermissionResultForOriginWithoutContext
~~~cpp
virtual PermissionResult GetPermissionResultForOriginWithoutContext(
      blink::PermissionType permission,
      const url::Origin& origin) = 0;
~~~
 Returns the permission status for a given origin. Use this API only if
 there is no document and it is not a ServiceWorker.

### GetPermissionStatusForOriginWithoutContext

PermissionController
    : public::GetPermissionStatusForOriginWithoutContext
~~~cpp
virtual blink::mojom::PermissionStatus
  GetPermissionStatusForOriginWithoutContext(
      blink::PermissionType permission,
      const url::Origin& requesting_origin,
      const url::Origin& embedding_origin) = 0;
~~~
 The method does the same as `GetPermissionResultForOriginWithoutContext`
 but it can be used for `PermissionType` that are keyed on a combination of
 requesting and embedding origins, e.g., Notifications.

### RequestPermissionFromCurrentDocument

PermissionController
    : public::RequestPermissionFromCurrentDocument
~~~cpp
virtual void RequestPermissionFromCurrentDocument(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host,
      bool user_gesture,
      base::OnceCallback<void(blink::mojom::PermissionStatus)> callback) = 0;
~~~
 Requests the permission from the current document in the given
 RenderFrameHost. This API takes into account the lifecycle state of a given
 document (i.e. whether it's in back-forward cache or being prerendered) in
 addition to its origin.

### RequestPermissionsFromCurrentDocument

PermissionController
    : public::RequestPermissionsFromCurrentDocument
~~~cpp
virtual void RequestPermissionsFromCurrentDocument(
      const std::vector<blink::PermissionType>& permission,
      RenderFrameHost* render_frame_host,
      bool user_gesture,
      base::OnceCallback<void(
          const std::vector<blink::mojom::PermissionStatus>&)> callback) = 0;
~~~
 Requests permissions from the current document in the given
 RenderFrameHost. This API takes into account the lifecycle state of a given
 document (i.e. whether it's in back-forward cache or being prerendered) in
 addition to its origin.

 WARNING: Permission requests order is not guaranteed.

 TODO(crbug.com/1363094): Migrate to `std::set`.

### ResetPermission

PermissionController
    : public::ResetPermission
~~~cpp
virtual void ResetPermission(blink::PermissionType permission,
                               const url::Origin& origin) = 0;
~~~
 Sets the permission back to its default for the `origin`.

### SubscribePermissionStatusChange

PermissionController
    : public::SubscribePermissionStatusChange
~~~cpp
virtual SubscriptionId SubscribePermissionStatusChange(
      blink::PermissionType permission,
      RenderProcessHost* render_process_host,
      const url::Origin& requesting_origin,
      const base::RepeatingCallback<void(blink::mojom::PermissionStatus)>&
          callback) = 0;
~~~

### UnsubscribePermissionStatusChange

PermissionController
    : public::UnsubscribePermissionStatusChange
~~~cpp
virtual void UnsubscribePermissionStatusChange(
      SubscriptionId subscription_id) = 0;
~~~

### IsSubscribedToPermissionChangeEvent

PermissionController
    : public::IsSubscribedToPermissionChangeEvent
~~~cpp
virtual bool IsSubscribedToPermissionChangeEvent(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host) = 0;
~~~
 Returns `true` if a document subscribed to
 `PermissionStatus.onchange` listener or `PermissionStatus.AddEventListener`
 with a type `change` was added. Returns `false` otherwise.
