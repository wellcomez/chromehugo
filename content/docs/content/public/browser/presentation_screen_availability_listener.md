
## class PresentationScreenAvailabilityListener
 A listener interface used for receiving updates on screen availability
 associated with a presentation URL from an embedder.

 See also PresentationServiceDelegate.

### GetAvailabilityUrl

PresentationScreenAvailabilityListener::GetAvailabilityUrl
~~~cpp
virtual GURL GetAvailabilityUrl() = 0;
~~~
 Returns the screen availability URL associated with this listener.

 Empty string means this object is listening for screen availability
 for "1-UA" mode, i.e. offscreen tab rendering.

### OnScreenAvailabilityChanged

PresentationScreenAvailabilityListener::OnScreenAvailabilityChanged
~~~cpp
virtual void OnScreenAvailabilityChanged(
      blink::mojom::ScreenAvailability availability) = 0;
~~~
 Called when screen availability for the associated Presentation URL has
 changed to |availability|.
