
## class AllowServiceWorkerResult

### No

AllowServiceWorkerResult::No
~~~cpp
static AllowServiceWorkerResult No();
~~~

### FromPolicy

AllowServiceWorkerResult::FromPolicy
~~~cpp
static AllowServiceWorkerResult FromPolicy(bool javascript_blocked_by_policy,
                                             bool cookies_blocked_by_policy);
~~~

### 


~~~cpp
operator bool() { return allowed_; }
~~~

### javascript_blocked_by_policy

javascript_blocked_by_policy
~~~cpp
bool javascript_blocked_by_policy() const {
    return javascript_blocked_by_policy_;
  }
~~~

### cookies_blocked_by_policy

cookies_blocked_by_policy
~~~cpp
bool cookies_blocked_by_policy() const { return cookies_blocked_by_policy_; }
~~~

### operator==

AllowServiceWorkerResult::operator==
~~~cpp
bool operator==(const AllowServiceWorkerResult& other) const;
~~~
