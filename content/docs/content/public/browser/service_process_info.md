
## class ServiceProcessInfo
 Information about a running (or very recently running) service process.


 This class is move-only but can be copied by calling the Duplicate() method.

 This is explicitly defined to prevent accidental copying, as the Duplicate()
 operation will call Duplicate() on the underlying base::Process.

### ServiceProcessInfo

ServiceProcessInfo
~~~cpp
ServiceProcessInfo(const ServiceProcessInfo&) = delete;
~~~

### ServiceProcessInfo

ServiceProcessInfo::ServiceProcessInfo
~~~cpp
ServiceProcessInfo(ServiceProcessInfo&&)
~~~

### operator=

ServiceProcessInfo::operator=
~~~cpp
ServiceProcessInfo& operator=(const ServiceProcessInfo&) = delete;
~~~

### operator=

ServiceProcessInfo::operator=
~~~cpp
ServiceProcessInfo& operator=(ServiceProcessInfo&&);
~~~

### ~ServiceProcessInfo

ServiceProcessInfo::~ServiceProcessInfo
~~~cpp
~ServiceProcessInfo()
~~~

### IsService

IsService
~~~cpp
bool IsService() const {
    return service_interface_name_ == Interface::Name_;
  }
~~~

### Duplicate

ServiceProcessInfo::Duplicate
~~~cpp
ServiceProcessInfo Duplicate() const;
~~~
 Duplicates the ServiceProcessInfo since this struct is non-copyable. This
 Duplicates the underlying `process_`.

### service_process_id

service_process_id
~~~cpp
const ServiceProcessId service_process_id() const {
    return service_process_id_;
  }
~~~

### service_interface_name

service_interface_name
~~~cpp
const std::string service_interface_name() const {
    return service_interface_name_;
  }
~~~

### site

site
~~~cpp
const absl::optional<GURL>& site() const { return site_; }
~~~

### GetProcess

GetProcess
~~~cpp
const base::Process& GetProcess() const { return process_; }
~~~
