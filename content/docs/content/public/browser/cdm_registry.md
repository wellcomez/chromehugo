
## class CdmRegistry
 Keeps track of the Content Decryption Modules that are available.

### ~CdmRegistry

~CdmRegistry
~~~cpp
virtual ~CdmRegistry() {}
~~~

### Init

CdmRegistry::Init
~~~cpp
virtual void Init() = 0;
~~~
 Must be called on the instance to finish initialization.

### RegisterCdm

CdmRegistry::RegisterCdm
~~~cpp
virtual void RegisterCdm(const CdmInfo& info) = 0;
~~~
 Registers a CDM with the specified CDM information. The CDM will be
 inserted at the head of the list so that it can override any older
 registrations.

 Note: Since only 1 version of the CDM can be loaded at any given time,
 it is possible that there will be a mismatch between the functionality
 reported and what is actually available, if the reported functionality
 changes between versions. (http://crbug.com/599588)
### SetHardwareSecureCdmStatus

CdmRegistry::SetHardwareSecureCdmStatus
~~~cpp
virtual void SetHardwareSecureCdmStatus(CdmInfo::Status status) = 0;
~~~
 Sets the status for all hardware secure CDMs, e.g. to disable hardware
 secure CDMs.

 TODO(xhwang): Provide a way to disable a specific `key_system`