
## class WebDragDestDelegate
 An optional delegate that listens for drags of bookmark data.

### DragInitialize

DragInitialize
~~~cpp
virtual void DragInitialize(WebContents* contents) = 0;
~~~
 Announces that a drag has started. It's valid that a drag starts, along
 with over/enter/leave/drop notifications without receiving any bookmark
 data.

### OnDragOver

OnDragOver
~~~cpp
virtual void OnDragOver() = 0;
~~~
 Notifications of drag progression.

### OnDragEnter

OnDragEnter
~~~cpp
virtual void OnDragEnter() = 0;
~~~

### OnDrop

OnDrop
~~~cpp
virtual void OnDrop() = 0;
~~~

### OnDragLeave

OnDragLeave
~~~cpp
virtual void OnDragLeave() = 0;
~~~
 This should also clear any state kept about this drag.

### OnReceiveDragData

OnReceiveDragData
~~~cpp
virtual void OnReceiveDragData(const ui::OSExchangeData& data) = 0;
~~~
 Called at the start of every drag to supply the data associated with the
 drag.

### ~WebDragDestDelegate

~WebDragDestDelegate
~~~cpp
virtual ~WebDragDestDelegate() {}
~~~
 USE_AURA
### DragInitialize

DragInitialize
~~~cpp
virtual void DragInitialize(WebContents* contents) = 0;
~~~
 Announces that a drag has started. It's valid that a drag starts, along
 with over/enter/leave/drop notifications without receiving any bookmark
 data.

### OnDragOver

OnDragOver
~~~cpp
virtual void OnDragOver() = 0;
~~~
 Notifications of drag progression.

### OnDragEnter

OnDragEnter
~~~cpp
virtual void OnDragEnter() = 0;
~~~

### OnDrop

OnDrop
~~~cpp
virtual void OnDrop() = 0;
~~~

### OnDragLeave

OnDragLeave
~~~cpp
virtual void OnDragLeave() = 0;
~~~
 This should also clear any state kept about this drag.

### ~WebDragDestDelegate

~WebDragDestDelegate
~~~cpp
virtual ~WebDragDestDelegate() {}
~~~
 USE_AURA