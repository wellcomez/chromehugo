
## struct FileSystemAccessWriteItem
 Represents the state of a FileSystemAccessFileWriter when it is closed,
 containing all the data necessary to do a safe browsing download protection
 check on the data written to disk.

### ~FileSystemAccessWriteItem

FileSystemAccessWriteItem::~FileSystemAccessWriteItem
~~~cpp
~FileSystemAccessWriteItem()
~~~

### FileSystemAccessWriteItem

FileSystemAccessWriteItem
~~~cpp
FileSystemAccessWriteItem(const FileSystemAccessWriteItem&) = delete;
~~~

### operator=

FileSystemAccessWriteItem::operator=
~~~cpp
FileSystemAccessWriteItem& operator=(const FileSystemAccessWriteItem&) =
      delete;
~~~
