
## struct BackgroundFetchResponse
 Contains the response after a background fetch has started.

### BackgroundFetchResponse

BackgroundFetchResponse::BackgroundFetchResponse
~~~cpp
BackgroundFetchResponse(
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers)
~~~

### BackgroundFetchResponse

BackgroundFetchResponse
~~~cpp
BackgroundFetchResponse(const BackgroundFetchResponse&) = delete;
~~~

### operator=

BackgroundFetchResponse::operator=
~~~cpp
BackgroundFetchResponse& operator=(const BackgroundFetchResponse&) = delete;

  ~BackgroundFetchResponse();
~~~

## struct BackgroundFetchResult

### BackgroundFetchResult

BackgroundFetchResult::BackgroundFetchResult
~~~cpp
BackgroundFetchResult(std::unique_ptr<BackgroundFetchResponse> response,
                        base::Time response_time,
                        FailureReason failure_reason)
~~~

### BackgroundFetchResult

BackgroundFetchResult::BackgroundFetchResult
~~~cpp
BackgroundFetchResult(std::unique_ptr<BackgroundFetchResponse> response,
                        base::Time response_time,
                        const base::FilePath& path,
                        absl::optional<storage::BlobDataHandle> blob_handle,
                        uint64_t file_size)
~~~

### BackgroundFetchResult

BackgroundFetchResult
~~~cpp
BackgroundFetchResult(const BackgroundFetchResult&) = delete;
~~~

### operator=

BackgroundFetchResult::operator=
~~~cpp
BackgroundFetchResult& operator=(const BackgroundFetchResult&) = delete;

  ~BackgroundFetchResult();
~~~
