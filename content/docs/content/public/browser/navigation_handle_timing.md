
## struct NavigationHandleTiming
 NavigationHandleTiming contains timing information of loading for navigation
 recorded in NavigationHandle. This is used for UMAs, not exposed to
 JavaScript via Navigation Timing API etc unlike mojom::NavigationTiming. See
 the design doc for details.

 https://docs.google.com/document/d/16oqu9lyPbfgZIjQsRaCfaKE8r1Cdlb3d4GVSdth4AN8/edit?usp=sharing
### NavigationHandleTiming

NavigationHandleTiming::NavigationHandleTiming
~~~cpp
NavigationHandleTiming(const NavigationHandleTiming& timing)
~~~

### operator=

NavigationHandleTiming::operator=
~~~cpp
NavigationHandleTiming& operator=(const NavigationHandleTiming& timing);
~~~
