### operator==

operator==
~~~cpp
inline bool operator==(NavigationThrottle::ThrottleAction lhs,
                       const NavigationThrottle::ThrottleCheckResult& rhs) {
  return lhs == rhs.action();
}
~~~
 Test-only operator== to enable assertions like:
   EXPECT_EQ(NavigationThrottle::PROCEED, throttle->WillProcessResponse())
### operator!=

operator!=
~~~cpp
inline bool operator!=(NavigationThrottle::ThrottleAction lhs,
                       const NavigationThrottle::ThrottleCheckResult& rhs) {
  return lhs != rhs.action();
}
~~~
 Test-only operator!= to enable assertions like:
   EXPECT_NE(NavigationThrottle::PROCEED, throttle->WillProcessResponse())
## class NavigationThrottle::ThrottleCheckResult
 ThrottleCheckResult, the return value for NavigationThrottle decision
 methods, is a ThrottleAction value with an attached net::Error and an
 optional attached error page HTML string.


 ThrottleCheckResult is implicitly convertible from ThrottleAction, allowing
 the following examples to work:

   ThrottleCheckResult WillStartRequest() override {
      // Uses default error for PROCEED (net::OK).

      return PROCEED;
   }

   ThrottleCheckResult WillStartRequest() override {
      // Uses default error for BLOCK_REQUEST (net::ERR_BLOCKED_BY_CLIENT).

      return BLOCK_REQUEST;
   }

   ThrottleCheckResult WillStartRequest() override {
      // Identical to previous example (net::ERR_BLOCKED_BY_CLIENT)
      return {BLOCK_REQUEST};
   }

   ThrottleCheckResult WillStartRequest() override {
      // Uses a custom error code of ERR_FILE_NOT_FOUND.

      return {BLOCK_REQUEST, net::ERR_FILE_NOT_FOUND};
   }

   ThrottleCheckResult WillStartRequest() override {
      // Uses a custom error code of ERR_FILE_NOT_FOUND and an error page
      string.

      return {BLOCK_REQUEST,
              net::ERR_FILE_NOT_FOUND,
              std::string("<html><body>Could not find.</body></html>")};
   }
### ThrottleCheckResult

ThrottleCheckResult::NavigationThrottle::ThrottleCheckResult
~~~cpp
ThrottleCheckResult(ThrottleAction action, net::Error net_error_code)
~~~

### ThrottleCheckResult

ThrottleCheckResult::NavigationThrottle::ThrottleCheckResult
~~~cpp
ThrottleCheckResult(ThrottleAction action,
                        net::Error net_error_code,
                        absl::optional<std::string> error_page_content)
~~~

### ThrottleCheckResult

ThrottleCheckResult::NavigationThrottle::ThrottleCheckResult
~~~cpp
ThrottleCheckResult(const ThrottleCheckResult& other)
~~~

### ~ThrottleCheckResult

ThrottleCheckResult::NavigationThrottle::~ThrottleCheckResult
~~~cpp
~ThrottleCheckResult()
~~~

### action

action
~~~cpp
ThrottleAction action() const { return action_; }
~~~

### net_error_code

net_error_code
~~~cpp
net::Error net_error_code() const { return net_error_code_; }
~~~

### error_page_content

error_page_content
~~~cpp
const absl::optional<std::string>& error_page_content() {
      return error_page_content_;
    }
~~~

## class NavigationThrottle
 A NavigationThrottle tracks and allows interaction with a navigation on the
 UI thread. NavigationThrottles may not be run for some kinds of navigations
 (e.g. same-document navigations, about:blank, activations into the primary
 frame tree like prerendering and back-forward cache, etc.). Content-internal
 code that just wishes to defer a commit, including activations to the
 primary frame tree, should instead use a CommitDeferringCondition.

### ~NavigationThrottle

NavigationThrottle::~NavigationThrottle
~~~cpp
~NavigationThrottle()
~~~

### WillStartRequest

NavigationThrottle::WillStartRequest
~~~cpp
virtual ThrottleCheckResult WillStartRequest();
~~~
 Called when a network request is about to be made for this navigation.


 The implementer is responsible for ensuring that the WebContents this
 throttle is associated with remain alive during the duration of this
 method. Failing to do so will result in use-after-free bugs. Should the
 implementer need to destroy the WebContents, it should return CANCEL,
 CANCEL_AND_IGNORE or DEFER and perform the destruction asynchronously.

### WillRedirectRequest

NavigationThrottle::WillRedirectRequest
~~~cpp
virtual ThrottleCheckResult WillRedirectRequest();
~~~
 Called when a server redirect is received by the navigation.


 The implementer is responsible for ensuring that the WebContents this
 throttle is associated with remain alive during the duration of this
 method. Failing to do so will result in use-after-free bugs. Should the
 implementer need to destroy the WebContents, it should return CANCEL,
 CANCEL_AND_IGNORE or DEFER and perform the destruction asynchronously.

### WillFailRequest

NavigationThrottle::WillFailRequest
~~~cpp
virtual ThrottleCheckResult WillFailRequest();
~~~
 Called when a request will fail.


 The implementer is responsible for ensuring that the WebContents this
 throttle is associated with remain alive during the duration of this
 method. Failing to do so will result in use-after-free bugs. Should the
 implementer need to destroy the WebContents, it should return CANCEL,
 CANCEL_AND_IGNORE or DEFER and perform the destruction asynchronously.

### WillProcessResponse

NavigationThrottle::WillProcessResponse
~~~cpp
virtual ThrottleCheckResult WillProcessResponse();
~~~
 Called when a response's metadata is available.


 For HTTP(S) responses, headers will be available.

 The implementer is responsible for ensuring that the WebContents this
 throttle is associated with remain alive during the duration of this
 method. Failing to do so will result in use-after-free bugs. Should the
 implementer need to destroy the WebContents, it should return CANCEL,
 CANCEL_AND_IGNORE, or BLOCK_RESPONSE and perform the destruction
 asynchronously.

### WillCommitWithoutUrlLoader

NavigationThrottle::WillCommitWithoutUrlLoader
~~~cpp
virtual ThrottleCheckResult WillCommitWithoutUrlLoader();
~~~
 Called when a navigation is about to immediately commit because there's no
 need for a url loader. This includes browser-initiated same-document
 navigations, same-document history navigations, about:blank, about:srcdoc,
 any other empty document scheme, and MHTML subframes.

 Renderer-initiated non-history same-document navigations do NOT go through
 this path, because they are handled synchronously in the renderer and the
 browser process is only notified after the fact.

 BFCache and prerender activation also do NOT go through this path, because
 they are considered already loaded when they are activated.

 In order to get this event, a NavigationThrottle must register itself with
 RegisterNavigationThrottlesForCommitWithoutUrlLoader().

 This event is mutually exclusive with WillStartRequest,
 WillRedirectRequest, and WillProcessResponse. Only WillFailRequest can
 be called after WillCommitWithoutUrlLoader.

 Only PROCEED, DEFER, and CANCEL_AND_IGNORE results are supported at this
 time.

### GetNameForLogging

NavigationThrottle::GetNameForLogging
~~~cpp
virtual const char* GetNameForLogging() = 0;
~~~
 Returns the name of the throttle for logging purposes. It must not return
 nullptr.

### navigation_handle

navigation_handle
~~~cpp
NavigationHandle* navigation_handle() const { return navigation_handle_; }
~~~
 The NavigationHandle that is tracking the information related to this
 navigation.

### set_resume_callback_for_testing

set_resume_callback_for_testing
~~~cpp
void set_resume_callback_for_testing(const base::RepeatingClosure& callback) {
    resume_callback_ = callback;
  }
~~~
 Overrides the default Resume method and replaces it by |callback|. This
 should only be used in tests.

### set_cancel_deferred_navigation_callback_for_testing

set_cancel_deferred_navigation_callback_for_testing
~~~cpp
void set_cancel_deferred_navigation_callback_for_testing(
      const base::RepeatingCallback<void(ThrottleCheckResult)> callback) {
    cancel_deferred_navigation_callback_ = callback;
  }
~~~
 Overrides the default CancelDeferredNavigation method and replaces it by
 |callback|. This should only be used in tests.

### CancelDeferredNavigation

NavigationThrottle::CancelDeferredNavigation
~~~cpp
virtual void CancelDeferredNavigation(ThrottleCheckResult result);
~~~
 Cancels a navigation that was previously deferred by this
 NavigationThrottle. |result|'s action should be equal to either:
  - NavigationThrottle::CANCEL,
  - NavigationThrottle::CANCEL_AND_IGNORE, or
  - NavigationThrottle::BLOCK_REQUEST_AND_COLLAPSE.

 Note: this may lead to the deletion of the NavigationHandle and its
 associated NavigationThrottles, including this one.
