
## class SpeechRecognitionEventListener
 The interface to be implemented by consumers interested in receiving
 speech recognition events.

### OnAudioStart

SpeechRecognitionEventListener::OnAudioStart
~~~cpp
virtual void OnAudioStart(int session_id) = 0;
~~~
 Invoked when the first audio capture is initiated.

### OnEnvironmentEstimationComplete

SpeechRecognitionEventListener::OnEnvironmentEstimationComplete
~~~cpp
virtual void OnEnvironmentEstimationComplete(int session_id) = 0;
~~~
 At the start of recognition, a short amount of audio is recorded to
 estimate the environment/background noise and this callback is issued
 after that is complete. Typically the delegate brings up any speech
 recognition UI once this callback is received.

### OnSoundStart

SpeechRecognitionEventListener::OnSoundStart
~~~cpp
virtual void OnSoundStart(int session_id) = 0;
~~~
 Informs that the endpointer has started detecting sound (possibly speech).

### OnSoundEnd

SpeechRecognitionEventListener::OnSoundEnd
~~~cpp
virtual void OnSoundEnd(int session_id) = 0;
~~~
 Informs that the endpointer has stopped detecting sound (a long silence).

### OnAudioEnd

SpeechRecognitionEventListener::OnAudioEnd
~~~cpp
virtual void OnAudioEnd(int session_id) = 0;
~~~
 Invoked when audio capture stops, either due to the endpoint detecting
 silence, an internal error, or an explicit stop was issued.

### OnRecognitionResults

SpeechRecognitionEventListener::OnRecognitionResults
~~~cpp
virtual void OnRecognitionResults(
      int session_id,
      const std::vector<blink::mojom::SpeechRecognitionResultPtr>& results) = 0;
~~~
 Invoked when a result is retrieved.

### OnRecognitionError

SpeechRecognitionEventListener::OnRecognitionError
~~~cpp
virtual void OnRecognitionError(
      int session_id,
      const blink::mojom::SpeechRecognitionError& error) = 0;
~~~
 Invoked if there was an error while capturing or recognizing audio.

 The recognition has already been cancelled when this call is made and
 no more events will be raised.

### OnAudioLevelsChange

SpeechRecognitionEventListener::OnAudioLevelsChange
~~~cpp
virtual void OnAudioLevelsChange(int session_id,
                                   float volume, float noise_volume) = 0;
~~~
 Informs of a change in the captured audio level, useful if displaying
 a microphone volume indicator while recording.

 The value of |volume| and |noise_volume| is in the [0.0, 1.0] range.

 TODO(janx): Is this necessary? It looks like only x-webkit-speech bubble
 uses it (see crbug.com/247351).

### OnRecognitionEnd

SpeechRecognitionEventListener::OnRecognitionEnd
~~~cpp
virtual void OnRecognitionEnd(int session_id) = 0;
~~~
 This is guaranteed to be the last event raised in the recognition
 process and the |SpeechRecognizer| object can be freed if necessary.
