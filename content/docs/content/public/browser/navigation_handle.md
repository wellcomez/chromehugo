
## class NavigationHandle : public
 A NavigationHandle tracks information related to a single navigation.

 NavigationHandles are provided to several WebContentsObserver methods to
 allow observers to track specific navigations. Observers should clear any
 references to a NavigationHandle at the time of
 WebContentsObserver::DidFinishNavigation, just before the handle is
 destroyed.

### GetNavigationId

NavigationHandle : public::GetNavigationId
~~~cpp
virtual int64_t GetNavigationId() = 0;
~~~
###  Parameters available at navigation start time 
-----------------------------

 These parameters are always available during the navigation. Note that
 some may change during navigation (e.g. due to server redirects).

 Get a unique ID for this navigation.

### GetNextPageUkmSourceId

NavigationHandle : public::GetNextPageUkmSourceId
~~~cpp
virtual ukm::SourceId GetNextPageUkmSourceId() = 0;
~~~
 Get the page UKM ID that will be in use once this navigation fully commits
 (the eventual value of GetRenderFrameHost()->GetPageUkmSourceId()).

### GetURL

NavigationHandle : public::GetURL
~~~cpp
virtual const GURL& GetURL() = 0;
~~~
 The URL the frame is navigating to. This may change during the navigation
 when encountering a server redirect.

 This URL may not be the same as the virtual URL returned from
 WebContents::GetVisibleURL and WebContents::GetLastCommittedURL. For
 example, viewing a page's source navigates to the URL of the page, but the
 virtual URL is prefixed with "view-source:".

 Note: The URL of a NavigationHandle can change over its lifetime.

 e.g. URLs might be rewritten by the renderer before being committed.

### GetStartingSiteInstance

NavigationHandle : public::GetStartingSiteInstance
~~~cpp
virtual SiteInstance* GetStartingSiteInstance() = 0;
~~~
 Returns the SiteInstance where the frame being navigated was at the start
 of the navigation.  If a frame in SiteInstance A navigates a frame in
 SiteInstance B to a URL in SiteInstance C, then this returns B.

### GetSourceSiteInstance

NavigationHandle : public::GetSourceSiteInstance
~~~cpp
virtual SiteInstance* GetSourceSiteInstance() = 0;
~~~
 Returns the SiteInstance of the initiator of the navigation.  If a frame in
 SiteInstance A navigates a frame in SiteInstance B to a URL in SiteInstance
 C, then this returns A.

### IsInMainFrame

NavigationHandle : public::IsInMainFrame
~~~cpp
virtual bool IsInMainFrame() const = 0;
~~~
 Whether the navigation is taking place in a main frame or in a subframe.

 This can also return true for navigations in the root of a non-primary
 page, so consider whether you want to call IsInPrimaryMainFrame() instead.

 See the documentation below for details. The return value remains constant
 over the navigation lifetime.

### IsInPrimaryMainFrame

NavigationHandle : public::IsInPrimaryMainFrame
~~~cpp
virtual bool IsInPrimaryMainFrame() const = 0;
~~~
 Whether the navigation is taking place in the main frame of the primary
 frame tree. With MPArch (crbug.com/1164280), a WebContents may have
 additional frame trees for prerendering pages in addition to the primary
 frame tree (holding the page currently shown to the user). The return
 value remains constant over the navigation lifetime.

 See docs/frame_trees.md for more details.

### IsInOutermostMainFrame

NavigationHandle : public::IsInOutermostMainFrame
~~~cpp
virtual bool IsInOutermostMainFrame() = 0;
~~~
 Whether the navigation is taking place in a main frame which does not have
 an outer document. For example, this will return true for the primary main
 frame and for a prerendered main frame, but false for a <fencedframe>. See
 documentation for `RenderFrameHost::GetParentOrOuterDocument()` for more
 details.

### IsInPrerenderedMainFrame

NavigationHandle : public::IsInPrerenderedMainFrame
~~~cpp
virtual bool IsInPrerenderedMainFrame() const = 0;
~~~
 Prerender2:
 Whether the navigation is taking place in the main frame of the
 prerendered frame tree. Prerender will create separate frame trees to load
 a page in the background, which later then be activated by a separate
 prerender page activation navigation in the primary main frame. This
 returns false for prerender page activation navigations, which should be
 checked by IsPrerenderedPageActivation(). The return value remains
 constant over the navigation lifetime.

### IsPrerenderedPageActivation

NavigationHandle : public::IsPrerenderedPageActivation
~~~cpp
virtual bool IsPrerenderedPageActivation() const = 0;
~~~
 Prerender2:
 Returns true if this navigation will activate a prerendered page. It is
 only meaningful to call this after BeginNavigation().

### IsInFencedFrameTree

NavigationHandle : public::IsInFencedFrameTree
~~~cpp
virtual bool IsInFencedFrameTree() const = 0;
~~~
 FencedFrame:
 Returns true if the navigation is taking place in a frame in a fenced frame
 tree.

### GetNavigatingFrameType

NavigationHandle : public::GetNavigatingFrameType
~~~cpp
virtual FrameType GetNavigatingFrameType() const = 0;
~~~
 Returns the type of the frame in which this navigation is taking place.

### IsRendererInitiated

NavigationHandle : public::IsRendererInitiated
~~~cpp
virtual bool IsRendererInitiated() = 0;
~~~
 Whether the navigation was initiated by the renderer process. Examples of
 renderer-initiated navigations include:
  * <a> link click
  * changing window.location.href
  * redirect via the <meta http-equiv="refresh"> tag
  * using window.history.pushState() or window.history.replaceState()
  * using window.history.forward() or window.history.back()

 This method returns false for browser-initiated navigations, including:
  * any navigation initiated from the omnibox
  * navigations via suggestions in browser UI
  * navigations via browser UI: Ctrl-R, refresh/forward/back/home buttons
  * any other "explicit" URL navigations, e.g. bookmarks
### GetNavigationInitiatorActivationAndAdStatus

NavigationHandle : public::GetNavigationInitiatorActivationAndAdStatus
~~~cpp
virtual blink::mojom::NavigationInitiatorActivationAndAdStatus
  GetNavigationInitiatorActivationAndAdStatus() = 0;
~~~
 The navigation initiator's user activation and ad status.


 TODO(yaoxia): this will be used for recording a page load UKM
 (https://crrev.com/c/4080612).

### IsSameOrigin

NavigationHandle : public::IsSameOrigin
~~~cpp
virtual bool IsSameOrigin() = 0;
~~~
 Whether the previous document in this frame was same-origin with the new
 one created by this navigation.


 |HasCommitted()| must be true before calling this function.


 Note: This doesn't take the initiator of the navigation into consideration.

 For instance, a parent (A) can initiate a navigation in its iframe,
 replacing document (B) by (C). This methods compare (B) with (C).

### GetFrameTreeNodeId

NavigationHandle : public::GetFrameTreeNodeId
~~~cpp
virtual int GetFrameTreeNodeId() = 0;
~~~
 Returns the FrameTreeNode ID for the frame in which the navigation is
 performed. This ID is browser-global and uniquely identifies a frame that
 hosts content. The return value remains constant over the navigation
 lifetime.


 However, because of prerender activations, the RenderFrameHost that this
 navigation is committed into may later transfer to another FrameTreeNode.

 See documentation for RenderFrameHost::GetFrameTreeNodeId() for more
 details.

### GetParentFrame

NavigationHandle : public::GetParentFrame
~~~cpp
virtual RenderFrameHost* GetParentFrame() = 0;
~~~
 Returns the RenderFrameHost for the parent frame, or nullptr if this
 navigation is taking place in the main frame. This value will not change
 during a navigation.

### GetParentFrameOrOuterDocument

NavigationHandle : public::GetParentFrameOrOuterDocument
~~~cpp
virtual RenderFrameHost* GetParentFrameOrOuterDocument() = 0;
~~~
 Returns the document owning the frame this NavigationHandle is located
 in, which will either be a parent (for <iframe>s) or outer document (for
 <fencedframe> and <portal>). See documentation for
 `RenderFrameHost::GetParentOrOuterDocument()` for more details.

### GetWebContents

NavigationHandle : public::GetWebContents
~~~cpp
virtual WebContents* GetWebContents();
~~~
 The WebContents the navigation is taking place in.

### NavigationStart

NavigationHandle : public::NavigationStart
~~~cpp
virtual base::TimeTicks NavigationStart() = 0;
~~~
 The time the navigation started, recorded either in the renderer or in the
 browser process. Corresponds to Navigation Timing API.

### NavigationInputStart

NavigationHandle : public::NavigationInputStart
~~~cpp
virtual base::TimeTicks NavigationInputStart() = 0;
~~~
 The time the input leading to the navigation started. Will not be
 set if unknown.

### GetNavigationHandleTiming

NavigationHandle : public::GetNavigationHandleTiming
~~~cpp
virtual const NavigationHandleTiming& GetNavigationHandleTiming() = 0;
~~~
 The timing information of loading for the navigation.

### WasStartedFromContextMenu

NavigationHandle : public::WasStartedFromContextMenu
~~~cpp
virtual bool WasStartedFromContextMenu() = 0;
~~~
 Whether or not the navigation was started within a context menu.

### GetSearchableFormURL

NavigationHandle : public::GetSearchableFormURL
~~~cpp
virtual const GURL& GetSearchableFormURL() = 0;
~~~
 Returns the URL and encoding of an INPUT field that corresponds to a
 searchable form request.

### GetSearchableFormEncoding

NavigationHandle : public::GetSearchableFormEncoding
~~~cpp
virtual const std::string& GetSearchableFormEncoding() = 0;
~~~

### GetReloadType

NavigationHandle : public::GetReloadType
~~~cpp
virtual ReloadType GetReloadType() = 0;
~~~
 Returns the reload type for this navigation.

### GetRestoreType

NavigationHandle : public::GetRestoreType
~~~cpp
virtual RestoreType GetRestoreType() = 0;
~~~
 Returns the restore type for this navigation. RestoreType::NONE is returned
 if the navigation is not a restore.

### GetBaseURLForDataURL

NavigationHandle : public::GetBaseURLForDataURL
~~~cpp
virtual const GURL& GetBaseURLForDataURL() = 0;
~~~
 Used for specifying a base URL for pages loaded via data URLs.

### IsPost

NavigationHandle : public::IsPost
~~~cpp
virtual bool IsPost() = 0;
~~~
 Whether the navigation is done using HTTP POST method. This may change
 during the navigation (e.g. after encountering a server redirect).


 Note: page and frame navigations can only be done using HTTP POST or HTTP
 GET methods (and using other, scheme-specific protocols for non-http(s) URI
 schemes like data: or file:).  Therefore //content public API exposes only
 |bool IsPost()| as opposed to |const std::string& GetMethod()| method.

### GetReferrer

NavigationHandle : public::GetReferrer
~~~cpp
virtual const blink::mojom::Referrer& GetReferrer() = 0;
~~~
 Returns a sanitized version of the referrer for this request.

### SetReferrer

NavigationHandle : public::SetReferrer
~~~cpp
virtual void SetReferrer(blink::mojom::ReferrerPtr referrer) = 0;
~~~
 Sets the referrer. The referrer may only be set during start and redirect
 phases. If the referer is set in navigation start, it is reset during the
 redirect. In other words, if you need to set a referer that applies to
 redirects, then this must be called during DidRedirectNavigation().

### HasUserGesture

NavigationHandle : public::HasUserGesture
~~~cpp
virtual bool HasUserGesture() = 0;
~~~
 Whether the navigation was initiated by a user gesture. Note that this
 will return false for browser-initiated navigations.

 TODO(clamy): This should return true for browser-initiated navigations.

### GetPageTransition

NavigationHandle : public::GetPageTransition
~~~cpp
virtual ui::PageTransition GetPageTransition() = 0;
~~~
 Returns the page transition type.

### GetNavigationUIData

NavigationHandle : public::GetNavigationUIData
~~~cpp
virtual NavigationUIData* GetNavigationUIData() = 0;
~~~
 Returns the NavigationUIData associated with the navigation.

### IsExternalProtocol

NavigationHandle : public::IsExternalProtocol
~~~cpp
virtual bool IsExternalProtocol() = 0;
~~~
 Whether the target URL cannot be handled by the browser's internal protocol
 handlers.

### IsServedFromBackForwardCache

NavigationHandle : public::IsServedFromBackForwardCache
~~~cpp
virtual bool IsServedFromBackForwardCache() = 0;
~~~
 Whether the navigation is restoring a page from back-forward cache.

### IsPageActivation

NavigationHandle : public::IsPageActivation
~~~cpp
virtual bool IsPageActivation() const = 0;
~~~
 Whether this navigation is activating an existing page (e.g. served from
 the BackForwardCache or Prerender).

### GetNetErrorCode

NavigationHandle : public::GetNetErrorCode
~~~cpp
virtual net::Error GetNetErrorCode() = 0;
~~~
###  Navigation control flow 
--------------------------------------------------
 The net error code if an error happened prior to commit. Otherwise it will
 be net::OK.

### GetRenderFrameHost

NavigationHandle : public::GetRenderFrameHost
~~~cpp
virtual RenderFrameHost* GetRenderFrameHost() const = 0;
~~~
 Returns the RenderFrameHost this navigation is committing in.  The
 RenderFrameHost returned will be the final host for the navigation. (Use
 WebContentsObserver::RenderFrameHostChanged() to observe RenderFrameHost
 changes that occur during navigation.) This can only be accessed after a
 response has been delivered for processing, or after the navigation fails
 with an error page.


 Note that null will be returned for downloads and/or 204 responses, because
 they don't commit a new document into a renderer process.

### GetPreviousRenderFrameHostId

NavigationHandle : public::GetPreviousRenderFrameHostId
~~~cpp
virtual GlobalRenderFrameHostId GetPreviousRenderFrameHostId() = 0;
~~~
 Returns the id of the RenderFrameHost this navigation is committing from.

 In case a navigation happens within the same RenderFrameHost,
 GetRenderFrameHost() and GetPreviousRenderFrameHostId() will refer to the
 same RenderFrameHost.

 Note: This is not guaranteed to refer to a RenderFrameHost that still
 exists.

### GetExpectedRenderProcessHostId

NavigationHandle : public::GetExpectedRenderProcessHostId
~~~cpp
virtual int GetExpectedRenderProcessHostId() = 0;
~~~
 Returns the id of the RenderProcessHost this navigation is expected to
 commit in. The actual RenderProcessHost may change at commit time. It is
 only valid to call this before commit.

### IsSameDocument

NavigationHandle : public::IsSameDocument
~~~cpp
virtual bool IsSameDocument() const = 0;
~~~
 Whether the navigation happened without changing document. Examples of
 same document navigations are:
 * reference fragment navigations
 * pushState/replaceState
 * same page history navigation
### WasServerRedirect

NavigationHandle : public::WasServerRedirect
~~~cpp
virtual bool WasServerRedirect() = 0;
~~~
 Whether the navigation has encountered a server redirect or not.

### GetRedirectChain

NavigationHandle : public::GetRedirectChain
~~~cpp
virtual const std::vector<GURL>& GetRedirectChain() = 0;
~~~
 Lists the redirects that occurred on the way to the current page. The
 current page is the last one in the list (so even when there's no redirect,
 there will be one entry in the list).

### HasCommitted

NavigationHandle : public::HasCommitted
~~~cpp
virtual bool HasCommitted() const = 0;
~~~
 Whether the navigation has committed. Navigations that end up being
 downloads or return 204/205 response codes do not commit (i.e. the
 WebContents stays at the existing URL).

 This returns true for either successful commits or error pages that
 replace the previous page (distinguished by |IsErrorPage|), and false for
 errors that leave the user on the previous page.

### IsErrorPage

NavigationHandle : public::IsErrorPage
~~~cpp
virtual bool IsErrorPage() const = 0;
~~~
 Whether the navigation committed an error page.


 DO NOT use this before the navigation commit. It would always return false.

 You can use it from WebContentsObserver::DidFinishNavigation().

### HasSubframeNavigationEntryCommitted

NavigationHandle : public::HasSubframeNavigationEntryCommitted
~~~cpp
virtual bool HasSubframeNavigationEntryCommitted() = 0;
~~~
 Not all committed subframe navigations (i.e., !IsInMainFrame &&
 HasCommitted) end up causing a change of the current NavigationEntry. For
 example, some users of NavigationHandle may want to ignore the initial
 commit in a newly added subframe or location.replace events in subframes
 (e.g., ads), while still reacting to user actions like link clicks and
 back/forward in subframes.  Such users should check if this method returns
 true before proceeding.

 Note: it's only valid to call this method for subframes for which
 HasCommitted returns true.

### DidReplaceEntry

NavigationHandle : public::DidReplaceEntry
~~~cpp
virtual bool DidReplaceEntry() = 0;
~~~
 True if the committed entry has replaced the existing one. A non-user
 initiated redirect causes such replacement.

### ShouldUpdateHistory

NavigationHandle : public::ShouldUpdateHistory
~~~cpp
virtual bool ShouldUpdateHistory() = 0;
~~~
 Returns true if the browser history should be updated. Otherwise only
 the session history will be updated. E.g., on unreachable urls or other
 navigations that the users may not think of as navigations (such as
 happens with 'history.replaceState()'), or navigations in non-primary frame
 trees or portals that should not appear in history.

### GetPreviousPrimaryMainFrameURL

NavigationHandle : public::GetPreviousPrimaryMainFrameURL
~~~cpp
virtual const GURL& GetPreviousPrimaryMainFrameURL() = 0;
~~~
 The previous main frame URL that the user was on. This may be empty if
 there was no last committed entry. It is only valid to call this for
 navigations in the primary main frame itself or its subframes.

### GetSocketAddress

NavigationHandle : public::GetSocketAddress
~~~cpp
virtual net::IPEndPoint GetSocketAddress() = 0;
~~~
 Returns the remote address of the socket which fetched this resource.

### GetRequestHeaders

NavigationHandle : public::GetRequestHeaders
~~~cpp
virtual const net::HttpRequestHeaders& GetRequestHeaders() = 0;
~~~
 Returns the headers used for this request.

### RemoveRequestHeader

NavigationHandle : public::RemoveRequestHeader
~~~cpp
virtual void RemoveRequestHeader(const std::string& header_name) = 0;
~~~
 Remove a request's header. If the header is not present, it has no effect.

 Must be called during a redirect.

### SetRequestHeader

NavigationHandle : public::SetRequestHeader
~~~cpp
virtual void SetRequestHeader(const std::string& header_name,
                                const std::string& header_value) = 0;
~~~
 Set a request's header. If the header is already present, its value is
 overwritten. When modified during a navigation start, the headers will be
 applied to the initial network request. When modified during a redirect,
 the headers will be applied to the redirected request.

### SetCorsExemptRequestHeader

NavigationHandle : public::SetCorsExemptRequestHeader
~~~cpp
virtual void SetCorsExemptRequestHeader(const std::string& header_name,
                                          const std::string& header_value) = 0;
~~~
 Set a request's header that is exempt from CORS checks. This is only
 honored if the NetworkContext was configured to allow any cors exempt
 header (see
 |NetworkContext::mojom::allow_any_cors_exempt_header_for_browser|) or
 if |header_name| is specified in
 |NetworkContextParams::cors_exempt_header_list|.

### GetResponseHeaders

NavigationHandle : public::GetResponseHeaders
~~~cpp
virtual const net::HttpResponseHeaders* GetResponseHeaders() = 0;
~~~
 Returns the response headers for the request, or nullptr if there aren't
 any response headers or they have not been received yet. The response
 headers may change during the navigation (e.g. after encountering a server
 redirect). The headers returned should not be modified, as modifications
 will not be reflected in the network stack.

### GetConnectionInfo

NavigationHandle : public::GetConnectionInfo
~~~cpp
virtual net::HttpResponseInfo::ConnectionInfo GetConnectionInfo() = 0;
~~~
 Returns the connection info for the request, the default value is
 CONNECTION_INFO_UNKNOWN if there hasn't been a response (or redirect)
 yet. The connection info may change during the navigation (e.g. after
 encountering a server redirect).

### GetSSLInfo

NavigationHandle : public::GetSSLInfo
~~~cpp
virtual const absl::optional<net::SSLInfo>& GetSSLInfo() = 0;
~~~
 Returns the SSLInfo for a request that succeeded or failed due to a
 certificate error. In the case of other request failures or of a non-secure
 scheme, returns an empty object.

### GetAuthChallengeInfo

NavigationHandle : public::GetAuthChallengeInfo
~~~cpp
virtual const absl::optional<net::AuthChallengeInfo>&
  GetAuthChallengeInfo() = 0;
~~~
 Returns the AuthChallengeInfo for the request, if the response contained an
 authentication challenge.

### GetResolveErrorInfo

NavigationHandle : public::GetResolveErrorInfo
~~~cpp
virtual net::ResolveErrorInfo GetResolveErrorInfo() = 0;
~~~
 Returns host resolution error info associated with the request.

### GetIsolationInfo

NavigationHandle : public::GetIsolationInfo
~~~cpp
virtual net::IsolationInfo GetIsolationInfo() = 0;
~~~
 Gets the net::IsolationInfo associated with the navigation. Updated as
 redirects are followed. When one of the origins used to construct the
 IsolationInfo is opaque, the returned IsolationInfo will not be consistent
 between calls.

### GetGlobalRequestID

NavigationHandle : public::GetGlobalRequestID
~~~cpp
virtual const GlobalRequestID& GetGlobalRequestID() = 0;
~~~
 Returns the ID of the URLRequest associated with this navigation. Can only
 be called from NavigationThrottle::WillProcessResponse and
 WebContentsObserver::ReadyToCommitNavigation.

 In the case of transfer navigations, this is the ID of the first request
 made. The transferred request's ID will not be tracked by the
 NavigationHandle.

### IsDownload

NavigationHandle : public::IsDownload
~~~cpp
virtual bool IsDownload() = 0;
~~~
 Returns true if this navigation resulted in a download. Returns false if
 this navigation did not result in a download, or if download status is not
 yet known for this navigation.  Download status is determined for a
 navigation when processing final (post redirect) HTTP response headers.

### IsFormSubmission

NavigationHandle : public::IsFormSubmission
~~~cpp
virtual bool IsFormSubmission() = 0;
~~~
 Returns true if this navigation was initiated by a form submission.

### WasInitiatedByLinkClick

NavigationHandle : public::WasInitiatedByLinkClick
~~~cpp
virtual bool WasInitiatedByLinkClick() = 0;
~~~
 Returns true if this navigation was initiated by a link click.

### IsSignedExchangeInnerResponse

NavigationHandle : public::IsSignedExchangeInnerResponse
~~~cpp
virtual bool IsSignedExchangeInnerResponse() = 0;
~~~
 Returns true if the target is an inner response of a signed exchange.

### HasPrefetchedAlternativeSubresourceSignedExchange

NavigationHandle : public::HasPrefetchedAlternativeSubresourceSignedExchange
~~~cpp
virtual bool HasPrefetchedAlternativeSubresourceSignedExchange() = 0;
~~~
 Returns true if prefetched alternative subresource signed exchange was sent
 to the renderer process.

### WasResponseCached

NavigationHandle : public::WasResponseCached
~~~cpp
virtual bool WasResponseCached() = 0;
~~~
 Returns true if the navigation response was cached.

### GetProxyServer

NavigationHandle : public::GetProxyServer
~~~cpp
virtual const net::ProxyServer& GetProxyServer() = 0;
~~~
 Returns the proxy server used for this navigation, if any.

### GetHrefTranslate

NavigationHandle : public::GetHrefTranslate
~~~cpp
virtual const std::string& GetHrefTranslate() = 0;
~~~
 Returns the value of the hrefTranslate attribute if this navigation was
 initiated from a link that had that attribute set.

### GetImpression

NavigationHandle : public::GetImpression
~~~cpp
virtual const absl::optional<blink::Impression>& GetImpression() = 0;
~~~
 Returns, if available, the impression associated with the link clicked to
 initiate this navigation. The impression is available for the entire
 lifetime of the navigation.

### GetInitiatorFrameToken

NavigationHandle : public::GetInitiatorFrameToken
~~~cpp
virtual const absl::optional<blink::LocalFrameToken>&
  GetInitiatorFrameToken() = 0;
~~~
 Returns the frame token associated with the frame that initiated the
 navigation. This can be nullptr if the navigation was not associated with a
 frame, or may return a valid frame token to a frame that no longer exists
 because it was deleted before the navigation began. This parameter is
 defined if and only if GetInitiatorProcessID below is.

### GetInitiatorProcessID

NavigationHandle : public::GetInitiatorProcessID
~~~cpp
virtual int GetInitiatorProcessID() = 0;
~~~
 Return the ID of the renderer process of the frame host that initiated the
 navigation. This is defined if and only if GetInitiatorFrameToken above is,
 and it is only valid in conjunction with it.

### GetInitiatorOrigin

NavigationHandle : public::GetInitiatorOrigin
~~~cpp
virtual const absl::optional<url::Origin>& GetInitiatorOrigin() = 0;
~~~
 Returns, if available, the origin of the document that has initiated the
 navigation for this NavigationHandle.

 NOTE: If this is a history navigation, the initiator origin will be the
 origin that initiated the *original* navigation, not the history
 navigation. This means that if there was no initiator origin for the
 original navigation, but the history navigation was initiated by
 javascript, the initiator origin will be null even though
 IsRendererInitiated() returns true.

### GetInitiatorBaseUrl

NavigationHandle : public::GetInitiatorBaseUrl
~~~cpp
virtual const absl::optional<GURL>& GetInitiatorBaseUrl() = 0;
~~~
 Returns, for renderer-initiated about:blank and about:srcdoc navigations,
 the base url of the document that has initiated the navigation for this
 NavigationHandle. The same caveats apply here as for GetInitiatorOrigin().

### GetDnsAliases

NavigationHandle : public::GetDnsAliases
~~~cpp
virtual const std::vector<std::string>& GetDnsAliases() = 0;
~~~
 Retrieves any DNS aliases for the requested URL. Includes all known
 aliases, e.g. from A, AAAA, or HTTPS, not just from the address used for
 the connection, in no particular order.

### IsSameProcess

NavigationHandle : public::IsSameProcess
~~~cpp
virtual bool IsSameProcess() = 0;
~~~
 Whether the new document will be hosted in the same process as the current
 document or not. Set only when the navigation commits.

### GetNavigationEntry

NavigationHandle : public::GetNavigationEntry
~~~cpp
virtual NavigationEntry* GetNavigationEntry() = 0;
~~~
 Returns the NavigationEntry associated with this, which may be null.

### GetNavigationEntryOffset

NavigationHandle : public::GetNavigationEntryOffset
~~~cpp
virtual int GetNavigationEntryOffset() = 0;
~~~
 Returns the offset between the indices of the previous last committed and
 the newly committed navigation entries.

 (e.g. -1 for back navigations, 0 for reloads, 1 for forward navigations).


 Note that this value is computed when we create the navigation request
 and doesn't fully cover all corner cases.

 We try to approximate them with params.should_replace_entry, but in
 some cases it's inaccurate:
 - Main frame client redirects,
 - History navigation to the page with subframes. The subframe
   navigations will return 1 here although they don't create a new
   navigation entry.

### RegisterSubresourceOverride

NavigationHandle : public::RegisterSubresourceOverride
~~~cpp
virtual void RegisterSubresourceOverride(
      blink::mojom::TransferrableURLLoaderPtr transferrable_loader) = 0;
~~~

### ForceEnableOriginTrials

NavigationHandle : public::ForceEnableOriginTrials
~~~cpp
virtual void ForceEnableOriginTrials(
      const std::vector<std::string>& trials) = 0;
~~~
 Force enables the given origin trials for this navigation. This needs to
 be called from WebContents::ReadyToCommitNavigation or earlier to have an
 effect.

### SetIsOverridingUserAgent

NavigationHandle : public::SetIsOverridingUserAgent
~~~cpp
virtual void SetIsOverridingUserAgent(bool override_ua) = 0;
~~~
 Store whether or not we're overriding the user agent. This may only be
 called from DidStartNavigation().

### SetSilentlyIgnoreErrors

NavigationHandle : public::SetSilentlyIgnoreErrors
~~~cpp
virtual void SetSilentlyIgnoreErrors() = 0;
~~~
 Suppress any errors during a navigation and behave as if the user cancelled
 the navigation: no error page will commit.

### SandboxFlagsInherited

NavigationHandle : public::SandboxFlagsInherited
~~~cpp
virtual network::mojom::WebSandboxFlags SandboxFlagsInherited() = 0;
~~~
 The sandbox flags inherited at the beginning of the navigation.


 This is the sandbox flags intersection of:
 - The parent document.

 - The iframe.sandbox attribute.


 Contrary to `SandboxFlagsToCommit()`, this can be called at the beginning
 of the navigation. However, this doesn't include the sandbox flags a
 document applies on itself, via the "Content-Security-Policy: sandbox"
 response header.


 See also: content/browser/renderer_host/sandbox_flags.md
### SandboxFlagsToCommit

NavigationHandle : public::SandboxFlagsToCommit
~~~cpp
virtual network::mojom::WebSandboxFlags SandboxFlagsToCommit() = 0;
~~~
 The sandbox flags of the new document created by this navigation. This
 function can only be called for cross-document navigations after receiving
 the final response.

 See also: content/browser/renderer_host/sandbox_flags.md

 TODO(arthursonzogni): After RenderDocument, this can be computed and stored
 directly into the RenderDocumentHost.

### IsWaitingToCommit

NavigationHandle : public::IsWaitingToCommit
~~~cpp
virtual bool IsWaitingToCommit() = 0;
~~~
 Whether the navigation was sent to be committed in a renderer by the
 RenderFrameHost. This can either be for the commit of a successful
 navigation or an error page.

### WasResourceHintsReceived

NavigationHandle : public::WasResourceHintsReceived
~~~cpp
virtual bool WasResourceHintsReceived() = 0;
~~~
 Returns true when at least one preload or preconnect Link header was
 received via an Early Hints response during this navigation. True only for
 a main frame navigation.

### IsPdf

NavigationHandle : public::IsPdf
~~~cpp
virtual bool IsPdf() = 0;
~~~
 Whether this navigation is for PDF content in a PDF-specific renderer.

### WriteIntoTrace

NavigationHandle : public::WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(
      perfetto::TracedProto<TraceProto> context) const = 0;
~~~
 Write a representation of this object into a trace.

### SetNavigationTimeout

NavigationHandle : public::SetNavigationTimeout
~~~cpp
virtual bool SetNavigationTimeout(base::TimeDelta timeout) = 0;
~~~
 Sets an overall request timeout for this navigation, which will cause the
 navigation to fail if it expires before the navigation commits. This is
 separate from any //net level timeouts. This can only be set at the
 NavigationThrottle::WillRedirectRequest() stage of the navigation. Returns
 `true` if the timeout is being started for the first time. Repeated calls
 will be ignored (they won't reset the timeout) and will return `false`.

### SetAllowCookiesFromBrowser

NavigationHandle : public::SetAllowCookiesFromBrowser
~~~cpp
virtual void SetAllowCookiesFromBrowser(bool allow_cookies_from_browser) = 0;
~~~
 Configures whether a Cookie header added to this request should not be
 overwritten by the network service.

### GetPrerenderTriggerType

NavigationHandle : public::GetPrerenderTriggerType
~~~cpp
virtual PrerenderTriggerType GetPrerenderTriggerType() = 0;
~~~
 Prerender2:
 Used for metrics.

### GetPrerenderEmbedderHistogramSuffix

NavigationHandle : public::GetPrerenderEmbedderHistogramSuffix
~~~cpp
virtual std::string GetPrerenderEmbedderHistogramSuffix() = 0;
~~~

### GetSafeRef

NavigationHandle : public::GetSafeRef
~~~cpp
virtual base::SafeRef<NavigationHandle> GetSafeRef() = 0;
~~~
 Returns a SafeRef to this handle.

### RegisterThrottleForTesting

NavigationHandle : public::RegisterThrottleForTesting
~~~cpp
virtual void RegisterThrottleForTesting(
      std::unique_ptr<NavigationThrottle> navigation_throttle) = 0;
~~~
###  Testing methods 
----------------------------------------------------------

 The following methods should be used exclusively for writing unit tests.

 Registers a NavigationThrottle for tests. The throttle can
 modify the request, pause the request or cancel the request. This will
 take ownership of the NavigationThrottle.

 Note: in non-test cases, NavigationThrottles should not be added directly
 but returned by the implementation of
 ContentBrowserClient::CreateThrottlesForNavigation. This ensures proper
 ordering of the throttles.

### IsDeferredForTesting

NavigationHandle : public::IsDeferredForTesting
~~~cpp
virtual bool IsDeferredForTesting() = 0;
~~~
 Returns whether this navigation is currently deferred.

### IsCommitDeferringConditionDeferredForTesting

NavigationHandle : public::IsCommitDeferringConditionDeferredForTesting
~~~cpp
virtual bool IsCommitDeferringConditionDeferredForTesting() = 0;
~~~

### GetJavaNavigationHandle

NavigationHandle : public::GetJavaNavigationHandle
~~~cpp
virtual const base::android::JavaRef<jobject>& GetJavaNavigationHandle() = 0;
~~~
 Returns a reference to NavigationHandle Java counterpart.

### GetCommitDeferringConditionForTesting

NavigationHandle : public::GetCommitDeferringConditionForTesting
~~~cpp
virtual CommitDeferringCondition* GetCommitDeferringConditionForTesting() = 0;
~~~
 Returns the CommitDeferringCondition that is currently preventing this
 navigation from committing, or nullptr if the navigation isn't currently
 blocked on a CommitDeferringCondition.

### ExistingDocumentWasDiscarded

NavigationHandle : public::ExistingDocumentWasDiscarded
~~~cpp
virtual bool ExistingDocumentWasDiscarded() const = 0;
~~~
 Returns true if the navigation is a reload due to the existing document
 represented by the FrameTreeNode being previously discarded by the browser.

 This can be used as soon as the navigation begins.
