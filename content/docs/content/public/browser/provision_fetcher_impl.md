
## class ProvisionFetcherImpl
    : public
 A media::mojom::ProvisionFetcher implementation based on
 media::ProvisionFetcher.

### ProvisionFetcherImpl

ProvisionFetcherImpl
~~~cpp
explicit ProvisionFetcherImpl(
      std::unique_ptr<media::ProvisionFetcher> provision_fetcher);

  ProvisionFetcherImpl(const ProvisionFetcherImpl&) = delete;
~~~

### operator=

ProvisionFetcherImpl
    : public::operator=
~~~cpp
ProvisionFetcherImpl& operator=(const ProvisionFetcherImpl&) = delete;
~~~

### ~ProvisionFetcherImpl

ProvisionFetcherImpl
    : public::~ProvisionFetcherImpl
~~~cpp
~ProvisionFetcherImpl() override
~~~

### Retrieve

ProvisionFetcherImpl
    : public::Retrieve
~~~cpp
void Retrieve(const GURL& default_url,
                const std::string& request_data,
                RetrieveCallback callback) final;
~~~
 media::mojom::ProvisionFetcher implementation.
