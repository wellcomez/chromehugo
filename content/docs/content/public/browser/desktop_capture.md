### webrtc::DesktopCaptureOptions CreateDesktopCaptureOptions

webrtc::DesktopCaptureOptions CreateDesktopCaptureOptions
~~~cpp
CONTENT_EXPORT webrtc::DesktopCaptureOptions CreateDesktopCaptureOptions();
~~~
 Creates a DesktopCaptureOptions with required settings.

### std::unique_ptr&lt;webrtc::DesktopCapturer&gt; CreateScreenCapturer

std::unique_ptr&lt;webrtc::DesktopCapturer&gt; CreateScreenCapturer
~~~cpp
CONTENT_EXPORT std::unique_ptr<webrtc::DesktopCapturer> CreateScreenCapturer();
~~~
 Creates specific DesktopCapturer with required settings.

### std::unique_ptr&lt;webrtc::DesktopCapturer&gt; CreateWindowCapturer

std::unique_ptr&lt;webrtc::DesktopCapturer&gt; CreateWindowCapturer
~~~cpp
CONTENT_EXPORT std::unique_ptr<webrtc::DesktopCapturer> CreateWindowCapturer();
~~~

### BindAuraWindowCapturer

BindAuraWindowCapturer
~~~cpp
CONTENT_EXPORT void BindAuraWindowCapturer(
    mojo::PendingReceiver<video_capture::mojom::Device> receiver,
    const content::DesktopMediaID& id);
~~~
 This is currently used only by ash-chrome, and we don't yet want to stabilize
 this API.

### CanUsePipeWire

CanUsePipeWire
~~~cpp
CONTENT_EXPORT bool CanUsePipeWire();
~~~
 Returns whether we can use PipeWire capturer based on:
 1) We run Linux Wayland session
 2) WebRTC is built with PipeWire enabled
 3) Chromium has features::kWebRtcPipeWireCapturer enabled
### ShouldEnumerateCurrentProcessWindows

ShouldEnumerateCurrentProcessWindows
~~~cpp
CONTENT_EXPORT bool ShouldEnumerateCurrentProcessWindows();
~~~
 Whether the capturer should find windows owned by the current process.

