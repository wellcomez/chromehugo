
## class BluetoothDelegate::FramePermissionObserver : public
 An observer used to track permission revocation events for a particular
 RenderFrameHost.

### GetRenderFrameHost

FramePermissionObserver : public::BluetoothDelegate::GetRenderFrameHost
~~~cpp
virtual RenderFrameHost* GetRenderFrameHost() = 0;
~~~
 Returns the frame that the observer wishes to watch.

## class BluetoothDelegate
 Provides an interface for managing device permissions for Web Bluetooth and
 Web Bluetooth Scanning API. An embedder may implement this to manage these
 permissions.

 TODO(https://crbug.com/1048325): There are several Bluetooth related methods
 in WebContentsDelegate and ContentBrowserClient that can be moved into this
 class.

### ~BluetoothDelegate

~BluetoothDelegate
~~~cpp
virtual ~BluetoothDelegate() = default;
~~~

### RunBluetoothChooser

BluetoothDelegate::RunBluetoothChooser
~~~cpp
virtual std::unique_ptr<BluetoothChooser> RunBluetoothChooser(
      RenderFrameHost* frame,
      const BluetoothChooser::EventHandler& event_handler) = 0;
~~~
 Shows a chooser for the user to select a nearby Bluetooth device. The
 EventHandler should live at least as long as the returned chooser object.

### ShowBluetoothScanningPrompt

BluetoothDelegate::ShowBluetoothScanningPrompt
~~~cpp
virtual std::unique_ptr<BluetoothScanningPrompt> ShowBluetoothScanningPrompt(
      RenderFrameHost* frame,
      const BluetoothScanningPrompt::EventHandler& event_handler) = 0;
~~~
 Shows a prompt for the user to allow/block Bluetooth scanning. The
 EventHandler should live at least as long as the returned prompt object.

### ShowDevicePairPrompt

BluetoothDelegate::ShowDevicePairPrompt
~~~cpp
virtual void ShowDevicePairPrompt(
      RenderFrameHost* frame,
      const std::u16string& device_identifier,
      PairPromptCallback callback,
      PairingKind pairing_kind,
      const absl::optional<std::u16string>& pin) = 0;
~~~
 Prompt the user (via dialog, etc.) for pairing Bluetooth device
 |device_identifier| is any string the caller wants to display
 to the user to identify the device (MAC address, name, etc.). |callback|
 will be called with the prompt result. |callback| may be called immediately
 from this function, for example, if a credential prompt for the given
 |frame| is already displayed.

 |pairing_kind| is to determine which pairing kind of prompt to be created
### GetWebBluetoothDeviceId

BluetoothDelegate::GetWebBluetoothDeviceId
~~~cpp
virtual blink::WebBluetoothDeviceId GetWebBluetoothDeviceId(
      RenderFrameHost* frame,
      const std::string& device_address) = 0;
~~~
 This should return the WebBluetoothDeviceId that corresponds to the device
 with |device_address| in the current |frame|. If there is not a
 corresponding ID, then an invalid WebBluetoothDeviceId should be returned.

### GetDeviceAddress

BluetoothDelegate::GetDeviceAddress
~~~cpp
virtual std::string GetDeviceAddress(
      RenderFrameHost* frame,
      const blink::WebBluetoothDeviceId& device_id) = 0;
~~~
 This should return the device address corresponding to a device with
 |device_id| in the current |frame|. If there is not a corresponding
 address, then an empty string should be returned.

### AddScannedDevice

BluetoothDelegate::AddScannedDevice
~~~cpp
virtual blink::WebBluetoothDeviceId AddScannedDevice(
      RenderFrameHost* frame,
      const std::string& device_address) = 0;
~~~
 This should return the WebBluetoothDeviceId for |device_address| if the
 device has been assigned an ID previously through AddScannedDevice() or
 GrantServiceAccessPermission(). If not, a new ID should be generated for
 |device_address| and stored in a temporary map of address to ID. Service
 access should not be granted to these devices.

### GrantServiceAccessPermission

BluetoothDelegate::GrantServiceAccessPermission
~~~cpp
virtual blink::WebBluetoothDeviceId GrantServiceAccessPermission(
      RenderFrameHost* frame,
      const device::BluetoothDevice* device,
      const blink::mojom::WebBluetoothRequestDeviceOptions* options) = 0;
~~~
 This should grant permission to the requesting and embedding origins
 represented by |frame| to connect to the device with |device_address| and
 access its |services|. Once permission is granted, a |WebBluetoothDeviceId|
 should be generated for the device and returned.

### HasDevicePermission

BluetoothDelegate::HasDevicePermission
~~~cpp
virtual bool HasDevicePermission(
      RenderFrameHost* frame,
      const blink::WebBluetoothDeviceId& device_id) = 0;
~~~
 This should return true if |frame| has been granted permission to access
 the device with |device_id| through GrantServiceAccessPermission().

 |device_id|s generated with AddScannedDevices() should return false.

### RevokeDevicePermissionWebInitiated

BluetoothDelegate::RevokeDevicePermissionWebInitiated
~~~cpp
virtual void RevokeDevicePermissionWebInitiated(
      RenderFrameHost* frame,
      const blink::WebBluetoothDeviceId& device_id) = 0;
~~~
 Revokes |frame| access to the Bluetooth device ordered by website.

### IsAllowedToAccessService

BluetoothDelegate::IsAllowedToAccessService
~~~cpp
virtual bool IsAllowedToAccessService(
      RenderFrameHost* frame,
      const blink::WebBluetoothDeviceId& device_id,
      const device::BluetoothUUID& service) = 0;
~~~
 This should return true if |frame| has permission to access |service| from
 the device with |device_id|.

### IsAllowedToAccessAtLeastOneService

BluetoothDelegate::IsAllowedToAccessAtLeastOneService
~~~cpp
virtual bool IsAllowedToAccessAtLeastOneService(
      RenderFrameHost* frame,
      const blink::WebBluetoothDeviceId& device_id) = 0;
~~~
 This should return true if |frame| can access at least one service from the
 device with |device_id|.

### IsAllowedToAccessManufacturerData

BluetoothDelegate::IsAllowedToAccessManufacturerData
~~~cpp
virtual bool IsAllowedToAccessManufacturerData(
      RenderFrameHost* frame,
      const blink::WebBluetoothDeviceId& device_id,
      uint16_t manufacturer_code) = 0;
~~~
 This should return true if |frame| has permission to access data associated
 with |manufacturer_code| from advertisement packets from the device with
 |device_id|.

### GetPermittedDevices

BluetoothDelegate::GetPermittedDevices
~~~cpp
virtual std::vector<blink::mojom::WebBluetoothDevicePtr> GetPermittedDevices(
      RenderFrameHost* frame) = 0;
~~~
 This should return a list of devices that the origin in |frame| has been
 allowed to access. Access permission is granted with
 GrantServiceAccessPermission() and can be revoked by the user in the
 embedder's UI. The list of devices returned should be PermittedDevice
 objects, which contain the necessary fields to create the BluetoothDevice
 JavaScript objects.

### AddFramePermissionObserver

BluetoothDelegate::AddFramePermissionObserver
~~~cpp
virtual void AddFramePermissionObserver(
      FramePermissionObserver* observer) = 0;
~~~
 Add a permission observer to allow observing permission revocation effects
 for a particular frame.

### RemoveFramePermissionObserver

BluetoothDelegate::RemoveFramePermissionObserver
~~~cpp
virtual void RemoveFramePermissionObserver(
      FramePermissionObserver* observer) = 0;
~~~
 Remove a previously added permission observer.
