
## class WebContentsUserData

### WebContentsUserData

WebContentsUserData
~~~cpp
explicit WebContentsUserData(WebContents& web_contents)
      : web_contents_(&web_contents) {}
~~~

### CreateForWebContents

CreateForWebContents
~~~cpp
static void CreateForWebContents(WebContents* contents, Args&&... args) {
    DCHECK(contents);
    if (!FromWebContents(contents)) {
      contents->SetUserData(
          UserDataKey(),
          base::WrapUnique(new T(contents, std::forward<Args>(args)...)));
    }
  }
~~~

### FromWebContents

FromWebContents
~~~cpp
static T* FromWebContents(WebContents* contents) {
    DCHECK(contents);
    return static_cast<T*>(contents->GetUserData(UserDataKey()));
  }
~~~
 Retrieves the instance of type T that was attached to the specified
 WebContents (via CreateForWebContents above) and returns it. If no instance
 of the type was attached, returns nullptr.

### FromWebContents

FromWebContents
~~~cpp
static const T* FromWebContents(const WebContents* contents) {
    DCHECK(contents);
    return static_cast<const T*>(contents->GetUserData(UserDataKey()));
  }
~~~

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### GetWebContents

GetWebContents
~~~cpp
content::WebContents& GetWebContents() { return *web_contents_; }
~~~
 Returns the WebContents associated with `this` object of a subclass
 which inherits from WebContentsUserData.


 The returned `WebContents` is guaranteed to live as long as `this`
 WebContentsUserData (due to how UserData works - WebContents
 owns `this` UserData).

### GetWebContents

GetWebContents
~~~cpp
const content::WebContents& GetWebContents() const { return *web_contents_; }
~~~

### web_contents_



~~~cpp

const raw_ptr<content::WebContents> web_contents_ = nullptr;

~~~

 This is a pointer (rather than a reference) to ensure that go/miracleptr
 can cover this field (see also //base/memory/raw_ptr.md).

### WebContentsUserData

WebContentsUserData
~~~cpp
explicit WebContentsUserData(WebContents& web_contents)
      : web_contents_(&web_contents) {}
~~~

### FromWebContents

FromWebContents
~~~cpp
static T* FromWebContents(WebContents* contents) {
    DCHECK(contents);
    return static_cast<T*>(contents->GetUserData(UserDataKey()));
  }
~~~
 Retrieves the instance of type T that was attached to the specified
 WebContents (via CreateForWebContents above) and returns it. If no instance
 of the type was attached, returns nullptr.

### FromWebContents

FromWebContents
~~~cpp
static const T* FromWebContents(const WebContents* contents) {
    DCHECK(contents);
    return static_cast<const T*>(contents->GetUserData(UserDataKey()));
  }
~~~

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### GetWebContents

GetWebContents
~~~cpp
content::WebContents& GetWebContents() { return *web_contents_; }
~~~
 Returns the WebContents associated with `this` object of a subclass
 which inherits from WebContentsUserData.


 The returned `WebContents` is guaranteed to live as long as `this`
 WebContentsUserData (due to how UserData works - WebContents
 owns `this` UserData).

### GetWebContents

GetWebContents
~~~cpp
const content::WebContents& GetWebContents() const { return *web_contents_; }
~~~

### web_contents_



~~~cpp

const raw_ptr<content::WebContents> web_contents_ = nullptr;

~~~

 This is a pointer (rather than a reference) to ensure that go/miracleptr
 can cover this field (see also //base/memory/raw_ptr.md).
