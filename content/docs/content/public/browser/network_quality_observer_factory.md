### std::unique_ptr&lt;
    network::NetworkQualityTracker::RTTAndThroughputEstimatesObserver&gt;
CreateNetworkQualityObserver

std::unique_ptr&lt;
    network::NetworkQualityTracker::RTTAndThroughputEstimatesObserver&gt;
CreateNetworkQualityObserver
~~~cpp
CONTENT_EXPORT std::unique_ptr<
    network::NetworkQualityTracker::RTTAndThroughputEstimatesObserver>
CreateNetworkQualityObserver(
    network::NetworkQualityTracker* network_quality_tracker);
~~~
 Creates network quality observer that listens for changes to the network
 quality and manages sending updates to each RenderProcess.

