
## class BrowserPluginGuestDelegate
 Objects implement this interface to get notified about changes in the guest
 WebContents and to provide necessary functionality.

### CreateNewGuestWindow

BrowserPluginGuestDelegate::CreateNewGuestWindow
~~~cpp
virtual std::unique_ptr<WebContents> CreateNewGuestWindow(
      const WebContents::CreateParams& create_params);
~~~

### GetOwnerWebContents

BrowserPluginGuestDelegate::GetOwnerWebContents
~~~cpp
virtual WebContents* GetOwnerWebContents();
~~~
 Returns the WebContents that currently owns this guest.

### GetProspectiveOuterDocument

BrowserPluginGuestDelegate::GetProspectiveOuterDocument
~~~cpp
virtual RenderFrameHost* GetProspectiveOuterDocument();
~~~
 Returns the RenderFrameHost that owns this guest, but has not yet attached
 it.

 TODO(crbug.com/769461): Have all guest types return the specific owner
 RenderFrameHost and not assume it's the owner's main frame.

### GetGuestDelegateWeakPtr

BrowserPluginGuestDelegate::GetGuestDelegateWeakPtr
~~~cpp
virtual base::WeakPtr<BrowserPluginGuestDelegate> GetGuestDelegateWeakPtr();
~~~
