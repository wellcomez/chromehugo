
## class BackgroundSyncRegistration

### BackgroundSyncRegistration

BackgroundSyncRegistration::BackgroundSyncRegistration
~~~cpp
BackgroundSyncRegistration(const BackgroundSyncRegistration& other)
~~~

### operator=

BackgroundSyncRegistration::operator=
~~~cpp
BackgroundSyncRegistration& operator=(
      const BackgroundSyncRegistration& other);
~~~

### ~BackgroundSyncRegistration

BackgroundSyncRegistration::~BackgroundSyncRegistration
~~~cpp
~BackgroundSyncRegistration()
~~~

### Equals

BackgroundSyncRegistration::Equals
~~~cpp
bool Equals(const BackgroundSyncRegistration& other) const;
~~~

### IsFiring

BackgroundSyncRegistration::IsFiring
~~~cpp
bool IsFiring() const;
~~~

### options

options
~~~cpp
const blink::mojom::SyncRegistrationOptions* options() const {
    return &options_;
  }
~~~

### options

options
~~~cpp
blink::mojom::SyncRegistrationOptions* options() { return &options_; }
~~~

### sync_state

sync_state
~~~cpp
blink::mojom::BackgroundSyncState sync_state() const { return sync_state_; }
~~~

### set_sync_state

set_sync_state
~~~cpp
void set_sync_state(blink::mojom::BackgroundSyncState state) {
    sync_state_ = state;
  }
~~~

### num_attempts

num_attempts
~~~cpp
int num_attempts() const { return num_attempts_; }
~~~

### set_num_attempts

set_num_attempts
~~~cpp
void set_num_attempts(int num_attempts) { num_attempts_ = num_attempts; }
~~~

### max_attempts

max_attempts
~~~cpp
int max_attempts() const { return max_attempts_; }
~~~

### set_max_attempts

set_max_attempts
~~~cpp
void set_max_attempts(int max_attempts) { max_attempts_ = max_attempts; }
~~~

### delay_until

delay_until
~~~cpp
base::Time delay_until() const { return delay_until_; }
~~~

### set_delay_until

set_delay_until
~~~cpp
void set_delay_until(base::Time delay_until) { delay_until_ = delay_until; }
~~~

### resolved

resolved
~~~cpp
bool resolved() const { return resolved_; }
~~~
 By default, new registrations will not fire until set_resolved is called
 after the registration resolves.

### set_resolved

set_resolved
~~~cpp
void set_resolved() { resolved_ = true; }
~~~

### sync_type

sync_type
~~~cpp
blink::mojom::BackgroundSyncType sync_type() const {
    return options_.min_interval >= 0
               ? blink::mojom::BackgroundSyncType::PERIODIC
               : blink::mojom::BackgroundSyncType::ONE_SHOT;
  }
~~~
 Whether the registration is periodic or one-shot.

### origin

origin
~~~cpp
const url::Origin& origin() const { return origin_; }
~~~

### set_origin

set_origin
~~~cpp
void set_origin(const url::Origin& origin) { origin_ = origin; }
~~~

### is_suspended

is_suspended
~~~cpp
bool is_suspended() const {
    if (sync_type() == blink::mojom::BackgroundSyncType::ONE_SHOT)
      return false;
    return delay_until_.is_max();
  }
~~~
