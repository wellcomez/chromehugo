
## class PeakGpuMemoryTracker
 Tracks the peak memory of the GPU service for its lifetime. Upon its
 destruction a report will be requested from the GPU service. The peak will be
 reported to UMA Histograms.


 If the GPU is lost during this objects lifetime, there will be no
 corresponding report of usage. The same for if there is never a successful
 GPU connection.


 See PeakGpuMemoryTracker::Create.

### Create

PeakGpuMemoryTracker::Create
~~~cpp
static std::unique_ptr<PeakGpuMemoryTracker> Create(Usage usage);
~~~
 Creates the PeakGpuMemoryTracker, which performs the registration with the
 GPU service. Destroy the PeakGpuMemoryTracker to request a report from the
 GPU service. The report will be recorded in UMA Histograms for the given
 |usage| type.

### ~PeakGpuMemoryTracker

~PeakGpuMemoryTracker
~~~cpp
virtual ~PeakGpuMemoryTracker() = default;
~~~

### PeakGpuMemoryTracker

PeakGpuMemoryTracker
~~~cpp
PeakGpuMemoryTracker(const PeakGpuMemoryTracker*) = delete;
~~~

### operator=

PeakGpuMemoryTracker::operator=
~~~cpp
PeakGpuMemoryTracker& operator=(const PeakGpuMemoryTracker&) = delete;
~~~

### Cancel

PeakGpuMemoryTracker::Cancel
~~~cpp
virtual void Cancel() = 0;
~~~
 Invalidates this tracker, no UMA Histogram report is generated.
