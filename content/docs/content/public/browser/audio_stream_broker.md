### AudioStreamBroker

AudioStreamBroker
~~~cpp
AudioStreamBroker(int render_process_id, int render_frame_id)
~~~

### AudioStreamBroker

AudioStreamBroker
~~~cpp
AudioStreamBroker(const AudioStreamBroker&) = delete;
~~~

### operator=

operator=
~~~cpp
AudioStreamBroker& operator=(const AudioStreamBroker&) = delete;
~~~

### ~AudioStreamBroker

~AudioStreamBroker
~~~cpp
~AudioStreamBroker()
~~~

### CreateStream

CreateStream
~~~cpp
virtual void CreateStream(media::mojom::AudioStreamFactory* factory) = 0;
~~~

### NotifyProcessHostOfStartedStream

NotifyProcessHostOfStartedStream
~~~cpp
static void NotifyProcessHostOfStartedStream(int render_process_id);
~~~
 Thread-safe utility that notifies the process host identified by
 |render_process_id| of a started stream to ensure that the renderer is not
 backgrounded. Must be paired with a later call to
 NotifyRenderProcessOfStoppedStream()
### NotifyProcessHostOfStoppedStream

NotifyProcessHostOfStoppedStream
~~~cpp
static void NotifyProcessHostOfStoppedStream(int render_process_id);
~~~

### render_process_id

render_process_id
~~~cpp
int render_process_id() const { return render_process_id_; }
~~~

### render_frame_id

render_frame_id
~~~cpp
int render_frame_id() const { return render_frame_id_; }
~~~

## class AudioStreamBroker
 An AudioStreamBroker is used to broker a connection between a client
 (typically renderer) and the audio service. It also sets up all objects
 used for monitoring the stream. All AudioStreamBrokers are used on the IO
 thread.

### LoopbackSink

LoopbackSink
~~~cpp
LoopbackSink(const LoopbackSink&) = delete;
~~~

### operator=

AudioStreamBroker::operator=
~~~cpp
LoopbackSink& operator=(const LoopbackSink&) = delete;
~~~

### ~LoopbackSink

AudioStreamBroker::~LoopbackSink
~~~cpp
~LoopbackSink()
~~~

### OnSourceGone

AudioStreamBroker::OnSourceGone
~~~cpp
virtual void OnSourceGone() = 0;
~~~

## class LoopbackSource

### LoopbackSource

LoopbackSource
~~~cpp
LoopbackSource(const LoopbackSource&) = delete;
~~~

### operator=

LoopbackSource::operator=
~~~cpp
LoopbackSource& operator=(const LoopbackSource&) = delete;
~~~

### ~LoopbackSource

LoopbackSource::~LoopbackSource
~~~cpp
~LoopbackSource()
~~~

### AddLoopbackSink

LoopbackSource::AddLoopbackSink
~~~cpp
virtual void AddLoopbackSink(LoopbackSink* sink) = 0;
~~~

### RemoveLoopbackSink

LoopbackSource::RemoveLoopbackSink
~~~cpp
virtual void RemoveLoopbackSink(LoopbackSink* sink) = 0;
~~~

### GetGroupID

LoopbackSource::GetGroupID
~~~cpp
virtual const base::UnguessableToken& GetGroupID() = 0;
~~~

## class AudioStreamBrokerFactory
 Used for dependency injection into ForwardingAudioStreamFactory. Used on the
 IO thread.

### AudioStreamBrokerFactory

AudioStreamBrokerFactory
~~~cpp
AudioStreamBrokerFactory(const AudioStreamBrokerFactory&) = delete;
~~~

### operator=

AudioStreamBrokerFactory::operator=
~~~cpp
AudioStreamBrokerFactory& operator=(const AudioStreamBrokerFactory&) = delete;
~~~

### ~AudioStreamBrokerFactory

AudioStreamBrokerFactory::~AudioStreamBrokerFactory
~~~cpp
~AudioStreamBrokerFactory()
~~~

### CreateAudioInputStreamBroker

AudioStreamBrokerFactory::CreateAudioInputStreamBroker
~~~cpp
virtual std::unique_ptr<AudioStreamBroker> CreateAudioInputStreamBroker(
      int render_process_id,
      int render_frame_id,
      const std::string& device_id,
      const media::AudioParameters& params,
      uint32_t shared_memory_count,
      media::UserInputMonitorBase* user_input_monitor,
      bool enable_agc,
      media::mojom::AudioProcessingConfigPtr processing_config,
      AudioStreamBroker::DeleterCallback deleter,
      mojo::PendingRemote<blink::mojom::RendererAudioInputStreamFactoryClient>
          renderer_factory_client) = 0;
~~~

### CreateAudioLoopbackStreamBroker

AudioStreamBrokerFactory::CreateAudioLoopbackStreamBroker
~~~cpp
virtual std::unique_ptr<AudioStreamBroker> CreateAudioLoopbackStreamBroker(
      int render_process_id,
      int render_frame_id,
      AudioStreamBroker::LoopbackSource* source,
      const media::AudioParameters& params,
      uint32_t shared_memory_count,
      bool mute_source,
      AudioStreamBroker::DeleterCallback deleter,
      mojo::PendingRemote<blink::mojom::RendererAudioInputStreamFactoryClient>
          renderer_factory_client) = 0;
~~~

### CreateAudioOutputStreamBroker

AudioStreamBrokerFactory::CreateAudioOutputStreamBroker
~~~cpp
virtual std::unique_ptr<AudioStreamBroker> CreateAudioOutputStreamBroker(
      int render_process_id,
      int render_frame_id,
      int stream_id,
      const std::string& output_device_id,
      const media::AudioParameters& params,
      const base::UnguessableToken& group_id,
      AudioStreamBroker::DeleterCallback deleter,
      mojo::PendingRemote<media::mojom::AudioOutputStreamProviderClient>
          client) = 0;
~~~
