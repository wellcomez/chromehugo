
## class RenderWidgetHostIterator
 RenderWidgetHostIterator is used to safely iterate over a list of
 RenderWidgetHosts.

### ~RenderWidgetHostIterator

~RenderWidgetHostIterator
~~~cpp
virtual ~RenderWidgetHostIterator() {}
~~~

### GetNextHost

GetNextHost
~~~cpp
virtual RenderWidgetHost* GetNextHost() = 0;
~~~
 Returns the next RenderWidgetHost in the list. Returns nullptr if none is
 available.

### ~RenderWidgetHostIterator

~RenderWidgetHostIterator
~~~cpp
virtual ~RenderWidgetHostIterator() {}
~~~

### GetNextHost

GetNextHost
~~~cpp
virtual RenderWidgetHost* GetNextHost() = 0;
~~~
 Returns the next RenderWidgetHost in the list. Returns nullptr if none is
 available.
