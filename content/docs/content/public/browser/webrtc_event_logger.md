
## class WebRtcEventLogger
 Interface for a logger of WebRTC events, which the embedding application may
 subclass and instantiate. Only one instance may ever be created, and it must
 live until the embedding application terminates.

### ~WebRtcEventLogger

WebRtcEventLogger::~WebRtcEventLogger
~~~cpp
~WebRtcEventLogger()
~~~

### EnableLocalLogging

WebRtcEventLogger::EnableLocalLogging
~~~cpp
virtual void EnableLocalLogging(const base::FilePath& base_path) = 0;
~~~
 Enable local logging of WebRTC events.

 Local logging is distinguished from remote logging, in that local logs are
 kept in response to explicit user input, are saved to a specific location,
 and are never removed by the application. Logging is done on a best-effort
 basis. An illegal file path, or one where we don't have the necessary
 permissions, will result in a failure to log WebRTC events. Also, there is
 a limit on the number of files and on the length of the log filenames that
 could also prevent the logging from happening. If the number of currently
 active peer connections exceeds the maximum number of local log files,
 there is no guarantee about which PCs will get a local log file associated
 (specifically, we do *not* guarantee it would be either the oldest or the
 newest).

### DisableLocalLogging

WebRtcEventLogger::DisableLocalLogging
~~~cpp
virtual void DisableLocalLogging() = 0;
~~~
 Disable local logging of WebRTC events.

 Any active local logs are stopped. Peer connections added after this call
 will not get a local log associated with them (unless local logging is
 once again enabled).
