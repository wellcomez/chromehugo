
## class BrowserChildProcessHostDelegate : public
 Interface that all users of BrowserChildProcessHost need to provide.

### OnProcessLaunched

OnProcessLaunched
~~~cpp
virtual void OnProcessLaunched() {}
~~~
 Called when the process has been started.

### OnProcessLaunchFailed

OnProcessLaunchFailed
~~~cpp
virtual void OnProcessLaunchFailed(int error_code) {}
~~~
 Called if the process failed to launch.  In this case the process never
 started so the code here is a platform specific error code.

### OnProcessCrashed

OnProcessCrashed
~~~cpp
virtual void OnProcessCrashed(int exit_code) {}
~~~
 Called if the process crashed. |exit_code| is the status returned when the
 process crashed (for posix, as returned from waitpid(), for Windows, as
 returned from GetExitCodeProcess()).

### GetServiceName

BrowserChildProcessHostDelegate : public::GetServiceName
~~~cpp
virtual absl::optional<std::string> GetServiceName();
~~~
 Returns a string identifying the primary service running in the child
 process, if any.

### BindHostReceiver

BindHostReceiver
~~~cpp
virtual void BindHostReceiver(mojo::GenericPendingReceiver receiver) {}
~~~
 Binds an interface receiver in the host process, as requested by the child
 process.

### OnMessageReceived

BrowserChildProcessHostDelegate : public::OnMessageReceived
~~~cpp
bool OnMessageReceived(const IPC::Message& message) override;
~~~
 Default no-op handler for incoming legacy IPCs, for processes that don't
 use legacy IPC.
