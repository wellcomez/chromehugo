
## class PageUserData

### CreateForPage

CreateForPage
~~~cpp
static void CreateForPage(Page& page, Args&&... args) {
    if (!GetForPage(page)) {
      T* data = new T(page, std::forward<Args>(args)...);
      page.SetUserData(UserDataKey(), base::WrapUnique(data));
    }
  }
~~~

### GetForPage

GetForPage
~~~cpp
static T* GetForPage(Page& page) {
    return static_cast<T*>(page.GetUserData(UserDataKey()));
  }
~~~
 TODO(crbug.com/1335845): Due to a unresolved bug, this can return nullptr
 even after CreateForPage() or GetOrCreateForPage() has been called.

### GetOrCreateForPage

GetOrCreateForPage
~~~cpp
static T* GetOrCreateForPage(Page& page) {
    if (auto* data = GetForPage(page)) {
      return data;
    }

    CreateForPage(page);
    return GetForPage(page);
  }
~~~

### DeleteForPage

DeleteForPage
~~~cpp
static void DeleteForPage(Page& page) {
    DCHECK(GetForPage(page));
    page.RemoveUserData(UserDataKey());
  }
~~~

### page

page
~~~cpp
Page& page() const { return *page_; }
~~~

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### PageUserData

PageUserData
~~~cpp
explicit PageUserData(Page& page) : page_(page) {}
~~~

### page_



~~~cpp

const raw_ref<Page> page_;

~~~

 Page associated with subclass which inherits this PageUserData.

### GetForPage

GetForPage
~~~cpp
static T* GetForPage(Page& page) {
    return static_cast<T*>(page.GetUserData(UserDataKey()));
  }
~~~
 TODO(crbug.com/1335845): Due to a unresolved bug, this can return nullptr
 even after CreateForPage() or GetOrCreateForPage() has been called.

### GetOrCreateForPage

GetOrCreateForPage
~~~cpp
static T* GetOrCreateForPage(Page& page) {
    if (auto* data = GetForPage(page)) {
      return data;
    }

    CreateForPage(page);
    return GetForPage(page);
  }
~~~

### DeleteForPage

DeleteForPage
~~~cpp
static void DeleteForPage(Page& page) {
    DCHECK(GetForPage(page));
    page.RemoveUserData(UserDataKey());
  }
~~~

### page

page
~~~cpp
Page& page() const { return *page_; }
~~~

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### PageUserData

PageUserData
~~~cpp
explicit PageUserData(Page& page) : page_(page) {}
~~~

### page_



~~~cpp

const raw_ref<Page> page_;

~~~

 Page associated with subclass which inherits this PageUserData.
