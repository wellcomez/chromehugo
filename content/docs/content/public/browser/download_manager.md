---
tags:
  - download
---

### SetDelegate

SetDelegate
~~~cpp
virtual void SetDelegate(DownloadManagerDelegate* delegate) = 0;
~~~
 Sets/Gets the delegate for this DownloadManager. The delegate has to live
 past its Shutdown method being called (by the DownloadManager).

### GetDelegate

GetDelegate
~~~cpp
virtual DownloadManagerDelegate* GetDelegate() = 0;
~~~

### Shutdown

Shutdown
~~~cpp
virtual void Shutdown() = 0;
~~~
 Shutdown the download manager. Content calls this when BrowserContext is
 being destructed. If the embedder needs this to be called earlier, it can
 call it. In that case, the delegate's Shutdown() method will only be called
 once.

### RemoveDownloadsByURLAndTime

RemoveDownloadsByURLAndTime
~~~cpp
virtual int RemoveDownloadsByURLAndTime(
      const base::RepeatingCallback<bool(const GURL&)>& url_filter,
      base::Time remove_begin,
      base::Time remove_end) = 0;
~~~
 Remove downloads whose URLs match the |url_filter| and are within
 the given time constraints - after remove_begin (inclusive) and before
 remove_end (exclusive). You may pass in null Time values to do an unbounded
 delete in either direction.

### DownloadUrl

DownloadUrl
~~~cpp
virtual void DownloadUrl(
      std::unique_ptr<download::DownloadUrlParameters> parameters,
      scoped_refptr<network::SharedURLLoaderFactory>
          blob_url_loader_factory) = 0;
~~~
 For downloads of blob URLs, the caller can pass a URLLoaderFactory to
 use to load the Blob URL. If none is specified and the blob URL cannot be
 mapped to a blob by the time the download request starts, then the download
 will fail.

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(Observer* observer) = 0;
~~~
 Allow objects to observe the download creation process.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(Observer* observer) = 0;
~~~
 Remove a download observer from ourself.

### CreateDownloadItem

CreateDownloadItem
~~~cpp
virtual download::DownloadItem* CreateDownloadItem(
      const std::string& guid,
      uint32_t id,
      const base::FilePath& current_path,
      const base::FilePath& target_path,
      const std::vector<GURL>& url_chain,
      const GURL& referrer_url,
      const StoragePartitionConfig& storage_partition_config,
      const GURL& tab_url,
      const GURL& tab_referrer_url,
      const absl::optional<url::Origin>& request_initiator,
      const std::string& mime_type,
      const std::string& original_mime_type,
      base::Time start_time,
      base::Time end_time,
      const std::string& etag,
      const std::string& last_modified,
      int64_t received_bytes,
      int64_t total_bytes,
      const std::string& hash,
      download::DownloadItem::DownloadState state,
      download::DownloadDangerType danger_type,
      download::DownloadInterruptReason interrupt_reason,
      bool opened,
      base::Time last_access_time,
      bool transient,
      const std::vector<download::DownloadItem::ReceivedSlice>&
          received_slices) = 0;
~~~
 Called by the embedder, after creating the download manager, to let it know
 about downloads from previous runs of the browser.

### PostInitialization

PostInitialization
~~~cpp
virtual void PostInitialization(
      DownloadInitializationDependency dependency) = 0;
~~~
 Called when download manager has loaded all the data, once when the history
 db is initialized and once when the in-progress cache is initialized.

### IsManagerInitialized

IsManagerInitialized
~~~cpp
virtual bool IsManagerInitialized() = 0;
~~~
 Returns if the manager has been initialized and loaded all the data.

### InProgressCount

InProgressCount
~~~cpp
virtual int InProgressCount() = 0;
~~~
 The number of in progress (including paused) downloads.

 Performance note: this loops over all items. If profiling finds that this
 is too slow, use an AllDownloadItemNotifier to count in-progress items.

### NonMaliciousInProgressCount

NonMaliciousInProgressCount
~~~cpp
virtual int NonMaliciousInProgressCount() = 0;
~~~
 The number of in progress (including paused) downloads.

 Performance note: this loops over all items. If profiling finds that this
 is too slow, use an AllDownloadItemNotifier to count in-progress items.

 This excludes downloads that are marked as malicious.

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual BrowserContext* GetBrowserContext() = 0;
~~~

### OnHistoryQueryComplete

OnHistoryQueryComplete
~~~cpp
virtual void OnHistoryQueryComplete(
      base::OnceClosure load_history_downloads_cb) = 0;
~~~
 Called when download history query completes. Call
 |load_history_downloads_cb| to load all the history downloads.

### GetDownload

GetDownload
~~~cpp
virtual download::DownloadItem* GetDownload(uint32_t id) = 0;
~~~
 Get the download item for |id| if present, no matter what type of download
 it is or state it's in.

 DEPRECATED: Don't add new callers for GetDownload(uint32_t). Instead keep
 track of the GUID and use GetDownloadByGuid(), or observe the
 download::DownloadItem if you need to keep track of a specific download.

 (http://crbug.com/593020)
### GetNextId

GetNextId
~~~cpp
virtual void GetNextId(GetNextIdCallback callback) = 0;
~~~
 Called to get an ID for a new download. |callback| may be called
 synchronously.

### StoragePartitionConfigToSerializedEmbedderDownloadData

StoragePartitionConfigToSerializedEmbedderDownloadData
~~~cpp
virtual std::string StoragePartitionConfigToSerializedEmbedderDownloadData(
      const StoragePartitionConfig& storage_partition_config) = 0;
~~~
 Called to convert between a StoragePartitionConfig and a serialized
 proto::EmbedderDownloadData. The serialized proto::EmbedderDownloadData is
 written to the downloads database.

### SerializedEmbedderDownloadDataToStoragePartitionConfig

SerializedEmbedderDownloadDataToStoragePartitionConfig
~~~cpp
virtual StoragePartitionConfig
  SerializedEmbedderDownloadDataToStoragePartitionConfig(
      const std::string& serialized_embedder_download_data) = 0;
~~~

### GetStoragePartitionConfigForSiteUrl

GetStoragePartitionConfigForSiteUrl
~~~cpp
virtual StoragePartitionConfig GetStoragePartitionConfigForSiteUrl(
      const GURL& site_url) = 0;
~~~
 Called to get the proper StoragePartitionConfig that corresponds to the
 given site URL. This method is used in DownloadHistory to convert download
 history entries containing just site URLs to DownloadItem objects that no
 longer use site URL. The download history database is not able to migrate
 away from site URL because it is shared by all platforms, therefore it
 cannot reference StoragePartitionConfig since it is a content class.

 See https://crbug.com/1258193 for more details.

## class Observer
 Interface to implement for observers that wish to be informed of changes
 to the DownloadManager's collection of downloads.

### OnDownloadDropped

OnDownloadDropped
~~~cpp
virtual void OnDownloadDropped(DownloadManager* manager) {}
~~~
 Called when the download manager intercepted a download navigation but
 didn't create the download item. Possible reasons:
 1. |delegate| is null.

 2. |delegate| doesn't allow the download.

### OnManagerInitialized

OnManagerInitialized
~~~cpp
virtual void OnManagerInitialized() {}
~~~
 Called when the download manager has finished loading the data.

### ManagerGoingDown

ManagerGoingDown
~~~cpp
virtual void ManagerGoingDown(DownloadManager* manager) {}
~~~
 Called when the DownloadManager is being destroyed to prevent Observers
 from calling back to a stale pointer.
