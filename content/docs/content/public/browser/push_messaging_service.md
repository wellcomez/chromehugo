
## class PushMessagingService
 A push service-agnostic interface that the Push API uses for talking to
 push messaging services like GCM. Must only be used on the UI thread.

### ~PushMessagingService

~PushMessagingService
~~~cpp
virtual ~PushMessagingService() {}
~~~

### SubscribeFromDocument

PushMessagingService::SubscribeFromDocument
~~~cpp
virtual void SubscribeFromDocument(
      const GURL& requesting_origin,
      int64_t service_worker_registration_id,
      int render_process_id,
      int render_frame_id,
      blink::mojom::PushSubscriptionOptionsPtr options,
      bool user_gesture,
      RegisterCallback callback) = 0;
~~~
 Subscribes the given |options->sender_info| with the push messaging service
 in a document context. The frame is known and a permission UI may be
 displayed to the user. It's safe to call this method multiple times for
 the same registration information, in which case the existing subscription
 will be returned by the server.

### SubscribeFromWorker

PushMessagingService::SubscribeFromWorker
~~~cpp
virtual void SubscribeFromWorker(
      const GURL& requesting_origin,
      int64_t service_worker_registration_id,
      int render_process_id,
      blink::mojom::PushSubscriptionOptionsPtr options,
      RegisterCallback callback) = 0;
~~~
 Subscribes the given |options->sender_info| with the push messaging
 service. The frame is not known so if permission was not previously granted
 by the user this request should fail. It's safe to call this method
 multiple times for the same registration information, in which case the
 existing subscription will be returned by the server.

### GetSubscriptionInfo

PushMessagingService::GetSubscriptionInfo
~~~cpp
virtual void GetSubscriptionInfo(const GURL& origin,
                                   int64_t service_worker_registration_id,
                                   const std::string& sender_id,
                                   const std::string& subscription_id,
                                   SubscriptionInfoCallback callback) = 0;
~~~
 Retrieves the subscription associated with |origin| and
 |service_worker_registration_id|, validates that the provided
 |subscription_id| matches the stored one, then passes the encryption
 information to the callback. |sender_id| is also required since an
 InstanceID might have multiple tokens associated with different senders,
 though in practice Push doesn't yet use that.

### Unsubscribe

PushMessagingService::Unsubscribe
~~~cpp
virtual void Unsubscribe(blink::mojom::PushUnregistrationReason reason,
                           const GURL& requesting_origin,
                           int64_t service_worker_registration_id,
                           const std::string& sender_id,
                           UnregisterCallback callback) = 0;
~~~
 Unsubscribe the given |sender_id| from the push messaging service. Locally
 deactivates the subscription, then runs |callback|, then asynchronously
 attempts to unsubscribe with the push service.

### SupportNonVisibleMessages

PushMessagingService::SupportNonVisibleMessages
~~~cpp
virtual bool SupportNonVisibleMessages() = 0;
~~~
 Returns whether subscriptions that do not mandate user visible UI upon
 receiving a push message are supported. Influences permission request and
 permission check behaviour.

### DidDeleteServiceWorkerRegistration

PushMessagingService::DidDeleteServiceWorkerRegistration
~~~cpp
virtual void DidDeleteServiceWorkerRegistration(
      const GURL& origin,
      int64_t service_worker_registration_id) = 0;
~~~
 Unsubscribes the push subscription associated with this service worker
 registration, if such a push subscription exists.

### DidDeleteServiceWorkerDatabase

PushMessagingService::DidDeleteServiceWorkerDatabase
~~~cpp
virtual void DidDeleteServiceWorkerDatabase() = 0;
~~~
 Unsubscribes all existing push subscriptions because the Service Worker
 database has been deleted.

### GetSWData

PushMessagingService::GetSWData
~~~cpp
static void GetSWData(BrowserContext* browser_context,
                        const GURL& origin,
                        int64_t service_worker_registration_id,
                        SWDataCallback callback);
~~~
 Get |sender_id| and |subscription_id| from Service Worker database. Can be
 empty if no data found in the database.

### ClearPushSubscriptionId

PushMessagingService::ClearPushSubscriptionId
~~~cpp
static void ClearPushSubscriptionId(BrowserContext* browser_context,
                                      const GURL& origin,
                                      int64_t service_worker_registration_id,
                                      base::OnceClosure callback);
~~~
 Clear the push subscription id stored in the service worker with the given
 |service_worker_registration_id| for the given |origin|.

### UpdatePushSubscriptionId

PushMessagingService::UpdatePushSubscriptionId
~~~cpp
static void UpdatePushSubscriptionId(BrowserContext* browser_context,
                                       const GURL& origin,
                                       int64_t service_worker_registration_id,
                                       const std::string& subscription_id,
                                       base::OnceClosure callback);
~~~
 Update |subscription_id| stored in Service Worker database.

### StorePushSubscriptionForTesting

PushMessagingService::StorePushSubscriptionForTesting
~~~cpp
static void StorePushSubscriptionForTesting(
      BrowserContext* browser_context,
      const GURL& origin,
      int64_t service_worker_registration_id,
      const std::string& subscription_id,
      const std::string& sender_id,
      base::OnceClosure callback);
~~~
 Stores a push subscription in the service worker for the given |origin|.

 Must only be used by tests.
