
## class OverscrollConfig

### OverscrollConfig

OverscrollConfig
~~~cpp
OverscrollConfig() = delete;
~~~

### OverscrollConfig

OverscrollConfig
~~~cpp
OverscrollConfig(const OverscrollConfig&) = delete;
~~~

### operator=

OverscrollConfig::operator=
~~~cpp
OverscrollConfig& operator=(const OverscrollConfig&) = delete;
~~~

### GetPullToRefreshMode

OverscrollConfig::GetPullToRefreshMode
~~~cpp
static PullToRefreshMode GetPullToRefreshMode();
~~~

### TouchpadOverscrollHistoryNavigationEnabled

OverscrollConfig::TouchpadOverscrollHistoryNavigationEnabled
~~~cpp
static bool TouchpadOverscrollHistoryNavigationEnabled();
~~~

### MaxInertialEventsBeforeOverscrollCancellation

OverscrollConfig::MaxInertialEventsBeforeOverscrollCancellation
~~~cpp
static base::TimeDelta MaxInertialEventsBeforeOverscrollCancellation();
~~~

### SetPullToRefreshMode

OverscrollConfig::SetPullToRefreshMode
~~~cpp
static void SetPullToRefreshMode(PullToRefreshMode mode);
~~~
 Helper functions used by |ScopedPullToRefreshMode| to set and reset mode in
 tests.

### ResetPullToRefreshMode

OverscrollConfig::ResetPullToRefreshMode
~~~cpp
static void ResetPullToRefreshMode();
~~~

### ResetTouchpadOverscrollHistoryNavigationEnabled

OverscrollConfig::ResetTouchpadOverscrollHistoryNavigationEnabled
~~~cpp
static void ResetTouchpadOverscrollHistoryNavigationEnabled();
~~~
 Helper functions to reset TouchpadOverscrollHistoryNavigationEnabled in
 tests.
