
## struct DesktopMediaID
 Type used to identify desktop media sources. It's converted to string and
 stored in MediaStreamRequest::requested_video_device_id.

### RegisterNativeWindow

DesktopMediaID::RegisterNativeWindow
~~~cpp
static DesktopMediaID RegisterNativeWindow(Type type,
                                             gfx::NativeWindow window);
~~~
 Assigns integer identifier to the |window| and returns its DesktopMediaID.

### GetNativeWindowById

DesktopMediaID::GetNativeWindowById
~~~cpp
static gfx::NativeWindow GetNativeWindowById(const DesktopMediaID& id);
~~~
 Returns the Window that was previously registered using
 RegisterNativeWindow(), else nullptr.

### DesktopMediaID

DesktopMediaID
~~~cpp
constexpr DesktopMediaID() = default;
~~~
 defined(USE_AURA) || BUILDFLAG(IS_MAC)
### DesktopMediaID

DesktopMediaID
~~~cpp
constexpr DesktopMediaID(Type type, Id id) : type(type), id(id) {}
~~~

### DesktopMediaID

DesktopMediaID
~~~cpp
constexpr DesktopMediaID(Type type,
                           Id id,
                           WebContentsMediaCaptureId web_contents_id)
      : type(type), id(id), web_contents_id(web_contents_id) {}
~~~

### DesktopMediaID

DesktopMediaID
~~~cpp
constexpr DesktopMediaID(Type type, Id id, bool audio_share)
      : type(type), id(id), audio_share(audio_share) {}
~~~

### operator&lt;

DesktopMediaID::operator&lt;
~~~cpp
bool operator<(const DesktopMediaID& other) const;
~~~
 Operators so that DesktopMediaID can be used with STL containers.

### operator==

DesktopMediaID::operator==
~~~cpp
bool operator==(const DesktopMediaID& other) const;
~~~

### operator!=

DesktopMediaID::operator!=
~~~cpp
bool operator!=(const DesktopMediaID& other) const;
~~~

### is_null

is_null
~~~cpp
bool is_null() const { return type == TYPE_NONE; }
~~~

### ToString

DesktopMediaID::ToString
~~~cpp
std::string ToString() const;
~~~

### Parse

DesktopMediaID::Parse
~~~cpp
static DesktopMediaID Parse(const std::string& str);
~~~
