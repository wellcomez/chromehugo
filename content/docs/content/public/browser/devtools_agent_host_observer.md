
## class DevToolsAgentHostObserver
 Observer API notifies interested parties about changes in DevToolsAgentHosts.

### ShouldForceDevToolsAgentHostCreation

DevToolsAgentHostObserver::ShouldForceDevToolsAgentHostCreation
~~~cpp
virtual bool ShouldForceDevToolsAgentHostCreation();
~~~
 If observer returns |true|, DevToolsAgentHost instances are created
 (and reported in DevToolsAgentHostCreated) for every possible devtools
 target (e.g. WebContents).

### DevToolsAgentHostCreated

DevToolsAgentHostObserver::DevToolsAgentHostCreated
~~~cpp
virtual void DevToolsAgentHostCreated(DevToolsAgentHost* agent_host);
~~~
 Called when DevToolsAgentHost was created and is ready to be used.

### DevToolsAgentHostNavigated

DevToolsAgentHostObserver::DevToolsAgentHostNavigated
~~~cpp
virtual void DevToolsAgentHostNavigated(DevToolsAgentHost* agent_host);
~~~
 Called when DevToolsAgentHost was created and is ready to be used.

### DevToolsAgentHostAttached

DevToolsAgentHostObserver::DevToolsAgentHostAttached
~~~cpp
virtual void DevToolsAgentHostAttached(DevToolsAgentHost* agent_host);
~~~
 Called when client has attached to DevToolsAgentHost.

### DevToolsAgentHostDetached

DevToolsAgentHostObserver::DevToolsAgentHostDetached
~~~cpp
virtual void DevToolsAgentHostDetached(DevToolsAgentHost* agent_host);
~~~
 Called when client has detached from DevToolsAgentHost.

### DevToolsAgentHostCrashed

DevToolsAgentHostObserver::DevToolsAgentHostCrashed
~~~cpp
virtual void DevToolsAgentHostCrashed(DevToolsAgentHost* agent_host,
                                        base::TerminationStatus status);
~~~
 Called when DevToolsAgentHost crashed.

### DevToolsAgentHostDestroyed

DevToolsAgentHostObserver::DevToolsAgentHostDestroyed
~~~cpp
virtual void DevToolsAgentHostDestroyed(DevToolsAgentHost* agent_host);
~~~
 Called when DevToolsAgentHost was destroyed.
