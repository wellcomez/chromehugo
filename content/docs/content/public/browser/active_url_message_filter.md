
## class ActiveUrlMessageFilter : public
 Message filter which sets the active URL for crash reporting while a mojo
 message is being handled.


 TODO(csharrison): Move out of the internal namespace if callers outside of
 //content are found. This is only exposed in //content/public because it is
 used by the RenderFrameHostReceiverSet which is a header-only class.

### ~ActiveUrlMessageFilter

ActiveUrlMessageFilter : public::~ActiveUrlMessageFilter
~~~cpp
~ActiveUrlMessageFilter() override
~~~

### WillDispatch

ActiveUrlMessageFilter : public::WillDispatch
~~~cpp
bool WillDispatch(mojo::Message* message) override;
~~~
 mojo::MessageFilter overrides.

### DidDispatchOrReject

ActiveUrlMessageFilter : public::DidDispatchOrReject
~~~cpp
void DidDispatchOrReject(mojo::Message* message, bool accepted) override;
~~~
