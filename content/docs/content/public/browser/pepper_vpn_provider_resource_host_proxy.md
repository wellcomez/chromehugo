
## class PepperVpnProviderResourceHostProxy
 Describes interface for communication with the VpnProviderResouceHost.

### SendOnUnbind

PepperVpnProviderResourceHostProxy::SendOnUnbind
~~~cpp
virtual void SendOnUnbind() = 0;
~~~
 Passes an Unbind event to the VpnProviderResouceHost.

### SendOnPacketReceived

PepperVpnProviderResourceHostProxy::SendOnPacketReceived
~~~cpp
virtual void SendOnPacketReceived(const std::vector<char>& data) = 0;
~~~
 Sends an IP packet to the VpnProviderResouceHost.
