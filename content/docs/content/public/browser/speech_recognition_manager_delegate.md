
## class SpeechRecognitionManagerDelegate
 Allows embedders to display the current state of recognition, for getting the
 user's permission and for fetching optional request information.

### ~SpeechRecognitionManagerDelegate

~SpeechRecognitionManagerDelegate
~~~cpp
virtual ~SpeechRecognitionManagerDelegate() {}
~~~

### CheckRecognitionIsAllowed

CheckRecognitionIsAllowed
~~~cpp
virtual void CheckRecognitionIsAllowed(
      int session_id,
      base::OnceCallback<void(bool ask_user, bool is_allowed)> callback) = 0;
~~~
 Checks (asynchronously) if current setup allows speech recognition.

 This is called on the IO thread.

### GetEventListener

GetEventListener
~~~cpp
virtual SpeechRecognitionEventListener* GetEventListener() = 0;
~~~
 Checks whether the delegate is interested (returning a non nullptr ptr) or
 not (returning nullptr) in receiving a copy of all sessions events.

 This is called on the IO thread.

### FilterProfanities

FilterProfanities
~~~cpp
virtual bool FilterProfanities(int render_process_id) = 0;
~~~
 Checks whether the speech recognition for the given renderer should filter
 profanities or not.

### ~SpeechRecognitionManagerDelegate

~SpeechRecognitionManagerDelegate
~~~cpp
virtual ~SpeechRecognitionManagerDelegate() {}
~~~

### CheckRecognitionIsAllowed

CheckRecognitionIsAllowed
~~~cpp
virtual void CheckRecognitionIsAllowed(
      int session_id,
      base::OnceCallback<void(bool ask_user, bool is_allowed)> callback) = 0;
~~~
 Checks (asynchronously) if current setup allows speech recognition.

 This is called on the IO thread.

### GetEventListener

GetEventListener
~~~cpp
virtual SpeechRecognitionEventListener* GetEventListener() = 0;
~~~
 Checks whether the delegate is interested (returning a non nullptr ptr) or
 not (returning nullptr) in receiving a copy of all sessions events.

 This is called on the IO thread.

### FilterProfanities

FilterProfanities
~~~cpp
virtual bool FilterProfanities(int render_process_id) = 0;
~~~
 Checks whether the speech recognition for the given renderer should filter
 profanities or not.
