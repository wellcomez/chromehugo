
## class PluginServiceFilter
 Callback class to let the client filter the list of all installed plugins
 and block them from being loaded.

 This class is called on the UI thread.

### ~PluginServiceFilter

~PluginServiceFilter
~~~cpp
virtual ~PluginServiceFilter() = default;
~~~

### IsPluginAvailable

IsPluginAvailable
~~~cpp
virtual bool IsPluginAvailable(content::BrowserContext* browser_context,
                                 const WebPluginInfo& plugin) = 0;
~~~
 Whether `plugin` is available. The client can return false to hide the
 plugin. The result may be cached, and should be consistent between calls.

### CanLoadPlugin

CanLoadPlugin
~~~cpp
virtual bool CanLoadPlugin(int render_process_id,
                             const base::FilePath& path) = 0;
~~~
 Whether the renderer has permission to load available `plugin`.

### ~PluginServiceFilter

~PluginServiceFilter
~~~cpp
virtual ~PluginServiceFilter() = default;
~~~

### IsPluginAvailable

IsPluginAvailable
~~~cpp
virtual bool IsPluginAvailable(content::BrowserContext* browser_context,
                                 const WebPluginInfo& plugin) = 0;
~~~
 Whether `plugin` is available. The client can return false to hide the
 plugin. The result may be cached, and should be consistent between calls.

### CanLoadPlugin

CanLoadPlugin
~~~cpp
virtual bool CanLoadPlugin(int render_process_id,
                             const base::FilePath& path) = 0;
~~~
 Whether the renderer has permission to load available `plugin`.
