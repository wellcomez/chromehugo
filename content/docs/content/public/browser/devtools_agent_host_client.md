
## class DevToolsAgentHostClient
 DevToolsAgentHostClient can attach to a DevToolsAgentHost and start
 debugging it.

### DispatchProtocolMessage

DevToolsAgentHostClient::DispatchProtocolMessage
~~~cpp
virtual void DispatchProtocolMessage(DevToolsAgentHost* agent_host,
                                       base::span<const uint8_t> message) = 0;
~~~
 Dispatches given protocol message on the client.

### AgentHostClosed

DevToolsAgentHostClient::AgentHostClosed
~~~cpp
virtual void AgentHostClosed(DevToolsAgentHost* agent_host) = 0;
~~~
 This method is called when attached agent host is closed.

### MayAttachToURL

DevToolsAgentHostClient::MayAttachToURL
~~~cpp
virtual bool MayAttachToURL(const GURL& url, bool is_webui);
~~~
 Returns true if the client is allowed to attach to the given URL.

 Note: this method may be called before navigation commits.

### MayAttachToRenderFrameHost

DevToolsAgentHostClient::MayAttachToRenderFrameHost
~~~cpp
virtual bool MayAttachToRenderFrameHost(RenderFrameHost* render_frame_host);
~~~
 Returns true if the client is allowed to attach to the given
 RenderFrameHost.

### IsTrusted

DevToolsAgentHostClient::IsTrusted
~~~cpp
virtual bool IsTrusted();
~~~
 Returns true if the client is considered to be in the same trust domain
 from security perspective. It implies that the client is allowed to attach
 to the browser agent host and perform other privileged operations. Browser
 client is allowed to discover other DevTools targets and generally
 manipulate browser altogether.

### MayReadLocalFiles

DevToolsAgentHostClient::MayReadLocalFiles
~~~cpp
virtual bool MayReadLocalFiles();
~~~
 Returns true if the client is allowed to read local files over the
 protocol. Example would be exposing file content to the page under debug.

### MayWriteLocalFiles

DevToolsAgentHostClient::MayWriteLocalFiles
~~~cpp
virtual bool MayWriteLocalFiles();
~~~
 Returns true if the client is allowed to write local files over the
 protocol. Example would be manipulating a deault downloads path.

### AllowUnsafeOperations

DevToolsAgentHostClient::AllowUnsafeOperations
~~~cpp
virtual bool AllowUnsafeOperations();
~~~
 Returns true if the client is allowed to perform operations
 they may potentially be used to gain privileges, e.g. providing
 JS compilation cache entries. This should only be true for clients
 that are already privileged, such as local automation clients.

### GetNavigationInitiatorOrigin

DevToolsAgentHostClient::GetNavigationInitiatorOrigin
~~~cpp
virtual absl::optional<url::Origin> GetNavigationInitiatorOrigin();
~~~
 A value to use as NavigationController::LoadURLParams::initiator_origin.

 If set, navigations would also be treated as renderer-initiated.

 This is useful e.g. for Chrome Extensions so that their calls to
 Page.navigate would be treated as renderer-initiated naviation subject to
 URL spoofing protection.

### UsesBinaryProtocol

DevToolsAgentHostClient::UsesBinaryProtocol
~~~cpp
virtual bool UsesBinaryProtocol();
~~~
 Determines protocol message format.

### GetTypeForMetrics

DevToolsAgentHostClient::GetTypeForMetrics
~~~cpp
virtual std::string GetTypeForMetrics();
~~~
 Returns "DevTools" | "Extension" | "RemoteDebugger" | "Other", which is
 used to emit to the correct UMA histogram.
