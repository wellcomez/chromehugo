
## class InstalledPaymentAppsFinder
 This is helper class for retrieving installed payment apps.

 The instance of this class can be retrieved using the static
 GetInstance() method. All methods must be called on the UI thread.

 Example:
    base::WeakPtr<InstalledPaymentAppsFinder> finder =
      content::InstalledPaymentAppsFinder::GetInstance(context);
### GetAllPaymentApps

InstalledPaymentAppsFinder::GetAllPaymentApps
~~~cpp
virtual void GetAllPaymentApps(GetAllPaymentAppsCallback callback) = 0;
~~~
 This method is used to query all registered payment apps with payment
 instruments in browser side. When merchant site requests payment to browser
 via PaymentRequest API, then the UA will display the all proper stored
 payment instruments by this method.
