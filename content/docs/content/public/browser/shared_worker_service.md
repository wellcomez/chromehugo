### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(Observer* observer) = 0;
~~~
 Adds/removes an observer.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(Observer* observer) = 0;
~~~

### EnumerateSharedWorkers

EnumerateSharedWorkers
~~~cpp
virtual void EnumerateSharedWorkers(Observer* observer) = 0;
~~~
 Invokes OnWorkerCreated() on |observer| for all existing shared workers.


 This function must be invoked in conjunction with AddObserver(). It is
 meant to be used by an observer that dynamically subscribe to the
 SharedWorkerService while some workers already exist. It avoids
 receiving a OnBeforeWorkerDestroyed() event without having received the
 corresponding OnWorkerCreated() event.


 Note: Due to current callers not needing it, this function does NOT call
       OnClientAdded() for each worker's clients.

### TerminateWorker

TerminateWorker
~~~cpp
virtual bool TerminateWorker(const GURL& url,
                               const std::string& name,
                               const blink::StorageKey& storage_key) = 0;
~~~
 Terminates the given shared worker identified by its name, the URL of its
 main script resource, and the storage key.  Returns true on success.

### Shutdown

Shutdown
~~~cpp
virtual void Shutdown() = 0;
~~~
 Drops all shared workers and references to processes for shared workers
 synchronously.

## class SharedWorkerService
 An interface for managing shared workers. These may be run in a separate
 process, since multiple renderer processes can be talking to a single shared
 worker. All the methods below can only be called on the UI thread.

### OnBeforeWorkerDestroyed

SharedWorkerService::OnBeforeWorkerDestroyed
~~~cpp
virtual void OnBeforeWorkerDestroyed(
        const blink::SharedWorkerToken& token) = 0;
~~~

### OnFinalResponseURLDetermined

OnFinalResponseURLDetermined
~~~cpp
virtual void OnFinalResponseURLDetermined(
        const blink::SharedWorkerToken& token,
        const GURL& url) {}
~~~
 Called when the final response URL (the URL after redirects) was
 determined when fetching the worker's script.


 TODO(pmonette): Implement this in derived classes and make it pure.

### OnClientAdded

SharedWorkerService::OnClientAdded
~~~cpp
virtual void OnClientAdded(
        const blink::SharedWorkerToken& token,
        content::GlobalRenderFrameHostId render_frame_host_id) = 0;
~~~
 Called when a frame starts/stop being a client of a shared worker. It is
 guaranteed that OnWorkerCreated() is called before receiving these
 notifications.

### OnClientRemoved

SharedWorkerService::OnClientRemoved
~~~cpp
virtual void OnClientRemoved(
        const blink::SharedWorkerToken& token,
        content::GlobalRenderFrameHostId render_frame_host_id) = 0;
~~~
