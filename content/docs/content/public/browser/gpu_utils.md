### gpu::GpuPreferences GetGpuPreferencesFromCommandLine

gpu::GpuPreferences GetGpuPreferencesFromCommandLine
~~~cpp
CONTENT_EXPORT const gpu::GpuPreferences GetGpuPreferencesFromCommandLine();
~~~

### KillGpuProcess

KillGpuProcess
~~~cpp
CONTENT_EXPORT void KillGpuProcess();
~~~
 Kills the GPU process with a normal termination status.

### GetGpuChannelEstablishFactory

GetGpuChannelEstablishFactory
~~~cpp
CONTENT_EXPORT gpu::GpuChannelEstablishFactory* GetGpuChannelEstablishFactory();
~~~

### DumpGpuProfilingData

DumpGpuProfilingData
~~~cpp
CONTENT_EXPORT void DumpGpuProfilingData(base::OnceClosure callback);
~~~

