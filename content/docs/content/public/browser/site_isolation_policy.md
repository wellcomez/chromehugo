
## class SiteIsolationPolicy
 A centralized place for making policy decisions about out-of-process iframes,
 site isolation, --site-per-process, and related features.


 This is currently static because all these modes are controlled by command-
 line flags or field trials.


 Unless otherwise stated, these methods can be called from any thread.

### operator=

SiteIsolationPolicy::operator=
~~~cpp
SiteIsolationPolicy& operator=(const SiteIsolationPolicy&) = delete;
~~~

### UseDedicatedProcessesForAllSites

SiteIsolationPolicy::UseDedicatedProcessesForAllSites
~~~cpp
static bool UseDedicatedProcessesForAllSites();
~~~
 Returns true if every site should be placed in a dedicated process.

### AreIsolatedSandboxedIframesEnabled

SiteIsolationPolicy::AreIsolatedSandboxedIframesEnabled
~~~cpp
static bool AreIsolatedSandboxedIframesEnabled();
~~~
 Returns true if sandboxed iframes should be isolated.

### AreIsolatedOriginsEnabled

SiteIsolationPolicy::AreIsolatedOriginsEnabled
~~~cpp
static bool AreIsolatedOriginsEnabled();
~~~
 Returns true if isolated origins feature is enabled.

### IsStrictOriginIsolationEnabled

SiteIsolationPolicy::IsStrictOriginIsolationEnabled
~~~cpp
static bool IsStrictOriginIsolationEnabled();
~~~
 Returns true if strict origin isolation is enabled. Controls whether site
 isolation uses origins instead of scheme and eTLD+1.

### IsErrorPageIsolationEnabled

SiteIsolationPolicy::IsErrorPageIsolationEnabled
~~~cpp
static bool IsErrorPageIsolationEnabled(bool in_main_frame);
~~~
 Returns true if error page isolation is enabled.

### AreDynamicIsolatedOriginsEnabled

SiteIsolationPolicy::AreDynamicIsolatedOriginsEnabled
~~~cpp
static bool AreDynamicIsolatedOriginsEnabled();
~~~
 Returns true if isolated origins may be added at runtime in response
 to hints such as users typing in a password or sites serving headers like
 Cross-Origin-Opener-Policy.

### ArePreloadedIsolatedOriginsEnabled

SiteIsolationPolicy::ArePreloadedIsolatedOriginsEnabled
~~~cpp
static bool ArePreloadedIsolatedOriginsEnabled();
~~~
 Returns true if isolated origins preloaded with the browser should be
 applied.  For example, this is used to apply memory limits to preloaded
 isolated origins on Android.

### IsProcessIsolationForOriginAgentClusterEnabled

SiteIsolationPolicy::IsProcessIsolationForOriginAgentClusterEnabled
~~~cpp
static bool IsProcessIsolationForOriginAgentClusterEnabled();
~~~
 Returns true if the "Origin-Agent-Cluster" header should result in a
 separate process for isolated origins.  This is used to turn off opt-in
 origin isolation on low-memory Android devices.

### IsOriginAgentClusterEnabled

SiteIsolationPolicy::IsOriginAgentClusterEnabled
~~~cpp
static bool IsOriginAgentClusterEnabled();
~~~
 Returns true if the OriginAgentCluster header will be respected.

### AreOriginAgentClustersEnabledByDefault

SiteIsolationPolicy::AreOriginAgentClustersEnabledByDefault
~~~cpp
static bool AreOriginAgentClustersEnabledByDefault(
      BrowserContext* browser_context);
~~~
 Returns whether defaulting to origin-keyed agent cluster (without
 necessarily an origin-keyed process) is enabled.

 OriginAgentClusters are enabled by default if kOriginIsolationHeader and
 kOriginAgentClusterDefaultEnabled are enabled, and if there is no
 enterprise policy forbidding it.

### IsSiteIsolationForCOOPEnabled

SiteIsolationPolicy::IsSiteIsolationForCOOPEnabled
~~~cpp
static bool IsSiteIsolationForCOOPEnabled();
~~~
 Returns true if Cross-Origin-Opener-Policy headers may be used as
 heuristics for turning on site isolation.

### ShouldPersistIsolatedCOOPSites

SiteIsolationPolicy::ShouldPersistIsolatedCOOPSites
~~~cpp
static bool ShouldPersistIsolatedCOOPSites();
~~~
 Return true if sites that were isolated due to COOP headers should be
 persisted across restarts.

### IsSiteIsolationForGuestsEnabled

SiteIsolationPolicy::IsSiteIsolationForGuestsEnabled
~~~cpp
static bool IsSiteIsolationForGuestsEnabled();
~~~
 Returns true when site isolation is turned on for <webview> guests.

### ApplyGlobalIsolatedOrigins

SiteIsolationPolicy::ApplyGlobalIsolatedOrigins
~~~cpp
static void ApplyGlobalIsolatedOrigins();
~~~
 Applies isolated origins from all available sources, including the
 command-line switch, field trials, enterprise policy, and the embedder.

 See also AreIsolatedOriginsEnabled. These origins apply globally to the
 whole browser in all profiles.  This should be called once on browser
 startup.

### ShouldUrlUseApplicationIsolationLevel

SiteIsolationPolicy::ShouldUrlUseApplicationIsolationLevel
~~~cpp
static bool ShouldUrlUseApplicationIsolationLevel(
      BrowserContext* browser_context,
      const GURL& url);
~~~
 Returns true if the given URL should be assigned the application isolation
 level.

 This must be called on the UI thread.

### DisableFlagCachingForTesting

SiteIsolationPolicy::DisableFlagCachingForTesting
~~~cpp
static void DisableFlagCachingForTesting();
~~~
 Forces other methods in this class to reread flag values instead of using
 their cached value.

### IsProcessIsolationForFencedFramesEnabled

SiteIsolationPolicy::IsProcessIsolationForFencedFramesEnabled
~~~cpp
static bool IsProcessIsolationForFencedFramesEnabled();
~~~
 Returns true when process-isolation of fenced frames from their embedders
 is enabled.

### GetIsolatedOriginsFromCommandLine

SiteIsolationPolicy::GetIsolatedOriginsFromCommandLine
~~~cpp
static std::string GetIsolatedOriginsFromCommandLine();
~~~
 Not instantiable.

 Gets isolated origins from cmdline and/or from field trial param.

### GetIsolatedOriginsFromFieldTrial

SiteIsolationPolicy::GetIsolatedOriginsFromFieldTrial
~~~cpp
static std::string GetIsolatedOriginsFromFieldTrial();
~~~
