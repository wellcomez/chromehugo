
## struct SpeechRecognitionSessionConfig
 Configuration params for creating a new speech recognition session.

### SpeechRecognitionSessionConfig

SpeechRecognitionSessionConfig::SpeechRecognitionSessionConfig
~~~cpp
SpeechRecognitionSessionConfig(const SpeechRecognitionSessionConfig& other)
~~~

### ~SpeechRecognitionSessionConfig

SpeechRecognitionSessionConfig::~SpeechRecognitionSessionConfig
~~~cpp
~SpeechRecognitionSessionConfig()
~~~
