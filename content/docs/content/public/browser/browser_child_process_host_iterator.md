
## class BrowserChildProcessHostIterator
 This class allows iteration through either all child processes, or ones of a
 specific type, depending on which constructor is used.  Note that this should
 be done from the IO thread and that the iterator should not be kept around as
 it may be invalidated on subsequent event processing in the event loop.

### BrowserChildProcessHostIterator

BrowserChildProcessHostIterator::BrowserChildProcessHostIterator
~~~cpp
BrowserChildProcessHostIterator(int type)
~~~

### ~BrowserChildProcessHostIterator

BrowserChildProcessHostIterator::~BrowserChildProcessHostIterator
~~~cpp
~BrowserChildProcessHostIterator()
~~~

### operator++

BrowserChildProcessHostIterator::operator++
~~~cpp
bool operator++();
~~~
 These methods work on the current iterator object. Only call them if
 Done() returns false.

### Done

BrowserChildProcessHostIterator::Done
~~~cpp
bool Done();
~~~

### GetData

BrowserChildProcessHostIterator::GetData
~~~cpp
const ChildProcessData& GetData();
~~~

### Send

BrowserChildProcessHostIterator::Send
~~~cpp
bool Send(IPC::Message* message);
~~~

### GetDelegate

BrowserChildProcessHostIterator::GetDelegate
~~~cpp
BrowserChildProcessHostDelegate* GetDelegate();
~~~

### GetHost

BrowserChildProcessHostIterator::GetHost
~~~cpp
ChildProcessHost* GetHost();
~~~

## class BrowserChildProcessHostTypeIterator

### operator-&gt;

operator-&gt;
~~~cpp
T* operator->() {
    return static_cast<T*>(GetDelegate());
  }
~~~

### operator*

operator*
~~~cpp
T* operator*() {
    return static_cast<T*>(GetDelegate());
  }
~~~
