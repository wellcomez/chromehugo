### GetProcessLauncherTaskRunner

GetProcessLauncherTaskRunner
~~~cpp
CONTENT_EXPORT base::SingleThreadTaskRunner* GetProcessLauncherTaskRunner();
~~~
 The caller must take a reference to the returned TaskRunner pointer if it
 wants to use the pointer directly.

### CurrentlyOnProcessLauncherTaskRunner

CurrentlyOnProcessLauncherTaskRunner
~~~cpp
CONTENT_EXPORT bool CurrentlyOnProcessLauncherTaskRunner();
~~~

