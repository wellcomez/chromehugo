### GetContextFactory

GetContextFactory
~~~cpp
CONTENT_EXPORT ui::ContextFactory* GetContextFactory();
~~~
 Returns the singleton ContextFactory used by content. The return value is
 owned by content.

