### OpenTCPFirewallHole

OpenTCPFirewallHole
~~~cpp
CONTENT_EXPORT void OpenTCPFirewallHole(const std::string& interface,
                         uint16_t port,
                         FirewallHoleProxy::OpenCallback callback);
~~~
 Opens a TCP port on the system firewall for the given |interface|
 (or all interfaces if |interface| is ""). On success invokes the callback
 with a valid FirewallHoleProxy; on failure supplies nullptr.

### OpenUDPFirewallHole

OpenUDPFirewallHole
~~~cpp
CONTENT_EXPORT void OpenUDPFirewallHole(const std::string& interface,
                         uint16_t port,
                         FirewallHoleProxy::OpenCallback callback);
~~~
 Opens a UDP port on the system firewall for the given |interface|
 (or all interfaces if |interface| is ""). On success invokes the callback
 with a valid FirewallHoleProxy; on failure supplies nullptr.

## class FirewallHoleProxy
 FirewallHoleProxy represents a hole in the ChromeOS system firewall.

 When this class gets destroyed, it's supposed the close the corresponding
 firewall hole.

### ~FirewallHoleProxy

~FirewallHoleProxy
~~~cpp
virtual ~FirewallHoleProxy() = default;
~~~
