
## struct AXEventNotificationDetails
 Use this object in conjunction with the
 |WebContentsObserver::AccessibilityEventReceived| method.

### AXEventNotificationDetails

AXEventNotificationDetails::AXEventNotificationDetails
~~~cpp
AXEventNotificationDetails(const AXEventNotificationDetails& other)
~~~

### ~AXEventNotificationDetails

AXEventNotificationDetails::~AXEventNotificationDetails
~~~cpp
~AXEventNotificationDetails()
~~~

## struct AXLocationChangeNotificationDetails
 Use this object in conjunction with the
 |WebContentsObserver::AccessibilityLocationChangeReceived| method.

### AXLocationChangeNotificationDetails

AXLocationChangeNotificationDetails::AXLocationChangeNotificationDetails
~~~cpp
AXLocationChangeNotificationDetails(
      const AXLocationChangeNotificationDetails& other)
~~~

### ~AXLocationChangeNotificationDetails

AXLocationChangeNotificationDetails::~AXLocationChangeNotificationDetails
~~~cpp
~AXLocationChangeNotificationDetails()
~~~
