
## struct NotificationResourceData
 Contains the |resources| for a notification with |notification_id| and
 |origin|. Used to pass multiple resources to the PlatformNotificationContext.

### NotificationResourceData

NotificationResourceData
~~~cpp
NotificationResourceData(std::string notification_id,
                           GURL origin,
                           blink::NotificationResources resources)
      : notification_id(std::move(notification_id)),
        origin(std::move(origin)),
        resources(std::move(resources)) {}
~~~
