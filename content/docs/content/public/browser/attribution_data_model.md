### ~AttributionDataModel

~AttributionDataModel
~~~cpp
virtual ~AttributionDataModel() = default;
~~~

### GetAllDataKeys

GetAllDataKeys
~~~cpp
virtual void GetAllDataKeys(
      base::OnceCallback<void(std::vector<DataKey>)> callback) = 0;
~~~

### RemoveAttributionDataByDataKey

RemoveAttributionDataByDataKey
~~~cpp
virtual void RemoveAttributionDataByDataKey(const DataKey& data_key,
                                              base::OnceClosure callback) = 0;
~~~

## class AttributionDataModel

### DataKey

AttributionDataModel::DataKey
~~~cpp
DataKey(const DataKey&)
~~~

### DataKey

AttributionDataModel::DataKey
~~~cpp
DataKey(DataKey&&)
~~~

### operator=

AttributionDataModel::operator=
~~~cpp
DataKey& operator=(const DataKey&);
~~~

### operator=

AttributionDataModel::operator=
~~~cpp
DataKey& operator=(DataKey&&);
~~~

### ~DataKey

AttributionDataModel::~DataKey
~~~cpp
~DataKey()
~~~

### reporting_origin

reporting_origin
~~~cpp
const url::Origin& reporting_origin() const { return reporting_origin_; }
~~~

### operator&lt;

AttributionDataModel::operator&lt;
~~~cpp
bool operator<(const DataKey&) const;
~~~

### operator==

AttributionDataModel::operator==
~~~cpp
bool operator==(const DataKey&) const;
~~~
