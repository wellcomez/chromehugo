
## class BackgroundTracingManager::EnabledStateTestObserver
 Enabled state observers get a callback when the state of background tracing
 changes.

### OnTracingEnabled

EnabledStateTestObserver::BackgroundTracingManager::OnTracingEnabled
~~~cpp
virtual void OnTracingEnabled() = 0;
~~~
 Called after tracing is enabled on all processes because the rule was
 triggered.

## class BackgroundTracingManager::EnabledStateTestObserver
 Enabled state observers get a callback when the state of background tracing
 changes.

### OnTracingEnabled

EnabledStateTestObserver::BackgroundTracingManager::OnTracingEnabled
~~~cpp
virtual void OnTracingEnabled() = 0;
~~~
 Called after tracing is enabled on all processes because the rule was
 triggered.

## class BackgroundTracingManager
 BackgroundTracingManager is used on the browser process to trigger the
 collection of trace data and upload the results. Only the browser UI thread
 is allowed to interact with the BackgroundTracingManager. All callbacks are
 called on the UI thread.

###  GetInstance


~~~cpp
CONTENT_EXPORT static BackgroundTracingManager& GetInstance();
~~~
### field error



~~~cpp

CONTENT_EXPORT static const char kContentTriggerConfig[];

~~~



### enum DataFiltering

~~~cpp
enum DataFiltering {
    NO_DATA_FILTERING,
    ANONYMIZE_DATA,
  }
~~~
### SetActiveScenario

SetActiveScenario
~~~cpp
virtual bool SetActiveScenario(
      std::unique_ptr<BackgroundTracingConfig> config,
      DataFiltering data_filtering) = 0;
~~~

### SetActiveScenarioWithReceiveCallback

SetActiveScenarioWithReceiveCallback
~~~cpp
virtual bool SetActiveScenarioWithReceiveCallback(
      std::unique_ptr<BackgroundTracingConfig> config,
      ReceiveCallback receive_callback,
      DataFiltering data_filtering) = 0;
~~~
 Identical to SetActiveScenario except that whenever a trace is finalized,
 BackgroundTracingManager calls `receive_callback` to upload the trace.

 `local_output` should be true if `receive_callback` saves the trace
 locally (such as for testing), false if `receive_callback` uploads the
 trace to a server.

### WhenIdle

WhenIdle
~~~cpp
virtual void WhenIdle(IdleCallback idle_callback) = 0;
~~~

### EmitNamedTrigger

EmitNamedTrigger
~~~cpp
virtual bool EmitNamedTrigger(const std::string& trigger_name) = 0;
~~~
 Notifies that a manual trigger event has occurred. Returns true if the
 trigger caused a scenario to either begin recording or finalize the trace
 depending on the config, or false if the trigger had no effect. If the
 trigger specified isn't active in the config, this will do nothing.

### HasActiveScenario

HasActiveScenario
~~~cpp
virtual bool HasActiveScenario() = 0;
~~~

### HasTraceToUpload

HasTraceToUpload
~~~cpp
virtual bool HasTraceToUpload() = 0;
~~~
 Returns true whether a trace is ready to be uploaded.

### GetLatestTraceToUpload

GetLatestTraceToUpload
~~~cpp
virtual std::string GetLatestTraceToUpload() = 0;
~~~
 Returns the latest trace created for uploading in a serialized proto of
 message type perfetto::Trace.

 TODO(ssid): This should also return the trigger for the trace along with
 the serialized trace proto.

### GetBackgroundTracingConfig

GetBackgroundTracingConfig
~~~cpp
virtual std::unique_ptr<BackgroundTracingConfig> GetBackgroundTracingConfig(
      const std::string& trial_name) = 0;
~~~
 Returns background tracing configuration for the experiment |trial_name|.

### AbortScenarioForTesting

AbortScenarioForTesting
~~~cpp
virtual void AbortScenarioForTesting() = 0;
~~~
 For tests
### SetTraceToUploadForTesting

SetTraceToUploadForTesting
~~~cpp
virtual void SetTraceToUploadForTesting(
      std::unique_ptr<std::string> trace_data) = 0;
~~~

### SetConfigTextFilterForTesting

SetConfigTextFilterForTesting
~~~cpp
virtual void SetConfigTextFilterForTesting(
      ConfigTextFilterForTesting predicate) = 0;
~~~
 Sets a callback to override the background tracing config for testing.

### ~BackgroundTracingManager

~BackgroundTracingManager
~~~cpp
virtual ~BackgroundTracingManager() {}
~~~

### SetActiveScenario

SetActiveScenario
~~~cpp
virtual bool SetActiveScenario(
      std::unique_ptr<BackgroundTracingConfig> config,
      DataFiltering data_filtering) = 0;
~~~

### SetActiveScenarioWithReceiveCallback

SetActiveScenarioWithReceiveCallback
~~~cpp
virtual bool SetActiveScenarioWithReceiveCallback(
      std::unique_ptr<BackgroundTracingConfig> config,
      ReceiveCallback receive_callback,
      DataFiltering data_filtering) = 0;
~~~
 Identical to SetActiveScenario except that whenever a trace is finalized,
 BackgroundTracingManager calls `receive_callback` to upload the trace.

 `local_output` should be true if `receive_callback` saves the trace
 locally (such as for testing), false if `receive_callback` uploads the
 trace to a server.

### WhenIdle

WhenIdle
~~~cpp
virtual void WhenIdle(IdleCallback idle_callback) = 0;
~~~

### EmitNamedTrigger

EmitNamedTrigger
~~~cpp
virtual bool EmitNamedTrigger(const std::string& trigger_name) = 0;
~~~
 Notifies that a manual trigger event has occurred. Returns true if the
 trigger caused a scenario to either begin recording or finalize the trace
 depending on the config, or false if the trigger had no effect. If the
 trigger specified isn't active in the config, this will do nothing.

### HasActiveScenario

HasActiveScenario
~~~cpp
virtual bool HasActiveScenario() = 0;
~~~

### HasTraceToUpload

HasTraceToUpload
~~~cpp
virtual bool HasTraceToUpload() = 0;
~~~
 Returns true whether a trace is ready to be uploaded.

### GetLatestTraceToUpload

GetLatestTraceToUpload
~~~cpp
virtual std::string GetLatestTraceToUpload() = 0;
~~~
 Returns the latest trace created for uploading in a serialized proto of
 message type perfetto::Trace.

 TODO(ssid): This should also return the trigger for the trace along with
 the serialized trace proto.

### GetBackgroundTracingConfig

GetBackgroundTracingConfig
~~~cpp
virtual std::unique_ptr<BackgroundTracingConfig> GetBackgroundTracingConfig(
      const std::string& trial_name) = 0;
~~~
 Returns background tracing configuration for the experiment |trial_name|.

### AbortScenarioForTesting

AbortScenarioForTesting
~~~cpp
virtual void AbortScenarioForTesting() = 0;
~~~
 For tests
### SetTraceToUploadForTesting

SetTraceToUploadForTesting
~~~cpp
virtual void SetTraceToUploadForTesting(
      std::unique_ptr<std::string> trace_data) = 0;
~~~

### SetConfigTextFilterForTesting

SetConfigTextFilterForTesting
~~~cpp
virtual void SetConfigTextFilterForTesting(
      ConfigTextFilterForTesting predicate) = 0;
~~~
 Sets a callback to override the background tracing config for testing.

### ~BackgroundTracingManager

~BackgroundTracingManager
~~~cpp
virtual ~BackgroundTracingManager() {}
~~~

###  GetInstance


~~~cpp
CONTENT_EXPORT static BackgroundTracingManager& GetInstance();
~~~
### field error



~~~cpp

CONTENT_EXPORT static const char kContentTriggerConfig[];

~~~



### enum DataFiltering
 Set the triggering rules for when to start recording.


 In preemptive mode, recording begins immediately and any calls to
 TriggerNamedEvent() will potentially trigger the trace to finalize and get
 uploaded. Once the trace has been uploaded, tracing will be enabled again.


 In reactive mode, recording begins when TriggerNamedEvent() is called, and
 continues until either the next call to TriggerNamedEvent, or a timeout
 occurs. Tracing will not be re-enabled after the trace is finalized and
 uploaded.


 This function uploads traces through UMA using SetTraceToUpload /
 GetLatestTraceToUpload. To specify a destination to upload to, use
 SetActiveScenarioWithReceiveCallback.


 Calls to SetActiveScenario() with a config will fail if tracing is
 currently on. Use WhenIdle to register a callback to get notified when
 the manager is idle and a config can be set again.

~~~cpp
enum DataFiltering {
    NO_DATA_FILTERING,
    ANONYMIZE_DATA,
  };
~~~