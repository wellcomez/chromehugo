---
tags:
---

## class DownloadManagerDelegate
 Browser's download manager: manages all downloads and destination view.

### GetNextId

DownloadManagerDelegate::GetNextId
~~~cpp
virtual void GetNextId(DownloadIdCallback callback);
~~~
 Runs |callback| with a new download id when possible, perhaps
 synchronously. If this call fails, |callback| will be called with
 kInvalidId.

### DetermineDownloadTarget

DownloadManagerDelegate::DetermineDownloadTarget
~~~cpp
virtual bool DetermineDownloadTarget(download::DownloadItem* item,
                                       DownloadTargetCallback* callback);
~~~
 Called to notify the delegate that a new download |item| requires a
 download target to be determined. The delegate should return |true| if it
 will determine the target information and will invoke |callback|. The
 callback may be invoked directly (synchronously). If this function returns
 |false|, the download manager will continue the download using a default
 target path.


 The state of the |item| shouldn't be modified during the process of
 filename determination save for external data (GetExternalData() /
 SetExternalData()).


 If the download should be canceled, |callback| should be invoked with an
 empty |target_path| argument.

### ShouldAutomaticallyOpenFile

DownloadManagerDelegate::ShouldAutomaticallyOpenFile
~~~cpp
virtual bool ShouldAutomaticallyOpenFile(const GURL& url,
                                           const base::FilePath& path);
~~~
 Tests if a file type should be opened automatically. This consider both
 user and policy settings, and should be called when it doesn't matter
 what set the auto-open, just if it is set.

### ShouldAutomaticallyOpenFileByPolicy

DownloadManagerDelegate::ShouldAutomaticallyOpenFileByPolicy
~~~cpp
virtual bool ShouldAutomaticallyOpenFileByPolicy(const GURL& url,
                                                   const base::FilePath& path);
~~~
 Tests if a file type should be opened automatically by policy. This
 should only be used if it matters if the file will auto-open by policy.

 Generally used to determine if we need to show UI indicating an active
 policy.

### ShouldCompleteDownload

DownloadManagerDelegate::ShouldCompleteDownload
~~~cpp
virtual bool ShouldCompleteDownload(download::DownloadItem* item,
                                      base::OnceClosure complete_callback);
~~~
 Allows the delegate to delay completion of the download.  This function
 will either return true (in which case the download may complete)
 or will call the callback passed when the download is ready for
 completion.  This routine may be called multiple times; once the callback
 has been called or the function has returned true for a particular
 download it should continue to return true for that download.

### ShouldOpenDownload

DownloadManagerDelegate::ShouldOpenDownload
~~~cpp
virtual bool ShouldOpenDownload(download::DownloadItem* item,
                                  DownloadOpenDelayedCallback callback);
~~~
 Allows the delegate to override opening the download. If this function
 returns false, the delegate needs to call |callback| when it's done
 with the item, and is responsible for opening it. When it returns true,
 the callback will not be used. This function is called after the final
 rename, but before the download state is set to COMPLETED.

### InterceptDownloadIfApplicable

DownloadManagerDelegate::InterceptDownloadIfApplicable
~~~cpp
virtual bool InterceptDownloadIfApplicable(
      const GURL& url,
      const std::string& user_agent,
      const std::string& content_disposition,
      const std::string& mime_type,
      const std::string& request_origin,
      int64_t content_length,
      bool is_transient,
      WebContents* web_contents);
~~~
 Checks and hands off the downloading to be handled by another system based
 on mime type. Returns true if the download was intercepted.

### GetSaveDir

GetSaveDir
~~~cpp
virtual void GetSaveDir(BrowserContext* browser_context,
                          base::FilePath* website_save_dir,
                          base::FilePath* download_save_dir) {}
~~~
 Retrieve the directories to save html pages and downloads to.

### ChooseSavePath

ChooseSavePath
~~~cpp
virtual void ChooseSavePath(
      WebContents* web_contents,
      const base::FilePath& suggested_path,
      const base::FilePath::StringType& default_extension,
      bool can_save_as_complete,
      SavePackagePathPickedCallback callback) {}
~~~
 Asks the user for the path to save a page. The delegate calls the callback
 to give the answer.

### SanitizeSavePackageResourceName

SanitizeSavePackageResourceName
~~~cpp
virtual void SanitizeSavePackageResourceName(base::FilePath* filename,
                                               const GURL& source_url) {}
~~~
 Sanitize a filename that's going to be used for saving a subresource of a
 SavePackage.


 If the delegate does nothing, the default filename already populated in
 |filename| will be used. Otherwise, the delegate can update |filename| to
 the desired filename.


 |filename| contains a basename with an extension, but without a path. This
 should be the case on return as well. I.e. |filename| cannot specify a
 relative path.

 |source_url| contains the URL from which the download originates and is
 needed to determine the file's danger level.

### SanitizeDownloadParameters

SanitizeDownloadParameters
~~~cpp
virtual void SanitizeDownloadParameters(
      download::DownloadUrlParameters* params) {}
~~~
 Sanitize a download parameters

 If the delegate does nothing, the default parameters already populated in
 |params| will be used. Otherwise, the delegate can update |params| to
 the desired parameters.

### OpenDownload

OpenDownload
~~~cpp
virtual void OpenDownload(download::DownloadItem* download) {}
~~~
 Opens the file associated with this download.

### ShowDownloadInShell

ShowDownloadInShell
~~~cpp
virtual void ShowDownloadInShell(download::DownloadItem* download) {}
~~~
 Shows the download via the OS shell.

### ApplicationClientIdForFileScanning

DownloadManagerDelegate::ApplicationClientIdForFileScanning
~~~cpp
virtual std::string ApplicationClientIdForFileScanning();
~~~
 Return a GUID string used for identifying the application to the system AV
 function for scanning downloaded files. If no GUID is provided or if the
 provided GUID is invalid, then the appropriate quarantining will be
 performed manually without passing the download to the system AV function.


 This GUID is only used on Windows.

### CheckDownloadAllowed

DownloadManagerDelegate::CheckDownloadAllowed
~~~cpp
virtual void CheckDownloadAllowed(
      const WebContents::Getter& web_contents_getter,
      const GURL& url,
      const std::string& request_method,
      absl::optional<url::Origin> request_initiator,
      bool from_download_cross_origin_redirect,
      bool content_initiated,
      CheckDownloadAllowedCallback check_download_allowed_cb);
~~~
 Checks whether download is allowed to continue. |check_download_allowed_cb|
 is called with the decision on completion.

### GetQuarantineConnectionCallback

DownloadManagerDelegate::GetQuarantineConnectionCallback
~~~cpp
virtual download::QuarantineConnectionCallback
  GetQuarantineConnectionCallback();
~~~
 Gets a callback which can connect the download manager to a Quarantine
 Service instance if available.

### GetDownloadByGuid

DownloadManagerDelegate::GetDownloadByGuid
~~~cpp
virtual download::DownloadItem* GetDownloadByGuid(const std::string& guid);
~~~
 Gets a |DownloadItem| from the GUID, or null if no such GUID is available.

### CheckSavePackageAllowed

DownloadManagerDelegate::CheckSavePackageAllowed
~~~cpp
virtual void CheckSavePackageAllowed(
      download::DownloadItem* download_item,
      base::flat_map<base::FilePath, base::FilePath> save_package_files,
      SavePackageAllowedCallback callback);
~~~
 Allows the delegate to delay completion of a SavePackage's final renaming
 step so it can be disallowed.
