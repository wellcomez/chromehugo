
## class PrefetchServiceDelegate
 Allows embedders to control certain aspects of |PrefetchService|.

### ClearData

PrefetchServiceDelegate::ClearData
~~~cpp
static void ClearData(BrowserContext* browser_context);
~~~
 Clears data for delegate associated with given |browser_context|.

### GetMajorVersionNumber

PrefetchServiceDelegate::GetMajorVersionNumber
~~~cpp
virtual std::string GetMajorVersionNumber() = 0;
~~~
 Gets the major version of the embedder. Only the major version of embedder
 is included in the user agent for prefetch requests.

### GetAcceptLanguageHeader

PrefetchServiceDelegate::GetAcceptLanguageHeader
~~~cpp
virtual std::string GetAcceptLanguageHeader() = 0;
~~~
 Gets the accept language header to be included in prefetch requests.

### GetDefaultPrefetchProxyHost

PrefetchServiceDelegate::GetDefaultPrefetchProxyHost
~~~cpp
virtual GURL GetDefaultPrefetchProxyHost() = 0;
~~~
 Gets the default host of the prefetch proxy.

### GetAPIKey

PrefetchServiceDelegate::GetAPIKey
~~~cpp
virtual std::string GetAPIKey() = 0;
~~~
 Gets API key for making prefetching requests.

### GetDefaultDNSCanaryCheckURL

PrefetchServiceDelegate::GetDefaultDNSCanaryCheckURL
~~~cpp
virtual GURL GetDefaultDNSCanaryCheckURL() = 0;
~~~
 Gets the default URLs used in the DNS and TLS canary checks.

### GetDefaultTLSCanaryCheckURL

PrefetchServiceDelegate::GetDefaultTLSCanaryCheckURL
~~~cpp
virtual GURL GetDefaultTLSCanaryCheckURL() = 0;
~~~

### ReportOriginRetryAfter

PrefetchServiceDelegate::ReportOriginRetryAfter
~~~cpp
virtual void ReportOriginRetryAfter(const GURL& url,
                                      base::TimeDelta retry_after) = 0;
~~~
 Reports that a 503 response with a "Retry-After" header was received from
 |url|. Indicates that we shouldn't send new prefetch requests to that
 origin for |retry_after| amount of time.

### IsOriginOutsideRetryAfterWindow

PrefetchServiceDelegate::IsOriginOutsideRetryAfterWindow
~~~cpp
virtual bool IsOriginOutsideRetryAfterWindow(const GURL& url) = 0;
~~~
 Returns whether or not the URL is eligible for prefetch based on previous
 responses with a "Retry-After" header.

### ClearData

PrefetchServiceDelegate::ClearData
~~~cpp
virtual void ClearData() = 0;
~~~
 Clears any browsing data associated with the delegate, specifically any
 information about "Retry-Afters" received.

### DisableDecoysBasedOnUserSettings

PrefetchServiceDelegate::DisableDecoysBasedOnUserSettings
~~~cpp
virtual bool DisableDecoysBasedOnUserSettings() = 0;
~~~
 Checks if we can disable sending decoy prefetches based on the user's
 settings.

### IsSomePreloadingEnabled

PrefetchServiceDelegate::IsSomePreloadingEnabled
~~~cpp
virtual PreloadingEligibility IsSomePreloadingEnabled() = 0;
~~~
 Get the state of the user's preloading settings.

### IsExtendedPreloadingEnabled

PrefetchServiceDelegate::IsExtendedPreloadingEnabled
~~~cpp
virtual bool IsExtendedPreloadingEnabled() = 0;
~~~

### IsDomainInPrefetchAllowList

PrefetchServiceDelegate::IsDomainInPrefetchAllowList
~~~cpp
virtual bool IsDomainInPrefetchAllowList(const GURL& referring_url) = 0;
~~~
 Checks if the referring page is in the allow list to make prefetches.

### OnPrefetchLikely

PrefetchServiceDelegate::OnPrefetchLikely
~~~cpp
virtual void OnPrefetchLikely(WebContents* web_contents) = 0;
~~~
