
## class RenderWidgetHostObserver : public
 An observer API implemented by classes which are interested
 in RenderWidgetHost events.

### RenderWidgetHostDidUpdateVisualProperties

RenderWidgetHostDidUpdateVisualProperties
~~~cpp
virtual void RenderWidgetHostDidUpdateVisualProperties(
      RenderWidgetHost* widget_host) {}
~~~
 Invoked after the renderer has updated visual properties on the main thread
 and committed the change on the compositor thread.

### RenderWidgetHostDestroyed

RenderWidgetHostDestroyed
~~~cpp
virtual void RenderWidgetHostDestroyed(RenderWidgetHost* widget_host) {}
~~~
 This method is invoked when the observed RenderWidgetHost is destroyed.

 This is guaranteed to be the last call made to the observer, so if the
 observer is tied to the observed RenderWidgetHost, it is safe to delete it.
