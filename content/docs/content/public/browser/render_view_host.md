
## class RenderViewHost
 A RenderViewHost is responsible for creating and talking to a
 `blink::WebView` object in a child process. It exposes a high level API to
 users, for things like loading pages, adjusting the display and other browser
 functionality, which it translates into IPC messages sent over the IPC
 channel with the `blink::WebView`. It responds to all IPC messages sent by
 that `blink::WebView` and cracks them, calling a delegate object back with
 higher level types where possible.


 The intent of this interface is to provide a view-agnostic communication
 conduit with a renderer. This is so we can build HTML views not only as
 WebContents (see WebContents for an example) but also as views, etc.


 DEPRECATED: RenderViewHost is being removed as part of the SiteIsolation
 project. New code should not be added here, but to RenderWidgetHost (if it's
 about drawing or events), RenderFrameHost (if it's frame specific), or
 Page (if it's page specific).


 For context, please see https://crbug.com/467770 and
 https://www.chromium.org/developers/design-documents/site-isolation.

### From

RenderViewHost::From
~~~cpp
static RenderViewHost* From(RenderWidgetHost* rwh);
~~~
 Returns the RenderViewHost, if any, that uses the specified
 RenderWidgetHost. Returns nullptr if there is no such RenderViewHost.

### ~RenderViewHost

~RenderViewHost
~~~cpp
virtual ~RenderViewHost() {}
~~~

### GetWidget

RenderViewHost::GetWidget
~~~cpp
virtual RenderWidgetHost* GetWidget() const = 0;
~~~
 Returns the RenderWidgetHost for this RenderViewHost.

### GetProcess

RenderViewHost::GetProcess
~~~cpp
virtual RenderProcessHost* GetProcess() const = 0;
~~~
 Returns the RenderProcessHost for this RenderViewHost.

### GetRoutingID

RenderViewHost::GetRoutingID
~~~cpp
virtual int GetRoutingID() const = 0;
~~~
 Returns the routing id for IPC use for this RenderViewHost.


 Implementation note: Historically, RenderViewHost was-a RenderWidgetHost,
 and shared its IPC channel and its routing ID. Although this inheritance is
 no longer so, the IPC channel is currently still shared. Expect this to
 change.

### EnablePreferredSizeMode

RenderViewHost::EnablePreferredSizeMode
~~~cpp
virtual void EnablePreferredSizeMode() = 0;
~~~
 Instructs the `blink::WebView` to send back updates to the preferred size.

### WriteIntoTrace

RenderViewHost::WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(
      perfetto::TracedProto<TraceProto> context) const = 0;
~~~
 Write a representation of this object into a trace.

### RenderViewHost

RenderViewHost
~~~cpp
RenderViewHost() {}
~~~
