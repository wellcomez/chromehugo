
## class SmsFetcher
 namespace
 SmsFetcher coordinates between the provisioning of SMSes coming from the
 local device or remote devices to multiple origins.

 There is one SmsFetcher per profile.

### enum class

~~~cpp
enum class UserConsent {
    // The fetcher has not obtained the user consent to share the OTP. It is
    // expected that the the subscriber (the browser) will do so.
    kNotObtained,
    // The fetcher has already obtained the user consent to share the OTP.
    // Currently this is the case only when GMS User Consent API is used.
    kObtained
  }
~~~
### SmsFetcher

SmsFetcher
~~~cpp
SmsFetcher() = default;
~~~

### ~SmsFetcher

~SmsFetcher
~~~cpp
virtual ~SmsFetcher() = default;
~~~

### Get

SmsFetcher::Get
~~~cpp
CONTENT_EXPORT static SmsFetcher* Get(BrowserContext* context);
~~~
 Retrieval for devices that exclusively listen for SMSes coming from other
 telephony devices. (eg. desktop)
###  OnReceive


~~~cpp
class Subscriber : public base::CheckedObserver {
   public:
    // Receive an |origin_list| and a |one_time_code| from subscribed origin.
    // The |origin_list| is for verification purpose on remote device and the
    // |one_time_code| is used as an alphanumeric value which the origin uses to
    // verify the ownership of the phone number.
    virtual void OnReceive(const OriginList& origin_list,
                           const std::string& one_time_code,
                           UserConsent) = 0;
    virtual void OnFailure(SmsFetchFailureType failure_type) = 0;
  };
~~~
### Subscribe

Subscribe
~~~cpp
virtual void Subscribe(const OriginList& origin_list,
                         Subscriber& subscriber) = 0;
~~~
 Subscribes to incoming SMSes from SmsProvider for subscribers that do not
 have telephony capabilities and exclusively listen for SMSes received
 on other devices.

### Subscribe

Subscribe
~~~cpp
virtual void Subscribe(const OriginList& origin_list,
                         Subscriber& subscriber,
                         RenderFrameHost& render_frame_host) = 0;
~~~
 Subscribes to incoming SMSes from SmsProvider for telephony
 devices that can receive SMSes locally and can show a permission prompt.

 TODO(yigu): This API is used in content/ only. We should move it to the
 SmsFetcherImpl per guideline. https://crbug.com/1136062.

### Unsubscribe

Unsubscribe
~~~cpp
virtual void Unsubscribe(const OriginList& origin_list,
                           Subscriber* subscriber) = 0;
~~~

### HasSubscribers

HasSubscribers
~~~cpp
virtual bool HasSubscribers() = 0;
~~~
 TODO(yigu): This API is used in content/ only. We should move it to the
 SmsFetcherImpl per guideline. https://crbug.com/1136062.

### SmsFetcher

SmsFetcher
~~~cpp
SmsFetcher() = default;
~~~

### ~SmsFetcher

~SmsFetcher
~~~cpp
virtual ~SmsFetcher() = default;
~~~

### Subscribe

Subscribe
~~~cpp
virtual void Subscribe(const OriginList& origin_list,
                         Subscriber& subscriber) = 0;
~~~
 Subscribes to incoming SMSes from SmsProvider for subscribers that do not
 have telephony capabilities and exclusively listen for SMSes received
 on other devices.

### Subscribe

Subscribe
~~~cpp
virtual void Subscribe(const OriginList& origin_list,
                         Subscriber& subscriber,
                         RenderFrameHost& render_frame_host) = 0;
~~~
 Subscribes to incoming SMSes from SmsProvider for telephony
 devices that can receive SMSes locally and can show a permission prompt.

 TODO(yigu): This API is used in content/ only. We should move it to the
 SmsFetcherImpl per guideline. https://crbug.com/1136062.

### Unsubscribe

Unsubscribe
~~~cpp
virtual void Unsubscribe(const OriginList& origin_list,
                           Subscriber* subscriber) = 0;
~~~

### HasSubscribers

HasSubscribers
~~~cpp
virtual bool HasSubscribers() = 0;
~~~
 TODO(yigu): This API is used in content/ only. We should move it to the
 SmsFetcherImpl per guideline. https://crbug.com/1136062.

### enum class
 Indicates whether the subscriber needs to obtain its own user consent or
 not.

~~~cpp
enum class UserConsent {
    // The fetcher has not obtained the user consent to share the OTP. It is
    // expected that the the subscriber (the browser) will do so.
    kNotObtained,
    // The fetcher has already obtained the user consent to share the OTP.
    // Currently this is the case only when GMS User Consent API is used.
    kObtained
  };
~~~
### Get

SmsFetcher::Get
~~~cpp
CONTENT_EXPORT static SmsFetcher* Get(BrowserContext* context);
~~~
 Retrieval for devices that exclusively listen for SMSes coming from other
 telephony devices. (eg. desktop)
###  OnReceive


~~~cpp
class Subscriber : public base::CheckedObserver {
   public:
    // Receive an |origin_list| and a |one_time_code| from subscribed origin.
    // The |origin_list| is for verification purpose on remote device and the
    // |one_time_code| is used as an alphanumeric value which the origin uses to
    // verify the ownership of the phone number.
    virtual void OnReceive(const OriginList& origin_list,
                           const std::string& one_time_code,
                           UserConsent) = 0;
    virtual void OnFailure(SmsFetchFailureType failure_type) = 0;
  };
~~~