
## struct SupportedDelegations
 This class represents the supported delegations of the StoredPaymentApp.

### ~SupportedDelegations

SupportedDelegations::~SupportedDelegations
~~~cpp
~SupportedDelegations()
~~~

### ProvidesAll

SupportedDelegations::ProvidesAll
~~~cpp
bool ProvidesAll(
      const payments::mojom::PaymentOptionsPtr& payment_options) const;
~~~
