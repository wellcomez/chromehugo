### CONTENT_EXPORT CreateRenderWidgetHostNSView

CONTENT_EXPORT CreateRenderWidgetHostNSView
~~~cpp
void CONTENT_EXPORT CreateRenderWidgetHostNSView(
    uint64_t view_id,
    mojo::ScopedInterfaceEndpointHandle host_handle,
    mojo::ScopedInterfaceEndpointHandle view_request_handle,
    RenderWidgetHostViewMacDelegateCallback
        responder_delegate_creation_callback);
~~~
 Create the NSView for a RenderWidgetHostView or WebContentsView. This is
 called in the app shim process through an interface in remote_cocoa. These
 functions should be moved to remote_cocoa, but currently have dependencies on
 content.

 https://crbug.com/888290
### CONTENT_EXPORT CreateWebContentsNSView

CONTENT_EXPORT CreateWebContentsNSView
~~~cpp
void CONTENT_EXPORT CreateWebContentsNSView(
    uint64_t view_id,
    mojo::ScopedInterfaceEndpointHandle host_handle,
    mojo::ScopedInterfaceEndpointHandle view_request_handle);
~~~

