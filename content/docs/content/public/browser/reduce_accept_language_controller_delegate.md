
## class ReduceAcceptLanguageControllerDelegate
 Interface for managing reduced accept languages. It depends on existing
 language prefs service to get user's accept-language list to help the
 language negotiation process.

### GetReducedLanguage

ReduceAcceptLanguageControllerDelegate::GetReducedLanguage
~~~cpp
virtual absl::optional<std::string> GetReducedLanguage(
      const url::Origin& origin) = 0;
~~~
 Get which language was persisted for the given origin, if any.

### GetUserAcceptLanguages

ReduceAcceptLanguageControllerDelegate::GetUserAcceptLanguages
~~~cpp
virtual std::vector<std::string> GetUserAcceptLanguages() const = 0;
~~~
 Get user's current list of accepted languages.

### PersistReducedLanguage

ReduceAcceptLanguageControllerDelegate::PersistReducedLanguage
~~~cpp
virtual void PersistReducedLanguage(const url::Origin& origin,
                                      const std::string& language) = 0;
~~~
 Persist the language of the top-level frame for use on future visits to
 top-level frames with the same origin.

### ClearReducedLanguage

ReduceAcceptLanguageControllerDelegate::ClearReducedLanguage
~~~cpp
virtual void ClearReducedLanguage(const url::Origin& origin) = 0;
~~~
 Clear the persisted reduced language for the given origin.
