
## class PermissionOverrides
 Maintains permission overrides for each origin.

### ~PermissionOverrides

PermissionOverrides::~PermissionOverrides
~~~cpp
~PermissionOverrides()
~~~

### operator=

PermissionOverrides::operator=
~~~cpp
PermissionOverrides& operator=(PermissionOverrides&& other);
~~~

### PermissionOverrides

PermissionOverrides
~~~cpp
PermissionOverrides(const PermissionOverrides&) = delete;
~~~

### operator=

PermissionOverrides::operator=
~~~cpp
PermissionOverrides& operator=(const PermissionOverrides&) = delete;
~~~

### Set

PermissionOverrides::Set
~~~cpp
void Set(const absl::optional<url::Origin>& origin,
           blink::PermissionType permission,
           const blink::mojom::PermissionStatus& status);
~~~
 Set permission override for |permission| at |origin| to |status|.

 Null |origin| specifies global overrides.

### Get

PermissionOverrides::Get
~~~cpp
absl::optional<blink::mojom::PermissionStatus> Get(
      const url::Origin& origin,
      blink::PermissionType permission) const;
~~~
 Get override for |origin| set for |permission|, if specified.

### GetAllForTest

PermissionOverrides::GetAllForTest
~~~cpp
const base::flat_map<blink::PermissionType, blink::mojom::PermissionStatus>&
  GetAllForTest(const absl::optional<url::Origin>& origin) const;
~~~
 Get all overrides for particular |origin|, stored in |overrides|
 if found. Will return empty overrides if none previously existed. Returns
 global overrides when |origin| is nullptr.

### Reset

PermissionOverrides::Reset
~~~cpp
void Reset(const absl::optional<url::Origin>& origin);
~~~
 Resets overrides for |origin|.

 Null |origin| resets global overrides.

### GrantPermissions

PermissionOverrides::GrantPermissions
~~~cpp
void GrantPermissions(const absl::optional<url::Origin>& origin,
                        const std::vector<blink::PermissionType>& permissions);
~~~
 Sets status for |permissions| to GRANTED in |origin|, and DENIED
 for all others.

 Null |origin| grants permissions globally for context.
