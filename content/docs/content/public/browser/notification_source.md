
## class NotificationSource
 Do not declare a NotificationSource directly--use either
 "Source<sourceclassname>(sourceclasspointer)" or
 NotificationService::AllSources().

### ~NotificationSource

~NotificationSource
~~~cpp
~NotificationSource() {}
~~~

### map_key

map_key
~~~cpp
uintptr_t map_key() const { return reinterpret_cast<uintptr_t>(ptr_.get()); }
~~~
 NotificationSource can be used as the index for a map; this method
 returns the pointer to the current source as an identifier, for use as a
 map index.

### operator!=

operator!=
~~~cpp
bool operator!=(const NotificationSource& other) const {
    return ptr_ != other.ptr_;
  }
~~~

### operator==

operator==
~~~cpp
bool operator==(const NotificationSource& other) const {
    return ptr_ == other.ptr_;
  }
~~~

## class Source

### Source

Source
~~~cpp
Source(const T* ptr) : NotificationSource(ptr) {}
~~~
 TODO(erg): Our code hard relies on implicit conversion
### Source

Source
~~~cpp
Source(const NotificationSource& other)      // NOLINT
    : NotificationSource(other) {}
~~~
 NOLINT
### operator-&gt;

operator-&gt;
~~~cpp
T* operator->() const { return ptr(); }
~~~

### ptr

ptr
~~~cpp
T* ptr() const { return static_cast<T*>(const_cast<void*>(ptr_.get())); }
~~~
 The casts here allow this to compile with both T = Foo and T = const Foo.

### Source

Source
~~~cpp
Source(const T* ptr) : NotificationSource(ptr) {}
~~~
 TODO(erg): Our code hard relies on implicit conversion
### Source

Source
~~~cpp
Source(const NotificationSource& other)      // NOLINT
    : NotificationSource(other) {}
~~~
 NOLINT
### operator-&gt;

operator-&gt;
~~~cpp
T* operator->() const { return ptr(); }
~~~

### ptr

ptr
~~~cpp
T* ptr() const { return static_cast<T*>(const_cast<void*>(ptr_.get())); }
~~~
 The casts here allow this to compile with both T = Foo and T = const Foo.
