### GetFontListAsync

GetFontListAsync
~~~cpp
CONTENT_EXPORT void GetFontListAsync(
    base::OnceCallback<void(base::Value::List)> callback);
~~~
 Retrieves the list of fonts on the system as a list of strings. It provides
 a non-blocking interface to GetFontList_SlowBlocking in common/.


 This function will run asynchronously on a background thread since getting
 the font list from the system can be slow. The callback will be executed on
 the calling sequence.

