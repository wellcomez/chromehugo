### StartThrottlingAllFrameSinks

StartThrottlingAllFrameSinks
~~~cpp
CONTENT_EXPORT void StartThrottlingAllFrameSinks(base::TimeDelta interval);
~~~
 Signals the frame sink manager that all current and future frame sinks should
 start sending BeginFrames at an interval that is at least as long `interval`.

 Because there is a single viz process, which itself contains a single host
 frame sink manager, calling this function multiple times from anywhere will
 apply the throttling described by the latest call. For instance:

 Foo calling StartThrottlingAllFrameSinks(Hertz(15));

 followed by

 Bar calling StartThrottlingAllFrameSinks(Hertz(30));

 Will result in framerate being throttled at 30hz
 Should be called from the UI thread.

### StopThrottlingAllFrameSinks

StopThrottlingAllFrameSinks
~~~cpp
CONTENT_EXPORT void StopThrottlingAllFrameSinks();
~~~
 Stops the BeginFrame throttling enabled by `StartThrottlingAllFrameSinks()`.

 Should be called from the UI thread.

