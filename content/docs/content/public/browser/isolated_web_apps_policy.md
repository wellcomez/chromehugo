
## class IsolatedWebAppsPolicy
 A centralized place for making a decision about Isolated Web Apps (IWAs).

 For more information about IWAs, see:
 https://github.com/WICG/isolated-web-apps/blob/main/README.md
### operator=

IsolatedWebAppsPolicy::operator=
~~~cpp
IsolatedWebAppsPolicy& operator=(const IsolatedWebAppsPolicy&) = delete;
~~~

### IsolatedWebAppsPolicy

IsolatedWebAppsPolicy
~~~cpp
IsolatedWebAppsPolicy() = delete;
~~~

### AreIsolatedWebAppsEnabled

IsolatedWebAppsPolicy::AreIsolatedWebAppsEnabled
~~~cpp
static bool AreIsolatedWebAppsEnabled(BrowserContext* browser_context);
~~~
 Returns true if Isolated Web Apps are enabled.
