### MustDownload

MustDownload
~~~cpp
CONTENT_EXPORT bool MustDownload(const GURL& url,
                                 const net::HttpResponseHeaders* headers,
                                 const std::string& mime_type);
~~~
 Returns true if the given response must be downloaded because of the headers.

