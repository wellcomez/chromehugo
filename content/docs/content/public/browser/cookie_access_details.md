
## struct CookieAccessDetails

### CookieAccessDetails

CookieAccessDetails::CookieAccessDetails
~~~cpp
CookieAccessDetails(Type type,
                      const GURL& url,
                      const GURL& first_party_url,
                      const net::CookieList& list,
                      bool blocked_by_policy = false)
~~~

### ~CookieAccessDetails

CookieAccessDetails::~CookieAccessDetails
~~~cpp
~CookieAccessDetails()
~~~

### CookieAccessDetails

CookieAccessDetails::CookieAccessDetails
~~~cpp
CookieAccessDetails(const CookieAccessDetails&)
~~~

### operator=

CookieAccessDetails::operator=
~~~cpp
CookieAccessDetails& operator=(const CookieAccessDetails&);
~~~
