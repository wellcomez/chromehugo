
## class BrowserOrResourceContext
 A class holding either a BrowserContext* or a ResourceContext*.

 This class should hold a BrowserContext* when constructed on the UI thread
 and a ResourceContext* when constructed on the IO thread. This object must
 only be accessed on the thread it was constructed and does not allow
 converting between the two pointer types.

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
BrowserOrResourceContext() {
    union_.browser_context_ = nullptr;
    flavour_ = kNullFlavour;
  }
~~~

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
explicit BrowserOrResourceContext(BrowserContext* browser_context) {
    DCHECK(browser_context);
    DCHECK_CURRENTLY_ON(BrowserThread::UI);
    union_.browser_context_ = browser_context;
    flavour_ = kBrowserContextFlavour;
  }
~~~
 BrowserOrResourceContext is implicitly constructible from either
 BrowserContext* or ResourceContext*.  Neither of the constructor arguments
 can be null (enforced by DCHECKs and in some cases at compile time).

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
explicit BrowserOrResourceContext(ResourceContext* resource_context) {
    DCHECK(resource_context);
    DCHECK_CURRENTLY_ON(BrowserThread::IO);
    union_.resource_context_ = resource_context;
    flavour_ = kResourceContextFlavour;
  }
~~~

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
BrowserOrResourceContext(std::nullptr_t) = delete;
~~~

### ~BrowserOrResourceContext

~BrowserOrResourceContext
~~~cpp
~BrowserOrResourceContext() = default;
~~~
 BrowserOrResourceContext has a trivial, default destructor.

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
BrowserOrResourceContext(const BrowserOrResourceContext& other) = default;
~~~
 BrowserOrResourceContext is trivially copyable.

### operator=

operator=
~~~cpp
BrowserOrResourceContext& operator=(const BrowserOrResourceContext& other) =
      default;
~~~

### 


~~~cpp
explicit operator bool() const {
    return (union_.resource_context_ != nullptr &&
            union_.browser_context_ != nullptr);
  }
~~~

### ToBrowserContext

ToBrowserContext
~~~cpp
BrowserContext* ToBrowserContext() const {
    DCHECK_CURRENTLY_ON(BrowserThread::UI);
    CHECK_EQ(kBrowserContextFlavour, flavour_);
    return union_.browser_context_;
  }
~~~
 To be called only on the UI thread.  In DCHECK-enabled builds will verify
 that this object has kBrowserContextFlavour (implying that the returned
 BrowserContext* is valid and non-null.

### ToResourceContext

ToResourceContext
~~~cpp
ResourceContext* ToResourceContext() const {
    DCHECK_CURRENTLY_ON(BrowserThread::IO);
    CHECK_EQ(kResourceContextFlavour, flavour_);
    return union_.resource_context_;
  }
~~~
 To be called only on the IO thread.  In DCHECK-enabled builds will verify
 that this object has kResourceContextFlavour (implying that the returned
 ResourceContext* is valid and non-null.

### union_



~~~cpp

union Union {
    BrowserContext* browser_context_;
    ResourceContext* resource_context_;
  } union_;

~~~


### enum Flavour

~~~cpp
enum Flavour {
    kNullFlavour,
    kBrowserContextFlavour,
    kResourceContextFlavour,
  }
~~~
### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
BrowserOrResourceContext() {
    union_.browser_context_ = nullptr;
    flavour_ = kNullFlavour;
  }
~~~

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
explicit BrowserOrResourceContext(BrowserContext* browser_context) {
    DCHECK(browser_context);
    DCHECK_CURRENTLY_ON(BrowserThread::UI);
    union_.browser_context_ = browser_context;
    flavour_ = kBrowserContextFlavour;
  }
~~~
 BrowserOrResourceContext is implicitly constructible from either
 BrowserContext* or ResourceContext*.  Neither of the constructor arguments
 can be null (enforced by DCHECKs and in some cases at compile time).

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
explicit BrowserOrResourceContext(ResourceContext* resource_context) {
    DCHECK(resource_context);
    DCHECK_CURRENTLY_ON(BrowserThread::IO);
    union_.resource_context_ = resource_context;
    flavour_ = kResourceContextFlavour;
  }
~~~

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
BrowserOrResourceContext(std::nullptr_t) = delete;
~~~

### ~BrowserOrResourceContext

~BrowserOrResourceContext
~~~cpp
~BrowserOrResourceContext() = default;
~~~
 BrowserOrResourceContext has a trivial, default destructor.

### BrowserOrResourceContext

BrowserOrResourceContext
~~~cpp
BrowserOrResourceContext(const BrowserOrResourceContext& other) = default;
~~~
 BrowserOrResourceContext is trivially copyable.

### operator=

operator=
~~~cpp
BrowserOrResourceContext& operator=(const BrowserOrResourceContext& other) =
      default;
~~~

### 


~~~cpp
explicit operator bool() const {
    return (union_.resource_context_ != nullptr &&
            union_.browser_context_ != nullptr);
  }
~~~

### ToBrowserContext

ToBrowserContext
~~~cpp
BrowserContext* ToBrowserContext() const {
    DCHECK_CURRENTLY_ON(BrowserThread::UI);
    CHECK_EQ(kBrowserContextFlavour, flavour_);
    return union_.browser_context_;
  }
~~~
 To be called only on the UI thread.  In DCHECK-enabled builds will verify
 that this object has kBrowserContextFlavour (implying that the returned
 BrowserContext* is valid and non-null.

### ToResourceContext

ToResourceContext
~~~cpp
ResourceContext* ToResourceContext() const {
    DCHECK_CURRENTLY_ON(BrowserThread::IO);
    CHECK_EQ(kResourceContextFlavour, flavour_);
    return union_.resource_context_;
  }
~~~
 To be called only on the IO thread.  In DCHECK-enabled builds will verify
 that this object has kResourceContextFlavour (implying that the returned
 ResourceContext* is valid and non-null.

### union_



~~~cpp

union Union {
    BrowserContext* browser_context_;
    ResourceContext* resource_context_;
  } union_;

~~~


### enum Flavour

~~~cpp
enum Flavour {
    kNullFlavour,
    kBrowserContextFlavour,
    kResourceContextFlavour,
  } flavour_;
~~~