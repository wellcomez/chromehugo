
## class FileSystemAccessPermissionGrant
    : public::Observer
 This observer can be used to be notified of changes to the permission
 status of a grant.

### OnPermissionStatusChanged

OnPermissionStatusChanged
~~~cpp
virtual void OnPermissionStatusChanged() = 0;
~~~

### OnPermissionStatusChanged

OnPermissionStatusChanged
~~~cpp
virtual void OnPermissionStatusChanged() = 0;
~~~

## class FileSystemAccessPermissionGrant
    : public
 A ref-counted permission grant. This is needed so the implementation of
 this class can keep track of references to permissions, and clean up state
 when no more references remain. Multiple FileSystemAccessHandle instances
 can share the same permission grant. For example a directory and all its
 children will use the same grant.


 Instances of this class can be retrieved via a
 FileSystemAccessPermissionContext instance.


 FileSystemAccessPermissionGrant instances are not thread safe, and should
 only be used (and referenced) on the same sequence as the PermissionContext
 that created them, i.e. the UI thread.

### GetStatus

FileSystemAccessPermissionGrant
    : public::GetStatus
~~~cpp
virtual PermissionStatus GetStatus() = 0;
~~~

### GetPath

FileSystemAccessPermissionGrant
    : public::GetPath
~~~cpp
virtual base::FilePath GetPath() = 0;
~~~
 Returns the path this permission grant is associated with. Can return an
 empty FilePath if the permission grant isn't associated with any specific
 path.

### RequestPermission

FileSystemAccessPermissionGrant
    : public::RequestPermission
~~~cpp
virtual void RequestPermission(
      GlobalRenderFrameHostId frame_id,
      UserActivationState user_activation_state,
      base::OnceCallback<void(PermissionRequestOutcome)> callback) = 0;
~~~
 Call this method to request permission for this grant. The |callback|
 should be called after the status of this grant has been updated with
 the outcome of the request.

### AddObserver

FileSystemAccessPermissionGrant
    : public::AddObserver
~~~cpp
void AddObserver(Observer* observer);
~~~
 Per base::ObserverList, it is an error to attempt to add an observer more
 than once.

### RemoveObserver

FileSystemAccessPermissionGrant
    : public::RemoveObserver
~~~cpp
void RemoveObserver(Observer* observer);
~~~
 Does nothing if the observer isn't in the list of observers.

### ~FileSystemAccessPermissionGrant

FileSystemAccessPermissionGrant
    : public::~FileSystemAccessPermissionGrant
~~~cpp
~FileSystemAccessPermissionGrant()
~~~

### NotifyPermissionStatusChanged

FileSystemAccessPermissionGrant
    : public::NotifyPermissionStatusChanged
~~~cpp
void NotifyPermissionStatusChanged();
~~~
