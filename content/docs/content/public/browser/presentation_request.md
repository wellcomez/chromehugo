
## struct PresentationRequest
 Represents a presentation request made from a render frame. Contains a list
 of presentation URLs of the request, and information on the originating
 frame.

### ~PresentationRequest

PresentationRequest::~PresentationRequest
~~~cpp
~PresentationRequest()
~~~

### PresentationRequest

PresentationRequest::PresentationRequest
~~~cpp
PresentationRequest(const PresentationRequest& other)
~~~

### operator=

PresentationRequest::operator=
~~~cpp
PresentationRequest& operator=(const PresentationRequest& other);
~~~

### operator==

PresentationRequest::operator==
~~~cpp
bool operator==(const PresentationRequest& other) const;
~~~

### operator!=

operator!=
~~~cpp
bool operator!=(const PresentationRequest& other) const {
    return !(*this == other);
  }
~~~
