
## class NotificationEventDispatcher
 This is the dispatcher to be used for firing events related to notifications.

 This class is a singleton, the instance of which can be retrieved using the
 static GetInstance() method. All methods must be called on the UI thread.

### DispatchNotificationClickEvent

NotificationEventDispatcher::DispatchNotificationClickEvent
~~~cpp
virtual void DispatchNotificationClickEvent(
      BrowserContext* browser_context,
      const std::string& notification_id,
      const GURL& origin,
      const absl::optional<int>& action_index,
      const absl::optional<std::u16string>& reply,
      NotificationDispatchCompleteCallback dispatch_complete_callback) = 0;
~~~
 Dispatch methods for persistent (SW backed) notifications.

 TODO(miguelg) consider merging them with the non persistent ones below.

 Dispatches the "notificationclick" event on the Service Worker associated
 with |notification_id| belonging to |origin|. The |callback| will be
 invoked when it's known whether the event successfully executed.

### DispatchNotificationCloseEvent

NotificationEventDispatcher::DispatchNotificationCloseEvent
~~~cpp
virtual void DispatchNotificationCloseEvent(
      BrowserContext* browser_context,
      const std::string& notification_id,
      const GURL& origin,
      bool by_user,
      NotificationDispatchCompleteCallback dispatch_complete_callback) = 0;
~~~
 Dispatches the "notificationclose" event on the Service Worker associated
 with |notification_id| belonging to |origin|. The
 |dispatch_complete_callback| will be invoked when it's known whether the
 event successfully executed.

### DispatchNonPersistentShowEvent

NotificationEventDispatcher::DispatchNonPersistentShowEvent
~~~cpp
virtual void DispatchNonPersistentShowEvent(
      const std::string& notification_id) = 0;
~~~
 Dispatch methods for the different non persistent (not backed by a service
 worker) notification events.

### DispatchNonPersistentClickEvent

NotificationEventDispatcher::DispatchNonPersistentClickEvent
~~~cpp
virtual void DispatchNonPersistentClickEvent(
      const std::string& notification_id,
      NotificationClickEventCallback callback) = 0;
~~~

### DispatchNonPersistentCloseEvent

NotificationEventDispatcher::DispatchNonPersistentCloseEvent
~~~cpp
virtual void DispatchNonPersistentCloseEvent(
      const std::string& notification_id,
      base::OnceClosure completed_closure) = 0;
~~~
