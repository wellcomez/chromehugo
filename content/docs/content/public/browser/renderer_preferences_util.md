### UpdateFontRendererPreferencesFromSystemSettings

UpdateFontRendererPreferencesFromSystemSettings
~~~cpp
CONTENT_EXPORT void UpdateFontRendererPreferencesFromSystemSettings(
    blink::RendererPreferences* prefs);
~~~
 Updates |prefs| from system settings.

