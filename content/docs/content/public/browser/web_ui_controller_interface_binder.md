### SafeDownCastAndBindInterface

SafeDownCastAndBindInterface
~~~cpp
bool SafeDownCastAndBindInterface(RenderFrameHost* host,
                                  mojo::PendingReceiver<Interface>& receiver) {
  // Performs a safe downcast to the concrete WebUIController subclass.
  WebUI* web_ui = host->GetWebUI();
  WebUIControllerSubclass* concrete_controller =
      web_ui ? web_ui->GetController()->GetAs<WebUIControllerSubclass>()
             : nullptr;

  if (!concrete_controller)
    return false;

  // Fails to compile if |Subclass| does not implement the appropriate overload
  // for |Interface|.
  if constexpr (BinderTakesRenderFrameHost<Interface,
                                           WebUIControllerSubclass>::value) {
    concrete_controller->BindInterface(host, std::move(receiver));
  } else {
    concrete_controller->BindInterface(std::move(receiver));
  }
  return true;
}
~~~

### CONTENT_EXPORT ReceivedInvalidWebUIControllerMessage

CONTENT_EXPORT ReceivedInvalidWebUIControllerMessage
~~~cpp
void CONTENT_EXPORT ReceivedInvalidWebUIControllerMessage(RenderFrameHost* rfh);
~~~

### RegisterWebUIControllerInterfaceBinder

RegisterWebUIControllerInterfaceBinder
~~~cpp
void RegisterWebUIControllerInterfaceBinder(
    mojo::BinderMapWithContext<RenderFrameHost*>* map) {
  DCHECK(!map->Contains<Interface>())
      << "A binder for " << Interface::Name_ << " has already been registered.";
  map->Add<Interface>(base::BindRepeating(
      [](RenderFrameHost* host, mojo::PendingReceiver<Interface> receiver) {
        // This is expected to be called only for outermost main frames.
        if (host->GetParentOrOuterDocument()) {
          internal::ReceivedInvalidWebUIControllerMessage(host);
          return;
        }

        const int size = sizeof...(WebUIControllerSubclasses);
        bool is_bound =
            internal::BinderHelper<Interface, size - 1,
                                   std::tuple<WebUIControllerSubclasses...>>::
                BindInterface(host, std::move(receiver));

        // This is expected to be called only for the right WebUI pages matching
        // the same WebUI associated to the RenderFrameHost.
        if (!is_bound) {
          internal::ReceivedInvalidWebUIControllerMessage(host);
          return;
        }
      }));
}
~~~

