
## struct ClientMetadata

### ClientMetadata

ClientMetadata::ClientMetadata
~~~cpp
ClientMetadata(const GURL& tos_url, const GURL& privacy_policy_url)
~~~

### ClientMetadata

ClientMetadata::ClientMetadata
~~~cpp
ClientMetadata(const ClientMetadata& other)
~~~

### ~ClientMetadata

ClientMetadata::~ClientMetadata
~~~cpp
~ClientMetadata()
~~~

## struct IdentityProviderMetadata

### IdentityProviderMetadata

IdentityProviderMetadata::IdentityProviderMetadata
~~~cpp
IdentityProviderMetadata(const IdentityProviderMetadata& other)
~~~

### ~IdentityProviderMetadata

IdentityProviderMetadata::~IdentityProviderMetadata
~~~cpp
~IdentityProviderMetadata()
~~~

## struct IdentityProviderData

### IdentityProviderData

IdentityProviderData::IdentityProviderData
~~~cpp
IdentityProviderData(const std::string& idp_url_for_display,
                       const std::vector<IdentityRequestAccount>& accounts,
                       const IdentityProviderMetadata& idp_metadata,
                       const ClientMetadata& client_metadata,
                       const blink::mojom::RpContext& rp_context)
~~~

### IdentityProviderData

IdentityProviderData::IdentityProviderData
~~~cpp
IdentityProviderData(const IdentityProviderData& other)
~~~

### ~IdentityProviderData

IdentityProviderData::~IdentityProviderData
~~~cpp
~IdentityProviderData()
~~~

## class IdentityRequestDialogController
 IdentityRequestDialogController is in interface for control of the UI
 surfaces that are displayed to intermediate the exchange of ID tokens.

### IdentityRequestDialogController

IdentityRequestDialogController
~~~cpp
IdentityRequestDialogController() = default;
~~~

### IdentityRequestDialogController

IdentityRequestDialogController
~~~cpp
IdentityRequestDialogController(const IdentityRequestDialogController&) =
      delete;
~~~

### operator=

IdentityRequestDialogController::operator=
~~~cpp
IdentityRequestDialogController& operator=(
      const IdentityRequestDialogController&) = delete;
~~~

### ~IdentityRequestDialogController

~IdentityRequestDialogController
~~~cpp
virtual ~IdentityRequestDialogController() = default;
~~~

### GetBrandIconIdealSize

IdentityRequestDialogController::GetBrandIconIdealSize
~~~cpp
virtual int GetBrandIconIdealSize();
~~~
 Returns the ideal size for the identity provider brand icon. The brand icon
 is displayed in the accounts dialog.

### GetBrandIconMinimumSize

IdentityRequestDialogController::GetBrandIconMinimumSize
~~~cpp
virtual int GetBrandIconMinimumSize();
~~~
 Returns the minimum size for the identity provider brand icon. The brand
 icon is displayed in the accounts dialog.

### SetIsInterceptionEnabled

IdentityRequestDialogController::SetIsInterceptionEnabled
~~~cpp
virtual void SetIsInterceptionEnabled(bool enabled);
~~~
 When this is true, the dialog should not be immediately auto-accepted.

### ShowAccountsDialog

IdentityRequestDialogController::ShowAccountsDialog
~~~cpp
virtual void ShowAccountsDialog(
      WebContents* rp_web_contents,
      const std::string& top_frame_for_display,
      const absl::optional<std::string>& iframe_url_for_display,
      const std::vector<IdentityProviderData>& identity_provider_data,
      IdentityRequestAccount::SignInMode sign_in_mode,
      bool show_auto_reauthn_checkbox,
      AccountSelectionCallback on_selected,
      DismissCallback dismiss_callback);
~~~
 Shows and accounts selections for the given IDP. The |on_selected| callback
 is called with the selected account id or empty string otherwise.

 |sign_in_mode| represents whether this is an auto re-authn flow.

### ShowFailureDialog

IdentityRequestDialogController::ShowFailureDialog
~~~cpp
virtual void ShowFailureDialog(WebContents* rp_web_contents,
                                 const std::string& top_frame_for_display,
                                 const std::string& idp_for_display,
                                 const IdentityProviderMetadata& idp_metadata,
                                 DismissCallback dismiss_callback);
~~~
 Shows a failure UI when the accounts fetch is failed such that it is
 observable by users. This could happen when an IDP claims that the user is
 signed in but not respond with any user account during browser fetches.

### GetTitle

IdentityRequestDialogController::GetTitle
~~~cpp
virtual std::string GetTitle() const;
~~~
 Only to be called after a dialog is shown.

### GetSubtitle

IdentityRequestDialogController::GetSubtitle
~~~cpp
virtual absl::optional<std::string> GetSubtitle() const;
~~~

### ShowIdpSigninFailureDialog

IdentityRequestDialogController::ShowIdpSigninFailureDialog
~~~cpp
virtual void ShowIdpSigninFailureDialog(base::OnceClosure dismiss_callback);
~~~
 Show dialog notifying user that IdP sign-in failed.
