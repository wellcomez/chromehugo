
## class WebUIConfig
 Class that stores properties for a WebUI.


 Clients that implement WebUI pages subclass WebUIConfig, overload the
 relevant methods and add an instance of their subclass to WebUIConfigMap.


 WebUIConfig is used when navigating to chrome:// or chrome-untrusted://
 pages to create the WebUIController and register the URLDataSource for
 the WebUI.


 WebUI pages are currently being migrated to use WebUIConfig so not
 all existing WebUI pages use this.

### WebUIConfig

WebUIConfig
~~~cpp
virtual ~WebUIConfig();
  WebUIConfig(const WebUIConfig&) = delete;
~~~

### operator=

WebUIConfig::operator=
~~~cpp
WebUIConfig& operator=(const WebUIConfig&) = delete;
~~~

### scheme

scheme
~~~cpp
const std::string& scheme() const { return scheme_; }
~~~
 Scheme for the WebUI.

### host

host
~~~cpp
const std::string& host() const { return host_; }
~~~
 Host the WebUI serves.

### IsWebUIEnabled

WebUIConfig::IsWebUIEnabled
~~~cpp
virtual bool IsWebUIEnabled(BrowserContext* browser_context);
~~~
 Returns whether the WebUI is enabled e.g. the necessary feature flags are
 on/off, the WebUI is enabled in incognito, etc. Defaults to true.

### CreateWebUIController

WebUIConfig::CreateWebUIController
~~~cpp
virtual std::unique_ptr<WebUIController> CreateWebUIController(
      WebUI* web_ui) = 0;
~~~
 Returns a WebUIController for the WebUI.


 URLDataSource is usually created in the constructor of WebUIController. The
 URLDataSource should be the same as the one registered in
 `RegisterURLDataSource()` or resources will fail to load.

### RegisterURLDataSource

RegisterURLDataSource
~~~cpp
virtual void RegisterURLDataSource(BrowserContext* browser_context) {}
~~~
 This is called when registering or updating a Service Worker.


 The URLDataSource here should be the same as the one registered in
 the WebUIController or resources will fail to load.

## class DefaultWebUIConfig

### ~DefaultWebUIConfig

~DefaultWebUIConfig
~~~cpp
~DefaultWebUIConfig() override = default;
~~~

### CreateWebUIController

CreateWebUIController
~~~cpp
std::unique_ptr<WebUIController> CreateWebUIController(
      WebUI* web_ui) override {
    return std::make_unique<T>(web_ui);
  }
~~~
