
## class IdleManager
 Provides an interface for handling mojo connections from the renderer,
 keeping track of clients that are monitoring the user's idle state.

### IdleManager

IdleManager
~~~cpp
IdleManager() = default;
~~~

### ~IdleManager

~IdleManager
~~~cpp
virtual ~IdleManager() = default;
~~~

### IdleManager

IdleManager
~~~cpp
IdleManager(const IdleManager&) = delete;
~~~

### operator=

operator=
~~~cpp
IdleManager& operator=(const IdleManager&) = delete;
~~~

### CreateService

CreateService
~~~cpp
virtual void CreateService(
      mojo::PendingReceiver<blink::mojom::IdleManager> receiver,
      const url::Origin& origin) = 0;
~~~

### SetIdleOverride

SetIdleOverride
~~~cpp
virtual void SetIdleOverride(blink::mojom::UserIdleState user_state,
                               blink::mojom::ScreenIdleState screen_state) = 0;
~~~

### ClearIdleOverride

ClearIdleOverride
~~~cpp
virtual void ClearIdleOverride() = 0;
~~~

### IdleManager

IdleManager
~~~cpp
IdleManager() = default;
~~~

### ~IdleManager

~IdleManager
~~~cpp
virtual ~IdleManager() = default;
~~~

### IdleManager

IdleManager
~~~cpp
IdleManager(const IdleManager&) = delete;
~~~

### operator=

operator=
~~~cpp
IdleManager& operator=(const IdleManager&) = delete;
~~~

### CreateService

CreateService
~~~cpp
virtual void CreateService(
      mojo::PendingReceiver<blink::mojom::IdleManager> receiver,
      const url::Origin& origin) = 0;
~~~

### SetIdleOverride

SetIdleOverride
~~~cpp
virtual void SetIdleOverride(blink::mojom::UserIdleState user_state,
                               blink::mojom::ScreenIdleState screen_state) = 0;
~~~

### ClearIdleOverride

ClearIdleOverride
~~~cpp
virtual void ClearIdleOverride() = 0;
~~~
