### ~BackgroundSyncController

~BackgroundSyncController
~~~cpp
virtual ~BackgroundSyncController() {}
~~~

### GetParameterOverrides

GetParameterOverrides
~~~cpp
virtual void GetParameterOverrides(BackgroundSyncParameters* parameters) {}
~~~
 This function allows the controller to alter the parameters used by
 background sync. Note that disable can be overridden from false to true
 but overrides from true to false will be ignored.

### NotifyOneShotBackgroundSyncRegistered

NotifyOneShotBackgroundSyncRegistered
~~~cpp
virtual void NotifyOneShotBackgroundSyncRegistered(const url::Origin& origin,
                                                     bool can_fire,
                                                     bool is_reregistered) {}
~~~
 Notification that a service worker registration with origin |origin| just
 registered a one-shot background sync event. Also includes information
 about the registration.

### NotifyPeriodicBackgroundSyncRegistered

NotifyPeriodicBackgroundSyncRegistered
~~~cpp
virtual void NotifyPeriodicBackgroundSyncRegistered(const url::Origin& origin,
                                                      int min_interval,
                                                      bool is_reregistered) {}
~~~
 Notification that a service worker registration with origin |origin| just
 registered a periodic background sync event. Also includes information
 about the registration.

### NotifyOneShotBackgroundSyncCompleted

NotifyOneShotBackgroundSyncCompleted
~~~cpp
virtual void NotifyOneShotBackgroundSyncCompleted(
      const url::Origin& origin,
      blink::ServiceWorkerStatusCode status_code,
      int num_attempts,
      int max_attempts) {}
~~~
 Notification that a service worker registration with origin |origin| just
 completed a one-shot background sync registration. Also include the
 |status_code| the registration finished with, the number of attempts, and
 the max allowed number of attempts.

### NotifyPeriodicBackgroundSyncCompleted

NotifyPeriodicBackgroundSyncCompleted
~~~cpp
virtual void NotifyPeriodicBackgroundSyncCompleted(
      const url::Origin& origin,
      blink::ServiceWorkerStatusCode status_code,
      int num_attempts,
      int max_attempts) {}
~~~
 Notification that a service worker registration with origin |origin| just
 completed a periodic background sync registration. Also include the
 |status_code| the registration finished with, the number of attempts, and
 the max allowed number of attempts.

### ScheduleBrowserWakeUpWithDelay

ScheduleBrowserWakeUpWithDelay
~~~cpp
virtual void ScheduleBrowserWakeUpWithDelay(
      blink::mojom::BackgroundSyncType sync_type,
      base::TimeDelta delay) {}
~~~
 Schedules a background task with delay |delay| to wake up the browser to
 process Background Sync registrations of type |sync_type|.

### CancelBrowserWakeup

CancelBrowserWakeup
~~~cpp
virtual void CancelBrowserWakeup(blink::mojom::BackgroundSyncType sync_type) {
  }
~~~
 Cancel the background task that wakes the browser up to process Background
 Sync registrations of type |sync_type|.

### GetNextEventDelay

GetNextEventDelay
~~~cpp
virtual base::TimeDelta GetNextEventDelay(
      const BackgroundSyncRegistration& registration,
      content::BackgroundSyncParameters* parameters,
      base::TimeDelta time_till_soonest_scheduled_event_for_origin) = 0;
~~~
 Calculates the delay after which the next sync event should be fired
 for a BackgroundSync registration. The delay is based on the sync_type of
 the |registration|, the |parameters| for the feature, the soonest time
 a (periodic)sync event is scheduled to fire for this origin, and other
 browser-specific considerations.

### CreateBackgroundSyncEventKeepAlive

CreateBackgroundSyncEventKeepAlive
~~~cpp
virtual std::unique_ptr<BackgroundSyncEventKeepAlive>
  CreateBackgroundSyncEventKeepAlive() = 0;
~~~
 Keeps the browser alive to allow a one-shot Background Sync registration
 to finish firing one sync event.

### NoteSuspendedPeriodicSyncOrigins

NoteSuspendedPeriodicSyncOrigins
~~~cpp
virtual void NoteSuspendedPeriodicSyncOrigins(
      std::set<url::Origin> suspended_origins) = 0;
~~~
 Updates its internal list of origins for which we have suspended periodic
 Background Sync registrations. This is compiled from each
 BackgroundSyncManager when they are initialized. This list used to ignore
 changes concerning origins we don't care about.

### NoteRegisteredPeriodicSyncOrigins

NoteRegisteredPeriodicSyncOrigins
~~~cpp
virtual void NoteRegisteredPeriodicSyncOrigins(
      std::set<url::Origin> registered_origins) = 0;
~~~
 Updates its internal list of origins for which we have periodic Background
 Sync registrations. This is compiled from each BackgroundSyncManager when
 they are initialized and subsequently kept up to date. This list is used to
 respond to changes in site settings affecting the feature.

### AddToTrackedOrigins

AddToTrackedOrigins
~~~cpp
virtual void AddToTrackedOrigins(const url::Origin& origin) = 0;
~~~
 Adds |origin| to its internal list of origins for which we have periodic
 Background Sync registrations. This list used to unregister periodic
 Background Sync when the controller deems necessary, for instance, upon
 revocation of permission.

### RemoveFromTrackedOrigins

RemoveFromTrackedOrigins
~~~cpp
virtual void RemoveFromTrackedOrigins(const url::Origin& origin) = 0;
~~~
 Removes |origin| from its internal list of origins for which we have
 periodic Background Sync registrations. This list used to unregister
 periodic Background Sync when the controller deems necessary, for instance,
  upon revocation of permission.

