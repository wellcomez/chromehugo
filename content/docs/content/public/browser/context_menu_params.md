
## struct ContextMenuParams
    : public
 FIXME(beng): This would be more useful in the future and more efficient
              if the parameters here weren't so literally mapped to what
              they contain for the ContextMenu task. It might be better
              to make the string fields more generic so that this object
              could be used for more contextual actions.


 SECURITY NOTE: This struct should be populated by the browser process,
 after validating the IPC payload from blink::UntrustworthyContextMenuParams.

 Note that the fields declared in ContextMenuParams can be populated based on
 the trustworthy, browser-side data (i.e. don't need to be sent over IPC and
 therefore don't need to be covered by blink::UntrustworthyContextMenuParams).

### ContextMenuParams

ContextMenuParams
    : public::ContextMenuParams
~~~cpp
ContextMenuParams(const ContextMenuParams& other)
~~~

### ~ContextMenuParams

ContextMenuParams
    : public::~ContextMenuParams
~~~cpp
~ContextMenuParams()
~~~

### ContextMenuParams

ContextMenuParams
    : public::ContextMenuParams
~~~cpp
ContextMenuParams(
      const blink::UntrustworthyContextMenuParams& other)
~~~
