### base::ProcessId GetProcessIdForAudioService

base::ProcessId GetProcessIdForAudioService
~~~cpp
CONTENT_EXPORT base::ProcessId GetProcessIdForAudioService();
~~~
 Returns the process id of the audio service utility process or
 base::kNullProcessId if audio service is not running in an utility process.

 Must be called on UI thread.

