
## class TtsEnvironmentAndroid
 Provides embedder specific information for text-to-speech.

### CanSpeakUtterancesFromHiddenWebContents

TtsEnvironmentAndroid::CanSpeakUtterancesFromHiddenWebContents
~~~cpp
virtual bool CanSpeakUtterancesFromHiddenWebContents() = 0;
~~~
 Returns whether utterances are allowed to speak from WebContents that are
 hidden. Returning false prevents speaking utterance from hidden
 WebContents, and also stops any playing utterance if the WebContents is
 hidden.

### CanSpeakNow

TtsEnvironmentAndroid::CanSpeakNow
~~~cpp
virtual bool CanSpeakNow() = 0;
~~~
 Returns true if speech is allowed at the current time. This is called
 right before an utterance is about to be spoken.

### SetCanSpeakNowChangedCallback

TtsEnvironmentAndroid::SetCanSpeakNowChangedCallback
~~~cpp
virtual void SetCanSpeakNowChangedCallback(
      base::RepeatingClosure callback) = 0;
~~~
 Sets the callback that is notified when the value of CanSpeakNow() changes.
