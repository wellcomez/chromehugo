### ~VideoCaptureDeviceLauncher

~VideoCaptureDeviceLauncher
~~~cpp
virtual ~VideoCaptureDeviceLauncher() {}
~~~

### CreateInProcessVideoCaptureDeviceLauncher

CreateInProcessVideoCaptureDeviceLauncher
~~~cpp
static std::unique_ptr<VideoCaptureDeviceLauncher>
  CreateInProcessVideoCaptureDeviceLauncher(
      scoped_refptr<base::SingleThreadTaskRunner> device_task_runner);
~~~
 Creates an InProcessVideoCaptureDeviceLauncher.

### LaunchDeviceAsync

LaunchDeviceAsync
~~~cpp
virtual void LaunchDeviceAsync(
      const std::string& device_id,
      blink::mojom::MediaStreamType stream_type,
      const media::VideoCaptureParams& params,
      base::WeakPtr<media::VideoFrameReceiver> receiver,
      base::OnceClosure connection_lost_cb,
      Callbacks* callbacks,
      base::OnceClosure done_cb) = 0;
~~~
 The passed-in |done_cb| must guarantee that the context relevant
 during the asynchronous processing stays alive.

### AbortLaunch

AbortLaunch
~~~cpp
virtual void AbortLaunch() = 0;
~~~

## class VideoCaptureDeviceLauncher
 Asynchronously launches video capture devices. After a call to
 LaunchDeviceAsync() it is illegal to call LaunchDeviceAsync() again until
 |callbacks| has been notified about the outcome of the asynchronous launch.

### OnDeviceLaunchFailed

VideoCaptureDeviceLauncher::OnDeviceLaunchFailed
~~~cpp
virtual void OnDeviceLaunchFailed(media::VideoCaptureError error) = 0;
~~~

### OnDeviceLaunchAborted

VideoCaptureDeviceLauncher::OnDeviceLaunchAborted
~~~cpp
virtual void OnDeviceLaunchAborted() = 0;
~~~

## class LaunchedVideoCaptureDevice
    : public

### SetPhotoOptions

LaunchedVideoCaptureDevice
    : public::SetPhotoOptions
~~~cpp
virtual void SetPhotoOptions(
      media::mojom::PhotoSettingsPtr settings,
      media::VideoCaptureDevice::SetPhotoOptionsCallback callback) = 0;
~~~

### TakePhoto

LaunchedVideoCaptureDevice
    : public::TakePhoto
~~~cpp
virtual void TakePhoto(
      media::VideoCaptureDevice::TakePhotoCallback callback) = 0;
~~~

### MaybeSuspendDevice

LaunchedVideoCaptureDevice
    : public::MaybeSuspendDevice
~~~cpp
virtual void MaybeSuspendDevice() = 0;
~~~

### ResumeDevice

LaunchedVideoCaptureDevice
    : public::ResumeDevice
~~~cpp
virtual void ResumeDevice() = 0;
~~~

### Crop

LaunchedVideoCaptureDevice
    : public::Crop
~~~cpp
virtual void Crop(
      const base::Token& crop_id,
      uint32_t crop_version,
      base::OnceCallback<void(media::mojom::CropRequestResult)> callback) = 0;
~~~

### RequestRefreshFrame

LaunchedVideoCaptureDevice
    : public::RequestRefreshFrame
~~~cpp
virtual void RequestRefreshFrame() = 0;
~~~

### SetDesktopCaptureWindowIdAsync

LaunchedVideoCaptureDevice
    : public::SetDesktopCaptureWindowIdAsync
~~~cpp
virtual void SetDesktopCaptureWindowIdAsync(gfx::NativeViewId window_id,
                                              base::OnceClosure done_cb) = 0;
~~~
 Methods for specific types of devices.
