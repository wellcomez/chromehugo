
## class InterestGroupManager
 InterestGroupManager is a per-StoragePartition class that owns shared
 state needed to run FLEDGE auctions. It lives on the UI thread.

###  operator<


~~~cpp
struct InterestGroupDataKey {
    url::Origin owner;
    url::Origin joining_origin;

    bool operator<(const InterestGroupDataKey& other) const {
      return std::tie(owner, joining_origin) <
             std::tie(other.owner, other.joining_origin);
    }

    bool operator==(const InterestGroupDataKey& other) const {
      return std::tie(owner, joining_origin) ==
             std::tie(other.owner, other.joining_origin);
    }
  };
~~~
### GetAllInterestGroupJoiningOrigins

GetAllInterestGroupJoiningOrigins
~~~cpp
virtual void GetAllInterestGroupJoiningOrigins(
      base::OnceCallback<void(std::vector<url::Origin>)> callback) = 0;
~~~
 Gets a list of all interest group joining origins. Each joining origin
 will only appear once. A joining origin is the top-frame origin for a page
 on which the join action occurred. Interest groups the user is joined to
 can be used later as part of on device ad auctions. Control over whether
 sites can join clients to interest groups is provided by the
 IsInterestGroupAPIAllowed function on the ContentBrowserClient.

### GetAllInterestGroupDataKeys

GetAllInterestGroupDataKeys
~~~cpp
virtual void GetAllInterestGroupDataKeys(
      base::OnceCallback<void(std::vector<InterestGroupDataKey>)> callback) = 0;
~~~
 Gets a list of all interest group data keys of owner origin and joining
 origin pairs.

### RemoveInterestGroupsByDataKey

RemoveInterestGroupsByDataKey
~~~cpp
virtual void RemoveInterestGroupsByDataKey(InterestGroupDataKey data_key,
                                             base::OnceClosure callback) = 0;
~~~
 Removes all interest groups that match the given interest group data key of
 same owner origin and joining origins.

### ~InterestGroupManager

~InterestGroupManager
~~~cpp
virtual ~InterestGroupManager() = default;
~~~

### GetAllInterestGroupJoiningOrigins

GetAllInterestGroupJoiningOrigins
~~~cpp
virtual void GetAllInterestGroupJoiningOrigins(
      base::OnceCallback<void(std::vector<url::Origin>)> callback) = 0;
~~~
 Gets a list of all interest group joining origins. Each joining origin
 will only appear once. A joining origin is the top-frame origin for a page
 on which the join action occurred. Interest groups the user is joined to
 can be used later as part of on device ad auctions. Control over whether
 sites can join clients to interest groups is provided by the
 IsInterestGroupAPIAllowed function on the ContentBrowserClient.

### GetAllInterestGroupDataKeys

GetAllInterestGroupDataKeys
~~~cpp
virtual void GetAllInterestGroupDataKeys(
      base::OnceCallback<void(std::vector<InterestGroupDataKey>)> callback) = 0;
~~~
 Gets a list of all interest group data keys of owner origin and joining
 origin pairs.

### RemoveInterestGroupsByDataKey

RemoveInterestGroupsByDataKey
~~~cpp
virtual void RemoveInterestGroupsByDataKey(InterestGroupDataKey data_key,
                                             base::OnceClosure callback) = 0;
~~~
 Removes all interest groups that match the given interest group data key of
 same owner origin and joining origins.

### ~InterestGroupManager

~InterestGroupManager
~~~cpp
virtual ~InterestGroupManager() = default;
~~~

###  operator<


~~~cpp
struct InterestGroupDataKey {
    url::Origin owner;
    url::Origin joining_origin;

    bool operator<(const InterestGroupDataKey& other) const {
      return std::tie(owner, joining_origin) <
             std::tie(other.owner, other.joining_origin);
    }

    bool operator==(const InterestGroupDataKey& other) const {
      return std::tie(owner, joining_origin) ==
             std::tie(other.owner, other.joining_origin);
    }
  };
~~~