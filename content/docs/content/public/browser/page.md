
## class Page : public
 Page represents a collection of documents with the same main document.

 At the moment some navigations might create a new blink::Document in the
 existing RenderFrameHost, which will lead to a creation of a new Page
 associated with the same main RenderFrameHost. See the comment in
 |DocumentUserData| for more details and crbug.com/936696 for the
 progress on always creating a new RenderFrameHost for each new document.

 Page is created when a main document is created, which can happen in the
 following ways:
 1) Main RenderFrameHost is created.

 2) A cross-document non-bfcached navigation is committed in the same
    RenderFrameHost.

 3) Main RenderFrameHost is re-created after crash.

 Page is deleted in the following cases:
 1) Main RenderFrameHost is deleted. Note that this might be different from
    when the navigation commits, see the comment in
    RenderFrameHost::LifecycleState::kPendingDeletion for more details.

 2) A cross-document non-bfcached navigation is committed in the same
    RenderFrameHost.

 3) Before main RenderFrameHost is re-created after crash.

 If a method can be called only for main RenderFrameHosts or if its behaviour
 is identical when called on the parent / child RenderFrameHosts, it should
 be added to Page(Impl).

 With Multiple Page Architecture (MPArch), each WebContents may have
 additional FrameTrees which will have their own associated Page. Please take
 into consideration when assuming that Page is appropriate for storing
 something that's common for all frames you see on a tab.

 See docs/frame_trees.md for more details.

 NOTE: Depending on the process model, the cross-origin iframes are likely to
 be hosted in a different renderer process than the main document, so a given
 page is hosted in multiple renderer processes at the same time.

 RenderViewHost / `blink::WebView` / blink::Page (which are all 1:1:1)
 represent a part of a given content::Page in a given renderer process (note,
 however, that like RenderFrameHosts, these objects at the moment can be
 reused for a new content::Page for a cross-document same-site main-frame
 navigation).

### GetManifestUrl

Page : public::GetManifestUrl
~~~cpp
virtual const absl::optional<GURL>& GetManifestUrl() const = 0;
~~~
 The GURL for the page's web application manifest.

 See https://w3c.github.io/manifest/#web-application-manifest
### GetManifest

Page : public::GetManifest
~~~cpp
virtual void GetManifest(GetManifestCallback callback) = 0;
~~~
 Requests the manifest URL and the Manifest of the main frame's document.

 |callback| may be called after the WebContents has been destroyed.

 This must be invoked on the UI thread, |callback| will be invoked on the UI
 thread.

### IsPrimary

Page : public::IsPrimary
~~~cpp
virtual bool IsPrimary() const = 0;
~~~
 Returns true iff this Page is primary for the associated `WebContents`
 (i.e. web_contents->GetPrimaryPage() == this_page). Non-primary pages
 include pages in bfcache, portal, prerendering, fenced frames, pending
 commit and pending deletion pages. See WebContents::GetPrimaryPage for more
 details.

### GetMainDocument

GetMainDocument
~~~cpp
RenderFrameHost& GetMainDocument() { return GetMainDocumentHelper(); }
~~~
 Returns the main RenderFrameHost associated with this Page.

### WriteIntoTrace

Page : public::WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(perfetto::TracedValue context) = 0;
~~~
 Write a description of this Page into the provided |context|.

### GetWeakPtr

Page : public::GetWeakPtr
~~~cpp
virtual base::WeakPtr<Page> GetWeakPtr() = 0;
~~~

### IsPageScaleFactorOne

Page : public::IsPageScaleFactorOne
~~~cpp
virtual bool IsPageScaleFactorOne() = 0;
~~~
 Whether the most recent page scale factor sent by the main frame's renderer
 is 1 (i.e. no magnification).

### Page

Page
~~~cpp
Page() = default;
~~~
