
## struct ContentIndexEntry

### ContentIndexEntry

ContentIndexEntry::ContentIndexEntry
~~~cpp
ContentIndexEntry(int64_t service_worker_registration_id,
                    blink::mojom::ContentDescriptionPtr description,
                    const GURL& launch_url,
                    base::Time registration_time,
                    bool is_top_level_context)
~~~

### operator=

ContentIndexEntry::operator=
~~~cpp
ContentIndexEntry& operator=(ContentIndexEntry&& other);
~~~

### ~ContentIndexEntry

ContentIndexEntry::~ContentIndexEntry
~~~cpp
~ContentIndexEntry()
~~~

## class ContentIndexProvider
 Interface for content providers to receive content-related updates.

### ContentIndexProvider

ContentIndexProvider
~~~cpp
ContentIndexProvider(const ContentIndexProvider&) = delete;
~~~

### operator=

ContentIndexProvider::operator=
~~~cpp
ContentIndexProvider& operator=(const ContentIndexProvider&) = delete;
~~~

### ~ContentIndexProvider

ContentIndexProvider::~ContentIndexProvider
~~~cpp
~ContentIndexProvider()
~~~

### GetIconSizes

ContentIndexProvider::GetIconSizes
~~~cpp
virtual std::vector<gfx::Size> GetIconSizes(
      blink::mojom::ContentCategory category) = 0;
~~~
 Returns the number of icons needed and their ideal sizes (in pixels).

### OnContentAdded

ContentIndexProvider::OnContentAdded
~~~cpp
virtual void OnContentAdded(ContentIndexEntry entry) = 0;
~~~
 Called when a new entry is registered. Must be called on the UI thread.

### OnContentDeleted

ContentIndexProvider::OnContentDeleted
~~~cpp
virtual void OnContentDeleted(int64_t service_worker_registration_id,
                                const url::Origin& origin,
                                const std::string& description_id) = 0;
~~~
 Called when an entry is unregistered. Must be called on the UI thread.
