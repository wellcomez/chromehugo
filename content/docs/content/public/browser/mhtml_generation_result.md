
## struct MHTMLGenerationResult
 A result container for the output of executing
 WebContents::GenerateMHTMLWithResult().

### MHTMLGenerationResult

MHTMLGenerationResult::MHTMLGenerationResult
~~~cpp
MHTMLGenerationResult(int64_t file_size, const std::string* digest)
~~~

### MHTMLGenerationResult

MHTMLGenerationResult::MHTMLGenerationResult
~~~cpp
MHTMLGenerationResult(const MHTMLGenerationResult& other)
~~~

### ~MHTMLGenerationResult

MHTMLGenerationResult::~MHTMLGenerationResult
~~~cpp
~MHTMLGenerationResult()
~~~
