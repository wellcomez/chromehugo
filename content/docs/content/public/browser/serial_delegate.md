### ~SerialDelegate

~SerialDelegate
~~~cpp
virtual ~SerialDelegate() = default;
~~~

### RunChooser

RunChooser
~~~cpp
virtual std::unique_ptr<SerialChooser> RunChooser(
      RenderFrameHost* frame,
      std::vector<blink::mojom::SerialPortFilterPtr> filters,
      SerialChooser::Callback callback) = 0;
~~~
 Shows a chooser for the user to select a serial port.  |callback| will be
 run when the prompt is closed. Deleting the returned object will cancel the
 prompt. This method should not be called if CanRequestPortPermission()
 below returned false.

### CanRequestPortPermission

CanRequestPortPermission
~~~cpp
virtual bool CanRequestPortPermission(RenderFrameHost* frame) = 0;
~~~
 Returns whether |frame| has permission to request access to a port.

### HasPortPermission

HasPortPermission
~~~cpp
virtual bool HasPortPermission(RenderFrameHost* frame,
                                 const device::mojom::SerialPortInfo& port) = 0;
~~~
 Returns whether |frame| has permission to access |port|.

### RevokePortPermissionWebInitiated

RevokePortPermissionWebInitiated
~~~cpp
virtual void RevokePortPermissionWebInitiated(
      RenderFrameHost* frame,
      const base::UnguessableToken& token) = 0;
~~~
 Revokes |frame| permission to access port identified by |token| ordered by
 website.

### GetPortInfo

GetPortInfo
~~~cpp
virtual const device::mojom::SerialPortInfo* GetPortInfo(
      RenderFrameHost* frame,
      const base::UnguessableToken& token) = 0;
~~~
 Gets the port info for a particular port, identified by its |token|.

### GetPortManager

GetPortManager
~~~cpp
virtual device::mojom::SerialPortManager* GetPortManager(
      RenderFrameHost* frame) = 0;
~~~
 Returns an open connection to the SerialPortManager interface owned by
 the embedder and being used to serve requests from |frame|.


 Content and the embedder must use the same connection so that the embedder
 can process connect/disconnect events for permissions management purposes
 before they are delivered to content. Otherwise race conditions are
 possible.

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(RenderFrameHost* frame, Observer* observer) = 0;
~~~
 Functions to manage the set of Observer instances registered to this
 object.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(RenderFrameHost* frame, Observer* observer) = 0;
~~~

## class SerialDelegate

### OnPortRemoved

SerialDelegate::OnPortRemoved
~~~cpp
virtual void OnPortRemoved(const device::mojom::SerialPortInfo& port) = 0;
~~~

### OnPortManagerConnectionError

SerialDelegate::OnPortManagerConnectionError
~~~cpp
virtual void OnPortManagerConnectionError() = 0;
~~~

### OnPermissionRevoked

SerialDelegate::OnPermissionRevoked
~~~cpp
virtual void OnPermissionRevoked(const url::Origin& origin) = 0;
~~~
 Event forwarded from permissions::ChooserContextBase::PermissionObserver: