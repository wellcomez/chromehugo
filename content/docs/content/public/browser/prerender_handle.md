
## class PrerenderHandle
 PrerenderHandle is the class used to encapsulate prerender resources in
 content/. In its destructor, the resource is expected to be released.

### PrerenderHandle

PrerenderHandle
~~~cpp
PrerenderHandle() = default;
~~~

### ~PrerenderHandle

~PrerenderHandle
~~~cpp
virtual ~PrerenderHandle() = default;
~~~

### GetInitialPrerenderingUrl

GetInitialPrerenderingUrl
~~~cpp
virtual GURL GetInitialPrerenderingUrl() = 0;
~~~
 Returns the initial URL that is passed to PrerenderHostRegistry for
 starting a prerendering page.

### GetWeakPtr

GetWeakPtr
~~~cpp
virtual base::WeakPtr<PrerenderHandle> GetWeakPtr() = 0;
~~~

### SetPreloadingAttemptFailureReason

SetPreloadingAttemptFailureReason
~~~cpp
virtual void SetPreloadingAttemptFailureReason(
      PreloadingFailureReason reason) = 0;
~~~

### PrerenderHandle

PrerenderHandle
~~~cpp
PrerenderHandle() = default;
~~~

### ~PrerenderHandle

~PrerenderHandle
~~~cpp
virtual ~PrerenderHandle() = default;
~~~

### GetInitialPrerenderingUrl

GetInitialPrerenderingUrl
~~~cpp
virtual GURL GetInitialPrerenderingUrl() = 0;
~~~
 Returns the initial URL that is passed to PrerenderHostRegistry for
 starting a prerendering page.

### GetWeakPtr

GetWeakPtr
~~~cpp
virtual base::WeakPtr<PrerenderHandle> GetWeakPtr() = 0;
~~~

### SetPreloadingAttemptFailureReason

SetPreloadingAttemptFailureReason
~~~cpp
virtual void SetPreloadingAttemptFailureReason(
      PreloadingFailureReason reason) = 0;
~~~
