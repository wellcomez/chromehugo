
## struct FileSystemAccessEntryFactory
    : public::BindingContext
 Context from which a created handle is going to be used. This is used for
 security and permission checks. Pass in the URL most relevant as the url
 parameter. This url will be used for verifications later for SafeBrowsing
 and Quarantine Service if used for writes.

### BindingContext

BindingContext
~~~cpp
BindingContext(const blink::StorageKey& storage_key,
                   const GURL& url,
                   GlobalRenderFrameHostId frame_id)
        : storage_key(storage_key), url(url), frame_id(frame_id) {}
~~~

### BindingContext

BindingContext
~~~cpp
BindingContext(const blink::StorageKey& storage_key,
                   const GURL& url,
                   int worker_process_id)
        : storage_key(storage_key),
          url(url),
          frame_id(worker_process_id, MSG_ROUTING_NONE) {}
~~~

### is_worker

is_worker
~~~cpp
bool is_worker() const { return !frame_id; }
~~~

### process_id

process_id
~~~cpp
int process_id() const { return frame_id.child_id; }
~~~

## class FileSystemAccessEntryFactory
    : public
 Exposes methods for creating FileSystemAccessEntries. All these methods need
 to be called on the UI thread.

### CreateFileEntryFromPath

FileSystemAccessEntryFactory
    : public::CreateFileEntryFromPath
~~~cpp
virtual blink::mojom::FileSystemAccessEntryPtr CreateFileEntryFromPath(
      const BindingContext& binding_context,
      PathType path_type,
      const base::FilePath& file_path,
      UserAction user_action) = 0;
~~~
 Creates a new FileSystemAccessEntryPtr from the path to a file. Assumes the
 passed in path is valid and represents a file.

### CreateDirectoryEntryFromPath

FileSystemAccessEntryFactory
    : public::CreateDirectoryEntryFromPath
~~~cpp
virtual blink::mojom::FileSystemAccessEntryPtr CreateDirectoryEntryFromPath(
      const BindingContext& binding_context,
      PathType path_type,
      const base::FilePath& directory_path,
      UserAction user_action) = 0;
~~~
 Creates a new FileSystemAccessEntryPtr from the path to a directory.

 Assumes the passed in path is valid and represents a directory.

### ResolveTransferToken

FileSystemAccessEntryFactory
    : public::ResolveTransferToken
~~~cpp
virtual void ResolveTransferToken(
      mojo::PendingRemote<blink::mojom::FileSystemAccessTransferToken>
          transfer_token,
      ResolveTransferTokenCallback callback) = 0;
~~~

### ~FileSystemAccessEntryFactory

~FileSystemAccessEntryFactory
~~~cpp
virtual ~FileSystemAccessEntryFactory() {}
~~~
