
## class WebSocketHandshakeRequestInfo
 WebSocketHandshakeRequestInfo provides additional information attached to
 a net::URLRequest.

### ~WebSocketHandshakeRequestInfo

~WebSocketHandshakeRequestInfo
~~~cpp
virtual ~WebSocketHandshakeRequestInfo() {}
~~~

### GetChildId

GetChildId
~~~cpp
virtual int GetChildId() = 0;
~~~
 Returns the ID of the child process where the WebSocket connection lives.

### GetRenderFrameId

GetRenderFrameId
~~~cpp
virtual int GetRenderFrameId() = 0;
~~~
 Returns the ID of the renderer frame where the WebSocket connection lives.

### ForRequest

WebSocketHandshakeRequestInfo::ForRequest
~~~cpp
CONTENT_EXPORT static WebSocketHandshakeRequestInfo* ForRequest(
      const net::URLRequest* request);
~~~
 Returns the WebSocketHandshakeRequestInfo instance attached to the given
 URLRequest. Returns nullptr when no instance is attached.

### ~WebSocketHandshakeRequestInfo

~WebSocketHandshakeRequestInfo
~~~cpp
virtual ~WebSocketHandshakeRequestInfo() {}
~~~

### GetChildId

GetChildId
~~~cpp
virtual int GetChildId() = 0;
~~~
 Returns the ID of the child process where the WebSocket connection lives.

### GetRenderFrameId

GetRenderFrameId
~~~cpp
virtual int GetRenderFrameId() = 0;
~~~
 Returns the ID of the renderer frame where the WebSocket connection lives.

### ForRequest

WebSocketHandshakeRequestInfo::ForRequest
~~~cpp
CONTENT_EXPORT static WebSocketHandshakeRequestInfo* ForRequest(
      const net::URLRequest* request);
~~~
 Returns the WebSocketHandshakeRequestInfo instance attached to the given
 URLRequest. Returns nullptr when no instance is attached.
