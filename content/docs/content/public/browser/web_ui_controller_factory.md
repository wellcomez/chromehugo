
## class WebUIControllerFactory
 Interface for an object which controls which URLs are considered WebUI URLs
 and creates WebUIController instances for given URLs.

### RegisterFactory

WebUIControllerFactory::RegisterFactory
~~~cpp
static void RegisterFactory(WebUIControllerFactory* factory);
~~~
 Call to register a factory.

### GetNumRegisteredFactoriesForTesting

WebUIControllerFactory::GetNumRegisteredFactoriesForTesting
~~~cpp
static int GetNumRegisteredFactoriesForTesting();
~~~
 Returns the number of registered factories.

### CreateWebUIControllerForURL

WebUIControllerFactory::CreateWebUIControllerForURL
~~~cpp
virtual std::unique_ptr<WebUIController> CreateWebUIControllerForURL(
      WebUI* web_ui,
      const GURL& url) = 0;
~~~
 Returns a WebUIController instance for the given URL, or nullptr if the URL
 doesn't correspond to a WebUI.

### GetWebUIType

WebUIControllerFactory::GetWebUIType
~~~cpp
virtual WebUI::TypeID GetWebUIType(BrowserContext* browser_context,
                                     const GURL& url) = 0;
~~~
 Gets the WebUI type for the given URL. This will return kNoWebUI if the
 corresponding call to CreateWebUIForURL would fail, or something
 non-nullptr if CreateWebUIForURL would succeed.

### UseWebUIForURL

WebUIControllerFactory::UseWebUIForURL
~~~cpp
virtual bool UseWebUIForURL(BrowserContext* browser_context,
                              const GURL& url) = 0;
~~~
 Shorthand for the above, but returns a simple yes/no.

 See also ContentClient::HasWebUIScheme, which only checks the scheme
 (faster) and can be used to determine security policy.

### UnregisterFactoryForTesting

WebUIControllerFactory::UnregisterFactoryForTesting
~~~cpp
static void UnregisterFactoryForTesting(WebUIControllerFactory* factory);
~~~
