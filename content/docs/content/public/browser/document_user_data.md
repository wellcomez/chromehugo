### GetDocumentUserData

GetDocumentUserData
~~~cpp
CONTENT_EXPORT base::SupportsUserData::Data* GetDocumentUserData(
    const RenderFrameHost* rfh,
    const void* key);
~~~

### SetDocumentUserData

SetDocumentUserData
~~~cpp
CONTENT_EXPORT void SetDocumentUserData(
    RenderFrameHost* rfh,
    const void* key,
    std::unique_ptr<base::SupportsUserData::Data> data);
~~~

### RemoveDocumentUserData

RemoveDocumentUserData
~~~cpp
CONTENT_EXPORT void RemoveDocumentUserData(RenderFrameHost* rfh,
                                           const void* key);
~~~

## class DocumentUserData

### CreateForCurrentDocument

CreateForCurrentDocument
~~~cpp
static void CreateForCurrentDocument(RenderFrameHost* rfh, Args&&... args) {
    DCHECK(rfh);
    if (!GetForCurrentDocument(rfh)) {
      T* data = new T(rfh, std::forward<Args>(args)...);
      SetDocumentUserData(rfh, UserDataKey(), base::WrapUnique(data));
    }
  }
~~~

### GetForCurrentDocument

GetForCurrentDocument
~~~cpp
static T* GetForCurrentDocument(RenderFrameHost* rfh) {
    DCHECK(rfh);
    return static_cast<T*>(GetDocumentUserData(rfh, UserDataKey()));
  }
~~~

### GetOrCreateForCurrentDocument

GetOrCreateForCurrentDocument
~~~cpp
static T* GetOrCreateForCurrentDocument(RenderFrameHost* rfh) {
    DCHECK(rfh);
    if (auto* data = GetForCurrentDocument(rfh)) {
      return data;
    }

    CreateForCurrentDocument(rfh);
    return GetForCurrentDocument(rfh);
  }
~~~

### DeleteForCurrentDocument

DeleteForCurrentDocument
~~~cpp
static void DeleteForCurrentDocument(RenderFrameHost* rfh) {
    DCHECK(rfh);
    DCHECK(GetForCurrentDocument(rfh));
    RemoveDocumentUserData(rfh, UserDataKey());
  }
~~~

### render_frame_host

render_frame_host
~~~cpp
RenderFrameHost& render_frame_host() const { return *render_frame_host_; }
~~~
 Returns the RenderFrameHost associated with `this` object of a subclass
 which inherits from DocumentUserData.


 The returned `render_frame_host()` is guaranteed to live as long as `this`
 DocumentUserData (due to how UserData works - RenderFrameHost
 owns `this` UserData).  Note that only the lifetime of
 `render_frame_host()` is guaranteed, but not its state - e.g. the frame may
 be `!IsActive()`.

### DocumentUserData

DocumentUserData
~~~cpp
explicit DocumentUserData(RenderFrameHost* rfh) : render_frame_host_(rfh) {
    CHECK(rfh);
  }
~~~
 TODO(https://crbug.com/1252044): Take a reference instead of a pointer
 (here + transitively/as-far-as-reasonably-possible in callers).

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### render_frame_host_



~~~cpp

const raw_ptr<RenderFrameHost> render_frame_host_ = nullptr;

~~~

 This is a pointer (rather than a reference) to ensure that go/miracleptr
 can cover this field (see also //base/memory/raw_ptr.md).

### GetForCurrentDocument

GetForCurrentDocument
~~~cpp
static T* GetForCurrentDocument(RenderFrameHost* rfh) {
    DCHECK(rfh);
    return static_cast<T*>(GetDocumentUserData(rfh, UserDataKey()));
  }
~~~

### GetOrCreateForCurrentDocument

GetOrCreateForCurrentDocument
~~~cpp
static T* GetOrCreateForCurrentDocument(RenderFrameHost* rfh) {
    DCHECK(rfh);
    if (auto* data = GetForCurrentDocument(rfh)) {
      return data;
    }

    CreateForCurrentDocument(rfh);
    return GetForCurrentDocument(rfh);
  }
~~~

### DeleteForCurrentDocument

DeleteForCurrentDocument
~~~cpp
static void DeleteForCurrentDocument(RenderFrameHost* rfh) {
    DCHECK(rfh);
    DCHECK(GetForCurrentDocument(rfh));
    RemoveDocumentUserData(rfh, UserDataKey());
  }
~~~

### render_frame_host

render_frame_host
~~~cpp
RenderFrameHost& render_frame_host() const { return *render_frame_host_; }
~~~
 Returns the RenderFrameHost associated with `this` object of a subclass
 which inherits from DocumentUserData.


 The returned `render_frame_host()` is guaranteed to live as long as `this`
 DocumentUserData (due to how UserData works - RenderFrameHost
 owns `this` UserData).  Note that only the lifetime of
 `render_frame_host()` is guaranteed, but not its state - e.g. the frame may
 be `!IsActive()`.

### DocumentUserData

DocumentUserData
~~~cpp
explicit DocumentUserData(RenderFrameHost* rfh) : render_frame_host_(rfh) {
    CHECK(rfh);
  }
~~~
 TODO(https://crbug.com/1252044): Take a reference instead of a pointer
 (here + transitively/as-far-as-reasonably-possible in callers).

### UserDataKey

UserDataKey
~~~cpp
static const void* UserDataKey() { return &T::kUserDataKey; }
~~~

### render_frame_host_



~~~cpp

const raw_ptr<RenderFrameHost> render_frame_host_ = nullptr;

~~~

 This is a pointer (rather than a reference) to ensure that go/miracleptr
 can cover this field (see also //base/memory/raw_ptr.md).
