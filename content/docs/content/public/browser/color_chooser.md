
## class ColorChooser
 Interface for a color chooser.

### ~ColorChooser

~ColorChooser
~~~cpp
virtual ~ColorChooser() {}
~~~

### End

End
~~~cpp
virtual void End() = 0;
~~~
 Ends connection with color chooser. Closes color chooser depending on the
 platform.

### SetSelectedColor

SetSelectedColor
~~~cpp
virtual void SetSelectedColor(SkColor color) = 0;
~~~
 Sets the selected color.

### ~ColorChooser

~ColorChooser
~~~cpp
virtual ~ColorChooser() {}
~~~

### End

End
~~~cpp
virtual void End() = 0;
~~~
 Ends connection with color chooser. Closes color chooser depending on the
 platform.

### SetSelectedColor

SetSelectedColor
~~~cpp
virtual void SetSelectedColor(SkColor color) = 0;
~~~
 Sets the selected color.
