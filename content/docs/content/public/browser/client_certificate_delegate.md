
## class ClientCertificateDelegate
 A delegate interface for selecting a client certificate for use with a
 network request. If the delegate is destroyed without calling
 ContinueWithCertificate, the certificate request will be aborted.

### ClientCertificateDelegate

ClientCertificateDelegate
~~~cpp
ClientCertificateDelegate() {}
~~~

### ClientCertificateDelegate

ClientCertificateDelegate
~~~cpp
ClientCertificateDelegate(const ClientCertificateDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
ClientCertificateDelegate& operator=(const ClientCertificateDelegate&) =
      delete;
~~~

### ~ClientCertificateDelegate

~ClientCertificateDelegate
~~~cpp
virtual ~ClientCertificateDelegate() {}
~~~

### ContinueWithCertificate

ContinueWithCertificate
~~~cpp
virtual void ContinueWithCertificate(
      scoped_refptr<net::X509Certificate> cert,
      scoped_refptr<net::SSLPrivateKey> key) = 0;
~~~
 Continue the request with |cert| and matching |key|. |cert| may be nullptr
 to continue without supplying a certificate. This decision will be
 remembered for future requests to the domain.

### ClientCertificateDelegate

ClientCertificateDelegate
~~~cpp
ClientCertificateDelegate() {}
~~~

### ClientCertificateDelegate

ClientCertificateDelegate
~~~cpp
ClientCertificateDelegate(const ClientCertificateDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
ClientCertificateDelegate& operator=(const ClientCertificateDelegate&) =
      delete;
~~~

### ~ClientCertificateDelegate

~ClientCertificateDelegate
~~~cpp
virtual ~ClientCertificateDelegate() {}
~~~

### ContinueWithCertificate

ContinueWithCertificate
~~~cpp
virtual void ContinueWithCertificate(
      scoped_refptr<net::X509Certificate> cert,
      scoped_refptr<net::SSLPrivateKey> key) = 0;
~~~
 Continue the request with |cert| and matching |key|. |cert| may be nullptr
 to continue without supplying a certificate. This decision will be
 remembered for future requests to the domain.
