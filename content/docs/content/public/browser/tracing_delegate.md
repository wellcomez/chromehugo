
## class TracingDelegate
 This can be implemented by the embedder to provide functionality for the
 about://tracing WebUI.

### IsAllowedToBeginBackgroundScenario

TracingDelegate::IsAllowedToBeginBackgroundScenario
~~~cpp
virtual bool IsAllowedToBeginBackgroundScenario(
      const BackgroundTracingConfig& config,
      bool requires_anonymized_data);
~~~
 This can be used to veto a particular background tracing scenario.

### IsAllowedToEndBackgroundScenario

TracingDelegate::IsAllowedToEndBackgroundScenario
~~~cpp
virtual bool IsAllowedToEndBackgroundScenario(
      const content::BackgroundTracingConfig& config,
      bool requires_anonymized_data,
      bool is_crash_scenario);
~~~

### IsSystemWideTracingEnabled

TracingDelegate::IsSystemWideTracingEnabled
~~~cpp
virtual bool IsSystemWideTracingEnabled();
~~~
 Whether system-wide performance trace collection using the external system
 tracing service is enabled.

### GenerateMetadataDict

TracingDelegate::GenerateMetadataDict
~~~cpp
virtual absl::optional<base::Value::Dict> GenerateMetadataDict();
~~~
 Used to add any additional metadata to traces.
