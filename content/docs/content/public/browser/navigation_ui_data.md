
## class NavigationUIData
 Copyable interface for embedders to pass opaque data to content/. It is
 expected to be created on the UI thread at the start of the navigation, and
 content/ will transfer it to the IO thread as a clone.

### ~NavigationUIData

~NavigationUIData
~~~cpp
virtual ~NavigationUIData() {}
~~~

### Clone

Clone
~~~cpp
virtual std::unique_ptr<NavigationUIData> Clone() = 0;
~~~
 Creates a new NavigationData that is a deep copy of the original.

### ~NavigationUIData

~NavigationUIData
~~~cpp
virtual ~NavigationUIData() {}
~~~

### Clone

Clone
~~~cpp
virtual std::unique_ptr<NavigationUIData> Clone() = 0;
~~~
 Creates a new NavigationData that is a deep copy of the original.
