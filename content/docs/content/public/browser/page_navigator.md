
## struct OpenURLParams

### OpenURLParams

OpenURLParams::OpenURLParams
~~~cpp
OpenURLParams(const GURL& url,
                const Referrer& referrer,
                WindowOpenDisposition disposition,
                ui::PageTransition transition,
                bool is_renderer_initiated)
~~~

### OpenURLParams

OpenURLParams::OpenURLParams
~~~cpp
OpenURLParams(const GURL& url,
                const Referrer& referrer,
                WindowOpenDisposition disposition,
                ui::PageTransition transition,
                bool is_renderer_initiated,
                bool started_from_context_menu)
~~~

### OpenURLParams

OpenURLParams::OpenURLParams
~~~cpp
OpenURLParams(const GURL& url,
                const Referrer& referrer,
                int frame_tree_node_id,
                WindowOpenDisposition disposition,
                ui::PageTransition transition,
                bool is_renderer_initiated)
~~~

### OpenURLParams

OpenURLParams::OpenURLParams
~~~cpp
OpenURLParams(const OpenURLParams& other)
~~~

### ~OpenURLParams

OpenURLParams::~OpenURLParams
~~~cpp
~OpenURLParams()
~~~

### FromNavigationHandle

OpenURLParams::FromNavigationHandle
~~~cpp
static OpenURLParams FromNavigationHandle(NavigationHandle* handle);
~~~
 Creates OpenURLParams that 1) preserve all applicable |handle| properties
 (URL, referrer, initiator, etc.) with OpenURLParams equivalents and 2) fill
 in reasonable defaults for other properties (like WindowOpenDisposition).

### Valid

OpenURLParams::Valid
~~~cpp
bool Valid() const;
~~~
 Returns true if the contents of this struct are considered valid and
 satisfy dependencies between fields (e.g. about:blank URLs require
 |initiator_origin| and |source_site_instance| to be set).

## class PageNavigator

### ~PageNavigator

~PageNavigator
~~~cpp
virtual ~PageNavigator() {}
~~~

### OpenURL

OpenURL
~~~cpp
virtual WebContents* OpenURL(const OpenURLParams& params) = 0;
~~~
 Opens a URL with the given disposition.  The transition specifies how this
 navigation should be recorded in the history system (for example, typed).

 Returns the WebContents the URL is opened in, or nullptr if the URL wasn't
 opened immediately.

### ~PageNavigator

~PageNavigator
~~~cpp
virtual ~PageNavigator() {}
~~~

### OpenURL

OpenURL
~~~cpp
virtual WebContents* OpenURL(const OpenURLParams& params) = 0;
~~~
 Opens a URL with the given disposition.  The transition specifies how this
 navigation should be recorded in the history system (for example, typed).

 Returns the WebContents the URL is opened in, or nullptr if the URL wasn't
 opened immediately.
