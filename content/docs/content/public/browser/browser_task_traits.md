### MakeTaskTraitsExtension

MakeTaskTraitsExtension
~~~cpp
constexpr base::TaskTraitsExtensionStorage MakeTaskTraitsExtension(
    ArgTypes&&... args) {
  return BrowserTaskTraitsExtension(std::forward<ArgTypes>(args)...)
      .Serialize();
}
~~~

### BrowserTaskTraits

BrowserTaskTraits
~~~cpp
constexpr BrowserTaskTraits(ArgTypes... args) : base::TaskTraits(args...) {}
~~~

### task_type

task_type
~~~cpp
BrowserTaskType task_type() {
    return GetExtension<BrowserTaskTraitsExtension>().task_type();
  }
~~~

## class BrowserTaskTraitsExtension::;

### BrowserTaskTraitsExtension

BrowserTaskTraitsExtension
~~~cpp
constexpr BrowserTaskTraitsExtension(ArgTypes... args)
      : browser_thread_(
            base::trait_helpers::GetEnum<BrowserThread::ID,
                                         BrowserThread::ID_COUNT>(args...)),
        task_type_(
            base::trait_helpers::GetEnum<BrowserTaskType,
                                         BrowserTaskType::kDefault>(args...)) {}
~~~

## class BrowserTaskTraitsExtension
 TaskTraits for running tasks on the browser threads.


 To post a task to the UI thread (analogous for IO thread):
     GetUIThreadTaskRunner({})->PostTask(FROM_HERE, task);

 To obtain a TaskRunner for the UI thread (analogous for the IO thread):
     GetUIThreadTaskRunner({});

 Tasks posted to the same BrowserThread with the same traits will be executed
 in the order they were posted, regardless of the TaskRunners they were
 posted via.


 Posting to a BrowserThread must only be done after it was initialized (ref.

 BrowserMainLoop::CreateThreads() phase).

### BrowserTaskTraitsExtension

BrowserTaskTraitsExtension
~~~cpp
constexpr BrowserTaskTraitsExtension(ArgTypes... args)
      : browser_thread_(
            base::trait_helpers::GetEnum<BrowserThread::ID,
                                         BrowserThread::ID_COUNT>(args...)),
        task_type_(
            base::trait_helpers::GetEnum<BrowserTaskType,
                                         BrowserTaskType::kDefault>(args...)) {}
~~~

### Serialize

Serialize
~~~cpp
constexpr base::TaskTraitsExtensionStorage Serialize() const {
    static_assert(8 == sizeof(BrowserTaskTraitsExtension),
                  "Update Serialize() and Parse() when changing "
                  "BrowserTaskTraitsExtension");
    return {kExtensionId,
            {static_cast<uint8_t>(browser_thread_),
             static_cast<uint8_t>(task_type_)}};
  }
~~~

### Parse

Parse
~~~cpp
static const BrowserTaskTraitsExtension Parse(
      const base::TaskTraitsExtensionStorage& extension) {
    return BrowserTaskTraitsExtension(
        static_cast<BrowserThread::ID>(extension.data[0]),
        static_cast<BrowserTaskType>(extension.data[1]));
  }
~~~

### browser_thread

browser_thread
~~~cpp
constexpr BrowserThread::ID browser_thread() const {
    // TODO(1026641): Migrate to BrowserTaskTraits under which BrowserThread is
    // not a trait. Until then, only code that knows traits have explicitly set
    // the BrowserThread trait should check this field.
    DCHECK_NE(browser_thread_, BrowserThread::ID_COUNT);
    return browser_thread_;
  }
~~~

### task_type

task_type
~~~cpp
constexpr BrowserTaskType task_type() const { return task_type_; }
~~~
