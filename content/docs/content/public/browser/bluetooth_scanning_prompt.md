
## class BluetoothScanningPrompt
 Represents a way to ask the user permission to allow a site to receive
 Bluetooth advertisement packets from Bluetooth devices.

### ~BluetoothScanningPrompt

BluetoothScanningPrompt::~BluetoothScanningPrompt
~~~cpp
~BluetoothScanningPrompt()
~~~

### AddOrUpdateDevice

AddOrUpdateDevice
~~~cpp
virtual void AddOrUpdateDevice(const std::string& device_id,
                                 bool should_update_name,
                                 const std::u16string& device_name) {}
~~~
 Adds a new device to the prompt or updates the information of an
 existing device.


 Sometimes when a Bluetooth device stops advertising, the |device_name|
 can be invalid, and in that case |should_update_name| will be set
 false.
