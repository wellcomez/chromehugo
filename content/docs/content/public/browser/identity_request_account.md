
## struct IdentityRequestAccount
 Represents a federated user account which is used when displaying the FedCM
 account selector.

### IdentityRequestAccount

IdentityRequestAccount::IdentityRequestAccount
~~~cpp
IdentityRequestAccount(
      const std::string& id,
      const std::string& email,
      const std::string& name,
      const std::string& given_name,
      const GURL& picture,
      absl::optional<LoginState> login_state = absl::nullopt)
~~~

### IdentityRequestAccount

IdentityRequestAccount::IdentityRequestAccount
~~~cpp
IdentityRequestAccount(const IdentityRequestAccount&)
~~~

### ~IdentityRequestAccount

IdentityRequestAccount::~IdentityRequestAccount
~~~cpp
~IdentityRequestAccount()
~~~
