
## class ZoomLevelDelegate
 An interface to allow the client to initialize the HostZoomMap.

### InitHostZoomMap

InitHostZoomMap
~~~cpp
virtual void InitHostZoomMap(HostZoomMap* host_zoom_map) = 0;
~~~

### ~ZoomLevelDelegate

~ZoomLevelDelegate
~~~cpp
virtual ~ZoomLevelDelegate() {}
~~~

### InitHostZoomMap

InitHostZoomMap
~~~cpp
virtual void InitHostZoomMap(HostZoomMap* host_zoom_map) = 0;
~~~

### ~ZoomLevelDelegate

~ZoomLevelDelegate
~~~cpp
virtual ~ZoomLevelDelegate() {}
~~~
