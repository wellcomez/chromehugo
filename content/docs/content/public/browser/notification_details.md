
## class NotificationDetails
 Do not declare a NotificationDetails directly--use either
 "Details<detailsclassname>(detailsclasspointer)" or
 NotificationService::NoDetails().

### NotificationDetails

NotificationDetails
~~~cpp
NotificationDetails(const NotificationDetails& other) : ptr_(other.ptr_) {}
~~~

### ~NotificationDetails

~NotificationDetails
~~~cpp
~NotificationDetails() {}
~~~

### map_key

map_key
~~~cpp
uintptr_t map_key() const { return reinterpret_cast<uintptr_t>(ptr_.get()); }
~~~
 NotificationDetails can be used as the index for a map; this method
 returns the pointer to the current details as an identifier, for use as a
 map index.

### operator!=

operator!=
~~~cpp
bool operator!=(const NotificationDetails& other) const {
    return ptr_ != other.ptr_;
  }
~~~

### operator==

operator==
~~~cpp
bool operator==(const NotificationDetails& other) const {
    return ptr_ == other.ptr_;
  }
~~~

## class Details

### Details

Details
~~~cpp
Details(T* ptr) : NotificationDetails(ptr) {}
~~~
 TODO(erg): Our code hard relies on implicit conversion
### Details

Details
~~~cpp
Details(const NotificationDetails& other)      // NOLINT
    : NotificationDetails(other) {}
~~~
 NOLINT
### operator-&gt;

operator-&gt;
~~~cpp
T* operator->() const { return ptr(); }
~~~

### ptr

ptr
~~~cpp
T* ptr() const { return static_cast<T*>(const_cast<void*>(ptr_.get())); }
~~~
 The casts here allow this to compile with both T = Foo and T = const Foo.

### Details

Details
~~~cpp
Details(T* ptr) : NotificationDetails(ptr) {}
~~~
 TODO(erg): Our code hard relies on implicit conversion
### Details

Details
~~~cpp
Details(const NotificationDetails& other)      // NOLINT
    : NotificationDetails(other) {}
~~~
 NOLINT
### operator-&gt;

operator-&gt;
~~~cpp
T* operator->() const { return ptr(); }
~~~

### ptr

ptr
~~~cpp
T* ptr() const { return static_cast<T*>(const_cast<void*>(ptr_.get())); }
~~~
 The casts here allow this to compile with both T = Foo and T = const Foo.
