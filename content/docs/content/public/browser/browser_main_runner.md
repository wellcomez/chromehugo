
## class BrowserMainRunner
 This class is responsible for browser initialization, running and shutdown.

### Create

BrowserMainRunner::Create
~~~cpp
static std::unique_ptr<BrowserMainRunner> Create();
~~~
 Create a new BrowserMainRunner object.

### ExitedMainMessageLoop

BrowserMainRunner::ExitedMainMessageLoop
~~~cpp
static bool ExitedMainMessageLoop();
~~~
 Returns true if the BrowserMainRunner has exited the main loop.

### Initialize

BrowserMainRunner::Initialize
~~~cpp
virtual int Initialize(content::MainFunctionParams parameters) = 0;
~~~
 Initialize all necessary browser state. Returning a non-negative value
 indicates that initialization failed, and the returned value is used as
 the exit code for the process.

### SynchronouslyFlushStartupTasks

BrowserMainRunner::SynchronouslyFlushStartupTasks
~~~cpp
virtual void SynchronouslyFlushStartupTasks() = 0;
~~~
 Run all queued startup tasks. Only defined on Android because other
 platforms run startup tasks immediately.

### Run

BrowserMainRunner::Run
~~~cpp
virtual int Run() = 0;
~~~
 BUILDFLAG(IS_ANDROID)
 Perform the default run logic.

### Shutdown

BrowserMainRunner::Shutdown
~~~cpp
virtual void Shutdown() = 0;
~~~
 Shut down the browser state.
