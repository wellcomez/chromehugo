
## class RenderProcessHostFactory
 Factory object for RenderProcessHosts. Using this factory allows tests to
 swap out a different one to use a TestRenderProcessHost.

### ~RenderProcessHostFactory

~RenderProcessHostFactory
~~~cpp
virtual ~RenderProcessHostFactory() {}
~~~

### CreateRenderProcessHost

CreateRenderProcessHost
~~~cpp
virtual RenderProcessHost* CreateRenderProcessHost(
      BrowserContext* browser_context,
      SiteInstance* site_instance) = 0;
~~~

### ~RenderProcessHostFactory

~RenderProcessHostFactory
~~~cpp
virtual ~RenderProcessHostFactory() {}
~~~

### CreateRenderProcessHost

CreateRenderProcessHost
~~~cpp
virtual RenderProcessHost* CreateRenderProcessHost(
      BrowserContext* browser_context,
      SiteInstance* site_instance) = 0;
~~~
