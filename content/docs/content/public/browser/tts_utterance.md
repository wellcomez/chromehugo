### IsFinalTtsEventType

IsFinalTtsEventType
~~~cpp
CONTENT_EXPORT bool IsFinalTtsEventType(TtsEventType event_type);
~~~
 Returns true if this event type is one that indicates an utterance
 is finished and can be destroyed.

## class UtteranceEventDelegate
 Class that wants to receive events on utterances.

### OnTtsEvent

UtteranceEventDelegate::OnTtsEvent
~~~cpp
virtual void OnTtsEvent(TtsUtterance* utterance,
                          TtsEventType event_type,
                          int char_index,
                          int length,
                          const std::string& error_message) = 0;
~~~
 Called when the engine reaches a TTS event in an utterance. If |char_index|
 or |length| are invalid or not applicable for the given |event_type|, they
 should be set to -1.

## class TtsUtterance
 One speech utterance.

### Create

TtsUtterance::Create
~~~cpp
static std::unique_ptr<TtsUtterance> Create(
      BrowserContext* browser_context,
      bool should_always_be_spoken = false);
~~~
 |should_always_be_spoken|: See comment for ShouldAlwaysBeSpoken().

### Create

TtsUtterance::Create
~~~cpp
static std::unique_ptr<TtsUtterance> Create();
~~~

### ~TtsUtterance

~TtsUtterance
~~~cpp
virtual ~TtsUtterance() = default;
~~~

### OnTtsEvent

TtsUtterance::OnTtsEvent
~~~cpp
virtual void OnTtsEvent(TtsEventType event_type,
                          int char_index,
                          int length,
                          const std::string& error_message) = 0;
~~~
 Sends an event to the delegate. If the event type is TTS_EVENT_END
 or TTS_EVENT_ERROR, deletes the utterance. If |char_index| is -1,
 uses the last good value. If |length| is -1, that represents an unknown
 length, and will simply be passed to the delegate as -1.

### Finish

TtsUtterance::Finish
~~~cpp
virtual void Finish() = 0;
~~~
 Finish an utterance without sending an event to the delegate.

### SetText

TtsUtterance::SetText
~~~cpp
virtual void SetText(const std::string& text) = 0;
~~~
 Getters and setters for the text to speak and other speech options.

### GetText

TtsUtterance::GetText
~~~cpp
virtual const std::string& GetText() = 0;
~~~

### SetOptions

TtsUtterance::SetOptions
~~~cpp
virtual void SetOptions(base::Value::Dict options) = 0;
~~~

### GetOptions

TtsUtterance::GetOptions
~~~cpp
virtual const base::Value::Dict* GetOptions() = 0;
~~~

### SetSrcId

TtsUtterance::SetSrcId
~~~cpp
virtual void SetSrcId(int src_id) = 0;
~~~

### GetSrcId

TtsUtterance::GetSrcId
~~~cpp
virtual int GetSrcId() = 0;
~~~

### SetSrcUrl

TtsUtterance::SetSrcUrl
~~~cpp
virtual void SetSrcUrl(const GURL& src_url) = 0;
~~~

### GetSrcUrl

TtsUtterance::GetSrcUrl
~~~cpp
virtual const GURL& GetSrcUrl() = 0;
~~~

### SetVoiceName

TtsUtterance::SetVoiceName
~~~cpp
virtual void SetVoiceName(const std::string& voice_name) = 0;
~~~

### GetVoiceName

TtsUtterance::GetVoiceName
~~~cpp
virtual const std::string& GetVoiceName() = 0;
~~~

### SetLang

TtsUtterance::SetLang
~~~cpp
virtual void SetLang(const std::string& lang) = 0;
~~~

### GetLang

TtsUtterance::GetLang
~~~cpp
virtual const std::string& GetLang() = 0;
~~~

### SetContinuousParameters

TtsUtterance::SetContinuousParameters
~~~cpp
virtual void SetContinuousParameters(const double rate,
                                       const double pitch,
                                       const double volume) = 0;
~~~

### GetContinuousParameters

TtsUtterance::GetContinuousParameters
~~~cpp
virtual const UtteranceContinuousParameters& GetContinuousParameters() = 0;
~~~

### SetShouldClearQueue

TtsUtterance::SetShouldClearQueue
~~~cpp
virtual void SetShouldClearQueue(bool value) = 0;
~~~
 Prior to processing this utterance, determines whether the utterance queue
 gets cleared.

### GetShouldClearQueue

TtsUtterance::GetShouldClearQueue
~~~cpp
virtual bool GetShouldClearQueue() = 0;
~~~

### SetRequiredEventTypes

TtsUtterance::SetRequiredEventTypes
~~~cpp
virtual void SetRequiredEventTypes(const std::set<TtsEventType>& types) = 0;
~~~

### GetRequiredEventTypes

TtsUtterance::GetRequiredEventTypes
~~~cpp
virtual const std::set<TtsEventType>& GetRequiredEventTypes() = 0;
~~~

### SetDesiredEventTypes

TtsUtterance::SetDesiredEventTypes
~~~cpp
virtual void SetDesiredEventTypes(const std::set<TtsEventType>& types) = 0;
~~~

### GetDesiredEventTypes

TtsUtterance::GetDesiredEventTypes
~~~cpp
virtual const std::set<TtsEventType>& GetDesiredEventTypes() = 0;
~~~

### SetEngineId

TtsUtterance::SetEngineId
~~~cpp
virtual void SetEngineId(const std::string& engine_id) = 0;
~~~

### GetEngineId

TtsUtterance::GetEngineId
~~~cpp
virtual const std::string& GetEngineId() = 0;
~~~

### SetEventDelegate

TtsUtterance::SetEventDelegate
~~~cpp
virtual void SetEventDelegate(UtteranceEventDelegate* event_delegate) = 0;
~~~

### GetEventDelegate

TtsUtterance::GetEventDelegate
~~~cpp
virtual UtteranceEventDelegate* GetEventDelegate() = 0;
~~~

### GetBrowserContext

TtsUtterance::GetBrowserContext
~~~cpp
virtual BrowserContext* GetBrowserContext() = 0;
~~~
 Getters and setters for internal state.

### ClearBrowserContext

TtsUtterance::ClearBrowserContext
~~~cpp
virtual void ClearBrowserContext() = 0;
~~~

### GetId

TtsUtterance::GetId
~~~cpp
virtual int GetId() = 0;
~~~

### IsFinished

TtsUtterance::IsFinished
~~~cpp
virtual bool IsFinished() = 0;
~~~

### GetWebContents

TtsUtterance::GetWebContents
~~~cpp
virtual WebContents* GetWebContents() = 0;
~~~

### ShouldAlwaysBeSpoken

TtsUtterance::ShouldAlwaysBeSpoken
~~~cpp
virtual bool ShouldAlwaysBeSpoken() = 0;
~~~
 An utterance could become invalid (for example, its associated WebContents
 has been destroyed) and therefore should not be spoken when it is
 processed by TtsController from the utterance queue. If this function
 returns true, it guarantees that the utterance must be a valid one and
 should be spoken.
