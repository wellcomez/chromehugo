
## class NotificationRegistrar
 Aids in registering for notifications and ensures that all registered
 notifications are unregistered when the class is destroyed.


 The intended use is that you make a NotificationRegistrar member in your
 class and use it to register your notifications instead of going through the
 notification service directly. It will automatically unregister them for
 you.

### NotificationRegistrar

NotificationRegistrar
~~~cpp
NotificationRegistrar(const NotificationRegistrar&) = delete;
~~~

### operator=

NotificationRegistrar::operator=
~~~cpp
NotificationRegistrar& operator=(const NotificationRegistrar&) = delete;

  ~NotificationRegistrar();
~~~

### Add

NotificationRegistrar::Add
~~~cpp
void Add(NotificationObserver* observer,
           int type,
           const NotificationSource& source);
~~~
 Wrappers around NotificationService::[Add|Remove]Observer.

### Remove

NotificationRegistrar::Remove
~~~cpp
void Remove(NotificationObserver* observer,
              int type,
              const NotificationSource& source);
~~~

### RemoveAll

NotificationRegistrar::RemoveAll
~~~cpp
void RemoveAll();
~~~
 Unregisters all notifications.

### IsEmpty

NotificationRegistrar::IsEmpty
~~~cpp
bool IsEmpty() const;
~~~
 Returns true if no notifications are registered.

### IsRegistered

NotificationRegistrar::IsRegistered
~~~cpp
bool IsRegistered(NotificationObserver* observer,
                    int type,
                    const NotificationSource& source);
~~~
 Returns true if there is already a registered notification with the
 specified details.
