
## struct StoredRelatedApplication
 This class represents the stored related application of the StoredPaymentApp.

### ~StoredRelatedApplication

StoredRelatedApplication::~StoredRelatedApplication
~~~cpp
~StoredRelatedApplication()
~~~

## struct StoredCapabilities
 This class represents the stored capabilities.

### StoredCapabilities

StoredCapabilities::StoredCapabilities
~~~cpp
StoredCapabilities(const StoredCapabilities&)
~~~

### ~StoredCapabilities

StoredCapabilities::~StoredCapabilities
~~~cpp
~StoredCapabilities()
~~~

## struct StoredPaymentApp
 This class represents the stored payment app.

### ~StoredPaymentApp

StoredPaymentApp::~StoredPaymentApp
~~~cpp
~StoredPaymentApp()
~~~
