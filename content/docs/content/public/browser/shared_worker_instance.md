
## class SharedWorkerInstance
 This class hold the necessary information to decide if a shared worker
 connection request (SharedWorkerConnector::Connect()) matches an existing
 shared worker.


 Note: There exist one SharedWorkerInstance per SharedWorkerHost but it's
 possible to have 2 distinct SharedWorkerHost that have an identical
 SharedWorkerInstance. An example is if |url_| or |constructor_origin| has a
 "file:" scheme, which is treated as opaque.

### SharedWorkerInstance

SharedWorkerInstance::SharedWorkerInstance
~~~cpp
SharedWorkerInstance(const SharedWorkerInstance& other)
~~~

### operator=

SharedWorkerInstance::operator=
~~~cpp
SharedWorkerInstance& operator=(const SharedWorkerInstance& other) = delete;
  SharedWorkerInstance& operator=(SharedWorkerInstance&& other) = delete;
  ~SharedWorkerInstance();
~~~

### Matches

SharedWorkerInstance::Matches
~~~cpp
bool Matches(const GURL& url,
               const std::string& name,
               const blink::StorageKey& storage_key) const;
~~~
 Checks if this SharedWorkerInstance matches the passed url, name, and
 constructor origin params according to the SharedWorker constructor steps
 in the HTML spec:
 https://html.spec.whatwg.org/multipage/workers.html#shared-workers-and-the-sharedworker-interface
 Note that we are using StorageKey to represent the constructor origin.

### url

url
~~~cpp
const GURL& url() const { return url_; }
~~~
 Accessors.

### name

name
~~~cpp
const std::string& name() const { return name_; }
~~~

### script_type

script_type
~~~cpp
blink::mojom::ScriptType script_type() const { return script_type_; }
~~~

### credentials_mode

credentials_mode
~~~cpp
network::mojom::CredentialsMode credentials_mode() const {
    return credentials_mode_;
  }
~~~

### storage_key

storage_key
~~~cpp
const blink::StorageKey& storage_key() const { return storage_key_; }
~~~

### creation_context_type

creation_context_type
~~~cpp
blink::mojom::SharedWorkerCreationContextType creation_context_type() const {
    return creation_context_type_;
  }
~~~
