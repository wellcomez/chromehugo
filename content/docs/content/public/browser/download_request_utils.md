
## class DownloadRequestUtils
 Utility methods for download requests.

### IsURLSafe

DownloadRequestUtils::IsURLSafe
~~~cpp
static bool IsURLSafe(int render_process_id, const GURL& url);
~~~
 Returns if the URL passes the security check and can be requested.

 |render_process_id| can be -1 when no renderer process is associated with
 this request.
