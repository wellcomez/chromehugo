
## struct SpeechRecognitionSessionContext
 The context information required by clients of the SpeechRecognitionManager
 and its delegates for mapping the recognition session to other browser
 elements involved with it (e.g., the page element that requested the
 recognition). The manager keeps this struct attached to the recognition
 session during all the session lifetime, making its contents available to
 clients. (In this regard, see SpeechRecognitionManager::GetSessionContext().)
### SpeechRecognitionSessionContext

SpeechRecognitionSessionContext::SpeechRecognitionSessionContext
~~~cpp
SpeechRecognitionSessionContext(const SpeechRecognitionSessionContext& other)
~~~

### ~SpeechRecognitionSessionContext

SpeechRecognitionSessionContext::~SpeechRecognitionSessionContext
~~~cpp
~SpeechRecognitionSessionContext()
~~~
