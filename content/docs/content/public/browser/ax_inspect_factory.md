
## class AXInspectFactory
 Accessibility tree formatters and event recorders factory.

### CreatePlatformRecorder

AXInspectFactory::CreatePlatformRecorder
~~~cpp
static std::unique_ptr<ui::AXEventRecorder> CreatePlatformRecorder(
      BrowserAccessibilityManager* manager = nullptr,
      base::ProcessId pid = 0,
      const ui::AXTreeSelector& selector = {});
~~~
 Creates the appropriate event recorder for the platform we are currently
 running on.

### CreateBlinkFormatter

AXInspectFactory::CreateBlinkFormatter
~~~cpp
static std::unique_ptr<ui::AXTreeFormatter> CreateBlinkFormatter();
~~~
 Creates the internal accessibility tree formatter, AKA the Blink tree
 formatter, which is used to dump the Blink accessibility tree to a string
### CreateFormatter

AXInspectFactory::CreateFormatter
~~~cpp
static std::unique_ptr<ui::AXTreeFormatter> CreateFormatter(
      ui::AXApiType::Type);
~~~
 Creates a tree formatter of a given API type if supported by platform.

### CreateRecorder

AXInspectFactory::CreateRecorder
~~~cpp
static std::unique_ptr<ui::AXEventRecorder> CreateRecorder(
      ui::AXApiType::Type,
      BrowserAccessibilityManager* manager = nullptr,
      base::ProcessId pid = 0,
      const ui::AXTreeSelector& selector = {});
~~~
 Creates an event recorder of a given API type if supported by platform.

### SupportedApis

AXInspectFactory::SupportedApis
~~~cpp
static std::vector<ui::AXApiType::Type> SupportedApis();
~~~
 Returns a list of APIs supported by the current platform