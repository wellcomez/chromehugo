
## class RenderWidgetHostView
 RenderWidgetHostView is an interface implemented by an object that acts as
 the "View" portion of a RenderWidgetHost. The RenderWidgetHost and its
 associated RenderProcessHost own the "Model" in this case which is the
 child renderer process. The View is responsible for receiving events from
 the surrounding environment and passing them to the RenderWidgetHost, and
 for actually displaying the content of the RenderWidgetHost when it
 changes.


 RenderWidgetHostView Class Hierarchy:
   RenderWidgetHostView - Public interface.

   RenderWidgetHostViewBase - Common implementation between platforms.

   RenderWidgetHostViewAura, ... - Platform specific implementations.

### InitAsChild

RenderWidgetHostView::InitAsChild
~~~cpp
virtual void InitAsChild(gfx::NativeView parent_view) = 0;
~~~
 Initialize this object for use as a drawing area.  |parent_view| may be
 left as nullptr on platforms where a parent view is not required to
 initialize a child window.

### GetRenderWidgetHost

RenderWidgetHostView::GetRenderWidgetHost
~~~cpp
virtual RenderWidgetHost* GetRenderWidgetHost() = 0;
~~~
 Returns the associated RenderWidgetHost.

### SetSize

RenderWidgetHostView::SetSize
~~~cpp
virtual void SetSize(const gfx::Size& size) = 0;
~~~
 Tells the View to size itself to the specified size.

### EnableAutoResize

RenderWidgetHostView::EnableAutoResize
~~~cpp
virtual void EnableAutoResize(const gfx::Size& min_size,
                                const gfx::Size& max_size) = 0;
~~~
 Instructs the View to automatically resize and send back updates
 for the new size.

### DisableAutoResize

RenderWidgetHostView::DisableAutoResize
~~~cpp
virtual void DisableAutoResize(const gfx::Size& new_size) = 0;
~~~
 Turns off auto-resize and gives a new size that the view should be.

### SetBounds

RenderWidgetHostView::SetBounds
~~~cpp
virtual void SetBounds(const gfx::Rect& rect) = 0;
~~~
 Tells the View to size and move itself to the specified size and point in
 screen space.

### TransformPointToRootCoordSpaceF

RenderWidgetHostView::TransformPointToRootCoordSpaceF
~~~cpp
virtual gfx::PointF TransformPointToRootCoordSpaceF(
      const gfx::PointF& point) = 0;
~~~
 Coordinate points received from a renderer process need to be transformed
 to the top-level frame's coordinate space. For coordinates received from
 the top-level frame's renderer this is a no-op as they are already
 properly transformed; however, coordinates received from an out-of-process
 iframe renderer process require transformation.

### TransformPointToRootCoordSpace

TransformPointToRootCoordSpace
~~~cpp
gfx::Point TransformPointToRootCoordSpace(const gfx::Point& point) {
    return gfx::ToRoundedPoint(
        TransformPointToRootCoordSpaceF(gfx::PointF(point)));
  }
~~~
 A int point variant of the above. Use float version to transform,
 then rounded back to int point.

### TransformRootPointToViewCoordSpace

RenderWidgetHostView::TransformRootPointToViewCoordSpace
~~~cpp
virtual gfx::PointF TransformRootPointToViewCoordSpace(
      const gfx::PointF& point) = 0;
~~~
 Converts a point in the root view's coordinate space to the coordinate
 space of whichever view is used to call this method.

### GetNativeView

RenderWidgetHostView::GetNativeView
~~~cpp
virtual gfx::NativeView GetNativeView() = 0;
~~~
 Retrieves the native view used to contain plugins and identify the
 renderer in IPC messages.

### GetNativeViewAccessible

RenderWidgetHostView::GetNativeViewAccessible
~~~cpp
virtual gfx::NativeViewAccessible GetNativeViewAccessible() = 0;
~~~

### GetTextInputClient

RenderWidgetHostView::GetTextInputClient
~~~cpp
virtual ui::TextInputClient* GetTextInputClient() = 0;
~~~
 Returns a ui::TextInputClient to support text input or nullptr if this RWHV
 doesn't support text input.

 Note: Not all the platforms use ui::InputMethod and ui::TextInputClient for
 text input.  Some platforms (Mac and Android for example) use their own
 text input system.

### Focus

RenderWidgetHostView::Focus
~~~cpp
virtual void Focus() = 0;
~~~
 Set focus to the associated View component.

### HasFocus

RenderWidgetHostView::HasFocus
~~~cpp
virtual bool HasFocus() = 0;
~~~
 Returns true if the View currently has the focus.

### Show

RenderWidgetHostView::Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows/hides the view.  These must always be called together in pairs.

 It is not legal to call Hide() multiple times in a row.

### Hide

RenderWidgetHostView::Hide
~~~cpp
virtual void Hide() = 0;
~~~

### IsShowing

RenderWidgetHostView::IsShowing
~~~cpp
virtual bool IsShowing() = 0;
~~~
 Whether the view is showing.

### WasUnOccluded

RenderWidgetHostView::WasUnOccluded
~~~cpp
virtual void WasUnOccluded() = 0;
~~~
 Indicates if the view is currently occluded (e.g, not visible because it's
 covered up by other windows), and as a result the view's renderer may be
 suspended. Calling Show()/Hide() overrides the state set by these methods.

### WasOccluded

RenderWidgetHostView::WasOccluded
~~~cpp
virtual void WasOccluded() = 0;
~~~

### GetViewBounds

RenderWidgetHostView::GetViewBounds
~~~cpp
virtual gfx::Rect GetViewBounds() = 0;
~~~
 Retrieve the bounds of the View, in screen coordinates.

### GetSelectedText

RenderWidgetHostView::GetSelectedText
~~~cpp
virtual std::u16string GetSelectedText() = 0;
~~~
 Returns the currently selected text in both of editable text fields and
 non-editable texts.

### GetTouchSelectionControllerClientManager

RenderWidgetHostView::GetTouchSelectionControllerClientManager
~~~cpp
virtual TouchSelectionControllerClientManager*
  GetTouchSelectionControllerClientManager() = 0;
~~~
 This only returns non-null on platforms that implement touch
 selection editing (TSE), currently Aura and Android.

### SetBackgroundColor

RenderWidgetHostView::SetBackgroundColor
~~~cpp
virtual void SetBackgroundColor(SkColor color) = 0;
~~~
 Subclasses should override this method to set the background color. |color|
 has to be either SK_ColorTRANSPARENT or opaque. If set to
 SK_ColorTRANSPARENT, the renderer's background color will be overridden to
 be fully transparent.

 SetBackgroundColor is called to set the default color of the view,
 which is shown if the background color of the renderer is not available.

### GetBackgroundColor

RenderWidgetHostView::GetBackgroundColor
~~~cpp
virtual absl::optional<SkColor> GetBackgroundColor() = 0;
~~~
 GetBackgroundColor returns the current background color of the view.

### CopyBackgroundColorIfPresentFrom

RenderWidgetHostView::CopyBackgroundColorIfPresentFrom
~~~cpp
virtual void CopyBackgroundColorIfPresentFrom(
      const RenderWidgetHostView& other) = 0;
~~~
 Copy background color from another view if other view has background color.

### LockMouse

RenderWidgetHostView::LockMouse
~~~cpp
virtual blink::mojom::PointerLockResult LockMouse(
      bool request_unadjusted_movement) = 0;
~~~
 Return value indicates whether the mouse is locked successfully or a
 reason why it failed.

### ChangeMouseLock

RenderWidgetHostView::ChangeMouseLock
~~~cpp
virtual blink::mojom::PointerLockResult ChangeMouseLock(
      bool request_unadjusted_movement) = 0;
~~~
 Return value indicates whether the MouseLock was changed successfully
 or a reason why the change failed.

### UnlockMouse

RenderWidgetHostView::UnlockMouse
~~~cpp
virtual void UnlockMouse() = 0;
~~~

### IsMouseLocked

RenderWidgetHostView::IsMouseLocked
~~~cpp
virtual bool IsMouseLocked() = 0;
~~~
 Returns true if the mouse pointer is currently locked.

### GetIsMouseLockedUnadjustedMovementForTesting

RenderWidgetHostView::GetIsMouseLockedUnadjustedMovementForTesting
~~~cpp
virtual bool GetIsMouseLockedUnadjustedMovementForTesting() = 0;
~~~
 Get the pointer lock unadjusted movement setting for testing.

 Returns true if mouse is locked and is in unadjusted movement mode.

### CanBeMouseLocked

RenderWidgetHostView::CanBeMouseLocked
~~~cpp
virtual bool CanBeMouseLocked() = 0;
~~~
 Whether the view can trigger pointer lock. This is the same as `HasFocus`
 on non-Mac platforms, but on Mac it also ensures that the window is key.

### LockKeyboard

RenderWidgetHostView::LockKeyboard
~~~cpp
virtual bool LockKeyboard(
      absl::optional<base::flat_set<ui::DomCode>> dom_codes) = 0;
~~~
 Start/Stop intercepting future system keyboard events.

### UnlockKeyboard

RenderWidgetHostView::UnlockKeyboard
~~~cpp
virtual void UnlockKeyboard() = 0;
~~~

### IsKeyboardLocked

RenderWidgetHostView::IsKeyboardLocked
~~~cpp
virtual bool IsKeyboardLocked() = 0;
~~~
 Returns true if keyboard lock is active.

### GetKeyboardLayoutMap

RenderWidgetHostView::GetKeyboardLayoutMap
~~~cpp
virtual base::flat_map<std::string, std::string> GetKeyboardLayoutMap() = 0;
~~~
 Return a mapping dictionary from keyboard code to key values for the
 highest-priority ASCII-capable layout in the list of currently installed
 keyboard layouts.

### GetVisibleViewportSize

RenderWidgetHostView::GetVisibleViewportSize
~~~cpp
virtual gfx::Size GetVisibleViewportSize() = 0;
~~~
 Retrives the size of the viewport for the visible region. May be smaller
 than the view size if a portion of the view is obstructed (e.g. by a
 virtual keyboard).

### SetInsets

RenderWidgetHostView::SetInsets
~~~cpp
virtual void SetInsets(const gfx::Insets& insets) = 0;
~~~
 Set insets for the visible region of the root window. Used to compute the
 visible viewport.

### IsSurfaceAvailableForCopy

RenderWidgetHostView::IsSurfaceAvailableForCopy
~~~cpp
virtual bool IsSurfaceAvailableForCopy() = 0;
~~~
 Returns true if the current display surface is available.

### CopyFromSurface

RenderWidgetHostView::CopyFromSurface
~~~cpp
virtual void CopyFromSurface(
      const gfx::Rect& src_rect,
      const gfx::Size& output_size,
      base::OnceCallback<void(const SkBitmap&)> callback) = 0;
~~~
 Copies the given subset of the view's surface, optionally scales it, and
 returns the result as a bitmap via the provided callback. This is meant for
 one-off snapshots. For continuous video capture of the surface, please use
 `CreateVideoCapturer()` instead.


 `src_rect` is either the subset of the view's surface, in view coordinates,
 or empty to indicate that all of it should be copied. This is NOT the same
 coordinate system as that used `GetViewBounds()` (https://crbug.com/73362).


 `output_size` is the size of the resulting bitmap, or empty to indicate no
 scaling is desired. If an empty size is provided, note that the resulting
 bitmap's size may not be the same as `src_rect.size()` due to the pixel
 scale used by the underlying device.


 `callback` is guaranteed to be run, either synchronously or at some point
 in the future (depending on the platform implementation and the current
 state of the Surface). If the copy failed, the bitmap's `drawsNothing()`
 method will return true. `callback` isn't guaranteed to run on the same
 task sequence as this method was called from.


 If the view's renderer is suspended (see `WasOccluded()`), this may result
 in copying old data or failing.

### EnsureSurfaceSynchronizedForWebTest

RenderWidgetHostView::EnsureSurfaceSynchronizedForWebTest
~~~cpp
virtual void EnsureSurfaceSynchronizedForWebTest() = 0;
~~~
 Ensures that all surfaces are synchronized for the next call to
 CopyFromSurface. This is used by web tests.

### CreateVideoCapturer

RenderWidgetHostView::CreateVideoCapturer
~~~cpp
virtual std::unique_ptr<viz::ClientFrameSinkVideoCapturer>
  CreateVideoCapturer() = 0;
~~~
 Creates a video capturer, which will allow the caller to receive a stream
 of media::VideoFrames captured from this view. The capturer is configured
 to target this view, so there is no need to call ChangeTarget() before
 Start(). See viz.mojom.FrameSinkVideoCapturer for documentation.

### GetScreenInfo

RenderWidgetHostView::GetScreenInfo
~~~cpp
virtual display::ScreenInfo GetScreenInfo() const = 0;
~~~
 This method returns the ScreenInfo used by the view to render. If the
 information is not knowable (e.g, because the view is not attached to a
 screen yet), then a default best-guess will be used.

### GetScreenInfos

RenderWidgetHostView::GetScreenInfos
~~~cpp
virtual display::ScreenInfos GetScreenInfos() const = 0;
~~~
 This method returns the ScreenInfos used by the view to render. If the
 information is not knowable (e.g, because the view is not attached to a
 screen yet), then a default best-guess will be used.

### GetDeviceScaleFactor

RenderWidgetHostView::GetDeviceScaleFactor
~~~cpp
virtual float GetDeviceScaleFactor() const = 0;
~~~
 This must always return the same device scale factor as GetScreenInfo.

### SetActive

RenderWidgetHostView::SetActive
~~~cpp
virtual void SetActive(bool active) = 0;
~~~
 Set the view's active state (i.e., tint state of controls).

### ShowDefinitionForSelection

RenderWidgetHostView::ShowDefinitionForSelection
~~~cpp
virtual void ShowDefinitionForSelection() = 0;
~~~
 Brings up the dictionary showing a definition for the selected text.

### SpeakSelection

RenderWidgetHostView::SpeakSelection
~~~cpp
virtual void SpeakSelection() = 0;
~~~
 Tells the view to speak the currently selected text.

### SetWindowFrameInScreen

RenderWidgetHostView::SetWindowFrameInScreen
~~~cpp
virtual void SetWindowFrameInScreen(const gfx::Rect& rect) = 0;
~~~
 Allows to update the widget's screen rects when it is not attached to
 a window (e.g. in headless mode).

### ShowSharePicker

RenderWidgetHostView::ShowSharePicker
~~~cpp
virtual void ShowSharePicker(
      const std::string& title,
      const std::string& text,
      const std::string& url,
      const std::vector<std::string>& file_paths,
      blink::mojom::ShareService::ShareCallback callback) = 0;
~~~
 Invoked by browser implementation of the navigator.share() to trigger the
 NSSharingServicePicker.


 |title|, |text|, |url| makes up the requested data that is passed to the
 picker after being converted to NSString.

 |file_paths| is the set of paths to files to be shared passed onto the
 picker after being converted to NSURL.

 |callback| returns the result from the NSSharingServicePicker depending
 upon the user's action.

### TakeFallbackContentFrom

RenderWidgetHostView::TakeFallbackContentFrom
~~~cpp
virtual void TakeFallbackContentFrom(RenderWidgetHostView* view) = 0;
~~~
 BUILDFLAG(IS_MAC)
 Indicates that this view should show the contents of |view| if it doesn't
 have anything to show.

### GetVirtualKeyboardMode

RenderWidgetHostView::GetVirtualKeyboardMode
~~~cpp
virtual ui::mojom::VirtualKeyboardMode GetVirtualKeyboardMode() = 0;
~~~
 Returns the virtual keyboard mode requested via author APIs.

### NotifyVirtualKeyboardOverlayRect

RenderWidgetHostView::NotifyVirtualKeyboardOverlayRect
~~~cpp
virtual void NotifyVirtualKeyboardOverlayRect(
      const gfx::Rect& keyboard_rect) = 0;
~~~
 Create a geometrychange event and forward it to the JS with the keyboard
 coordinates. No-op unless VirtualKeyboardMode is kOverlaysContent.

### IsHTMLFormPopup

RenderWidgetHostView::IsHTMLFormPopup
~~~cpp
virtual bool IsHTMLFormPopup() const = 0;
~~~
 Returns true if this widget is a HTML popup, e.g. a <select> menu.
