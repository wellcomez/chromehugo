
## class FederatedIdentityPermissionContextDelegate
 Delegate interface for the FedCM implementation in content to query and
 manage permission grants associated with the ability to share identity
 information from a given provider to a given relying party.

###  OnIdpSigninStatusChanged

 Observes IdP sign-in status changes.

~~~cpp
class IdpSigninStatusObserver : public base::CheckedObserver {
   public:
    virtual void OnIdpSigninStatusChanged(const url::Origin& idp_origin,
                                          bool idp_signin_status) = 0;

   protected:
    IdpSigninStatusObserver() = default;
    ~IdpSigninStatusObserver() override = default;
  };
~~~
### FederatedIdentityPermissionContextDelegate

FederatedIdentityPermissionContextDelegate
~~~cpp
FederatedIdentityPermissionContextDelegate() = default;
~~~

### ~FederatedIdentityPermissionContextDelegate

~FederatedIdentityPermissionContextDelegate
~~~cpp
virtual ~FederatedIdentityPermissionContextDelegate() = default;
~~~

### AddIdpSigninStatusObserver

AddIdpSigninStatusObserver
~~~cpp
virtual void AddIdpSigninStatusObserver(
      IdpSigninStatusObserver* observer) = 0;
~~~
 Adds/removes observer for IdP sign-in status.

### RemoveIdpSigninStatusObserver

RemoveIdpSigninStatusObserver
~~~cpp
virtual void RemoveIdpSigninStatusObserver(
      IdpSigninStatusObserver* observer) = 0;
~~~

### HasActiveSession

HasActiveSession
~~~cpp
virtual bool HasActiveSession(const url::Origin& relying_party_requester,
                                const url::Origin& identity_provider,
                                const std::string& account_identifier) = 0;
~~~
 Determine whether the `relying_party_requester` has an existing active
 session for the specified `account_identifier` with the
 `identity_provider`.

### GrantActiveSession

GrantActiveSession
~~~cpp
virtual void GrantActiveSession(const url::Origin& relying_party_requester,
                                  const url::Origin& identity_provider,
                                  const std::string& account_identifier) = 0;
~~~
 Grant active session capabilities between the `relying_party_requester` and
 `identity_provider` origins for the specified account.

### RevokeActiveSession

RevokeActiveSession
~~~cpp
virtual void RevokeActiveSession(const url::Origin& relying_party_requester,
                                   const url::Origin& identity_provider,
                                   const std::string& account_identifier) = 0;
~~~
 Revoke a previously-provided grant from the `relying_party_requester` to
 the `identity_provider` for the specified account.

### HasSharingPermission

HasSharingPermission
~~~cpp
virtual bool HasSharingPermission(const url::Origin& relying_party_requester,
                                    const url::Origin& relying_party_embedder,
                                    const url::Origin& identity_provider,
                                    const std::string& account_id) = 0;
~~~
 Determine whether there is an existing permission grant to share identity
 information for the given account to the `relying_party_requester` when
 embedded in `relying_party_embedder`.

### GrantSharingPermission

GrantSharingPermission
~~~cpp
virtual void GrantSharingPermission(
      const url::Origin& relying_party_requester,
      const url::Origin& relying_party_embedder,
      const url::Origin& identity_provider,
      const std::string& account_id) = 0;
~~~
 Grants permission to share identity information for the given account to
 `relying_party_requester` when embedded in `relying_party_embedder`.

### GetIdpSigninStatus

GetIdpSigninStatus
~~~cpp
virtual absl::optional<bool> GetIdpSigninStatus(
      const url::Origin& idp_origin) = 0;
~~~
 Returns whether the user is signed in with the IDP. If unknown, return
 absl::nullopt.

### SetIdpSigninStatus

SetIdpSigninStatus
~~~cpp
virtual void SetIdpSigninStatus(const url::Origin& idp_origin,
                                  bool idp_signin_status) = 0;
~~~
 Updates the IDP sign-in status. This could be called by
   1. IdpSigninStatus API
   2. fetching accounts response callback
### GetRegisteredIdPs

GetRegisteredIdPs
~~~cpp
virtual std::vector<GURL> GetRegisteredIdPs() = 0;
~~~
 Returns all origins that are registered as IDP.

### RegisterIdP

RegisterIdP
~~~cpp
virtual void RegisterIdP(const GURL& url) = 0;
~~~
 Registers an IdP.

### UnregisterIdP

UnregisterIdP
~~~cpp
virtual void UnregisterIdP(const GURL& url) = 0;
~~~
 Unregisters an IdP.

### FederatedIdentityPermissionContextDelegate

FederatedIdentityPermissionContextDelegate
~~~cpp
FederatedIdentityPermissionContextDelegate() = default;
~~~

### ~FederatedIdentityPermissionContextDelegate

~FederatedIdentityPermissionContextDelegate
~~~cpp
virtual ~FederatedIdentityPermissionContextDelegate() = default;
~~~

### AddIdpSigninStatusObserver

AddIdpSigninStatusObserver
~~~cpp
virtual void AddIdpSigninStatusObserver(
      IdpSigninStatusObserver* observer) = 0;
~~~
 Adds/removes observer for IdP sign-in status.

### RemoveIdpSigninStatusObserver

RemoveIdpSigninStatusObserver
~~~cpp
virtual void RemoveIdpSigninStatusObserver(
      IdpSigninStatusObserver* observer) = 0;
~~~

### HasActiveSession

HasActiveSession
~~~cpp
virtual bool HasActiveSession(const url::Origin& relying_party_requester,
                                const url::Origin& identity_provider,
                                const std::string& account_identifier) = 0;
~~~
 Determine whether the `relying_party_requester` has an existing active
 session for the specified `account_identifier` with the
 `identity_provider`.

### GrantActiveSession

GrantActiveSession
~~~cpp
virtual void GrantActiveSession(const url::Origin& relying_party_requester,
                                  const url::Origin& identity_provider,
                                  const std::string& account_identifier) = 0;
~~~
 Grant active session capabilities between the `relying_party_requester` and
 `identity_provider` origins for the specified account.

### RevokeActiveSession

RevokeActiveSession
~~~cpp
virtual void RevokeActiveSession(const url::Origin& relying_party_requester,
                                   const url::Origin& identity_provider,
                                   const std::string& account_identifier) = 0;
~~~
 Revoke a previously-provided grant from the `relying_party_requester` to
 the `identity_provider` for the specified account.

### HasSharingPermission

HasSharingPermission
~~~cpp
virtual bool HasSharingPermission(const url::Origin& relying_party_requester,
                                    const url::Origin& relying_party_embedder,
                                    const url::Origin& identity_provider,
                                    const std::string& account_id) = 0;
~~~
 Determine whether there is an existing permission grant to share identity
 information for the given account to the `relying_party_requester` when
 embedded in `relying_party_embedder`.

### GrantSharingPermission

GrantSharingPermission
~~~cpp
virtual void GrantSharingPermission(
      const url::Origin& relying_party_requester,
      const url::Origin& relying_party_embedder,
      const url::Origin& identity_provider,
      const std::string& account_id) = 0;
~~~
 Grants permission to share identity information for the given account to
 `relying_party_requester` when embedded in `relying_party_embedder`.

### GetIdpSigninStatus

GetIdpSigninStatus
~~~cpp
virtual absl::optional<bool> GetIdpSigninStatus(
      const url::Origin& idp_origin) = 0;
~~~
 Returns whether the user is signed in with the IDP. If unknown, return
 absl::nullopt.

### SetIdpSigninStatus

SetIdpSigninStatus
~~~cpp
virtual void SetIdpSigninStatus(const url::Origin& idp_origin,
                                  bool idp_signin_status) = 0;
~~~
 Updates the IDP sign-in status. This could be called by
   1. IdpSigninStatus API
   2. fetching accounts response callback
### GetRegisteredIdPs

GetRegisteredIdPs
~~~cpp
virtual std::vector<GURL> GetRegisteredIdPs() = 0;
~~~
 Returns all origins that are registered as IDP.

### RegisterIdP

RegisterIdP
~~~cpp
virtual void RegisterIdP(const GURL& url) = 0;
~~~
 Registers an IdP.

### UnregisterIdP

UnregisterIdP
~~~cpp
virtual void UnregisterIdP(const GURL& url) = 0;
~~~
 Unregisters an IdP.

###  OnIdpSigninStatusChanged

 Observes IdP sign-in status changes.

~~~cpp
class IdpSigninStatusObserver : public base::CheckedObserver {
   public:
    virtual void OnIdpSigninStatusChanged(const url::Origin& idp_origin,
                                          bool idp_signin_status) = 0;

   protected:
    IdpSigninStatusObserver() = default;
    ~IdpSigninStatusObserver() override = default;
  };
~~~