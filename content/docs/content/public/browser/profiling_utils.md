### AskAllChildrenToDumpProfilingData

AskAllChildrenToDumpProfilingData
~~~cpp
CONTENT_EXPORT void AskAllChildrenToDumpProfilingData(
    base::OnceClosure callback);
~~~
 Ask all the child processes to dump their profiling data to disk and calls
 |callback| once it's done.

