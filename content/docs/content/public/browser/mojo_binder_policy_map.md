
## class MojoBinderPolicyMap
 Used by content/ layer to manage interfaces' binding policies. Embedders can
 set their own policies via this interface.

 TODO(https://crbug.com/1157334): Consider integrating it with
 mojo::BinderMap.

### ~MojoBinderPolicyMap

~MojoBinderPolicyMap
~~~cpp
virtual ~MojoBinderPolicyMap() = default;
~~~

### SetAssociatedPolicy

SetAssociatedPolicy
~~~cpp
void SetAssociatedPolicy(MojoBinderAssociatedPolicy policy) {
    SetPolicyByName(Interface::Name_, policy);
  }
~~~

### SetNonAssociatedPolicy

SetNonAssociatedPolicy
~~~cpp
void SetNonAssociatedPolicy(MojoBinderNonAssociatedPolicy policy) {
    SetPolicyByName(Interface::Name_, policy);
  }
~~~

### SetPolicyByName

MojoBinderPolicyMap::SetPolicyByName
~~~cpp
virtual void SetPolicyByName(const base::StringPiece& name,
                               MojoBinderNonAssociatedPolicy policy) = 0;
~~~
