
## class ServiceWorkerContext
 Represents the per-StoragePartition service worker data.


 See service_worker_context_wrapper.cc for the implementation
 of ServiceWorkerContext and ServiceWorkerContextWrapper (the
 primary implementation of this abstract class).


 All methods must be called on the UI thread.

### ScopeMatches

ServiceWorkerContext::ScopeMatches
~~~cpp
static bool ScopeMatches(const GURL& scope, const GURL& url);
~~~
 Returns true if |url| is within the service worker |scope|.

### RunTask

ServiceWorkerContext::RunTask
~~~cpp
static void RunTask(scoped_refptr<base::SequencedTaskRunner> runner,
                      const base::Location& from_here,
                      ServiceWorkerContext* service_worker_context,
                      base::OnceClosure task);
~~~
 Runs a |task| on task |runner| making sure that
 |service_worker_context| is alive while the task is being run.

### AddObserver

ServiceWorkerContext::AddObserver
~~~cpp
virtual void AddObserver(ServiceWorkerContextObserver* observer) = 0;
~~~

### RemoveObserver

ServiceWorkerContext::RemoveObserver
~~~cpp
virtual void RemoveObserver(ServiceWorkerContextObserver* observer) = 0;
~~~

### RegisterServiceWorker

ServiceWorkerContext::RegisterServiceWorker
~~~cpp
virtual void RegisterServiceWorker(
      const GURL& script_url,
      const blink::StorageKey& key,
      const blink::mojom::ServiceWorkerRegistrationOptions& options,
      StatusCodeCallback callback) = 0;
~~~
 Equivalent to calling navigator.serviceWorker.register(script_url,
 options) for a given `key`. `callback` is passed true when the JS promise
 is fulfilled or false when the JS promise is rejected.


 The registration can fail if:
  * `script_url` is on a different origin from `scope`
  * Fetching `script_url` fails.

  * `script_url` fails to parse or its top-level execution fails.

  * Something unexpected goes wrong, like a renderer crash or a full disk.

### UnregisterServiceWorker

ServiceWorkerContext::UnregisterServiceWorker
~~~cpp
virtual void UnregisterServiceWorker(const GURL& scope,
                                       const blink::StorageKey& key,
                                       ResultCallback callback) = 0;
~~~
 Equivalent to calling ServiceWorkerRegistration#unregister on the
 registration for `scope`. `callback` is passed true when the JS promise is
 fulfilled or false when the JS promise is rejected.


 Unregistration can fail if:
  * No registration exists for `scope`.

  * Something unexpected goes wrong, like a renderer crash.

### StartingExternalRequest

ServiceWorkerContext::StartingExternalRequest
~~~cpp
virtual ServiceWorkerExternalRequestResult StartingExternalRequest(
      int64_t service_worker_version_id,
      ServiceWorkerExternalRequestTimeoutType timeout_type,
      const std::string& request_uuid) = 0;
~~~
 Mechanism for embedder to increment/decrement ref count of a service
 worker.


 Embedders can call StartingExternalRequest() while it is performing some
 work with the worker. The worker is considered to be working until embedder
 calls FinishedExternalRequest(). This ensures that content/ does not
 shut the worker down while embedder is expecting the worker to be kept
 alive.

### FinishedExternalRequest

ServiceWorkerContext::FinishedExternalRequest
~~~cpp
virtual ServiceWorkerExternalRequestResult FinishedExternalRequest(
      int64_t service_worker_version_id,
      const std::string& request_uuid) = 0;
~~~

### CountExternalRequestsForTest

ServiceWorkerContext::CountExternalRequestsForTest
~~~cpp
virtual size_t CountExternalRequestsForTest(const blink::StorageKey& key) = 0;
~~~
 Returns the pending external request count for the worker with the
 specified `key`.

### ExecuteScriptForTest

ServiceWorkerContext::ExecuteScriptForTest
~~~cpp
virtual bool ExecuteScriptForTest(
      const std::string& script,
      int64_t service_worker_version_id,
      ServiceWorkerScriptExecutionCallback callback) = 0;
~~~
 Executes the given `script` in the context of the worker specified by the
 given `service_worker_version_id`. If non-empty, `callback` is invoked
 with the result of the script. See also service_worker.mojom.

### MaybeHasRegistrationForStorageKey

ServiceWorkerContext::MaybeHasRegistrationForStorageKey
~~~cpp
virtual bool MaybeHasRegistrationForStorageKey(
      const blink::StorageKey& key) = 0;
~~~
 Whether `key` has any registrations. Uninstalling and uninstalled
 registrations do not cause this to return true, that is, only registrations
 with status ServiceWorkerRegistration::Status::kIntact are considered, such
 as even if the corresponding live registrations may still exist. Also,
 returns true if it doesn't know (registrations are not yet initialized).

### GetAllStorageKeysInfo

ServiceWorkerContext::GetAllStorageKeysInfo
~~~cpp
virtual void GetAllStorageKeysInfo(GetUsageInfoCallback callback) = 0;
~~~
 Used in response to browsing data and quota manager requests to get
 the per-StorageKey size and last time used data.

### DeleteForStorageKey

ServiceWorkerContext::DeleteForStorageKey
~~~cpp
virtual void DeleteForStorageKey(const blink::StorageKey& key,
                                   ResultCallback callback) = 0;
~~~
 Deletes all registrations for `key` and clears all service workers
 belonging to the registrations. All clients controlled by those service
 workers will lose their controllers immediately after this operation.

### CheckHasServiceWorker

ServiceWorkerContext::CheckHasServiceWorker
~~~cpp
virtual void CheckHasServiceWorker(
      const GURL& url,
      const blink::StorageKey& key,
      CheckHasServiceWorkerCallback callback) = 0;
~~~
 Returns ServiceWorkerCapability describing existence and properties of a
 Service Worker registration matching `url` and `key`. In case the service
 worker is being installed as of calling this method, it will wait for the
 installation to finish before coming back with the result.

### CheckOfflineCapability

ServiceWorkerContext::CheckOfflineCapability
~~~cpp
virtual void CheckOfflineCapability(
      const GURL& url,
      const blink::StorageKey& key,
      CheckOfflineCapabilityCallback callback) = 0;
~~~
 Simulates a navigation request in the offline state and dispatches a fetch
 event. Returns OfflineCapability::kSupported and the registration id if
 the response's status code is 200.


 TODO(hayato): Re-visit to integrate this function with
 |ServiceWorkerContext::CheckHasServiceWorker|.

### ClearAllServiceWorkersForTest

ServiceWorkerContext::ClearAllServiceWorkersForTest
~~~cpp
virtual void ClearAllServiceWorkersForTest(base::OnceClosure callback) = 0;
~~~
 Stops all running service workers and unregisters all service worker
 registrations. This method is used in web tests to make sure that the
 existing service worker will not affect the succeeding tests.

### StartWorkerForScope

ServiceWorkerContext::StartWorkerForScope
~~~cpp
virtual void StartWorkerForScope(const GURL& scope,
                                   const blink::StorageKey& key,
                                   StartWorkerCallback info_callback,
                                   StatusCodeCallback failure_callback) = 0;
~~~
 Starts the active worker of the registration for the given `scope` and
 `key`. If there is no active worker, starts the installing worker.

 `info_callback` is passed information about the started worker if
 successful, otherwise `failure_callback` is passed information about the
 error.


 There is no guarantee about whether the callback is called synchronously or
 asynchronously.

### StartServiceWorkerAndDispatchMessage

ServiceWorkerContext::StartServiceWorkerAndDispatchMessage
~~~cpp
virtual void StartServiceWorkerAndDispatchMessage(
      const GURL& scope,
      const blink::StorageKey& key,
      blink::TransferableMessage message,
      ResultCallback result_callback) = 0;
~~~
 Starts the active worker of the registration for the given `scope` and
 `key` and dispatches the given `message` to the service worker.

 `result_callback` is passed a success boolean indicating whether the
 message was dispatched successfully.

### StartServiceWorkerForNavigationHint

ServiceWorkerContext::StartServiceWorkerForNavigationHint
~~~cpp
virtual void StartServiceWorkerForNavigationHint(
      const GURL& document_url,
      const blink::StorageKey& key,
      StartServiceWorkerForNavigationHintCallback callback) = 0;
~~~
 Starts the service worker for `document_url` and `key`. Called when a
 navigation to that URL is predicted to occur soon.

### StopAllServiceWorkersForStorageKey

ServiceWorkerContext::StopAllServiceWorkersForStorageKey
~~~cpp
virtual void StopAllServiceWorkersForStorageKey(
      const blink::StorageKey& key) = 0;
~~~
 Stops all running workers on the given `key`.

### StopAllServiceWorkers

ServiceWorkerContext::StopAllServiceWorkers
~~~cpp
virtual void StopAllServiceWorkers(base::OnceClosure callback) = 0;
~~~
 Stops all running service workers.

### GetRunningServiceWorkerInfos

ServiceWorkerContext::GetRunningServiceWorkerInfos
~~~cpp
virtual const base::flat_map<int64_t /* version_id */,
                               ServiceWorkerRunningInfo>&
  GetRunningServiceWorkerInfos() = 0;
~~~
 Gets info about all running workers.

### IsLiveRunningServiceWorker

ServiceWorkerContext::IsLiveRunningServiceWorker
~~~cpp
virtual bool IsLiveRunningServiceWorker(
      int64_t service_worker_version_id) = 0;
~~~
 Returns true if the ServiceWorkerVersion for `service_worker_version_id` is
 live and running.

### GetRemoteInterfaces

ServiceWorkerContext::GetRemoteInterfaces
~~~cpp
virtual service_manager::InterfaceProvider& GetRemoteInterfaces(
      int64_t service_worker_version_id) = 0;
~~~
 Returns the InterfaceProvider for the worker specified by
 `service_worker_version_id`. The caller can use InterfaceProvider to bind
 interfaces exposed by the Service Worker. CHECKs if
 `IsLiveRunningServiceWorker()` returns false.

### ~ServiceWorkerContext

~ServiceWorkerContext
~~~cpp
virtual ~ServiceWorkerContext() {}
~~~
