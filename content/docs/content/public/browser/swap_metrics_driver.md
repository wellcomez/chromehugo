
## class SwapMetricsDriver::Delegate
 Delegate class that handles the metrics computed by SwapMetricsDriver.

### operator=

Delegate::SwapMetricsDriver::operator=
~~~cpp
Delegate& operator=(const Delegate&) = delete;
~~~

### ~Delegate

Delegate::SwapMetricsDriver::~Delegate
~~~cpp
~Delegate()
~~~

### OnSwapInCount

OnSwapInCount
~~~cpp
virtual void OnSwapInCount(uint64_t count, base::TimeDelta interval) {}
~~~

### OnSwapOutCount

OnSwapOutCount
~~~cpp
virtual void OnSwapOutCount(uint64_t count, base::TimeDelta interval) {}
~~~

### OnDecompressedPageCount

OnDecompressedPageCount
~~~cpp
virtual void OnDecompressedPageCount(uint64_t count,
                                         base::TimeDelta interval) {}
~~~

### OnCompressedPageCount

OnCompressedPageCount
~~~cpp
virtual void OnCompressedPageCount(uint64_t count,
                                       base::TimeDelta interval) {}
~~~

### OnUpdateMetricsFailed

OnUpdateMetricsFailed
~~~cpp
virtual void OnUpdateMetricsFailed() {}
~~~

## class SwapMetricsDriver
 This class collects metrics about the system's swapping behavior and provides
 these metrics to an associated delegate. Metrics can be platform-specific.


 Updated swap metrics can be obtained through Delegate methods in either a
 push-based manner, driven by a periodic base::Timer, or in a synchronous,
 pull-based manner. For receiving periodic updates at a regular frequency, use
 Start() to begin recieiving updates, and Stop() to end them. For obtaining
 updates at specified times, for example over a specific interval or event,
 use InitializeMetrics() at the start, and UpdateMetrics() at any subsequent
 time to immediately and synchronously receive updates. In the case of
 periodic updates, UpdateMetrics() can also be called after Stop() to retrieve
 updated metrics since the last update.


 The SwapMetricsDriver API is not thread safe. The Delegate methods run on
 either the sequence on which Start() was called, in the case of periodic
 updates, or the sequence InitializeMetrics() is called on in the case of
 using pull-based updates. In either case, metrics must always be updated on
 the same sequence and subsequent invocations of this API's methods must be
 made on that sequence.

### ~SwapMetricsDriver

~SwapMetricsDriver
~~~cpp
virtual ~SwapMetricsDriver() {}
~~~

### Create

SwapMetricsDriver::Create
~~~cpp
static std::unique_ptr<SwapMetricsDriver> Create(
      std::unique_ptr<Delegate> delegate,
      const base::TimeDelta update_interval);
~~~
 Create a SwapMetricsDriver that will notify the |delegate| when updated
 metrics are available to consume. If |update_interval| is 0, periodic
 updating is disabled and metrics should be updated manually via
 UpdateMetrics().

 This returns nullptr when swap metrics are not available on the system.

### InitializeMetrics

SwapMetricsDriver::InitializeMetrics
~~~cpp
virtual SwapMetricsUpdateResult InitializeMetrics() = 0;
~~~
 Initialze swap metrics so updates will start from the values read from this
 point. If an error occurs while retrieving the platform specific metrics,
 e.g. I/O error, this returns
 SwapMetricsUpdateResult::kSwapMetricsUpdateFailed.

 After InitializeMetrics() is called, all subsequent calls to this API must
 be made on the sequence this is called from.

### IsRunning

SwapMetricsDriver::IsRunning
~~~cpp
virtual bool IsRunning() = 0;
~~~
 Returns whether or not the driver is periodically computing metrics.

 This returns false if the driver has been stopped or never started, or if
 an error occurred during UpdateMetrics().

 This method must be called from the same sequence that Start() is called
 on. If Start() has not yet been called, the subsequent call to Start() must
 be from the same sequence.

### Start

SwapMetricsDriver::Start
~~~cpp
virtual SwapMetricsUpdateResult Start() = 0;
~~~
 Starts computing swap metrics every at the interval specified in the
 constructor, which must be > 0. If an error occurs while initializing the
 metrics, this returns SwapMetricsUpdateResult::kSwapMetricsUpdateFailed. If
 an error occurs in UpdateMetrics() during a periodic update, the driver
 will be stopped. IsRunning() can be used to determine if metrics are still
 being computed, or by handling the Delegate's OnMetricsFailed() method.

 After Start() is called, all subsequent calls to this API must be made on
 the sequence the driver is started from.

### Stop

SwapMetricsDriver::Stop
~~~cpp
virtual void Stop() = 0;
~~~
 Stop computing swap metrics. To compute metrics for the remaining time
 since the last update, UpdateMetrics() can be called after Stop().

 Stop() must be called on the same sequence as Start().

### UpdateMetrics

SwapMetricsDriver::UpdateMetrics
~~~cpp
virtual SwapMetricsUpdateResult UpdateMetrics() = 0;
~~~
 Update metrics immediately and synchronously notify the associated
 Delegate. InitializeMetrics() or Start() must be called before
 UpdateMetrics(), and UpdateMetrics() must be called on the same sequence
 that those methods were invoked on. If an error occurs while retrieving the
 platform specific metrics, e.g. I/O error, this returns
 SwapMetricsUpdateResult::kSwapMetricsUpdateFailed and the associated
 delegate's OnUpdateMetricsFailed() method will be called.

### SwapMetricsDriver

SwapMetricsDriver
~~~cpp
SwapMetricsDriver() {}
~~~
