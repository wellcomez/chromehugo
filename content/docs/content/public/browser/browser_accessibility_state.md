
## class BrowserAccessibilityState
 The BrowserAccessibilityState class is used to determine if the browser
 should be customized for users with assistive technology, such as screen
 readers.

### GetInstance

BrowserAccessibilityState::GetInstance
~~~cpp
static BrowserAccessibilityState* GetInstance();
~~~
 Returns the singleton instance.

### EnableAccessibility

BrowserAccessibilityState::EnableAccessibility
~~~cpp
virtual void EnableAccessibility() = 0;
~~~
 Enables accessibility for all running tabs.

### DisableAccessibility

BrowserAccessibilityState::DisableAccessibility
~~~cpp
virtual void DisableAccessibility() = 0;
~~~
 Disables accessibility for all running tabs. (Only if accessibility is not
 required by a command line flag or by a platform requirement.)
### IsRendererAccessibilityEnabled

BrowserAccessibilityState::IsRendererAccessibilityEnabled
~~~cpp
virtual bool IsRendererAccessibilityEnabled() = 0;
~~~

### GetAccessibilityMode

BrowserAccessibilityState::GetAccessibilityMode
~~~cpp
virtual ui::AXMode GetAccessibilityMode() = 0;
~~~

### AddAccessibilityModeFlags

BrowserAccessibilityState::AddAccessibilityModeFlags
~~~cpp
virtual void AddAccessibilityModeFlags(ui::AXMode mode) = 0;
~~~
 Adds the given accessibility mode flags to the current accessibility
 mode bitmap.

### RemoveAccessibilityModeFlags

BrowserAccessibilityState::RemoveAccessibilityModeFlags
~~~cpp
virtual void RemoveAccessibilityModeFlags(ui::AXMode mode) = 0;
~~~
 Remove the given accessibility mode flags from the current accessibility
 mode bitmap.

### ResetAccessibilityMode

BrowserAccessibilityState::ResetAccessibilityMode
~~~cpp
virtual void ResetAccessibilityMode() = 0;
~~~
 Resets accessibility to the platform default for all running tabs.

 This is probably off, but may be on, if --force_renderer_accessibility is
 passed, or EditableTextOnly if this is Win7.

### OnScreenReaderDetected

BrowserAccessibilityState::OnScreenReaderDetected
~~~cpp
virtual void OnScreenReaderDetected() = 0;
~~~
 Called when screen reader client is detected.

### OnScreenReaderStopped

BrowserAccessibilityState::OnScreenReaderStopped
~~~cpp
virtual void OnScreenReaderStopped() = 0;
~~~
 Called when screen reader client that had been detected is no longer
 running.

### IsAccessibleBrowser

BrowserAccessibilityState::IsAccessibleBrowser
~~~cpp
virtual bool IsAccessibleBrowser() = 0;
~~~
 Returns true if the browser should be customized for accessibility.

### AddUIThreadHistogramCallback

BrowserAccessibilityState::AddUIThreadHistogramCallback
~~~cpp
virtual void AddUIThreadHistogramCallback(base::OnceClosure callback) = 0;
~~~
 Add a callback method that will be called once, a small while after the
 browser starts up, when accessibility state histograms are updated.

 Use this to register a method to update additional accessibility
 histograms.


 Use this variant for a callback that must be run on the UI thread,
 for example something that needs to access prefs.

### AddOtherThreadHistogramCallback

BrowserAccessibilityState::AddOtherThreadHistogramCallback
~~~cpp
virtual void AddOtherThreadHistogramCallback(base::OnceClosure callback) = 0;
~~~
 Use this variant for a callback that's better to run on another
 thread, for example something that may block or run slowly.

### UpdateUniqueUserHistograms

BrowserAccessibilityState::UpdateUniqueUserHistograms
~~~cpp
virtual void UpdateUniqueUserHistograms() = 0;
~~~
 Fire frequent metrics signals to ensure users keeping browser open multiple
 days are counted each day, not only at launch. This is necessary, because
 UMA only aggregates uniques on a daily basis,
### UpdateHistogramsForTesting

BrowserAccessibilityState::UpdateHistogramsForTesting
~~~cpp
virtual void UpdateHistogramsForTesting() = 0;
~~~

### SetCaretBrowsingState

BrowserAccessibilityState::SetCaretBrowsingState
~~~cpp
virtual void SetCaretBrowsingState(bool enabled) = 0;
~~~
 Update BrowserAccessibilityState with the current status of caret browsing.

### SetImageLabelsModeForProfile

BrowserAccessibilityState::SetImageLabelsModeForProfile
~~~cpp
virtual void SetImageLabelsModeForProfile(bool enabled,
                                            BrowserContext* profile) = 0;
~~~
 Update BrowserAccessibilityState with the current state of accessibility
 image labels. Used exclusively on Android.

### RegisterFocusChangedCallback

BrowserAccessibilityState::RegisterFocusChangedCallback
~~~cpp
virtual base::CallbackListSubscription RegisterFocusChangedCallback(
      FocusChangedCallback callback) = 0;
~~~
 Registers a callback method that is called whenever the focused element
 has changed inside a WebContents.

## class ScopedContentAXModeSetter

### ~ScopedContentAXModeSetter

~ScopedContentAXModeSetter
~~~cpp
~ScopedContentAXModeSetter() { ResetMode(); }
~~~

### ResetMode

ResetMode
~~~cpp
void ResetMode() {
    BrowserAccessibilityState::GetInstance()->RemoveAccessibilityModeFlags(
        mode_);
  }
~~~
