
## struct ServiceWorkerVersionBaseInfo
 Basic information about a service worker.

### ServiceWorkerVersionBaseInfo

ServiceWorkerVersionBaseInfo::ServiceWorkerVersionBaseInfo
~~~cpp
ServiceWorkerVersionBaseInfo(
      const GURL& scope,
      const blink::StorageKey& storage_key,
      int64_t registration_id,
      int64_t version_id,
      int process_id,
      blink::mojom::AncestorFrameType ancestor_frame_type)
~~~

### ServiceWorkerVersionBaseInfo

ServiceWorkerVersionBaseInfo::ServiceWorkerVersionBaseInfo
~~~cpp
ServiceWorkerVersionBaseInfo(const ServiceWorkerVersionBaseInfo& other)
~~~

### ~ServiceWorkerVersionBaseInfo

~ServiceWorkerVersionBaseInfo
~~~cpp
virtual ~ServiceWorkerVersionBaseInfo() = default;
~~~
