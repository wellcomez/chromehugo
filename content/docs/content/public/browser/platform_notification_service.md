
## class PlatformNotificationService
 The service using which notifications can be presented to the user. There
 should be a unique instance of the PlatformNotificationService depending
 on the browsing context being used.

### DisplayNotification

PlatformNotificationService::DisplayNotification
~~~cpp
virtual void DisplayNotification(
      const std::string& notification_id,
      const GURL& origin,
      const GURL& document_url,
      const blink::PlatformNotificationData& notification_data,
      const blink::NotificationResources& notification_resources) = 0;
~~~
 Displays the notification described in |notification_data| to the user.

 This method must be called on the UI thread. |document_url| is empty when
 the display notification originates from a worker.

### DisplayPersistentNotification

PlatformNotificationService::DisplayPersistentNotification
~~~cpp
virtual void DisplayPersistentNotification(
      const std::string& notification_id,
      const GURL& service_worker_origin,
      const GURL& origin,
      const blink::PlatformNotificationData& notification_data,
      const blink::NotificationResources& notification_resources) = 0;
~~~
 Displays the persistent notification described in |notification_data| to
 the user. This method must be called on the UI thread.

### CloseNotification

PlatformNotificationService::CloseNotification
~~~cpp
virtual void CloseNotification(const std::string& notification_id) = 0;
~~~
 Closes the notification identified by |notification_id|. This method must
 be called on the UI thread.

### ClosePersistentNotification

PlatformNotificationService::ClosePersistentNotification
~~~cpp
virtual void ClosePersistentNotification(
      const std::string& notification_id) = 0;
~~~
 Closes the persistent notification identified by |notification_id|. This
 method must be called on the UI thread.

### GetDisplayedNotifications

PlatformNotificationService::GetDisplayedNotifications
~~~cpp
virtual void GetDisplayedNotifications(
      DisplayedNotificationsCallback callback) = 0;
~~~
 Retrieves the ids of all currently displaying notifications and
 posts |callback| with the result.

### ScheduleTrigger

PlatformNotificationService::ScheduleTrigger
~~~cpp
virtual void ScheduleTrigger(base::Time timestamp) = 0;
~~~
 Schedules a job to run at |timestamp| and call TriggerNotifications
 on all PlatformNotificationContext instances.

### ReadNextTriggerTimestamp

PlatformNotificationService::ReadNextTriggerTimestamp
~~~cpp
virtual base::Time ReadNextTriggerTimestamp() = 0;
~~~
 Reads the value of the next notification trigger time for this profile.

 This will return base::Time::Max if there is no trigger set.

### ReadNextPersistentNotificationId

PlatformNotificationService::ReadNextPersistentNotificationId
~~~cpp
virtual int64_t ReadNextPersistentNotificationId() = 0;
~~~
 Reads the value of the next persistent notification ID from the profile and
 increments the value, as it is called once per notification write.

### RecordNotificationUkmEvent

PlatformNotificationService::RecordNotificationUkmEvent
~~~cpp
virtual void RecordNotificationUkmEvent(
      const NotificationDatabaseData& data) = 0;
~~~
 Records a given notification to UKM.
