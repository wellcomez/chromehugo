### ~RenderProcessHost

~RenderProcessHost
~~~cpp
~RenderProcessHost() override {}
~~~
###  General functions 
---------------------------------------------------------
### Init

Init
~~~cpp
virtual bool Init() = 0;
~~~
 Initialize the new renderer process, returning true on success. This must
 be called once before the object can be used, but can be called after
 that with no effect. Therefore, if the caller isn't sure about whether
 the process has been created, it should just call Init().

### EnableSendQueue

EnableSendQueue
~~~cpp
virtual void EnableSendQueue() = 0;
~~~
 Ensures that a Channel exists and is at least queueing outgoing messages
 if there isn't a render process connected to it yet. This may be used to
 ensure that in the event of a renderer crash and restart, subsequent
 messages sent via Send() will eventually reach the new process.

### GetNextRoutingID

GetNextRoutingID
~~~cpp
virtual int GetNextRoutingID() = 0;
~~~
 Gets the next available routing id.

### AddRoute

AddRoute
~~~cpp
virtual void AddRoute(int32_t routing_id, IPC::Listener* listener) = 0;
~~~
 These methods add or remove listener for a specific message routing ID.

 Used for refcounting, each holder of this object must AddRoute and
 RemoveRoute. This object should be allocated on the heap; when no
 listeners own it any more, it will delete itself.

### RemoveRoute

RemoveRoute
~~~cpp
virtual void RemoveRoute(int32_t routing_id) = 0;
~~~

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(RenderProcessHostObserver* observer) = 0;
~~~
 Add and remove observers for lifecycle events. The order in which
 notifications are sent to observers is undefined. Observers must be sure to
 remove the observer before they go away.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(RenderProcessHostObserver* observer) = 0;
~~~

### ShutdownForBadMessage

ShutdownForBadMessage
~~~cpp
virtual void ShutdownForBadMessage(CrashReportMode crash_report_mode) = 0;
~~~
 Called when a received message cannot be decoded. Terminates the renderer.

 Most callers should not call this directly, but instead should call
 bad_message::BadMessageReceived() or an equivalent method outside of the
 content module.


 If |crash_report_mode| is GENERATE_CRASH_DUMP, then a browser crash dump
 will be reported as well.

### UpdateClientPriority

UpdateClientPriority
~~~cpp
virtual void UpdateClientPriority(
      RenderProcessHostPriorityClient* client) = 0;
~~~
 Recompute Priority state. RenderProcessHostPriorityClient should call this
 when their individual priority changes.

### VisibleClientCount

VisibleClientCount
~~~cpp
virtual int VisibleClientCount() = 0;
~~~
 Number of visible (ie |!is_hidden|) RenderProcessHostPriorityClients.

### GetFrameDepth

GetFrameDepth
~~~cpp
virtual unsigned int GetFrameDepth() = 0;
~~~
 Get computed frame depth from RenderProcessHostPriorityClients.

### GetIntersectsViewport

GetIntersectsViewport
~~~cpp
virtual bool GetIntersectsViewport() = 0;
~~~
 Get computed viewport intersection state from
 RenderProcessHostPriorityClients.

### OnMediaStreamAdded

OnMediaStreamAdded
~~~cpp
virtual void OnMediaStreamAdded() = 0;
~~~
 Called when a video capture stream or an audio stream is added or removed
 and used to determine if the process should be backgrounded or not.

### OnMediaStreamRemoved

OnMediaStreamRemoved
~~~cpp
virtual void OnMediaStreamRemoved() = 0;
~~~

### OnForegroundServiceWorkerAdded

OnForegroundServiceWorkerAdded
~~~cpp
virtual void OnForegroundServiceWorkerAdded() = 0;
~~~
 Called when a service worker is executing in the process and may need
 to respond to events from other processes in a timely manner.  This is
 used to determine if the process should be backgrounded or not.

### OnForegroundServiceWorkerRemoved

OnForegroundServiceWorkerRemoved
~~~cpp
virtual void OnForegroundServiceWorkerRemoved() = 0;
~~~

### IsForGuestsOnly

IsForGuestsOnly
~~~cpp
virtual bool IsForGuestsOnly() = 0;
~~~
 Indicates whether the current RenderProcessHost is exclusively hosting
 guest RenderFrames. Not all guest RenderFrames are created equal.  A guest,
 as indicated by BrowserPluginGuest::IsGuest, may coexist with other
 non-guest RenderFrames in the same process if IsForGuestsOnly() is false.

### IsJitDisabled

IsJitDisabled
~~~cpp
virtual bool IsJitDisabled() = 0;
~~~
 Indicates whether the current RenderProcessHost is running with JavaScript
 JIT disabled.

### IsPdf

IsPdf
~~~cpp
virtual bool IsPdf() = 0;
~~~
 Indicates whether the current RenderProcessHost exclusively hosts PDF
 content.

### GetStoragePartition

GetStoragePartition
~~~cpp
virtual StoragePartition* GetStoragePartition() = 0;
~~~
 Returns the storage partition associated with this process.

### Shutdown

Shutdown
~~~cpp
virtual bool Shutdown(int exit_code) = 0;
~~~
 Terminate the associated renderer process without running unload handlers,
 waiting for the process to exit, etc. If supported by the OS, set the
 process exit code to |exit_code|. Returns false if the shutdown request
 will not be dispatched. If called before
 RenderProcessHostObserver::RenderProcessReady,
 RenderProcessHostObserver::RenderProcessExited may never be called since
 Shutdown() will race with child process startup.

### ShutdownRequested

ShutdownRequested
~~~cpp
virtual bool ShutdownRequested() = 0;
~~~
 Returns true if shutdown was started by calling |Shutdown()|.

### FastShutdownIfPossible

FastShutdownIfPossible
~~~cpp
virtual bool FastShutdownIfPossible(size_t page_count = 0,
                                      bool skip_unload_handlers = false) = 0;
~~~
 Try to shut down the associated renderer process as fast as possible.

 If a non-zero |page_count| value is provided, then a fast shutdown will
 only happen if the count matches the active view count. If
 |skip_unload_handlers| is false and this renderer has any RenderViews with
 unload handlers, then this function does nothing. Otherwise, the function
 will ingnore checking for those handlers. Returns true if it was able to do
 fast shutdown.

### FastShutdownStarted

FastShutdownStarted
~~~cpp
virtual bool FastShutdownStarted() = 0;
~~~
 Returns true if fast shutdown was started for the renderer.

### GetProcess

GetProcess
~~~cpp
virtual const base::Process& GetProcess() = 0;
~~~
 Returns the process object associated with the child process.  In certain
 tests or single-process mode, this will actually represent the current
 process.


 NOTE: this is not necessarily valid immediately after calling Init, as
 Init starts the process asynchronously.  It's guaranteed to be valid after
 the first IPC arrives or RenderProcessReady was called on a
 RenderProcessHostObserver for this. At that point, IsReady() returns true.

### IsReady

IsReady
~~~cpp
virtual bool IsReady() = 0;
~~~
 Returns whether the process is ready. The process is ready once both
 conditions (which can happen in arbitrary order) are true:
 1- the launcher reported a successful launch
 2- the channel is connected.


 After that point, GetHandle() is valid, and deferred messages have been
 sent.

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual content::BrowserContext* GetBrowserContext() = 0;
~~~
 Returns the user browser context associated with this renderer process.

### InSameStoragePartition

InSameStoragePartition
~~~cpp
virtual bool InSameStoragePartition(StoragePartition* partition) = 0;
~~~
 Returns whether this process is using the same StoragePartition as
 |partition|.

### GetID

GetID
~~~cpp
virtual int GetID() const = 0;
~~~
 Returns the unique ID for this child process host. This can be used later
 in a call to FromID() to get back to this object (this is used to avoid
 sending non-threadsafe pointers to other threads).


 This ID will be unique across all child process hosts, including workers,
 plugins, etc.


 This will never return ChildProcessHost::kInvalidUniqueID.

### GetSafeRef

GetSafeRef
~~~cpp
virtual base::SafeRef<RenderProcessHost> GetSafeRef() const = 0;
~~~
 Returns a SafeRef to `this`. It should only be used in non-owning cases,
 where the caller is not expected to outlive `this`.

 This method is public so that it can be called from within //content, and
 used by MockRenderProcessHost. It isn't meant to be called outside of
 //content.

### IsInitializedAndNotDead

IsInitializedAndNotDead
~~~cpp
virtual bool IsInitializedAndNotDead() = 0;
~~~
 Returns true iff the Init() was called and the process hasn't died yet.


 Note that even if IsInitializedAndNotDead() returns true, then (for a short
 duration after calling Init()) the process might not be fully spawned
 *yet*.  For example - IsReady() might return false and GetProcess() might
 still return an invalid process with a null handle.

### GetChannel

GetChannel
~~~cpp
virtual IPC::ChannelProxy* GetChannel() = 0;
~~~
 Returns the renderer channel.

### AddFilter

AddFilter
~~~cpp
virtual void AddFilter(BrowserMessageFilter* filter) = 0;
~~~
 Adds a message filter to the IPC channel.

### SetBlocked

SetBlocked
~~~cpp
virtual void SetBlocked(bool blocked) = 0;
~~~
 Sets whether this render process is blocked. This means that input events
 should not be sent to it, nor other timely signs of life expected from it.

### IsBlocked

IsBlocked
~~~cpp
virtual bool IsBlocked() = 0;
~~~

### RegisterBlockStateChangedCallback

RegisterBlockStateChangedCallback
~~~cpp
virtual base::CallbackListSubscription RegisterBlockStateChangedCallback(
      const BlockStateChangedCallback& cb) = 0;
~~~

### Cleanup

Cleanup
~~~cpp
virtual void Cleanup() = 0;
~~~
 Schedules the host for deletion and removes it from the all_hosts list.

### AddPendingView

AddPendingView
~~~cpp
virtual void AddPendingView() = 0;
~~~
 Track the count of pending views that are being swapped back in.  Called
 by listeners to register and unregister pending views to prevent the
 process from exiting.

### RemovePendingView

RemovePendingView
~~~cpp
virtual void RemovePendingView() = 0;
~~~

### AddPriorityClient

AddPriorityClient
~~~cpp
virtual void AddPriorityClient(
      RenderProcessHostPriorityClient* priority_client) = 0;
~~~
 Adds and removes priority clients.

### RemovePriorityClient

RemovePriorityClient
~~~cpp
virtual void RemovePriorityClient(
      RenderProcessHostPriorityClient* priority_client) = 0;
~~~

### SetPriorityOverride

SetPriorityOverride
~~~cpp
virtual void SetPriorityOverride(bool foreground) = 0;
~~~
 Sets a process priority override. This overrides the entire built-in
 priority setting mechanism for the process.

### HasPriorityOverride

HasPriorityOverride
~~~cpp
virtual bool HasPriorityOverride() = 0;
~~~

### ClearPriorityOverride

ClearPriorityOverride
~~~cpp
virtual void ClearPriorityOverride() = 0;
~~~

### GetEffectiveImportance

GetEffectiveImportance
~~~cpp
virtual ChildProcessImportance GetEffectiveImportance() = 0;
~~~
 Return the highest importance of all widgets in this process.

### GetEffectiveChildBindingState

GetEffectiveChildBindingState
~~~cpp
virtual base::android::ChildBindingState GetEffectiveChildBindingState() = 0;
~~~
 Return the highest binding this process has.

### DumpProcessStack

DumpProcessStack
~~~cpp
virtual void DumpProcessStack() = 0;
~~~
 Dumps the stack of this render process without crashing it.

### PauseSocketManagerForRenderFrameHost

PauseSocketManagerForRenderFrameHost
~~~cpp
virtual void PauseSocketManagerForRenderFrameHost(
      const GlobalRenderFrameHostId& render_frame_host_id) = 0;
~~~

### ResumeSocketManagerForRenderFrameHost

ResumeSocketManagerForRenderFrameHost
~~~cpp
virtual void ResumeSocketManagerForRenderFrameHost(
      const GlobalRenderFrameHostId& render_frame_host_id) = 0;
~~~

### SetSuddenTerminationAllowed

SetSuddenTerminationAllowed
~~~cpp
virtual void SetSuddenTerminationAllowed(bool allowed) = 0;
~~~
 Sets a flag indicating that the process can be abnormally terminated.

### SuddenTerminationAllowed

SuddenTerminationAllowed
~~~cpp
virtual bool SuddenTerminationAllowed() = 0;
~~~
 Returns true if the process can be abnormally terminated.

### GetChildProcessIdleTime

GetChildProcessIdleTime
~~~cpp
virtual base::TimeDelta GetChildProcessIdleTime() = 0;
~~~
 Returns how long the child has been idle. The definition of idle
 depends on when a derived class calls mark_child_process_activity_time().

 This is a rough indicator and its resolution should not be better than
 10 milliseconds.

### FilterURL

FilterURL
~~~cpp
virtual void FilterURL(bool empty_allowed, GURL* url) = 0;
~~~
 Checks that the given renderer can request |url|, if not it sets it to
 about:blank.

 |empty_allowed| must be set to false for navigations for security reasons.

### EnableAudioDebugRecordings

EnableAudioDebugRecordings
~~~cpp
virtual void EnableAudioDebugRecordings(const base::FilePath& file) = 0;
~~~

### DisableAudioDebugRecordings

DisableAudioDebugRecordings
~~~cpp
virtual void DisableAudioDebugRecordings() = 0;
~~~

### StartRtpDump

StartRtpDump
~~~cpp
virtual WebRtcStopRtpDumpCallback StartRtpDump(
      bool incoming,
      bool outgoing,
      WebRtcRtpPacketCallback packet_callback) = 0;
~~~
 Starts passing RTP packets to |packet_callback| and returns the callback
 used to stop dumping.

### BindReceiver

BindReceiver
~~~cpp
virtual void BindReceiver(mojo::GenericPendingReceiver receiver) = 0;
~~~
 Asks the renderer process to bind |receiver|. |receiver| arrives in the
 renderer process and is carried through the following flow, stopping if any
 step decides to bind it:

   1. IO thread, |ChildProcessImpl::BindReceiver()| (child_thread_impl.cc)
   2. IO thread ,|ContentClient::BindChildProcessInterface()|
   3. Main thread, |ChildThreadImpl::OnBindReceiver()| (virtual)
   4. Possibly more steps, depending on the ChildThreadImpl subclass.

### TakeMetricsAllocator

TakeMetricsAllocator
~~~cpp
virtual std::unique_ptr<base::PersistentMemoryAllocator>
  TakeMetricsAllocator() = 0;
~~~
 Extracts any persistent-memory-allocator used for renderer metrics.

 Ownership is passed to the caller. To support sharing of histogram data
 between the Renderer and the Browser, the allocator is created when the
 process is created and later retrieved by the SubprocessMetricsProvider
 for management.

### GetLastInitTime

GetLastInitTime
~~~cpp
virtual const base::TimeTicks& GetLastInitTime() = 0;
~~~
 Returns the time of the last call to Init that was completed successfully
 (after a new renderer process was created); further calls to Init would
 change this value only when they caused the new process to be created after
 a crash.

### IsProcessBackgrounded

IsProcessBackgrounded
~~~cpp
virtual bool IsProcessBackgrounded() = 0;
~~~
 Returns true if this process currently has backgrounded priority.

### IncrementKeepAliveRefCount

IncrementKeepAliveRefCount
~~~cpp
virtual void IncrementKeepAliveRefCount(uint64_t handle_id) = 0;
~~~
 "Keep alive ref count" represents the number of the customers of this
 render process who wish the renderer process to be alive. While the ref
 count is positive, |this| object will keep the renderer process alive,
 unless DisableRefCounts() is called. |handle_id| is a unique identifier
 associated with each keep-alive request.

 TODO(wjmaclean): Remove |handle_id| once the causes behind
 https://crbug.com/1148542 are known.


 Here is the list of users:
  - Keepalive request (if the KeepAliveRendererForKeepaliveRequests
    feature is enabled):
    When a fetch request with keepalive flag
    (https://fetch.spec.whatwg.org/#request-keepalive-flag) specified is
    pending, it wishes the renderer process to be kept alive.

  - Unload handlers:
    Keeps the process alive briefly to give subframe unload handlers a
    chance to execute after their parent frame navigates or is detached.

    See https://crbug.com/852204.

  - Process reuse timer (experimental):
    Keeps the process alive for a set period of time in case it can be
    reused for the same site. See https://crbug.com/894253.

### DecrementKeepAliveRefCount

DecrementKeepAliveRefCount
~~~cpp
virtual void DecrementKeepAliveRefCount(uint64_t handle_id) = 0;
~~~

### GetKeepAliveDurations

GetKeepAliveDurations
~~~cpp
virtual std::string GetKeepAliveDurations() const = 0;
~~~
 Returns a list of durations for active KeepAlive requests.

 For debugging only. TODO(wjmaclean): Remove once the causes behind
 https://crbug.com/1148542 are known.

### GetShutdownDelayRefCount

GetShutdownDelayRefCount
~~~cpp
virtual size_t GetShutdownDelayRefCount() const = 0;
~~~
 Returns the number of active Shutdown-Delay requests.

 For debugging only. TODO(wjmaclean): Remove once the causes behind
 https://crbug.com/1148542 are known.

### GetRenderFrameHostCount

GetRenderFrameHostCount
~~~cpp
virtual int GetRenderFrameHostCount() const = 0;
~~~
 Diagnostic code for https://crbug/1148542. This will be removed prior to
 resolving that issue. It counts all RenderFrameHosts that have not been
 destroyed, including speculative ones and pending deletion ones. This
 is included to allow MockRenderProcessHost to override them, and should not
 be called from outside of content/.

### ForEachRenderFrameHost

ForEachRenderFrameHost
~~~cpp
virtual void ForEachRenderFrameHost(
      base::RepeatingCallback<void(RenderFrameHost*)> on_frame) = 0;
~~~
 Calls |on_frame| for every RenderFrameHost whose frames live in this
 process. Note that speculative RenderFrameHosts will be skipped.

### RegisterRenderFrameHost

RegisterRenderFrameHost
~~~cpp
virtual void RegisterRenderFrameHost(
      const GlobalRenderFrameHostId& render_frame_host_id) = 0;
~~~
 Register/unregister a RenderFrameHost instance whose frame lives in this
 process. RegisterRenderFrameHost and UnregisterRenderFrameHost are the
 implementation details and should be called only from within //content.

### UnregisterRenderFrameHost

UnregisterRenderFrameHost
~~~cpp
virtual void UnregisterRenderFrameHost(
      const GlobalRenderFrameHostId& render_frame_host_id) = 0;
~~~

### IncrementWorkerRefCount

IncrementWorkerRefCount
~~~cpp
virtual void IncrementWorkerRefCount() = 0;
~~~
 "Worker ref count" is similar to "Keep alive ref count", but is specific to
 workers since they do not have pre-defined timeouts. Also affected by
 DisableRefCounts() in the same manner as for
 Increment/DecrementKeepAliveRefCount() functions.


 List of users:
  - Service Worker:
    While there are service workers who live in this process, they wish
    the renderer process to be alive. The ref count is incremented when this
    process is allocated to the worker, and decremented when worker's
    shutdown sequence is completed.

  - Shared Worker:
    While there are shared workers who live in this process, they wish
    the renderer process to be alive. The ref count is incremented when
    a shared worker is created in the process, and decremented when
    it is terminated (it self-destructs when it no longer has clients).

  - Shared Storage Worklet:
    (https://github.com/pythagoraskitty/shared-storage)
    While there are shared storage worklets who live in this process, they
    wish the renderer process to be alive. The ref count is incremented when
    a shared storage worklet is created in the process, and decremented when
    it is terminated (after the document is destroyed, the worklet
    will be destroyed when all pending operations have finished or a timeout
    is reached). Note that this is only relevant for the implementation of
    shared storage worklets that run in the renderer process. The actual
    process that the shared storage worklets are going to use is determined
    by the concrete implementation of
    `SharedStorageRenderThreadWorkletDriver`.

  - Auction Worklets (on Android only):
    (https://github.com/WICG/turtledove/blob/main/FLEDGE.md)
    Keeps the renderer alive if there are any worklets using it.

### DecrementWorkerRefCount

DecrementWorkerRefCount
~~~cpp
virtual void DecrementWorkerRefCount() = 0;
~~~

### IncrementPendingReuseRefCount

IncrementPendingReuseRefCount
~~~cpp
virtual void IncrementPendingReuseRefCount() = 0;
~~~
 "Pending reuse" ref count may be used to keep a process alive because we
 know that it will be reused soon.  Unlike the keep alive ref count, it is
 not time-based, and unlike the worker ref count above, it is not used for
 workers.  It is intentionally kept separate from the other ref counts to
 ease debugging, so that it's easier to tell what kept a particular process
 alive.

### DecrementPendingReuseRefCount

DecrementPendingReuseRefCount
~~~cpp
virtual void DecrementPendingReuseRefCount() = 0;
~~~

### DisableRefCounts

DisableRefCounts
~~~cpp
virtual void DisableRefCounts() = 0;
~~~
 Sets all the various process lifetime ref counts to zero (e.g., keep alive,
 worker, etc). Called when the browser context will be destroyed so this
 RenderProcessHost can immediately die.


 After this is called, the Increment/DecrementKeepAliveRefCount() functions
 and Increment/DecrementWorkerRefCount() functions must not be called.

### AreRefCountsDisabled

AreRefCountsDisabled
~~~cpp
virtual bool AreRefCountsDisabled() = 0;
~~~
 Returns true if DisableRefCounts() was called.

### GetRendererInterface

GetRendererInterface
~~~cpp
virtual mojom::Renderer* GetRendererInterface() = 0;
~~~
 Acquires the |mojom::Renderer| interface to the render process. This is for
 internal use only, and is only exposed here to support
 MockRenderProcessHost usage in tests.

### CreateURLLoaderFactory

CreateURLLoaderFactory
~~~cpp
virtual void CreateURLLoaderFactory(
      mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver,
      network::mojom::URLLoaderFactoryParamsPtr params) = 0;
~~~
 Create an URLLoaderFactory from |this| renderer process.


 This method will bind |receiver| with a new URLLoaderFactory created from
 the storage partition's Network Context. Note that the URLLoaderFactory
 returned by this method does NOT support auto-reconnect after a crash of
 Network Service.

### MayReuseHost

MayReuseHost
~~~cpp
virtual bool MayReuseHost() = 0;
~~~
 Whether this process is locked out from ever being reused for sites other
 than the ones it currently has.

### IsUnused

IsUnused
~~~cpp
virtual bool IsUnused() = 0;
~~~
 Indicates whether this RenderProcessHost is "unused".  This starts out as
 true for new processes and becomes false after one of the following:
 (1) This process commits any page.

 (2) This process is given to a SiteInstance that already has a site
     assigned.

 Note that a process hosting ServiceWorkers will be implicitly handled by
 (2) during ServiceWorker initialization, and SharedWorkers will be handled
 by (1) since a page needs to commit before it can create a SharedWorker.


 While a process is unused, it is still suitable to host a URL that
 requires a dedicated process.

### SetIsUsed

SetIsUsed
~~~cpp
virtual void SetIsUsed() = 0;
~~~

### HostHasNotBeenUsed

HostHasNotBeenUsed
~~~cpp
virtual bool HostHasNotBeenUsed() = 0;
~~~
 Return true if the host has not been used. This is stronger than IsUnused()
 in that it checks if this RPH has ever been used to render at all, rather
 than just no being suitable to host a URL that requires a dedicated
 process.

 TODO(alexmos): can this be unified with IsUnused()? See also
 crbug.com/738634.

### SetProcessLock

SetProcessLock
~~~cpp
virtual void SetProcessLock(const IsolationContext& isolation_context,
                              const ProcessLock& process_lock) = 0;
~~~
 Locks this RenderProcessHost to documents compatible with |process_lock|.

 This method is public so that it can be called from within //content, and
 used by MockRenderProcessHost. It isn't meant to be called outside of
 //content.

### GetProcessLock

GetProcessLock
~~~cpp
virtual ProcessLock GetProcessLock() const = 0;
~~~
 Returns the ProcessLock associated with this process.

 This method is public so that it can be called from within //content, and
 used by MockRenderProcessHost. It isn't meant to be called outside of
 //content.

### IsProcessLockedToSiteForTesting

IsProcessLockedToSiteForTesting
~~~cpp
virtual bool IsProcessLockedToSiteForTesting() = 0;
~~~
 Returns true if this process is locked to a particular site-specific
 ProcessLock.  See the SetProcessLock() call above.

### DelayProcessShutdown

DelayProcessShutdown
~~~cpp
virtual void DelayProcessShutdown(
      const base::TimeDelta& subframe_shutdown_timeout,
      const base::TimeDelta& unload_handler_timeout,
      const SiteInfo& site_info) = 0;
~~~
 The following several methods are for internal use only, and are only
 exposed here to support MockRenderProcessHost usage in tests.

### StopTrackingProcessForShutdownDelay

StopTrackingProcessForShutdownDelay
~~~cpp
virtual void StopTrackingProcessForShutdownDelay() = 0;
~~~

### BindCacheStorage

BindCacheStorage
~~~cpp
virtual void BindCacheStorage(
      const network::CrossOriginEmbedderPolicy& cross_origin_embedder_policy,
      mojo::PendingRemote<network::mojom::CrossOriginEmbedderPolicyReporter>
          coep_reporter_remote,
      const storage::BucketLocator& bucket_locator,
      mojo::PendingReceiver<blink::mojom::CacheStorage> receiver) = 0;
~~~

### BindFileSystemManager

BindFileSystemManager
~~~cpp
virtual void BindFileSystemManager(
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<blink::mojom::FileSystemManager> receiver) = 0;
~~~

### BindFileSystemAccessManager

BindFileSystemAccessManager
~~~cpp
virtual void BindFileSystemAccessManager(
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<blink::mojom::FileSystemAccessManager>
          receiver) = 0;
~~~

### GetSandboxedFileSystemForBucket

GetSandboxedFileSystemForBucket
~~~cpp
virtual void GetSandboxedFileSystemForBucket(
      const storage::BucketLocator& bucket_locator,
      blink::mojom::FileSystemAccessManager::GetSandboxedFileSystemCallback
          callback) = 0;
~~~

### BindIndexedDB

BindIndexedDB
~~~cpp
virtual void BindIndexedDB(
      const blink::StorageKey& storage_key,
      const GlobalRenderFrameHostId& rfh_id,
      mojo::PendingReceiver<blink::mojom::IDBFactory> receiver) = 0;
~~~

### BindBucketManagerHost

BindBucketManagerHost
~~~cpp
virtual void BindBucketManagerHost(
      base::WeakPtr<BucketContext> bucket_context,
      mojo::PendingReceiver<blink::mojom::BucketManagerHost> receiver) = 0;
~~~

### BindRestrictedCookieManagerForServiceWorker

BindRestrictedCookieManagerForServiceWorker
~~~cpp
virtual void BindRestrictedCookieManagerForServiceWorker(
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<network::mojom::RestrictedCookieManager>
          receiver) = 0;
~~~

### BindVideoDecodePerfHistory

BindVideoDecodePerfHistory
~~~cpp
virtual void BindVideoDecodePerfHistory(
      mojo::PendingReceiver<media::mojom::VideoDecodePerfHistory> receiver) = 0;
~~~

### BindMediaCodecProvider

BindMediaCodecProvider
~~~cpp
virtual void BindMediaCodecProvider(
      mojo::PendingReceiver<media::mojom::FuchsiaMediaCodecProvider>
          receiver) = 0;
~~~

### CreateOneShotSyncService

CreateOneShotSyncService
~~~cpp
virtual void CreateOneShotSyncService(
      const url::Origin& origin,
      mojo::PendingReceiver<blink::mojom::OneShotBackgroundSyncService>
          receiver) = 0;
~~~

### CreatePeriodicSyncService

CreatePeriodicSyncService
~~~cpp
virtual void CreatePeriodicSyncService(
      const url::Origin& origin,
      mojo::PendingReceiver<blink::mojom::PeriodicBackgroundSyncService>
          receiver) = 0;
~~~

### BindQuotaManagerHost

BindQuotaManagerHost
~~~cpp
virtual void BindQuotaManagerHost(
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<blink::mojom::QuotaManagerHost> receiver) = 0;
~~~

### CreateLockManager

CreateLockManager
~~~cpp
virtual void CreateLockManager(
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<blink::mojom::LockManager> receiver) = 0;
~~~

### CreatePermissionService

CreatePermissionService
~~~cpp
virtual void CreatePermissionService(
      const url::Origin& origin,
      mojo::PendingReceiver<blink::mojom::PermissionService> receiver) = 0;
~~~

### CreatePaymentManagerForOrigin

CreatePaymentManagerForOrigin
~~~cpp
virtual void CreatePaymentManagerForOrigin(
      const url::Origin& origin,
      mojo::PendingReceiver<payments::mojom::PaymentManager> receiver) = 0;
~~~

### CreateNotificationService

CreateNotificationService
~~~cpp
virtual void CreateNotificationService(
      GlobalRenderFrameHostId rfh_id,
      NotificationServiceCreatorType creator_type,
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<blink::mojom::NotificationService> receiver) = 0;
~~~
 `rfh_id` is the id for RenderFrameHost for the `receiver` if
 the notification service is created by a document, or the id for the
 ancestor RenderFrameHost of the worker if the notification service is
 created by a dedicated worker, or empty value otherwise.

### CreateWebSocketConnector

CreateWebSocketConnector
~~~cpp
virtual void CreateWebSocketConnector(
      const blink::StorageKey& storage_key,
      mojo::PendingReceiver<blink::mojom::WebSocketConnector> receiver) = 0;
~~~

### CreateStableVideoDecoder

CreateStableVideoDecoder
~~~cpp
virtual void CreateStableVideoDecoder(
      mojo::PendingReceiver<media::stable::mojom::StableVideoDecoder>
          receiver) = 0;
~~~

### GetActiveViewCount

GetActiveViewCount
~~~cpp
size_t GetActiveViewCount();
~~~
 BUILDFLAG(IS_LINUX) || BUILDFLAG(IS_CHROMEOS)
 Returns the current number of active views in this process.  Excludes
 any RenderViewHosts that are swapped out.

### GetWebExposedIsolationLevel

GetWebExposedIsolationLevel
~~~cpp
WebExposedIsolationLevel GetWebExposedIsolationLevel();
~~~
 Returns the isolation level of the RenderProcessHost's ProcessLock. We
 do not return the ProcessLock or WebExposedIsolationInfo because those
 are not exposed outside of //content for now.

### PostTaskWhenProcessIsReady

PostTaskWhenProcessIsReady
~~~cpp
void PostTaskWhenProcessIsReady(base::OnceClosure task);
~~~
 Posts |task|, if this RenderProcessHost is ready or when it becomes ready
 (see RenderProcessHost::IsReady method).  The |task| might not run at all
 (e.g. if |render_process_host| is destroyed before becoming ready).  This
 function can only be called on the browser's UI thread (and the |task| will
 be posted back on the UI thread).

### ForceCrash

ForceCrash
~~~cpp
virtual void ForceCrash() {}
~~~
 Forces the renderer process to crash ASAP.

### GetInfoForBrowserContextDestructionCrashReporting

GetInfoForBrowserContextDestructionCrashReporting
~~~cpp
virtual std::string GetInfoForBrowserContextDestructionCrashReporting() = 0;
~~~
 Returns a string that contains information useful for debugging
 crashes related to RenderProcessHost objects staying alive longer than
 the BrowserContext they are associated with.

### WriteIntoTrace

WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(
      perfetto::TracedProto<TraceProto> proto) const = 0;
~~~
 Write a representation of this object into a trace.

### DumpProfilingData

DumpProfilingData
~~~cpp
virtual void DumpProfilingData(base::OnceClosure callback) {}
~~~
 Ask the renderer process to dump its profiling data to disk. Invokes
 |callback| once this has completed.

### ReinitializeLogging

ReinitializeLogging
~~~cpp
virtual void ReinitializeLogging(uint32_t logging_dest,
                                   base::ScopedFD log_file_descriptor) = 0;
~~~
 Reinitializes the child process's logging with the given settings. This
 is needed on Chrome OS, which switches to a log file in the user's home
 directory once they log in.

### SetOsSupportForAttributionReporting

SetOsSupportForAttributionReporting
~~~cpp
virtual void SetOsSupportForAttributionReporting(
      attribution_reporting::mojom::OsSupport os_support) = 0;
~~~
 Sets whether OS-level support is enabled for Attribution Reporting API.

 See
 https://github.com/WICG/attribution-reporting-api/blob/main/app_to_web.md.

### WarmupSpareRenderProcessHost

WarmupSpareRenderProcessHost
~~~cpp
static void WarmupSpareRenderProcessHost(BrowserContext* browser_context);
~~~
###  Static management functions 
-----------------------------------------------
 Possibly start an unbound, spare RenderProcessHost. A subsequent creation
 of a RenderProcessHost with a matching browser_context may use this
 preinitialized RenderProcessHost, improving performance.


 It is safe to call this multiple times or when it is not certain that the
 spare renderer will be used, although calling this too eagerly may reduce
 performance as unnecessary RenderProcessHosts are created. The spare
 renderer will only be used if it using the default StoragePartition of a
 matching BrowserContext.


 The spare RenderProcessHost is meant to be created in a situation where a
 navigation is imminent and it is unlikely an existing RenderProcessHost
 will be used, for example in a cross-site navigation when a Service Worker
 will need to be started.  Note that if ContentBrowserClient opts into
 strict site isolation (via ShouldEnableStrictSiteIsolation), then the
 //content layer will maintain a warm spare process host at all times
 (without a need for separate calls to WarmupSpareRenderProcessHost).

### GetSpareRenderProcessHostForTesting

GetSpareRenderProcessHostForTesting
~~~cpp
static RenderProcessHost* GetSpareRenderProcessHostForTesting();
~~~
 Return the spare RenderProcessHost, if it exists. There is at most one
 globally-used spare RenderProcessHost at any time.

### RegisterSpareRenderProcessHostChangedCallback

RegisterSpareRenderProcessHostChangedCallback
~~~cpp
static base::CallbackListSubscription
  RegisterSpareRenderProcessHostChangedCallback(
      const base::RepeatingCallback<void(RenderProcessHost*)>& cb);
~~~
 Registers a callback to be notified when the spare RenderProcessHost is
 changed. If a new spare RenderProcessHost is created, the callback is made
 when the host is ready (RenderProcessHostObserver::RenderProcessReady). If
 the spare RenderProcessHost is promoted to be a "real" RenderProcessHost or
 discarded for any reason, the callback is made with a null pointer.

### run_renderer_in_process

run_renderer_in_process
~~~cpp
static bool run_renderer_in_process();
~~~
 Flag to run the renderer in process.  This is primarily
 for debugging purposes.  When running "in process", the
 browser maintains a single RenderProcessHost which communicates
 to a RenderProcess which is instantiated in the same process
 with the Browser.  All IPC between the Browser and the
 Renderer is the same, it's just not crossing a process boundary.

### SetRunRendererInProcess

SetRunRendererInProcess
~~~cpp
static void SetRunRendererInProcess(bool value);
~~~
 This also calls out to ContentBrowserClient::GetApplicationLocale and
 modifies the current process' command line.

 NOTE: This function is fundamentally unsafe and *should not be used*. By
 the time a ContentBrowserClient exists in test fixtures, it is already
 unsafe to modify the command-line. The command-line should only be modified
 from SetUpCommandLine, which is before the ContentClient is created. See
 crbug.com/1197147
### ShutDownInProcessRenderer

ShutDownInProcessRenderer
~~~cpp
static void ShutDownInProcessRenderer();
~~~
 This forces a renderer that is running "in process" to shut down.

### AllHostsIterator

AllHostsIterator
~~~cpp
static iterator AllHostsIterator();
~~~
 Allows iteration over all the RenderProcessHosts in the browser. Note
 that each host may not be active, and therefore may have nullptr channels.

### FromID

FromID
~~~cpp
static RenderProcessHost* FromID(int render_process_id);
~~~
 Returns the RenderProcessHost given its ID.  Returns nullptr if the ID does
 not correspond to a live RenderProcessHost.

### FromRendererInstanceId

FromRendererInstanceId
~~~cpp
static RenderProcessHost* FromRendererInstanceId(
      const base::Token& instance_id);
~~~
 Returns the RenderProcessHost given its renderer's service instance ID,
 generated randomly when launching the renderer. Returns nullptr if the
 instance does not correspond to a live RenderProcessHost.

### ShouldTryToUseExistingProcessHost

ShouldTryToUseExistingProcessHost
~~~cpp
static bool ShouldTryToUseExistingProcessHost(
      content::BrowserContext* browser_context,
      const GURL& site_url);
~~~
 Returns true if the caller should attempt to use an existing
 RenderProcessHost rather than creating a new one.

### SetMaxRendererProcessCount

SetMaxRendererProcessCount
~~~cpp
static void SetMaxRendererProcessCount(size_t count);
~~~
 Overrides the default heuristic for limiting the max renderer process
 count.  This is useful for unit testing process limit behaviors.  It is
 also used to allow a command line parameter to configure the max number of
 renderer processes and should only be called once during startup.

 A value of zero means to use the default heuristic.

### GetMaxRendererProcessCount

GetMaxRendererProcessCount
~~~cpp
static size_t GetMaxRendererProcessCount();
~~~
 Returns the current maximum number of renderer process hosts kept by the
 content module.

### SetHungRendererAnalysisFunction

SetHungRendererAnalysisFunction
~~~cpp
static void SetHungRendererAnalysisFunction(
      AnalyzeHungRendererFunction analyze_hung_renderer);
~~~

### GetCurrentRenderProcessCountForTesting

GetCurrentRenderProcessCountForTesting
~~~cpp
static int GetCurrentRenderProcessCountForTesting();
~~~
 Counts current RenderProcessHost(s), ignoring the spare process.

### InterceptBindHostReceiverForTesting

InterceptBindHostReceiverForTesting
~~~cpp
static void InterceptBindHostReceiverForTesting(
      BindHostReceiverInterceptor callback);
~~~

