
## class VideoPictureInPictureWindowController

### GetWindowForTesting

GetWindowForTesting
~~~cpp
virtual VideoOverlayWindow* GetWindowForTesting() = 0;
~~~

### UpdateLayerBounds

UpdateLayerBounds
~~~cpp
virtual void UpdateLayerBounds() = 0;
~~~

### IsPlayerActive

IsPlayerActive
~~~cpp
virtual bool IsPlayerActive() = 0;
~~~

### SkipAd

SkipAd
~~~cpp
virtual void SkipAd() = 0;
~~~
 Called when the user interacts with the "Skip Ad" control.

### NextTrack

NextTrack
~~~cpp
virtual void NextTrack() = 0;
~~~
 Called when the user interacts with the "Next Track" control.

### PreviousTrack

PreviousTrack
~~~cpp
virtual void PreviousTrack() = 0;
~~~
 Called when the user interacts with the "Previous Track" control.

### TogglePlayPause

TogglePlayPause
~~~cpp
virtual bool TogglePlayPause() = 0;
~~~
 Commands.

 Returns true if the player is active (i.e. currently playing) after this
 call.

### ToggleMicrophone

ToggleMicrophone
~~~cpp
virtual void ToggleMicrophone() = 0;
~~~
 Called when the user interacts with the "Toggle Microphone" control.

### ToggleCamera

ToggleCamera
~~~cpp
virtual void ToggleCamera() = 0;
~~~
 Called when the user interacts with the "Toggle Camera" control.

### HangUp

HangUp
~~~cpp
virtual void HangUp() = 0;
~~~
 Called when the user interacts with the "Hang Up" control.

### PreviousSlide

PreviousSlide
~~~cpp
virtual void PreviousSlide() = 0;
~~~
 Called when the user interacts with the "Previous Slide" control.

### NextSlide

NextSlide
~~~cpp
virtual void NextSlide() = 0;
~~~
 Called when the user interacts with the "Next Slide" control.

### GetSourceBounds

GetSourceBounds
~~~cpp
virtual const gfx::Rect& GetSourceBounds() const = 0;
~~~
 Returns the source bounds of the video, in the WebContents top-level
 coordinate space, of the video before it enters picture in picture.

### VideoPictureInPictureWindowController

VideoPictureInPictureWindowController
~~~cpp
VideoPictureInPictureWindowController() = default;
~~~
 Use PictureInPictureWindowController::GetOrCreateForWebContents() to
 create an instance.

### GetWindowForTesting

GetWindowForTesting
~~~cpp
virtual VideoOverlayWindow* GetWindowForTesting() = 0;
~~~

### UpdateLayerBounds

UpdateLayerBounds
~~~cpp
virtual void UpdateLayerBounds() = 0;
~~~

### IsPlayerActive

IsPlayerActive
~~~cpp
virtual bool IsPlayerActive() = 0;
~~~

### SkipAd

SkipAd
~~~cpp
virtual void SkipAd() = 0;
~~~
 Called when the user interacts with the "Skip Ad" control.

### NextTrack

NextTrack
~~~cpp
virtual void NextTrack() = 0;
~~~
 Called when the user interacts with the "Next Track" control.

### PreviousTrack

PreviousTrack
~~~cpp
virtual void PreviousTrack() = 0;
~~~
 Called when the user interacts with the "Previous Track" control.

### TogglePlayPause

TogglePlayPause
~~~cpp
virtual bool TogglePlayPause() = 0;
~~~
 Commands.

 Returns true if the player is active (i.e. currently playing) after this
 call.

### ToggleMicrophone

ToggleMicrophone
~~~cpp
virtual void ToggleMicrophone() = 0;
~~~
 Called when the user interacts with the "Toggle Microphone" control.

### ToggleCamera

ToggleCamera
~~~cpp
virtual void ToggleCamera() = 0;
~~~
 Called when the user interacts with the "Toggle Camera" control.

### HangUp

HangUp
~~~cpp
virtual void HangUp() = 0;
~~~
 Called when the user interacts with the "Hang Up" control.

### PreviousSlide

PreviousSlide
~~~cpp
virtual void PreviousSlide() = 0;
~~~
 Called when the user interacts with the "Previous Slide" control.

### NextSlide

NextSlide
~~~cpp
virtual void NextSlide() = 0;
~~~
 Called when the user interacts with the "Next Slide" control.

### GetSourceBounds

GetSourceBounds
~~~cpp
virtual const gfx::Rect& GetSourceBounds() const = 0;
~~~
 Returns the source bounds of the video, in the WebContents top-level
 coordinate space, of the video before it enters picture in picture.

### VideoPictureInPictureWindowController

VideoPictureInPictureWindowController
~~~cpp
VideoPictureInPictureWindowController() = default;
~~~
 Use PictureInPictureWindowController::GetOrCreateForWebContents() to
 create an instance.
