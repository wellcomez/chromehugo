
## class SSLHostStateDelegate
 The SSLHostStateDelegate encapulates the host-specific state for SSL errors.

 For example, SSLHostStateDelegate remembers whether the user has whitelisted
 a particular broken cert for use with particular host.  We separate this
 state from the SSLManager because this state is shared across many navigation
 controllers.


 SSLHostStateDelegate may be implemented by the embedder to provide a storage
 strategy for certificate decisions or it may be left unimplemented to use a
 default strategy of not remembering decisions at all.

### enum CertJudgment

~~~cpp
enum CertJudgment {
    DENIED,
    ALLOWED
  }
~~~
### enum InsecureContentType

~~~cpp
enum InsecureContentType {
    // A  MIXED subresource was loaded over HTTP on an HTTPS page.
    MIXED_CONTENT,
    // A CERT_ERRORS subresource was loaded over HTTPS with certificate
    // errors on an HTTPS page.
    CERT_ERRORS_CONTENT,
  }
~~~
### AllowCert

AllowCert
~~~cpp
virtual void AllowCert(const std::string&,
                         const net::X509Certificate& cert,
                         int error,
                         StoragePartition* storage_partition) = 0;
~~~
 Records that |cert| is permitted to be used for |host| in the future, for
 a specified |error| type.

### Clear

Clear
~~~cpp
virtual void Clear(
      base::RepeatingCallback<bool(const std::string&)> host_filter) = 0;
~~~
 Clear allow preferences matched by |host_filter|. If the filter is null,
 clear all preferences.

### QueryPolicy

QueryPolicy
~~~cpp
virtual CertJudgment QueryPolicy(const std::string& host,
                                   const net::X509Certificate& cert,
                                   int error,
                                   StoragePartition* storage_partition) = 0;
~~~
 Queries whether |cert| is allowed for |host| and |error|. Returns true in
### HostRanInsecureContent

HostRanInsecureContent
~~~cpp
virtual void HostRanInsecureContent(const std::string& host,
                                      int child_id,
                                      InsecureContentType content_type) = 0;
~~~
 Records that a host has run insecure content of the given |content_type|.

### DidHostRunInsecureContent

DidHostRunInsecureContent
~~~cpp
virtual bool DidHostRunInsecureContent(const std::string& host,
                                         int child_id,
                                         InsecureContentType content_type) = 0;
~~~
 Returns whether the specified host ran insecure content of the given
 |content_type|.

### AllowHttpForHost

AllowHttpForHost
~~~cpp
virtual void AllowHttpForHost(const std::string& host,
                                StoragePartition* storage_partition) = 0;
~~~
 Allowlists site so it can be loaded over HTTP when HTTPS-First Mode is
 enabled.

### IsHttpAllowedForHost

IsHttpAllowedForHost
~~~cpp
virtual bool IsHttpAllowedForHost(const std::string& host,
                                    StoragePartition* storage_partition) = 0;
~~~
 Returns whether site is allowed to load over HTTP when HTTPS-First Mode is
 enabled.

### RevokeUserAllowExceptions

RevokeUserAllowExceptions
~~~cpp
virtual void RevokeUserAllowExceptions(const std::string& host) = 0;
~~~
 Revokes all SSL certificate error allow exceptions made by the user for
 |host|.

### HasAllowException

HasAllowException
~~~cpp
virtual bool HasAllowException(const std::string& host,
                                 StoragePartition* storage_partition) = 0;
~~~
 Returns whether the user has allowed a certificate error exception or
 HTTP exception for |host|. This does not mean that *all* certificate errors
 are allowed, just that there exists an exception. To see if a particular
 certificate and error combination exception is allowed, use QueryPolicy().

### ~SSLHostStateDelegate

~SSLHostStateDelegate
~~~cpp
virtual ~SSLHostStateDelegate() {}
~~~

### AllowCert

AllowCert
~~~cpp
virtual void AllowCert(const std::string&,
                         const net::X509Certificate& cert,
                         int error,
                         StoragePartition* storage_partition) = 0;
~~~
 Records that |cert| is permitted to be used for |host| in the future, for
 a specified |error| type.

### Clear

Clear
~~~cpp
virtual void Clear(
      base::RepeatingCallback<bool(const std::string&)> host_filter) = 0;
~~~
 Clear allow preferences matched by |host_filter|. If the filter is null,
 clear all preferences.

### QueryPolicy

QueryPolicy
~~~cpp
virtual CertJudgment QueryPolicy(const std::string& host,
                                   const net::X509Certificate& cert,
                                   int error,
                                   StoragePartition* storage_partition) = 0;
~~~
 Queries whether |cert| is allowed for |host| and |error|. Returns true in
### HostRanInsecureContent

HostRanInsecureContent
~~~cpp
virtual void HostRanInsecureContent(const std::string& host,
                                      int child_id,
                                      InsecureContentType content_type) = 0;
~~~
 Records that a host has run insecure content of the given |content_type|.

### DidHostRunInsecureContent

DidHostRunInsecureContent
~~~cpp
virtual bool DidHostRunInsecureContent(const std::string& host,
                                         int child_id,
                                         InsecureContentType content_type) = 0;
~~~
 Returns whether the specified host ran insecure content of the given
 |content_type|.

### AllowHttpForHost

AllowHttpForHost
~~~cpp
virtual void AllowHttpForHost(const std::string& host,
                                StoragePartition* storage_partition) = 0;
~~~
 Allowlists site so it can be loaded over HTTP when HTTPS-First Mode is
 enabled.

### IsHttpAllowedForHost

IsHttpAllowedForHost
~~~cpp
virtual bool IsHttpAllowedForHost(const std::string& host,
                                    StoragePartition* storage_partition) = 0;
~~~
 Returns whether site is allowed to load over HTTP when HTTPS-First Mode is
 enabled.

### RevokeUserAllowExceptions

RevokeUserAllowExceptions
~~~cpp
virtual void RevokeUserAllowExceptions(const std::string& host) = 0;
~~~
 Revokes all SSL certificate error allow exceptions made by the user for
 |host|.

### HasAllowException

HasAllowException
~~~cpp
virtual bool HasAllowException(const std::string& host,
                                 StoragePartition* storage_partition) = 0;
~~~
 Returns whether the user has allowed a certificate error exception or
 HTTP exception for |host|. This does not mean that *all* certificate errors
 are allowed, just that there exists an exception. To see if a particular
 certificate and error combination exception is allowed, use QueryPolicy().

### ~SSLHostStateDelegate

~SSLHostStateDelegate
~~~cpp
virtual ~SSLHostStateDelegate() {}
~~~

### enum CertJudgment
 The judgements that can be reached by a user for invalid certificates.

~~~cpp
enum CertJudgment {
    DENIED,
    ALLOWED
  };
~~~
### enum InsecureContentType
 The types of nonsecure subresources that this class keeps track of.

~~~cpp
enum InsecureContentType {
    // A  MIXED subresource was loaded over HTTP on an HTTPS page.
    MIXED_CONTENT,
    // A CERT_ERRORS subresource was loaded over HTTPS with certificate
    // errors on an HTTPS page.
    CERT_ERRORS_CONTENT,
  };
~~~