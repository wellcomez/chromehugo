
## class DevToolsAgentHost
    : public
 Describes interface for managing devtools agents from browser process.

### GetProtocolVersion

DevToolsAgentHost
    : public::GetProtocolVersion
~~~cpp
static std::string GetProtocolVersion();
~~~
 Latest DevTools protocol version supported.

### IsSupportedProtocolVersion

DevToolsAgentHost
    : public::IsSupportedProtocolVersion
~~~cpp
static bool IsSupportedProtocolVersion(const std::string& version);
~~~
 Returns whether particular version of DevTools protocol is supported.

### GetForId

DevToolsAgentHost
    : public::GetForId
~~~cpp
static scoped_refptr<DevToolsAgentHost> GetForId(const std::string& id);
~~~
 Returns DevToolsAgentHost with a given |id| or nullptr of it doesn't exist.

### GetOrCreateFor

DevToolsAgentHost
    : public::GetOrCreateFor
~~~cpp
static scoped_refptr<DevToolsAgentHost> GetOrCreateFor(
      WebContents* web_contents);
~~~
 Returns DevToolsAgentHost that can be used for inspecting |web_contents|.

 A new DevToolsAgentHost will be created if it does not exist.

### GetOrCreateForTab

DevToolsAgentHost
    : public::GetOrCreateForTab
~~~cpp
static scoped_refptr<DevToolsAgentHost> GetOrCreateForTab(
      WebContents* web_contents);
~~~
 Similar to the above, but returns a DevToolsAgentHost representing 'tab'
 target. Unlike the one for RenderFrame, this will remain the same through
 all possible transitions of underlying frame trees.

### HasFor

DevToolsAgentHost
    : public::HasFor
~~~cpp
static bool HasFor(WebContents* web_contents);
~~~
 Returns true iff an instance of DevToolsAgentHost for the |web_contents|
 exists. This is equivalent to if a DevToolsAgentHost has ever been
 created for the |web_contents|.

### GetForServiceWorker

DevToolsAgentHost
    : public::GetForServiceWorker
~~~cpp
static scoped_refptr<DevToolsAgentHost> GetForServiceWorker(
      ServiceWorkerContext* context,
      int64_t version_id);
~~~
 Return an instance of DevToolsAgentHost associated with the specified
 service worker version, if such instance exists.

### Forward

DevToolsAgentHost
    : public::Forward
~~~cpp
static scoped_refptr<DevToolsAgentHost> Forward(
      const std::string& id,
      std::unique_ptr<DevToolsExternalAgentProxyDelegate> delegate);
~~~
 Creates DevToolsAgentHost that communicates to the target by means of
 provided |delegate|. |delegate| ownership is passed to the created agent
 host.

### CreateForBrowser

DevToolsAgentHost
    : public::CreateForBrowser
~~~cpp
static scoped_refptr<DevToolsAgentHost> CreateForBrowser(
      scoped_refptr<base::SingleThreadTaskRunner> tethering_task_runner,
      const CreateServerSocketCallback& socket_callback);
~~~
 Creates DevToolsAgentHost for the browser, which works with browser-wide
 debugging protocol.

### CreateForDiscovery

DevToolsAgentHost
    : public::CreateForDiscovery
~~~cpp
static scoped_refptr<DevToolsAgentHost> CreateForDiscovery();
~~~
 Creates DevToolsAgentHost for discovery, which supports part of the
 protocol to discover other agent hosts.

### IsDebuggerAttached

DevToolsAgentHost
    : public::IsDebuggerAttached
~~~cpp
static bool IsDebuggerAttached(WebContents* web_contents);
~~~

### GetAll

DevToolsAgentHost
    : public::GetAll
~~~cpp
static List GetAll();
~~~
 Returns all DevToolsAgentHosts without forcing their creation.

### GetOrCreateAll

DevToolsAgentHost
    : public::GetOrCreateAll
~~~cpp
static List GetOrCreateAll();
~~~
 Returns all non-browser target DevToolsAgentHosts content is aware of.

### StartRemoteDebuggingServer

DevToolsAgentHost
    : public::StartRemoteDebuggingServer
~~~cpp
static void StartRemoteDebuggingServer(
      std::unique_ptr<DevToolsSocketFactory> server_socket_factory,
      const base::FilePath& active_port_output_directory,
      const base::FilePath& debug_frontend_dir);
~~~
 Starts remote debugging.

 Takes ownership over |socket_factory|.

 If |active_port_output_directory| is non-empty, it is assumed the
 socket_factory was initialized with an ephemeral port (0). The
 port selected by the OS will be written to a well-known file in
 the output directory.

### StopRemoteDebuggingServer

DevToolsAgentHost
    : public::StopRemoteDebuggingServer
~~~cpp
static void StopRemoteDebuggingServer();
~~~

### StartRemoteDebuggingPipeHandler

DevToolsAgentHost
    : public::StartRemoteDebuggingPipeHandler
~~~cpp
static void StartRemoteDebuggingPipeHandler(base::OnceClosure on_disconnect);
~~~
 Starts remote debugging for browser target for the given fd=3
 for reading and fd=4 for writing remote debugging messages.

### StopRemoteDebuggingPipeHandler

DevToolsAgentHost
    : public::StopRemoteDebuggingPipeHandler
~~~cpp
static void StopRemoteDebuggingPipeHandler();
~~~

### AddObserver

DevToolsAgentHost
    : public::AddObserver
~~~cpp
static void AddObserver(DevToolsAgentHostObserver*);
~~~
 Observer is notified about changes in DevToolsAgentHosts.

### RemoveObserver

DevToolsAgentHost
    : public::RemoveObserver
~~~cpp
static void RemoveObserver(DevToolsAgentHostObserver*);
~~~

### CreateIOStreamFromData

DevToolsAgentHost
    : public::CreateIOStreamFromData
~~~cpp
virtual std::string CreateIOStreamFromData(
      scoped_refptr<base::RefCountedMemory>) = 0;
~~~
 Create a DevTools IO Stream from data.

 Returns a DevTools IO Stream handle that can be used to read and close the
 stream.

### AttachClient

DevToolsAgentHost
    : public::AttachClient
~~~cpp
virtual bool AttachClient(DevToolsAgentHostClient* client) = 0;
~~~
 Attaches |client| to this agent host to start debugging.

 Returns |true| on success. Note that some policies defined by
 embedder or |client| itself may prevent attaching.

### AttachClientWithoutWakeLock

DevToolsAgentHost
    : public::AttachClientWithoutWakeLock
~~~cpp
virtual bool AttachClientWithoutWakeLock(DevToolsAgentHostClient* client) = 0;
~~~
 Same as the above, but does not acquire the WakeLock.

### DetachClient

DevToolsAgentHost
    : public::DetachClient
~~~cpp
virtual bool DetachClient(DevToolsAgentHostClient* client) = 0;
~~~
 Already attached client detaches from this agent host to stop debugging it.

 Returns true iff detach succeeded.

### IsAttached

DevToolsAgentHost
    : public::IsAttached
~~~cpp
virtual bool IsAttached() = 0;
~~~
 Returns true if there is a client attached.

### DispatchProtocolMessage

DevToolsAgentHost
    : public::DispatchProtocolMessage
~~~cpp
virtual void DispatchProtocolMessage(DevToolsAgentHostClient* client,
                                       base::span<const uint8_t> message) = 0;
~~~
 Sends |message| from |client| to the agent.

### InspectElement

DevToolsAgentHost
    : public::InspectElement
~~~cpp
virtual void InspectElement(RenderFrameHost* frame_host, int x, int y) = 0;
~~~
 Starts inspecting element at position (|x|, |y|) in the frame
 represented by |frame_host|.

### GetId

DevToolsAgentHost
    : public::GetId
~~~cpp
virtual std::string GetId() = 0;
~~~
 Returns the unique id of the agent.

### GetParentId

DevToolsAgentHost
    : public::GetParentId
~~~cpp
virtual std::string GetParentId() = 0;
~~~
 Returns the id of the parent host, or empty string if no parent.

### GetOpenerId

DevToolsAgentHost
    : public::GetOpenerId
~~~cpp
virtual std::string GetOpenerId() = 0;
~~~
 Returns the id of the opener host, or empty string if no opener.

### CanAccessOpener

DevToolsAgentHost
    : public::CanAccessOpener
~~~cpp
virtual bool CanAccessOpener() = 0;
~~~
 Returns whether the opened window has access to its opener (can be false
 when using 'noopener' or with enabled COOP).

### GetOpenerFrameId

DevToolsAgentHost
    : public::GetOpenerFrameId
~~~cpp
virtual std::string GetOpenerFrameId() = 0;
~~~
 Returns the DevTools token of this window's opener, or empty string if no
 opener.

### GetWebContents

DevToolsAgentHost
    : public::GetWebContents
~~~cpp
virtual WebContents* GetWebContents() = 0;
~~~
 Returns web contents instance for this host if any.

### GetBrowserContext

DevToolsAgentHost
    : public::GetBrowserContext
~~~cpp
virtual BrowserContext* GetBrowserContext() = 0;
~~~
 Returns related browser context instance if available.

### DisconnectWebContents

DevToolsAgentHost
    : public::DisconnectWebContents
~~~cpp
virtual void DisconnectWebContents() = 0;
~~~
 Temporarily detaches WebContents from this host. Must be followed by
 a call to ConnectWebContents (may leak the host instance otherwise).

### ConnectWebContents

DevToolsAgentHost
    : public::ConnectWebContents
~~~cpp
virtual void ConnectWebContents(WebContents* web_contents) = 0;
~~~
 Attaches render view host to this host.

### GetType

DevToolsAgentHost
    : public::GetType
~~~cpp
virtual std::string GetType() = 0;
~~~
 Returns agent host type.

### GetTitle

DevToolsAgentHost
    : public::GetTitle
~~~cpp
virtual std::string GetTitle() = 0;
~~~
 Returns agent host title.

### GetDescription

DevToolsAgentHost
    : public::GetDescription
~~~cpp
virtual std::string GetDescription() = 0;
~~~
 Returns the host description.

### GetURL

DevToolsAgentHost
    : public::GetURL
~~~cpp
virtual GURL GetURL() = 0;
~~~
 Returns url associated with agent host.

### GetFaviconURL

DevToolsAgentHost
    : public::GetFaviconURL
~~~cpp
virtual GURL GetFaviconURL() = 0;
~~~
 Returns the favicon url for this host.

### GetFrontendURL

DevToolsAgentHost
    : public::GetFrontendURL
~~~cpp
virtual std::string GetFrontendURL() = 0;
~~~
 Returns the frontend url for this host.

### Activate

DevToolsAgentHost
    : public::Activate
~~~cpp
virtual bool Activate() = 0;
~~~
 Activates agent host. Returns false if the operation failed.

### Reload

DevToolsAgentHost
    : public::Reload
~~~cpp
virtual void Reload() = 0;
~~~
 Reloads the host.

### Close

DevToolsAgentHost
    : public::Close
~~~cpp
virtual bool Close() = 0;
~~~
 Closes agent host. Returns false if the operation failed.

### GetLastActivityTime

DevToolsAgentHost
    : public::GetLastActivityTime
~~~cpp
virtual base::TimeTicks GetLastActivityTime() = 0;
~~~
 Returns the time when the host was last active.

### ForceDetachAllSessions

DevToolsAgentHost
    : public::ForceDetachAllSessions
~~~cpp
virtual void ForceDetachAllSessions() = 0;
~~~
 Terminates all debugging sessions and detaches all clients.

### DetachAllClients

DevToolsAgentHost
    : public::DetachAllClients
~~~cpp
static void DetachAllClients();
~~~
 Terminates all debugging sessions and detaches all clients.

### GetProcessHost

DevToolsAgentHost
    : public::GetProcessHost
~~~cpp
virtual RenderProcessHost* GetProcessHost() = 0;
~~~

### ~DevToolsAgentHost

~DevToolsAgentHost
~~~cpp
virtual ~DevToolsAgentHost() {}
~~~
