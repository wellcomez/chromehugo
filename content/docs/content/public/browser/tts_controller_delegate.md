### GetPreferredVoiceIdsForUtterance

GetPreferredVoiceIdsForUtterance
~~~cpp
virtual std::unique_ptr<PreferredVoiceIds> GetPreferredVoiceIdsForUtterance(
      TtsUtterance* utterance) = 0;
~~~
 Returns the PreferredVoiceIds for an utterance. PreferredVoiceIds are used
 in determining which Voice is used for an Utterance.

### UpdateUtteranceDefaultsFromPrefs

UpdateUtteranceDefaultsFromPrefs
~~~cpp
virtual void UpdateUtteranceDefaultsFromPrefs(TtsUtterance* utterance,
                                                double* rate,
                                                double* pitch,
                                                double* volume) = 0;
~~~
 Uses the user preferences to update the |rate|, |pitch| and |volume| for
 a given |utterance|.

## class TtsControllerDelegate
 Allows embedders to control certain aspects of tts. This is only used on
 ChromeOS.

### ~PreferredVoiceId

TtsControllerDelegate::~PreferredVoiceId
~~~cpp
~PreferredVoiceId()
~~~

## struct PreferredVoiceIds

### PreferredVoiceIds

PreferredVoiceIds::PreferredVoiceIds
~~~cpp
PreferredVoiceIds(const PreferredVoiceIds&)
~~~

### operator=

PreferredVoiceIds::operator=
~~~cpp
PreferredVoiceIds& operator=(const PreferredVoiceIds&);
~~~

### ~PreferredVoiceIds

PreferredVoiceIds::~PreferredVoiceIds
~~~cpp
~PreferredVoiceIds()
~~~
