
## struct WebContentsMediaCaptureId

### WebContentsMediaCaptureId

WebContentsMediaCaptureId
~~~cpp
WebContentsMediaCaptureId(int render_process_id, int main_render_frame_id)
      : render_process_id(render_process_id),
        main_render_frame_id(main_render_frame_id) {}
~~~

### WebContentsMediaCaptureId

WebContentsMediaCaptureId
~~~cpp
WebContentsMediaCaptureId(int render_process_id,
                            int main_render_frame_id,
                            bool disable_local_echo)
      : render_process_id(render_process_id),
        main_render_frame_id(main_render_frame_id),
        disable_local_echo(disable_local_echo) {}
~~~

### operator&lt;

WebContentsMediaCaptureId::operator&lt;
~~~cpp
bool operator<(const WebContentsMediaCaptureId& other) const;
~~~

### operator==

WebContentsMediaCaptureId::operator==
~~~cpp
bool operator==(const WebContentsMediaCaptureId& other) const;
~~~

### is_null

WebContentsMediaCaptureId::is_null
~~~cpp
bool is_null() const;
~~~
 Return true if render_process_id or main_render_frame_id is invalid.

### ToString

WebContentsMediaCaptureId::ToString
~~~cpp
std::string ToString() const;
~~~

### Parse

WebContentsMediaCaptureId::Parse
~~~cpp
static bool Parse(const std::string& str,
                    WebContentsMediaCaptureId* output_id);
~~~
 TODO(qiangchen): Pass structured ID along code paths, instead of doing
 string conversion back and forth. See crbug/648666.

 Create WebContentsMediaCaptureId based on a string.

 Return false if the input string does not represent a
 WebContentsMediaCaptureId.
