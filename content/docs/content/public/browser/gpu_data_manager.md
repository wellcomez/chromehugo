
## class GpuDataManager
 This class is fully thread-safe.

### GetInstance

GpuDataManager::GetInstance
~~~cpp
CONTENT_EXPORT static GpuDataManager* GetInstance();
~~~
 Getter for the singleton.

### Initialized

GpuDataManager::Initialized
~~~cpp
CONTENT_EXPORT static bool Initialized();
~~~

### BlocklistWebGLForTesting

BlocklistWebGLForTesting
~~~cpp
virtual void BlocklistWebGLForTesting() = 0;
~~~
 This is only called by extensions testing.

### GetGPUInfo

GetGPUInfo
~~~cpp
virtual gpu::GPUInfo GetGPUInfo() = 0;
~~~

### GetFeatureStatus

GetFeatureStatus
~~~cpp
virtual gpu::GpuFeatureStatus GetFeatureStatus(
      gpu::GpuFeatureType feature) = 0;
~~~

### GpuAccessAllowed

GpuAccessAllowed
~~~cpp
virtual bool GpuAccessAllowed(std::string* reason) = 0;
~~~
 This indicator might change because we could collect more GPU info or
 because the GPU blocklist could be updated.

 If this returns false, any further GPU access, including establishing GPU
 channel, and GPU info collection, should be blocked.

 Can be called on any thread.

 If |reason| is not nullptr and GPU access is blocked, upon return, |reason|
 contains a description of the reason why GPU access is blocked.

### IsEssentialGpuInfoAvailable

IsEssentialGpuInfoAvailable
~~~cpp
virtual bool IsEssentialGpuInfoAvailable() = 0;
~~~
 Check if basic and context GPU info have been collected.

### RequestVideoMemoryUsageStatsUpdate

RequestVideoMemoryUsageStatsUpdate
~~~cpp
virtual void RequestVideoMemoryUsageStatsUpdate(
      VideoMemoryUsageStatsCallback callback) = 0;
~~~
 Requests that the GPU process report its current video memory usage stats.

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(GpuDataManagerObserver* observer) = 0;
~~~
 Registers/unregister |observer|.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(GpuDataManagerObserver* observer) = 0;
~~~

### DisableHardwareAcceleration

DisableHardwareAcceleration
~~~cpp
virtual void DisableHardwareAcceleration() = 0;
~~~

### HardwareAccelerationEnabled

HardwareAccelerationEnabled
~~~cpp
virtual bool HardwareAccelerationEnabled() = 0;
~~~
 Whether a GPU is in use (as opposed to a software renderer).

### AppendGpuCommandLine

AppendGpuCommandLine
~~~cpp
virtual void AppendGpuCommandLine(base::CommandLine* command_line,
                                    GpuProcessKind kind) = 0;
~~~
 Insert switches into gpu process command line: kUseGL, etc.

### ~GpuDataManager

~GpuDataManager
~~~cpp
virtual ~GpuDataManager() {}
~~~

### BlocklistWebGLForTesting

BlocklistWebGLForTesting
~~~cpp
virtual void BlocklistWebGLForTesting() = 0;
~~~
 This is only called by extensions testing.

### GetGPUInfo

GetGPUInfo
~~~cpp
virtual gpu::GPUInfo GetGPUInfo() = 0;
~~~

### GetFeatureStatus

GetFeatureStatus
~~~cpp
virtual gpu::GpuFeatureStatus GetFeatureStatus(
      gpu::GpuFeatureType feature) = 0;
~~~

### GpuAccessAllowed

GpuAccessAllowed
~~~cpp
virtual bool GpuAccessAllowed(std::string* reason) = 0;
~~~
 This indicator might change because we could collect more GPU info or
 because the GPU blocklist could be updated.

 If this returns false, any further GPU access, including establishing GPU
 channel, and GPU info collection, should be blocked.

 Can be called on any thread.

 If |reason| is not nullptr and GPU access is blocked, upon return, |reason|
 contains a description of the reason why GPU access is blocked.

### IsEssentialGpuInfoAvailable

IsEssentialGpuInfoAvailable
~~~cpp
virtual bool IsEssentialGpuInfoAvailable() = 0;
~~~
 Check if basic and context GPU info have been collected.

### RequestVideoMemoryUsageStatsUpdate

RequestVideoMemoryUsageStatsUpdate
~~~cpp
virtual void RequestVideoMemoryUsageStatsUpdate(
      VideoMemoryUsageStatsCallback callback) = 0;
~~~
 Requests that the GPU process report its current video memory usage stats.

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(GpuDataManagerObserver* observer) = 0;
~~~
 Registers/unregister |observer|.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(GpuDataManagerObserver* observer) = 0;
~~~

### DisableHardwareAcceleration

DisableHardwareAcceleration
~~~cpp
virtual void DisableHardwareAcceleration() = 0;
~~~

### HardwareAccelerationEnabled

HardwareAccelerationEnabled
~~~cpp
virtual bool HardwareAccelerationEnabled() = 0;
~~~
 Whether a GPU is in use (as opposed to a software renderer).

### AppendGpuCommandLine

AppendGpuCommandLine
~~~cpp
virtual void AppendGpuCommandLine(base::CommandLine* command_line,
                                    GpuProcessKind kind) = 0;
~~~
 Insert switches into gpu process command line: kUseGL, etc.

### ~GpuDataManager

~GpuDataManager
~~~cpp
virtual ~GpuDataManager() {}
~~~

### GetInstance

GpuDataManager::GetInstance
~~~cpp
CONTENT_EXPORT static GpuDataManager* GetInstance();
~~~
 Getter for the singleton.

### Initialized

GpuDataManager::Initialized
~~~cpp
CONTENT_EXPORT static bool Initialized();
~~~
