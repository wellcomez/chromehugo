
## class WebUIConfigMap
 Class that holds all WebUIConfigs for the browser.


 Embedders wishing to register WebUIConfigs should use
 AddWebUIConfig and AddUntrustedWebUIConfig.


 Underneath it uses a WebUIControllerFactory to hook into the rest of the
 WebUI infra.

### WebUIConfigMap

WebUIConfigMap
~~~cpp
WebUIConfigMap(const WebUIConfigMap&) = delete;
~~~

### operator=

WebUIConfigMap::operator=
~~~cpp
WebUIConfigMap& operator=(const WebUIConfigMap&) = delete;
  ~WebUIConfigMap();
~~~

### AddWebUIConfig

WebUIConfigMap::AddWebUIConfig
~~~cpp
void AddWebUIConfig(std::unique_ptr<WebUIConfig> config);
~~~
 Adds a chrome:// WebUIConfig. CHECKs if the WebUIConfig is for a
 chrome-untrusted:// WebUIConfig.

### AddUntrustedWebUIConfig

WebUIConfigMap::AddUntrustedWebUIConfig
~~~cpp
void AddUntrustedWebUIConfig(std::unique_ptr<WebUIConfig> config);
~~~
 Adds a chrome-untrusted:// WebUIConfig. CHECKs if the WebUIConfig is
 for a chrome:// WebUIConfig.


 Although the scheme is included as part of the WebUIConfig, having
 two separate methods for chrome:// and chrome-untrusted:// helps
 readers tell what type of WebUIConfig is being added.

### GetConfig

WebUIConfigMap::GetConfig
~~~cpp
WebUIConfig* GetConfig(BrowserContext* browser_context,
                         const url::Origin& origin);
~~~
 Returns the WebUIConfig for |origin| if it's registered and the WebUI is
 enabled. (WebUIs can be disabled based on the profile or feature flags.)
### RemoveConfig

WebUIConfigMap::RemoveConfig
~~~cpp
std::unique_ptr<WebUIConfig> RemoveConfig(const url::Origin& origin);
~~~
 Removes and returns the WebUIConfig with |origin|. Returns nullptr if
 there is no WebUIConfig with |origin|.

### GetSizeForTesting

GetSizeForTesting
~~~cpp
size_t GetSizeForTesting() { return configs_map_.size(); }
~~~
 Returns the size of the map, i.e. how many WebUIConfigs are registered.
