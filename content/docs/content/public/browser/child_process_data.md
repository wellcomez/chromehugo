
## struct ChildProcessData
 Holds information about a child process.

### GetProcess

GetProcess
~~~cpp
const base::Process& GetProcess() const { return process_; }
~~~

### SetProcess

SetProcess
~~~cpp
void SetProcess(base::Process process) { process_ = std::move(process); }
~~~
 Since base::Process is non-copyable, the caller has to provide a rvalue.

### ChildProcessData

ChildProcessData::ChildProcessData
~~~cpp
ChildProcessData(int process_type)
~~~

### ~ChildProcessData

ChildProcessData::~ChildProcessData
~~~cpp
~ChildProcessData()
~~~

### ChildProcessData

ChildProcessData::ChildProcessData
~~~cpp
ChildProcessData(ChildProcessData&& rhs)
~~~
