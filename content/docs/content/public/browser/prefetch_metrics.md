
## struct PrefetchReferringPageMetrics
 Holds metrics related to the prefetches requested by a page load.

### GetForCurrentDocument

PrefetchReferringPageMetrics::GetForCurrentDocument
~~~cpp
static absl::optional<PrefetchReferringPageMetrics> GetForCurrentDocument(
      RenderFrameHost* rfh);
~~~

### PrefetchReferringPageMetrics

PrefetchReferringPageMetrics
~~~cpp
PrefetchReferringPageMetrics() = default;
~~~

### ~PrefetchReferringPageMetrics

~PrefetchReferringPageMetrics
~~~cpp
~PrefetchReferringPageMetrics() = default;
~~~

### PrefetchReferringPageMetrics

PrefetchReferringPageMetrics
~~~cpp
PrefetchReferringPageMetrics(const PrefetchReferringPageMetrics&) = default;
~~~

### operator=

PrefetchReferringPageMetrics::operator=
~~~cpp
PrefetchReferringPageMetrics& operator=(const PrefetchReferringPageMetrics&) =
      default;
~~~

## struct PrefetchServingPageMetrics
 Holds metrics related to page loads that may benefit from prefetches
 triggered by speculation rules.

### GetForNavigationHandle

PrefetchServingPageMetrics::GetForNavigationHandle
~~~cpp
static absl::optional<PrefetchServingPageMetrics> GetForNavigationHandle(
      NavigationHandle& navigation_handle);
~~~

### PrefetchServingPageMetrics

PrefetchServingPageMetrics
~~~cpp
PrefetchServingPageMetrics() = default;
~~~

### ~PrefetchServingPageMetrics

~PrefetchServingPageMetrics
~~~cpp
~PrefetchServingPageMetrics() = default;
~~~

### PrefetchServingPageMetrics

PrefetchServingPageMetrics
~~~cpp
PrefetchServingPageMetrics(const PrefetchServingPageMetrics&) = default;
~~~

### operator=

PrefetchServingPageMetrics::operator=
~~~cpp
PrefetchServingPageMetrics& operator=(const PrefetchServingPageMetrics&) =
      default;
~~~
