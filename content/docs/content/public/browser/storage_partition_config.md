### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
CONTENT_EXPORT std::ostream& operator<<(std::ostream& out,
                                        const StoragePartitionConfig& config);
~~~

## class StoragePartitionConfig
 Each StoragePartition is uniquely identified by which partition domain
 it belongs to (such as an app or the browser itself), the user supplied
 partition name and the bit indicating whether it should be persisted on
 disk or not. This structure contains those elements and is used as
 uniqueness key to lookup StoragePartition objects in the global map.

### StoragePartitionConfig

StoragePartitionConfig::StoragePartitionConfig
~~~cpp
StoragePartitionConfig(const StoragePartitionConfig&)
~~~

### operator=

StoragePartitionConfig::operator=
~~~cpp
StoragePartitionConfig& operator=(const StoragePartitionConfig&);
~~~

### CreateDefault

StoragePartitionConfig::CreateDefault
~~~cpp
static StoragePartitionConfig CreateDefault(BrowserContext* browser_context);
~~~
 Creates a default config for |browser_context|. If |browser_context| is an
 off-the-record profile, then the config will have |in_memory_| set to true.

### Create

StoragePartitionConfig::Create
~~~cpp
static StoragePartitionConfig Create(BrowserContext* browser_context,
                                       const std::string& partition_domain,
                                       const std::string& partition_name,
                                       bool in_memory);
~~~
 Creates a config tied to a specific domain.

 The |partition_domain| is [a-z]* UTF-8 string, specifying the domain in
 which partitions live (similar to namespace). |partition_domain| must NOT
 be an empty string. Within a domain, partitions can be uniquely identified
 by the combination of |partition_name| and |in_memory| values. When a
 partition is not to be persisted, the |in_memory| value must be set to
 true. If |browser_context| is an off-the-record profile, then the config
 will have |in_memory_| set to true independent of what is specified in
 the |in_memory| parameter. This is because these profiles are not allowed
 to persist information on disk.

### partition_domain

partition_domain
~~~cpp
std::string partition_domain() const { return partition_domain_; }
~~~

### partition_name

partition_name
~~~cpp
std::string partition_name() const { return partition_name_; }
~~~

### in_memory

in_memory
~~~cpp
bool in_memory() const { return in_memory_; }
~~~

### is_default

is_default
~~~cpp
bool is_default() const { return partition_domain_.empty(); }
~~~
 Returns true if this config was created by CreateDefault() or is
 a copy of a config created with that method.

### set_fallback_to_partition_domain_for_blob_urls

set_fallback_to_partition_domain_for_blob_urls
~~~cpp
void set_fallback_to_partition_domain_for_blob_urls(FallbackMode fallback) {
    if (fallback != FallbackMode::kNone) {
      DCHECK(!is_default());
      DCHECK(!partition_domain_.empty());
      // TODO(https://crbug.com/1279537): Ideally we shouldn't have storage
      // partition configs that differ only in their fallback mode, but
      // unfortunately that isn't true. When that is fixed this can be made more
      // robust by disallowing fallback from storage partitions with an empty
      // partition name.
      // DCHECK(!partition_name_.empty());
    }
    fallback_to_partition_domain_for_blob_urls_ = fallback;
  }
~~~

### fallback_to_partition_domain_for_blob_urls

fallback_to_partition_domain_for_blob_urls
~~~cpp
FallbackMode fallback_to_partition_domain_for_blob_urls() const {
    return fallback_to_partition_domain_for_blob_urls_;
  }
~~~

### GetFallbackForBlobUrls

StoragePartitionConfig::GetFallbackForBlobUrls
~~~cpp
absl::optional<StoragePartitionConfig> GetFallbackForBlobUrls() const;
~~~

### operator&lt;

StoragePartitionConfig::operator&lt;
~~~cpp
bool operator<(const StoragePartitionConfig& rhs) const;
~~~

### operator==

StoragePartitionConfig::operator==
~~~cpp
bool operator==(const StoragePartitionConfig& rhs) const;
~~~

### operator!=

StoragePartitionConfig::operator!=
~~~cpp
bool operator!=(const StoragePartitionConfig& rhs) const;
~~~

### StoragePartitionConfig

StoragePartitionConfig::StoragePartitionConfig
~~~cpp
StoragePartitionConfig(const std::string& partition_domain,
                         const std::string& partition_name,
                         bool in_memory)
~~~
