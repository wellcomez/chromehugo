
## struct StorageUsageInfo
 Used to report per-storage key storage info for a storage type. The storage
 type (Cache API, Indexed DB, Local Storage, etc) is implied by context.

### StorageUsageInfo

StorageUsageInfo
~~~cpp
StorageUsageInfo(const blink::StorageKey& storage_key,
                   int64_t total_size_bytes,
                   base::Time last_modified)
      : storage_key(storage_key),
        total_size_bytes(total_size_bytes),
        last_modified(last_modified) {}
~~~

### StorageUsageInfo

StorageUsageInfo
~~~cpp
StorageUsageInfo() = default;
~~~
 For assignment into maps without wordy emplace(std::make_pair()) syntax.
