### MessageSourceToString

MessageSourceToString
~~~cpp
CONTENT_EXPORT const char* MessageSourceToString(
    blink::mojom::ConsoleMessageSource source);
~~~

### logging::LogSeverity ConsoleMessageLevelToLogSeverity

logging::LogSeverity ConsoleMessageLevelToLogSeverity
~~~cpp
CONTENT_EXPORT logging::LogSeverity ConsoleMessageLevelToLogSeverity(
    blink::mojom::ConsoleMessageLevel level);
~~~

