### GetServiceSandboxType

GetServiceSandboxType
~~~cpp
inline sandbox::mojom::Sandbox GetServiceSandboxType() {
  using ProvidedSandboxType = decltype(Interface::kServiceSandbox);
  static_assert(
      std::is_same<ProvidedSandboxType, const sandbox::mojom::Sandbox>::value,
      "This interface does not declare a proper ServiceSandbox attribute. See "
      "//docs/mojo_and_services.md (Specifying a sandbox).");

  return Interface::kServiceSandbox;
}
~~~

### Launch

Launch
~~~cpp
static void Launch(mojo::PendingReceiver<Interface> receiver,
                     Options options = {}) {
    Launch(mojo::GenericPendingReceiver(std::move(receiver)),
           std::move(options), content::GetServiceSandboxType<Interface>());
  }
~~~

### Launch

Launch
~~~cpp
static mojo::Remote<Interface> Launch(Options options = {}) {
    mojo::Remote<Interface> remote;
    Launch(remote.BindNewPipeAndPassReceiver(), std::move(options),
           content::GetServiceSandboxType<Interface>());
    return remote;
  }
~~~

### GetRunningProcessInfo

GetRunningProcessInfo
~~~cpp
static std::vector<ServiceProcessInfo> GetRunningProcessInfo();
~~~
 Yields information about currently active service processes. Must be called
 from the UI Thread only.

### AddObserver

AddObserver
~~~cpp
static void AddObserver(Observer* observer);
~~~
 Registers a global observer of all service process lifetimes. Must be
 removed before destruction. Must be called from the UI thread only.

### RemoveObserver

RemoveObserver
~~~cpp
static void RemoveObserver(Observer* observer);
~~~
 Removes a registered observer. This must be called some time before
 |*observer| is destroyed and must be called from the UI thread only.

### CONTENT_EXPORT LaunchUtilityProcessServiceDeprecated

CONTENT_EXPORT LaunchUtilityProcessServiceDeprecated
~~~cpp
void CONTENT_EXPORT LaunchUtilityProcessServiceDeprecated(
    const std::string& service_name,
    const std::u16string& display_name,
    sandbox::mojom::Sandbox sandbox_type,
    mojo::ScopedMessagePipeHandle service_pipe,
    base::OnceCallback<void(base::ProcessId)> callback);
~~~
 DEPRECATED. DO NOT USE THIS. This is a helper for any remaining service
 launching code which uses an older code path to launch services in a utility
 process. All new code must use ServiceProcessHost instead of this API.

## class ServiceProcessHost
 ServiceProcessHost is used to launch new service processes given basic
 parameters like sandbox type, as well as a primordial Mojo interface to drive
 the service's behavior. See |Launch()| methods below for more details.


 Typical usage might look something like:

   constexpr auto kFooServiceIdleTimeout = base::Seconds(5);
   auto foo_service = ServiceProcessHost::Launch<foo::mojom::FooService>(
       ServiceProcessHost::Options()
           .WithDisplayName(IDS_FOO_SERVICE_DISPLAY_NAME)
           .Pass());
   foo_service.set_idle_handler(
       kFooServiceIdleTimeout,
       base::BindRepeating(
           /* Something to reset |foo_service|,  killing the process. */));
   foo_service->DoWork();

### ~Options

ServiceProcessHost::~Options
~~~cpp
~Options()
~~~

### Options

ServiceProcessHost::Options
~~~cpp
Options(Options&&)
~~~

### WithDisplayName

ServiceProcessHost::WithDisplayName
~~~cpp
Options& WithDisplayName(const std::string& name);
~~~
 Specifies the display name of the service process. This should generally
 be a human readable and meaningful application or service name and will
 appear in places like the system task viewer.

### WithDisplayName

ServiceProcessHost::WithDisplayName
~~~cpp
Options& WithDisplayName(const std::u16string& name);
~~~

### WithDisplayName

ServiceProcessHost::WithDisplayName
~~~cpp
Options& WithDisplayName(int resource_id);
~~~

### WithSite

ServiceProcessHost::WithSite
~~~cpp
Options& WithSite(const GURL& url);
~~~
 Specifies the site associated with the service process, only needed for
 per-site service processes.

### WithChildFlags

ServiceProcessHost::WithChildFlags
~~~cpp
Options& WithChildFlags(int flags);
~~~
 Specifies additional flags to configure the launched process. See
 ChildProcessHost for flag definitions.

### WithExtraCommandLineSwitches

ServiceProcessHost::WithExtraCommandLineSwitches
~~~cpp
Options& WithExtraCommandLineSwitches(std::vector<std::string> switches);
~~~
 Specifies extra command line switches to append before launch.

### WithProcessCallback

ServiceProcessHost::WithProcessCallback
~~~cpp
Options& WithProcessCallback(
        base::OnceCallback<void(const base::Process&)>);
~~~
 Specifies a callback to be invoked with service process once it's
 launched. Will be on UI thread.

### Pass

ServiceProcessHost::Pass
~~~cpp
Options Pass();
~~~
 Passes the contents of this Options object to a newly returned Options
 value. This must be called when moving a built Options object into a call
 to |Launch()|.

## class Observer : public
 An interface which can be implemented and registered/unregistered with
 |Add/RemoveObserver()| below to watch for all service process creation and
 and termination events globally. Methods are always called from the UI
 UI thread.

### OnServiceProcessLaunched

OnServiceProcessLaunched
~~~cpp
virtual void OnServiceProcessLaunched(const ServiceProcessInfo& info) {}
~~~

### OnServiceProcessTerminatedNormally

OnServiceProcessTerminatedNormally
~~~cpp
virtual void OnServiceProcessTerminatedNormally(
        const ServiceProcessInfo& info) {}
~~~

### OnServiceProcessCrashed

OnServiceProcessCrashed
~~~cpp
virtual void OnServiceProcessCrashed(const ServiceProcessInfo& info) {}
~~~
