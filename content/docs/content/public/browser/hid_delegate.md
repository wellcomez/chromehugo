
## class HidDelegate::Observer

### OnDeviceAdded

OnDeviceAdded
~~~cpp
virtual void OnDeviceAdded(const device::mojom::HidDeviceInfo&) = 0;
~~~
 Events forwarded from HidChooserContext::DeviceObserver:
### OnDeviceRemoved

OnDeviceRemoved
~~~cpp
virtual void OnDeviceRemoved(const device::mojom::HidDeviceInfo&) = 0;
~~~

### OnDeviceChanged

OnDeviceChanged
~~~cpp
virtual void OnDeviceChanged(const device::mojom::HidDeviceInfo&) = 0;
~~~

### OnHidManagerConnectionError

OnHidManagerConnectionError
~~~cpp
virtual void OnHidManagerConnectionError() = 0;
~~~

### OnPermissionRevoked

OnPermissionRevoked
~~~cpp
virtual void OnPermissionRevoked(const url::Origin& origin) = 0;
~~~
 Event forwarded from permissions::ChooserContextBase::PermissionObserver:
### OnDeviceAdded

OnDeviceAdded
~~~cpp
virtual void OnDeviceAdded(const device::mojom::HidDeviceInfo&) = 0;
~~~
 Events forwarded from HidChooserContext::DeviceObserver:
### OnDeviceRemoved

OnDeviceRemoved
~~~cpp
virtual void OnDeviceRemoved(const device::mojom::HidDeviceInfo&) = 0;
~~~

### OnDeviceChanged

OnDeviceChanged
~~~cpp
virtual void OnDeviceChanged(const device::mojom::HidDeviceInfo&) = 0;
~~~

### OnHidManagerConnectionError

OnHidManagerConnectionError
~~~cpp
virtual void OnHidManagerConnectionError() = 0;
~~~

### OnPermissionRevoked

OnPermissionRevoked
~~~cpp
virtual void OnPermissionRevoked(const url::Origin& origin) = 0;
~~~
 Event forwarded from permissions::ChooserContextBase::PermissionObserver:
## class HidDelegate

### RunChooser

HidDelegate::RunChooser
~~~cpp
virtual std::unique_ptr<HidChooser> RunChooser(
      RenderFrameHost* render_frame_host,
      std::vector<blink::mojom::HidDeviceFilterPtr> filters,
      std::vector<blink::mojom::HidDeviceFilterPtr> exclusion_filters,
      HidChooser::Callback callback) = 0;
~~~
 Shows a chooser for the user to select a HID device. |callback| will be
 run when the prompt is closed. Deleting the returned object will cancel the
 prompt.

 If |filters| is empty, all connected devices are included in the chooser
 list except devices that match one or more filters in |exclusion_filters|.

 If |filters| is non-empty, connected devices are included if they match one
 or more filters in |filters| and do not match any filters in
 |exclusion_filters|.

 This method should not be called if CanRequestDevicePermission() below
 returned false.

### CanRequestDevicePermission

HidDelegate::CanRequestDevicePermission
~~~cpp
virtual bool CanRequestDevicePermission(BrowserContext* browser_context,
                                          const url::Origin& origin) = 0;
~~~
 Returns whether `origin` has permission to request access to a device.

### HasDevicePermission

HidDelegate::HasDevicePermission
~~~cpp
virtual bool HasDevicePermission(
      BrowserContext* browser_context,
      const url::Origin& origin,
      const device::mojom::HidDeviceInfo& device) = 0;
~~~
 Returns whether `origin` has permission to access `device`.

### RevokeDevicePermission

HidDelegate::RevokeDevicePermission
~~~cpp
virtual void RevokeDevicePermission(
      BrowserContext* browser_context,
      const url::Origin& origin,
      const device::mojom::HidDeviceInfo& device) = 0;
~~~
 Revoke `device` access permission to `origin`.

### GetHidManager

HidDelegate::GetHidManager
~~~cpp
virtual device::mojom::HidManager* GetHidManager(
      BrowserContext* browser_context) = 0;
~~~
 Returns an open connection to the HidManager interface owned by the
 embedder and being used to serve requests associated with
 `browser_context`.


 Content and the embedder must use the same connection so that the embedder
 can process connect/disconnect events for permissions management purposes
 before they are delivered to content. Otherwise race conditions are
 possible.

### AddObserver

HidDelegate::AddObserver
~~~cpp
virtual void AddObserver(BrowserContext* browser_context,
                           Observer* observer) = 0;
~~~
 Functions to manage the set of Observer instances registered to this
 object.

### RemoveObserver

HidDelegate::RemoveObserver
~~~cpp
virtual void RemoveObserver(BrowserContext* browser_context,
                              Observer* observer) = 0;
~~~

### IsFidoAllowedForOrigin

HidDelegate::IsFidoAllowedForOrigin
~~~cpp
virtual bool IsFidoAllowedForOrigin(BrowserContext* browser_context,
                                      const url::Origin& origin) = 0;
~~~
 Returns true if |origin| is allowed to bypass the HID blocklist and
 access reports contained in FIDO collections.

### IsServiceWorkerAllowedForOrigin

HidDelegate::IsServiceWorkerAllowedForOrigin
~~~cpp
virtual bool IsServiceWorkerAllowedForOrigin(const url::Origin& origin) = 0;
~~~
 Returns true if |origin| is allowed to access HID from service workers.

### GetDeviceInfo

HidDelegate::GetDeviceInfo
~~~cpp
virtual const device::mojom::HidDeviceInfo* GetDeviceInfo(
      BrowserContext* browser_context,
      const std::string& guid) = 0;
~~~
 Gets the device info for a particular device, identified by its guid.

### IncrementConnectionCount

HidDelegate::IncrementConnectionCount
~~~cpp
virtual void IncrementConnectionCount(BrowserContext* browser_context,
                                        const url::Origin& origin) = 0;
~~~
 Notify the delegate a connection is created on |origin| by
 |browser_context|.

### DecrementConnectionCount

HidDelegate::DecrementConnectionCount
~~~cpp
virtual void DecrementConnectionCount(BrowserContext* browser_context,
                                        const url::Origin& origin) = 0;
~~~
 Notify the delegate a connection is closed on |origin| by
 |browser_context|.
