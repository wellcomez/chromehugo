
## class ResourceContext : public
 ResourceContext contains the relevant context information required for
 resource loading. It lives on the IO thread, although it is constructed on
 the UI thread. It must be destructed on the IO thread.

 TODO(mmenke):  Get rid of this class.

### ~ResourceContext

ResourceContext : public::~ResourceContext
~~~cpp
~ResourceContext() override
~~~

### GetWeakPtr

ResourceContext : public::GetWeakPtr
~~~cpp
base::WeakPtr<ResourceContext> GetWeakPtr();
~~~
