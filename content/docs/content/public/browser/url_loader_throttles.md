### std::vector&lt;std::unique_ptr&lt;blink::URLLoaderThrottle&gt;&gt;
CreateContentBrowserURLLoaderThrottles

std::vector&lt;std::unique_ptr&lt;blink::URLLoaderThrottle&gt;&gt;
CreateContentBrowserURLLoaderThrottles
~~~cpp
CONTENT_EXPORT
std::vector<std::unique_ptr<blink::URLLoaderThrottle>>
CreateContentBrowserURLLoaderThrottles(
    const network::ResourceRequest& request,
    BrowserContext* browser_context,
    const base::RepeatingCallback<WebContents*()>& wc_getter,
    NavigationUIData* navigation_ui_data,
    int frame_tree_node_id);
~~~
 Wrapper around ContentBrowserClient::CreateURLLoaderThrottles which inserts
 additional content specific throttles.

