
## class MultiCaptureService
    : public

### MultiCaptureService

MultiCaptureService
~~~cpp
MultiCaptureService(const MultiCaptureService&) = delete;
~~~

### operator=

MultiCaptureService
    : public::operator=
~~~cpp
MultiCaptureService& operator=(const MultiCaptureService&) = delete;
~~~

### ~MultiCaptureService

MultiCaptureService
    : public::~MultiCaptureService
~~~cpp
~MultiCaptureService() override
~~~

### BindMultiCaptureService

MultiCaptureService
    : public::BindMultiCaptureService
~~~cpp
void BindMultiCaptureService(
      mojo::PendingReceiver<video_capture::mojom::MultiCaptureService>
          receiver);
~~~

### AddObserver

MultiCaptureService
    : public::AddObserver
~~~cpp
void AddObserver(
      mojo::PendingRemote<video_capture::mojom::MultiCaptureServiceClient>
          observer) override;
~~~
 video_capture::mojom::MultiCaptureService:
### NotifyMultiCaptureStarted

MultiCaptureService
    : public::NotifyMultiCaptureStarted
~~~cpp
void NotifyMultiCaptureStarted(const std::string& label,
                                 const url::Origin& origin);
~~~

### NotifyMultiCaptureStopped

MultiCaptureService
    : public::NotifyMultiCaptureStopped
~~~cpp
void NotifyMultiCaptureStopped(const std::string& label);
~~~
