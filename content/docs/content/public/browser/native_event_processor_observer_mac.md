
## class NativeEventProcessorObserver

### WillRunNativeEvent

WillRunNativeEvent
~~~cpp
virtual void WillRunNativeEvent(const void* opaque_identifier) = 0;
~~~
 Called right before a native event is run.

### DidRunNativeEvent

DidRunNativeEvent
~~~cpp
virtual void DidRunNativeEvent(const void* opaque_identifier) = 0;
~~~
 Called right after a native event is run.

### WillRunNativeEvent

WillRunNativeEvent
~~~cpp
virtual void WillRunNativeEvent(const void* opaque_identifier) = 0;
~~~
 Called right before a native event is run.

### DidRunNativeEvent

DidRunNativeEvent
~~~cpp
virtual void DidRunNativeEvent(const void* opaque_identifier) = 0;
~~~
 Called right after a native event is run.

## class ScopedNotifyNativeEventProcessorObserver
 The constructor sends a WillRunNativeEvent callback to each observer.

 The destructor sends a DidRunNativeEvent callback to each observer.

### ScopedNotifyNativeEventProcessorObserver

ScopedNotifyNativeEventProcessorObserver
~~~cpp
ScopedNotifyNativeEventProcessorObserver(
      const ScopedNotifyNativeEventProcessorObserver&) = delete;
~~~

### operator=

ScopedNotifyNativeEventProcessorObserver::operator=
~~~cpp
ScopedNotifyNativeEventProcessorObserver& operator=(
      const ScopedNotifyNativeEventProcessorObserver&) = delete;

  ~ScopedNotifyNativeEventProcessorObserver();
~~~
