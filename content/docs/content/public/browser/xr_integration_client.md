
## class XrIntegrationClient
 A helper class for |ContentBrowserClient| to wrap for XR-specific
 integration that may be needed from content/. Currently it only provides
 access to relevant XrInstallHelpers, but will eventually be expanded to
 include integration points for VrUiHost.

 This should be implemented by embedders.

### ~XrIntegrationClient

~XrIntegrationClient
~~~cpp
virtual ~XrIntegrationClient() = default;
~~~

### XrIntegrationClient

XrIntegrationClient
~~~cpp
XrIntegrationClient(const XrIntegrationClient&) = delete;
~~~

### operator=

XrIntegrationClient::operator=
~~~cpp
XrIntegrationClient& operator=(const XrIntegrationClient&) = delete;
~~~

### GetInstallHelper

XrIntegrationClient::GetInstallHelper
~~~cpp
virtual std::unique_ptr<XrInstallHelper> GetInstallHelper(
      device::mojom::XRDeviceId device_id);
~~~
 Returns the |XrInstallHelper| for the corresponding |XRDeviceId|, or
 nullptr if the requested |XRDeviceId| does not have any required extra
 installation steps.

### GetAdditionalProviders

XrIntegrationClient::GetAdditionalProviders
~~~cpp
virtual XRProviderList GetAdditionalProviders();
~~~
 Returns a vector of device providers that should be used in addition to
 any default providers built-in to //content.

### CreateRuntimeObserver

XrIntegrationClient::CreateRuntimeObserver
~~~cpp
virtual std::unique_ptr<BrowserXRRuntime::Observer> CreateRuntimeObserver();
~~~
 Creates a runtime observer that will respond to browser XR runtime state
 changes. May return null if the integraton client does not need to observe
 state changes.

### CreateVrUiHost

XrIntegrationClient::CreateVrUiHost
~~~cpp
virtual std::unique_ptr<VrUiHost> CreateVrUiHost(
      device::mojom::XRDeviceId device_id,
      mojo::PendingRemote<device::mojom::XRCompositorHost> compositor);
~~~
 Creates a VrUiHost object for the specified device_id, and takes ownership
 of any XRCompositor supplied from the runtime.
