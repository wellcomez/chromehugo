
## class RenderProcessHostCreationObserver
 An observer that gets notified any time a new RenderProcessHost is created.

 This can only be used on the UI thread.

### operator=

RenderProcessHostCreationObserver::operator=
~~~cpp
RenderProcessHostCreationObserver& operator=(
      const RenderProcessHostCreationObserver&) = delete;
~~~

### ~RenderProcessHostCreationObserver

RenderProcessHostCreationObserver::~RenderProcessHostCreationObserver
~~~cpp
~RenderProcessHostCreationObserver()
~~~

### OnRenderProcessHostCreated

RenderProcessHostCreationObserver::OnRenderProcessHostCreated
~~~cpp
virtual void OnRenderProcessHostCreated(RenderProcessHost* process_host) = 0;
~~~
 This method is invoked when the process was successfully launched. Note
 that the channel may or may not have been connected when this is invoked.
