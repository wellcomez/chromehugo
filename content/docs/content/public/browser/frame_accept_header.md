### std::string FrameAcceptHeaderValue

std::string FrameAcceptHeaderValue
~~~cpp
CONTENT_EXPORT std::string FrameAcceptHeaderValue(
    bool allow_sxg_responses,
    BrowserContext* browser_context);
~~~
 The value that should be set for the "Accept" header for frame navigations.

 Includes signed exchange and image decoder information (when applicable) that
 are not available from the network service. This may also accept signed
 exchange responses when |allow_sxg_responses| is true.

