
## class StorageNotificationService
 This interface is used to create a connection between the storage layer and
 the embedder layer, where calls to UI code can be made. Embedders should
 vend an instance of this interface in an override of
 BrowserContext::GetStorageNotificationService().

### StorageNotificationService

StorageNotificationService
~~~cpp
StorageNotificationService() = default;
~~~

### StorageNotificationService

StorageNotificationService
~~~cpp
StorageNotificationService(const StorageNotificationService&) = delete;
~~~

### operator=

operator=
~~~cpp
StorageNotificationService& operator=(const StorageNotificationService&) =
      delete;
~~~

### ~StorageNotificationService

~StorageNotificationService
~~~cpp
~StorageNotificationService() = default;
~~~

### MaybeShowStoragePressureNotification

MaybeShowStoragePressureNotification
~~~cpp
virtual void MaybeShowStoragePressureNotification(
      const blink::StorageKey) = 0;
~~~
 These pure virtual functions should be implemented in the embedder layer
 where calls to UI and notification code can be implemented. This closure
 is passed to QuotaManager in StoragePartitionImpl, where it is called
 when QuotaManager determines appropriate to alert the user that the device
 is in a state of storage pressure.

### CreateThreadSafePressureNotificationCallback

CreateThreadSafePressureNotificationCallback
~~~cpp
virtual StoragePressureNotificationCallback
  CreateThreadSafePressureNotificationCallback() = 0;
~~~

### StorageNotificationService

StorageNotificationService
~~~cpp
StorageNotificationService() = default;
~~~

### StorageNotificationService

StorageNotificationService
~~~cpp
StorageNotificationService(const StorageNotificationService&) = delete;
~~~

### operator=

operator=
~~~cpp
StorageNotificationService& operator=(const StorageNotificationService&) =
      delete;
~~~

### ~StorageNotificationService

~StorageNotificationService
~~~cpp
~StorageNotificationService() = default;
~~~

### MaybeShowStoragePressureNotification

MaybeShowStoragePressureNotification
~~~cpp
virtual void MaybeShowStoragePressureNotification(
      const blink::StorageKey) = 0;
~~~
 These pure virtual functions should be implemented in the embedder layer
 where calls to UI and notification code can be implemented. This closure
 is passed to QuotaManager in StoragePartitionImpl, where it is called
 when QuotaManager determines appropriate to alert the user that the device
 is in a state of storage pressure.

### CreateThreadSafePressureNotificationCallback

CreateThreadSafePressureNotificationCallback
~~~cpp
virtual StoragePressureNotificationCallback
  CreateThreadSafePressureNotificationCallback() = 0;
~~~
