
## class CorsOriginPatternSetter
    : public
 A class for mutating CORS examptions list associated with a BrowserContext:
 1. the in-process list in BrowserContext::shared_cors_origin_access_list
 2. the per-NetworkContext (i.e. per-StoragePartition) lists
### CorsOriginPatternSetter

CorsOriginPatternSetter
    : public::CorsOriginPatternSetter
~~~cpp
CorsOriginPatternSetter(
      base::PassKey<CorsOriginPatternSetter> pass_key,
      const url::Origin& source_origin,
      std::vector<network::mojom::CorsOriginPatternPtr> allow_patterns,
      std::vector<network::mojom::CorsOriginPatternPtr> block_patterns,
      base::OnceClosure closure)
~~~

### CorsOriginPatternSetter

CorsOriginPatternSetter
~~~cpp
CorsOriginPatternSetter(const CorsOriginPatternSetter&) = delete;
~~~

### operator=

CorsOriginPatternSetter
    : public::operator=
~~~cpp
CorsOriginPatternSetter& operator=(const CorsOriginPatternSetter&) = delete;
~~~

### ~CorsOriginPatternSetter

CorsOriginPatternSetter
    : public::~CorsOriginPatternSetter
~~~cpp
~CorsOriginPatternSetter()
~~~

### SetForStoragePartition

CorsOriginPatternSetter
    : public::SetForStoragePartition
~~~cpp
void SetForStoragePartition(content::StoragePartition* partition);
~~~
