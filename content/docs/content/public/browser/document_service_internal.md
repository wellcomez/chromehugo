
## class DocumentServiceBase
 Internal helper to provide a common base class for `DocumentService<T>` so
 that //content can internally track all live `DocumentService<T>` instances.

### operator=

DocumentServiceBase::operator=
~~~cpp
DocumentServiceBase& operator=(const DocumentServiceBase&) = delete;
~~~

### ~DocumentServiceBase

DocumentServiceBase::~DocumentServiceBase
~~~cpp
~DocumentServiceBase()
~~~

### ResetAndDeleteThis

DocumentServiceBase::ResetAndDeleteThis
~~~cpp
virtual void ResetAndDeleteThis() = 0;
~~~
 Virtual as an implementation detail of //content, which keeps a generic
 container of pointers to document services via this base class, but still
 needs to be able to end the lifetime of document service instances. See
 `DocumentService<T>` for more information.

### WillBeDestroyed

WillBeDestroyed
~~~cpp
virtual void WillBeDestroyed(DocumentServiceDestructionReason reason) {}
~~~
 To be called just before the destructor, when the object does not
 self-destroy via one of the *AndDeleteThis() helpers. `reason` provides
 context on why `this` is being destroyed (i.e. the document is deleted or
 the Mojo message pipe is disconnected); subclasses can override this method
 to react in a specific way to a destruction reason.

### render_frame_host

render_frame_host
~~~cpp
RenderFrameHost& render_frame_host() const { return *render_frame_host_; }
~~~
