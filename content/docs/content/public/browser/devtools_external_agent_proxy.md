
## class DevToolsExternalAgentProxy
 Describes interface for communication with an external DevTools agent.

### DispatchOnClientHost

DispatchOnClientHost
~~~cpp
virtual void DispatchOnClientHost(base::span<const uint8_t> message) = 0;
~~~
 Sends the message to the client host.

### ConnectionClosed

ConnectionClosed
~~~cpp
virtual void ConnectionClosed() = 0;
~~~
 Informs the client that the connection has closed.

### ~DevToolsExternalAgentProxy

~DevToolsExternalAgentProxy
~~~cpp
virtual ~DevToolsExternalAgentProxy() {}
~~~

### DispatchOnClientHost

DispatchOnClientHost
~~~cpp
virtual void DispatchOnClientHost(base::span<const uint8_t> message) = 0;
~~~
 Sends the message to the client host.

### ConnectionClosed

ConnectionClosed
~~~cpp
virtual void ConnectionClosed() = 0;
~~~
 Informs the client that the connection has closed.

### ~DevToolsExternalAgentProxy

~DevToolsExternalAgentProxy
~~~cpp
virtual ~DevToolsExternalAgentProxy() {}
~~~
