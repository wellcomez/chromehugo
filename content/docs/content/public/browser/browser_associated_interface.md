
## class BrowserAssociatedInterface

### BrowserAssociatedInterface

BrowserAssociatedInterface
~~~cpp
explicit BrowserAssociatedInterface(BrowserMessageFilter* filter)
      : internal_state_(new InternalState(this)) {
    filter->AddAssociatedInterface(
        Interface::Name_,
        base::BindRepeating(&InternalState::BindReceiver, internal_state_),
        base::BindOnce(&InternalState::ClearReceivers, internal_state_));
  }
~~~
 |filter| and |impl| must live at least as long as this object.

### BrowserAssociatedInterface

BrowserAssociatedInterface
~~~cpp
BrowserAssociatedInterface(const BrowserAssociatedInterface&) = delete;
~~~

### operator=

operator=
~~~cpp
BrowserAssociatedInterface& operator=(const BrowserAssociatedInterface&) =
      delete;
~~~

### ~BrowserAssociatedInterface

~BrowserAssociatedInterface
~~~cpp
~BrowserAssociatedInterface() { internal_state_->ClearReceivers(); }
~~~

###  InternalState


~~~cpp
class InternalState : public base::RefCountedThreadSafe<InternalState> {
   public:
    explicit InternalState(Interface* impl)
        : impl_(impl), receivers_(absl::in_place) {}

    InternalState(const InternalState&) = delete;
    InternalState& operator=(const InternalState&) = delete;

    void ClearReceivers() {
      if (!BrowserThread::CurrentlyOn(BrowserThread::IO)) {
        GetIOThreadTaskRunner({})->PostTask(
            FROM_HERE, base::BindOnce(&InternalState::ClearReceivers, this));
        return;
      }
      receivers_.reset();
      // `impl_` destructor calls this function, so it must be reset. It isn't
      // useful anymore, because `BindReceiver` become a no-op after resetting
      // `receiver_`.
      impl_ = nullptr;
    }

    void BindReceiver(mojo::ScopedInterfaceEndpointHandle handle) {
      DCHECK_CURRENTLY_ON(BrowserThread::IO);
      // If this interface has already been shut down we drop the receiver.
      if (!receivers_)
        return;
      receivers_->Add(
          impl_, mojo::PendingAssociatedReceiver<Interface>(std::move(handle)));
    }

   private:
    friend class base::RefCountedThreadSafe<InternalState>;
    friend class TestDriverMessageFilter;

    ~InternalState() {}

    raw_ptr<Interface> impl_;
    absl::optional<mojo::AssociatedReceiverSet<Interface>> receivers_;
  };
~~~
### internal_state_



~~~cpp

scoped_refptr<InternalState> internal_state_;

~~~


### BrowserAssociatedInterface

BrowserAssociatedInterface
~~~cpp
explicit BrowserAssociatedInterface(BrowserMessageFilter* filter)
      : internal_state_(new InternalState(this)) {
    filter->AddAssociatedInterface(
        Interface::Name_,
        base::BindRepeating(&InternalState::BindReceiver, internal_state_),
        base::BindOnce(&InternalState::ClearReceivers, internal_state_));
  }
~~~
 |filter| and |impl| must live at least as long as this object.

### BrowserAssociatedInterface

BrowserAssociatedInterface
~~~cpp
BrowserAssociatedInterface(const BrowserAssociatedInterface&) = delete;
~~~

### operator=

operator=
~~~cpp
BrowserAssociatedInterface& operator=(const BrowserAssociatedInterface&) =
      delete;
~~~

### ~BrowserAssociatedInterface

~BrowserAssociatedInterface
~~~cpp
~BrowserAssociatedInterface() { internal_state_->ClearReceivers(); }
~~~

###  InternalState


~~~cpp
class InternalState : public base::RefCountedThreadSafe<InternalState> {
   public:
    explicit InternalState(Interface* impl)
        : impl_(impl), receivers_(absl::in_place) {}

    InternalState(const InternalState&) = delete;
    InternalState& operator=(const InternalState&) = delete;

    void ClearReceivers() {
      if (!BrowserThread::CurrentlyOn(BrowserThread::IO)) {
        GetIOThreadTaskRunner({})->PostTask(
            FROM_HERE, base::BindOnce(&InternalState::ClearReceivers, this));
        return;
      }
      receivers_.reset();
      // `impl_` destructor calls this function, so it must be reset. It isn't
      // useful anymore, because `BindReceiver` become a no-op after resetting
      // `receiver_`.
      impl_ = nullptr;
    }

    void BindReceiver(mojo::ScopedInterfaceEndpointHandle handle) {
      DCHECK_CURRENTLY_ON(BrowserThread::IO);
      // If this interface has already been shut down we drop the receiver.
      if (!receivers_)
        return;
      receivers_->Add(
          impl_, mojo::PendingAssociatedReceiver<Interface>(std::move(handle)));
    }

   private:
    friend class base::RefCountedThreadSafe<InternalState>;
    friend class TestDriverMessageFilter;

    ~InternalState() {}

    raw_ptr<Interface> impl_;
    absl::optional<mojo::AssociatedReceiverSet<Interface>> receivers_;
  };
~~~
### internal_state_



~~~cpp

scoped_refptr<InternalState> internal_state_;

~~~

