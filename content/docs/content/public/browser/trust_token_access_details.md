
## struct TrustTokenAccessDetails

### TrustTokenAccessDetails

TrustTokenAccessDetails::TrustTokenAccessDetails
~~~cpp
TrustTokenAccessDetails(const url::Origin& origin,
                          network::mojom::TrustTokenOperationType type,
                          const absl::optional<url::Origin>& issuer,
                          bool blocked)
~~~

### TrustTokenAccessDetails

TrustTokenAccessDetails::TrustTokenAccessDetails
~~~cpp
TrustTokenAccessDetails(
      const network::mojom::TrustTokenAccessDetailsPtr& details)
~~~

### ~TrustTokenAccessDetails

TrustTokenAccessDetails::~TrustTokenAccessDetails
~~~cpp
~TrustTokenAccessDetails()
~~~

### TrustTokenAccessDetails

TrustTokenAccessDetails::TrustTokenAccessDetails
~~~cpp
TrustTokenAccessDetails(const TrustTokenAccessDetails&)
~~~

### operator=

TrustTokenAccessDetails::operator=
~~~cpp
operator=(const TrustTokenAccessDetails&)
~~~
