
## class WebAuthenticationRequestProxy
 WebAuthentcationRequestProxy allows the embedder to intercept and handle Web
 Authentication API requests.

### ~WebAuthenticationRequestProxy

~WebAuthenticationRequestProxy
~~~cpp
virtual ~WebAuthenticationRequestProxy() = default;
~~~

### IsActive

IsActive
~~~cpp
virtual bool IsActive(const url::Origin& caller_origin) = 0;
~~~
 IsActive indicates whether the proxy expects to handle Web Authentication
 API requests for the given `caller_origin`.

### SignalCreateRequest

SignalCreateRequest
~~~cpp
virtual RequestId SignalCreateRequest(
      const blink::mojom::PublicKeyCredentialCreationOptionsPtr& options,
      CreateCallback callback) = 0;
~~~
 SignalCreateRequest is invoked when a Web Authentication API
 `navigator.credentials.create()` request occurs.

### SignalGetRequest

SignalGetRequest
~~~cpp
virtual RequestId SignalGetRequest(
      const blink::mojom::PublicKeyCredentialRequestOptionsPtr& options,
      GetCallback callback) = 0;
~~~
 SignalGetRequest is invoked when a Web Authentication API
 `navigator.credentials.get()` request occurs.

### SignalIsUvpaaRequest

SignalIsUvpaaRequest
~~~cpp
virtual RequestId SignalIsUvpaaRequest(IsUvpaaCallback callback) = 0;
~~~
 SignalIsUvpaaRequest is invoked when a
 PublicKeyCredential.isUserVerifyingPlatformAuthenticatorAvailable() (aka
 `IsUvpaa`) request occurs.

### CancelRequest

CancelRequest
~~~cpp
virtual void CancelRequest(RequestId request_id) = 0;
~~~
 CancelRequest cancels processing of the request with the
 given `request_id`.

### ~WebAuthenticationRequestProxy

~WebAuthenticationRequestProxy
~~~cpp
virtual ~WebAuthenticationRequestProxy() = default;
~~~

### IsActive

IsActive
~~~cpp
virtual bool IsActive(const url::Origin& caller_origin) = 0;
~~~
 IsActive indicates whether the proxy expects to handle Web Authentication
 API requests for the given `caller_origin`.

### SignalCreateRequest

SignalCreateRequest
~~~cpp
virtual RequestId SignalCreateRequest(
      const blink::mojom::PublicKeyCredentialCreationOptionsPtr& options,
      CreateCallback callback) = 0;
~~~
 SignalCreateRequest is invoked when a Web Authentication API
 `navigator.credentials.create()` request occurs.

### SignalGetRequest

SignalGetRequest
~~~cpp
virtual RequestId SignalGetRequest(
      const blink::mojom::PublicKeyCredentialRequestOptionsPtr& options,
      GetCallback callback) = 0;
~~~
 SignalGetRequest is invoked when a Web Authentication API
 `navigator.credentials.get()` request occurs.

### SignalIsUvpaaRequest

SignalIsUvpaaRequest
~~~cpp
virtual RequestId SignalIsUvpaaRequest(IsUvpaaCallback callback) = 0;
~~~
 SignalIsUvpaaRequest is invoked when a
 PublicKeyCredential.isUserVerifyingPlatformAuthenticatorAvailable() (aka
 `IsUvpaa`) request occurs.

### CancelRequest

CancelRequest
~~~cpp
virtual void CancelRequest(RequestId request_id) = 0;
~~~
 CancelRequest cancels processing of the request with the
 given `request_id`.
