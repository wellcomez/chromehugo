
## class TouchSelectionControllerClientManager
 This class defines an interface for a manager class that allows multiple
 TouchSelectionControllerClients to work together with a single
 TouchSelectionController.

### DidStopFlinging

TouchSelectionControllerClientManager::DidStopFlinging
~~~cpp
virtual void DidStopFlinging() = 0;
~~~

### UpdateClientSelectionBounds

TouchSelectionControllerClientManager::UpdateClientSelectionBounds
~~~cpp
virtual void UpdateClientSelectionBounds(
      const gfx::SelectionBound& start,
      const gfx::SelectionBound& end,
      ui::TouchSelectionControllerClient* client,
      ui::TouchSelectionMenuClient* menu_client) = 0;
~~~
 Clients call this method when their selection bounds change, so that the
 manager can determine which client should be considered the active client,
 i.e. receive the selection handles and (possibly) a quickmenu.

### InvalidateClient

TouchSelectionControllerClientManager::InvalidateClient
~~~cpp
virtual void InvalidateClient(ui::TouchSelectionControllerClient* client) = 0;
~~~
 Used by clients to inform the manager that the client no longer wants to
 participate in touch selection editing, usually because the client's view
 is being destroyed or detached.

### GetTouchSelectionController

TouchSelectionControllerClientManager::GetTouchSelectionController
~~~cpp
virtual ui::TouchSelectionController* GetTouchSelectionController() = 0;
~~~
 Provides direct access to the TouchSelectionController that will be used
 with all clients accessing this manager. May return null values on Android.

### AddObserver

TouchSelectionControllerClientManager::AddObserver
~~~cpp
virtual void AddObserver(Observer* observer) = 0;
~~~
 The following two functions allow clients (or their owners, etc.) to
 monitor the manager's lifetime.

### RemoveObserver

TouchSelectionControllerClientManager::RemoveObserver
~~~cpp
virtual void RemoveObserver(Observer* observer) = 0;
~~~

### ShowContextMenu

ShowContextMenu
~~~cpp
virtual void ShowContextMenu(const gfx::Point& location) {}
~~~
 Used to request the active client to show a context menu at |location|.
