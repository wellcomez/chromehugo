
## class SerialChooser
 Token representing an open serial port chooser prompt. Destroying this
 object should cancel the prompt.

### SerialChooser

SerialChooser
~~~cpp
SerialChooser(const SerialChooser&) = delete;
~~~

### operator=

SerialChooser::operator=
~~~cpp
SerialChooser& operator=(const SerialChooser&) = delete;
~~~

### ~SerialChooser

SerialChooser::~SerialChooser
~~~cpp
~SerialChooser()
~~~
