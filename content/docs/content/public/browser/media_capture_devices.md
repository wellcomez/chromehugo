
## class MediaCaptureDevices
 This is a singleton class, used to get Audio/Video devices, it must be
 called in UI thread.

### GetAudioCaptureDevices

MediaCaptureDevices::GetAudioCaptureDevices
~~~cpp
virtual const blink::MediaStreamDevices& GetAudioCaptureDevices() = 0;
~~~
 Return all Audio/Video devices.

### GetVideoCaptureDevices

MediaCaptureDevices::GetVideoCaptureDevices
~~~cpp
virtual const blink::MediaStreamDevices& GetVideoCaptureDevices() = 0;
~~~

### AddVideoCaptureObserver

MediaCaptureDevices::AddVideoCaptureObserver
~~~cpp
virtual void AddVideoCaptureObserver(
      media::VideoCaptureObserver* observer) = 0;
~~~

### RemoveAllVideoCaptureObservers

MediaCaptureDevices::RemoveAllVideoCaptureObservers
~~~cpp
virtual void RemoveAllVideoCaptureObservers() = 0;
~~~

### MediaCaptureDevices

MediaCaptureDevices
~~~cpp
MediaCaptureDevices() {}
~~~

### ~MediaCaptureDevices

~MediaCaptureDevices
~~~cpp
virtual ~MediaCaptureDevices() {}
~~~
