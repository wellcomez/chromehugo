### mojo::PendingRemote&lt;network::mojom::URLLoaderFactory&gt;
CreateWebUIURLLoaderFactory

mojo::PendingRemote&lt;network::mojom::URLLoaderFactory&gt;
CreateWebUIURLLoaderFactory
~~~cpp
CONTENT_EXPORT
mojo::PendingRemote<network::mojom::URLLoaderFactory>
CreateWebUIURLLoaderFactory(RenderFrameHost* render_frame_host,
                            const std::string& scheme,
                            base::flat_set<std::string> allowed_hosts);
~~~
 Create and bind a URLLoaderFactory for loading resources matching the
 specified |scheme| and also from a "pseudo host" matching one in
 |allowed_hosts|.


 The factory will only create loaders for requests with the same scheme as
 |scheme|. This is needed because there is more than one scheme used for
 WebUI, and not all have WebUI bindings.

### mojo::PendingRemote&lt;network::mojom::URLLoaderFactory&gt;
CreateWebUIServiceWorkerLoaderFactory

mojo::PendingRemote&lt;network::mojom::URLLoaderFactory&gt;
CreateWebUIServiceWorkerLoaderFactory
~~~cpp
CONTENT_EXPORT
mojo::PendingRemote<network::mojom::URLLoaderFactory>
CreateWebUIServiceWorkerLoaderFactory(
    BrowserContext* browser_context,
    const std::string& scheme,
    base::flat_set<std::string> allowed_hosts);
~~~

