
## class LoginDelegate
 Interface for getting login credentials for HTTP auth requests. If the login
 delegate obtains credentials, it should call the LoginAuthRequiredCallback
 passed to it on creation. If it is destroyed before that point, the request
 has been canceled and the callback should not be called.

### ~LoginDelegate

~LoginDelegate
~~~cpp
virtual ~LoginDelegate() = default;
~~~
