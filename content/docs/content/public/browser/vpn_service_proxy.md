
## class VpnServiceProxy
 Describes interface for communication with an external VpnService.

 All the methods below can only be called on the UI thread.

### ~VpnServiceProxy

~VpnServiceProxy
~~~cpp
virtual ~VpnServiceProxy() {}
~~~

### Bind

VpnServiceProxy::Bind
~~~cpp
virtual void Bind(const std::string& host_id,
                    const std::string& configuration_id,
                    const std::string& configuration_name,
                    SuccessCallback success,
                    FailureCallback failure,
                    std::unique_ptr<PepperVpnProviderResourceHostProxy>
                        pepper_vpn_provider_proxy) = 0;
~~~
 Binds an existing VPN connection in the VpnService. Registers with the
 VpnService the Resource host back-end.

### SendPacket

VpnServiceProxy::SendPacket
~~~cpp
virtual void SendPacket(const std::string& host_id,
                          const std::vector<char>& data,
                          SuccessCallback success,
                          FailureCallback failure) = 0;
~~~
 Sends an IP packet to the VpnService.
