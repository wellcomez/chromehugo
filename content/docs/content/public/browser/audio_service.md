### std::unique_ptr&lt;media::AudioSystem&gt;
CreateAudioSystemForAudioService

std::unique_ptr&lt;media::AudioSystem&gt;
CreateAudioSystemForAudioService
~~~cpp
CONTENT_EXPORT std::unique_ptr<media::AudioSystem>
CreateAudioSystemForAudioService();
~~~
 Creates an instance of AudioSystem for use with the Audio Service, bound to
 the thread it's used on for the first time.

### AudioServiceStreamFactoryBinder
GetAudioServiceStreamFactoryBinder

AudioServiceStreamFactoryBinder
GetAudioServiceStreamFactoryBinder
~~~cpp
CONTENT_EXPORT AudioServiceStreamFactoryBinder
GetAudioServiceStreamFactoryBinder();
~~~

