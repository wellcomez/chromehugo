
## class NetworkContextClientBase
    : public
 This is a mostly empty NetworkContextClient implementation that code can use
 as a default client. The only method it implements is OnFileUploadRequested
 so that POSTs in a given NetworkContext work.

### ~NetworkContextClientBase

NetworkContextClientBase
    : public::~NetworkContextClientBase
~~~cpp
~NetworkContextClientBase() override
~~~

### OnFileUploadRequested

NetworkContextClientBase
    : public::OnFileUploadRequested
~~~cpp
void OnFileUploadRequested(int32_t process_id,
                             bool async,
                             const std::vector<base::FilePath>& file_paths,
                             const GURL& destination_url,
                             OnFileUploadRequestedCallback callback) override;
~~~
 network::mojom::NetworkContextClient implementation:
### OnCanSendReportingReports

NetworkContextClientBase
    : public::OnCanSendReportingReports
~~~cpp
void OnCanSendReportingReports(
      const std::vector<url::Origin>& origins,
      OnCanSendReportingReportsCallback callback) override;
~~~

### OnCanSendDomainReliabilityUpload

NetworkContextClientBase
    : public::OnCanSendDomainReliabilityUpload
~~~cpp
void OnCanSendDomainReliabilityUpload(
      const url::Origin& origin,
      OnCanSendDomainReliabilityUploadCallback callback) override;
~~~

### OnGenerateHttpNegotiateAuthToken

NetworkContextClientBase
    : public::OnGenerateHttpNegotiateAuthToken
~~~cpp
void OnGenerateHttpNegotiateAuthToken(
      const std::string& server_auth_token,
      bool can_delegate,
      const std::string& auth_negotiate_android_account_type,
      const std::string& spn,
      OnGenerateHttpNegotiateAuthTokenCallback callback) override;
~~~

### OnTrustAnchorUsed

NetworkContextClientBase
    : public::OnTrustAnchorUsed
~~~cpp
void OnTrustAnchorUsed() override;
~~~

### OnCanSendSCTAuditingReport

NetworkContextClientBase
    : public::OnCanSendSCTAuditingReport
~~~cpp
void OnCanSendSCTAuditingReport(
      OnCanSendSCTAuditingReportCallback callback) override;
~~~

### OnNewSCTAuditingReportSent

NetworkContextClientBase
    : public::OnNewSCTAuditingReportSent
~~~cpp
void OnNewSCTAuditingReportSent() override;
~~~
