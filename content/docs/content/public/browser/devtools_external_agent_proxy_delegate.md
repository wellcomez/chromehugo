
## class DevToolsExternalAgentProxyDelegate
 Describes the interface for sending messages to an external DevTools agent.

### ~DevToolsExternalAgentProxyDelegate

~DevToolsExternalAgentProxyDelegate
~~~cpp
virtual ~DevToolsExternalAgentProxyDelegate() {}
~~~

### Attach

Attach
~~~cpp
virtual void Attach(DevToolsExternalAgentProxy* proxy) = 0;
~~~
 Informs the agent that a client has attached.

### Detach

Detach
~~~cpp
virtual void Detach(DevToolsExternalAgentProxy* proxy) = 0;
~~~
 Informs the agent that a client has detached.

### SendMessageToBackend

SendMessageToBackend
~~~cpp
virtual void SendMessageToBackend(DevToolsExternalAgentProxy* proxy,
                                    base::span<const uint8_t> message) = 0;
~~~
 Sends a message to the agent from a client.

### GetType

GetType
~~~cpp
virtual std::string GetType() = 0;
~~~
 Returns agent host type.

### GetTitle

GetTitle
~~~cpp
virtual std::string GetTitle() = 0;
~~~
 Returns agent host title.

### GetDescription

GetDescription
~~~cpp
virtual std::string GetDescription() = 0;
~~~
 Returns the agent host description.

### GetURL

GetURL
~~~cpp
virtual GURL GetURL() = 0;
~~~
 Returns url associated with agent host.

### GetFaviconURL

GetFaviconURL
~~~cpp
virtual GURL GetFaviconURL() = 0;
~~~
 Returns the favicon url for this agent host.

### GetFrontendURL

GetFrontendURL
~~~cpp
virtual std::string GetFrontendURL() = 0;
~~~
 Returns the front-end url for this agent host.

### Activate

Activate
~~~cpp
virtual bool Activate() = 0;
~~~
 Activates agent host.

### Reload

Reload
~~~cpp
virtual void Reload() = 0;
~~~
 Reloads agent host.

### Close

Close
~~~cpp
virtual bool Close() = 0;
~~~
 Reloads agent host.

### GetLastActivityTime

GetLastActivityTime
~~~cpp
virtual base::TimeTicks GetLastActivityTime() = 0;
~~~
 Returns the time when the host was last active.

### ~DevToolsExternalAgentProxyDelegate

~DevToolsExternalAgentProxyDelegate
~~~cpp
virtual ~DevToolsExternalAgentProxyDelegate() {}
~~~

### Attach

Attach
~~~cpp
virtual void Attach(DevToolsExternalAgentProxy* proxy) = 0;
~~~
 Informs the agent that a client has attached.

### Detach

Detach
~~~cpp
virtual void Detach(DevToolsExternalAgentProxy* proxy) = 0;
~~~
 Informs the agent that a client has detached.

### SendMessageToBackend

SendMessageToBackend
~~~cpp
virtual void SendMessageToBackend(DevToolsExternalAgentProxy* proxy,
                                    base::span<const uint8_t> message) = 0;
~~~
 Sends a message to the agent from a client.

### GetType

GetType
~~~cpp
virtual std::string GetType() = 0;
~~~
 Returns agent host type.

### GetTitle

GetTitle
~~~cpp
virtual std::string GetTitle() = 0;
~~~
 Returns agent host title.

### GetDescription

GetDescription
~~~cpp
virtual std::string GetDescription() = 0;
~~~
 Returns the agent host description.

### GetURL

GetURL
~~~cpp
virtual GURL GetURL() = 0;
~~~
 Returns url associated with agent host.

### GetFaviconURL

GetFaviconURL
~~~cpp
virtual GURL GetFaviconURL() = 0;
~~~
 Returns the favicon url for this agent host.

### GetFrontendURL

GetFrontendURL
~~~cpp
virtual std::string GetFrontendURL() = 0;
~~~
 Returns the front-end url for this agent host.

### Activate

Activate
~~~cpp
virtual bool Activate() = 0;
~~~
 Activates agent host.

### Reload

Reload
~~~cpp
virtual void Reload() = 0;
~~~
 Reloads agent host.

### Close

Close
~~~cpp
virtual bool Close() = 0;
~~~
 Reloads agent host.

### GetLastActivityTime

GetLastActivityTime
~~~cpp
virtual base::TimeTicks GetLastActivityTime() = 0;
~~~
 Returns the time when the host was last active.
