
## struct BackgroundSyncParameters

### BackgroundSyncParameters

BackgroundSyncParameters::BackgroundSyncParameters
~~~cpp
BackgroundSyncParameters(const BackgroundSyncParameters& other)
~~~

### operator=

BackgroundSyncParameters::operator=
~~~cpp
BackgroundSyncParameters& operator=(const BackgroundSyncParameters& other);
~~~

### operator==

BackgroundSyncParameters::operator==
~~~cpp
bool operator==(const BackgroundSyncParameters& other) const;
~~~
