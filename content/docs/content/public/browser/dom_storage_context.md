
## class DOMStorageContext
 Represents the per-BrowserContext Local Storage data.

### GetLocalStorageUsage

GetLocalStorageUsage
~~~cpp
virtual void GetLocalStorageUsage(GetLocalStorageUsageCallback callback) = 0;
~~~
 Returns a collection of StorageKeys using local storage to the given
 callback.

### GetSessionStorageUsage

GetSessionStorageUsage
~~~cpp
virtual void GetSessionStorageUsage(
      GetSessionStorageUsageCallback callback) = 0;
~~~
 Returns a collection of StorageKeys using session storage to the given
 callback.

### DeleteLocalStorage

DeleteLocalStorage
~~~cpp
virtual void DeleteLocalStorage(const blink::StorageKey& storage_key,
                                  base::OnceClosure callback) = 0;
~~~
 Deletes the local storage for `storage_key`. `callback` is called when the
 deletion is sent to the database and GetLocalStorageUsage() will not return
 entries for `storage_key` anymore.

### PerformLocalStorageCleanup

PerformLocalStorageCleanup
~~~cpp
virtual void PerformLocalStorageCleanup(base::OnceClosure callback) = 0;
~~~
 Removes traces of deleted data from the local storage backend.

### DeleteSessionStorage

DeleteSessionStorage
~~~cpp
virtual void DeleteSessionStorage(const SessionStorageUsageInfo& usage_info,
                                    base::OnceClosure callback) = 0;
~~~
 Deletes the session storage data identified by `usage_info`.

### PerformSessionStorageCleanup

PerformSessionStorageCleanup
~~~cpp
virtual void PerformSessionStorageCleanup(base::OnceClosure callback) = 0;
~~~

### RecreateSessionStorage

RecreateSessionStorage
~~~cpp
virtual scoped_refptr<SessionStorageNamespace> RecreateSessionStorage(
      const std::string& namespace_id) = 0;
~~~
 Creates a SessionStorageNamespace with the given `namespace_id`. Used
 after tabs are restored by session restore. When created, the
 SessionStorageNamespace with the correct `namespace_id` will be
 associated with the persisted sessionStorage data.

### StartScavengingUnusedSessionStorage

StartScavengingUnusedSessionStorage
~~~cpp
virtual void StartScavengingUnusedSessionStorage() = 0;
~~~
 Starts deleting sessionStorages which don't have an associated
 SessionStorageNamespace alive. Called when SessionStorageNamespaces have
 been created after a session restore, or a session restore won't happen.

### ~DOMStorageContext

~DOMStorageContext
~~~cpp
virtual ~DOMStorageContext() {}
~~~

### GetLocalStorageUsage

GetLocalStorageUsage
~~~cpp
virtual void GetLocalStorageUsage(GetLocalStorageUsageCallback callback) = 0;
~~~
 Returns a collection of StorageKeys using local storage to the given
 callback.

### GetSessionStorageUsage

GetSessionStorageUsage
~~~cpp
virtual void GetSessionStorageUsage(
      GetSessionStorageUsageCallback callback) = 0;
~~~
 Returns a collection of StorageKeys using session storage to the given
 callback.

### DeleteLocalStorage

DeleteLocalStorage
~~~cpp
virtual void DeleteLocalStorage(const blink::StorageKey& storage_key,
                                  base::OnceClosure callback) = 0;
~~~
 Deletes the local storage for `storage_key`. `callback` is called when the
 deletion is sent to the database and GetLocalStorageUsage() will not return
 entries for `storage_key` anymore.

### PerformLocalStorageCleanup

PerformLocalStorageCleanup
~~~cpp
virtual void PerformLocalStorageCleanup(base::OnceClosure callback) = 0;
~~~
 Removes traces of deleted data from the local storage backend.

### DeleteSessionStorage

DeleteSessionStorage
~~~cpp
virtual void DeleteSessionStorage(const SessionStorageUsageInfo& usage_info,
                                    base::OnceClosure callback) = 0;
~~~
 Deletes the session storage data identified by `usage_info`.

### PerformSessionStorageCleanup

PerformSessionStorageCleanup
~~~cpp
virtual void PerformSessionStorageCleanup(base::OnceClosure callback) = 0;
~~~

### RecreateSessionStorage

RecreateSessionStorage
~~~cpp
virtual scoped_refptr<SessionStorageNamespace> RecreateSessionStorage(
      const std::string& namespace_id) = 0;
~~~
 Creates a SessionStorageNamespace with the given `namespace_id`. Used
 after tabs are restored by session restore. When created, the
 SessionStorageNamespace with the correct `namespace_id` will be
 associated with the persisted sessionStorage data.

### StartScavengingUnusedSessionStorage

StartScavengingUnusedSessionStorage
~~~cpp
virtual void StartScavengingUnusedSessionStorage() = 0;
~~~
 Starts deleting sessionStorages which don't have an associated
 SessionStorageNamespace alive. Called when SessionStorageNamespaces have
 been created after a session restore, or a session restore won't happen.

### ~DOMStorageContext

~DOMStorageContext
~~~cpp
virtual ~DOMStorageContext() {}
~~~
