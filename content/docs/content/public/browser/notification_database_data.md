
## struct NotificationDatabaseData
 Stores information about a Web Notification as available in the notification
 database. Beyond the notification's own data, its id and attribution need
 to be available for users of the database as well.

 Note: There are extra properties being stored for UKM logging purposes.

 TODO(https://crbug.com/842622): Add the UKM that will use these properties.

### NotificationDatabaseData

NotificationDatabaseData::NotificationDatabaseData
~~~cpp
NotificationDatabaseData(const NotificationDatabaseData& other)
~~~

### ~NotificationDatabaseData

NotificationDatabaseData::~NotificationDatabaseData
~~~cpp
~NotificationDatabaseData()
~~~

### operator=

NotificationDatabaseData::operator=
~~~cpp
NotificationDatabaseData& operator=(const NotificationDatabaseData& other);
~~~
