
## struct BackForwardCache::DisabledReason
 Represents a reason to disable back-forward cache, given by a |source|.

 |context| is arbitrary context that will be preserved and passed through,
 e.g. an extension ID responsible for disabling BFCache that can be shown in
 passed devtools. It preserves the |description| and |context| that
 accompany it, however they are ignored for <, == and !=.

### DisabledReason

DisabledReason::BackForwardCache::DisabledReason
~~~cpp
DisabledReason(BackForwardCache::DisabledSource source,
                   BackForwardCache::DisabledReasonType id,
                   std::string description,
                   std::string context,
                   std::string report_string)
~~~

### DisabledReason

DisabledReason::BackForwardCache::DisabledReason
~~~cpp
DisabledReason(const DisabledReason&)
~~~

### operator&lt;

DisabledReason::BackForwardCache::operator&lt;
~~~cpp
bool operator<(const DisabledReason&) const;
~~~

### operator==

DisabledReason::BackForwardCache::operator==
~~~cpp
bool operator==(const DisabledReason&) const;
~~~

### operator!=

DisabledReason::BackForwardCache::operator!=
~~~cpp
bool operator!=(const DisabledReason&) const;
~~~

## class BackForwardCache
 Public API for the BackForwardCache.


 After the user navigates away from a document, the old one might go into the
 frozen state and will be kept in the cache. It can potentially be reused
 at a later time if the user navigates back.


 Not all documents can or will be cached. You should not assume a document
 will be cached.


 All methods of this class should be called from the UI thread.

### DisableForRenderFrameHost

BackForwardCache::DisableForRenderFrameHost
~~~cpp
static void DisableForRenderFrameHost(
      RenderFrameHost* render_frame_host,
      DisabledReason reason,
      absl::optional<ukm::SourceId> source_id = absl::nullopt);
~~~
 Prevents the `render_frame_host` from entering the BackForwardCache. A
 RenderFrameHost can only enter the BackForwardCache if the main one and all
 its children can. This action can not be undone. Any document that is
 assigned to this same RenderFrameHost in the future will not be cached
 either. In practice this is not a big deal as only navigations that use a
 new frame can be cached.


 This might be needed for example by components that listen to events via a
 WebContentsObserver and keep some sort of per frame state, as this state
 might be lost and not be recreated when navigating back.


 If the page is already in the cache an eviction is triggered.


 `render_frame_host`: non-null.

 `reason`: Describes who is disabling this and why.

 `source_id`: see
 `BackForwardCacheCanStoreDocumentResult::DisabledReasonsMap` for what it
 means and when it's set.

### DisableForRenderFrameHost

BackForwardCache::DisableForRenderFrameHost
~~~cpp
static void DisableForRenderFrameHost(
      GlobalRenderFrameHostId id,
      DisabledReason reason,
      absl::optional<ukm::SourceId> source_id = absl::nullopt);
~~~
 Helper function to be used when it is not always possible to guarantee the
 `render_frame_host` to be still alive when this is called. In this case,
 its `id` can be used.

 For what `source_id` means and when it's set, see
 `BackForwardCacheCanStoreDocumentResult::DisabledReasonsMap`.

### SetHadFormDataAssociated

BackForwardCache::SetHadFormDataAssociated
~~~cpp
static void SetHadFormDataAssociated(Page& page);
~~~
 Helper function to be used when the input |page| has seen any form data
 associated. This state will be set on the BackForwardCacheMetrics
 associated with the main frame, is not persisted across session restores,
 and only set in Android Custom tabs for now.

 TODO(crbug.com/1403292): Set this boolean for all platforms.

### Flush

BackForwardCache::Flush
~~~cpp
virtual void Flush() = 0;
~~~
 Evict all entries from the BackForwardCache.

### Prune

BackForwardCache::Prune
~~~cpp
virtual void Prune(size_t limit) = 0;
~~~
 Evict back/forward cache entries from the least recently used ones until
 the cache is within the given size limit.

### DisableForTesting

BackForwardCache::DisableForTesting
~~~cpp
virtual void DisableForTesting(DisableForTestingReason reason) = 0;
~~~
 Disables the BackForwardCache so that no documents will be stored/served.

 This allows tests to "force" not using the BackForwardCache, this can be
 useful when:
 * Tests rely on a new document being loaded.

 * Tests want to test this case specifically.

 Callers should pass an accurate |reason| to make future triaging of
 disabled tests easier.


 Note: It's preferable to make tests BackForwardCache compatible
 when feasible, rather than using this method. Also please consider whether
 you actually should have 2 tests, one with the document cached
 (BackForwardCache enabled), and one without.
