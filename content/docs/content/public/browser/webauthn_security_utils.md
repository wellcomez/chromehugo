### blink::mojom::AuthenticatorStatus
OriginAllowedToMakeWebAuthnRequests

blink::mojom::AuthenticatorStatus
OriginAllowedToMakeWebAuthnRequests
~~~cpp
CONTENT_EXPORT blink::mojom::AuthenticatorStatus
OriginAllowedToMakeWebAuthnRequests(url::Origin caller_origin);
~~~
 Returns AuthenticatorStatus::SUCCESS if the caller origin is in principle
 authorized to make WebAuthn requests, and an error if it fails some criteria,
 e.g. an insecure protocol or domain.


 Reference https://url.spec.whatwg.org/#valid-domain-string and
 https://html.spec.whatwg.org/multipage/origin.html#concept-origin-effective-domain.

### OriginIsAllowedToClaimRelyingPartyId

OriginIsAllowedToClaimRelyingPartyId
~~~cpp
CONTENT_EXPORT bool OriginIsAllowedToClaimRelyingPartyId(
    const std::string& claimed_relying_party_id,
    const url::Origin& caller_origin);
~~~
 Returns whether a caller origin is allowed to claim a given Relying Party ID.

 It's valid for the requested RP ID to be a registrable domain suffix of, or
 be equal to, the origin's effective domain.  Reference:
 https://html.spec.whatwg.org/multipage/origin.html#is-a-registrable-domain-suffix-of-or-is-equal-to.

