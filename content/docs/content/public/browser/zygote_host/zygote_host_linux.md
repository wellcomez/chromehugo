
## class ZygoteHost
 https://chromium.googlesource.com/chromium/src/+/main/docs/linux/zygote.md
 The zygote host is an interface, in the browser process, to the zygote
 process.

### GetInstance

ZygoteHost::GetInstance
~~~cpp
static CONTENT_EXPORT ZygoteHost* GetInstance();
~~~
 Returns the singleton instance.

### ~ZygoteHost

~ZygoteHost
~~~cpp
virtual ~ZygoteHost() {}
~~~

### IsZygotePid

IsZygotePid
~~~cpp
virtual bool IsZygotePid(pid_t pid) = 0;
~~~
 Returns the pid of the Zygote process.

### GetRendererSandboxStatus

GetRendererSandboxStatus
~~~cpp
virtual int GetRendererSandboxStatus() = 0;
~~~
 Returns an int which is a bitmask of kSandboxLinux* values. Only valid
 after the first render has been forked.

### AdjustRendererOOMScore

AdjustRendererOOMScore
~~~cpp
virtual void AdjustRendererOOMScore(base::ProcessHandle process_handle,
                                      int score) = 0;
~~~
 Adjust the OOM score of the given renderer's PID.  The allowed
 range for the score is [0, 1000], where higher values are more
 likely to be killed by the OOM killer.

### ReinitializeLogging

ReinitializeLogging
~~~cpp
virtual void ReinitializeLogging(uint32_t logging_dest,
                                   base::PlatformFile log_file_fd) = 0;
~~~
 Reinitialize logging for the Zygote processes. Needed on ChromeOS, which
 switches to a log file in the user's home directory once they log in.

### ~ZygoteHost

~ZygoteHost
~~~cpp
virtual ~ZygoteHost() {}
~~~

### IsZygotePid

IsZygotePid
~~~cpp
virtual bool IsZygotePid(pid_t pid) = 0;
~~~
 Returns the pid of the Zygote process.

### GetRendererSandboxStatus

GetRendererSandboxStatus
~~~cpp
virtual int GetRendererSandboxStatus() = 0;
~~~
 Returns an int which is a bitmask of kSandboxLinux* values. Only valid
 after the first render has been forked.

### AdjustRendererOOMScore

AdjustRendererOOMScore
~~~cpp
virtual void AdjustRendererOOMScore(base::ProcessHandle process_handle,
                                      int score) = 0;
~~~
 Adjust the OOM score of the given renderer's PID.  The allowed
 range for the score is [0, 1000], where higher values are more
 likely to be killed by the OOM killer.

### GetInstance

ZygoteHost::GetInstance
~~~cpp
static CONTENT_EXPORT ZygoteHost* GetInstance();
~~~
 Returns the singleton instance.
