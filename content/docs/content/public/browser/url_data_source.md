
## class URLDataSource
 A URLDataSource is an object that can answer requests for WebUI data
 asynchronously. An implementation of URLDataSource should handle calls to
 StartDataRequest() by starting its (implementation-specific) asynchronous
 request for the data, then running the callback given in that method to
 notify.

### URLToRequestPath

URLDataSource::URLToRequestPath
~~~cpp
static std::string URLToRequestPath(const GURL& url);
~~~
 Parse |url| to get the path which will be used to resolve the request. The
 path is the remaining portion after the scheme and hostname, without the
 leading slash.

### ~URLDataSource

~URLDataSource
~~~cpp
virtual ~URLDataSource() {}
~~~

### GetSource

URLDataSource::GetSource
~~~cpp
virtual std::string GetSource() = 0;
~~~
 The name of this source.

 E.g., for favicons, this could be "favicon", which results in paths for
 specific resources like "favicon/34" getting sent to this source. For
 sources where a scheme is used instead of the hostname as the unique
 identifier, the suffix "://" must be added to the return value, eg. for a
 URLDataSource which would display resources with URLs on the form
 your-scheme://anything , GetSource() must return "your-scheme://".

### StartDataRequest

URLDataSource::StartDataRequest
~~~cpp
virtual void StartDataRequest(const GURL& url,
                                const WebContents::Getter& wc_getter,
                                GotDataCallback callback) = 0;
~~~
 Called by URLDataSource to request data at |url|. The child class should
 run |callback| when the data is available or if the request could not be
 satisfied. This can be called either in this callback or asynchronously
 with the response. |wc_getter| can be called on the UI thread to return the
 WebContents for this request if it originates from a render frame. If it
 originated from a worker or if the frame has destructed it will return
 null.

### GetMimeType

URLDataSource::GetMimeType
~~~cpp
virtual std::string GetMimeType(const GURL& url) = 0;
~~~
 Return the mimetype that should be sent with this response, or empty
 string to specify no mime type.

### ShouldReplaceExistingSource

URLDataSource::ShouldReplaceExistingSource
~~~cpp
virtual bool ShouldReplaceExistingSource();
~~~
 Returns true if the URLDataSource should replace an existing URLDataSource
 with the same name that has already been registered. The default is true.


 TODO: nuke this and convert all callers to not replace.

### AllowCaching

URLDataSource::AllowCaching
~~~cpp
virtual bool AllowCaching();
~~~
 Returns true if responses from this URLDataSource can be cached.

### ShouldAddContentSecurityPolicy

URLDataSource::ShouldAddContentSecurityPolicy
~~~cpp
virtual bool ShouldAddContentSecurityPolicy();
~~~
 If you are overriding the following two methods, then you have a bug.

 It is not acceptable to disable content-security-policy on chrome:// pages
 to permit functionality excluded by CSP, such as inline script.

 Instead, you must go back and change your WebUI page so that it is
 compliant with the policy. This typically involves ensuring that all script
 is delivered through the data manager backend. Do not disable CSP on your
 page without first contacting the chrome security team.

### GetContentSecurityPolicy

URLDataSource::GetContentSecurityPolicy
~~~cpp
virtual std::string GetContentSecurityPolicy(
      network::mojom::CSPDirectiveName directive);
~~~
 By default, the following CSPs are added. Override to change this.

  - "child-src 'none';"
  - "object-src 'none';"
  - "frame ancestors: 'none'" is added to the CSP unless
    ShouldDenyXFrameOptions() returns false
  - "script-src chrome://resources 'self';"
### GetCrossOriginOpenerPolicy

URLDataSource::GetCrossOriginOpenerPolicy
~~~cpp
virtual std::string GetCrossOriginOpenerPolicy();
~~~
 By default, neither of these headers are set. Override to change this.

 TODO(https://crbug.com/1189194): Consider setting COOP:same-origin and
 COEP:require-corp as the default instead.

### GetCrossOriginEmbedderPolicy

URLDataSource::GetCrossOriginEmbedderPolicy
~~~cpp
virtual std::string GetCrossOriginEmbedderPolicy();
~~~

### GetCrossOriginResourcePolicy

URLDataSource::GetCrossOriginResourcePolicy
~~~cpp
virtual std::string GetCrossOriginResourcePolicy();
~~~

### ShouldDenyXFrameOptions

URLDataSource::ShouldDenyXFrameOptions
~~~cpp
virtual bool ShouldDenyXFrameOptions();
~~~
 By default, the "X-Frame-Options: DENY" header is sent. To stop this from
 happening, return false. It is OK to return false as needed.

### ShouldServiceRequest

URLDataSource::ShouldServiceRequest
~~~cpp
virtual bool ShouldServiceRequest(const GURL& url,
                                    BrowserContext* browser_context,
                                    int render_process_id);
~~~
 By default, only chrome: and devtools: requests are allowed.

 Override in specific WebUI data sources to enable for additional schemes or
 to implement fancier access control.  Typically used in concert with
 ContentBrowserClient::GetAdditionalWebUISchemes() to permit additional
 WebUI scheme support for an embedder.

### ShouldServeMimeTypeAsContentTypeHeader

URLDataSource::ShouldServeMimeTypeAsContentTypeHeader
~~~cpp
virtual bool ShouldServeMimeTypeAsContentTypeHeader();
~~~
 By default, Content-Type: header is not sent along with the response.

 To start sending mime type returned by GetMimeType in HTTP headers,
 return true. It is useful when tunneling response served from this data
 source programmatically. Or when AppCache is enabled for this source as it
 is for devtools.

### GetAccessControlAllowOriginForOrigin

URLDataSource::GetAccessControlAllowOriginForOrigin
~~~cpp
virtual std::string GetAccessControlAllowOriginForOrigin(
      const std::string& origin);
~~~
 This method is called when the request contains "Origin:" header. The value
 of the header is passed in |origin| parameter. If the returned value is not
 empty, it is used as a value for "Access-Control-Allow-Origin:" response
 header, otherwise the header is not set. This method should return either
 |origin|, or "*", or "none", or empty string.

 Default implementation returns an empty string.

### GetReplacements

URLDataSource::GetReplacements
~~~cpp
virtual const ui::TemplateReplacements* GetReplacements();
~~~
 Replacements for i18n or null if no replacements are desired.

### ShouldReplaceI18nInJS

URLDataSource::ShouldReplaceI18nInJS
~~~cpp
virtual bool ShouldReplaceI18nInJS();
~~~
 Whether i18n template expression replacement should be allowed in HTML
 templates within JS files.
