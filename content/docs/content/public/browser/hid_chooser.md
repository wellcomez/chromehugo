
## class HidChooser
 Token representing an open HID device chooser prompt. Destroying this
 object should cancel the prompt.

### HidChooser

HidChooser
~~~cpp
HidChooser() = default;
~~~

### HidChooser

HidChooser
~~~cpp
HidChooser(const HidChooser&) = delete;
~~~

### operator=

HidChooser::operator=
~~~cpp
HidChooser& operator=(const HidChooser&) = delete;
~~~

### ~HidChooser

~HidChooser
~~~cpp
virtual ~HidChooser() = default;
~~~
