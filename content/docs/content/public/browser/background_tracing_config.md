
## class BackgroundTracingConfig
 BackgroundTracingConfig is passed to the BackgroundTracingManager to
 setup the trigger rules used to enable/disable background tracing.

### tracing_mode

tracing_mode
~~~cpp
TracingMode tracing_mode() const { return tracing_mode_; }
~~~

### scenario_name

scenario_name
~~~cpp
const std::string& scenario_name() const { return scenario_name_; }
~~~

### has_crash_scenario

has_crash_scenario
~~~cpp
bool has_crash_scenario() const { return has_crash_scenario_; }
~~~

### FromDict

BackgroundTracingConfig::FromDict
~~~cpp
static std::unique_ptr<BackgroundTracingConfig> FromDict(
      base::Value::Dict&& dict);
~~~

### ToDict

BackgroundTracingConfig::ToDict
~~~cpp
virtual base::Value::Dict ToDict() = 0;
~~~

### SetPackageNameFilteringEnabled

BackgroundTracingConfig::SetPackageNameFilteringEnabled
~~~cpp
virtual void SetPackageNameFilteringEnabled(bool) = 0;
~~~

### BackgroundTracingConfig

BackgroundTracingConfig::BackgroundTracingConfig
~~~cpp
BackgroundTracingConfig(TracingMode tracing_mode)
~~~
