
## class ServiceWorkerClientInfo
 Holds information about a single service worker client:
 https://w3c.github.io/ServiceWorker/#client
### blink::mojom::ServiceWorkerClientType type

blink::mojom::ServiceWorkerClientType type
~~~cpp
explicit ServiceWorkerClientInfo(
      const blink::DedicatedWorkerToken& dedicated_worker_token);
  explicit ServiceWorkerClientInfo(
      const blink::SharedWorkerToken& shared_worker_token);
  explicit ServiceWorkerClientInfo(
      const DedicatedOrSharedWorkerToken& worker_token);

  ServiceWorkerClientInfo(const ServiceWorkerClientInfo& other);
  ServiceWorkerClientInfo& operator=(const ServiceWorkerClientInfo& other);

  ~ServiceWorkerClientInfo();

  // Returns the type of this client.
  blink::mojom::ServiceWorkerClientType type() const { return type_; }
~~~

### GetRenderFrameHostId

ServiceWorkerClientInfo::GetRenderFrameHostId
~~~cpp
GlobalRenderFrameHostId GetRenderFrameHostId() const;
~~~

### SetRenderFrameHostId

ServiceWorkerClientInfo::SetRenderFrameHostId
~~~cpp
void SetRenderFrameHostId(
      const GlobalRenderFrameHostId& render_frame_host_id);
~~~

### GetDedicatedWorkerToken

ServiceWorkerClientInfo::GetDedicatedWorkerToken
~~~cpp
blink::DedicatedWorkerToken GetDedicatedWorkerToken() const;
~~~
 Returns the corresponding DedicatedWorkerToken. This should only be called
 if "type() == blink::mojom::ServiceWorkerClientType::kDedicatedWorker".

### GetSharedWorkerToken

ServiceWorkerClientInfo::GetSharedWorkerToken
~~~cpp
blink::SharedWorkerToken GetSharedWorkerToken() const;
~~~
 Returns the corresponding SharedWorkerToken. This should only be called
 if "type() == blink::mojom::ServiceWorkerClientType::kSharedWorker".
