
## struct VoiceData
 Information about one voice.

### VoiceData

VoiceData::VoiceData
~~~cpp
VoiceData(const VoiceData& other)
~~~

### ~VoiceData

VoiceData::~VoiceData
~~~cpp
~VoiceData()
~~~

## class TtsEngineDelegate
 Interface that delegates TTS requests to engines in content embedders.

### GetVoices

TtsEngineDelegate::GetVoices
~~~cpp
virtual void GetVoices(BrowserContext* browser_context,
                         const GURL& source_url,
                         std::vector<VoiceData>* out_voices) = 0;
~~~
 Return a list of all available voices registered. |source_url| will be used
 for policy decisions by engines to determine which voices to return.

### Speak

TtsEngineDelegate::Speak
~~~cpp
virtual void Speak(TtsUtterance* utterance, const VoiceData& voice) = 0;
~~~
 Speak the given utterance by sending an event to the given TTS engine.

### Stop

TtsEngineDelegate::Stop
~~~cpp
virtual void Stop(TtsUtterance* utterance) = 0;
~~~
 Stop speaking the given utterance by sending an event to the target
 associated with this utterance.

### Pause

TtsEngineDelegate::Pause
~~~cpp
virtual void Pause(TtsUtterance* utterance) = 0;
~~~
 Pause in the middle of speaking this utterance.

### Resume

TtsEngineDelegate::Resume
~~~cpp
virtual void Resume(TtsUtterance* utterance) = 0;
~~~
 Resume speaking this utterance.

### LoadBuiltInTtsEngine

TtsEngineDelegate::LoadBuiltInTtsEngine
~~~cpp
virtual void LoadBuiltInTtsEngine(BrowserContext* browser_context) = 0;
~~~
 Load the built-in TTS engine.

### IsBuiltInTtsEngineInitialized

TtsEngineDelegate::IsBuiltInTtsEngineInitialized
~~~cpp
virtual bool IsBuiltInTtsEngineInitialized(
      BrowserContext* browser_context) = 0;
~~~
 Returns whether the built in engine is initialized.

## class RemoteTtsEngineDelegate
 Interface that delegates TTS requests to a remote engine from another browser
 process.

### GetVoices

RemoteTtsEngineDelegate::GetVoices
~~~cpp
virtual void GetVoices(BrowserContext* browser_context,
                         std::vector<VoiceData>* out_voices) = 0;
~~~
 Returns a list of voices from remote tts engine for |browser_context|.

### Speak

RemoteTtsEngineDelegate::Speak
~~~cpp
virtual void Speak(TtsUtterance* utterance, const VoiceData& voice) = 0;
~~~
 Requests the given remote TTS engine to speak |utterance| with |voice|.

### Stop

RemoteTtsEngineDelegate::Stop
~~~cpp
virtual void Stop(TtsUtterance* utterance) = 0;
~~~
 Requests the remote TTS engine associated with |utterance| to stop
 speaking the |utterance|.

### Pause

RemoteTtsEngineDelegate::Pause
~~~cpp
virtual void Pause(TtsUtterance* utterance) = 0;
~~~
 Requests the remote TTS engine associated with |utterance| to pause
 speaking the |utterance|.

### Resume

RemoteTtsEngineDelegate::Resume
~~~cpp
virtual void Resume(TtsUtterance* utterance) = 0;
~~~
 Requests the remote TTS engine associated with |utterance| to resume
 speaking the |utterance|.

## class TtsController
 Singleton class that manages text-to-speech for all TTS engines and
 APIs, maintaining a queue of pending utterances and keeping
 track of all state.

### SkipAddNetworkChangeObserverForTests

TtsController::SkipAddNetworkChangeObserverForTests
~~~cpp
static void SkipAddNetworkChangeObserverForTests(bool enabled);
~~~

### IsSpeaking

TtsController::IsSpeaking
~~~cpp
virtual bool IsSpeaking() = 0;
~~~
 Returns true if we're currently speaking an utterance.

### SpeakOrEnqueue

TtsController::SpeakOrEnqueue
~~~cpp
virtual void SpeakOrEnqueue(std::unique_ptr<TtsUtterance> utterance) = 0;
~~~
 Speak the given utterance. If the utterance's should_flush_queue flag is
 true, clears the speech queue including the currently speaking utterance
 (if one exists), and starts processing the speech queue by speaking the new
 utterance immediately. Otherwise, enqueues the new utterance and triggers
 continued processing of the speech queue.

### Stop

TtsController::Stop
~~~cpp
virtual void Stop() = 0;
~~~
 Stop all utterances and flush the queue. Implies leaving pause mode
 as well.

### Stop

TtsController::Stop
~~~cpp
virtual void Stop(const GURL& source_url) = 0;
~~~
 Stops the current utterance if it matches the given |source_url|.

### Pause

TtsController::Pause
~~~cpp
virtual void Pause() = 0;
~~~
 Pause the speech queue. Some engines may support pausing in the middle
 of an utterance.

### Resume

TtsController::Resume
~~~cpp
virtual void Resume() = 0;
~~~
 Resume speaking.

### OnTtsEvent

TtsController::OnTtsEvent
~~~cpp
virtual void OnTtsEvent(int utterance_id,
                          TtsEventType event_type,
                          int char_index,
                          int length,
                          const std::string& error_message) = 0;
~~~
 Handle events received from the speech engine. Events are forwarded to
 the callback function, and in addition, completion and error events
 trigger finishing the current utterance and starting the next one, if
 any. If the |char_index| or |length| are not available, the speech engine
 should pass -1.

### OnTtsUtteranceBecameInvalid

TtsController::OnTtsUtteranceBecameInvalid
~~~cpp
virtual void OnTtsUtteranceBecameInvalid(int utterance_id) = 0;
~~~
 Called when the utterance with |utterance_id| becomes invalid.

 For example, when the WebContents associated with the utterance
 living in a standalone browser is destroyed, the utterance becomes
 invalid and should not be spoken.

### GetVoices

TtsController::GetVoices
~~~cpp
virtual void GetVoices(BrowserContext* browser_context,
                         const GURL& source_url,
                         std::vector<VoiceData>* out_voices) = 0;
~~~
 Return a list of all available voices, including the native voice,
 if supported, and all voices registered by engines. |source_url|
 will be used for policy decisions by engines to determine which
 voices to return.

### VoicesChanged

TtsController::VoicesChanged
~~~cpp
virtual void VoicesChanged() = 0;
~~~
 Called by the content embedder or platform implementation when the
 list of voices may have changed and should be re-queried.

### AddVoicesChangedDelegate

TtsController::AddVoicesChangedDelegate
~~~cpp
virtual void AddVoicesChangedDelegate(VoicesChangedDelegate* delegate) = 0;
~~~
 Add a delegate that wants to be notified when the set of voices changes.

### RemoveVoicesChangedDelegate

TtsController::RemoveVoicesChangedDelegate
~~~cpp
virtual void RemoveVoicesChangedDelegate(VoicesChangedDelegate* delegate) = 0;
~~~
 Remove delegate that wants to be notified when the set of voices changes.

### RemoveUtteranceEventDelegate

TtsController::RemoveUtteranceEventDelegate
~~~cpp
virtual void RemoveUtteranceEventDelegate(
      UtteranceEventDelegate* delegate) = 0;
~~~
 Remove delegate that wants to be notified when an utterance fires an event.

 Note: this cancels speech from any utterance with this delegate, and
 removes any utterances with this delegate from the queue.

### SetTtsEngineDelegate

TtsController::SetTtsEngineDelegate
~~~cpp
virtual void SetTtsEngineDelegate(TtsEngineDelegate* delegate) = 0;
~~~
 Set the delegate that processes TTS requests with engines in a content
 embedder.

### SetRemoteTtsEngineDelegate

TtsController::SetRemoteTtsEngineDelegate
~~~cpp
virtual void SetRemoteTtsEngineDelegate(
      RemoteTtsEngineDelegate* delegate) = 0;
~~~
 Sets the delegate that processes TTS requests with the remote enigne.

### GetTtsEngineDelegate

TtsController::GetTtsEngineDelegate
~~~cpp
virtual TtsEngineDelegate* GetTtsEngineDelegate() = 0;
~~~
 Get the delegate that processes TTS requests with engines in a content
 embedder.

### RefreshVoices

TtsController::RefreshVoices
~~~cpp
virtual void RefreshVoices() = 0;
~~~
 Triggers the TtsPlatform to update its list of voices and relay that update
 through VoicesChanged.

### SetTtsPlatform

TtsController::SetTtsPlatform
~~~cpp
virtual void SetTtsPlatform(TtsPlatform* tts_platform) = 0;
~~~
 Visible for testing.

### QueueSize

TtsController::QueueSize
~~~cpp
virtual int QueueSize() = 0;
~~~

### StripSSML

TtsController::StripSSML
~~~cpp
virtual void StripSSML(
      const std::string& utterance,
      base::OnceCallback<void(const std::string&)> callback) = 0;
~~~
