
## struct NavigationController::LoadURLParams
 Extra optional parameters for LoadURLWithParams.

### LoadURLParams

LoadURLParams::NavigationController::LoadURLParams
~~~cpp
LoadURLParams(const GURL& url)
~~~

### LoadURLParams

LoadURLParams
~~~cpp
explicit LoadURLParams(const OpenURLParams& open_url_params);
    LoadURLParams(const LoadURLParams&) = delete;
~~~
 Copies |open_url_params| into LoadURLParams, attempting to copy all
 fields that are present in both structs (some properties are ignored
 because they are unique to LoadURLParams or OpenURLParams).

### LoadURLParams

LoadURLParams::NavigationController::LoadURLParams
~~~cpp
LoadURLParams(LoadURLParams&&)
~~~

### operator=

LoadURLParams::NavigationController::operator=
~~~cpp
LoadURLParams& operator=(const LoadURLParams&) = delete;
~~~

### operator=

LoadURLParams::NavigationController::operator=
~~~cpp
LoadURLParams& operator=(LoadURLParams&&);
~~~

### ~LoadURLParams

LoadURLParams::NavigationController::~LoadURLParams
~~~cpp
~LoadURLParams()
~~~

## struct NavigationController::LoadURLParams
 Extra optional parameters for LoadURLWithParams.

### LoadURLParams

LoadURLParams::NavigationController::LoadURLParams
~~~cpp
LoadURLParams(const GURL& url)
~~~

### LoadURLParams

LoadURLParams
~~~cpp
explicit LoadURLParams(const OpenURLParams& open_url_params);
    LoadURLParams(const LoadURLParams&) = delete;
~~~
 Copies |open_url_params| into LoadURLParams, attempting to copy all
 fields that are present in both structs (some properties are ignored
 because they are unique to LoadURLParams or OpenURLParams).

### LoadURLParams

LoadURLParams::NavigationController::LoadURLParams
~~~cpp
LoadURLParams(LoadURLParams&&)
~~~

### operator=

LoadURLParams::NavigationController::operator=
~~~cpp
LoadURLParams& operator=(const LoadURLParams&) = delete;
~~~

### operator=

LoadURLParams::NavigationController::operator=
~~~cpp
LoadURLParams& operator=(LoadURLParams&&);
~~~

### ~LoadURLParams

LoadURLParams::NavigationController::~LoadURLParams
~~~cpp
~LoadURLParams()
~~~

## class NavigationController
 A NavigationController manages session history, i.e., a back-forward list
 of navigation entries.


 FOR CONTENT EMBEDDERS: You can think of each WebContents as having one
 NavigationController. Technically, this is the NavigationController for
 the primary frame tree of the WebContents. See the comments for
 WebContents::GetPrimaryPage() for more about primary vs non-primary frame
 trees. This NavigationController is retrievable by
 WebContents::GetController(). It is the only one that affects the actual
 user-exposed session history list (e.g., via back/forward buttons). It is
 not intended to expose other NavigationControllers to the content/public
 API.


 FOR CONTENT INTERNALS: Be aware that NavigationControllerImpl is 1:1 with a
 FrameTree. With MPArch there can be multiple FrameTrees associated with a
 WebContents, so there can be multiple NavigationControllers associated with
 a WebContents. However only the primary one, and the
 NavigationEntries/events originating from it, is exposed to //content
 embedders. See docs/frame_trees.md for more details.

### enum LoadURLType

~~~cpp
enum LoadURLType {
    // For loads that do not fall into any types below.
    LOAD_TYPE_DEFAULT,

    // An http post load request.  The post data is passed in |post_data|.
    LOAD_TYPE_HTTP_POST,

    // Loads a 'data:' scheme URL with specified base URL and a history entry
    // URL. This is only safe to be used for browser-initiated data: URL
    // navigations, since it shows arbitrary content as if it comes from
    // |virtual_url_for_data_url|.
    LOAD_TYPE_DATA

    // Adding new LoadURLType? Also update LoadUrlParams.java static constants.
  }
~~~
### enum UserAgentOverrideOption

~~~cpp
enum UserAgentOverrideOption {
    // Use the override value from the previous NavigationEntry in the
    // NavigationController.
    UA_OVERRIDE_INHERIT,

    // Use the default user agent.
    UA_OVERRIDE_FALSE,

    // Use the user agent override, if it's available.
    UA_OVERRIDE_TRUE

    // Adding new UserAgentOverrideOption? Also update LoadUrlParams.java
    // static constants.
  }
~~~
###  CreateNavigationEntry

 Creates a navigation entry and translates the virtual url to a real one.

 This is a general call; prefer LoadURL[WithParams] below.

 Extra headers are separated by \n.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<NavigationEntry> CreateNavigationEntry(
      const GURL& url,
      Referrer referrer,
      absl::optional<url::Origin> initiator_origin,
      absl::optional<GURL> initiator_base_url,
      ui::PageTransition transition,
      bool is_renderer_initiated,
      const std::string& extra_headers,
      BrowserContext* browser_context,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory);
~~~

### DisablePromptOnRepost

NavigationController::DisablePromptOnRepost
~~~cpp
CONTENT_EXPORT static void DisablePromptOnRepost();
~~~
 Disables checking for a repost and prompting the user. This is used during
 testing.

### ~NavigationController

~NavigationController
~~~cpp
virtual ~NavigationController() {}
~~~

### DeprecatedGetWebContents

DeprecatedGetWebContents
~~~cpp
virtual WebContents* DeprecatedGetWebContents() = 0;
~~~
 Returns the web contents associated with this controller. It can never be
 nullptr.


 TODO(crbug.com/1225205): Remove this. It is a layering violation as it is
 implemented in renderer_host/ which cannot depend on WebContents.

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual BrowserContext* GetBrowserContext() = 0;
~~~
 Get the browser context for this controller. It can never be nullptr.

### Restore

Restore
~~~cpp
virtual void Restore(
      int selected_navigation,
      RestoreType type,
      std::vector<std::unique_ptr<NavigationEntry>>* entries) = 0;
~~~
 Initializes this NavigationController with the given saved navigations,
 using |selected_navigation| as the currently loaded entry. Before this call
 the controller should be unused (there should be no current entry). |type|
 indicates where the restor comes from. This takes ownership of the
 NavigationEntrys in |entries| and clears it out. This is used for session
 restore.

### GetActiveEntry

GetActiveEntry
~~~cpp
virtual NavigationEntry* GetActiveEntry() = 0;
~~~
###  Entries 
-------------------------------------------------------------------
 There are two basic states for entries: pending and committed. When an
 entry is navigated to, a request is sent to the server. While that request
 has not been responded to, the NavigationEntry is pending. Once data is
 received for that entry, that NavigationEntry is committed.

###  Active entry 
--------------------------------------------------------------
 THIS IS DEPRECATED. DO NOT USE. Use GetVisibleEntry instead.

 See http://crbug.com/273710.


 Returns the active entry, which is the pending entry if a navigation is in
 progress or the last committed entry otherwise.

### GetVisibleEntry

GetVisibleEntry
~~~cpp
virtual NavigationEntry* GetVisibleEntry() = 0;
~~~
 Returns the entry that should be displayed to the user in the address bar.

 This is the pending entry if a navigation is in progress *and* is safe to
 display to the user (see below), or the last committed entry otherwise.


 A pending entry is safe to display if it started in the browser process or
 if it's a renderer-initiated navigation in a new tab which hasn't been
 accessed by another tab.  (If it has been accessed, it risks a URL spoof.)
### GetCurrentEntryIndex

GetCurrentEntryIndex
~~~cpp
virtual int GetCurrentEntryIndex() = 0;
~~~
 Returns the index from which we would go back/forward or reload.  This is
 the last_committed_entry_index_ if pending_entry_index_ is -1.  Otherwise,
 it is the pending_entry_index_.

### GetLastCommittedEntry

GetLastCommittedEntry
~~~cpp
virtual NavigationEntry* GetLastCommittedEntry() = 0;
~~~
 Returns the last "committed" entry. Note that even when no navigation has
 actually committed, this will never return null as long as the FrameTree
 associated with the NavigationController is already initialized, as a
 FrameTree will always start with the initial NavigationEntry.

### GetLastCommittedEntryIndex

GetLastCommittedEntryIndex
~~~cpp
virtual int GetLastCommittedEntryIndex() = 0;
~~~
 Returns the index of the last committed entry.

### CanViewSource

CanViewSource
~~~cpp
virtual bool CanViewSource() = 0;
~~~
 Returns true if the source for the current entry can be viewed.

### GetEntryCount

GetEntryCount
~~~cpp
virtual int GetEntryCount() = 0;
~~~
###  Navigation list 
-----------------------------------------------------------
 Returns the number of entries in the NavigationController, excluding
 the pending entry if there is one.

### GetEntryAtIndex

GetEntryAtIndex
~~~cpp
virtual NavigationEntry* GetEntryAtIndex(int index) = 0;
~~~

### GetEntryAtOffset

GetEntryAtOffset
~~~cpp
virtual NavigationEntry* GetEntryAtOffset(int offset) = 0;
~~~
 Returns the entry at the specified offset from current.  Returns nullptr
 if out of bounds.

### DiscardNonCommittedEntries

DiscardNonCommittedEntries
~~~cpp
virtual void DiscardNonCommittedEntries() = 0;
~~~
###  Pending entry 
-------------------------------------------------------------
 Discards the pending entry if any.

### GetPendingEntry

GetPendingEntry
~~~cpp
virtual NavigationEntry* GetPendingEntry() = 0;
~~~
 Returns the pending entry corresponding to the navigation that is
 currently in progress, or null if there is none.

### GetPendingEntryIndex

GetPendingEntryIndex
~~~cpp
virtual int GetPendingEntryIndex() = 0;
~~~
 Returns the index of the pending entry or -1 if the pending entry
 corresponds to a new navigation (created via LoadURL).

### LoadURL

LoadURL
~~~cpp
virtual base::WeakPtr<NavigationHandle> LoadURL(
      const GURL& url,
      const Referrer& referrer,
      ui::PageTransition type,
      const std::string& extra_headers) = 0;
~~~
###  New navigations 
-----------------------------------------------------------
 Loads the specified URL, specifying extra http headers to add to the
 request. Extra headers are separated by \n.


 Returns NavigationHandle for the initiated navigation (might be null if
 the navigation couldn't be started for some reason). WeakPtr is used as if
 the navigation is cancelled before it reaches DidStartNavigation, the
 WebContentsObserver::DidFinishNavigation callback won't be dispatched.

### LoadURLWithParams

LoadURLWithParams
~~~cpp
virtual base::WeakPtr<NavigationHandle> LoadURLWithParams(
      const LoadURLParams& params) = 0;
~~~
 More general version of LoadURL. See comments in LoadURLParams for
 using |params|.

### LoadIfNecessary

LoadIfNecessary
~~~cpp
virtual void LoadIfNecessary() = 0;
~~~
 Loads the current page if this NavigationController was restored from
 history and the current page has not loaded yet or if the load was
 explicitly requested using SetNeedsReload().

### LoadPostCommitErrorPage

LoadPostCommitErrorPage
~~~cpp
virtual base::WeakPtr<NavigationHandle> LoadPostCommitErrorPage(
      RenderFrameHost* render_frame_host,
      const GURL& url,
      const std::string& error_page_html,
      net::Error error) = 0;
~~~
 Navigates directly to an error page in response to an event on the last
 committed page (e.g., triggered by a subresource), with |error_page_html|
 as the contents and |url| as the URL.


 The error page will create a NavigationEntry that temporarily replaces the
 original page's entry. The original entry will be put back into the entry
 list after any other navigation.


 Returns the handle to the navigation for the error page, which may be null
 if the navigation is immediately canceled.

### CanGoBack

CanGoBack
~~~cpp
virtual bool CanGoBack() = 0;
~~~
###  Renavigation 
--------------------------------------------------------------
 Navigation relative to the "current entry"
### CanGoForward

CanGoForward
~~~cpp
virtual bool CanGoForward() = 0;
~~~

### CanGoToOffset

CanGoToOffset
~~~cpp
virtual bool CanGoToOffset(int offset) = 0;
~~~

### GoBack

GoBack
~~~cpp
virtual void GoBack() = 0;
~~~
 `CanGoBack`/`CanGoForward` are preconditions for these respective methods.

### GoForward

GoForward
~~~cpp
virtual void GoForward() = 0;
~~~

### GoToIndex

GoToIndex
~~~cpp
virtual void GoToIndex(int index) = 0;
~~~
 Navigates to the specified absolute index. Should only be used for
 browser-initiated navigations.

### GoToOffset

GoToOffset
~~~cpp
virtual void GoToOffset(int offset) = 0;
~~~
 Navigates to the specified offset from the "current entry". Does nothing if
 the offset is out of bounds.

### Reload

Reload
~~~cpp
virtual void Reload(ReloadType reload_type, bool check_for_repost) = 0;
~~~
 Reloads the current entry under the specified ReloadType.  If
 |check_for_repost| is true and the current entry has POST data the user is
 prompted to see if they really want to reload the page.  In nearly all
 cases pass in true in production code, but would do false for testing, or
 in cases where no user interface is available for prompting.

 NOTE: |reload_type| should never be NONE.

### RemoveEntryAtIndex

RemoveEntryAtIndex
~~~cpp
virtual bool RemoveEntryAtIndex(int index) = 0;
~~~
###  Removing of entries 
-------------------------------------------------------
 Removes the entry at the specified |index|.  If the index is the last
 committed index or the pending entry, this does nothing and returns false.

 Otherwise this call discards any pending entry.

### PruneForwardEntries

PruneForwardEntries
~~~cpp
virtual void PruneForwardEntries() = 0;
~~~
 Discards any pending entry, then discards all entries after the current
 entry index.

### GetSessionStorageNamespaceMap

GetSessionStorageNamespaceMap
~~~cpp
virtual const SessionStorageNamespaceMap& GetSessionStorageNamespaceMap() = 0;
~~~
###  Random 
--------------------------------------------------------------------
 Session storage depends on dom_storage that depends on blink::WebString.

 Returns all the SessionStorageNamespace objects that this
 NavigationController knows about, the map key is a StoragePartition id.

### GetDefaultSessionStorageNamespace

GetDefaultSessionStorageNamespace
~~~cpp
virtual SessionStorageNamespace* GetDefaultSessionStorageNamespace() = 0;
~~~
 TODO(ajwong): Remove this once prerendering, instant, and session restore
 are migrated.

### NeedsReload

NeedsReload
~~~cpp
virtual bool NeedsReload() = 0;
~~~
 Returns true if a reload happens when activated (SetActive(true) is
 invoked). This is true for session/tab restore, cloned tabs and tabs that
 requested a reload (using SetNeedsReload()) after their renderer was
 killed.

### SetNeedsReload

SetNeedsReload
~~~cpp
virtual void SetNeedsReload() = 0;
~~~
 Request a reload to happen when activated. This can be used when a renderer
 backing a background tab is killed by the system on Android or ChromeOS.

### CancelPendingReload

CancelPendingReload
~~~cpp
virtual void CancelPendingReload() = 0;
~~~
 Cancels a repost that brought up a warning.

### ContinuePendingReload

ContinuePendingReload
~~~cpp
virtual void ContinuePendingReload() = 0;
~~~
 Continues a repost that brought up a warning.

### IsInitialNavigation

IsInitialNavigation
~~~cpp
virtual bool IsInitialNavigation() = 0;
~~~
 Returns true if this is a newly created tab or a cloned tab, which has not
 yet committed a real page. Returns false after the initial navigation has
 committed.

### IsInitialBlankNavigation

IsInitialBlankNavigation
~~~cpp
virtual bool IsInitialBlankNavigation() = 0;
~~~
 Returns true if this is a newly created tab (not a clone) that has not yet
 committed a real page.

### NotifyEntryChanged

NotifyEntryChanged
~~~cpp
virtual void NotifyEntryChanged(NavigationEntry* entry) = 0;
~~~
 Broadcasts the NOTIFICATION_NAV_ENTRY_CHANGED notification for the given
 entry. This will keep things in sync like the saved session.

### CopyStateFrom

CopyStateFrom
~~~cpp
virtual void CopyStateFrom(NavigationController* source,
                             bool needs_reload) = 0;
~~~
 Copies the navigation state from the given controller to this one. This one
 should be empty (just created). |needs_reload| indicates whether a reload
 needs to happen when activated. If false, the WebContents remains unloaded
 and is painted as a plain grey rectangle when activated. To force a reload,
 call SetNeedsReload() followed by LoadIfNecessary().

### CopyStateFromAndPrune

CopyStateFromAndPrune
~~~cpp
virtual void CopyStateFromAndPrune(NavigationController* source,
                                     bool replace_entry) = 0;
~~~
 A variant of CopyStateFrom. Removes all entries from this except the last
 committed entry, and inserts all entries from |source| before and including
 its last committed entry. For example:
 source: A B *C* D
 this:   E F *G*
 result: A B C *G*
 If there is a pending entry after *G* in |this|, it is also preserved.

 If |replace_entry| is true, the current entry in |source| is replaced. So
 the result above would be A B *G*.

 This ignores any pending entry in |source|.  Callers must ensure that
 |CanPruneAllButLastCommitted| returns true before calling this, or it will
 crash.

### CanPruneAllButLastCommitted

CanPruneAllButLastCommitted
~~~cpp
virtual bool CanPruneAllButLastCommitted() = 0;
~~~
 Returns whether it is safe to call PruneAllButLastCommitted or
 CopyStateFromAndPrune.  There must be a last committed entry, and if there
 is a pending entry, it must be new and not an existing entry.


 If there were no last committed entry, the pending entry might not commit,
 leaving us with a blank page.  This is unsafe when used with
 |CopyStateFromAndPrune|, which would show an existing entry above the blank
 page.

 If there were an existing pending entry, we could not prune the last
 committed entry, in case it did not commit.  That would leave us with no
 sensible place to put the pending entry when it did commit, after all other
 entries are pruned.  For example, it could be going back several entries.

 (New pending entries are safe, because they can always commit to the end.)
### PruneAllButLastCommitted

PruneAllButLastCommitted
~~~cpp
virtual void PruneAllButLastCommitted() = 0;
~~~
 Removes all the entries except the last committed entry. If there is a new
 pending navigation it is preserved.  Callers must ensure
 |CanPruneAllButLastCommitted| returns true before calling this, or it will
 crash.

### DeleteNavigationEntries

DeleteNavigationEntries
~~~cpp
virtual void DeleteNavigationEntries(
      const DeletionPredicate& deletionPredicate) = 0;
~~~
 Removes all navigation entries matching |deletionPredicate| except the last
 commited entry.

 Callers must ensure |CanPruneAllButLastCommitted| returns true before
 calling this, or it will crash.

### IsEntryMarkedToBeSkipped

IsEntryMarkedToBeSkipped
~~~cpp
virtual bool IsEntryMarkedToBeSkipped(int index) = 0;
~~~
 Returns whether entry at the given index is marked to be skipped on
 back/forward UI. The history manipulation intervention marks entries to be
 skipped in order to intervene against pages that manipulate browser history
 such that the user is not able to use the back button to go to the previous
 page they interacted with.

### GetBackForwardCache

GetBackForwardCache
~~~cpp
virtual BackForwardCache& GetBackForwardCache() = 0;
~~~
 Gets the BackForwardCache for this NavigationController.

### NavigationController

NavigationController
~~~cpp
NavigationController() {}
~~~

### ~NavigationController

~NavigationController
~~~cpp
virtual ~NavigationController() {}
~~~

### DeprecatedGetWebContents

DeprecatedGetWebContents
~~~cpp
virtual WebContents* DeprecatedGetWebContents() = 0;
~~~
 Returns the web contents associated with this controller. It can never be
 nullptr.


 TODO(crbug.com/1225205): Remove this. It is a layering violation as it is
 implemented in renderer_host/ which cannot depend on WebContents.

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual BrowserContext* GetBrowserContext() = 0;
~~~
 Get the browser context for this controller. It can never be nullptr.

### Restore

Restore
~~~cpp
virtual void Restore(
      int selected_navigation,
      RestoreType type,
      std::vector<std::unique_ptr<NavigationEntry>>* entries) = 0;
~~~
 Initializes this NavigationController with the given saved navigations,
 using |selected_navigation| as the currently loaded entry. Before this call
 the controller should be unused (there should be no current entry). |type|
 indicates where the restor comes from. This takes ownership of the
 NavigationEntrys in |entries| and clears it out. This is used for session
 restore.

### GetActiveEntry

GetActiveEntry
~~~cpp
virtual NavigationEntry* GetActiveEntry() = 0;
~~~
###  Entries 
-------------------------------------------------------------------
 There are two basic states for entries: pending and committed. When an
 entry is navigated to, a request is sent to the server. While that request
 has not been responded to, the NavigationEntry is pending. Once data is
 received for that entry, that NavigationEntry is committed.

###  Active entry 
--------------------------------------------------------------
 THIS IS DEPRECATED. DO NOT USE. Use GetVisibleEntry instead.

 See http://crbug.com/273710.


 Returns the active entry, which is the pending entry if a navigation is in
 progress or the last committed entry otherwise.

### GetVisibleEntry

GetVisibleEntry
~~~cpp
virtual NavigationEntry* GetVisibleEntry() = 0;
~~~
 Returns the entry that should be displayed to the user in the address bar.

 This is the pending entry if a navigation is in progress *and* is safe to
 display to the user (see below), or the last committed entry otherwise.


 A pending entry is safe to display if it started in the browser process or
 if it's a renderer-initiated navigation in a new tab which hasn't been
 accessed by another tab.  (If it has been accessed, it risks a URL spoof.)
### GetCurrentEntryIndex

GetCurrentEntryIndex
~~~cpp
virtual int GetCurrentEntryIndex() = 0;
~~~
 Returns the index from which we would go back/forward or reload.  This is
 the last_committed_entry_index_ if pending_entry_index_ is -1.  Otherwise,
 it is the pending_entry_index_.

### GetLastCommittedEntry

GetLastCommittedEntry
~~~cpp
virtual NavigationEntry* GetLastCommittedEntry() = 0;
~~~
 Returns the last "committed" entry. Note that even when no navigation has
 actually committed, this will never return null as long as the FrameTree
 associated with the NavigationController is already initialized, as a
 FrameTree will always start with the initial NavigationEntry.

### GetLastCommittedEntryIndex

GetLastCommittedEntryIndex
~~~cpp
virtual int GetLastCommittedEntryIndex() = 0;
~~~
 Returns the index of the last committed entry.

### CanViewSource

CanViewSource
~~~cpp
virtual bool CanViewSource() = 0;
~~~
 Returns true if the source for the current entry can be viewed.

### GetEntryCount

GetEntryCount
~~~cpp
virtual int GetEntryCount() = 0;
~~~
###  Navigation list 
-----------------------------------------------------------
 Returns the number of entries in the NavigationController, excluding
 the pending entry if there is one.

### GetEntryAtIndex

GetEntryAtIndex
~~~cpp
virtual NavigationEntry* GetEntryAtIndex(int index) = 0;
~~~

### GetEntryAtOffset

GetEntryAtOffset
~~~cpp
virtual NavigationEntry* GetEntryAtOffset(int offset) = 0;
~~~
 Returns the entry at the specified offset from current.  Returns nullptr
 if out of bounds.

### DiscardNonCommittedEntries

DiscardNonCommittedEntries
~~~cpp
virtual void DiscardNonCommittedEntries() = 0;
~~~
###  Pending entry 
-------------------------------------------------------------
 Discards the pending entry if any.

### GetPendingEntry

GetPendingEntry
~~~cpp
virtual NavigationEntry* GetPendingEntry() = 0;
~~~
 Returns the pending entry corresponding to the navigation that is
 currently in progress, or null if there is none.

### GetPendingEntryIndex

GetPendingEntryIndex
~~~cpp
virtual int GetPendingEntryIndex() = 0;
~~~
 Returns the index of the pending entry or -1 if the pending entry
 corresponds to a new navigation (created via LoadURL).

### LoadURL

LoadURL
~~~cpp
virtual base::WeakPtr<NavigationHandle> LoadURL(
      const GURL& url,
      const Referrer& referrer,
      ui::PageTransition type,
      const std::string& extra_headers) = 0;
~~~
###  New navigations 
-----------------------------------------------------------
 Loads the specified URL, specifying extra http headers to add to the
 request. Extra headers are separated by \n.


 Returns NavigationHandle for the initiated navigation (might be null if
 the navigation couldn't be started for some reason). WeakPtr is used as if
 the navigation is cancelled before it reaches DidStartNavigation, the
 WebContentsObserver::DidFinishNavigation callback won't be dispatched.

### LoadURLWithParams

LoadURLWithParams
~~~cpp
virtual base::WeakPtr<NavigationHandle> LoadURLWithParams(
      const LoadURLParams& params) = 0;
~~~
 More general version of LoadURL. See comments in LoadURLParams for
 using |params|.

### LoadIfNecessary

LoadIfNecessary
~~~cpp
virtual void LoadIfNecessary() = 0;
~~~
 Loads the current page if this NavigationController was restored from
 history and the current page has not loaded yet or if the load was
 explicitly requested using SetNeedsReload().

### LoadPostCommitErrorPage

LoadPostCommitErrorPage
~~~cpp
virtual base::WeakPtr<NavigationHandle> LoadPostCommitErrorPage(
      RenderFrameHost* render_frame_host,
      const GURL& url,
      const std::string& error_page_html,
      net::Error error) = 0;
~~~
 Navigates directly to an error page in response to an event on the last
 committed page (e.g., triggered by a subresource), with |error_page_html|
 as the contents and |url| as the URL.


 The error page will create a NavigationEntry that temporarily replaces the
 original page's entry. The original entry will be put back into the entry
 list after any other navigation.


 Returns the handle to the navigation for the error page, which may be null
 if the navigation is immediately canceled.

### CanGoBack

CanGoBack
~~~cpp
virtual bool CanGoBack() = 0;
~~~
###  Renavigation 
--------------------------------------------------------------
 Navigation relative to the "current entry"
### CanGoForward

CanGoForward
~~~cpp
virtual bool CanGoForward() = 0;
~~~

### CanGoToOffset

CanGoToOffset
~~~cpp
virtual bool CanGoToOffset(int offset) = 0;
~~~

### GoBack

GoBack
~~~cpp
virtual void GoBack() = 0;
~~~
 `CanGoBack`/`CanGoForward` are preconditions for these respective methods.

### GoForward

GoForward
~~~cpp
virtual void GoForward() = 0;
~~~

### GoToIndex

GoToIndex
~~~cpp
virtual void GoToIndex(int index) = 0;
~~~
 Navigates to the specified absolute index. Should only be used for
 browser-initiated navigations.

### GoToOffset

GoToOffset
~~~cpp
virtual void GoToOffset(int offset) = 0;
~~~
 Navigates to the specified offset from the "current entry". Does nothing if
 the offset is out of bounds.

### Reload

Reload
~~~cpp
virtual void Reload(ReloadType reload_type, bool check_for_repost) = 0;
~~~
 Reloads the current entry under the specified ReloadType.  If
 |check_for_repost| is true and the current entry has POST data the user is
 prompted to see if they really want to reload the page.  In nearly all
 cases pass in true in production code, but would do false for testing, or
 in cases where no user interface is available for prompting.

 NOTE: |reload_type| should never be NONE.

### RemoveEntryAtIndex

RemoveEntryAtIndex
~~~cpp
virtual bool RemoveEntryAtIndex(int index) = 0;
~~~
###  Removing of entries 
-------------------------------------------------------
 Removes the entry at the specified |index|.  If the index is the last
 committed index or the pending entry, this does nothing and returns false.

 Otherwise this call discards any pending entry.

### PruneForwardEntries

PruneForwardEntries
~~~cpp
virtual void PruneForwardEntries() = 0;
~~~
 Discards any pending entry, then discards all entries after the current
 entry index.

### GetSessionStorageNamespaceMap

GetSessionStorageNamespaceMap
~~~cpp
virtual const SessionStorageNamespaceMap& GetSessionStorageNamespaceMap() = 0;
~~~
###  Random 
--------------------------------------------------------------------
 Session storage depends on dom_storage that depends on blink::WebString.

 Returns all the SessionStorageNamespace objects that this
 NavigationController knows about, the map key is a StoragePartition id.

### GetDefaultSessionStorageNamespace

GetDefaultSessionStorageNamespace
~~~cpp
virtual SessionStorageNamespace* GetDefaultSessionStorageNamespace() = 0;
~~~
 TODO(ajwong): Remove this once prerendering, instant, and session restore
 are migrated.

### NeedsReload

NeedsReload
~~~cpp
virtual bool NeedsReload() = 0;
~~~
 Returns true if a reload happens when activated (SetActive(true) is
 invoked). This is true for session/tab restore, cloned tabs and tabs that
 requested a reload (using SetNeedsReload()) after their renderer was
 killed.

### SetNeedsReload

SetNeedsReload
~~~cpp
virtual void SetNeedsReload() = 0;
~~~
 Request a reload to happen when activated. This can be used when a renderer
 backing a background tab is killed by the system on Android or ChromeOS.

### CancelPendingReload

CancelPendingReload
~~~cpp
virtual void CancelPendingReload() = 0;
~~~
 Cancels a repost that brought up a warning.

### ContinuePendingReload

ContinuePendingReload
~~~cpp
virtual void ContinuePendingReload() = 0;
~~~
 Continues a repost that brought up a warning.

### IsInitialNavigation

IsInitialNavigation
~~~cpp
virtual bool IsInitialNavigation() = 0;
~~~
 Returns true if this is a newly created tab or a cloned tab, which has not
 yet committed a real page. Returns false after the initial navigation has
 committed.

### IsInitialBlankNavigation

IsInitialBlankNavigation
~~~cpp
virtual bool IsInitialBlankNavigation() = 0;
~~~
 Returns true if this is a newly created tab (not a clone) that has not yet
 committed a real page.

### NotifyEntryChanged

NotifyEntryChanged
~~~cpp
virtual void NotifyEntryChanged(NavigationEntry* entry) = 0;
~~~
 Broadcasts the NOTIFICATION_NAV_ENTRY_CHANGED notification for the given
 entry. This will keep things in sync like the saved session.

### CopyStateFrom

CopyStateFrom
~~~cpp
virtual void CopyStateFrom(NavigationController* source,
                             bool needs_reload) = 0;
~~~
 Copies the navigation state from the given controller to this one. This one
 should be empty (just created). |needs_reload| indicates whether a reload
 needs to happen when activated. If false, the WebContents remains unloaded
 and is painted as a plain grey rectangle when activated. To force a reload,
 call SetNeedsReload() followed by LoadIfNecessary().

### CopyStateFromAndPrune

CopyStateFromAndPrune
~~~cpp
virtual void CopyStateFromAndPrune(NavigationController* source,
                                     bool replace_entry) = 0;
~~~
 A variant of CopyStateFrom. Removes all entries from this except the last
 committed entry, and inserts all entries from |source| before and including
 its last committed entry. For example:
 source: A B *C* D
 this:   E F *G*
 result: A B C *G*
 If there is a pending entry after *G* in |this|, it is also preserved.

 If |replace_entry| is true, the current entry in |source| is replaced. So
 the result above would be A B *G*.

 This ignores any pending entry in |source|.  Callers must ensure that
 |CanPruneAllButLastCommitted| returns true before calling this, or it will
 crash.

### CanPruneAllButLastCommitted

CanPruneAllButLastCommitted
~~~cpp
virtual bool CanPruneAllButLastCommitted() = 0;
~~~
 Returns whether it is safe to call PruneAllButLastCommitted or
 CopyStateFromAndPrune.  There must be a last committed entry, and if there
 is a pending entry, it must be new and not an existing entry.


 If there were no last committed entry, the pending entry might not commit,
 leaving us with a blank page.  This is unsafe when used with
 |CopyStateFromAndPrune|, which would show an existing entry above the blank
 page.

 If there were an existing pending entry, we could not prune the last
 committed entry, in case it did not commit.  That would leave us with no
 sensible place to put the pending entry when it did commit, after all other
 entries are pruned.  For example, it could be going back several entries.

 (New pending entries are safe, because they can always commit to the end.)
### PruneAllButLastCommitted

PruneAllButLastCommitted
~~~cpp
virtual void PruneAllButLastCommitted() = 0;
~~~
 Removes all the entries except the last committed entry. If there is a new
 pending navigation it is preserved.  Callers must ensure
 |CanPruneAllButLastCommitted| returns true before calling this, or it will
 crash.

### DeleteNavigationEntries

DeleteNavigationEntries
~~~cpp
virtual void DeleteNavigationEntries(
      const DeletionPredicate& deletionPredicate) = 0;
~~~
 Removes all navigation entries matching |deletionPredicate| except the last
 commited entry.

 Callers must ensure |CanPruneAllButLastCommitted| returns true before
 calling this, or it will crash.

### IsEntryMarkedToBeSkipped

IsEntryMarkedToBeSkipped
~~~cpp
virtual bool IsEntryMarkedToBeSkipped(int index) = 0;
~~~
 Returns whether entry at the given index is marked to be skipped on
 back/forward UI. The history manipulation intervention marks entries to be
 skipped in order to intervene against pages that manipulate browser history
 such that the user is not able to use the back button to go to the previous
 page they interacted with.

### GetBackForwardCache

GetBackForwardCache
~~~cpp
virtual BackForwardCache& GetBackForwardCache() = 0;
~~~
 Gets the BackForwardCache for this NavigationController.

### NavigationController

NavigationController
~~~cpp
NavigationController() {}
~~~

### enum LoadURLType
 Load type used in LoadURLParams.


 A Java counterpart will be generated for this enum.

 GENERATED_JAVA_ENUM_PACKAGE: (
   org.chromium.content_public.browser.navigation_controller)
 GENERATED_JAVA_PREFIX_TO_STRIP: LOAD_TYPE_
~~~cpp
enum LoadURLType {
    // For loads that do not fall into any types below.
    LOAD_TYPE_DEFAULT,

    // An http post load request.  The post data is passed in |post_data|.
    LOAD_TYPE_HTTP_POST,

    // Loads a 'data:' scheme URL with specified base URL and a history entry
    // URL. This is only safe to be used for browser-initiated data: URL
    // navigations, since it shows arbitrary content as if it comes from
    // |virtual_url_for_data_url|.
    LOAD_TYPE_DATA

    // Adding new LoadURLType? Also update LoadUrlParams.java static constants.
  };
~~~
### enum UserAgentOverrideOption
 User agent override type used in LoadURLParams.


 A Java counterpart will be generated for this enum.

 GENERATED_JAVA_ENUM_PACKAGE: (
   org.chromium.content_public.browser.navigation_controller)
 GENERATED_JAVA_PREFIX_TO_STRIP: UA_OVERRIDE_
~~~cpp
enum UserAgentOverrideOption {
    // Use the override value from the previous NavigationEntry in the
    // NavigationController.
    UA_OVERRIDE_INHERIT,

    // Use the default user agent.
    UA_OVERRIDE_FALSE,

    // Use the user agent override, if it's available.
    UA_OVERRIDE_TRUE

    // Adding new UserAgentOverrideOption? Also update LoadUrlParams.java
    // static constants.
  };
~~~
###  CreateNavigationEntry

 Creates a navigation entry and translates the virtual url to a real one.

 This is a general call; prefer LoadURL[WithParams] below.

 Extra headers are separated by \n.

~~~cpp
CONTENT_EXPORT static std::unique_ptr<NavigationEntry> CreateNavigationEntry(
      const GURL& url,
      Referrer referrer,
      absl::optional<url::Origin> initiator_origin,
      absl::optional<GURL> initiator_base_url,
      ui::PageTransition transition,
      bool is_renderer_initiated,
      const std::string& extra_headers,
      BrowserContext* browser_context,
      scoped_refptr<network::SharedURLLoaderFactory> blob_url_loader_factory);
~~~

### DisablePromptOnRepost

NavigationController::DisablePromptOnRepost
~~~cpp
CONTENT_EXPORT static void DisablePromptOnRepost();
~~~
 Disables checking for a repost and prompting the user. This is used during
 testing.
