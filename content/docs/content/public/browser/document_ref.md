### DocumentRef::DocumentRef

DocumentRef::DocumentRef
~~~cpp
inline DocumentRef::DocumentRef(DocumentRef&&) = default;
~~~
 [chromium-style] requires these be out of line, but they are small enough to
 inline the defaults.

### DocumentRef::operator=

DocumentRef::operator=
~~~cpp
inline DocumentRef& DocumentRef::operator=(DocumentRef&&) = default;
~~~

### DocumentRef::DocumentRef

DocumentRef::DocumentRef
~~~cpp
inline DocumentRef::DocumentRef(const DocumentRef&) = default;
~~~

### DocumentRef::operator=

DocumentRef::operator=
~~~cpp
inline DocumentRef& DocumentRef::operator=(const DocumentRef&) = default;
~~~

### DocumentRef::~DocumentRef

DocumentRef::~DocumentRef
~~~cpp
inline DocumentRef::~DocumentRef() = default;
~~~

## class DocumentRef
 A non-nullable, checked reference to a document. This will CHECK if it is
 accessed after the document is no longer valid, because the RenderFrameHost
 is deleted or navigatese to a different document. See also
 document_user_data.h.


 Note that though this is implemented as a base::SafeRef<RenderFrameHost>,
 it is different from an ordinary SafeRef to the RenderFrameHost.


 docs/render_document.md will make these equivalent in the future.


 If the document may become invalid, use a WeakDocumentPtr instead.


 Treat this like you would a base::SafeRef, because that's essentially what it
 is.

### DocumentRef

DocumentRef::DocumentRef
~~~cpp
DocumentRef(DocumentRef&&);
~~~
 Copyable and movable.

###  operator=


~~~cpp
DocumentRef& operator=(DocumentRef&&);
~~~
### DocumentRef

DocumentRef::DocumentRef
~~~cpp
DocumentRef(const DocumentRef&);
~~~

###  operator=


~~~cpp
DocumentRef& operator=(const DocumentRef&);
~~~
### ~DocumentRef

DocumentRef::~DocumentRef
~~~cpp
~DocumentRef();
~~~

### AsRenderFrameHost

AsRenderFrameHost
~~~cpp
RenderFrameHost& AsRenderFrameHost() const { return *safe_document_; }
~~~

### DocumentRef

DocumentRef::DocumentRef
~~~cpp
explicit DocumentRef(base::SafeRef<RenderFrameHost> safe_document);
~~~

### safe_document_



~~~cpp

base::SafeRef<RenderFrameHost> safe_document_;

~~~

 Created from a factory scoped to document, rather than RenderFrameHost,
 lifetime.

### AsRenderFrameHost

AsRenderFrameHost
~~~cpp
RenderFrameHost& AsRenderFrameHost() const { return *safe_document_; }
~~~

###  operator=


~~~cpp
DocumentRef& operator=(DocumentRef&&);
~~~
###  operator=


~~~cpp
DocumentRef& operator=(const DocumentRef&);
~~~
### safe_document_



~~~cpp

base::SafeRef<RenderFrameHost> safe_document_;

~~~

 Created from a factory scoped to document, rather than RenderFrameHost,
 lifetime.
