
## class DevToolsBackgroundServicesContext
 This class is responsible for persisting the debugging events for the
 relevant Web Platform Features.

### DevToolsBackgroundServicesContext

DevToolsBackgroundServicesContext
~~~cpp
DevToolsBackgroundServicesContext(const DevToolsBackgroundServicesContext&) =
      delete;
~~~

### operator=

DevToolsBackgroundServicesContext::operator=
~~~cpp
DevToolsBackgroundServicesContext& operator=(
      const DevToolsBackgroundServicesContext&) = delete;
~~~

### ~DevToolsBackgroundServicesContext

~DevToolsBackgroundServicesContext
~~~cpp
virtual ~DevToolsBackgroundServicesContext() = default;
~~~

### IsRecording

DevToolsBackgroundServicesContext::IsRecording
~~~cpp
virtual bool IsRecording(DevToolsBackgroundService service) = 0;
~~~
 Whether events related to |service| should be recorded.

### LogBackgroundServiceEvent

DevToolsBackgroundServicesContext::LogBackgroundServiceEvent
~~~cpp
virtual void LogBackgroundServiceEvent(
      uint64_t service_worker_registration_id,
      blink::StorageKey storage_key,
      DevToolsBackgroundService service,
      const std::string& event_name,
      const std::string& instance_id,
      const std::map<std::string, std::string>& event_metadata) = 0;
~~~
 Logs the event if recording for |service| is enabled.

 |storage_key| refers to the storage partition the event belongs to.

 |event_name| is a description of the event.

 |instance_id| is for tracking events related to the same feature instance.

 Any additional useful information relating to the feature can be sent via
 |event_metadata|. Called from the UI thread.
