
## class ChildProcessHost : public
 This represents a non-browser process. This can include traditional child
 processes like plugins, or an embedder could even use this for long lived
 processes that run independent of the browser process.

### Create

ChildProcessHost : public::Create
~~~cpp
static std::unique_ptr<ChildProcessHost> Create(
      ChildProcessHostDelegate* delegate,
      IpcMode ipc_mode);
~~~
 Used to create a child process host. The delegate must outlive this object.

### GetChildPath

ChildProcessHost : public::GetChildPath
~~~cpp
static base::FilePath GetChildPath(int flags);
~~~
 Returns the pathname to be used for a child process.  If a subprocess
 pathname was specified on the command line, that will be used.  Otherwise,
 the default child process pathname will be returned.  On most platforms,
 this will be the same as the currently-executing process.


 The |flags| argument accepts one or more flags such as CHILD_ALLOW_SELF.

 Pass only CHILD_NORMAL if none of these special behaviors are required.


 On failure, returns an empty FilePath.

### ForceShutdown

ChildProcessHost : public::ForceShutdown
~~~cpp
virtual void ForceShutdown() = 0;
~~~
 Send the shutdown message to the child process.

### GetMojoInvitation

ChildProcessHost : public::GetMojoInvitation
~~~cpp
virtual absl::optional<mojo::OutgoingInvitation>& GetMojoInvitation() = 0;
~~~
 Exposes the outgoing Mojo invitation for this ChildProcessHost. The
 invitation can be given to ChildProcessLauncher to ensure that this
 ChildProcessHost's primordial Mojo IPC calls can properly communicate with
 the launched process.


 Always valid immediately after ChildProcessHost construction, but may be
 null if someone else has taken ownership.

### CreateChannelMojo

ChildProcessHost : public::CreateChannelMojo
~~~cpp
virtual void CreateChannelMojo() = 0;
~~~
 Creates a legacy IPC channel over a Mojo message pipe. Must be called if
 legacy IPC will be used to communicate with the child process, but
 otherwise should not be called.

### IsChannelOpening

ChildProcessHost : public::IsChannelOpening
~~~cpp
virtual bool IsChannelOpening() = 0;
~~~
 Returns true iff the IPC channel is currently being opened; this means
 CreateChannelMojo() has been called, but OnChannelConnected() has not yet
 been invoked.

### AddFilter

ChildProcessHost : public::AddFilter
~~~cpp
virtual void AddFilter(IPC::MessageFilter* filter) = 0;
~~~
 Adds an IPC message filter.  A reference will be kept to the filter.

### BindReceiver

ChildProcessHost : public::BindReceiver
~~~cpp
virtual void BindReceiver(mojo::GenericPendingReceiver receiver) = 0;
~~~
 Bind an interface exposed by the child process. Whether or not the
 interface in |receiver| can be bound depends on the process type and
 potentially on the Content embedder.


 Receivers passed to this call arrive in the child process and go through
 the following flow, stopping if any step decides to bind the receiver:

   1. IO thread, ChildProcessImpl::BindReceiver.

   2. IO thread, ContentClient::BindChildProcessInterface.

   3. Main thread, ChildThreadImpl::BindReceiver (virtual).

### ReinitializeLogging

ChildProcessHost : public::ReinitializeLogging
~~~cpp
virtual void ReinitializeLogging(uint32_t logging_dest,
                                   base::ScopedFD log_file_descriptor) = 0;
~~~
 Reinitializes the child process's logging with the given settings. This
 is needed on Chrome OS, which switches to a log file in the user's home
 directory once they log in.

### RunServiceDeprecated

ChildProcessHost : public::RunServiceDeprecated
~~~cpp
virtual void RunServiceDeprecated(
      const std::string& service_name,
      mojo::ScopedMessagePipeHandle service_pipe) = 0;
~~~
 Instructs the child process to run an instance of the named service. This
 is DEPRECATED and should never be used.

### DumpProfilingData

ChildProcessHost : public::DumpProfilingData
~~~cpp
virtual void DumpProfilingData(base::OnceClosure callback) = 0;
~~~
 Write out the accumulated code profiling profile to the configured file.

 The callback is invoked once the profile has been flushed to disk.

### SetProfilingFile

ChildProcessHost : public::SetProfilingFile
~~~cpp
virtual void SetProfilingFile(base::File file) = 0;
~~~
 Sets the profiling file for the child process.

 Used for the coverage builds.
