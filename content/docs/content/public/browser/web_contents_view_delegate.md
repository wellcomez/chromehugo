
## class WebContentsViewDelegate
 This interface allows a client to extend the functionality of the
 WebContentsView implementation.

### ~WebContentsViewDelegate

WebContentsViewDelegate::~WebContentsViewDelegate
~~~cpp
~WebContentsViewDelegate()
~~~

### GetNativeWindow

WebContentsViewDelegate::GetNativeWindow
~~~cpp
virtual gfx::NativeWindow GetNativeWindow();
~~~
 Returns the native window containing the WebContents, or nullptr if the
 WebContents is not in any window.

### GetDragDestDelegate

WebContentsViewDelegate::GetDragDestDelegate
~~~cpp
virtual WebDragDestDelegate* GetDragDestDelegate();
~~~
 Returns a delegate to process drags not handled by content.

### ShowContextMenu

WebContentsViewDelegate::ShowContextMenu
~~~cpp
virtual void ShowContextMenu(RenderFrameHost& render_frame_host,
                               const ContextMenuParams& params);
~~~
 Shows a context menu.


 The `render_frame_host` represents the frame that requests the context menu
 (typically this frame is focused, but this is not necessarily the case -
 see https://crbug.com/1257907#c14).

### DismissContextMenu

WebContentsViewDelegate::DismissContextMenu
~~~cpp
virtual void DismissContextMenu();
~~~
 Dismiss the context menu if one exists.

### ExecuteCommandForTesting

WebContentsViewDelegate::ExecuteCommandForTesting
~~~cpp
virtual void ExecuteCommandForTesting(int command_id, int event_flags);
~~~
 Tests can use ExecuteCommandForTesting to simulate executing a context menu
 item (after first opening the context menu using the ShowContextMenu
 method).

### StoreFocus

WebContentsViewDelegate::StoreFocus
~~~cpp
virtual void StoreFocus();
~~~
 Store the current focused view and start tracking it.

### RestoreFocus

WebContentsViewDelegate::RestoreFocus
~~~cpp
virtual bool RestoreFocus();
~~~
 Restore focus to stored view if possible, return true if successful.

### ResetStoredFocus

WebContentsViewDelegate::ResetStoredFocus
~~~cpp
virtual void ResetStoredFocus();
~~~
 Clears any stored focus.

### Focus

WebContentsViewDelegate::Focus
~~~cpp
virtual bool Focus();
~~~
 Allows the delegate to intercept a request to focus the WebContents,
 and focus something else instead. Returns true when intercepted.

### TakeFocus

WebContentsViewDelegate::TakeFocus
~~~cpp
virtual bool TakeFocus(bool reverse);
~~~
 Advance focus to the view that follows or precedes the WebContents.

### CreateRenderWidgetHostViewDelegate

WebContentsViewDelegate::CreateRenderWidgetHostViewDelegate
~~~cpp
virtual NSObject<RenderWidgetHostViewMacDelegate>*
  CreateRenderWidgetHostViewDelegate(RenderWidgetHost* render_widget_host,
                                     bool is_popup);
~~~

### CreateRenderWidgetHostViewDelegate

WebContentsViewDelegate::CreateRenderWidgetHostViewDelegate
~~~cpp
virtual void* CreateRenderWidgetHostViewDelegate(
      RenderWidgetHost* render_widget_host,
      bool is_popup);
~~~

### OnPerformDrop

WebContentsViewDelegate::OnPerformDrop
~~~cpp
virtual void OnPerformDrop(const DropData& drop_data,
                             DropCompletionCallback callback);
~~~
 Performs the actions needed for a drop and then calls the completion
 callback once done.
