### FetchHistogramsAsynchronously

FetchHistogramsAsynchronously
~~~cpp
CONTENT_EXPORT void FetchHistogramsAsynchronously(
    scoped_refptr<base::TaskRunner> task_runner,
    base::OnceClosure callback,
    base::TimeDelta wait_time);
~~~
 Fetch histogram data asynchronously from the various child processes, into
 the browser process. This method is used by the metrics services in
 preparation for a log upload. It contacts all processes, and get them to
 upload to the browser any/all changes to histograms.  When all changes have
 been acquired, or when the wait time expires (whichever is sooner), post the
 callback to the specified TaskRunner. Note the callback is posted exactly
 once.

