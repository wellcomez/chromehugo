
## class ClientHintsControllerDelegate

### GetNetworkQualityTracker

ClientHintsControllerDelegate::GetNetworkQualityTracker
~~~cpp
virtual network::NetworkQualityTracker* GetNetworkQualityTracker() = 0;
~~~

### GetAllowedClientHintsFromSource

ClientHintsControllerDelegate::GetAllowedClientHintsFromSource
~~~cpp
virtual void GetAllowedClientHintsFromSource(
      const url::Origin& origin,
      blink::EnabledClientHints* client_hints) = 0;
~~~
 Get which client hints opt-ins were persisted on current origin.

### IsJavaScriptAllowed

ClientHintsControllerDelegate::IsJavaScriptAllowed
~~~cpp
virtual bool IsJavaScriptAllowed(const GURL& url,
                                   RenderFrameHost* parent_rfh) = 0;
~~~
 Checks is script is enabled. |parent_rfh| is the document embedding the
 frame in which the request is taking place (it is null for outermost main
 frame requests).

### AreThirdPartyCookiesBlocked

ClientHintsControllerDelegate::AreThirdPartyCookiesBlocked
~~~cpp
virtual bool AreThirdPartyCookiesBlocked(const GURL& url,
                                           RenderFrameHost* rfh) = 0;
~~~
 Returns true iff cookies are blocked for the URL/RFH or third-party cookies
 are disabled in the user agent.

### GetUserAgentMetadata

ClientHintsControllerDelegate::GetUserAgentMetadata
~~~cpp
virtual blink::UserAgentMetadata GetUserAgentMetadata() = 0;
~~~

### PersistClientHints

ClientHintsControllerDelegate::PersistClientHints
~~~cpp
virtual void PersistClientHints(
      const url::Origin& primary_origin,
      RenderFrameHost* parent_rfh,
      const std::vector<network::mojom::WebClientHintsType>& client_hints) = 0;
~~~

### ResetForTesting

ResetForTesting
~~~cpp
virtual void ResetForTesting() {}
~~~
 Optionally implemented by implementations used in tests. Clears all hints
 that would have been returned by GetAllowedClientHintsFromSource(),
 regardless of whether they were added via PersistClientHints() or
 SetAdditionalHints().

### SetAdditionalClientHints

ClientHintsControllerDelegate::SetAdditionalClientHints
~~~cpp
virtual void SetAdditionalClientHints(
      const std::vector<network::mojom::WebClientHintsType>&) = 0;
~~~
 Sets additional `hints` that this delegate should add to the
 blink::EnabledClientHints object affected by
 |GetAllowedClientHintsFromSource|. This is for when there are additional
 client hints to be added to a request that are not in storage.

### ClearAdditionalClientHints

ClientHintsControllerDelegate::ClearAdditionalClientHints
~~~cpp
virtual void ClearAdditionalClientHints() = 0;
~~~
 Clears the additional hints set by |SetAdditionalHints|.

### SetMostRecentMainFrameViewportSize

ClientHintsControllerDelegate::SetMostRecentMainFrameViewportSize
~~~cpp
virtual void SetMostRecentMainFrameViewportSize(
      const gfx::Size& viewport_size) = 0;
~~~
 Used to track the visible viewport size. This value is only used when the
 viewport size cannot be directly obtained, such as for prefetch requests
 and for tab restores. The embedder is responsible for calling this when the
 size of the visible viewport changes.

### GetMostRecentMainFrameViewportSize

ClientHintsControllerDelegate::GetMostRecentMainFrameViewportSize
~~~cpp
virtual gfx::Size GetMostRecentMainFrameViewportSize() = 0;
~~~
