### ~UsbDelegate

~UsbDelegate
~~~cpp
virtual ~UsbDelegate() = default;
~~~

### AdjustProtectedInterfaceClasses

AdjustProtectedInterfaceClasses
~~~cpp
virtual void AdjustProtectedInterfaceClasses(
      BrowserContext* browser_context,
      const url::Origin& origin,
      RenderFrameHost* frame,
      std::vector<uint8_t>& classes) = 0;
~~~
 Allows the embedder to modify the set of protected interface classes for
 the given frame. `frame` may be nullptr if the context has no frame.

### RunChooser

RunChooser
~~~cpp
virtual std::unique_ptr<UsbChooser> RunChooser(
      RenderFrameHost& frame,
      std::vector<device::mojom::UsbDeviceFilterPtr> filters,
      blink::mojom::WebUsbService::GetPermissionCallback callback) = 0;
~~~
 Shows a chooser for the user to select a USB device.  |callback| will be
 run when the prompt is closed. Deleting the returned object will cancel the
 prompt. This method should not be called if CanRequestDevicePermission()
 below returned false.

### CanRequestDevicePermission

CanRequestDevicePermission
~~~cpp
virtual bool CanRequestDevicePermission(BrowserContext* browser_context,
                                          const url::Origin& origin) = 0;
~~~
 Returns whether `origin` in `browser_context` has permission to request
 access to a device.

### RevokeDevicePermissionWebInitiated

RevokeDevicePermissionWebInitiated
~~~cpp
virtual void RevokeDevicePermissionWebInitiated(
      BrowserContext* browser_context,
      const url::Origin& origin,
      const device::mojom::UsbDeviceInfo& device) = 0;
~~~
 Attempts to revoke the permission for `origin` in `browser_context` to
 access the USB device described by `device`.

### GetDeviceInfo

GetDeviceInfo
~~~cpp
virtual const device::mojom::UsbDeviceInfo* GetDeviceInfo(
      BrowserContext* browser_context,
      const std::string& guid) = 0;
~~~
 Returns device information for the device with matching `guid`.

### HasDevicePermission

HasDevicePermission
~~~cpp
virtual bool HasDevicePermission(
      BrowserContext* browser_context,
      const url::Origin& origin,
      const device::mojom::UsbDeviceInfo& device) = 0;
~~~
 Returns whether `origin` in `browser_context` has permission to access
 the USB device described by `device`.

### GetDevices

GetDevices
~~~cpp
virtual void GetDevices(
      BrowserContext* browser_context,
      blink::mojom::WebUsbService::GetDevicesCallback callback) = 0;
~~~
 These two methods are expected to proxy to the UsbDeviceManager interface
 owned by the embedder.


 Content and the embedder must use the same connection so that the embedder
 can process connect/disconnect events for permissions management purposes
 before they are delivered to content. Otherwise race conditions are
 possible.

### GetDevice

GetDevice
~~~cpp
virtual void GetDevice(
      BrowserContext* browser_context,
      const std::string& guid,
      base::span<const uint8_t> blocked_interface_classes,
      mojo::PendingReceiver<device::mojom::UsbDevice> device_receiver,
      mojo::PendingRemote<device::mojom::UsbDeviceClient> device_client) = 0;
~~~

### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(BrowserContext* browser_context,
                           Observer* observer) = 0;
~~~
 Functions to manage the set of Observer instances registered to this
 object.

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(BrowserContext* browser_context,
                              Observer* observer) = 0;
~~~

### IsServiceWorkerAllowedForOrigin

IsServiceWorkerAllowedForOrigin
~~~cpp
virtual bool IsServiceWorkerAllowedForOrigin(const url::Origin& origin) = 0;
~~~
 Returns true if `origin` is allowed to access WebUSB from service workers.

## class UsbDelegate
 Interface provided by the content embedder to support the WebUSB API.

### OnDeviceRemoved

UsbDelegate::OnDeviceRemoved
~~~cpp
virtual void OnDeviceRemoved(
        const device::mojom::UsbDeviceInfo& device) = 0;
~~~

### OnDeviceManagerConnectionError

UsbDelegate::OnDeviceManagerConnectionError
~~~cpp
virtual void OnDeviceManagerConnectionError() = 0;
~~~

### OnPermissionRevoked

UsbDelegate::OnPermissionRevoked
~~~cpp
virtual void OnPermissionRevoked(const url::Origin& origin) = 0;
~~~
