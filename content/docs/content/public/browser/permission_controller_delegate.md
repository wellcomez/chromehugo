
## class PermissionControllerDelegate

### ~PermissionControllerDelegate

~PermissionControllerDelegate
~~~cpp
virtual ~PermissionControllerDelegate() = default;
~~~

### RequestPermission

PermissionControllerDelegate::RequestPermission
~~~cpp
virtual void RequestPermission(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host,
      const GURL& requesting_origin,
      bool user_gesture,
      base::OnceCallback<void(blink::mojom::PermissionStatus)> callback) = 0;
~~~
 Requests a permission on behalf of a frame identified by
 render_frame_host.

 When the permission request is handled, whether it failed, timed out or
 succeeded, the |callback| will be run.

### RequestPermissions

PermissionControllerDelegate::RequestPermissions
~~~cpp
virtual void RequestPermissions(
      const std::vector<blink::PermissionType>& permission,
      RenderFrameHost* render_frame_host,
      const GURL& requesting_origin,
      bool user_gesture,
      base::OnceCallback<void(
          const std::vector<blink::mojom::PermissionStatus>&)> callback) = 0;
~~~
 Requests multiple permissions on behalf of a frame identified by
 render_frame_host.

 When the permission request is handled, whether it failed, timed out or
 succeeded, the |callback| will be run. The order of statuses in the
 returned vector will correspond to the order of requested permission
 types.

### RequestPermissionsFromCurrentDocument

PermissionControllerDelegate::RequestPermissionsFromCurrentDocument
~~~cpp
virtual void RequestPermissionsFromCurrentDocument(
      const std::vector<blink::PermissionType>& permissions,
      RenderFrameHost* render_frame_host,
      bool user_gesture,
      base::OnceCallback<void(
          const std::vector<blink::mojom::PermissionStatus>&)> callback) = 0;
~~~
 Requests permissions from the current document in the given
 RenderFrameHost. Use this over `RequestPermission` whenever possible as
 this API takes into account the lifecycle state of a given document (i.e.

 whether it's in back-forward cache or being prerendered) in addition to its
 origin.

### GetPermissionStatus

PermissionControllerDelegate::GetPermissionStatus
~~~cpp
virtual blink::mojom::PermissionStatus GetPermissionStatus(
      blink::PermissionType permission,
      const GURL& requesting_origin,
      const GURL& embedding_origin) = 0;
~~~
 Returns the permission status of a given requesting_origin/embedding_origin
 tuple. This is not taking a RenderFrameHost because the call might happen
 outside of a frame context. Prefer GetPermissionStatusForCurrentDocument
 (below) whenever possible.

### GetPermissionResultForOriginWithoutContext

PermissionControllerDelegate::GetPermissionResultForOriginWithoutContext
~~~cpp
virtual PermissionResult GetPermissionResultForOriginWithoutContext(
      blink::PermissionType permission,
      const url::Origin& origin) = 0;
~~~

### GetPermissionStatusForCurrentDocument

PermissionControllerDelegate::GetPermissionStatusForCurrentDocument
~~~cpp
virtual blink::mojom::PermissionStatus GetPermissionStatusForCurrentDocument(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host) = 0;
~~~
 Returns the permission status for the current document in the given
 RenderFrameHost. Use this over `GetPermissionStatus` whenever possible as
 this API takes into account the lifecycle state of a given document (i.e.

 whether it's in back-forward cache or being prerendered) in addition to its
 origin.

### GetPermissionResultForCurrentDocument

PermissionControllerDelegate::GetPermissionResultForCurrentDocument
~~~cpp
virtual PermissionResult GetPermissionResultForCurrentDocument(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host);
~~~
 The method does the same as `GetPermissionStatusForCurrentDocument` but
 additionally returns a source or reason for the permission status.

### GetPermissionStatusForWorker

PermissionControllerDelegate::GetPermissionStatusForWorker
~~~cpp
virtual blink::mojom::PermissionStatus GetPermissionStatusForWorker(
      blink::PermissionType permission,
      RenderProcessHost* render_process_host,
      const GURL& worker_origin) = 0;
~~~
 Returns the status of the given `permission` for a worker on
 `worker_origin` running in `render_process_host`, also performing
 additional checks such as Permission Policy.  Use this over
 GetPermissionStatus whenever possible.

### GetPermissionStatusForEmbeddedRequester

PermissionControllerDelegate::GetPermissionStatusForEmbeddedRequester
~~~cpp
virtual blink::mojom::PermissionStatus
  GetPermissionStatusForEmbeddedRequester(
      blink::PermissionType permission,
      RenderFrameHost* render_frame_host,
      const url::Origin& requesting_origin) = 0;
~~~
 Returns the permission status for `requesting_origin` in the given
 `RenderFrameHost`. Other APIs interpret `requesting_origin` as the last
 committed origin of the requesting frame. This API takes
 `requesting_origin` as a separate parameter because it does not equal the
 last committed origin of the requesting frame.  It is designed to be used
 only for `TOP_LEVEL_STORAGE_ACCESS`.

### ResetPermission

PermissionControllerDelegate::ResetPermission
~~~cpp
virtual void ResetPermission(blink::PermissionType permission,
                               const GURL& requesting_origin,
                               const GURL& embedding_origin) = 0;
~~~
 Sets the permission back to its default for the requesting_origin/
 embedding_origin tuple.

### SubscribePermissionStatusChange

PermissionControllerDelegate::SubscribePermissionStatusChange
~~~cpp
virtual SubscriptionId SubscribePermissionStatusChange(
      blink::PermissionType permission,
      content::RenderProcessHost* render_process_host,
      content::RenderFrameHost* render_frame_host,
      const GURL& requesting_origin,
      base::RepeatingCallback<void(blink::mojom::PermissionStatus)>
          callback) = 0;
~~~
 Runs the given |callback| whenever the |permission| associated with the
 given |render_frame_host| changes. |render_process_host| should be passed
 instead if the request is from a worker. Returns the ID to be used to
 unsubscribe, which can be `is_null()` if the subscribe was not successful.

 Exactly one of |render_process_host| and |render_frame_host| should be
 set, RenderProcessHost will be inferred from |render_frame_host|.

### UnsubscribePermissionStatusChange

PermissionControllerDelegate::UnsubscribePermissionStatusChange
~~~cpp
virtual void UnsubscribePermissionStatusChange(
      SubscriptionId subscription_id) = 0;
~~~
 Unregisters from permission status change notifications. The
 |subscription_id| must match the value returned by the
 SubscribePermissionStatusChange call. Unsubscribing an already
 unsubscribed |subscription_id| or an `is_null()` ID is a no-op.

### IsPermissionOverridable

PermissionControllerDelegate::IsPermissionOverridable
~~~cpp
virtual bool IsPermissionOverridable(
      blink::PermissionType permission,
      const absl::optional<url::Origin>& origin);
~~~
 Returns whether permission can be overridden.
