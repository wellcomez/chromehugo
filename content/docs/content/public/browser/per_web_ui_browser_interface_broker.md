
## class PerWebUIBrowserInterfaceBroker
 A browser interface broker that only binds the interface specified in
 binder_initializers. It should be owned by the |controller| in the
 constructor.

### PerWebUIBrowserInterfaceBroker

PerWebUIBrowserInterfaceBroker::PerWebUIBrowserInterfaceBroker
~~~cpp
PerWebUIBrowserInterfaceBroker(
      WebUIController& controller,
      const std::vector<BinderInitializer>& binder_initializers);
~~~

### ~PerWebUIBrowserInterfaceBroker

PerWebUIBrowserInterfaceBroker::~PerWebUIBrowserInterfaceBroker
~~~cpp
~PerWebUIBrowserInterfaceBroker() override;
~~~

### GetInterface

PerWebUIBrowserInterfaceBroker::GetInterface
~~~cpp
void GetInterface(mojo::GenericPendingReceiver receiver) override;
~~~

### BindNewPipeAndPassRemote

PerWebUIBrowserInterfaceBroker::BindNewPipeAndPassRemote
~~~cpp
[[nodiscard]] mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker>
  BindNewPipeAndPassRemote();
~~~

### controller_



~~~cpp

const raw_ref<WebUIController> controller_;

~~~


### binder_map_



~~~cpp

WebUIBinderMap binder_map_;

~~~


### receiver_



~~~cpp

mojo::Receiver<blink::mojom::BrowserInterfaceBroker> receiver_{this};

~~~


### GetInterface

PerWebUIBrowserInterfaceBroker::GetInterface
~~~cpp
void GetInterface(mojo::GenericPendingReceiver receiver) override;
~~~

### BindNewPipeAndPassRemote

PerWebUIBrowserInterfaceBroker::BindNewPipeAndPassRemote
~~~cpp
[[nodiscard]] mojo::PendingRemote<blink::mojom::BrowserInterfaceBroker>
  BindNewPipeAndPassRemote();
~~~

### controller_



~~~cpp

const raw_ref<WebUIController> controller_;

~~~


### binder_map_



~~~cpp

WebUIBinderMap binder_map_;

~~~


### receiver_



~~~cpp

mojo::Receiver<blink::mojom::BrowserInterfaceBroker> receiver_{this};

~~~

