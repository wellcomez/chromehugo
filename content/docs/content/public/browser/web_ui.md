### WebUI::GetValue&lt;bool&gt;

WebUI::GetValue&lt;bool&gt;
~~~cpp
inline bool WebUI::GetValue<bool>(const base::Value& value) {
  return value.GetBool();
}
~~~

### WebUI::GetValue&lt;int&gt;

WebUI::GetValue&lt;int&gt;
~~~cpp
inline int WebUI::GetValue<int>(const base::Value& value) {
  return value.GetInt();
}
~~~

### WebUI::GetValue&lt;const std::string&&gt;

WebUI::GetValue&lt;const std::string&&gt;
~~~cpp
inline const std::string& WebUI::GetValue<const std::string&>(
    const base::Value& value) {
  return value.GetString();
}
~~~

### WebUI::GetValue&lt;const base::Value::Dict&&gt;

WebUI::GetValue&lt;const base::Value::Dict&&gt;
~~~cpp
inline const base::Value::Dict& WebUI::GetValue<const base::Value::Dict&>(
    const base::Value& value) {
  return value.GetDict();
}
~~~

### WebUI::GetValue&lt;const base::Value::List&&gt;

WebUI::GetValue&lt;const base::Value::List&&gt;
~~~cpp
inline const base::Value::List& WebUI::GetValue<const base::Value::List&>(
    const base::Value& value) {
  return value.GetList();
}
~~~

## class WebUI
 A WebUI sets up the datasources and message handlers for a given HTML-based
 UI.

### GetJavascriptCall

WebUI::GetJavascriptCall
~~~cpp
static std::u16string GetJavascriptCall(
      base::StringPiece function_name,
      base::span<const base::ValueView> arg_list);
~~~
 Returns JavaScript code that, when executed, calls the function specified
 by |function_name| with the arguments specified in |arg_list|.

### ~WebUI

~WebUI
~~~cpp
virtual ~WebUI() {}
~~~

### GetWebContents

WebUI::GetWebContents
~~~cpp
virtual WebContents* GetWebContents() = 0;
~~~

### GetController

WebUI::GetController
~~~cpp
virtual WebUIController* GetController() = 0;
~~~

### SetController

WebUI::SetController
~~~cpp
virtual void SetController(std::unique_ptr<WebUIController> controller) = 0;
~~~

### GetDeviceScaleFactor

WebUI::GetDeviceScaleFactor
~~~cpp
virtual float GetDeviceScaleFactor() = 0;
~~~
 Returns the device scale factor of the monitor that the renderer is on.

 Whenever possible, WebUI should push resources with this scale factor to
 Javascript.

### GetOverriddenTitle

WebUI::GetOverriddenTitle
~~~cpp
virtual const std::u16string& GetOverriddenTitle() = 0;
~~~
 Gets a custom tab title provided by the Web UI. If there is no title
 override, the string will be empty which should trigger the default title
 behavior for the tab.

### OverrideTitle

WebUI::OverrideTitle
~~~cpp
virtual void OverrideTitle(const std::u16string& title) = 0;
~~~

### GetBindings

WebUI::GetBindings
~~~cpp
virtual int GetBindings() = 0;
~~~
 Allows a controller to override the BindingsPolicy that should be enabled
 for this page.

### SetBindings

WebUI::SetBindings
~~~cpp
virtual void SetBindings(int bindings) = 0;
~~~

### GetRequestableSchemes

WebUI::GetRequestableSchemes
~~~cpp
virtual const std::vector<std::string>& GetRequestableSchemes() = 0;
~~~
 Allows a scheme to be requested which is provided by the WebUIController.

### AddRequestableScheme

WebUI::AddRequestableScheme
~~~cpp
virtual void AddRequestableScheme(const char* scheme) = 0;
~~~

### AddMessageHandler

WebUI::AddMessageHandler
~~~cpp
virtual void AddMessageHandler(
      std::unique_ptr<WebUIMessageHandler> handler) = 0;
~~~

### RegisterMessageCallback

WebUI::RegisterMessageCallback
~~~cpp
virtual void RegisterMessageCallback(base::StringPiece message,
                                       MessageCallback callback) = 0;
~~~

### RegisterHandlerCallback

RegisterHandlerCallback
~~~cpp
void RegisterHandlerCallback(
      base::StringPiece message,
      base::RepeatingCallback<void(Args...)> callback) {
    RegisterMessageCallback(
        message, base::BindRepeating(
                     &Call<std::index_sequence_for<Args...>, Args...>::Impl,
                     callback, message));
  }
~~~

### ProcessWebUIMessage

WebUI::ProcessWebUIMessage
~~~cpp
virtual void ProcessWebUIMessage(const GURL& source_url,
                                   const std::string& message,
                                   base::Value::List args) = 0;
~~~
 This is only needed if an embedder overrides handling of a WebUIMessage and
 then later wants to undo that, or to route it to a different WebUI object.

### CanCallJavascript

WebUI::CanCallJavascript
~~~cpp
virtual bool CanCallJavascript() = 0;
~~~
 Returns true if this WebUI can currently call JavaScript.

### CallJavascriptFunctionUnsafe

WebUI::CallJavascriptFunctionUnsafe
~~~cpp
virtual void CallJavascriptFunctionUnsafe(
      base::StringPiece function_name) = 0;
~~~
 Calling these functions directly is discouraged. It's generally preferred
 to call WebUIMessageHandler::CallJavascriptFunction, as that has
 lifecycle controls to prevent calling JavaScript before the page is ready.


 Call a Javascript function by sending its name and arguments down to
 the renderer.  This is asynchronous; there's no way to get the result
 of the call, and should be thought of more like sending a message to
 the page.


 All function names in WebUI must consist of only ASCII characters.

 There are variants for calls with more arguments.

### CallJavascriptFunctionUnsafe

WebUI::CallJavascriptFunctionUnsafe
~~~cpp
virtual void CallJavascriptFunctionUnsafe(
      base::StringPiece function_name,
      base::span<const base::ValueView> args) = 0;
~~~

### CallJavascriptFunctionUnsafe

CallJavascriptFunctionUnsafe
~~~cpp
void CallJavascriptFunctionUnsafe(base::StringPiece function_name,
                                    const base::ValueView arg1,
                                    const Args&... arg) {
    base::ValueView args[] = {arg1, arg...};
    CallJavascriptFunctionUnsafe(function_name, args);
  }
~~~

### GetHandlersForTesting

WebUI::GetHandlersForTesting
~~~cpp
virtual std::vector<std::unique_ptr<WebUIMessageHandler>>*
  GetHandlersForTesting() = 0;
~~~
 Allows mutable access to this WebUI's message handlers for testing.

### GetValue

WebUI::GetValue
~~~cpp
private:
  template <typename T>
  static T GetValue(const base::Value& value);
~~~
