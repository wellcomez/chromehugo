
## class RenderWidgetHost::InputEventObserver
 Observer for WebInputEvents.

### ~InputEventObserver

~InputEventObserver
~~~cpp
virtual ~InputEventObserver() {}
~~~

### OnInputEvent

OnInputEvent
~~~cpp
virtual void OnInputEvent(const blink::WebInputEvent&) {}
~~~

### OnInputEventAck

OnInputEventAck
~~~cpp
virtual void OnInputEventAck(blink::mojom::InputEventResultSource source,
                                 blink::mojom::InputEventResultState state,
                                 const blink::WebInputEvent&) {}
~~~

### OnImeTextCommittedEvent

OnImeTextCommittedEvent
~~~cpp
virtual void OnImeTextCommittedEvent(const std::u16string& text_str) {}
~~~
 Not all key events are triggered through InputEvent on Android.

 InputEvents are only triggered when user typed in through number bar on
 Android keyboard. This function is triggered when text is committed in
 input form.

### OnImeSetComposingTextEvent

OnImeSetComposingTextEvent
~~~cpp
virtual void OnImeSetComposingTextEvent(const std::u16string& text_str) {}
~~~
 This function is triggered when composing text is updated. Note that
 text_str contains all text that is currently under composition rather
 than updated text only.

### OnImeFinishComposingTextEvent

OnImeFinishComposingTextEvent
~~~cpp
virtual void OnImeFinishComposingTextEvent() {}
~~~
 This function is triggered when composing text is filled into the input
 form.

### ~InputEventObserver

~InputEventObserver
~~~cpp
virtual ~InputEventObserver() {}
~~~

### OnInputEvent

OnInputEvent
~~~cpp
virtual void OnInputEvent(const blink::WebInputEvent&) {}
~~~

### OnInputEventAck

OnInputEventAck
~~~cpp
virtual void OnInputEventAck(blink::mojom::InputEventResultSource source,
                                 blink::mojom::InputEventResultState state,
                                 const blink::WebInputEvent&) {}
~~~

## class RenderWidgetHost
 A RenderWidgetHost acts as the abstraction for compositing and input
 functionality. It can exist in 3 different scenarios:

 1. Popups, which are spawned in situations like <select> menus or
    HTML calendar widgets. These are browser-implemented widgets that
    are created and owned by WebContents in response to a renderer
    request. Since they are divorced from the web page (they are not
    clipped to the bounds of the page), they are an independent
    compositing and input target. As they are owned by WebContents,
    they are also destroyed by WebContents.


 2. Main frames, which are a root frame of a WebContents. These frames
    are separated from the browser UI for compositing and input, as the
    renderer lives in its own coordinate space. These are attached to
    the lifetime of the main frame (currently, owned by the
    RenderViewHost, though that should change one day as per
    https://crbug.com/419087).


 3. Child local root frames, which are iframes isolated from their
    parent frame for security or performance purposes. This allows
    them to be placed in an arbitrary process relative to their
    parent frame. Since they are isolated from the parent, they live
    in their own coordinate space and are an independent unit of
    compositing and input. These are attached to the lifetime of
    the local root frame, and are explicitly owned by the
    RenderFrameHost.


 A RenderWidgetHost is platform-agnostic. It defers platform-specific
 behaviour to its RenderWidgetHostView, which ties the compositing
 output into the native browser UI. Child local root frames also have
 a separate "platform" RenderWidgetHostView type at this time, though
 it stretches the abstraction uncomfortably.


 The RenderWidgetHostView has a complex and somewhat broken lifetime as
 of this writing (see, e.g. https://crbug.com/1161585). It is eagerly
 created along with the RenderWidgetHost on the first creation, before
 the renderer process may exist. It is destroyed if the renderer process
 exits, and not recreated at that time. Then it is recreated lazily when
 the associated renderer frame/widget is recreated. 
### GetRenderWidgetHosts

RenderWidgetHost::GetRenderWidgetHosts
~~~cpp
static std::unique_ptr<RenderWidgetHostIterator> GetRenderWidgetHosts();
~~~
 Returns an iterator over the global list of active RenderWidgetHosts.

### ~RenderWidgetHost

~RenderWidgetHost
~~~cpp
virtual ~RenderWidgetHost() {}
~~~

### GetFrameSinkId

RenderWidgetHost::GetFrameSinkId
~~~cpp
virtual const viz::FrameSinkId& GetFrameSinkId() = 0;
~~~
 Returns the viz::FrameSinkId that this object uses to put things on screen.

 This value is constant throughout the lifetime of this object. Note that
 until a RenderWidgetHostView is created, initialized, and assigned to this
 object, viz may not be aware of this FrameSinkId.

### UpdateTextDirection

RenderWidgetHost::UpdateTextDirection
~~~cpp
virtual void UpdateTextDirection(base::i18n::TextDirection direction) = 0;
~~~
 Update the text direction of the focused input element and notify it to a
 renderer process.

 These functions have two usage scenarios: changing the text direction
 from a menu (as Safari does), and; changing the text direction when a user
 presses a set of keys (as IE and Firefox do).

 1. Change the text direction from a menu.

 In this scenario, we receive a menu event only once and we should update
 the text direction immediately when a user chooses a menu item. So, we
 should call both functions at once as listed in the following snippet.

   void RenderViewHost::SetTextDirection(
       base::i18n::TextDirection direction) {
     UpdateTextDirection(direction);
     NotifyTextDirection();
   }
 2. Change the text direction when pressing a set of keys.

 Because of auto-repeat, we may receive the same key-press event many
 times while we presses the keys and it is nonsense to send the same IPC
 message every time when we receive a key-press event.

 To suppress the number of IPC messages, we just update the text direction
 when receiving a key-press event and send an IPC message when we release
 the keys as listed in the following snippet.

   if (key_event.type == WebKeyboardEvent::KEY_DOWN) {
     if (key_event.windows_key_code == 'A' &&
         key_event.modifiers == WebKeyboardEvent::CTRL_KEY) {
       UpdateTextDirection(dir);
     } else {
       CancelUpdateTextDirection();
     }
   } else if (key_event.type == WebKeyboardEvent::KEY_UP) {
     NotifyTextDirection();
   }
 Once we cancel updating the text direction, we have to ignore all
 succeeding UpdateTextDirection() requests until calling
 NotifyTextDirection(). (We may receive keydown events even after we
 canceled updating the text direction because of auto-repeat.)
 Note: we cannot undo this change for compatibility with Firefox and IE.

### NotifyTextDirection

RenderWidgetHost::NotifyTextDirection
~~~cpp
virtual void NotifyTextDirection() = 0;
~~~

### Focus

RenderWidgetHost::Focus
~~~cpp
virtual void Focus() = 0;
~~~

### Blur

RenderWidgetHost::Blur
~~~cpp
virtual void Blur() = 0;
~~~

### FlushForTesting

RenderWidgetHost::FlushForTesting
~~~cpp
virtual void FlushForTesting() = 0;
~~~
 Tests may need to flush IPCs to ensure deterministic behavior.

### SetActive

RenderWidgetHost::SetActive
~~~cpp
virtual void SetActive(bool active) = 0;
~~~
 Sets whether the renderer should show controls in an active state.  On all
 platforms except mac, that's the same as focused. On mac, the frontmost
 window will show active controls even if the focus is not in the web
 contents, but e.g. in the omnibox.

### ForwardMouseEvent

RenderWidgetHost::ForwardMouseEvent
~~~cpp
virtual void ForwardMouseEvent(
      const blink::WebMouseEvent& mouse_event) = 0;
~~~
 Forwards the given message to the renderer. These are called by
 the view when it has received a message.

### ForwardWheelEvent

RenderWidgetHost::ForwardWheelEvent
~~~cpp
virtual void ForwardWheelEvent(
      const blink::WebMouseWheelEvent& wheel_event) = 0;
~~~

### ForwardKeyboardEvent

RenderWidgetHost::ForwardKeyboardEvent
~~~cpp
virtual void ForwardKeyboardEvent(
      const NativeWebKeyboardEvent& key_event) = 0;
~~~

### ForwardKeyboardEventWithLatencyInfo

RenderWidgetHost::ForwardKeyboardEventWithLatencyInfo
~~~cpp
virtual void ForwardKeyboardEventWithLatencyInfo(
      const NativeWebKeyboardEvent& key_event,
      const ui::LatencyInfo& latency_info) = 0;
~~~

### ForwardGestureEvent

RenderWidgetHost::ForwardGestureEvent
~~~cpp
virtual void ForwardGestureEvent(
      const blink::WebGestureEvent& gesture_event) = 0;
~~~

### GetProcess

RenderWidgetHost::GetProcess
~~~cpp
virtual RenderProcessHost* GetProcess() = 0;
~~~

### GetRoutingID

RenderWidgetHost::GetRoutingID
~~~cpp
virtual int GetRoutingID() = 0;
~~~

### GetView

RenderWidgetHost::GetView
~~~cpp
virtual RenderWidgetHostView* GetView() = 0;
~~~
 Gets the View of this RenderWidgetHost. Can be nullptr, e.g. if the
 RenderWidget is being destroyed or the render process crashed. You should
 never cache this pointer since it can become nullptr if the renderer
 crashes, instead you should always ask for it using the accessor.

### IsCurrentlyUnresponsive

RenderWidgetHost::IsCurrentlyUnresponsive
~~~cpp
virtual bool IsCurrentlyUnresponsive() = 0;
~~~
 Returns true if the renderer is considered unresponsive.

### SynchronizeVisualProperties

RenderWidgetHost::SynchronizeVisualProperties
~~~cpp
virtual bool SynchronizeVisualProperties() = 0;
~~~
 Called to propagate updated visual properties to the renderer. Returns
 true if visual properties have changed since last call.

### AddKeyPressEventCallback

RenderWidgetHost::AddKeyPressEventCallback
~~~cpp
virtual void AddKeyPressEventCallback(
      const KeyPressEventCallback& callback) = 0;
~~~

### RemoveKeyPressEventCallback

RenderWidgetHost::RemoveKeyPressEventCallback
~~~cpp
virtual void RemoveKeyPressEventCallback(
      const KeyPressEventCallback& callback) = 0;
~~~

### AddMouseEventCallback

RenderWidgetHost::AddMouseEventCallback
~~~cpp
virtual void AddMouseEventCallback(const MouseEventCallback& callback) = 0;
~~~

### RemoveMouseEventCallback

RenderWidgetHost::RemoveMouseEventCallback
~~~cpp
virtual void RemoveMouseEventCallback(const MouseEventCallback& callback) = 0;
~~~

### AddSuppressShowingImeCallback

RenderWidgetHost::AddSuppressShowingImeCallback
~~~cpp
virtual void AddSuppressShowingImeCallback(
      const SuppressShowingImeCallback& callback) = 0;
~~~

### RemoveSuppressShowingImeCallback

RenderWidgetHost::RemoveSuppressShowingImeCallback
~~~cpp
virtual void RemoveSuppressShowingImeCallback(
      const SuppressShowingImeCallback& callback) = 0;
~~~

### AddInputEventObserver

RenderWidgetHost::AddInputEventObserver
~~~cpp
virtual void AddInputEventObserver(InputEventObserver* observer) = 0;
~~~
 Add/remove an input event observer.

### RemoveInputEventObserver

RenderWidgetHost::RemoveInputEventObserver
~~~cpp
virtual void RemoveInputEventObserver(InputEventObserver* observer) = 0;
~~~

### AddImeInputEventObserver

RenderWidgetHost::AddImeInputEventObserver
~~~cpp
virtual void AddImeInputEventObserver(InputEventObserver* observer) = 0;
~~~
 Add/remove an Ime input event observer.

### RemoveImeInputEventObserver

RenderWidgetHost::RemoveImeInputEventObserver
~~~cpp
virtual void RemoveImeInputEventObserver(InputEventObserver* observer) = 0;
~~~

### AddObserver

RenderWidgetHost::AddObserver
~~~cpp
virtual void AddObserver(RenderWidgetHostObserver* observer) = 0;
~~~
 Add and remove observers for widget host events. The order in which
 notifications are sent to observers is undefined. Observers must be sure to
 remove the observer before they go away.

### RemoveObserver

RenderWidgetHost::RemoveObserver
~~~cpp
virtual void RemoveObserver(RenderWidgetHostObserver* observer) = 0;
~~~

### GetScreenInfo

RenderWidgetHost::GetScreenInfo
~~~cpp
virtual display::ScreenInfo GetScreenInfo() const = 0;
~~~
 Get info regarding the screen showing this RenderWidgetHost.

### GetScreenInfos

RenderWidgetHost::GetScreenInfos
~~~cpp
virtual display::ScreenInfos GetScreenInfos() const = 0;
~~~
 Get info regarding all screens, including which screen is currently showing
 this RenderWidgetHost.

### GetDeviceScaleFactor

RenderWidgetHost::GetDeviceScaleFactor
~~~cpp
virtual float GetDeviceScaleFactor() = 0;
~~~
 This must always return the same device scale factor as GetScreenInfo.

### GetAllowedTouchAction

RenderWidgetHost::GetAllowedTouchAction
~~~cpp
virtual absl::optional<cc::TouchAction> GetAllowedTouchAction() = 0;
~~~
 Get the allowed touch action corresponding to this RenderWidgetHost.

### WriteIntoTrace

RenderWidgetHost::WriteIntoTrace
~~~cpp
virtual void WriteIntoTrace(perfetto::TracedValue context) = 0;
~~~
 Write a representation of this object into a trace.

### DragTargetDragEnter

DragTargetDragEnter
~~~cpp
virtual void DragTargetDragEnter(const DropData& drop_data,
                                   const gfx::PointF& client_pt,
                                   const gfx::PointF& screen_pt,
                                   blink::DragOperationsMask operations_allowed,
                                   int key_modifiers,
                                   DragOperationCallback callback) {}
~~~
 Drag-and-drop drop target messages that get sent to Blink.

### DragTargetDragEnterWithMetaData

DragTargetDragEnterWithMetaData
~~~cpp
virtual void DragTargetDragEnterWithMetaData(
      const std::vector<DropData::Metadata>& metadata,
      const gfx::PointF& client_pt,
      const gfx::PointF& screen_pt,
      blink::DragOperationsMask operations_allowed,
      int key_modifiers,
      DragOperationCallback callback) {}
~~~

### DragTargetDragOver

DragTargetDragOver
~~~cpp
virtual void DragTargetDragOver(const gfx::PointF& client_pt,
                                  const gfx::PointF& screen_pt,
                                  blink::DragOperationsMask operations_allowed,
                                  int key_modifiers,
                                  DragOperationCallback callback) {}
~~~

### DragTargetDragLeave

DragTargetDragLeave
~~~cpp
virtual void DragTargetDragLeave(const gfx::PointF& client_point,
                                   const gfx::PointF& screen_point) {}
~~~

### DragTargetDrop

DragTargetDrop
~~~cpp
virtual void DragTargetDrop(const DropData& drop_data,
                              const gfx::PointF& client_pt,
                              const gfx::PointF& screen_pt,
                              int key_modifiers,
                              base::OnceClosure callback) {}
~~~

### DragSourceEndedAt

DragSourceEndedAt
~~~cpp
virtual void DragSourceEndedAt(const gfx::PointF& client_pt,
                                 const gfx::PointF& screen_pt,
                                 ui::mojom::DragOperation operation,
                                 base::OnceClosure callback) {}
~~~
 Notifies the renderer that a drag operation that it started has ended,
 either in a drop or by being cancelled.

### DragSourceSystemDragEnded

DragSourceSystemDragEnded
~~~cpp
virtual void DragSourceSystemDragEnded() {}
~~~
 Notifies the renderer that we're done with the drag and drop operation.

 This allows the renderer to reset some state.

### FilterDropData

FilterDropData
~~~cpp
virtual void FilterDropData(DropData* drop_data) {}
~~~
 Filters drop data before it is passed to RenderWidgetHost.

### SetCursor

SetCursor
~~~cpp
virtual void SetCursor(const ui::Cursor& cursor) {}
~~~
 Sets cursor to a specified one when it is over this widget.

### ShowContextMenuAtPoint

ShowContextMenuAtPoint
~~~cpp
virtual void ShowContextMenuAtPoint(const gfx::Point& point,
                                      const ui::MenuSourceType source_type) {}
~~~
 Shows the context menu using the specified point as anchor point.

### InsertVisualStateCallback

InsertVisualStateCallback
~~~cpp
virtual void InsertVisualStateCallback(VisualStateCallback callback) {}
~~~
