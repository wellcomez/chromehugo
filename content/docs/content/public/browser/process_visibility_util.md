### OnBrowserVisibilityChanged

OnBrowserVisibilityChanged
~~~cpp
CONTENT_EXPORT void OnBrowserVisibilityChanged(bool visible);
~~~
 Called by the browser when the browser process becomes visible or
 invisible.

