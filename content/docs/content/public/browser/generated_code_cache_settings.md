
## class GeneratedCodeCacheSettings

### GeneratedCodeCacheSettings

GeneratedCodeCacheSettings
~~~cpp
GeneratedCodeCacheSettings(bool enabled, int size, base::FilePath path)
      : enabled_(enabled), size_in_bytes_(size), path_(path) {
    DCHECK_GE(size_in_bytes_, 0);
  }
~~~

### enabled

enabled
~~~cpp
bool enabled() const { return enabled_; }
~~~
 Specifies if code caching is enanled. If it is disabled, the generated
 code will not be cached.

### size_in_bytes

size_in_bytes
~~~cpp
int64_t size_in_bytes() const { return size_in_bytes_; }
~~~
 Specifies the size of the code cache. If this value is 0, then the
 disk_cache chooses the size of the cache based on some heuristics.

### path

path
~~~cpp
base::FilePath path() const { return path_; }
~~~
 Specifies the path of the directory where the generated code be cached
 on the disk.

### enabled_



~~~cpp

const bool enabled_;

~~~


### size_in_bytes_



~~~cpp

const int64_t size_in_bytes_;

~~~


### path_



~~~cpp

const base::FilePath path_;

~~~


### GeneratedCodeCacheSettings

GeneratedCodeCacheSettings
~~~cpp
GeneratedCodeCacheSettings(bool enabled, int size, base::FilePath path)
      : enabled_(enabled), size_in_bytes_(size), path_(path) {
    DCHECK_GE(size_in_bytes_, 0);
  }
~~~

### enabled

enabled
~~~cpp
bool enabled() const { return enabled_; }
~~~
 Specifies if code caching is enanled. If it is disabled, the generated
 code will not be cached.

### size_in_bytes

size_in_bytes
~~~cpp
int64_t size_in_bytes() const { return size_in_bytes_; }
~~~
 Specifies the size of the code cache. If this value is 0, then the
 disk_cache chooses the size of the cache based on some heuristics.

### path

path
~~~cpp
base::FilePath path() const { return path_; }
~~~
 Specifies the path of the directory where the generated code be cached
 on the disk.

### enabled_



~~~cpp

const bool enabled_;

~~~


### size_in_bytes_



~~~cpp

const int64_t size_in_bytes_;

~~~


### path_



~~~cpp

const base::FilePath path_;

~~~

