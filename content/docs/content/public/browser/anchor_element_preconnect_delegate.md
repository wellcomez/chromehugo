
## class AnchorElementPreconnectDelegate
 Interface for preloading preconnets that are selected based on heuristic.

 TODO(isaboori): It is preferred to migrate the preconnect logic to content/
 and use NetworkContext::PreconnectSockets directly. Since, preloading
 preconnects should respect user settings regarding preloading, this migration
 also requires exposing prefetch::IsSomePreloadingEnabled to content/ as well.

### MaybePreconnect

AnchorElementPreconnectDelegate::MaybePreconnect
~~~cpp
virtual void MaybePreconnect(const GURL& target) = 0;
~~~
 Preconnects to the given URL `target`.
