
## class BrowserPluginGuestManager
 A BrowserPluginGuestManager offloads guest management and routing
 operations outside of the content layer.

### ForEachUnattachedGuest

ForEachUnattachedGuest
~~~cpp
virtual void ForEachUnattachedGuest(
      WebContents* owner_web_contents,
      base::RepeatingCallback<void(WebContents*)> callback) {}
~~~
 Iterates over guest WebContents that belong to a given
 |owner_web_contents|, but have not yet been attached.

### ForEachGuest

BrowserPluginGuestManager::ForEachGuest
~~~cpp
virtual bool ForEachGuest(WebContents* owner_web_contents,
                            const GuestCallback& callback);
~~~

### GetFullPageGuest

BrowserPluginGuestManager::GetFullPageGuest
~~~cpp
virtual WebContents* GetFullPageGuest(WebContents* embedder_web_contents);
~~~
 Returns the "full page" guest if there is one. That is, if there is a
 single BrowserPlugin in the given embedder which takes up the full page,
 then it is returned.
