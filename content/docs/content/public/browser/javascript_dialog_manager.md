
## class JavaScriptDialogManager
 An interface consisting of methods that can be called to produce and manage
 JavaScript dialogs.

### RunJavaScriptDialog

JavaScriptDialogManager::RunJavaScriptDialog
~~~cpp
virtual void RunJavaScriptDialog(WebContents* web_contents,
                                   RenderFrameHost* render_frame_host,
                                   JavaScriptDialogType dialog_type,
                                   const std::u16string& message_text,
                                   const std::u16string& default_prompt_text,
                                   DialogClosedCallback callback,
                                   bool* did_suppress_message) = 0;
~~~
 Displays a JavaScript dialog. |did_suppress_message| will not be nil; if
 |true| is returned in it, the caller will handle faking the reply.

### RunBeforeUnloadDialog

JavaScriptDialogManager::RunBeforeUnloadDialog
~~~cpp
virtual void RunBeforeUnloadDialog(WebContents* web_contents,
                                     RenderFrameHost* render_frame_host,
                                     bool is_reload,
                                     DialogClosedCallback callback) = 0;
~~~
 Displays a dialog asking the user if they want to leave a page.

### HandleJavaScriptDialog

JavaScriptDialogManager::HandleJavaScriptDialog
~~~cpp
virtual bool HandleJavaScriptDialog(WebContents* web_contents,
                                      bool accept,
                                      const std::u16string* prompt_override);
~~~
 Accepts or dismisses the active JavaScript dialog, which must be owned
 by the given |web_contents|. If |prompt_override| is not null, the prompt
 text of the dialog should be set before accepting. Returns true if the
 dialog was handled.

### CancelDialogs

JavaScriptDialogManager::CancelDialogs
~~~cpp
virtual void CancelDialogs(WebContents* web_contents,
                             bool reset_state) = 0;
~~~
 Cancels all active and pending dialogs for the given WebContents. If
 |reset_state| is true, resets any saved state tied to |web_contents|.

### ~JavaScriptDialogManager

~JavaScriptDialogManager
~~~cpp
virtual ~JavaScriptDialogManager() {}
~~~
