
## class MediaSession
 MediaSession manages the media session and audio focus for a given
 WebContents. There is only one MediaSession per WebContents.


 MediaSession allows clients to observe its changes via MediaSessionObserver,
 and allows clients to resume/suspend/stop the managed players.

### ~MediaSession

~MediaSession
~~~cpp
~MediaSession() override = default;
~~~

### Get

MediaSession::Get
~~~cpp
CONTENT_EXPORT static MediaSession* Get(WebContents* contents);
~~~
 Returns the MediaSession associated to this WebContents. Creates one if
 none is currently available.

###  GetSourceId

 Returns the source identity for the given BrowserContext.

~~~cpp
CONTENT_EXPORT static const base::UnguessableToken& GetSourceId(
      BrowserContext* browser_context);
~~~
### GetWebContentsFromRequestId

MediaSession::GetWebContentsFromRequestId
~~~cpp
CONTENT_EXPORT static WebContents* GetWebContentsFromRequestId(
      const base::UnguessableToken& request_id);
~~~

### GetWebContentsFromRequestId

MediaSession::GetWebContentsFromRequestId
~~~cpp
CONTENT_EXPORT static WebContents* GetWebContentsFromRequestId(
      const std::string& request_id);
~~~
 Media item IDs have a shared namespace including both UnguessableTokens and
 strings.

 TODO(https://crbug.com/1260385): Use UnguessableToken only and remove this
 API.

###  GetRequestIdFromWebContents


~~~cpp
CONTENT_EXPORT static const base::UnguessableToken&
  GetRequestIdFromWebContents(WebContents* web_contents);
~~~
### DidReceiveAction

DidReceiveAction
~~~cpp
virtual void DidReceiveAction(
      media_session::mojom::MediaSessionAction action) = 0;
~~~
 Tell the media session a user action has performed.

### SetDuckingVolumeMultiplier

SetDuckingVolumeMultiplier
~~~cpp
virtual void SetDuckingVolumeMultiplier(double multiplier) = 0;
~~~
 Set the volume multiplier applied during ducking.

### SetAudioFocusGroupId

SetAudioFocusGroupId
~~~cpp
virtual void SetAudioFocusGroupId(const base::UnguessableToken& group_id) = 0;
~~~
 Set the audio focus group id for this media session. Sessions in the same
 group can share audio focus. Setting this to null will use the browser
 default value. This will only have any effect if audio focus grouping is
 supported.

### Suspend

Suspend
~~~cpp
void Suspend(SuspendType suspend_type) override = 0;
~~~
###  media_session.mojom.MediaSession overrides 
-------------------------------
 Suspend the media session.

 |type| represents the origin of the request.

### Resume

Resume
~~~cpp
void Resume(SuspendType suspend_type) override = 0;
~~~
 Resume the media session.

 |type| represents the origin of the request.

### StartDucking

StartDucking
~~~cpp
void StartDucking() override = 0;
~~~
 Let the media session start ducking such that the volume multiplier is
 reduced.

### StopDucking

StopDucking
~~~cpp
void StopDucking() override = 0;
~~~
 Let the media session stop ducking such that the volume multiplier is
 recovered.

### GetMediaSessionInfo

GetMediaSessionInfo
~~~cpp
void GetMediaSessionInfo(GetMediaSessionInfoCallback callback) override = 0;
~~~
 Returns information about the MediaSession.

### GetDebugInfo

GetDebugInfo
~~~cpp
void GetDebugInfo(GetDebugInfoCallback callback) override = 0;
~~~
 Returns debug information about the MediaSession.

### AddObserver

AddObserver
~~~cpp
void AddObserver(
      mojo::PendingRemote<media_session::mojom::MediaSessionObserver> observer)
      override = 0;
~~~
 Adds an observer to listen to events related to this MediaSession.

### PreviousTrack

PreviousTrack
~~~cpp
void PreviousTrack() override = 0;
~~~
 Skip to the previous track. If there is no previous track then this will be
 a no-op.

### NextTrack

NextTrack
~~~cpp
void NextTrack() override = 0;
~~~
 Skip to the next track. If there is no next track then this will be a
 no-op.

### SkipAd

SkipAd
~~~cpp
void SkipAd() override = 0;
~~~
 Skip ad.

### Seek

Seek
~~~cpp
void Seek(base::TimeDelta seek_time) override = 0;
~~~
 Seek the media session from the current position. If the media cannot
 seek then this will be a no-op. The |seek_time| is the time delta that
 the media will seek by and supports both positive and negative values.

 This value cannot be zero. The |kDefaultSeekTimeSeconds| provides a
 default value for seeking by a few seconds.

### Stop

Stop
~~~cpp
void Stop(SuspendType suspend_type) override = 0;
~~~
 Stop the media session.

 |type| represents the origin of the request.

### GetMediaImageBitmap

GetMediaImageBitmap
~~~cpp
void GetMediaImageBitmap(const media_session::MediaImage& image,
                           int minimum_size_px,
                           int desired_size_px,
                           GetMediaImageBitmapCallback callback) override = 0;
~~~
 Downloads the bitmap version of a MediaImage at least |minimum_size_px|
 and closest to |desired_size_px|. If the download failed, was too small or
 the image did not come from the media session then returns a null image.

### SeekTo

SeekTo
~~~cpp
void SeekTo(base::TimeDelta seek_time) override = 0;
~~~
 Seek the media session to a non-negative |seek_time| from the beginning of
 the current playing media. If the media cannot seek then this will be a
 no-op.

### ScrubTo

ScrubTo
~~~cpp
void ScrubTo(base::TimeDelta seek_time) override = 0;
~~~
 Scrub ("fast seek") the media session to a non-negative |seek_time| from
 the beginning of the current playing media. If the media cannot scrub then
 this will be a no-op. The client should call |SeekTo| to finish the
 scrubbing operation.

### EnterPictureInPicture

EnterPictureInPicture
~~~cpp
void EnterPictureInPicture() override = 0;
~~~
 Enter picture-in-picture.

### ExitPictureInPicture

ExitPictureInPicture
~~~cpp
void ExitPictureInPicture() override = 0;
~~~
 Exit picture-in-picture.

### MediaSession

MediaSession
~~~cpp
MediaSession() = default;
~~~

### ~MediaSession

~MediaSession
~~~cpp
~MediaSession() override = default;
~~~

### DidReceiveAction

DidReceiveAction
~~~cpp
virtual void DidReceiveAction(
      media_session::mojom::MediaSessionAction action) = 0;
~~~
 Tell the media session a user action has performed.

### SetDuckingVolumeMultiplier

SetDuckingVolumeMultiplier
~~~cpp
virtual void SetDuckingVolumeMultiplier(double multiplier) = 0;
~~~
 Set the volume multiplier applied during ducking.

### SetAudioFocusGroupId

SetAudioFocusGroupId
~~~cpp
virtual void SetAudioFocusGroupId(const base::UnguessableToken& group_id) = 0;
~~~
 Set the audio focus group id for this media session. Sessions in the same
 group can share audio focus. Setting this to null will use the browser
 default value. This will only have any effect if audio focus grouping is
 supported.

### Suspend

Suspend
~~~cpp
void Suspend(SuspendType suspend_type) override = 0;
~~~
###  media_session.mojom.MediaSession overrides 
-------------------------------
 Suspend the media session.

 |type| represents the origin of the request.

### Resume

Resume
~~~cpp
void Resume(SuspendType suspend_type) override = 0;
~~~
 Resume the media session.

 |type| represents the origin of the request.

### StartDucking

StartDucking
~~~cpp
void StartDucking() override = 0;
~~~
 Let the media session start ducking such that the volume multiplier is
 reduced.

### StopDucking

StopDucking
~~~cpp
void StopDucking() override = 0;
~~~
 Let the media session stop ducking such that the volume multiplier is
 recovered.

### GetMediaSessionInfo

GetMediaSessionInfo
~~~cpp
void GetMediaSessionInfo(GetMediaSessionInfoCallback callback) override = 0;
~~~
 Returns information about the MediaSession.

### GetDebugInfo

GetDebugInfo
~~~cpp
void GetDebugInfo(GetDebugInfoCallback callback) override = 0;
~~~
 Returns debug information about the MediaSession.

### AddObserver

AddObserver
~~~cpp
void AddObserver(
      mojo::PendingRemote<media_session::mojom::MediaSessionObserver> observer)
      override = 0;
~~~
 Adds an observer to listen to events related to this MediaSession.

### PreviousTrack

PreviousTrack
~~~cpp
void PreviousTrack() override = 0;
~~~
 Skip to the previous track. If there is no previous track then this will be
 a no-op.

### NextTrack

NextTrack
~~~cpp
void NextTrack() override = 0;
~~~
 Skip to the next track. If there is no next track then this will be a
 no-op.

### SkipAd

SkipAd
~~~cpp
void SkipAd() override = 0;
~~~
 Skip ad.

### Seek

Seek
~~~cpp
void Seek(base::TimeDelta seek_time) override = 0;
~~~
 Seek the media session from the current position. If the media cannot
 seek then this will be a no-op. The |seek_time| is the time delta that
 the media will seek by and supports both positive and negative values.

 This value cannot be zero. The |kDefaultSeekTimeSeconds| provides a
 default value for seeking by a few seconds.

### Stop

Stop
~~~cpp
void Stop(SuspendType suspend_type) override = 0;
~~~
 Stop the media session.

 |type| represents the origin of the request.

### GetMediaImageBitmap

GetMediaImageBitmap
~~~cpp
void GetMediaImageBitmap(const media_session::MediaImage& image,
                           int minimum_size_px,
                           int desired_size_px,
                           GetMediaImageBitmapCallback callback) override = 0;
~~~
 Downloads the bitmap version of a MediaImage at least |minimum_size_px|
 and closest to |desired_size_px|. If the download failed, was too small or
 the image did not come from the media session then returns a null image.

### SeekTo

SeekTo
~~~cpp
void SeekTo(base::TimeDelta seek_time) override = 0;
~~~
 Seek the media session to a non-negative |seek_time| from the beginning of
 the current playing media. If the media cannot seek then this will be a
 no-op.

### ScrubTo

ScrubTo
~~~cpp
void ScrubTo(base::TimeDelta seek_time) override = 0;
~~~
 Scrub ("fast seek") the media session to a non-negative |seek_time| from
 the beginning of the current playing media. If the media cannot scrub then
 this will be a no-op. The client should call |SeekTo| to finish the
 scrubbing operation.

### EnterPictureInPicture

EnterPictureInPicture
~~~cpp
void EnterPictureInPicture() override = 0;
~~~
 Enter picture-in-picture.

### ExitPictureInPicture

ExitPictureInPicture
~~~cpp
void ExitPictureInPicture() override = 0;
~~~
 Exit picture-in-picture.

### MediaSession

MediaSession
~~~cpp
MediaSession() = default;
~~~

### Get

MediaSession::Get
~~~cpp
CONTENT_EXPORT static MediaSession* Get(WebContents* contents);
~~~
 Returns the MediaSession associated to this WebContents. Creates one if
 none is currently available.

###  GetSourceId

 Returns the source identity for the given BrowserContext.

~~~cpp
CONTENT_EXPORT static const base::UnguessableToken& GetSourceId(
      BrowserContext* browser_context);
~~~
### GetWebContentsFromRequestId

MediaSession::GetWebContentsFromRequestId
~~~cpp
CONTENT_EXPORT static WebContents* GetWebContentsFromRequestId(
      const base::UnguessableToken& request_id);
~~~

### GetWebContentsFromRequestId

MediaSession::GetWebContentsFromRequestId
~~~cpp
CONTENT_EXPORT static WebContents* GetWebContentsFromRequestId(
      const std::string& request_id);
~~~
 Media item IDs have a shared namespace including both UnguessableTokens and
 strings.

 TODO(https://crbug.com/1260385): Use UnguessableToken only and remove this
 API.

###  GetRequestIdFromWebContents


~~~cpp
CONTENT_EXPORT static const base::UnguessableToken&
  GetRequestIdFromWebContents(WebContents* web_contents);
~~~