### AddObserver

AddObserver
~~~cpp
virtual void AddObserver(Observer* observer) = 0;
~~~

### RemoveObserver

RemoveObserver
~~~cpp
virtual void RemoveObserver(Observer* observer) = 0;
~~~

## class BrowserXRRuntime
 This interface allows observing the state of the XR service for a particular
 runtime.  In particular, observers may currently know when the browser
 considers a WebContents presenting to an immersive headset.  Implementers of
 this interface will be called on the main browser thread.  Currently this is
 used on Windows to drive overlays.

### WebXRCameraInUseChanged

WebXRCameraInUseChanged
~~~cpp
virtual void WebXRCameraInUseChanged(WebContents* web_contents,
                                         bool in_use) {}
~~~

### SetDefaultXrViews

SetDefaultXrViews
~~~cpp
virtual void SetDefaultXrViews(
        const std::vector<device::mojom::XRViewPtr>& views) {}
~~~
