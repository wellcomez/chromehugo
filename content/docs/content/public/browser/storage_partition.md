
## class StoragePartition::DataRemovalObserver
 Observer interface that is notified of specific data clearing events which
 which were facilitated by the StoragePartition. Notification occurs on the
 UI thread, observer life time is not managed by the StoragePartition.

### OnStorageKeyDataCleared

OnStorageKeyDataCleared
~~~cpp
virtual void OnStorageKeyDataCleared(
        uint32_t remove_mask,
        StorageKeyMatcherFunction storage_key_policy_matcher,
        const base::Time begin,
        const base::Time end) = 0;
~~~
 Called on a deletion event for storage keyed storage APIs.

### OnStorageKeyDataCleared

OnStorageKeyDataCleared
~~~cpp
virtual void OnStorageKeyDataCleared(
        uint32_t remove_mask,
        StorageKeyMatcherFunction storage_key_policy_matcher,
        const base::Time begin,
        const base::Time end) = 0;
~~~
 Called on a deletion event for storage keyed storage APIs.

## class StoragePartition
 Defines what persistent state a child process can access.


 The StoragePartition defines the view each child process has of the
 persistent state inside the BrowserContext. This is used to implement
 isolated storage where a renderer with isolated storage cannot see
 the cookies, localStorage, etc., that normal web renderers have access to.

### GetPath

StoragePartition::GetPath
~~~cpp
virtual base::FilePath GetPath() = 0;
~~~

### GetNetworkContext

StoragePartition::GetNetworkContext
~~~cpp
virtual network::mojom::NetworkContext* GetNetworkContext() = 0;
~~~
 Returns a raw mojom::NetworkContext pointer. When network service crashes
 or restarts, the raw pointer will not be valid or safe to use. Therefore,
 caller should not hold onto this pointer beyond the same message loop task.

### GetSharedStorageManager

StoragePartition::GetSharedStorageManager
~~~cpp
virtual storage::SharedStorageManager* GetSharedStorageManager() = 0;
~~~
 Returns the SharedStorageManager for the StoragePartition, or nullptr if it
 doesn't exist because the feature is disabled.

### GetURLLoaderFactoryForBrowserProcess

StoragePartition::GetURLLoaderFactoryForBrowserProcess
~~~cpp
virtual scoped_refptr<network::SharedURLLoaderFactory>
  GetURLLoaderFactoryForBrowserProcess() = 0;
~~~
 Returns a pointer/info to a URLLoaderFactory/CookieManager owned by
 the storage partition. Prefer to use this instead of creating a new
 URLLoaderFactory when issuing requests from the Browser process, to
 share resources and preserve ordering.

 The returned info from |GetURLLoaderFactoryForBrowserProcessIOThread()|
 must be consumed on the IO thread to get the actual factory, and is safe to
 use after StoragePartition has gone.

 The returned SharedURLLoaderFactory can be held on and will work across
 network process restarts.


 SECURITY NOTE: This browser-process factory relaxes many security features
 (e.g. may disable CORB, won't set |request_initiator_origin_lock| or
 IsolationInfo, etc.).  Network requests that may be initiated or influenced
 by a web origin should typically use a different factory (e.g.  the one
 from RenderFrameHost::CreateNetworkServiceDefaultFactory).

### GetURLLoaderFactoryForBrowserProcessIOThread

StoragePartition::GetURLLoaderFactoryForBrowserProcessIOThread
~~~cpp
virtual std::unique_ptr<network::PendingSharedURLLoaderFactory>
  GetURLLoaderFactoryForBrowserProcessIOThread() = 0;
~~~

### GetCookieManagerForBrowserProcess

StoragePartition::GetCookieManagerForBrowserProcess
~~~cpp
virtual network::mojom::CookieManager*
  GetCookieManagerForBrowserProcess() = 0;
~~~

### CreateTrustTokenQueryAnswerer

StoragePartition::CreateTrustTokenQueryAnswerer
~~~cpp
virtual void CreateTrustTokenQueryAnswerer(
      mojo::PendingReceiver<network::mojom::TrustTokenQueryAnswerer> receiver,
      const url::Origin& top_frame_origin) = 0;
~~~

### CreateURLLoaderNetworkObserverForFrame

StoragePartition::CreateURLLoaderNetworkObserverForFrame
~~~cpp
virtual mojo::PendingRemote<network::mojom::URLLoaderNetworkServiceObserver>
  CreateURLLoaderNetworkObserverForFrame(int process_id,
                                         int frame_routing_id) = 0;
~~~

### CreateURLLoaderNetworkObserverForNavigationRequest

StoragePartition::CreateURLLoaderNetworkObserverForNavigationRequest
~~~cpp
virtual mojo::PendingRemote<network::mojom::URLLoaderNetworkServiceObserver>
  CreateURLLoaderNetworkObserverForNavigationRequest(
      NavigationRequest& navigation_request) = 0;
~~~

### GetQuotaManager

StoragePartition::GetQuotaManager
~~~cpp
virtual storage::QuotaManager* GetQuotaManager() = 0;
~~~

### GetBackgroundSyncContext

StoragePartition::GetBackgroundSyncContext
~~~cpp
virtual BackgroundSyncContext* GetBackgroundSyncContext() = 0;
~~~

### GetFileSystemContext

StoragePartition::GetFileSystemContext
~~~cpp
virtual storage::FileSystemContext* GetFileSystemContext() = 0;
~~~

### GetDatabaseTracker

StoragePartition::GetDatabaseTracker
~~~cpp
virtual storage::DatabaseTracker* GetDatabaseTracker() = 0;
~~~

### GetDOMStorageContext

StoragePartition::GetDOMStorageContext
~~~cpp
virtual DOMStorageContext* GetDOMStorageContext() = 0;
~~~

### GetLocalStorageControl

StoragePartition::GetLocalStorageControl
~~~cpp
virtual storage::mojom::LocalStorageControl* GetLocalStorageControl() = 0;
~~~

### GetIndexedDBControl

StoragePartition::GetIndexedDBControl
~~~cpp
virtual storage::mojom::IndexedDBControl& GetIndexedDBControl() = 0;
~~~

### GetFileSystemAccessEntryFactory

StoragePartition::GetFileSystemAccessEntryFactory
~~~cpp
virtual FileSystemAccessEntryFactory* GetFileSystemAccessEntryFactory() = 0;
~~~

### GetServiceWorkerContext

StoragePartition::GetServiceWorkerContext
~~~cpp
virtual ServiceWorkerContext* GetServiceWorkerContext() = 0;
~~~

### GetDedicatedWorkerService

StoragePartition::GetDedicatedWorkerService
~~~cpp
virtual DedicatedWorkerService* GetDedicatedWorkerService() = 0;
~~~

### GetSharedWorkerService

StoragePartition::GetSharedWorkerService
~~~cpp
virtual SharedWorkerService* GetSharedWorkerService() = 0;
~~~

### GetCacheStorageControl

StoragePartition::GetCacheStorageControl
~~~cpp
virtual storage::mojom::CacheStorageControl* GetCacheStorageControl() = 0;
~~~

### GetGeneratedCodeCacheContext

StoragePartition::GetGeneratedCodeCacheContext
~~~cpp
virtual GeneratedCodeCacheContext* GetGeneratedCodeCacheContext() = 0;
~~~

### GetDevToolsBackgroundServicesContext

StoragePartition::GetDevToolsBackgroundServicesContext
~~~cpp
virtual DevToolsBackgroundServicesContext*
  GetDevToolsBackgroundServicesContext() = 0;
~~~

### GetContentIndexContext

StoragePartition::GetContentIndexContext
~~~cpp
virtual ContentIndexContext* GetContentIndexContext() = 0;
~~~

### GetHostZoomMap

StoragePartition::GetHostZoomMap
~~~cpp
virtual HostZoomMap* GetHostZoomMap() = 0;
~~~

### GetHostZoomLevelContext

StoragePartition::GetHostZoomLevelContext
~~~cpp
virtual HostZoomLevelContext* GetHostZoomLevelContext() = 0;
~~~

### GetZoomLevelDelegate

StoragePartition::GetZoomLevelDelegate
~~~cpp
virtual ZoomLevelDelegate* GetZoomLevelDelegate() = 0;
~~~

### GetPlatformNotificationContext

StoragePartition::GetPlatformNotificationContext
~~~cpp
virtual PlatformNotificationContext* GetPlatformNotificationContext() = 0;
~~~

### GetInterestGroupManager

StoragePartition::GetInterestGroupManager
~~~cpp
virtual InterestGroupManager* GetInterestGroupManager() = 0;
~~~

### GetBrowsingTopicsSiteDataManager

StoragePartition::GetBrowsingTopicsSiteDataManager
~~~cpp
virtual BrowsingTopicsSiteDataManager* GetBrowsingTopicsSiteDataManager() = 0;
~~~

### GetAttributionDataModel

StoragePartition::GetAttributionDataModel
~~~cpp
virtual AttributionDataModel* GetAttributionDataModel() = 0;
~~~

### GetProtoDatabaseProvider

StoragePartition::GetProtoDatabaseProvider
~~~cpp
virtual leveldb_proto::ProtoDatabaseProvider* GetProtoDatabaseProvider() = 0;
~~~

### SetProtoDatabaseProvider

StoragePartition::SetProtoDatabaseProvider
~~~cpp
virtual void SetProtoDatabaseProvider(
      std::unique_ptr<leveldb_proto::ProtoDatabaseProvider>
          optional_proto_db_provider) = 0;
~~~
 Must be set before the first call to GetProtoDatabaseProvider(), or a new
 one will be created by get.

### ClearDataForOrigin

StoragePartition::ClearDataForOrigin
~~~cpp
virtual void ClearDataForOrigin(uint32_t remove_mask,
                                  uint32_t quota_storage_remove_mask,
                                  const GURL& storage_origin,
                                  base::OnceClosure callback) = 0;
~~~
 Starts an asynchronous task that does a best-effort clear the data
 corresponding to the given |remove_mask| and |quota_storage_remove_mask|
 inside this StoragePartition for the given |storage_origin|.

 |callback| is called when data deletion is done or at least the deletion is
 scheduled.

 Note session dom storage is not cleared even if you specify
 REMOVE_DATA_MASK_LOCAL_STORAGE.

 No notification is dispatched upon completion.


 TODO(ajwong): Right now, the embedder may have some
 URLRequestContextGetter objects that the StoragePartition does not know
 about.  This will no longer be the case when we resolve
 http://crbug.com/159193. Remove |request_context_getter| when that bug
 is fixed.

### ClearDataForBuckets

StoragePartition::ClearDataForBuckets
~~~cpp
virtual void ClearDataForBuckets(const blink::StorageKey& storage_key,
                                   const std::set<std::string>& storage_buckets,
                                   base::OnceClosure callback) = 0;
~~~
 Starts a task that will clear the data of each bucket name for the
 specified storage key.

### ClearData

StoragePartition::ClearData
~~~cpp
virtual void ClearData(uint32_t remove_mask,
                         uint32_t quota_storage_remove_mask,
                         const blink::StorageKey& storage_key,
                         const base::Time begin,
                         const base::Time end,
                         base::OnceClosure callback) = 0;
~~~
 Similar to ClearDataForOrigin().

 Deletes all data out for the StoragePartition if |storage_key|'s origin is
 opaque. |callback| is called when data deletion is done or at least the
 deletion is scheduled.

### ClearData

StoragePartition::ClearData
~~~cpp
virtual void ClearData(
      uint32_t remove_mask,
      uint32_t quota_storage_remove_mask,
      BrowsingDataFilterBuilder* filter_builder,
      StorageKeyPolicyMatcherFunction storage_key_policy_matcher,
      network::mojom::CookieDeletionFilterPtr cookie_deletion_filter,
      bool perform_storage_cleanup,
      const base::Time begin,
      const base::Time end,
      base::OnceClosure callback) = 0;
~~~
 Similar to ClearData().

 Deletes all data out for the StoragePartition.

 * `filter_builder` is present if origin/domain filters are to be handled,
   otherwise should be nullptr.

 * `storage_key_policy_matcher` is present if special storage policy is to
   be handled, otherwise the callback should be null.

   The StorageKey matcher does not apply to cookies, instead use:
 * `cookie_deletion_filter` identifies the cookies to delete and will be
   used if `remove_mask` has the REMOVE_DATA_MASK_COOKIES bit set. Note:
   CookieDeletionFilterPtr also contains a time interval
   (created_after_time/created_before_time), so when deleting cookies
   `begin` and `end` will be used ignoring the interval in
   `cookie_deletion_filter`.

   If `perform_storage_cleanup` is true, the storage will try to remove
   traces about deleted data from disk. This is an expensive operation that
   should only be performed if we are sure that almost all data will be
   deleted anyway.

 * `callback` is called when data deletion is done or at least the deletion
   is scheduled.

 Note: Make sure you know what you are doing before clearing cookies
 selectively. You don't want to break the web.

### ClearCodeCaches

StoragePartition::ClearCodeCaches
~~~cpp
virtual void ClearCodeCaches(
      base::Time begin,
      base::Time end,
      const base::RepeatingCallback<bool(const GURL&)>& url_matcher,
      base::OnceClosure callback) = 0;
~~~
 Clears code caches associated with this StoragePartition.

 If |begin| and |end| are not null, only entries with
 timestamps inbetween are deleted. If |url_matcher| is not null, only
 entries with URLs for which the |url_matcher| returns true are deleted.

### Flush

StoragePartition::Flush
~~~cpp
virtual void Flush() = 0;
~~~
 Write any unwritten data to disk.

 Note: this method does not sync the data - it only ensures that any
 unwritten data has been written out to the filesystem.

### ResetURLLoaderFactories

StoragePartition::ResetURLLoaderFactories
~~~cpp
virtual void ResetURLLoaderFactories() = 0;
~~~
 Resets all URLLoaderFactories bound to this partition's network context.

### AddObserver

StoragePartition::AddObserver
~~~cpp
virtual void AddObserver(DataRemovalObserver* observer) = 0;
~~~

### RemoveObserver

StoragePartition::RemoveObserver
~~~cpp
virtual void RemoveObserver(DataRemovalObserver* observer) = 0;
~~~

### ClearBluetoothAllowedDevicesMapForTesting

StoragePartition::ClearBluetoothAllowedDevicesMapForTesting
~~~cpp
virtual void ClearBluetoothAllowedDevicesMapForTesting() = 0;
~~~
 Clear the bluetooth allowed devices map. For test use only.

### FlushNetworkInterfaceForTesting

StoragePartition::FlushNetworkInterfaceForTesting
~~~cpp
virtual void FlushNetworkInterfaceForTesting() = 0;
~~~
 Call |FlushForTesting()| on Network Service related interfaces. For test
 use only.

### WaitForDeletionTasksForTesting

StoragePartition::WaitForDeletionTasksForTesting
~~~cpp
virtual void WaitForDeletionTasksForTesting() = 0;
~~~
 Wait until all deletions tasks are finished. For test use only.

### WaitForCodeCacheShutdownForTesting

StoragePartition::WaitForCodeCacheShutdownForTesting
~~~cpp
virtual void WaitForCodeCacheShutdownForTesting() = 0;
~~~
 Wait until code cache's shutdown is complete. For test use only.

### SetNetworkContextForTesting

StoragePartition::SetNetworkContextForTesting
~~~cpp
virtual void SetNetworkContextForTesting(
      mojo::PendingRemote<network::mojom::NetworkContext>
          network_context_remote) = 0;
~~~

### GetProtoDatabaseProviderForTesting

StoragePartition::GetProtoDatabaseProviderForTesting
~~~cpp
virtual leveldb_proto::ProtoDatabaseProvider*
  GetProtoDatabaseProviderForTesting() = 0;
~~~
 Returns the same provider as GetProtoDatabaseProvider() but doesn't create
 a new instance and returns nullptr instead.

### ResetAttributionManagerForTesting

StoragePartition::ResetAttributionManagerForTesting
~~~cpp
virtual void ResetAttributionManagerForTesting(
      base::OnceCallback<void(bool success)> callback) = 0;
~~~
 Resets all state associated with the Attribution Reporting API for use in
 hermetic tests.

### SetDefaultQuotaSettingsForTesting

StoragePartition::SetDefaultQuotaSettingsForTesting
~~~cpp
static void SetDefaultQuotaSettingsForTesting(
      const storage::QuotaSettings* settings);
~~~
 The value pointed to by |settings| should remain valid until the
 the function is called again with a new value or a nullptr.
