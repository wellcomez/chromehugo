
## struct NativeWebKeyboardEvent : public
 Owns a platform specific event; used to pass own and pass event through
 platform independent code.

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(blink::WebInputEvent::Type type,
                         int modifiers,
                         base::TimeTicks timestamp)
~~~

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(const blink::WebKeyboardEvent& web_event,
                         gfx::NativeView native_view)
~~~

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(gfx::NativeEvent native_event)
~~~

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(
      JNIEnv* env,
      const base::android::JavaRef<jobject>& android_key_event,
      blink::WebInputEvent::Type type,
      int modifiers,
      base::TimeTicks timestamp,
      int keycode,
      int scancode,
      int unicode_character,
      bool is_system_key)
~~~

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(const ui::KeyEvent& key_event)
~~~

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(const ui::KeyEvent& key_event, char16_t character)
~~~

### NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::NativeWebKeyboardEvent
~~~cpp
NativeWebKeyboardEvent(const NativeWebKeyboardEvent& event)
~~~

### ~NativeWebKeyboardEvent

NativeWebKeyboardEvent : public::~NativeWebKeyboardEvent
~~~cpp
~NativeWebKeyboardEvent() override
~~~

### operator=

NativeWebKeyboardEvent : public::operator=
~~~cpp
NativeWebKeyboardEvent& operator=(const NativeWebKeyboardEvent& event);
~~~
