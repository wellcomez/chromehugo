
## struct PermissionResult

### PermissionResult

PermissionResult::PermissionResult
~~~cpp
PermissionResult(blink::mojom::PermissionStatus status,
                   PermissionStatusSource source)
~~~

### ~PermissionResult

PermissionResult::~PermissionResult
~~~cpp
~PermissionResult()
~~~
