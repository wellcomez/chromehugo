
## class MessagePortProvider
 An interface consisting of methods that can be called to use Message ports.

### MessagePortProvider

MessagePortProvider
~~~cpp
MessagePortProvider(const MessagePortProvider&) = delete;
~~~

### operator=

MessagePortProvider::operator=
~~~cpp
MessagePortProvider& operator=(const MessagePortProvider&) = delete;
~~~

### PostMessageToFrame

MessagePortProvider::PostMessageToFrame
~~~cpp
static void PostMessageToFrame(Page& page,
                                 const std::u16string& source_origin,
                                 const std::u16string& target_origin,
                                 const blink::WebMessagePayload& data);
~~~
 Posts a MessageEvent to the main frame using the given source and target
 origins and data.

 See https://html.spec.whatwg.org/multipage/comms.html#messageevent for
 further information on message events.

 Should be called on UI thread.

### PostMessageToFrame

MessagePortProvider::PostMessageToFrame
~~~cpp
static void PostMessageToFrame(
      Page& page,
      JNIEnv* env,
      const base::android::JavaParamRef<jstring>& source_origin,
      const base::android::JavaParamRef<jstring>& target_origin,
      /* org.chromium.content_public.browser.MessagePayload */
      const base::android::JavaParamRef<jobject>& payload,
      const base::android::JavaParamRef<jobjectArray>& ports);
~~~

### PostMessageToFrame

MessagePortProvider::PostMessageToFrame
~~~cpp
static void PostMessageToFrame(
      Page& page,
      const std::u16string& source_origin,
      const absl::optional<std::u16string>& target_origin,
      const std::u16string& data,
      std::vector<blink::WebMessagePort> ports);
~~~
 If |target_origin| is unset, then no origin scoping is applied.
