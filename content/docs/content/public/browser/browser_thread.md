### scoped_refptr&lt;base::SingleThreadTaskRunner&gt;
GetUIThreadTaskRunner

scoped_refptr&lt;base::SingleThreadTaskRunner&gt;
GetUIThreadTaskRunner
~~~cpp
CONTENT_EXPORT scoped_refptr<base::SingleThreadTaskRunner>
GetUIThreadTaskRunner(const BrowserTaskTraits& traits);
~~~
 The main entry point to post tasks to the UI thread. Tasks posted with the
 same |traits| will run in posting order (i.e. according to the
 SequencedTaskRunner contract). Tasks posted with different |traits| can be
 re-ordered. You may keep a reference to this task runner, it's always
 thread-safe to post to it though it may start returning false at some point
 during shutdown when it definitely is no longer accepting tasks.


 In unit tests, there must be a content::BrowserTaskEnvironment in scope for
 this API to be available.


 TODO(1026641): Make default traits |{}| the default param when it's possible
 to include browser_task_traits.h in this file (see note above on the
 BrowserTaskTraits fwd-decl).

### scoped_refptr&lt;base::SingleThreadTaskRunner&gt;
GetIOThreadTaskRunner

scoped_refptr&lt;base::SingleThreadTaskRunner&gt;
GetIOThreadTaskRunner
~~~cpp
CONTENT_EXPORT scoped_refptr<base::SingleThreadTaskRunner>
GetIOThreadTaskRunner(const BrowserTaskTraits& traits);
~~~
 The BrowserThread::IO counterpart to GetUIThreadTaskRunner().

## class BrowserThread

 BrowserThread

 Utility functions for threads that are known by a browser-wide name.

### BrowserThread

BrowserThread
~~~cpp
BrowserThread(const BrowserThread&) = delete;
~~~

### operator=

BrowserThread::operator=
~~~cpp
BrowserThread& operator=(const BrowserThread&) = delete;
~~~

### DeleteSoon

DeleteSoon
~~~cpp
static bool DeleteSoon(ID identifier,
                         const base::Location& from_here,
                         const T* object) {
    return GetTaskRunnerForThread(identifier)->DeleteSoon(from_here, object);
  }
~~~

### DeleteSoon

DeleteSoon
~~~cpp
static bool DeleteSoon(ID identifier,
                         const base::Location& from_here,
                         std::unique_ptr<T> object) {
    return DeleteSoon(identifier, from_here, object.release());
  }
~~~

### ReleaseSoon

ReleaseSoon
~~~cpp
static void ReleaseSoon(ID identifier,
                          const base::Location& from_here,
                          scoped_refptr<T>&& object) {
    GetTaskRunnerForThread(identifier)
        ->ReleaseSoon(from_here, std::move(object));
  }
~~~

### PostBestEffortTask

BrowserThread::PostBestEffortTask
~~~cpp
static void PostBestEffortTask(const base::Location& from_here,
                                 scoped_refptr<base::TaskRunner> task_runner,
                                 base::OnceClosure task);
~~~
 Posts a |task| to run at BEST_EFFORT priority using an arbitrary
 |task_runner| for which we do not control the priority.


 This is useful when a task needs to run on |task_runner| (for thread-safety
 reasons) but should be delayed until after critical phases (e.g. startup).

 TODO(crbug.com/793069): Add support for sequence-funneling and remove this
 method.

### IsThreadInitialized

BrowserThread::IsThreadInitialized
~~~cpp
[[nodiscard]] static bool IsThreadInitialized(ID identifier);
~~~
 Callable on any thread.  Returns whether the given well-known thread is
 initialized.

### CurrentlyOn

BrowserThread::CurrentlyOn
~~~cpp
[[nodiscard]] static bool CurrentlyOn(ID identifier);
~~~
 Callable on any thread.  Returns whether you're currently on a particular
 thread.  To DCHECK this, use the DCHECK_CURRENTLY_ON() macro above.

### GetCurrentThreadIdentifier

BrowserThread::GetCurrentThreadIdentifier
~~~cpp
[[nodiscard]] static bool GetCurrentThreadIdentifier(ID* identifier);
~~~
 If the current message loop is one of the known threads, returns true and
 sets identifier to its ID.  Otherwise returns false.

### GetDCheckCurrentlyOnErrorMessage

BrowserThread::GetDCheckCurrentlyOnErrorMessage
~~~cpp
static std::string GetDCheckCurrentlyOnErrorMessage(ID expected);
~~~
 Returns an appropriate error message for when DCHECK_CURRENTLY_ON() fails.

### RunAllPendingTasksOnThreadForTesting

BrowserThread::RunAllPendingTasksOnThreadForTesting
~~~cpp
static void RunAllPendingTasksOnThreadForTesting(ID identifier);
~~~
 Runs all pending tasks for the given thread. Tasks posted after this method
 is called (in particular any task posted from within any of the pending
 tasks) will be queued but not run. Conceptually this call will disable all
 queues, run any pending tasks, and re-enable all the queues.


 If any of the pending tasks posted a task, these could be run by calling
 this method again or running a regular RunLoop. But if that were the case
 you should probably rewrite you tests to wait for a specific event instead.


 NOTE: Can only be called from the UI thread.

### GetTaskRunnerForThread

BrowserThread::GetTaskRunnerForThread
~~~cpp
static scoped_refptr<base::SingleThreadTaskRunner> GetTaskRunnerForThread(
      ID identifier);
~~~
 Helper that returns GetUIThreadTaskRunner({}) or GetIOThreadTaskRunner({})
 based on |identifier|. Requires that the BrowserThread with the provided
 |identifier| was started.

### BrowserThread

BrowserThread
~~~cpp
BrowserThread() = default;
~~~
