
## struct MediaPlayerWatchTime

### MediaPlayerWatchTime

MediaPlayerWatchTime::MediaPlayerWatchTime
~~~cpp
MediaPlayerWatchTime(GURL url,
                       GURL origin,
                       base::TimeDelta cumulative_watch_time,
                       base::TimeDelta last_timestamp,
                       bool has_video,
                       bool has_audio)
~~~

### MediaPlayerWatchTime

MediaPlayerWatchTime::MediaPlayerWatchTime
~~~cpp
MediaPlayerWatchTime(const MediaPlayerWatchTime& other)
~~~

### ~MediaPlayerWatchTime

~MediaPlayerWatchTime
~~~cpp
~MediaPlayerWatchTime() = default;
~~~
