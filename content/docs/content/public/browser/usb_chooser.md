
## class UsbChooser
 Token representing an open USB port chooser prompt. Destroying this object
 should cancel the prompt.

### UsbChooser

UsbChooser
~~~cpp
UsbChooser(const UsbChooser&) = delete;
~~~

### operator=

UsbChooser::operator=
~~~cpp
UsbChooser& operator=(const UsbChooser&) = delete;
~~~

### ~UsbChooser

UsbChooser::~UsbChooser
~~~cpp
~UsbChooser()
~~~
