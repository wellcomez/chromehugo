
## class BlobHandle
 A handle to Blobs that can be stored outside of content/. This class holds
 a reference to the Blob and should be used to keep alive a Blob.

### ~BlobHandle

~BlobHandle
~~~cpp
virtual ~BlobHandle() {}
~~~

### GetUUID

GetUUID
~~~cpp
virtual std::string GetUUID() = 0;
~~~

### PassBlob

PassBlob
~~~cpp
virtual mojo::PendingRemote<blink::mojom::Blob> PassBlob() = 0;
~~~

### Serialize

Serialize
~~~cpp
virtual blink::mojom::SerializedBlobPtr Serialize() = 0;
~~~

### BlobHandle

BlobHandle
~~~cpp
BlobHandle() {}
~~~

### ~BlobHandle

~BlobHandle
~~~cpp
virtual ~BlobHandle() {}
~~~

### GetUUID

GetUUID
~~~cpp
virtual std::string GetUUID() = 0;
~~~

### PassBlob

PassBlob
~~~cpp
virtual mojo::PendingRemote<blink::mojom::Blob> PassBlob() = 0;
~~~

### Serialize

Serialize
~~~cpp
virtual blink::mojom::SerializedBlobPtr Serialize() = 0;
~~~

### BlobHandle

BlobHandle
~~~cpp
BlobHandle() {}
~~~
