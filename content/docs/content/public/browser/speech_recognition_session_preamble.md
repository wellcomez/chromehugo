
## struct SpeechRecognitionSessionPreamble
    : public
 The preamble is the few seconds of audio before the speech recognition
 starts. This is used to contain trigger audio used to start a voice
 query, such as the 'Ok Google' hotword.

### ~SpeechRecognitionSessionPreamble

SpeechRecognitionSessionPreamble
    : public::~SpeechRecognitionSessionPreamble
~~~cpp
~SpeechRecognitionSessionPreamble()
~~~
