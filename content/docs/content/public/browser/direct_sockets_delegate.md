
## class DirectSocketsDelegate
 Allows the embedder to alter the logic of some operations in
 content::DirectSocketsServiceImpl.

### ~DirectSocketsDelegate

~DirectSocketsDelegate
~~~cpp
virtual ~DirectSocketsDelegate() = default;
~~~

### ValidateAddressAndPort

DirectSocketsDelegate::ValidateAddressAndPort
~~~cpp
virtual bool ValidateAddressAndPort(content::BrowserContext* browser_context,
                                      const GURL& lock_url,
                                      const std::string& address,
                                      uint16_t port,
                                      ProtocolType) const = 0;
~~~
 Allows embedders to introduce additional rules for specific
 addresses/ports. |lock_url| is the URL to which the renderer
 process is locked.
