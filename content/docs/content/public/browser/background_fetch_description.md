
## struct BackgroundFetchDescription
 Contains all information necessary to create a BackgroundFetch download job.

### BackgroundFetchDescription

BackgroundFetchDescription::BackgroundFetchDescription
~~~cpp
BackgroundFetchDescription(const std::string& job_unique_id,
                             const url::Origin& origin,
                             const std::string& title,
                             const SkBitmap& icon,
                             int completed_requests,
                             int total_requests,
                             uint64_t downloaded_bytes,
                             uint64_t uploaded_bytes,
                             uint64_t download_total_bytes,
                             uint64_t upload_total_bytes,
                             std::vector<std::string> outstanding_guids,
                             bool start_paused,
                             absl::optional<net::IsolationInfo> isolation_info)
~~~

### BackgroundFetchDescription

BackgroundFetchDescription
~~~cpp
BackgroundFetchDescription(const BackgroundFetchDescription&) = delete;
~~~

### operator=

BackgroundFetchDescription::operator=
~~~cpp
BackgroundFetchDescription& operator=(const BackgroundFetchDescription&) =
      delete;

  ~BackgroundFetchDescription();
~~~
