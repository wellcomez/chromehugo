
## class BackgroundFetchDelegate::Client
 Client interface that a BackgroundFetchDelegate would use to signal the
 progress of a background fetch.

### ~Client

~Client
~~~cpp
virtual ~Client() {}
~~~

### OnJobCancelled

OnJobCancelled
~~~cpp
virtual void OnJobCancelled(
        const std::string& job_unique_id,
        const std::string& download_guid,
        blink::mojom::BackgroundFetchFailureReason reason_to_abort) = 0;
~~~
 Called when the entire download job has been cancelled by the delegate,
 e.g. because the user clicked cancel on a notification.

### OnDownloadStarted

OnDownloadStarted
~~~cpp
virtual void OnDownloadStarted(
        const std::string& job_unique_id,
        const std::string& download_guid,
        std::unique_ptr<content::BackgroundFetchResponse> response) = 0;
~~~
 Called after the download has started with the initial response
 (including headers and URL chain). Always called on the UI thread.

### OnDownloadUpdated

OnDownloadUpdated
~~~cpp
virtual void OnDownloadUpdated(const std::string& job_unique_id,
                                   const std::string& download_guid,
                                   uint64_t bytes_uploaded,
                                   uint64_t bytes_downloaded) = 0;
~~~
 Called during the download to indicate the current progress. Always
 called on the UI thread.

### OnDownloadComplete

OnDownloadComplete
~~~cpp
virtual void OnDownloadComplete(
        const std::string& job_unique_id,
        const std::string& download_guid,
        std::unique_ptr<BackgroundFetchResult> result) = 0;
~~~
 Called after the download has completed giving the result including the
 path to the downloaded file and its size. Always called on the UI thread.

### OnUIActivated

OnUIActivated
~~~cpp
virtual void OnUIActivated(const std::string& job_unique_id) = 0;
~~~
 Called when the UI of a background fetch job is activated.

### OnUIUpdated

OnUIUpdated
~~~cpp
virtual void OnUIUpdated(const std::string& job_unique_id) = 0;
~~~
 Called after the UI has been updated.

### GetUploadData

GetUploadData
~~~cpp
virtual void GetUploadData(const std::string& job_unique_id,
                               const std::string& download_guid,
                               GetUploadDataCallback callback) = 0;
~~~
 Called by the Download Client when it needs the upload data for
 the given |download_guid|.

### ~Client

~Client
~~~cpp
virtual ~Client() {}
~~~

### OnJobCancelled

OnJobCancelled
~~~cpp
virtual void OnJobCancelled(
        const std::string& job_unique_id,
        const std::string& download_guid,
        blink::mojom::BackgroundFetchFailureReason reason_to_abort) = 0;
~~~
 Called when the entire download job has been cancelled by the delegate,
 e.g. because the user clicked cancel on a notification.

### OnDownloadStarted

OnDownloadStarted
~~~cpp
virtual void OnDownloadStarted(
        const std::string& job_unique_id,
        const std::string& download_guid,
        std::unique_ptr<content::BackgroundFetchResponse> response) = 0;
~~~
 Called after the download has started with the initial response
 (including headers and URL chain). Always called on the UI thread.

### OnDownloadUpdated

OnDownloadUpdated
~~~cpp
virtual void OnDownloadUpdated(const std::string& job_unique_id,
                                   const std::string& download_guid,
                                   uint64_t bytes_uploaded,
                                   uint64_t bytes_downloaded) = 0;
~~~
 Called during the download to indicate the current progress. Always
 called on the UI thread.

### OnDownloadComplete

OnDownloadComplete
~~~cpp
virtual void OnDownloadComplete(
        const std::string& job_unique_id,
        const std::string& download_guid,
        std::unique_ptr<BackgroundFetchResult> result) = 0;
~~~
 Called after the download has completed giving the result including the
 path to the downloaded file and its size. Always called on the UI thread.

### OnUIActivated

OnUIActivated
~~~cpp
virtual void OnUIActivated(const std::string& job_unique_id) = 0;
~~~
 Called when the UI of a background fetch job is activated.

### OnUIUpdated

OnUIUpdated
~~~cpp
virtual void OnUIUpdated(const std::string& job_unique_id) = 0;
~~~
 Called after the UI has been updated.

### GetUploadData

GetUploadData
~~~cpp
virtual void GetUploadData(const std::string& job_unique_id,
                               const std::string& download_guid,
                               GetUploadDataCallback callback) = 0;
~~~
 Called by the Download Client when it needs the upload data for
 the given |download_guid|.

## class BackgroundFetchDelegate
 Interface for launching background fetches. Implementing classes would
 generally interface with the DownloadService or DownloadManager.

 Must only be used on the UI thread and generally expected to be called by the
 BackgroundFetchDelegateProxy.

### ~BackgroundFetchDelegate

BackgroundFetchDelegate::~BackgroundFetchDelegate
~~~cpp
~BackgroundFetchDelegate()
~~~

### GetIconDisplaySize

BackgroundFetchDelegate::GetIconDisplaySize
~~~cpp
virtual void GetIconDisplaySize(GetIconDisplaySizeCallback callback) = 0;
~~~
 Gets size of the icon to display with the Background Fetch UI.

### CreateDownloadJob

BackgroundFetchDelegate::CreateDownloadJob
~~~cpp
virtual void CreateDownloadJob(
      base::WeakPtr<Client> client,
      std::unique_ptr<BackgroundFetchDescription> fetch_description) = 0;
~~~
 Creates a new download grouping identified by |job_unique_id|. Further
 downloads started by DownloadUrl will also use this |job_unique_id| so that
 a notification can be updated with the current status. If the download was
 already started in a previous browser session, then |current_guids| should
 contain the GUIDs of in progress downloads, while completed downloads are
 recorded in |completed_parts|. Updates are communicated to |client|.

### DownloadUrl

BackgroundFetchDelegate::DownloadUrl
~~~cpp
virtual void DownloadUrl(
      const std::string& job_unique_id,
      const std::string& download_guid,
      const std::string& method,
      const GURL& url,
      ::network::mojom::CredentialsMode credentials_mode,
      const net::NetworkTrafficAnnotationTag& traffic_annotation,
      const net::HttpRequestHeaders& headers,
      bool has_request_body) = 0;
~~~
 Creates a new download identified by |download_guid| in the download job
 identified by |job_unique_id|.

### Abort

BackgroundFetchDelegate::Abort
~~~cpp
virtual void Abort(const std::string& job_unique_id) = 0;
~~~
 Aborts any downloads associated with |job_unique_id|.

### MarkJobComplete

BackgroundFetchDelegate::MarkJobComplete
~~~cpp
virtual void MarkJobComplete(const std::string& job_unique_id) = 0;
~~~
 Called after the fetch has completed so that the delegate can clean up.

### UpdateUI

BackgroundFetchDelegate::UpdateUI
~~~cpp
virtual void UpdateUI(const std::string& job_unique_id,
                        const absl::optional<std::string>& title,
                        const absl::optional<SkBitmap>& icon) = 0;
~~~
 Updates the UI shown for the fetch job associated with |job_unique_id| to
 display a new |title| or |icon|.
