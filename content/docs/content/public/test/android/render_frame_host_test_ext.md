
## class RenderFrameHostTestExt

### RenderFrameHostTestExt

RenderFrameHostTestExt::RenderFrameHostTestExt
~~~cpp
explicit RenderFrameHostTestExt(RenderFrameHostImpl* rfhi);
~~~

### ExecuteJavaScript

RenderFrameHostTestExt::ExecuteJavaScript
~~~cpp
void ExecuteJavaScript(JNIEnv* env,
                         const base::android::JavaParamRef<jstring>& jscript,
                         const base::android::JavaParamRef<jobject>& jcallback,
                         jboolean with_user_gesture);
~~~

### UpdateVisualState

RenderFrameHostTestExt::UpdateVisualState
~~~cpp
void UpdateVisualState(JNIEnv* env,
                         const base::android::JavaParamRef<jobject>& jcallback);
~~~
 This calls InsertVisualStateCallback(). See it for details on the return
 value.

### NotifyVirtualKeyboardOverlayRect

RenderFrameHostTestExt::NotifyVirtualKeyboardOverlayRect
~~~cpp
void NotifyVirtualKeyboardOverlayRect(JNIEnv* env,
                                        jint x,
                                        jint y,
                                        jint width,
                                        jint height);
~~~

### render_frame_host_



~~~cpp

const raw_ptr<RenderFrameHostImpl> render_frame_host_;

~~~


### ExecuteJavaScript

RenderFrameHostTestExt::ExecuteJavaScript
~~~cpp
void ExecuteJavaScript(JNIEnv* env,
                         const base::android::JavaParamRef<jstring>& jscript,
                         const base::android::JavaParamRef<jobject>& jcallback,
                         jboolean with_user_gesture);
~~~

### UpdateVisualState

RenderFrameHostTestExt::UpdateVisualState
~~~cpp
void UpdateVisualState(JNIEnv* env,
                         const base::android::JavaParamRef<jobject>& jcallback);
~~~
 This calls InsertVisualStateCallback(). See it for details on the return
 value.

### NotifyVirtualKeyboardOverlayRect

RenderFrameHostTestExt::NotifyVirtualKeyboardOverlayRect
~~~cpp
void NotifyVirtualKeyboardOverlayRect(JNIEnv* env,
                                        jint x,
                                        jint y,
                                        jint width,
                                        jint height);
~~~

### render_frame_host_



~~~cpp

const raw_ptr<RenderFrameHostImpl> render_frame_host_;

~~~

