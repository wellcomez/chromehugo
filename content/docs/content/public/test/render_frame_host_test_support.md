### LeaveInPendingDeletionState

LeaveInPendingDeletionState
~~~cpp
void LeaveInPendingDeletionState(RenderFrameHost* rfh);
~~~
 Test-only helpers.

 Forces RenderFrameHost to be left in pending deletion, so it would not be
 deleted. This is done to ensure that the tests have a way to reliably get a
 RenderFrameHost which is inactive (see RenderFrameHost::IsActive) and
 test that they handle it correctly.

