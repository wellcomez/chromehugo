### FromRoutingID

FromRoutingID
~~~cpp
static RenderFrame* FromRoutingID(int routing_id);
~~~
 Returns the RenderFrame given a routing id.

### ForEach

ForEach
~~~cpp
static void ForEach(RenderFrameVisitor* visitor);
~~~
 Visit all live RenderFrames.

### GetMainRenderFrame

GetMainRenderFrame
~~~cpp
virtual RenderFrame* GetMainRenderFrame() = 0;
~~~
 Returns the RenderFrame associated with the main frame of the WebView.

 See `blink::WebView::MainFrame()`. Note that this will be null when
 the main frame in this process is a remote frame.

### GetRenderAccessibility

GetRenderAccessibility
~~~cpp
virtual RenderAccessibility* GetRenderAccessibility() = 0;
~~~
 Return the RenderAccessibility associated with this frame.

### CreateAXTreeSnapshotter

CreateAXTreeSnapshotter
~~~cpp
virtual std::unique_ptr<AXTreeSnapshotter> CreateAXTreeSnapshotter(
      ui::AXMode ax_mode) = 0;
~~~
 Return an object that can take a snapshot of the accessibility tree.

 |ax_mode| is the accessibility mode to use, which determines which
 fields of AXNodeData are populated when you make a snapshot.

### GetRoutingID

GetRoutingID
~~~cpp
virtual int GetRoutingID() = 0;
~~~
 Get the routing ID of the frame.

### GetWebView

GetWebView
~~~cpp
virtual blink::WebView* GetWebView() = 0;
~~~
 Returns the associated WebView.

### GetWebView

GetWebView
~~~cpp
virtual const blink::WebView* GetWebView() const = 0;
~~~

### GetWebFrame

GetWebFrame
~~~cpp
virtual blink::WebLocalFrame* GetWebFrame() = 0;
~~~
 Returns the associated WebFrame.

### GetWebFrame

GetWebFrame
~~~cpp
virtual const blink::WebLocalFrame* GetWebFrame() const = 0;
~~~

### GetBlinkPreferences

GetBlinkPreferences
~~~cpp
virtual const blink::web_pref::WebPreferences& GetBlinkPreferences() = 0;
~~~
 Gets WebKit related preferences associated with this frame.

### ShowVirtualKeyboard

ShowVirtualKeyboard
~~~cpp
virtual void ShowVirtualKeyboard() = 0;
~~~
 Issues a request to show the virtual keyboard.

### CreatePlugin

CreatePlugin
~~~cpp
virtual blink::WebPlugin* CreatePlugin(
      const WebPluginInfo& info,
      const blink::WebPluginParams& params) = 0;
~~~
 Create a new Pepper plugin depending on |info|. Returns NULL if no plugin
 was found.

### ExecuteJavaScript

ExecuteJavaScript
~~~cpp
virtual void ExecuteJavaScript(const std::u16string& javascript) = 0;
~~~
 Execute a string of JavaScript in this frame's context.

### IsMainFrame

IsMainFrame
~~~cpp
virtual bool IsMainFrame() = 0;
~~~
 Returns true if this is the main (top-level) frame.

### IsInFencedFrameTree

IsInFencedFrameTree
~~~cpp
virtual bool IsInFencedFrameTree() const = 0;
~~~
 Returns false if fenced frames are disabled. Returns true if the
 feature is enabled and if |this| or any of its ancestor nodes is a
 fenced frame.

### IsHidden

IsHidden
~~~cpp
virtual bool IsHidden() = 0;
~~~
 Return true if this frame is hidden.

### BindLocalInterface

BindLocalInterface
~~~cpp
virtual void BindLocalInterface(
      const std::string& interface_name,
      mojo::ScopedMessagePipeHandle interface_pipe) = 0;
~~~
 Ask the RenderFrame (or its observers) to bind a request for
 |interface_name| to |interface_pipe|.

### GetBrowserInterfaceBroker

GetBrowserInterfaceBroker
~~~cpp
virtual blink::BrowserInterfaceBrokerProxy* GetBrowserInterfaceBroker() = 0;
~~~
 Returns the BrowserInterfaceBrokerProxy that this process can use to bind
 interfaces exposed to it by the application running in this frame.

### GetAssociatedInterfaceRegistry

GetAssociatedInterfaceRegistry
~~~cpp
virtual blink::AssociatedInterfaceRegistry*
  GetAssociatedInterfaceRegistry() = 0;
~~~
 Returns the AssociatedInterfaceRegistry this frame can use to expose
 frame-specific Channel-associated interfaces to the remote RenderFrameHost.

### GetRemoteAssociatedInterfaces

GetRemoteAssociatedInterfaces
~~~cpp
virtual blink::AssociatedInterfaceProvider*
  GetRemoteAssociatedInterfaces() = 0;
~~~
 Returns the AssociatedInterfaceProvider this frame can use to access
 frame-specific Channel-associated interfaces from the remote
 RenderFrameHost.

### SetSelectedText

SetSelectedText
~~~cpp
virtual void SetSelectedText(const std::u16string& selection_text,
                               size_t offset,
                               const gfx::Range& range) = 0;
~~~
 Notifies the browser of text selection changes made.

### AddMessageToConsole

AddMessageToConsole
~~~cpp
virtual void AddMessageToConsole(blink::mojom::ConsoleMessageLevel level,
                                   const std::string& message) = 0;
~~~
 Adds |message| to the DevTools console.

### IsPasting

IsPasting
~~~cpp
virtual bool IsPasting() = 0;
~~~
 Whether or not this frame is currently pasting.

### LoadHTMLStringForTesting

LoadHTMLStringForTesting
~~~cpp
virtual void LoadHTMLStringForTesting(const std::string& html,
                                        const GURL& base_url,
                                        const std::string& text_encoding,
                                        const GURL& unreachable_url,
                                        bool replace_current_item) = 0;
~~~
 Loads specified |html| to this frame. |base_url| is used to resolve
 relative urls in the document.

 |replace_current_item| should be true if we load html instead of the
 existing page. In this case |unreachable_url| might be the original url
 which did fail loading.


 This should be used only for testing. Real code should follow the
 navigation code path and inherit the correct security properties
### IsBrowserSideNavigationPending

IsBrowserSideNavigationPending
~~~cpp
virtual bool IsBrowserSideNavigationPending() = 0;
~~~
 Returns true in between the time that Blink requests navigation until the
 browser responds with the result.

 TODO(ahemery): Rename this to be more explicit.

### GetTaskRunner

GetTaskRunner
~~~cpp
virtual scoped_refptr<base::SingleThreadTaskRunner> GetTaskRunner(
      blink::TaskType task_type) = 0;
~~~
 Renderer scheduler frame-specific task queues handles.

 See third_party/WebKit/Source/platform/WebFrameScheduler.h for details.

### GetEnabledBindings

GetEnabledBindings
~~~cpp
virtual int GetEnabledBindings() = 0;
~~~
 Bitwise-ORed set of extra bindings that have been enabled.  See
 BindingsPolicy for details.

### SetAccessibilityModeForTest

SetAccessibilityModeForTest
~~~cpp
virtual void SetAccessibilityModeForTest(ui::AXMode new_mode) = 0;
~~~
 Set the accessibility mode to force creation of RenderAccessibility.

### GetRenderFrameMediaPlaybackOptions

GetRenderFrameMediaPlaybackOptions
~~~cpp
virtual const RenderFrameMediaPlaybackOptions&
  GetRenderFrameMediaPlaybackOptions() = 0;
~~~
 Per-frame media playback options passed to each WebMediaPlayer.

### SetRenderFrameMediaPlaybackOptions

SetRenderFrameMediaPlaybackOptions
~~~cpp
virtual void SetRenderFrameMediaPlaybackOptions(
      const RenderFrameMediaPlaybackOptions& opts) = 0;
~~~

### SetAllowsCrossBrowsingInstanceFrameLookup

SetAllowsCrossBrowsingInstanceFrameLookup
~~~cpp
virtual void SetAllowsCrossBrowsingInstanceFrameLookup() = 0;
~~~
 Sets that cross browsing instance frame lookup is allowed.

### ElementBoundsInWindow

ElementBoundsInWindow
~~~cpp
virtual gfx::RectF ElementBoundsInWindow(
      const blink::WebElement& element) = 0;
~~~
 Returns the bounds of |element| in Window coordinates which are device
 scale independent. The bounds have been adjusted to include any
 transformations, including page scale. This function will update the layout
 if required.

### ConvertViewportToWindow

ConvertViewportToWindow
~~~cpp
virtual void ConvertViewportToWindow(gfx::Rect* rect) = 0;
~~~
 Converts the |rect| to Window coordinates which are device scale
 independent.

### GetDeviceScaleFactor

GetDeviceScaleFactor
~~~cpp
virtual float GetDeviceScaleFactor() = 0;
~~~
 Returns the device scale factor of the display the render frame is in.

### GetAgentGroupScheduler

GetAgentGroupScheduler
~~~cpp
virtual blink::scheduler::WebAgentGroupScheduler&
  GetAgentGroupScheduler() = 0;
~~~
 Return the dedicated scheduler for the AgentSchedulingGroup associated with
 this RenderFrame.

### RenderFrame

RenderFrame
~~~cpp
RenderFrame() {}
~~~

## class AXTreeSnapshotter
 A class that takes a snapshot of the accessibility tree. Accessibility
 support in Blink is enabled for the lifetime of this object, which can
 be useful if you need consistent IDs between multiple snapshots.

### AXTreeSnapshotter

AXTreeSnapshotter
~~~cpp
AXTreeSnapshotter() = default;
~~~

### Snapshot

Snapshot
~~~cpp
virtual void Snapshot(bool exclude_offscreen,
                        size_t max_node_count,
                        base::TimeDelta timeout,
                        ui::AXTreeUpdate* accessibility_tree) = 0;
~~~
 Return in |accessibility_tree| a snapshot of the accessibility tree
 for the frame with the given accessibility mode.


 - |exclude_offscreen| excludes a subtree if a node is entirely offscreen,
   but note that this heuristic is imperfect, and an aboslute-positioned
   node that's visible, but whose ancestors are entirely offscreen, may
   get excluded.

 - |max_nodes_count| specifies the maximum number of nodes to snapshot
   before exiting early. Note that this is not a hard limit; once this limit
   is reached a few more nodes may be added in order to ensure a
   well-formed tree is returned. Use 0 for no max.

 - |timeout| will stop generating the result after a certain timeout
   (per frame), specified in milliseconds. Like max_node_count, this is not
   a hard limit, and once this/ limit is reached a few more nodes may
   be added in order to ensure a well-formed tree. Use 0 for no timeout.

### ~AXTreeSnapshotter

~AXTreeSnapshotter
~~~cpp
virtual ~AXTreeSnapshotter() = default;
~~~

### AXTreeSnapshotter

AXTreeSnapshotter
~~~cpp
AXTreeSnapshotter() = default;
~~~

### Snapshot

Snapshot
~~~cpp
virtual void Snapshot(bool exclude_offscreen,
                        size_t max_node_count,
                        base::TimeDelta timeout,
                        ui::AXTreeUpdate* accessibility_tree) = 0;
~~~
 Return in |accessibility_tree| a snapshot of the accessibility tree
 for the frame with the given accessibility mode.


 - |exclude_offscreen| excludes a subtree if a node is entirely offscreen,
   but note that this heuristic is imperfect, and an aboslute-positioned
   node that's visible, but whose ancestors are entirely offscreen, may
   get excluded.

 - |max_nodes_count| specifies the maximum number of nodes to snapshot
   before exiting early. Note that this is not a hard limit; once this limit
   is reached a few more nodes may be added in order to ensure a
   well-formed tree is returned. Use 0 for no max.

 - |timeout| will stop generating the result after a certain timeout
   (per frame), specified in milliseconds. Like max_node_count, this is not
   a hard limit, and once this/ limit is reached a few more nodes may
   be added in order to ensure a well-formed tree. Use 0 for no timeout.

### ~AXTreeSnapshotter

~AXTreeSnapshotter
~~~cpp
virtual ~AXTreeSnapshotter() = default;
~~~
