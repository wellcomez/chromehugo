
## class DownloadWorker
    : public
 Helper class used to send subsequent range requests to fetch slices of the
 file after handling response of the original non-range request.

 TODO(xingliu): we should consider to reuse this class for single connection
 download.

### DownloadWorker

DownloadWorker
    : public::DownloadWorker
~~~cpp
DownloadWorker(DownloadWorker::Delegate* delegate, int64_t offset)
~~~

### DownloadWorker

DownloadWorker
~~~cpp
DownloadWorker(const DownloadWorker&) = delete;
~~~

### operator=

DownloadWorker
    : public::operator=
~~~cpp
DownloadWorker& operator=(const DownloadWorker&) = delete;
~~~

### ~DownloadWorker

DownloadWorker
    : public::~DownloadWorker
~~~cpp
~DownloadWorker()
~~~

### offset

offset
~~~cpp
int64_t offset() const { return offset_; }
~~~

### SendRequest

DownloadWorker
    : public::SendRequest
~~~cpp
void SendRequest(
      std::unique_ptr<DownloadUrlParameters> params,
      URLLoaderFactoryProvider* url_loader_factory_provider,
      mojo::PendingRemote<device::mojom::WakeLockProvider> wake_lock_provider);
~~~
 Send network request to ask for a download.

### Pause

DownloadWorker
    : public::Pause
~~~cpp
void Pause();
~~~
 Download operations.

### Resume

DownloadWorker
    : public::Resume
~~~cpp
void Resume();
~~~

### Cancel

DownloadWorker
    : public::Cancel
~~~cpp
void Cancel(bool user_cancel);
~~~

### OnUrlDownloadStopped

DownloadWorker
    : public::OnUrlDownloadStopped
~~~cpp
void OnUrlDownloadStopped(UrlDownloadHandlerID downloader) override;
~~~

### OnUrlDownloadHandlerCreated

DownloadWorker
    : public::OnUrlDownloadHandlerCreated
~~~cpp
void OnUrlDownloadHandlerCreated(
      UrlDownloadHandler::UniqueUrlDownloadHandlerPtr downloader) override;
~~~
