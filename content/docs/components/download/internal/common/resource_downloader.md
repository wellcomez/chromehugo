---
tags:
  - download
---

Class for handing the download of a url. Lives on IO thread.


### InterceptNavigationResponse

InterceptNavigationResponse
~~~cpp
static void InterceptNavigationResponse(
      base::WeakPtr<UrlDownloadHandler::Delegate> delegate,
      std::unique_ptr<network::ResourceRequest> resource_request,
      int render_process_id,
      int render_frame_id,
      const std::string& serialized_embedder_download_data,
      const GURL& tab_url,
      const GURL& tab_referrer_url,
      std::vector<GURL> url_chain,
      net::CertStatus cert_status,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      const URLSecurityPolicy& url_security_policy,
      mojo::PendingRemote<device::mojom::WakeLockProvider> wake_lock_provider,
      const scoped_refptr<base::SingleThreadTaskRunner>& task_runner);
~~~
 Create a ResourceDownloader from a navigation that turns to be a download.

 No URLLoader is created, but the URLLoaderClient implementation is
 transferred.

### ResourceDownloader

ResourceDownloader
~~~cpp
ResourceDownloader(
      base::WeakPtr<UrlDownloadHandler::Delegate> delegate,
      std::unique_ptr<network::ResourceRequest> resource_request,
      int render_process_id,
      int render_frame_id,
      const std::string& serialized_embedder_download_data,
      const GURL& tab_url,
      const GURL& tab_referrer_url,
      bool is_new_download,
      const scoped_refptr<base::SingleThreadTaskRunner>& task_runner,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      const URLSecurityPolicy& url_security_policy,
      mojo::PendingRemote<device::mojom::WakeLockProvider> wake_lock_provider)
~~~

### ResourceDownloader

ResourceDownloader
~~~cpp
ResourceDownloader(const ResourceDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
ResourceDownloader& operator=(const ResourceDownloader&) = delete;
~~~

### ~ResourceDownloader

~ResourceDownloader
~~~cpp
~ResourceDownloader() override
~~~

### OnResponseStarted

OnResponseStarted
~~~cpp
void OnResponseStarted(
      std::unique_ptr<download::DownloadCreateInfo> download_create_info,
      mojom::DownloadStreamHandlePtr stream_handle) override;
~~~
 DownloadResponseHandler::Delegate
### OnReceiveRedirect

OnReceiveRedirect
~~~cpp
void OnReceiveRedirect() override;
~~~

### OnResponseCompleted

OnResponseCompleted
~~~cpp
void OnResponseCompleted() override;
~~~

### CanRequestURL

CanRequestURL
~~~cpp
bool CanRequestURL(const GURL& url) override;
~~~

### OnUploadProgress

OnUploadProgress
~~~cpp
void OnUploadProgress(uint64_t bytes_uploaded) override;
~~~

### InterceptResponse

InterceptResponse
~~~cpp
void InterceptResponse(
      std::vector<GURL> url_chain,
      net::CertStatus cert_status,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints);
~~~
 Intercepts the navigation response.

### Destroy

Destroy
~~~cpp
void Destroy();
~~~
 Ask the |delegate_| to destroy this object.

### RequestWakeLock

RequestWakeLock
~~~cpp
void RequestWakeLock(device::mojom::WakeLockProvider* provider);
~~~
 Requests the wake lock using |provider|.


### BeginDownload
Called to start a download, must be called on IO thread
~~~c
public:
  // Called to start a download, must be called on IO thread.
  static std::unique_ptr<ResourceDownloader> BeginDownload(
      base::WeakPtr<download::UrlDownloadHandler::Delegate> delegate,
      std::unique_ptr<download::DownloadUrlParameters> download_url_parameters,
      std::unique_ptr<network::ResourceRequest> request,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      const URLSecurityPolicy& url_security_policy,
      const std::string& serialized_embedder_download_data,
      const GURL& tab_url,
      const GURL& tab_referrer_url,
      bool is_new_download,
      bool is_parallel_request,
      mojo::PendingRemote<device::mojom::WakeLockProvider> wake_lock_provider,
      bool is_background_mode,
      const scoped_refptr<base::SingleThreadTaskRunner>& task_runner)
~~~

