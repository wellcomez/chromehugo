---
tags:
  - download
---

## class UrlDownloadRequestHandle
 Implementation of the DownloadRequestHandleInterface to handle url download.

### operator=

UrlDownloadRequestHandle::operator=
~~~cpp
UrlDownloadRequestHandle& operator=(UrlDownloadRequestHandle&& other);
~~~

### UrlDownloadRequestHandle

UrlDownloadRequestHandle
~~~cpp
UrlDownloadRequestHandle(const UrlDownloadRequestHandle&) = delete;
~~~

### operator=

UrlDownloadRequestHandle::operator=
~~~cpp
UrlDownloadRequestHandle& operator=(const UrlDownloadRequestHandle&) = delete;

  ~UrlDownloadRequestHandle();
~~~
