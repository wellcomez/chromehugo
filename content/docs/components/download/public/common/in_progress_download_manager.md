---
tags:
  - download
---

### InProgressDownloadManager

InProgressDownloadManager
~~~cpp
InProgressDownloadManager(Delegate* delegate,
                            const base::FilePath& in_progress_db_dir,
                            leveldb_proto::ProtoDatabaseProvider* db_provider,
                            const IsOriginSecureCallback& is_origin_secure_cb,
                            const URLSecurityPolicy& url_security_policy,
                            WakeLockProviderBinder wake_lock_provider_binder)
~~~

### InProgressDownloadManager

InProgressDownloadManager
~~~cpp
InProgressDownloadManager(const InProgressDownloadManager&) = delete;
~~~

### operator=

operator=
~~~cpp
InProgressDownloadManager& operator=(const InProgressDownloadManager&) =
      delete;
~~~

### ~InProgressDownloadManager

~InProgressDownloadManager
~~~cpp
~InProgressDownloadManager() override
~~~

### DownloadUrl

DownloadUrl
~~~cpp
void DownloadUrl(std::unique_ptr<DownloadUrlParameters> params) override;
~~~
 SimpleDownloadManager implementation.

### CanDownload

CanDownload
~~~cpp
bool CanDownload(DownloadUrlParameters* params) override;
~~~

### GetAllDownloads

GetAllDownloads
~~~cpp
void GetAllDownloads(
      SimpleDownloadManager::DownloadVector* downloads) override;
~~~

### GetDownloadByGuid

GetDownloadByGuid
~~~cpp
DownloadItem* GetDownloadByGuid(const std::string& guid) override;
~~~

### BeginDownload

BeginDownload
~~~cpp
void BeginDownload(std::unique_ptr<DownloadUrlParameters> params,
                     std::unique_ptr<network::PendingSharedURLLoaderFactory>
                         pending_url_loader_factory,
                     bool is_new_download,
                     const std::string& serialized_embedder_download_data,
                     const GURL& tab_url,
                     const GURL& tab_referrer_url);
~~~
 Called to start a download.

### InterceptDownloadFromNavigation

InterceptDownloadFromNavigation
~~~cpp
void InterceptDownloadFromNavigation(
      std::unique_ptr<network::ResourceRequest> resource_request,
      int render_process_id,
      int render_frame_id,
      const std::string& serialized_embedder_download_data,
      const GURL& tab_url,
      const GURL& tab_referrer_url,
      std::vector<GURL> url_chain,
      net::CertStatus cert_status,
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle response_body,
      network::mojom::URLLoaderClientEndpointsPtr url_loader_client_endpoints,
      std::unique_ptr<network::PendingSharedURLLoaderFactory>
          pending_url_loader_factory);
~~~
 Intercepts a download from navigation.

### StartDownload

StartDownload
~~~cpp
void StartDownload(std::unique_ptr<DownloadCreateInfo> info,
                     std::unique_ptr<InputStream> stream,
                     URLLoaderFactoryProvider::URLLoaderFactoryProviderPtr
                         url_loader_factory_provider,
                     DownloadJob::CancelRequestCallback cancel_request_callback,
                     DownloadUrlParameters::OnStartedCallback on_started);
~~~

### ShutDown

ShutDown
~~~cpp
void ShutDown();
~~~
 Shutting down the manager and stop all downloads.

### DetermineDownloadTarget

DetermineDownloadTarget
~~~cpp
void DetermineDownloadTarget(DownloadItemImpl* download,
                               DownloadTargetCallback callback) override;
~~~
 DownloadItemImplDelegate implementations.

### ResumeInterruptedDownload

ResumeInterruptedDownload
~~~cpp
void ResumeInterruptedDownload(
      std::unique_ptr<DownloadUrlParameters> params,
      const std::string& serialized_embedder_download_data) override;
~~~

### ShouldOpenDownload

ShouldOpenDownload
~~~cpp
bool ShouldOpenDownload(DownloadItemImpl* item,
                          ShouldOpenDownloadCallback callback) override;
~~~

### ReportBytesWasted

ReportBytesWasted
~~~cpp
void ReportBytesWasted(DownloadItemImpl* download) override;
~~~

### RemoveInProgressDownload

RemoveInProgressDownload
~~~cpp
void RemoveInProgressDownload(const std::string& guid);
~~~
 Called to remove an in-progress download.

### set_download_start_observer

set_download_start_observer
~~~cpp
void set_download_start_observer(DownloadStartObserver* observer) {
    download_start_observer_ = observer;
  }
~~~

### set_intermediate_path_cb

set_intermediate_path_cb
~~~cpp
void set_intermediate_path_cb(
      const IntermediatePathCallback& intermediate_path_cb) {
    intermediate_path_cb_ = intermediate_path_cb;
  }
~~~

### set_default_download_dir

set_default_download_dir
~~~cpp
void set_default_download_dir(base::FilePath default_download_dir) {
    default_download_dir_ = default_download_dir;
  }
~~~

### TakeInProgressDownloads

TakeInProgressDownloads
~~~cpp
virtual std::vector<std::unique_ptr<download::DownloadItemImpl>>
  TakeInProgressDownloads();
~~~
 Called to get all in-progress DownloadItemImpl.

 TODO(qinmin): remove this method once InProgressDownloadManager owns
 all in-progress downloads.

### OnAllInprogressDownloadsLoaded

OnAllInprogressDownloadsLoaded
~~~cpp
void OnAllInprogressDownloadsLoaded();
~~~
 Called when all the DownloadItem is loaded.

 TODO(qinmin): remove this once features::kDownloadDBForNewDownloads is
 enabled by default.

### GetDownloadDisplayName

GetDownloadDisplayName
~~~cpp
base::FilePath GetDownloadDisplayName(const base::FilePath& path);
~~~
 Gets the display name for a download. For non-android platforms, this
 always returns an empty path.

### set_file_factory

set_file_factory
~~~cpp
void set_file_factory(std::unique_ptr<DownloadFileFactory> file_factory) {
    file_factory_ = std::move(file_factory);
  }
~~~

### file_factory

file_factory
~~~cpp
DownloadFileFactory* file_factory() { return file_factory_.get(); }
~~~

### set_url_loader_factory

set_url_loader_factory
~~~cpp
void set_url_loader_factory(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory) {
    url_loader_factory_ = url_loader_factory;
  }
~~~

### SetDelegate

SetDelegate
~~~cpp
void SetDelegate(Delegate* delegate);
~~~

### set_is_origin_secure_cb

set_is_origin_secure_cb
~~~cpp
void set_is_origin_secure_cb(
      const IsOriginSecureCallback& is_origin_secure_cb) {
    is_origin_secure_cb_ = is_origin_secure_cb;
  }
~~~

### AddInProgressDownloadForTest

AddInProgressDownloadForTest
~~~cpp
void AddInProgressDownloadForTest(
      std::unique_ptr<download::DownloadItemImpl> download);
~~~
 Called to insert an in-progress download for testing purpose.

### OnUrlDownloadStarted

OnUrlDownloadStarted
~~~cpp
void OnUrlDownloadStarted(
      std::unique_ptr<DownloadCreateInfo> download_create_info,
      std::unique_ptr<InputStream> input_stream,
      URLLoaderFactoryProvider::URLLoaderFactoryProviderPtr
          url_loader_factory_provider,
      UrlDownloadHandlerID downloader,
      DownloadUrlParameters::OnStartedCallback callback) override;
~~~
 UrlDownloadHandler::Delegate implementations.

### OnUrlDownloadStopped

OnUrlDownloadStopped
~~~cpp
void OnUrlDownloadStopped(UrlDownloadHandlerID downloader) override;
~~~

### OnUrlDownloadHandlerCreated

OnUrlDownloadHandlerCreated
~~~cpp
void OnUrlDownloadHandlerCreated(
      UrlDownloadHandler::UniqueUrlDownloadHandlerPtr downloader) override;
~~~

### OnDBInitialized

OnDBInitialized
~~~cpp
void OnDBInitialized(bool success,
                       std::unique_ptr<std::vector<DownloadDBEntry>> entries);
~~~
 Called when the in-progress DB is initialized.

### OnDownloadNamesRetrieved

OnDownloadNamesRetrieved
~~~cpp
void OnDownloadNamesRetrieved(
      std::unique_ptr<std::vector<DownloadDBEntry>> entries,
      DisplayNames display_names);
~~~
 Called when download display names are retrieved,
### StartDownloadWithItem

StartDownloadWithItem
~~~cpp
void StartDownloadWithItem(
      std::unique_ptr<InputStream> stream,
      URLLoaderFactoryProvider::URLLoaderFactoryProviderPtr
          url_loader_factory_provider,
      DownloadJob::CancelRequestCallback cancel_request_callback,
      std::unique_ptr<DownloadCreateInfo> info,
      DownloadItemImpl* download,
      bool should_persist_new_download);
~~~
 Start a DownloadItemImpl.

### OnDownloadsInitialized

OnDownloadsInitialized
~~~cpp
void OnDownloadsInitialized();
~~~
 Called when downloads are initialized.

### NotifyDownloadsInitialized

NotifyDownloadsInitialized
~~~cpp
void NotifyDownloadsInitialized();
~~~
 Called to notify |delegate_| that downloads are initialized.

### CancelUrlDownload

CancelUrlDownload
~~~cpp
void CancelUrlDownload(UrlDownloadHandlerID downloader, bool user_cancel);
~~~
 Cancels the given UrlDownloadHandler.

## class Delegate
 Class to be notified when download starts/stops.

### InterceptDownload

Delegate::InterceptDownload
~~~cpp
virtual bool InterceptDownload(
        const DownloadCreateInfo& download_create_info);
~~~
 Intercepts the download to another system if applicable. Returns true if
 the download was intercepted.

### GetDefaultDownloadDirectory

Delegate::GetDefaultDownloadDirectory
~~~cpp
virtual base::FilePath GetDefaultDownloadDirectory();
~~~
 Gets the default download directory.

### StartDownloadItem

StartDownloadItem
~~~cpp
virtual void StartDownloadItem(
        std::unique_ptr<DownloadCreateInfo> info,
        DownloadUrlParameters::OnStartedCallback on_started,
        StartDownloadItemCallback callback) {}
~~~
 Gets the download item for the given |download_create_info|.

 TODO(qinmin): remove this method and let InProgressDownloadManager
 create the DownloadItem from in-progress cache.
