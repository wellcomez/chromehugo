---
tags:
  - download
---

## UrlDownloadHandlerFactory

Class for handling the creation of a URLDownloadHandler. This is used to allow injection of different URLDownloadHandler implementations.

TODO(qinmin): remove this factory once network service is fully enabled
~~~c

#ifndef 
namespace download {
class DownloadUrlParameters;

.
class COMPONENTS_DOWNLOAD_EXPORT UrlDownloadHandlerFactory {
 public:
  // Creates a URLDownloadHandler. By default the handler is used for network
  // service. Must be called on the IO thread.
  static UrlDownloadHandler::UniqueUrlDownloadHandlerPtr Create(
      std::unique_ptr<download::DownloadUrlParameters> params,
      base::WeakPtr<download::UrlDownloadHandler::Delegate> delegate,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      const URLSecurityPolicy& url_security_policy,
      mojo::PendingRemote<device::mojom::WakeLockProvider> wake_lock_provider,
      const scoped_refptr<base::SingleThreadTaskRunner>& task_runner);
};

~~~

### Create
  Creates a URLDownloadHandler. By default the handler is used for network service. Must be called on the IO thread.
  [url_download_handler](/chromehugo/docs/components/download/public/common/url_download_handler)
  ~~~c
  static UrlDownloadHandler::UniqueUrlDownloadHandlerPtr Create(
      std::unique_ptr<download::DownloadUrlParameters> params,
      base::WeakPtr<download::UrlDownloadHandler::Delegate> delegate,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      const URLSecurityPolicy& url_security_policy,
      mojo::PendingRemote<device::mojom::WakeLockProvider> wake_lock_provider,
      const scoped_refptr<base::SingleThreadTaskRunner>& task_runner);
~~~