---
tags:
  - download
---

## class UrlDownloadHandler::Delegate
 Class to be notified when download starts/stops.

### OnUrlDownloadStopped

Delegate::UrlDownloadHandler::OnUrlDownloadStopped
~~~cpp
virtual void OnUrlDownloadStopped(UrlDownloadHandlerID downloader) = 0;
~~~
 Called after the connection is cancelled or finished.

### OnUrlDownloadHandlerCreated

OnUrlDownloadHandlerCreated
~~~cpp
virtual void OnUrlDownloadHandlerCreated(
        UniqueUrlDownloadHandlerPtr downloader) {}
~~~
 Called when a UrlDownloadHandler is created.

## class UrlDownloadHandler
 Class for handling the download of a url. Implemented by child classes.

### UrlDownloadHandler

UrlDownloadHandler
~~~cpp
UrlDownloadHandler() = default;
~~~

### UrlDownloadHandler

UrlDownloadHandler
~~~cpp
UrlDownloadHandler(const UrlDownloadHandler&) = delete;
~~~

### operator=

UrlDownloadHandler::operator=
~~~cpp
UrlDownloadHandler& operator=(const UrlDownloadHandler&) = delete;
~~~

### ~UrlDownloadHandler

~UrlDownloadHandler
~~~cpp
virtual ~UrlDownloadHandler() = default;
~~~
