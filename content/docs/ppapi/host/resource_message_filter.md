### ResourceMessageFilter

ResourceMessageFilter
~~~cpp
ResourceMessageFilter(const ResourceMessageFilter&) = delete;
~~~

### operator=

operator=
~~~cpp
ResourceMessageFilter& operator=(const ResourceMessageFilter&) = delete;
~~~

### OnFilterAdded

OnFilterAdded
~~~cpp
void OnFilterAdded(ResourceHost* resource_host);
~~~
 Called when a filter is added to a ResourceHost.

### OnFilterDestroyed

OnFilterDestroyed
~~~cpp
virtual void OnFilterDestroyed();
~~~
 Called when a filter is removed from a ResourceHost.

### HandleMessage

HandleMessage
~~~cpp
bool HandleMessage(const IPC::Message& msg,
                     HostMessageContext* context) override;
~~~
 This will dispatch the message handler on the target thread. It returns
 true if the message was handled by this filter and false otherwise.

### SendReply

SendReply
~~~cpp
void SendReply(const ReplyMessageContext& context,
                 const IPC::Message& msg) override;
~~~
 This can be called from any thread.

### resource_host

resource_host
~~~cpp
ResourceHost* resource_host() const { return resource_host_; }
~~~
 Please see the comments of |resource_host_| for on which thread it can be
 used and when it is NULL.

### OverrideTaskRunnerForMessage

OverrideTaskRunnerForMessage
~~~cpp
virtual scoped_refptr<base::SequencedTaskRunner> OverrideTaskRunnerForMessage(
      const IPC::Message& message);
~~~
 If you want the message to be handled on another thread, return a non-null
 task runner which will target tasks accordingly.

### DispatchMessage

DispatchMessage
~~~cpp
void DispatchMessage(const IPC::Message& msg,
                       HostMessageContext context);
~~~
 This method is posted to the target thread and runs the message handler.

## struct ResourceMessageFilterDeleteTraits

### Destruct

ResourceMessageFilterDeleteTraits::Destruct
~~~cpp
static void Destruct(const ResourceMessageFilter* filter);
~~~
