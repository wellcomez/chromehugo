### SingleRequestURLLoaderFactory

SingleRequestURLLoaderFactory
~~~cpp
explicit SingleRequestURLLoaderFactory(RequestHandler handler);

  SingleRequestURLLoaderFactory(const SingleRequestURLLoaderFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
SingleRequestURLLoaderFactory& operator=(
      const SingleRequestURLLoaderFactory&) = delete;
~~~

### CreateLoaderAndStart

CreateLoaderAndStart
~~~cpp
void CreateLoaderAndStart(
      mojo::PendingReceiver<network::mojom::URLLoader> loader,
      int32_t request_id,
      uint32_t options,
      const network::ResourceRequest& request,
      mojo::PendingRemote<network::mojom::URLLoaderClient> client,
      const net::MutableNetworkTrafficAnnotationTag& traffic_annotation)
      override;
~~~
 SharedURLLoaderFactory:
### Clone

Clone
~~~cpp
void Clone(mojo::PendingReceiver<network::mojom::URLLoaderFactory> receiver)
      override;
~~~

### Clone

Clone
~~~cpp
std::unique_ptr<network::PendingSharedURLLoaderFactory> Clone() override;
~~~

### SingleRequestURLLoaderFactory

SingleRequestURLLoaderFactory
~~~cpp
SingleRequestURLLoaderFactory(scoped_refptr<HandlerState> state)
~~~

### ~SingleRequestURLLoaderFactory

~SingleRequestURLLoaderFactory
~~~cpp
~SingleRequestURLLoaderFactory() override
~~~

