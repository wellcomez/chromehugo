# Life of a Navigation

Navigation is one of the main functions of a browser. It is the process through
which the user loads documents. This documentation traces the life of a
navigation from the time a URL is typed in the URL bar to the time the web page
is completely loaded. This is one example of many types of navigations, some of
which may start in different places (e.g., in the renderer process).

See also:

* [Life_of_a_Navigation tech talk](https://youtu.be/mX7jQsGCF6E) and
  [slides](https://docs.google.com/presentation/d/1YVqDmbXI0cllpfXD7TuewiexDNZYfwk6fRdmoXJbBlM/edit),
  for an overview from Chrome University.
* [Navigation Concepts](navigation_concepts.md), for useful notes on
  navigation-related concepts in Chromium.

[TOC]

## BeforeUnload

Once a URL is entered, **the first step of a navigation** is to execute the
`beforeunload event handler `of the `previous document`, if a document is already
loaded. <u>This allows the previous document to prompt the user whether they want
to leave</u>, to avoid losing any unsaved data. In this case, the user can cancel
the navigation and no more work will be performed.

## Network Request and Response

If there is <u>no beforeunload handler registered, or the user agrees to proceed,</u>
the <mark>next step is making a network request </mark>to the specified URL to retrieve the
contents of the document to be rendered. (Note that not all navigations will go
to the actual network, for cases like ServiceWorkers, WebUI, cache, data:, etc.)
Assuming no network error is encountered (e.g. DNS resolution error, socket
connection timeout, etc.), the server will respond with data, with the response
headers coming first. The parsed headers give enough information to determine
what needs to be done next.

The HTTP response code allows the browser process to know whether one of the
following conditions has occurred:

* A successful response follows (2xx)
* A redirect has been encountered (response 3xx)
* An HTTP level error has occurred (response 4xx, 5xx)

There are two cases where a navigation network request can complete without
resulting in a new document being rendered. The first one is HTTP response code
204 or 205, which tells the browser that the response was successful, but there
is no content that follows, and therefore the current document must remain
active. The other case is when the server responds with a `Content-Disposition`
response header <u>indicating that the response must be treated as a download
instead of a navigation</u>.

If the server responds with a redirect, Chromium makes another request based on
the HTTP response code and the Location header. The browser continues following
redirects until either an error or a successful response is encountered.

Once there are no more redirects,` the network stack determines if MIME type
sniffing is needed to detect what type of response the server has sent`. This is
only needed if the response is not a 204/205 nor a download, doesn't already
have a `Content-Type` response header, and doesn’t include a
`X-Content-Type-Options: nosniff` response header. If MIME type sniffing is
needed, the network stack will read a small chunk of the actual response data
before proceeding with the commit.

## Commit

**At this point the response is passed from the network stack to the browser
process to be used for rendering a new document**. `The browser process` selects
`an appropriate renderer process` for the new document `based on the origin and
headers of the response` as well as the current` process model and isolation
policy.` It then sends the response to the chosen process, waiting for it to
create the document and send an acknowledgement. This acknowledgement from the
renderer process marks the _commit_ time, when the browser process changes its
security state to reflect the new document and creates a session history entry
for the previous document.

As part of **creating the new document**, <u>the old document needs to be unloaded</u>.
In navigations that stay in the same renderer process, the old document is
unloaded by Blink before the new document is created, including running any
registered unload handlers. In the case of a navigation that goes
cross-process, any unload handlers are executed in the previous document’s
process concurrently with the creation of the new document in the new process.

Once the creation of the new document is complete and the browser process
receives the commit message from the renderer process, the navigation is
complete.

## Loading

Even once navigation is complete, the user doesn't actually see the new page
yet. Most people use the word navigation to describe the act of moving from
one page to another, but in Chromium we separate that process into two phases.
So far we have described the _navigation_ phase; once the navigation has been
committed, Chromium moves into the _loading_ phase. Loading consists of
reading the remaining response data from the server, parsing it, rendering the
document so it is visible to the user, executing any script accompanying it,
and loading any subresources specified by the document.

The main reason for splitting into these two phases is that errors are treated
differently before and after a navigation commits. Consider the case where the
server responds with an HTTP error code. When this happens, the browser still
commits a new document, but that document is an error page. The error page is
either generated based on the HTTP response code or read as the response data
from the server. On the other hand, if a successful navigation has committed a
real document and has moved to the loading phase, it is still possible to
encounter an error, for example a network connection can be terminated or
times out. In that case the browser displays as much of the new document as it
can, without showing an error page.

## WebContentsObserver

Chromium exposes the various stages of navigation and document loading through
methods on the [<mark>WebContentsObserver</mark>] interface.

### Navigation

* `DidStartNavigation` - invoked after executing the beforeunload event handler
  and before making the initial network request.
* `DidRedirectNavigation` - invoked every time a server redirect is encountered.
* `ReadyToCommitNavigation` - invoked at the time the browser process has
  determined that it will commit the navigation and has picked a renderer
  process for it, but before it has sent it to the renderer process. It is not
  invoked for same-document navigations.
* `DidFinishNavigation` - invoked once the navigation has committed. The commit
  can be either an error page if the server responded with an error code or a
  successful document.

### Loading

* `DidStartLoading` - invoked once per WebContents, when a navigation is about
  to start, after executing the beforeunload handler. This is equivalent to the
  browser UI starting to show a spinner or other visual indicator for
  navigation and is invoked before the DidStartNavigation method for the
  navigation.
* `DOMContentLoaded` - invoked per RenderFrameHost, when the document itself
  has completed loading, but before subresources may have completed loading.
* `DidFinishLoad` - invoked per RenderFrameHost, when the document and all of
  its subresources have finished loading.
* `DidStopLoading` - invoked once per WebContents, when the top-level document,
  all of its subresources, all subframes, and their subresources have completed
  loading. This is equivalent to the browser UI stop showing a spinner or other
  visual indicator for navigation and loading.
* `DidFailLoad` - invoked per RenderFrameHost, when the document load failed,
  for example due to network connection termination before reading all of the
  response data.

### WebContentsObserver 调用

- chrome/browser/extensions/api/web_navigation/web_navigation_api.cc (1 result)
  - [322: void WebNavigationTabObserver::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/extensions/api/web_navigation/web_navigation_api.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=322)
- chrome/browser/history/history_tab_helper.cc (1 result)
  - [403: void HistoryTabHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/history/history_tab_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=403)

- chrome/browser/login_detection/login_detection_tab_helper.cc (1 result)
  - [92: void LoginDetectionTabHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/login_detection/login_detection_tab_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=92)

- chrome/browser/sync/sessions/sync_sessions_router_tab_helper.cc (1 result)
  - [77: void SyncSessionsRouterTabHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/sync/sessions/sync_sessions_router_tab_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=77)

- chrome/browser/tpcd/heuristics/opener_heuristic_tab_helper.cc (1 result)
  - [91: void OpenerHeuristicTabHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/tpcd/heuristics/opener_heuristic_tab_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=91)

- chrome/browser/ui/lens/lens_overlay_side_panel_coordinator.cc (1 result)
  - [150: void LensOverlaySidePanelCoordinator::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/lens/lens_overlay_side_panel_coordinator.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=150)

- chrome/browser/ui/side_search/side_search_side_contents_helper.cc (1 result)
  - [64: void SideSearchSideContentsHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/side_search/side_search_side_contents_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=64)

- chrome/browser/ui/side_search/side_search_tab_contents_helper.cc (1 result)
  
  - [100: void SideSearchTabContentsHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/side_search/side_search_tab_contents_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=100)

- chrome/browser/ui/views/page_info/about_this_site_side_panel_view.cc (1 result)
  
  - [102: void AboutThisSiteSidePanelView::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/views/page_info/about_this_site_side_panel_view.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=102)

- chrome/browser/ui/views/side_panel/companion/companion_tab_helper.cc (1 result)
  
  - [213: void CompanionTabHelper::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/views/side_panel/companion/companion_tab_helper.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=213)

- chrome/browser/ui/views/side_panel/search_companion/companion_side_panel_controller.cc (1 result)
  
  - [254: void CompanionSidePanelController::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/views/side_panel/search_companion/companion_side_panel_controller.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=254)

- components/safe_browsing/content/browser/safe_browsing_navigation_observer.cc (1 result)
  
  - [207: void SafeBrowsingNavigationObserver::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/content/browser/safe_browsing_navigation_observer.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=207)

- components/ukm/content/source_url_recorder.cc (1 result)
  
  - [235: void SourceUrlRecorderWebContentsObserver::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:components/ukm/content/source_url_recorder.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=235)

- content/public/test/mock_web_contents_observer.h (1 result)
  
  - [147: DidOpenRequestedURL,](https://source.chromium.org/chromium/chromium/src/+/main:content/public/test/mock_web_contents_observer.h;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=147)

- content/test/web_contents_observer_consistency_checker.cc (1 result)
  
  - [331: void WebContentsObserverConsistencyChecker::DidOpenRequestedURL(](https://source.chromium.org/chromium/chromium/src/+/main:content/test/web_contents_observer_consistency_checker.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=331)


## NavigationThrottles

`NavigationThrottles` allow `observing`, `deferring`, `blocking`, and `canceling` a given navigation. 

They should not generally be used for modifying a navigation (e.g.,
simulating a redirect), as discussed in [Navigation Concepts](navigation_concepts.md#rules-for-canceling-navigations).
They are typically registered in `NavigationThrottleRunner::RegisterNavigationThrottles` or
`ContentBrowserClient::CreateThrottlesForNavigation`.

The most common NavigationThrottles events are `WillStartRequest`,
`WillRedirectRequest`, and `WillProcessResponse`, which allow intercepting a
navigation before sending the network request, during any redirects, and after
receiving the response. These events are only invoked on navigations that
require a URLLoader (see NavigationRequest::NeedsUrlLoader).
A NavigationThrottle that wishes to intercept a non-URLLoader navigation
(same-document navigations, about:blank, etc.) should register itself in
`NavigationThrottleRunner::RegisterNavigationThrottlesForCommitWithoutUrlLoader`,
and will get a single `WillCommitWithoutUrlLoader` event instead of the full
set of events centered on network requests. Page-activation navigations, such
as activating a prerendered page or restoring a page from the back-forward
cache, skip NavigationThrottles entirely.

[WebContentsObserver]: https://source.chromium.org/chromium/chromium/src/+/main:content/public/browser/web_contents_observer.h




## Modifying a navigation

### NavigationThrottleRunner

### ContentBrowserClient

# [PPT]life of navigation

![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_2018-06.png)


![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_2018-06-1.png)

## ClassList
### URLLoaderRequestController

## [UI Thread]  Begin Navigation UI Thread

This is where the navigation is kicked off, with objects to track it `on the UI and Network stack`.

- `NavigationURLLoader` runs on the UI thread, 
  - creates a `URLLoaderRequestController` which lives on the `IO thread`, 
   
    which in turn  talks 
    
    - to ` the NetworkService`   through an `URLLoader` created by `ThrottlingURLLoader::CreateLoaderAndStart`.

NavigationThrottles are covered in more detail on the next slide

- `NavigationHandle`: tracks navigation until commit (`UI thread`)
- `NavigationURLLoader`(Impl): manages network request (`Network stack`)
- `NavigationThrottles`: interpose on various stages

- How to observe:
  - WebContentsObserver::DidStartLoading()
  - WebContentsObserver::DidStartNavigation(NavigationHandle*)
  
### How to control navigations

Throttles can defer or block navigations at various stages of network request.
They run on the UI thread.

NavigationThrottle
- `WillStartRequest`
- `WillRedirectRequest`
- `WillFailRequest`
- `WillProcessResponse`
  
`PROCEED`, `DEFER`, `CANCEL`, etc + error code
  
Useful for security checks or other asynchronous work that need to be performed before `navigation is allowed` to proceed.



## Network request  

### Start url request

Network stack

Note: Network stack may `later move out of the browser process`, as part of Servicification.
See also Anatomy of a Browser talk and Networking 101 talk.


- `Network stack` running on `IO thread `or `separate process` makes network requests
- Look up IP address via DNS
- Establish TLS connection
- Make HTTP request
- Attach cookies based on destination's origin

### Redirects

IO+UI Thread

Start url request <-> Redirects

- Server may respond by saying to go elsewhere
  - Status code 302, etc.
  - Different from client redirect (a page that commits and then goes elsewhere)
- NavigationThrottles on UI thread can interpose on redirects
  


### Redirects How to observe:
  - WebContentsObserver::`DidRedirectNavigation`(NavigationHandle*)
  - WebContentsObserver::`DidRedirectNavigation`()

![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_2018-06-1.png)




## [Network stack] Response started
- Start receiving data for the new document (probably `HTML`)
- Could be a `download or other non-document` (e.g. 204)
  - Might require MIME type sniffing
  - Can be blocked by Cross-Origin Read Blocking (CORB)
  
- Run additional checks (e.g., SafeBrowsing)

At this point we expect the document to commit and replace previous one

Start url request -> Read response body

## [UI Thread ] Creatation Render 

### Find renderer

- Some pages must share a process
  
  Same-site pages with window references to each other
  
- Other pages should never share

  Internal pages and extensions require privileged processes

- Have flexibility with most pages
  - Swap when possible to get a clean slate (some cross-site cases)
  - Don't swap if it might break scripting between documents
  - If over process limit, reuse compatible/suitable processes
  - Swap when moving cross-site (with Site Isolation)


### SiteInstance


site Isolation ensures that each site gets its own SiteInstance and different SiteInstances do not share process

1. Group of pages that share a process.

2. Sometimes dedicated to one site (e.g., privileged pages).

3. Sometimes can have pages from multiple web sites, to preserve compatibility (e.g., iframes, popups).
  

![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_2018-06-17.png)

####  RenderFrameHost Creatation Swapping processes


- Cross-process navigation: swap to a new `RenderFrameHost`
  
  Old one visible until new one commits its navigation
  >在同一个tab下打开新的网站

- Can speculatively start firing up a new process earlier
  
  >启动发出就预先启render进程
   
- Don't rely on speculative one; it can change before the commit
  
#### Swapping processes How to observe:
  - WebContentsObserver::`ReadyToCommitNavigation`(NavigationHandle*)
  - WebContentsObserver::`ReadyToCommitNavigation`()

![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_findrender.png)


Long term, we want each document change to result in a `RenderFrameHost` swap, however currently multiple documents can commit in the same RenderFrameHost.

RenderFrameHost is associated with a process, therefore `cross-process navigations` need to `swap` RenderFrameHosts.



## Renderer process Commit [DidCommitProvisionalLoad]
### Tell renderer to commit navigation Read response body
Network stack + Renderer process Commit Read response body

- Once renderer has started, tell it the response is ready
- Renderer receives a `Mojo DataPipe` which it uses to read the response body

### Blink's FrameLoader commits navigation
- Blink manages document transition during navigation
  - Blink: rendering engine
  - Much code leftover from old architecture; currently being refactored; will soon go away
- Makes a `DidCommitProvisionalLoad` Mojo interface call to browser
  

## [UI Thread] Browser process Frame has committed navigation

Frame has committed navigation

Hears about commit from `renderer`
 
`Key moment`:
- Finalizes any renderer process swap
- Updates session history, security state, visible URL, etc
    
### Frame has committed How to observe:
  - WebContentsObserver::`DidFinishNavigation`(NavigationHandle*)
    
![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_commit.png)



## [Renderer process Load]  Rendering & subresource requests
- Renderer process renders HTML into a page:
  - DOM tree, layout, style, etc.
  - V8 runs scripts: modify page, respond to events, fetch resources
- Can request images, JavaScript, CSS files, etc
  - Renderer process makes ResourceRequests to browser's network stack
  - Navigation system not involved, unless it's a subframe, which is a navigation

See also Life of a Pixel talk
Note: IPC/Mojo can target receivers on any thread.

Blink does a lot of work after commit time to actually produce pixels of the page and it is all after navigation has completed.


## [UI Thread] Load stop
- After page "finishes" rendering, it notifies browser
  - Main frame and all subframes have run their onload events
  - (There can still be activity after this)

- Throbber stops spinning at this point
  
- How to observe:
  - WebContentsObserver::`DidStopLoading`()
 

## [Renderer process] Leaving a page
- `beforeunload`: Show dialog before user leaves (to avoid data loss)
 
  Browser must ask renderer to run this before BeginNavigation

- `unload`: Cleanup scripts once actually leaving

  For cross-process cases, this runs in the background after commit
  
![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_leavepage.png)
~~~c
    Renderer process
Before --- -> Unload Unload
~~~

More details on next slide, only mention these without spending lots of ti

## Navigating again
- Renderer-initiated navigations
  - Links, forms, scripts
  - (Less trustworthy: bad web pages can try to send you places, but not internal pages)
  

- vs Browser-initiated navigations
  - Omnibox, bookmarks, context menus, etc
  
![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_again.png)


## Subframe Navigations
- Any RenderFrameHost can navigate
  - Even cross-process: Out-of-process iframes
- First navigation in a subframe doesn't create a back/forward item, but later ones do
~~~c
  '---------------
  ' a.com
  '  -------------
  '    b.com
  '  -------------
  '  
  '  
  '---------------
~~~
Mention spec speak - “initial empty document”


## Back/Forward
- Session history
  - `NavigationController` tracks state of all frames in a tab over time
  - `NavigationEntry` is a stop in the back/forward list
  - Can load serialized history items from PageState
  - Persists when closing tabs or quitting Chrome, or via Sync
- Browser history
  - Separate database tracking all tabs (e.g., for omnibox autocomplete
![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_goback.png)

Session History

Highlighted document in frame tree navigates from red page to yellow page.
This clears the subframes, prunes the forward history (blue), and adds a NavEntry for the new state of the frame tree.

## Common Failure Cases
- Navigations can fail for many reasons
  - Network errors
  - Canceled, blocked, etc
  - Not actually a document (e.g., download, 204)
  
## Common Failure Cases How to observe:
  - WebContentsObserver::DidFinishNavigation(NavigationHandle*)
![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_error.png)


## ServiceWorkers
- JS code that can intercept network requests within an origin
  - Useful for offline modes, etc
  - Must run in a renderer process before commit: find/create an appropriate process
 
![](/chromehugo/docs/note/Life_of_a_Navigation_public_-_servicework.png)


# WebNavigation API internals

  [webnavigation-api-internals](https://www.chromium.org/developers/design-documents/webnavigation-api-internals/)

  This document aims at explaining how the `webNavigation API` is tracking `the navigation state of a WebContents` (aka `RenderViewHostDelegate`). 
  If you’re interested in the source, the WebContentsObserver is defined in `src/chrome/browser/extensions/api/web_navigation/web_navigation_api.cc`
### What is the current RenderViewHost?

Contrary to popular belief, a `WebContents` has not a 1:1 relation to a `RenderViewHost`, but in fact, it's a `1:n relation`. A WebContents has` one visible RenderViewHost` at a time, but the RenderViewHost can change during navigation, and the WebContents `keeps around previous RenderViewHosts in case the user comes back to them`. Therefore, a WebContentsObserver has to pay close attention to which RenderViewHost triggered a given signal.

The WebNavigation API `tracks two RenderViewHosts per WebContents`, 
- the current RenderViewHost, 
- and the pending RenderViewHost. 
  
The former is the visible one: 

- the latest RenderViewHost in which a top-level navigation was committed
  
   or, 

- if no such RenderViewHost exists, the first RenderViewHost connected to the WebContents.

`The latter is the latest RenderViewHost` in which a provisional top-level navigation was started `other than the current RenderViewHost`.

We ignore all navigations from RenderViewHosts but those two RenderViewHosts. We also do not observe navigation events from interstitial pages such as SSL certificate errors - `these are rendered with an InterstitialPage as RenderViewHostDelegate`.

Those two RenderViewHosts are determined as follows.

- WebContentsObserver::`AboutToNavigateRenderView`
  
  Every top-level navigation starts with this call. 
  - If `this is the first call`, `the passed in RenderViewHost becomes the current RenderViewHost`, 
  - otherwise, if it isn't the current RenderViewHost, it `becomes the pending RenderViewHost`.

- WebContentsObserver::`DidCommitProvisionalLoadForFrame`
  
  As soon as a top-level navigation is committed`, the corresponding RenderViewHost becomes the current RenderViewHost`. 
  
  We **`discard the pending RenderViewHost`**.

Then

- WebContentsObserver::DidFailProvisionalLoad
 
  If the pending `RenderViewHost fails the provisional load`, we discard it.

- WebContentsObserver::RenderViewGone:
  
  If the current `RenderViewHost crashes`, we discard both RenderViewHosts.

- WebContentsObserver::RenderViewDeleted:
  
  If the pending `RenderViewHost is deleted` for whatever reason, we discard it. If the current RenderViewHost is deleted, we discard it and if there is a pending RenderViewHosts it becomes the current RenderViewHost.

Note that the current RenderViewHost is not only deleted when the tab is closed. The instant search and prerender features can swap in a different WebContents of a tab.

## Navigation of a top-level Frame

For a successful navigation, a frame has to go through several states (see also Adam’s recent presentation “How WebKit works”). Keep in mind that you can receive these signals from any number of RenderViewHosts. You should `ignore all signals but from the current RenderViewHost` or the pending RenderViewHost.

Since a frame is only uniquely identified within a renderer process, and a `WebContents can be a delegate for several RenderViewHosts in different renderer processes`, we need the tuple` (RenderViewHost, frame_id)` to `identify a frame` uniquely within a given WebContents.

1. WebContentsObserver::`DidStartProvisionalLoadForFrame`
    > 没有committed

    At this point, `the URL load is about to start`, but might never commit (invalid URL, download, etc..).` Only when the subsequently triggered resource load actually succeeds and results in a navigation`, we will know what `URL is going to be displayed`.

2. content::`NOTIFICATION_RESOURCE_RECEIVED_REDIRECT`

    While not strictly necessary to track the navigation state, we observe this **notification to determine whether a server-side redirect happened**.

3. WebContentsObserver::`DidCommitProvisionalLoadForFrame`

    > 即使没有 1也会收到 比如   `replace hash`

    At this point, <u>the navigation was committed</u>, i.e. we <u>received the first headers</u>, and an **history entry** was created.

    we will receive this signal `without a prior DidStartProvisionalLoadForFrame` signal.

    - If the navigation only `changed the reference fragment`,
    - or was triggered ` using the history API` (e.g. window.history.replaceState), 
     

- WebContentsObserver::`DocumentLoadedInFrame`
  
  - WebKit `finished parsing the document`. 
  - At this point 
    - `scripts marked as defer were executed`, 
    - `content scripts marked “document_end” get injected` into the frame.

- WebContentsObserver::`DidFinishLoad`

  The navigation is done, i.e. 
    - the spinner of the tab will stop spinning, 
    - and the onload event was dispatched.

  If we’re displaying replacement content, e.g. network error pages, we might see a DidFinishLoad event for a frame which we didn’t see before. It is safe to ignore these events.

## Navigation of a sub-frame

The navigation events for sub-frames do not differ from the events for the top-level frame. 

Sub-frame navigations can start at any time after the provisional load of their parent frame was committed. 

If a sub-frame is part of the parent document (as opposed to created by JavaScript), the `parent frame’s DidFinishLoad signal` will be sent `after all of its sub-frames DidFinishLoad signals`.

It’s `also possible for sub-frames of a frame to send navigation signals` while their `parent frame already started a new provisional load`.

## Navigation failures

A navigation can fail for a number of reasons. Since we’re tracking the state of all frames (and extensions using the webNavigation API might do the same), it is important to not have frames hanging around forever. They should either finish navigation, or fail.

The following is a list of events we observe to determine whether a navigation has failed.

- content::NOTIFICATION_RENDER_VIEW_HOST_DELETED

  When the frame’s RenderViewHost was deleted, the navigation failed.

- WebContentsObserver::AboutToNavigateRenderView

  When the pending RenderViewHost is replaced by a new pending RenderViewHost, all frames navigating in the old pending RenderViewHost fail.

- WebContentsObserver::DidCommitProvisionalLoadForFrame
- 
  If the main frame of the current RenderViewHost commits, all navigations in the pending RenderViewHost fail. If the main frame committed a real load (as opposed to a reference fragment navigation or an history API navigation), all sub frames in the current RenderViewHost fail.

  If a frame in the pending RenderViewHost commits, all navigations in the current RenderViewHost fail.

- WebContentsObserver::DidFailProvisionalLoad

  This error would occur if e.g. the host could not be found.

- WebContentsObserver::DidFailLoad

  This error would occur if e.g. window.stop() is invoked, or the user hits ESC.

- WebContentsObserver::WebContentsDestroyed

All frames we know about fail.