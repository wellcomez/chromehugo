---
tags:
  - download
title: DownloadNote
---

## 下载



### download_manager

- [download_manager](/chromehugo/docs/content/public/browser/download_manager)

- [download_manager_delegate](chromehugo/docs/content/public/browser/download_manager_delegate)

- [download_item_model](/chromehugo/docs/chrome/browser/download/download_item_model)

- [url_download_request_handle](/chromehugo/docs/components/download/public/common/url_download_request_handle)

- [url_download_handler](/chromehugo/docs/components/download/public/common/url_download_handler)
	- [in_progress_download_manager](/chromehugo/docs/components/download/public/common/in_progress_download_manager)
	- [download_worker](/chromehugo/docs/components/download/internal/common/download_worker)
	- [resource_downloader](/chromehugo/docs/components/download/internal/common/resource_downloader)
	- [url_download_handler_factory](/chromehugo/docs/components/download/public/common/url_download_handler_factory)
- [Download Sequence](developers/design-documents/downloadmanagersequences/index)

- [resource_downloader](/chromehugo/docs/components/download/internal/common/resource_downloader)



Chromium是一个复杂的项目，涉及许多不同的组件和文件。下载管理器（Download Manager）是Chromium项目中负责处理下载任务的部分，其代码分布在Chromium源代码的多个文件中。以下是一些可能包含下载管理器相关代码的文件或目录：

- components/download/public/common/
  
  1. **download_request_handle.h/cpp** - 处理下载请求的类定义和实现。
  2. **download_item.h/cpp** - 表示单个下载项的类定义和实现。
  3. **download_manager.h/cpp** - 下载管理器的主要类定义和实现。
  4. **download_manager_delegate.h/cpp** - 下载管理器委托接口，用于处理下载事件。
  5. **download_service.h/cpp** - 可能包含下载服务的接口定义。
  6. **download_file.h/cpp** - 处理下载文件写入的类。
  7. **download_interrupt_reasons.h** - 定义下载中断的原因。
  8. **download_prefs.h/cpp** - 包含下载偏好设置的类。
  9. **download_ui_controller.h/cpp** - 负责下载UI控制的类。

- **components/download/public/** - 包含公共下载组件的头文件和源文件。

- **components/download/internal/**
  
  1. **common/download_item_impl.cc**
     webui的native code

- **NET**
  
  1. **net/disk_cache/disk_cache.h/cpp** - 网络缓存相关的代码，可能与下载过程中的缓存有关。

- **chrome/browser/download/** - 包含Chromium浏览器特定下载功能的实现。

- UI 
  
    **chrome/browser/resource/download/** -  Html。
  
    [browser/resources/downloads](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/resources/downloads/;bpv=1;bpt=0)

- WebUI
  **chrome/browser/ui/webui/downloads**
  
  [BUILD.gn](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/BUILD.gn;bpv=1;bpt=0)  [DIR_METADATA](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/DIR_METADATA;bpv=1;bpt=0)  [OWNERS](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/OWNERS;bpv=1;bpt=0)  
  [downloads.mojom](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads.mojom;bpv=1;bpt=0)  [downloads_dom_handler.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_dom_handler.cc;bpv=1;bpt=0)  [downloads_dom_handler.h](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_dom_handler.h;bpv=1;bpt=0)  
  
  [downloads_dom_handler_unittest.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_dom_handler_unittest.cc;bpv=1;bpt=0)  [downloads_list_tracker.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_list_tracker.cc;bpv=1;bpt=0) 
   [downloads_list_tracker.h](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_list_tracker.h;bpv=1;bpt=0)  [downloads_list_tracker_unittest.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_list_tracker_unittest.cc;bpv=1;bpt=0)  
   [downloads_page_interactive_uitest.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_page_interactive_uitest.cc;bpv=1;bpt=0)  [downloads_ui.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_ui.cc;bpv=1;bpt=0)  
   [downloads_ui.h](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/downloads_ui.h;bpv=1;bpt=0)  [mock_downloads_page.cc](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/mock_downloads_page.cc;bpv=1;bpt=0)  [mock_downloads_page.h](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/ui/webui/downloads/mock_downloads_page.h;bpv=1;bpt=0)
  
  ### down
  
  #### createdownload

- components/download/internal/common/download_item_impl.cc [GetURL](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/download_item_impl.cc;bpv=1;bpt=1;l=868?q=fileExternallyRemoved&ss=chromium%2Fchromium%2Fsrc&gsn=GetURL&gs=KYTHE%3A%2F%2FKvQCCrcBa3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNvbXBvbmVudHMvZG93bmxvYWQvaW50ZXJuYWwvY29tbW9uL2Rvd25sb2FkX2l0ZW1faW1wbC5jYyNwQmF1YzVTb3ZIR09zQ05PN1gwNGs4eUNMR2xBMmwta3N2X0pjOGU3SHVjCrcBa3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNvbXBvbmVudHMvZG93bmxvYWQvaW50ZXJuYWwvY29tbW9uL2Rvd25sb2FkX2l0ZW1faW1wbC5jYyNxZkxRNThhWGZaUmM5WVgwSWZaT2VRRFJRc0hOekNJVWh3Skh4OFpnbUJB)
  
  - chrome/browser/download/download_ui_controller.cc (1 result)
    - [297:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_ui_controller.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=297)[OnDownloadCreated( manager, item)](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_ui_controller.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=287)[{... web_contents->GetURL() != item->GetURL()))) { ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_ui_controller.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=297)
      - components/download/content/public/all_download_item_notifier.cc (1 result)
        - [67:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/content/public/all_download_item_notifier.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=67)[OnDownloadCreated( manager, item)](https://source.chromium.org/chromium/chromium/src/+/main:components/download/content/public/all_download_item_notifier.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=62)[{... observer_->OnDownloadCreated(manager, item); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/content/public/all_download_item_notifier.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=67)
          - content/browser/download/download_manager_impl.cc (2 results)
            - [777:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=777)[CreateNewDownloadItemToStart( info, on_started, callback, id, const duplicate_download_file_path, duplicate_file_exists)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=757)[{... observer.OnDownloadCreated(this, download); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=777)
              - content/browser/download/download_manager_impl.cc (1 result)
            - [1181:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1181)[OnDownloadCreated( download)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1173)[{... observer.OnDownloadCreated(this, item); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1181)
              - content/browser/download/download_manager_impl.cc (3 results)
                - [923:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=923)[CreateSavePackageDownloadItemWithId](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=898)[{... OnDownloadCreated(base::WrapUnique(download_item)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=923)
                - [1169:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1169)[CreateDownloadItem()](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1081)[{... OnDownloadCreated(std::move(item)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1169)
                - [1246:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1246)[ImportInProgressDownloads( id)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1228)[{... OnDownloadCreated(std::move(item));](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1246)

- components/download/internal/background_service/ios/background_download_task_helper.h

- components/download/internal/common/in_progress_download_manager.cc (1 result)
  
  - [585:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=585-588)[StartDownloadWithItem](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=547)[{... download_file.reset(file_factory_->CreateFile( ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=585-588)
    
    - components/download/internal/common/in_progress_download_manager.cc (1 result)
      
      - [539:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=539-543)[StartDownload( info, stream, url_loader_factory_provider, cancel_request_callback, on_started)](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=488)[{... StartDownloadWithItem( ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=539-543)
        
        - components/download/internal/common/in_progress_download_manager.cc (1 result)
          
          - [246:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=246-250)[OnUrlDownloadStarted](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=231)[{... StartDownload(std::move(download_create_info), std::move(input_stream), ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=246-250)
        
        - content/browser/download/download_manager_impl.cc (1 result)
          
          - [814:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=814-817)[StartDownload( info, stream, on_started)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=808)[{... in_progress_manager_->StartDownload( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=814-817)

#### BeginDownloadInternal( params, blob_url_loader_factory, is_new_download, const serialized_embedder_download_data) content/browser/download/download_manager_impl.cc

- content/browser/download/download_manager_impl.cc (1 result)
  - [1497:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1497-1500)[BeginDownloadInternal( params, blob_url_loader_factory, is_new_download, const serialized_embedder_download_data)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1488)[{... CreateInterruptedDownload( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1497-1500)
    - content/browser/download/download_manager_impl.cc (2 results)
      - [934:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=934-936)[ResumeInterruptedDownload( params, const serialized_embedder_download_data)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=931)[{... BeginDownloadInternal(std::move(params), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=934-936)
      - [1068:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1068-1070)[DownloadUrl( params, blob_url_loader_factory)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1053)[{... BeginDownloadInternal(std::move(params), std::move(blob_url_loader_factory), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1068-1070)
        - content/browser/download/download_manager_impl.cc (1 result)
          - [1050:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1050)[DownloadUrl( params)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1048)[{... DownloadUrl(std::move(params), nullptr /* blob_url_loader_factory */); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1050)
            - components/download/content/internal/context_menu_download.cc (1 result)
              - [46:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/content/internal/context_menu_download.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=46)[CreateContextMenuDownload( web_contents, const params, const origin, is_link)](https://source.chromium.org/chromium/chromium/src/+/main:components/download/content/internal/context_menu_download.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=18)[{... dlm->DownloadUrl(std::move(dl_params)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/content/internal/context_menu_download.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=46)
                - chrome/browser/download/android/download_controller.cc (1 result)
                  - [97:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/android/download_controller.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=97)[CreateContextMenuDownloadInternal(const wc_getter, const params, is_link, granted)](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/android/download_controller.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=81)[{... download::CreateContextMenuDownload(web_contents, params, origin, is_link); ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/android/download_controller.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=97)
        - components/download/internal/common/simple_download_manager_coordinator.cc (1 result)
        - content/browser/download/drag_download_file.cc (1 result)
        - content/browser/indexed_db/indexed_db_internals_ui.cc (1 result)
        - content/browser/web_contents/web_contents_impl.cc (1 result)
          - [6060:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_contents/web_contents_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6060)[SaveFrameWithHeaders(const url, const referrer, const headers, const suggested_filename, rfh, is_subresource)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_contents/web_contents_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=5974)[{... GetBrowserContext()->GetDownloadManager()->DownloadUrl(std::move(params)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_contents/web_contents_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6060)
            - chrome/browser/renderer_context_menu/render_view_context_menu.cc (1 result)
            - content/browser/web_contents/web_contents_impl.cc (1 result)
              - [5970:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_contents/web_contents_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=5970-5971)[SaveFrame(const url, const referrer, rfh)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_contents/web_contents_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=5966)[{... SaveFrameWithHeaders(url, referrer, std::string(), std::u16string(), rfh, ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_contents/web_contents_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=5970-5971)
                - components/pdf/browser/pdf_document_helper.cc (1 result)
                - content/browser/web_contents/web_contents_impl.cc (1 result)
                - extensions/browser/guest_view/mime_handler_view/mime_handler_view_guest.cc (1 result)
      - content/browser/renderer_host/render_frame_host_impl.cc (1 result)

- content/browser/download/download_manager_impl.cc (1 result)
  
  - [1497:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1497-1500)[BeginDownloadInternal( params, blob_url_loader_factory, is_new_download, const serialized_embedder_download_data)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1488)[{... CreateInterruptedDownload( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1497-1500)
    - content/browser/download/download_manager_impl.cc (2 results)
      - [934:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=934-936)[ResumeInterruptedDownload( params, const serialized_embedder_download_data)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=931)[{... BeginDownloadInternal(std::move(params), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=934-936)
      - [1068:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1068-1070)[DownloadUrl( params, blob_url_loader_factory)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1053)[{... BeginDownloadInternal(std::move(params), std::move(blob_url_loader_factory), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1068-1070)
        - content/browser/download/download_manager_impl.cc (1 result)
        - content/browser/renderer_host/render_frame_host_impl.cc (1 result)
          - [601:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=601-602)[StartDownload( parameters, blob_url_loader_factory)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=588)[{... download_manager->DownloadUrl(std::move(parameters), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=601-602)
            - content/browser/renderer_host/render_frame_host_impl.cc (2 results)
              - [614:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=614)[OnDataURLRetrieved( parameters, data_url)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=607)[{... StartDownload(std::move(parameters), nullptr); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=614)
              - [6392:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6392)[DownloadURL( blink_parameters)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6317)[{... StartDownload(std::move(parameters), std::move(blob_url_loader_factory)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6392)
                - out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.cc (2 results)
                  - [8969:](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=8969-8970)[Accept( impl, message)](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=7846)[{... impl->DownloadURL( ...}](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=8969-8970)
                  - out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.h (1 result)
                    - [2175:](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.h;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=2175-2176)[Accept( message)](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.h;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=2172)[{... return LocalFrameHostStubDispatch::Accept( ...}](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/third_party/blink/public/mojom/frame/frame.mojom.h;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=2175-2176)
                      
                      #### Download mangaer

- content/browser/download/download_manager_impl.cc
    [DownloadManagerImpl::OnNewDownloadIdRetrieved](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=712?gsn=OnNewDownloadIdRetrieved&gs=KYTHE%3A%2F%2Fkythe%3A%2F%2Fchromium.googlesource.com%2Fcodesearch%2Fchromium%2Fsrc%2F%2Fmain%3Flang%3Dc%252B%252B%3Fpath%3Dcontent%2Fbrowser%2Fdownload%2Fdownload_manager_impl.cc%2320f9Uo9enoa_xBIFK9Sbl4CNEApKGuADd43wAk8cZ-U)

- download manager 外部触发

  - content/browser/download/download_manager_impl.cc (1 result)
    - [1497:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1497-1500)[BeginDownloadInternal( params, blob_url_loader_factory, is_new_download, const serialized_embedder_download_data)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1488)[{... CreateInterruptedDownload( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1497-1500)
      - content/browser/download/download_manager_impl.cc (2 results)
        - [934:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=934-936)[ResumeInterruptedDownload( params, const serialized_embedder_download_data)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=931)[{... BeginDownloadInternal(std::move(params), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=934-936)
          - chrome/browser/download/android/download_controller.cc (1 result)
          - chrome/browser/download/bubble/download_bubble_ui_controller.cc (1 result)
          - chrome/browser/download/simple_download_manager_coordinator_factory.cc (1 result)
          - chrome/browser/extensions/api/downloads/downloads_api.cc (1 result)
          - chrome/browser/extensions/webstore_installer.cc (1 result)
          - chrome/browser/offline_pages/android/downloads/offline_page_download_bridge.cc (1 result)
          - chrome/browser/plugins/plugin_observer.cc (1 result)
          - chrome/browser/renderer_context_menu/render_view_context_menu.cc (1 result)
          - chrome/browser/share/bitmap_download_request.cc (1 result)
          - chrome/browser/ui/views/qrcode_generator/qrcode_generator_bubble.cc (1 result)
          - chrome/browser/ui/views/sharing_hub/screenshot/screenshot_captured_bubble.cc (1 result)
          - chrome/browser/ui/webui/downloads/downloads_dom_handler.cc (1 result)
          - components/download/content/internal/context_menu_download.cc (1 result)
          - components/download/internal/common/simple_download_manager_coordinator.cc (1 result)
          - content/browser/download/drag_download_file.cc (1 result)
          - content/browser/indexed_db/indexed_db_internals_ui.cc (1 result)
          - content/browser/web_contents/web_contents_impl.cc (1 result)
        - [1068:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1068-1070)[DownloadUrl( params, blob_url_loader_factory)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1053)[{... BeginDownloadInternal(std::move(params), std::move(blob_url_loader_factory), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1068-1070)
          - content/browser/download/download_manager_impl.cc (1 result)
            - [1050:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1050)[DownloadUrl( params)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1048)[{... DownloadUrl(std::move(params), nullptr /* blob_url_loader_factory */); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1050)
        - content/browser/renderer_host/render_frame_host_impl.cc (1 result)
            - [601:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=601-602)[StartDownload( parameters, blob_url_loader_factory)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=588)[{... download_manager->DownloadUrl(std::move(parameters), ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=601-602)
              - content/browser/renderer_host/render_frame_host_impl.cc (2 results)
                - [614:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=614)[OnDataURLRetrieved( parameters, data_url)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=607)[{... StartDownload(std::move(parameters), nullptr); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=614)
                - [6392:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6392)[DownloadURL( blink_parameters)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6317)[{... StartDownload(std::move(parameters), std::move(blob_url_loader_factory)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/renderer_host/render_frame_host_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=6392)





- components/download/internal/common/in_progress_download_manager.cc (1 result)
  - [585:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=585-588)[StartDownloadWithItem](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=547)[{... download_file.reset(file_factory_->CreateFile( ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=585-588)
    - components/download/internal/common/in_progress_download_manager.cc (1 result)
      - [539:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=539-543)[StartDownload( info, stream, url_loader_factory_provider, cancel_request_callback, on_started)](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=488)[{... StartDownloadWithItem( ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=539-543)
        - components/download/internal/common/in_progress_download_manager.cc (1 result)
          - [246:](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=246-250)[OnUrlDownloadStarted](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=231)[{... StartDownload(std::move(download_create_info), std::move(input_stream), ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/download/internal/common/in_progress_download_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=246-250)
        - content/browser/download/download_manager_impl.cc (1 result)
          - [814:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=814-817)[StartDownload( info, stream, on_started)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=808)[{... in_progress_manager_->StartDownload( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/download/download_manager_impl.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=814-817)

### 参考答案

[chromium UI库简介-CSDN博客](https://blog.csdn.net/LisztLee/article/details/8246795)

[Chromium-UI项目调研学习（一）-UI术语及DevTools工具 ...](https://blog.csdn.net/qq_37705175/article/details/136913971)

[UI Framework · Chromium_doc_zh](https://ahangchen.gitbooks.io/chromium_doc_zh/content/zh/UI_Framework/)

### 术语

#### Ash

Chrome操作系统的窗口系统环境，负责非浏览器的用户界面，如登录屏幕、系统托盘和各种内置控制面版。与Chrome一样，Ash的用户界面也是使用Views工具包建立的。

#### Arua

：一个跨平台的窗口管理器抽象，主要目标是提供一个跨平台的、高效的窗口管理系统，用于 macOS以外的桌面平台。 Aura 处理的任务包括显示屏幕上的窗口和响应本地输入事件。

#### AX

 ： " 可及性 " 的缩写。许多带有 "AX 前缀的不同类别参与了对视图的可及性信息的暴露。

#### Cocoa：

 macOS 上的应用环境。最初， Chrome for Mac 的用户界面完全是用 Cocoa编写的；现在大多数原生用户界面都使用视图，但 Cocoa 中仍有部分作品。

#### Combox d：

从一个选项列表中显示一个选定的选择，即组合框控件（选择下拉框）。

#### Compositor ：

一个负责为widget制作最终可显示图形的系统，给定一个视图层次结构。合成器管理一个层树，使其能够缓存、转换和混合各种图形层，从而最大限度地减少重新涂抹，加速滚动，并与底层显示刷新率同步动画。

#### HWND：

特定窗口。一个系统原生窗口的不透明解决句柄，用于向各种 Windows 本地 API 传递。

#### Ink drop：

实现对视图的悬停和点击效果的类的集合。使用动态创建的层来绘制半透明的覆盖物，这些覆盖物可能包含在视图的边界内或延伸到视图之外。

#### Focus Ring ：

当物体有焦点时，物体周围的彩色轮廓是可见的，当焦点消失时，物体是隐藏的。它通常是由FocusRing 对象绘制的，这是一个视图子类，可以处理沿着路径绘制彩色边界。在创建焦点环时，重要的是给它们着色，使它们与环内的颜色和环外的颜色形成对比。

#### Ink drop：

实现对视图的悬停和点击效果的类的集合。使用动态创建的层来绘制半透明的覆盖物，这些覆盖物可能包含在视图的边界内或延伸到视图之外。

#### LaCrOS ：

"Linux 和 Chrome 操作系统 " 的缩写。

#### Layer ：

被 Compositor 用于管理纹理涂抹的对象树的一个节点。

#### Layout：

一个视图设置其所有子节点的边界的操作。在一个View上调用InvalidateLayout()会递归地将该View和所有祖先标记为dirty；然后在某个时刻，Widget::LayoutRootViewIfNecessary()会在RootView 上调用 Layout() ，它将在其内容上调用 Layout() ，以此类推在树上，进而通过覆盖虚拟 Layout()方法来实现视图的自定义布局。

#### NativeWidget ：

支持跨平台 Widget 对象的特定平台对象； Widget 通过调用 NativeWidget 上的方法来实现其API 。

#### NS* ：

"NeXTSTEP " 的缩写。该类型是 macOS中一个长期存在的基本类型。

#### Omnibox ：

浏览器窗口工具栏中的组合搜索和地址栏。

#### Progressive Web App (PWA)：

一个可以提供各种类似本地应用程序功能的网站，如可安装性、与系统硬件的集成和 / 或持久性。
SkColor ：一个 32bpp （每个通道 8 比特）的非预乘 ARGB 颜色值，存储为 uint32_t，是视图中与绘画和绘画 API 交互时最常见的颜色类型。

#### Skia ：

Chrome 使用的二维图形库，提供硬件加速的绘图原语。

#### Skia Gold：

一个用于比较像素测试结果图像与验证基线的网络应用程序。浏览器测试或交互式测试中的测试可以选择像素测试，然后产生由 Skia Gold 实例管理的结果图像。
Throbber ：一个动画对象，用于向用户表示明确或不确定的进展。视图中的具体控制是一个 16 个 DIP的旋转圈。

#### Views (library) ：

一个用于创建 " 本地 "用户界面的跨平台用户界面工具包（相对于由网络技术建立的用户界面）。视图对 Chrome 浏览器和 Ash 中的 Chrome 操作系统本地用户界面都很重要。视图是在Chrome 的早期创建的，以抽象出对平台特定功能的调用，并使其有可能为多个操作系统建立Chrome 用户界面；与其他跨平台工具包如 wxWidgets 或 GTK 不同，它旨在实现平台原生的感觉（特别是在Windows上）和最小的API，旨在完成Chrome的需要。

#### View (class) ：

用于表示 UI的对象树中的一个节点。一个视图有矩形的边界，一个（可能是空的）子视图集，以及一个大的 API集，以允许交互和扩展。它的主要目标是抽象出用户的互动，并将数据呈现给浏览器进行显示。

#### Widget ：

代表视图工具包中的 " 窗口 "的跨平台抽象。它是视图库和操作系统平台之间的网关。每个小工具正好包含一个 RootView 对象，该对象持有一个代表该窗口中用户界面的视图层次。 Widget是由一个特定平台的 NativeWidget 轮流实现的。