# sb



![](https://www.chromium.org/developers/design-documents/safebrowsing/csdservice.svg)

![](https://www.chromium.org/developers/design-documents/safebrowsing/chrome_safe_browsing_wo_legend_wo_download.png)


- [chrome](https://source.chromium.org/chromium/chromium/src/+/main:chrome/;bpv=1;bpt=1)[](https://source.chromium.org/chromium/chromium/src/+/main:chrome/;bpv=1;bpt=1 "Project info available. Click to view bugs, yaqs, teams, and more")/[browser](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/;bpv=1;bpt=1)[](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/;bpv=1;bpt=1 "Project info available. Click to view bugs, yaqs, teams, and more")/[safe_browsing](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/;bpv=1;bpt=1)[](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/;bpv=1;bpt=1 "Project info available. Click to view bugs, yaqs, teams, and more")/[download_protection](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/;bpv=1;bpt=1)[](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/;bpv=1;bpt=1 "Project info available. Click to view bugs, yaqs, teams, and more")/[download_url_sb_client.h](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_url_sb_client.h)


### DownloadUrlSBClient

class [DownloadUrlSBClient](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_url_sb_client.h;bpv=1;bpt=1;l=28?q=sbclient&gsn=DownloadUrlSBClient&gs=KYTHE%3A%2F%2FKpQDCscBa3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNocm9tZS9icm93c2VyL3NhZmVfYnJvd3NpbmcvZG93bmxvYWRfcHJvdGVjdGlvbi9kb3dubG9hZF91cmxfc2JfY2xpZW50LmgjRmY2YXVScXEtS0pkNU5OSlJUWDA4ZGZYdE13V1kwWGVkNnpDN2FKQ1FWZwrHAWt5dGhlOi8vY2hyb21pdW0uZ29vZ2xlc291cmNlLmNvbS9jb2Rlc2VhcmNoL2Nocm9taXVtL3NyYy8vbWFpbj9sYW5nPWMlMkIlMkI_cGF0aD1jaHJvbWUvYnJvd3Nlci9zYWZlX2Jyb3dzaW5nL2Rvd25sb2FkX3Byb3RlY3Rpb24vZG93bmxvYWRfdXJsX3NiX2NsaWVudC5oI3hmVnBrYU1fdGQzdUk2eEV6Qzk0dEU0eUFqSFdwbXIybUUxNjFhMndBTzg%3D) : public [SafeBrowsingDatabaseManager](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/database_manager.h;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=58)::[Client](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/database_manager.h;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=64),

~~~cpp
class DownloadUrlSBClient : public SafeBrowsingDatabaseManager::Client,
                            public download::DownloadItem::Observer,
                            public base::RefCountedThreadSafe<
                                DownloadUrlSBClient,
                                content::BrowserThread::DeleteOnUIThread> {
~~~
- void DownloadUrlSBClient::[OnCheckDownloadUrlResult](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_url_sb_client.h;bpv=1;bpt=1;l=52?q=sbclient&gsn=OnCheckDownloadUrlResult&gs=KYTHE%3A%2F%2Fkythe%3A%2F%2Fchromium.googlesource.com%2Fcodesearch%2Fchromium%2Fsrc%2F%2Fmain%3Flang%3Dc%252B%252B%3Fpath%3Dchrome%2Fbrowser%2Fsafe_browsing%2Fdownload_protection%2Fdownload_url_sb_client.h%23ERXvOyI8ec0S-u2BnhNRCZehgsBcZAPTCp73urXrruM)(const std::vector<GURL>& url_chain,
                                SBThreatType threat_type) override;


    - components/safe_browsing/core/browser/db/fake_database_manager.cc (1 result)
      - [148:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/fake_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=148)[CheckDownloadURLAsync(const url_chain, result_threat_type, client)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/fake_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=144)[{... client->OnCheckDownloadUrlResult(url_chain, result_threat_type); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/fake_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=148)
    - components/safe_browsing/core/browser/db/v4_local_database_manager.cc (1 result)
      - [1176:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1176-1177)[RespondToClientWithoutPendingCheckCleanup( check)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1163)[{... check->client->OnCheckDownloadUrlResult(check->urls, ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1176-1177)
        - components/safe_browsing/core/browser/db/v4_local_database_manager.cc (1 result after filtering, 1 displayed)
          - [1147:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1147)[RespondSafeToQueuedAndPendingChecks()](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1125)[{... RespondToClientWithoutPendingCheckCleanup(it); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1147)
  
- chrome/browser/safe_browsing/download_protection/download_url_sb_client.cc (1 result)
  - [64:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_url_sb_client.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=64)[StartCheck()](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_url_sb_client.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=61)[{... database_manager_->CheckDownloadUrl(url_chain_, this)) { ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_url_sb_client.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=64)
    - chrome/browser/safe_browsing/download_protection/download_protection_service.cc (1 result)
      - [300:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_protection_service.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=300)[CheckDownloadUrl( item, callback)](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_protection_service.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=276)[{... client->StartCheck(); ...}

- void DownloadProtectionService::CheckDownloadUrl
  
   Checks whether any of the URLs in the redirect chain of the
   download match the SafeBrowsing bad binary URL list.  The result is
   delivered asynchronously via the given callback.  This method must be
   called on the UI thread, and the callback will also be invoked on the UI
   thread.  Pre-condition: !info.download_url_chain.empty().

   void [DownloadProtectionService](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_protection_service.h;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=68)::[CheckDownloadUrl](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/safe_browsing/download_protection/download_protection_service.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=276?gsn=CheckDownloadUrl&gs=KYTHE%3A%2F%2FKqADCs0Ba3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNocm9tZS9icm93c2VyL3NhZmVfYnJvd3NpbmcvZG93bmxvYWRfcHJvdGVjdGlvbi9kb3dubG9hZF9wcm90ZWN0aW9uX3NlcnZpY2UuY2MjMUNBNmhycVpCdi03djY5c0ZkNnJkb01faHZ6SWFMZnlKUnVReW9DZFVDVQrNAWt5dGhlOi8vY2hyb21pdW0uZ29vZ2xlc291cmNlLmNvbS9jb2Rlc2VhcmNoL2Nocm9taXVtL3NyYy8vbWFpbj9sYW5nPWMlMkIlMkI_cGF0aD1jaHJvbWUvYnJvd3Nlci9zYWZlX2Jyb3dzaW5nL2Rvd25sb2FkX3Byb3RlY3Rpb24vZG93bmxvYWRfcHJvdGVjdGlvbl9zZXJ2aWNlLmNjIzFGWmw1R095R0pDOEZ6WW82NjN1THN4bHNlSjVGLTNuUTVUZXFCaFphWHc%3D)()

  - chrome/browser/download/download_target_determiner.cc (1 result)
  - [941:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_target_determiner.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=941-944)[DoCheckDownloadUrl()](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_target_determiner.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=932)[{... delegate_->CheckDownloadUrl( ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_target_determiner.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=941-944)
    - chrome/browser/download/download_target_determiner.cc (1 result)
      - [198:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_target_determiner.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=198)[DoLoop()](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_target_determiner.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=163)[{... result = DoCheckDownloadUrl(); ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/download/download_target_determiner.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=198)
        - chrome/browser/download/download_target_determiner.cc (13 results)
~~~cpp
void DownloadTargetDeterminer::DoLoop() {
  Result result = CONTINUE;
  do {
    State current_state = next_state_;
    next_state_ = STATE_NONE;

    switch (current_state) {
      case STATE_GENERATE_TARGET_PATH:
        result = DoGenerateTargetPath();
        break;
      case STATE_SET_INSECURE_DOWNLOAD_STATUS:
        result = DoSetInsecureDownloadStatus();
        break;
      case STATE_NOTIFY_EXTENSIONS:
        result = DoNotifyExtensions();
        break;
      case STATE_RESERVE_VIRTUAL_PATH:
        result = DoReserveVirtualPath();
        break;
      case STATE_PROMPT_USER_FOR_DOWNLOAD_PATH:
        result = DoRequestConfirmation();
        break;
      case STATE_DETERMINE_LOCAL_PATH:
        result = DoDetermineLocalPath();
        break;
      case STATE_DETERMINE_MIME_TYPE:
        result = DoDetermineMimeType();
        break;
      case STATE_DETERMINE_IF_HANDLED_SAFELY_BY_BROWSER:
        result = DoDetermineIfHandledSafely();
        break;
      case STATE_DETERMINE_IF_ADOBE_READER_UP_TO_DATE:
        result = DoDetermineIfAdobeReaderUpToDate();
        break;
      case STATE_CHECK_DOWNLOAD_URL:
        result = DoCheckDownloadUrl();
        break;
      case STATE_CHECK_VISITED_REFERRER_BEFORE:
        result = DoCheckVisitedReferrerBefore();
        break;
      case STATE_DETERMINE_INTERMEDIATE_PATH:
        result = DoDetermineIntermediatePath();
        break;
      case STATE_NONE:
        NOTREACHED_IN_MIGRATION();
        return;
    }
  } while (result == CONTINUE);
  // Note that if a callback completes synchronously, the handler will still
  // return QUIT_DOLOOP. In this case, an inner DoLoop() may complete the target
  // determination and delete |this|.

  if (result == COMPLETE)
    ScheduleCallbackAndDeleteSelf(download::DOWNLOAD_INTERRUPT_REASON_NONE);
}
~~~
- void V4LocalDatabaseManager::RespondToClientWithoutPendingCheckCleanup
    void [V4LocalDatabaseManager](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.h;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=34)::[RespondToClientWithoutPendingCheckCleanup](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=1f14cc876cc5bf899d13284a12c451498219bb2d;bpv=1;bpt=1;l=1163?gsn=RespondToClientWithoutPendingCheckCleanup&gs=KYTHE%3A%2F%2FKowDCsMBa3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNvbXBvbmVudHMvc2FmZV9icm93c2luZy9jb3JlL2Jyb3dzZXIvZGIvdjRfbG9jYWxfZGF0YWJhc2VfbWFuYWdlci5jYyM4YnctOGZ6Q212MG5pRDVsVy15N3FlNVRBanNWVkZhUnYyaU9NVnd2aDFvCsMBa3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNvbXBvbmVudHMvc2FmZV9icm93c2luZy9jb3JlL2Jyb3dzZXIvZGIvdjRfbG9jYWxfZGF0YWJhc2VfbWFuYWdlci5jYyNEeFRaUWRGelB5VHAzUDFVd3NjUG96eExleTd6VHZVYk0xbDBta3NxRldF)

  - components/safe_browsing/core/browser/db/v4_local_database_manager.cc (2 results)
    - [1147:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1147)[RespondSafeToQueuedAndPendingChecks()](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1125)[{... RespondToClientWithoutPendingCheckCleanup(it); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1147)
    - [1161:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1161)[RespondToClient( check)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1159)[{... RespondToClientWithoutPendingCheckCleanup(check.get()); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1161)
      - components/safe_browsing/core/browser/db/v4_local_database_manager.cc (5 results)
        - [898:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=898)[HandleAllowlistCheckContinuation( check, allow_async_full_hash_check, callback, match, results)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=826)[{... RespondToClient(std::move(check)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=898)
        - [955:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=955)[HandleCheckContinuation( check, match, results)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=927)[{... RespondToClient(std::move(check)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=955)
        - [1053:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1053)[OnFullHashResponse( check, const full_hash_infos)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1031)[{... RespondToClient(std::move(check)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1053)
        - [1119:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1119)[ProcessQueuedChecksContinuation( check, results)](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1097)[{... RespondToClient(std::move(check)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1119)
        - [1132:](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1132)[RespondSafeToQueuedAndPendingChecks()](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1125)[{... RespondToClient(std::move(it)); ...}](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=1132)
- void V4LocalDatabaseManager::HandleAllowlistCheckContinuation(
    std::unique_ptr<PendingCheck> check,
    bool allow_async_full_hash_check,
    base::OnceCallback<void(bool)> callback,
    AsyncMatch* match,
    FullHashToStoreAndHashPrefixesMap results) 

    void [V4LocalDatabaseManager](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.h;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=34)::[HandleAllowlistCheckContinuation](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/core/browser/db/v4_local_database_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=826?gsn=HandleAllowlistCheckContinuation&gs=KYTHE%3A%2F%2Fkythe%3A%2F%2Fchromium.googlesource.com%2Fcodesearch%2Fchromium%2Fsrc%2F%2Fmain%3Flang%3Dc%252B%252B%3Fpath%3Dcomponents%2Fsafe_browsing%2Fcore%2Fbrowser%2Fdb%2Fv4_local_database_manager.cc%23jWqbVjMEqH0wqkcOB8SQHKLc1AWfbjir5oKdGogu2gc)


- void SafeBrowsingUrlCheckerAsyncWaiter::CheckUrl
  
    void [SafeBrowsingUrlCheckerAsyncWaiter](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/components/safe_browsing/core/common/safe_browsing_url_checker.mojom-test-utils.h;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=20)::[CheckUrl](https://source.chromium.org/chromium/chromium/src/+/main:out/linux-Debug/gen/components/safe_browsing/core/common/safe_browsing_url_checker.mojom.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=424?gsn=CheckUrl&gs=KYTHE%3A%2F%2Fkythe%3A%2F%2Fchromium.googlesource.com%2Fcodesearch%2Fchromium%2Fsrc%2F%2Fmain%3Flang%3Dc%252B%252B%3Fpath%3Dout%2Flinux-Debug%2Fgen%2Fcomponents%2Fsafe_browsing%2Fcore%2Fcommon%2Fsafe_browsing_url_checker.mojom.cc%23csjo7JhijeavHVPbBd0DNBa21NxCGeT7fGeaC87tyfE)


### BrowserURLLoaderThrottle
- void BrowserURLLoaderThrottle::WillStartRequest
  `BrowserURLLoaderThrottle` 是浏览器中的一个接口，用于控制浏览器中 URL 加载器的速度。它通常用于限制浏览器在短时间内发送过多的请求，从而避免对服务器造成不必要的压力。
  `BrowserURLLoaderThrottle` 接口提供了以下方法：
  1. `startLoading(Request request)`：当浏览器开始加载一个请求时调用。
  2. `loadingFinished(Request request, Response response)`：当浏览器加载完成一个请求时调用。
  3. `loadingFailed(Request request, Error error)`：当浏览器加载失败一个请求时调用。


  
  void [BrowserURLLoaderThrottle](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/content/browser/browser_url_loader_throttle.h;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=44)::[WillStartRequest](https://source.chromium.org/chromium/chromium/src/+/main:components/safe_browsing/content/browser/browser_url_loader_throttle.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=164?gsn=WillStartRequest&gs=KYTHE%3A%2F%2FKpADCsUBa3l0aGU6Ly9jaHJvbWl1bS5nb29nbGVzb3VyY2UuY29tL2NvZGVzZWFyY2gvY2hyb21pdW0vc3JjLy9tYWluP2xhbmc9YyUyQiUyQj9wYXRoPWNvbXBvbmVudHMvc2FmZV9icm93c2luZy9jb250ZW50L2Jyb3dzZXIvYnJvd3Nlcl91cmxfbG9hZGVyX3Rocm90dGxlLmNjI1RfcS1MbDZJRkNnek1HN1hNZkZ1WGhCaFY4a0hPbUN3UW5MeFRyeDNURGMKxQFreXRoZTovL2Nocm9taXVtLmdvb2dsZXNvdXJjZS5jb20vY29kZXNlYXJjaC9jaHJvbWl1bS9zcmMvL21haW4_bGFuZz1jJTJCJTJCP3BhdGg9Y29tcG9uZW50cy9zYWZlX2Jyb3dzaW5nL2NvbnRlbnQvYnJvd3Nlci9icm93c2VyX3VybF9sb2FkZXJfdGhyb3R0bGUuY2MjWUhpODNpcXMyUEh3amJjZldudk4xdXNQTnZWUUVnM093TXQwWVBuNVY0MA%3D%3D)
  
  - chrome/browser/preloading/prefetch/search_prefetch/search_prefetch_request.cc (1 result)
    - [284:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/preloading/prefetch/search_prefetch/search_prefetch_request.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=284)[StartPrefetchRequest( profile)](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/preloading/prefetch/search_prefetch/search_prefetch_request.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=188)[{... throttle->WillStartRequest(resource_request.get(), &should_defer); ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/preloading/prefetch/search_prefetch/search_prefetch_request.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=284)
  - third_party/blink/common/loader/throttling_url_loader.cc (1 result)
    - [449:](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/common/loader/throttling_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=449)[Start( factory, request_id, options, url_request, task_runner, cors_exempt_header_list)](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/common/loader/throttling_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=416)[{... throttle->WillStartRequest(url_request, &throttle_deferred); ...}](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/common/loader/throttling_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=449)
      - third_party/blink/common/loader/throttling_url_loader.cc (1 result)
        - [276:](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/common/loader/throttling_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=276-277)[CreateLoaderAndStart](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/common/loader/throttling_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=261)[{... loader->Start(std::move(factory), request_id, options, url_request, ...}](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/common/loader/throttling_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=276-277)
          - chrome/browser/predictors/prefetch_manager.cc (1 result)
            - [314:](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/predictors/prefetch_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=314-319)[PrefetchUrl( job, factory)](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/predictors/prefetch_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=223)[{... blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/predictors/prefetch_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=314-319)
          - content/browser/[loader](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader;bpv=1;bpt=1)/keep_alive_url_loader.cc (1 result)
            - [348:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/keep_alive_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=348-355)[Start()](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/keep_alive_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=329)[{... url_loader_ = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/keep_alive_url_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=348-355)
          - content/browser/loader/navigation_early_hints_manager.cc (1 result)
            - [542:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/navigation_early_hints_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=542-547)[MaybePreloadHintedResource](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/navigation_early_hints_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=481)[{... auto loader = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/navigation_early_hints_manager.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=542-547)
          - content/browser/loader/navigation_url_loader_impl.cc (1 result)
            - [918:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/navigation_url_loader_impl.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=918-922)[CreateThrottlingLoaderAndStart](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/navigation_url_loader_impl.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=899)[{... url_loader_ = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/loader/navigation_url_loader_impl.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=918-922)
          - content/browser/service_worker/service_worker_new_script_loader.cc (1 result)
            - [183:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/service_worker/service_worker_new_script_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=183-187)[(ctor)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/service_worker/service_worker_new_script_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=85)[{... network_loader_ = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/service_worker/service_worker_new_script_loader.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=183-187)
          - content/browser/service_worker/service_worker_single_script_update_checker.cc (1 result)
            - [201:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/service_worker/service_worker_single_script_update_checker.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=201-206)[(ctor)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/service_worker/service_worker_single_script_update_checker.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=107)[{... network_loader_ = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/service_worker/service_worker_single_script_update_checker.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=201-206)
          - content/browser/web_package/signed_exchange_cert_fetcher.cc (1 result)
            - [162:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_package/signed_exchange_cert_fetcher.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=162-167)[Start()](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_package/signed_exchange_cert_fetcher.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=148)[{... url_loader_ = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/web_package/signed_exchange_cert_fetcher.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=162-167)
          - content/browser/worker_host/worker_script_fetcher.cc (1 result)
            - [643:](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/worker_host/worker_script_fetcher.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=643-647)[Start( throttles)](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/worker_host/worker_script_fetcher.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=635)[{... url_loader_ = blink::ThrottlingURLLoader::CreateLoaderAndStart( ...}](https://source.chromium.org/chromium/chromium/src/+/main:content/browser/worker_host/worker_script_fetcher.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=643-647)
          - third_party/blink/renderer/platform/loader/fetch/url_loader/resource_request_sender.cc (1 result)
            - [313:](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/renderer/platform/loader/fetch/url_loader/resource_request_sender.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=313-317)[SendAsync](https://source.chromium.org/chromium/chromium/src/+/main:third_party/blink/renderer/platform/loader/fetch/url_loader/resource_request_sender.cc;drc=197cb5f6af77ae63d1676eeb61a806a7fef06e6d;bpv=1;bpt=1;l=248)


### Safe Browsing in //components
[Android WebView Safe Browsing
](https://chromium.googlesource.com/chromium/src.git/+/master/android_webview/browser/safe_browsing/README.md)

Safe Browsing has many components; for brevity, we will only discuss the ones most relevant to WebView. Based on Safe Browsing’s version (v4 or v5) we have different lookup mechanisms to check if the URL is safe, it all starts from BrowserUrlLoaderThrottle::WillStartRequest which creates the Safe Browsing checker to start the check for url, that checker creates the appropriate lookup mechanism based on some conditions, the following diagram shows the flow in a bigger picture:

Code Overview

SafeBrowsingLookupMechanism is the base class and we have three implementations for it, hash_realtime == V5 (send partial hash of the URL to the server through a proxy), url_realtime == Protego (send URL to the server), hash_database == V4 (check against the local blocklist), worth mentioning that SafeBrowsingLookupMechanismRunner controls LookUpMechanism and sets a timeout for the mechanism to run within. The timeout is defined at the top of the class.

When the check is required in the remote DB, it delegates the call to ApiHandlerBridge, which uses JNI to call StartURLCheck on SafeBrowsingApiBridge. ApiBridge uses the SafetyNetApiHandler (Soon to be SafeBrowsingApiHandler) to make the startUriLookup call, the next section talks about WebView specifics.

See the relevant Chromium classes in //components/safe_browsing/.