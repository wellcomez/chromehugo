## 代码分析
### SSLConnectJob
SSLConnectJob establishes a connection, through a proxy if needed, and then
handles the SSL handshake. It returns an SSLClientSocket on success.
~~~c
class NET_EXPORT_PRIVATE SSLConnectJob : public ConnectJob,
                                         public ConnectJob::Delegate 
~~~

### TransportConnectJob
TransportConnectJob handles the host resolution necessary for socket creation
and the transport (likely TCP) connect. TransportConnectJob also has fallback
logic for IPv6 connect() timeouts (which may happen due to networks / routers
with broken IPv6 support). Those timeouts take 20s, so rather than make the
user wait 20s for the timeout to fire, we use a fallback timer
(kIPv6FallbackTime) and start a connect() to a IPv4 address if the timer
fires. Then we race the IPv4 connect() against the IPv6 connect() (which has
a headstart) and return the one that completes first to the socket pool.

~~~c
class NET_EXPORT_PRIVATE TransportConnectJob : public ConnectJob 
~~~

### HttpProxyConnectJob
~~~c
 HttpProxyConnectJob optionally establishes a tunnel through the proxy
 server after connecting the underlying transport socket.
class NET_EXPORT_PRIVATE HttpProxyConnectJob : public ConnectJob,
                                               public ConnectJob::Delegate 
~~~

### SOCKSConnectJob
 ` SOCKSConnectJob `  handles establishing a connection to a`  SOCKS4 ` or ` SOCKS5 ` proxy and then sending a handshake to establish a tunnel.
~~~c
class NET_EXPORT_PRIVATE SOCKSConnectJob : public ConnectJob,
                                           public ConnectJob::Delegate {
~~~
                                           
#### SOCKSConnectJob Connect

1. `SOCKSConnectJob::DoSOCKSConnect`  创建 ` std::make_unique<SOCKS5ClientSocket>`
    
    socket_->**Connect**(base::BindOnce(&SOCKSConnectJob::OnIOComplete, base::Unretained(this)));
~~~c
int SOCKSConnectJob::DoSOCKSConnect() {
  next_state_ = STATE_SOCKS_CONNECT_COMPLETE;

  Add a SOCKS connection on top of the tcp socket.
  if (socks_params_->is_socks_v5()) {
    socket_ = std::make_unique<SOCKS5ClientSocket>(
        transport_connect_job_->PassSocket(), socks_params_->destination(),
        socks_params_->traffic_annotation());
  } else {
    auto socks_socket = std::make_unique<SOCKSClientSocket>(
        transport_connect_job_->PassSocket(), socks_params_->destination(),
        socks_params_->network_anonymization_key(), priority(), host_resolver(),
        socks_params_->transport_params()->secure_dns_policy(),
        socks_params_->traffic_annotation());
    socks_socket_ptr_ = socks_socket.get();
    socket_ = std::move(socks_socket);
  }
  transport_connect_job_.reset();
  return socket_->Connect(
      base::BindOnce(&SOCKSConnectJob::OnIOComplete, base::Unretained(this)));
}
~~~


2. `SOCKSConnectJob::DoTransportConnect` 创建  `TransportConnectJob* transport_connect_job_`

    `transport_connect_job_->PassSocket()`==`transport_socket_`

~~~c
int SOCKSConnectJob::DoTransportConnect() {
  DCHECK(!transport_connect_job_);

  next_state_ = STATE_TRANSPORT_CONNECT_COMPLETE;
  transport_connect_job_ = std::make_unique<TransportConnectJob>(
      priority(), socket_tag(), common_connect_job_params(),
      socks_params_->transport_params(), this, &net_log());
  return transport_connect_job_->Connect();
}
~~~
3. `int SOCKS5ClientSocket::DoGreetWrite()` 调用 ` transport_socket_->write` 
    `transport_connect_job_->PassSocket()`==`transport_socket_`

~~~c
int SOCKSConnectJob::DoLoop(int result) {
  DCHECK_NE(next_state_, STATE_NONE);

  int rv = result;
  do {
    State state = next_state_;
    next_state_ = STATE_NONE;
    switch (state) {
      case STATE_TRANSPORT_CONNECT:
        DCHECK_EQ(OK, rv);
        rv = DoTransportConnect();
        break;
      case STATE_TRANSPORT_CONNECT_COMPLETE:
        rv = DoTransportConnectComplete(rv);
        break;
      case STATE_SOCKS_CONNECT:
        DCHECK_EQ(OK, rv);
        rv = DoSOCKSConnect();
        break;
      case STATE_SOCKS_CONNECT_COMPLETE:
        rv = DoSOCKSConnectComplete(rv);
        break;
      default:
        NOTREACHED() << "bad state";
        rv = ERR_FAILED;
        break;
    }
  } while (rv != ERR_IO_PENDING && next_state_ != STATE_NONE);

  return rv;
}
~~~


### TransportConnectJob

TransportConnectJob handles the host resolution necessary for socket creation and the transport (likely TCP) connect. TransportConnectJob also has fallback logic for IPv6 connect() timeouts (which may happen due to networks / routers with broken IPv6 support). Those timeouts take 20s, so rather than make the user wait 20s for the timeout to fire, we use a fallback timer (kIPv6FallbackTime) and start a connect() to a IPv4 address if the timer fires. Then we race the IPv4 connect() against the IPv6 connect() (which has a headstart) and return the one that completes first to the socket pool.
~~~c
class NET_EXPORT_PRIVATE TransportConnectJob : public ConnectJob {
~~~

####  int TransportConnectJob::DoTransportConnect() 
~~~c
int TransportConnectJob::DoTransportConnect() {

 if (!ipv4_addresses.empty()) {
    ipv4_job_ = std::make_unique<TransportConnectSubJob>(
        std::move(ipv4_addresses), this, SUB_JOB_IPV4);
  }
~~~


####  TransportConnectSubJob
- TransportConnectSubJob  :: **Connect**
net/socket/transport_connect_sub_job.cc


~~~c
 int TransportConnectSubJob::DoEndpointLockComplete()
  // If `websocket_endpoint_lock_manager_` is non-null, this class now owns an
  // endpoint lock. Wrap `socket` in a `WebSocketStreamSocket` to take ownership
  // of the lock and release it when the socket goes out of scope. This must
  // happen before any early returns in this method.
  if (parent_job_->websocket_endpoint_lock_manager()) {
    transport_socket_ = std::make_unique<WebSocketStreamSocket>(
        std::move(transport_socket_),
        parent_job_->websocket_endpoint_lock_manager(), CurrentAddress());
  }

  transport_socket_->ApplySocketTag(parent_job_->socket_tag());

  // This use of base::Unretained() is safe because transport_socket_ is
  // destroyed in the destructor.
  return transport_socket_->Connect(base::BindOnce(
      &TransportConnectSubJob::OnIOComplete, base::Unretained(this)));
~~~

### ProxyResolvingClientSocket
This class represents a net::StreamSocket implementation that does proxy
resolution for the provided url before establishing a connection. If there is
a proxy configured, a connection will be established to the proxy.


Constructs a new ProxyResolvingClientSocket. `url`'s host and port specify
where a connection will be established to. The full URL will be only used
for proxy resolution. Caller doesn't need to explicitly sanitize the url,
any sensitive data (like embedded usernames and passwords), and local data
(i.e. reference fragment) will be sanitized by net::ProxyResolutionService
before the url is disclosed to the PAC script. If `use_tls`, this will try
to do a tls connect instead of a regular tcp connect. `network_session`,
`common_connect_job_params`, and `connect_job_factory` must outlive `this`.
~~~c
class COMPONENT_EXPORT(NETWORK_SERVICE) ProxyResolvingClientSocket
    : public net::StreamSocket,
      public net::ConnectJob::Delegate {
 public:

  ProxyResolvingClientSocket(
~~~ 

#### connect call graph
`DoSOCKSConnect ` ->  `connect ` -> `connectinternal `
~~~c
net/socket/socks_connect_job.cc (1 result)
141:
DoLoop( result)
{... rv = DoSOCKSConnect(); ...}
net/socket/socks_connect_job.cc (2 results)104:
  OnIOComplete( result)net/socket/socks_connect_job.cc (1 result)112:
      OnConnectJobComplete( result, job)net/socket/connect_job.cc (1 result)181:
          NotifyDelegateOfCompletion( rv)213:
  ConnectInternal() net/socket/connect_job.cc (1 result)130:
    Connect()
    net/http/http_proxy_connect_job.cc (1 result)
    net/socket/socks_connect_job.cc (1 result)
    net/socket/ssl_connect_job.cc (3 results)
    net/socket/transport_client_socket_pool.cc (2 results)461:
      RequestSocketInternal net/socket/transport_client_socket_pool.cc (3 results) 271:
        RequestSocket net/socket/client_socket_handle.cc (1 result)51:
          Init net/socket/client_socket_pool_manager.cc (1 result)141:
            InitSocketPoolHelper net/socket/client_socket_pool_manager.cc (3 results) 238:
              InitSocketHandleForHttpRequest net/http/http_stream_factory_job.cc (1 result)837:
                  DoInitConnectionImpl() 269:
              InitSocketHandleForWebSocketRequest net/http/http_stream_factory_job.cc (1 result) 830:
                  DoInitConnectionImpl() net/http/http_stream_factory_job.cc (1 result)716:
              PreconnectSocketsForHttpRequest net/http/http_stream_factory_job.cc (1 result) 816:
                  DoInitConnectionImpl() 716:
                    DoInitConnection() net/http/http_stream_factory_job.cc (1 result) 642
~~~

`DoCreateJobs  `->....-> ` DoInitConnectionImpl  `->....->  `connect `
~~~c
 RunLoop net/http/http_stream_factory_job.cc (1 result)837:
  DoInitConnectionImpl() net/http/http_stream_factory_job.cc (1 result)716:
    DoInitConnection() net/http/http_stream_factory_job.cc (1 result) 642: 
        DoLoop( result) net/http/http_stream_factory_job.cc (1 result) 547: 
            RunLoop( result) net/http/http_stream_factory_job.cc (3 results)543:
                OnIOComplete( result)669:
                StartInternal() net/http/http_stream_factory_job.cc (2 results)238: 
                  Start( stream_type) net/http/http_stream_factory_job_controller.cc (3 results)943:
                    DoCreateJobs()947:
                    DoCreateJobs()951:
                    DoCreateJobs()261:
                  Preconnect( num_streams) /http/http_stream_factory_job_controller.cc (2 results)534:
                    OnPreconnectsComplete( job, result)net/http/http_stream_factory_job.cc (1 result)883:
                    DoCreateJobs() net/http/http_stream_factory_job_controller.cc (1 result)1234:
                OnSpdySessionAvailable( spdy_session)
~~~


` DoCreateJobs ` 
~~~c
net/http/http_stream_factory_job_controller.cc (1 result) 744:
DoLoop() net/http/http_stream_factory_job_controller.cc (1 result) 711:
  RunLoop() net/http/http_stream_factory_job_controller.cc (4 results) 215:
    Start() net/http/http_stream_factory.cc (1 result) 243:
      RequestStreamInternal() net/http/http_stream_factory.cc (3 results) 174:
        RequestStream() net/http/http_network_transaction.cc (1 result) 900:
          DoCreateStream() net/http/http_network_transaction.cc (1 result) 777:
            DoLoop() net/http/http_network_transaction.cc (7 results) 244:
              Start() 266: net/http/http_cache_transaction.cc (1 result) 1888:
                 DoSendRequest()
              RestartIgnoringLastError() net/http/http_cache_transaction.cc (1 result) 2903:
                 RestartNetworkRequest()
              RestartWithCertificate()  net/http/http_cache_transaction.cc (1 result) 2918:
              RestartWithAuth(const credentials, callback) 461:
              Read() net/http/http_cache_transaction.cc (2 results)
1969:
              ResumeNetworkStart() net/http/http_cache_transaction.cc (1 result) 641:
                ResumeNetworkStart()
              OnIOComplete()
~~~


外部调用接口 `android void CronetURLRequest::NetworkTasks::Start`
~~~c
components/cronet/cronet_url_request.cc (1 result after filtering, 1 displayed)
319: url_request_->Start();
components/domain_reliability/uploader.cc (1 result after filtering, 1 displayed)
156: it->first->Start();
net/cert_net/cert_net_fetcher_url_request.cc (1 result after filtering, 1 displayed)
548: url_request_->Start();
net/dns/dns_transaction.cc (1 result after filtering, 1 displayed)
633: request_->Start();
net/proxy_resolution/pac_file_fetcher_impl.cc (1 result after filtering, 1 displayed)
209: cur_request_->Start();
net/reporting/reporting_uploader.cc (2 results after filtering, 2 displayed)
177: raw_request->Start();
226: raw_request->Start();
net/url_request/url_request.cc (2 results after filtering, 2 displayed)
1010: Start();
1038: Start();
net/websockets/websocket_stream.cc (1 result after filtering, 1 displayed)
204: url_request_->Start();
remoting/host/token_validator_factory_impl.cc (1 result after filtering, 1 displayed)
113: request_->Start();
services/network/url_loader.cc (2 results after filtering, 2 displayed)
1136: url_request_->Start();
2659: url_request_->Start();
~~~
~~~c
net/url_request/url_request_http_job.cc (2 results)639:
StartTransaction() net/url_request/url_request_http_job.cc (3 results) 506:
  OnGotFirstPartySetMetadata( ) net/url_request/url_request_http_job.cc (1 result) 454:
    Start() net/url_request/url_request.cc (1 result) 710:
      StartJob( job) net/url_request/url_request.cc (5 results) 582:
        Start()components/cronet/cronet_url_request.cc (1 result) 319:
          Start( context, const method, request_headers, upload) components/cronet/cronet_url_request.cc 286 <<<<<< android 开始 >>>>>
          Start()  components/domain_reliability/uploader.cc (1 result) 156:
            UploadReport() components/domain_reliability/context.cc (1 result) 154:
              StartUpload() components/domain_reliability/context.cc (1 result) 118:
                OnUploadAllowedCallbackComplete( allowed)
                  DomainReliabilityContext::OnUploadAllowedCallbackComplete() components/domain_reliability/context.cc 116
          Start() ios/net/crn_http_protocol_handler.mm (2 results) 270:
              HandleStreamEvent( stream, event) 646: Start( base_client) <<ios>>
        BeforeRequestComplete( error) net/url_request/url_request.cc (1 result) 578:
          Start() 632:
        RestartWithJob( job) net/url_request/url_request.cc (1 result)
~~~
~~~c
services/network/url_loader.cc (2 results)
1139:
ScheduleStart() services/network/url_loader.cc (1 result) 967:
  ProcessOutboundSharedStorageInterceptor() services/network/url_loader.cc (1 result) 974:
    ProcessOutboundAttributionInterceptor() services/network/url_loader.cc (2 results) 987:
      ProcessOutboundTrustTokenInterceptor(const request)  services/network/url_loader.cc (2 results) 1101:
        OnDoneBeginningTrustTokenOperation( headers, status) 2662:
ResumeStart() services/network/url_loader.cc:2662 
~~~


### GetSocketPool
~~~c
net/socket/client_socket_pool_manager_impl.cc (1 result)
  86:GetSocketPool(const proxy_chain) net/http/http_network_session.cc (1 result)
    252:GetSocketPool( pool_type, const proxy_chain)
      net/http/http_stream_factory_job.cc (1 result)
      net/socket/client_socket_pool_manager.cc (1 result)
      net/spdy/spdy_test_util_common.cc (1 result)
~~~ 




## 简单URL请求的生命周期

### net 内部逻辑
从某个进程调度数据请求，这将导致在网络服务中创建一个network::URLLoader（在桌面平台上，它通常位于自己的进程中）。URLLoader然后创建一个URLRequest来驱动网络请求。该作业首先检查HTTP缓存，然后创建网络事务对象（如有必要）以实际获取数据。该事务尝试重用可用的连接。如果没有可用的，它将创建一个新的。一旦建立了连接，就会调度HTTP请求，读取并解析响应，返回的结果将备份堆栈并发送给调用方。


### Mojo通过IPC管道将network::ResourceRequest发送到网络进程中的network::URLLoaderFactory
当然，它不是那么简单：-}。
考虑到由网络服务进程以外的进程发出的简单请求。假设这是一个HTTP请求，响应未压缩，缓存中没有匹配的条目，并且套接字池中没有连接到服务器的空闲套接字。
继续使用一个“简单”的URLRequest，下面是关于如何工作的更多细节。
 请求开始于某些（非网络）进程中
总结：
- 在浏览器过程中，network::mojom::NetworkContext接口用于创建network::mojom::URLLoaderFactory。

- 使用者（例如，用于Blink的content::ResourceDispatcher、用于帧导航的content::NavigationURLLoaderImpl或network::SimpleURLLoader）将network::ResourceRequest对象和network::mojom::URLLoaderClient Mojo通道传递给network::mojom::URLLoaderFactory，并告诉它创建和启动network::mojom::URLLoader。
- Mojo通过IPC管道将network::ResourceRequest发送到网络进程中的network::URLLoaderFactory。





Chrome有一个单一的浏览器进程，处理启动和配置其他进程、选项卡管理和导航等，还有多个子进程，这些子进程通常是沙盒式的，除了网络服务（它可以在自己的进程中运行，也可以在浏览器进程中运行以节省RAM）之外，它们本身没有网络访问权限。有多种类型的子进程（渲染器、GPU、插件、网络等）。渲染器进程是布局网页和运行HTML的进程。
浏览器进程创建顶级network::mojom::NetworkContext对象。NetworkContext接口具有特权，只能从浏览器进程访问。浏览器进程使用它创建network::mojom::UrlLoaderFactory，然后可以将其传递给权限较低的进程，以允许它们使用NetworkContext加载资源。为了创建URLLoaderFactory，出于安全和隐私原因，将network::mojom::URLLoaderFactoryParams对象传递给NetworkContext，以配置其他进程不受信任设置的字段.

~~~cpp
network::mojom::URLLoaderFactoryParams

"out/linux-Debug/gen/services/network/public/mojom/network_context.mojom.h"
~~~



其中一个字段是net::IsolationInfo字段，它包括:(Isolation:隔离)
- net::NetworkIsolationKey，用于在网络堆栈中强制实施隐私沙盒，分离不同站点使用的网络资源，以防止跨站点跟踪用户。
- net::SiteForCookies，用于确定要为哪个站点发送SameSite Cookie。SameSite Cookie通过仅当站点是顶级站点时才可访问来防止跨站点攻击。
- 如何跨重定向更新这些值。

无论是在浏览器进程中还是在子进程中，想要发出网络请求的使用者都可以通过某种方式从浏览器进程中获取URLLoaderFactory，在大型network::ResourceRequest对象中组装一组参数，创建一个network::mojom::URLLoaderClient Mojo通道，供network::mojom::URLLoader用于与之对话，然后将它们全部传递给URLLoaderFactory，后者返回一个URLLoader对象，它可以使用该对象来管理网络请求。
URLLoaderFactory在网络服务中设置请求
~~~cpp
URLLoaderFactory::URLLoaderFactory

"out/linux-Debug/gen/services/network/public/mojom/url_loader_factory.mojom-blink.cc"
"out/linux-Debug/gen/services/network/public/mojom/url_loader_factory.mojom.cc"
~~~
总结：
- network::URLLoaderFactory创建network::URLLoader。
- network::URLLoader 使用network::NetworkContext的

#### URLRequestContext创建并启动URLRequest。

~~~cpp
URLLoader

"out/linux-Debug/gen/services/network/public/mojom/url_loader.mojom.h"


const char* URLLoader::MessageToMethodName_(mojo::Message& message) {
#if BUILDFLAG(MOJO_TRACE_ENABLED)
  bool is_response = message.has_flag(mojo::Message::kFlagIsResponse);
  if (!is_response) {
    switch (static_cast<messages::URLLoader>(message.name())) {
      case messages::URLLoader::kFollowRedirect:
            return "Receive network::mojom::URLLoader::FollowRedirect";
      case messages::URLLoader::kSetPriority:
            return "Receive network::mojom::URLLoader::SetPriority";
      case messages::URLLoader::kPauseReadingBodyFromNet:
            return "Receive network::mojom::URLLoader::PauseReadingBodyFromNet";
      case messages::URLLoader::kResumeReadingBodyFromNet:
            return "Receive network::mojom::URLLoader::ResumeReadingBodyFromNet";
    }
  } else {
    switch (static_cast<messages::URLLoader>(message.name())) {
      case messages::URLLoader::kFollowRedirect:
            return "Receive reply network::mojom::URLLoader::FollowRedirect";
      case messages::URLLoader::kSetPriority:
            return "Receive reply network::mojom::URLLoader::SetPriority";
      case messages::URLLoader::kPauseReadingBodyFromNet:
            return "Receive reply network::mojom::URLLoader::PauseReadingBodyFromNet";
      case messages::URLLoader::kResumeReadingBodyFromNet:
            return "Receive reply network::mojom::URLLoader::ResumeReadingBodyFromNet";
    }
  }
  return "Receive unknown mojo message";
#else
  bool is_response = message.has_flag(mojo::Message::kFlagIsResponse);
  if (is_response) {
    return "Receive mojo reply";
  } else {
    return "Receive mojo message";
  }
#endif // BUILDFLAG(MOJO_TRACE_ENABLED)
}

class URLLoader
    : public URLLoaderInterfaceBase {
 public:
  using IPCStableHashFunction = uint32_t(*)();

  static const char Name_[];
  static IPCStableHashFunction MessageToMethodInfo_(mojo::Message& message);
  static const char* MessageToMethodName_(mojo::Message& message);
  static constexpr uint32_t Version_ = 0;
  static constexpr bool PassesAssociatedKinds_ = false;
  static constexpr bool HasUninterruptableMethods_ = false;

  using Base_ = URLLoaderInterfaceBase;
  using Proxy_ = URLLoaderProxy;

  template <typename ImplRefTraits>
  using Stub_ = URLLoaderStub<ImplRefTraits>;

  using RequestValidator_ = URLLoaderRequestValidator;
  using ResponseValidator_ = mojo::PassThroughFilter;
  enum MethodMinVersions : uint32_t {
    kFollowRedirectMinVersion = 0,
    kSetPriorityMinVersion = 0,
    kPauseReadingBodyFromNetMinVersion = 0,
    kResumeReadingBodyFromNetMinVersion = 0,
  };

class URLLoaderStub
    : public mojo::MessageReceiverWithResponderStatus {
 public:
  using ImplPointerType = typename ImplRefTraits::PointerType;

  URLLoaderStub() = default;
  ~URLLoaderStub() override = default;

  void set_sink(ImplPointerType sink) { sink_ = std::move(sink); }
  ImplPointerType& sink() { return sink_; }

  bool Accept(mojo::Message* message) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderStubDispatch::Accept(
        ImplRefTraits::GetRawPointer(&sink_), message);
  }

  bool AcceptWithResponder(
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderStubDispatch::AcceptWithResponder(
        ImplRefTraits::GetRawPointer(&sink_), message, std::move(responder));
  }

 private:
  ImplPointerType sink_;
};
~~~
URLLoaderFactory以及所有网络上下文和大部分网络堆栈都位于网络服务中的单个线程上。它从network::mojom::URLLoaderFactory Mojo管道中获取一个重新构建的ResourceRequest对象，进行一些检查以确保它可以为请求提供服务，如果可以，则创建一个URLLoader，传递请求和与URLLoaderFactory关联的网络上下文。

然后，URLLoader调用NetworkContext的net::URLRequestContext来创建URLRequest。URLRequestContext具有指向通过网络发出请求所需的所有网络堆栈对象的指针，如缓存、cookie存储和主机解析器。URLLoader然后调用**network::ResourceScheduler**，后者可能会根据优先级和其他活动延迟启动请求。最后，ResourceScheduler启动请求。
~~~cpp
network::ResourceScheduler
"services/network/resource_scheduler/resource_scheduler.h"

NetworkContext services/network/network_context.cc::618
    resource_scheduler_ = std::make_unique<ResourceScheduler>();  services/network/network_context.cc::738
~~~

#####  HttpStream
总结:
- `HttpStreamFactory` 创建一个 `HttpStreamFactory::Job`.。
  - `HttpStreamFactory::Job` 调用 `TransportClientSocketPool` 填充 `ClientSocketHandle` 。
  - TransportClientSocketPool 没有空闲套接字，因此它创建并启动 `TransportConnectJob` 。
  - `HttpNetworkTransaction` 调用 `HttpStreamFactory` 以请求 `HttpStream` 。
- `URLRequest` 调用 `URLRequestJobFactory` 来创建 `URLRequestHttpJob` ，这是 `URLRequestJob` 的一个子类，
  - 如果需要然后启动它（历史上，非网络URL方案也通过网络堆栈进行分配，因此有多种作业类型）， `URLRequestHttpJob` 会将cookie 附加到请求。
  - 是否附加 SameSite Cookie 取决于 IsolationInfo的SiteForCookie、URL和 URLRequest的 request_initiator 字段。
- `URLRequestHttpJob` 调用 HttpCache 以创建` HttpCache::Transaction` 。
  缓存会检查具有相同URL和NetworkIsolationKey的条目。
  - 如果没有匹配的条目，`HttpCache::Transaction` 将调用 HttpNetworkLayer 来创建 `HttpNetworkTransaction` ，并透明地包装它。
- `HttpNetworkTransaction` 调用 `HttpStreamFactory` 向服务器请求 HttpStream 。

##### 创建HttpStream
总结：
- `HttpStreamFactory` 创建 HttpStreamFactory::Job 。
- HttpStreamFactory::Job 调用 TransportClientSocketPool 来填充 `ClientSocketHandle` 。
- TransportClientSocketPool 没有空闲套接字，因此它创建并启动 `TransportConnectJob` 。
- TransportConnectJob 创建 `StreamSocket` 并建立连接。
- `TransportClientSocketPool` 将 StreamSocket 放在 `ClientSocketHandle` 中，并调用 `HttpStreamFactory::Job` 。
- `HttpStreamFactory::Job` 创建一个 `HttpBasicStream` ，它拥有 `ClientSocketHandle` 的所有权。
- 它将 `HttpBasicStream` 返回给 `HttpNetworkTransaction` 。


HttpStreamFactory::Job 创建一个 ClientSocketHandle 来保存套接字，一旦连接，并将其传递给 ClientSocketPoolManager 。ClientSocketPoolManager 组装建立连接所需的 TransportSocketParams ，并创建一个组名（“主机：端口”），用于标识可以互换使用的套接字。

ClientSocketPoolManager 将请求定向到 TransportClientSocketPool ，因为没有代理，这是一个HTTP请求。请求被转发到池的ClientSocketPoolBase 的 ClientSocketPoolBaseHelper 。如果还没有空闲连接，并且存在可用的套接字插槽， ClientSocketPoolBaseHelper 将使用前面提到的 params 对象创建一个新的 TransportConnectJob 。如果需要，此作业将通过调用   HostResolverImpl 来执行实际的DNS查找，然后最终建立连接。

连接套接字后，套接字的所有权将传递给ClientSocketHandle。然后通知HttpStreamFactory::Job连接尝试成功，然后它创建一个HttpBasicStream，它拥有ClientSocketHandle的所有权。然后，它将HttpBasicStream的所有权传递回HttpNetworkTransaction。

##### 发送请求并读取响应标头
总结：

`HttpNetworkTransaction` 将请求头提供给 HttpBasicStream ，并告诉它启动请求。
HttpNetworkTransaction 将请求标头传递给 HttpBasicStream，

后者使用 `HttpStreamParser` （最终）**格式化请求标头和正文（如果存在）并将其发送到服务器**。

`HttpStreamParser` **等待接收响应，然后解析HTTP/1。x响应头**，

然后通过 HttpNetworkTransaction 和 HttpCache::Transaction 将它们传递给 URLRequestHttpJob 。

如果需要， URLRequestHttpJob 会保存任何cookie，然后将头传递给URLRequest并传递给 network::URLLoader ，后者通过Mojo管道将数据发送到 network::mojom::URLLoaderClient ，该客户端在创建时传递给 URLLoader。

- `HttpBasicStream` **发送请求**，并等待响应。
- `HttpBasicStream` 将响应头发送回 `HttpNetworkTransaction` 。
- 响应头通过 `URLRequest` 发送到 `network::URLLoader`。
- 然后通过 Mojo 将它们发送到 `network::mojom::URLLoaderClient`。
  
##### 读取Response body通过ipc回传客户短
读取URLLoader 和 URLRequest 
- URLLoader 创建一个`原始Mojo数据管道`，并将一端传递给 `network：：mojom：：URLLoaderClient`。
- URLLoader 从 Mojo数据管道请求 `共享内存缓冲区` 。
- `URLLoader` 告诉 `URLRequest` 写入 `内存缓冲区`，并在数据写入缓冲区时通知管道。
- 重复最后两个步骤，直到请求完成。

读取ipc

1. 无需等待 network::mojom::URLLoaderClient 的回复，network::URLLoade `分配一个原始mojo数据管道`，并将管道的读取端传递给客户端。
2. URLLoader 然后从管道中获取 IPC缓冲区，并通过 URLRequest 向下传递一个64KB的正文读取请求，一直传递到 `HttpStreamParser` 。
3. 一旦读取了一些数据（可能小于64KB），读取的字节数就会返回到 URLLoader， URLLoader 会`告诉Mojo管道读取完成`，
4. 然后从管道请求另一个缓冲区，以继续向其写入数据。
5. 管道可能会施加背压，以限制共享内存缓冲区中一次可以使用的未使用数据量。
6. 重复此过程，直到完全读取响应正文。

##### URLRequest销毁
总结：
- 完成后，network::URLLoaderFactory将删除network::URLLoader，后者将删除URLRequest。
- 销毁期间，HttpNetworkTransaction确定套接字是否可重用，如果是，则通知HttpBasicStream将其返回到套接字池。
当URLRequest通知network::URLLoader请求完成时，URLLoader通过其Mojo管道将消息传递给network::mojom::URLLoaderClient，然后通知URLLoaderFactory销毁URLLoader，这将导致销毁URLRequest并关闭与请求相关的所有Mojo管道。
当HttpNetworkTransaction被拆除时，它会判断套接字是否可重用。如果没有，它会告诉HttpBasicStream关闭套接字。无论哪种方式，ClientSocketHandle都会返回套接字，然后将其返回到套接字池，以供重用，或者让套接字池知道它有另一个可用的套接字插槽。

 对象关系和所有权

[图片]


上图中有几点在视觉上看不清楚：


生成挂起URLRequestJob的过滤器链的方法在URLRequestJob上声明，但它当前唯一的实现是在URLRequestHttpJob上，因此生成显示为从该类发生。

不同类型的HttpTransactions是分层的； i.e. HttpCache::ransaction包含指向HttpTransaction的指针，但指向HttpTransaction的指针通常是HttpNetworkTransaction。