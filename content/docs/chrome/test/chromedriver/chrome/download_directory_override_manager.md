
## class DownloadDirectoryOverrideManager
 Overrides the default download directory, if requested, for the duration
 of the given |DevToolsClient|'s lifetime.

### DownloadDirectoryOverrideManager

DownloadDirectoryOverrideManager::DownloadDirectoryOverrideManager
~~~cpp
explicit DownloadDirectoryOverrideManager(DevToolsClient* client);
~~~

### ~DownloadDirectoryOverrideManager

DownloadDirectoryOverrideManager::~DownloadDirectoryOverrideManager
~~~cpp
~DownloadDirectoryOverrideManager() override;
~~~

### OverrideDownloadDirectoryWhenConnected

DownloadDirectoryOverrideManager::OverrideDownloadDirectoryWhenConnected
~~~cpp
Status OverrideDownloadDirectoryWhenConnected(
      const std::string& new_download_directory);
~~~

### OnConnected

DownloadDirectoryOverrideManager::OnConnected
~~~cpp
Status OnConnected(DevToolsClient* client) override;
~~~
 Overridden from DevToolsEventListener:
### ApplyOverride

DownloadDirectoryOverrideManager::ApplyOverride
~~~cpp
Status ApplyOverride();
~~~

### client_



~~~cpp

raw_ptr<DevToolsClient> client_;

~~~


### is_connected_



~~~cpp

bool is_connected_;

~~~


### download_directory_



~~~cpp

std::unique_ptr<std::string> download_directory_;

~~~


### OverrideDownloadDirectoryWhenConnected

DownloadDirectoryOverrideManager::OverrideDownloadDirectoryWhenConnected
~~~cpp
Status OverrideDownloadDirectoryWhenConnected(
      const std::string& new_download_directory);
~~~

### OnConnected

DownloadDirectoryOverrideManager::OnConnected
~~~cpp
Status OnConnected(DevToolsClient* client) override;
~~~
 Overridden from DevToolsEventListener:
### ApplyOverride

DownloadDirectoryOverrideManager::ApplyOverride
~~~cpp
Status ApplyOverride();
~~~

### client_



~~~cpp

raw_ptr<DevToolsClient> client_;

~~~


### is_connected_



~~~cpp

bool is_connected_;

~~~


### download_directory_



~~~cpp

std::unique_ptr<std::string> download_directory_;

~~~

