### MakeCrxDownloaderFactory

MakeCrxDownloaderFactory
~~~cpp
scoped_refptr<update_client::CrxDownloaderFactory> MakeCrxDownloaderFactory(
    scoped_refptr<update_client::NetworkFetcherFactory>
        network_fetcher_factory) {
  return update_client::MakeCrxDownloaderFactory(network_fetcher_factory);
}
~~~
 Reimplement this function to provide a different download stack for
 downloading CRX payloads.

