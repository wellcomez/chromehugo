
## class ChromeExtensionDownloaderFactory
 namespace network
 This provides a simple static interface for constructing an
 ExtensionDownloader suitable for use from within Chrome.

### CreateForURLLoaderFactory

ChromeExtensionDownloaderFactory::CreateForURLLoaderFactory
~~~cpp
static std::unique_ptr<extensions::ExtensionDownloader>
  CreateForURLLoaderFactory(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      extensions::ExtensionDownloaderDelegate* delegate,
      crx_file::VerifierFormat required_verifier_format,
      const base::FilePath& profile_path = base::FilePath());
~~~
 Creates a downloader with a given "global" url loader factory instance.

 No profile identity is associated with this downloader, which means:

 - when this method is called directly, |file_path| is empty.

 - when this method is called through CreateForProfile, |profile_path| is
   non-empty.


 |profile_path| is used exclusely to support download of extensions through
 the file:// protocol. In practice, it allowlists specific directories the
 the browser has access to.

### CreateForProfile

ChromeExtensionDownloaderFactory::CreateForProfile
~~~cpp
static std::unique_ptr<extensions::ExtensionDownloader> CreateForProfile(
      Profile* profile,
      extensions::ExtensionDownloaderDelegate* delegate);
~~~
 Creates a downloader for a given Profile. This downloader will be able
 to authenticate as the signed-in user in the event that it's asked to
 fetch a protected download.

### CreateForURLLoaderFactory

ChromeExtensionDownloaderFactory::CreateForURLLoaderFactory
~~~cpp
static std::unique_ptr<extensions::ExtensionDownloader>
  CreateForURLLoaderFactory(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      extensions::ExtensionDownloaderDelegate* delegate,
      crx_file::VerifierFormat required_verifier_format,
      const base::FilePath& profile_path = base::FilePath());
~~~
 Creates a downloader with a given "global" url loader factory instance.

 No profile identity is associated with this downloader, which means:

 - when this method is called directly, |file_path| is empty.

 - when this method is called through CreateForProfile, |profile_path| is
   non-empty.


 |profile_path| is used exclusely to support download of extensions through
 the file:// protocol. In practice, it allowlists specific directories the
 the browser has access to.

### CreateForProfile

ChromeExtensionDownloaderFactory::CreateForProfile
~~~cpp
static std::unique_ptr<extensions::ExtensionDownloader> CreateForProfile(
      Profile* profile,
      extensions::ExtensionDownloaderDelegate* delegate);
~~~
 Creates a downloader for a given Profile. This downloader will be able
 to authenticate as the signed-in user in the event that it's asked to
 fetch a protected download.
