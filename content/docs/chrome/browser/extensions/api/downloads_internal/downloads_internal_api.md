
## class DownloadsInternalDetermineFilenameFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloadsInternal.determineFilename",
                             DOWNLOADSINTERNAL_DETERMINEFILENAME)
~~~
### DownloadsInternalDetermineFilenameFunction

DownloadsInternalDetermineFilenameFunction::DownloadsInternalDetermineFilenameFunction
~~~cpp
DownloadsInternalDetermineFilenameFunction();
~~~

### DownloadsInternalDetermineFilenameFunction

DownloadsInternalDetermineFilenameFunction
~~~cpp
DownloadsInternalDetermineFilenameFunction(
      const DownloadsInternalDetermineFilenameFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsInternalDetermineFilenameFunction& operator=(
      const DownloadsInternalDetermineFilenameFunction&) = delete;
~~~

### Run

DownloadsInternalDetermineFilenameFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsInternalDetermineFilenameFunction

DownloadsInternalDetermineFilenameFunction::~DownloadsInternalDetermineFilenameFunction
~~~cpp
~DownloadsInternalDetermineFilenameFunction() override;
~~~

### DownloadsInternalDetermineFilenameFunction

DownloadsInternalDetermineFilenameFunction
~~~cpp
DownloadsInternalDetermineFilenameFunction(
      const DownloadsInternalDetermineFilenameFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsInternalDetermineFilenameFunction& operator=(
      const DownloadsInternalDetermineFilenameFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloadsInternal.determineFilename",
                             DOWNLOADSINTERNAL_DETERMINEFILENAME)
~~~
### Run

DownloadsInternalDetermineFilenameFunction::Run
~~~cpp
ResponseAction Run() override;
~~~
