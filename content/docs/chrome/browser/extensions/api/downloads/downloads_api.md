
## class DownloadedByExtension

### Get

DownloadedByExtension::Get
~~~cpp
static DownloadedByExtension* Get(download::DownloadItem* item);
~~~

### DownloadedByExtension

DownloadedByExtension::DownloadedByExtension
~~~cpp
DownloadedByExtension(download::DownloadItem* item,
                        const std::string& id,
                        const std::string& name);
~~~

### DownloadedByExtension

DownloadedByExtension
~~~cpp
DownloadedByExtension(const DownloadedByExtension&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadedByExtension& operator=(const DownloadedByExtension&) = delete;
~~~

### id

id
~~~cpp
const std::string& id() const { return id_; }
~~~

### name

name
~~~cpp
const std::string& name() const { return name_; }
~~~

### field error



~~~cpp

static const char kKey[];

~~~


### id_



~~~cpp

std::string id_;

~~~


### name_



~~~cpp

std::string name_;

~~~


### DownloadedByExtension

DownloadedByExtension
~~~cpp
DownloadedByExtension(const DownloadedByExtension&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadedByExtension& operator=(const DownloadedByExtension&) = delete;
~~~

### id

id
~~~cpp
const std::string& id() const { return id_; }
~~~

### name

name
~~~cpp
const std::string& name() const { return name_; }
~~~

### Get

DownloadedByExtension::Get
~~~cpp
static DownloadedByExtension* Get(download::DownloadItem* item);
~~~

### field error



~~~cpp

static const char kKey[];

~~~


### id_



~~~cpp

std::string id_;

~~~


### name_



~~~cpp

std::string name_;

~~~


## class DownloadsDownloadFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.download", DOWNLOADS_DOWNLOAD)
~~~
### DownloadsDownloadFunction

DownloadsDownloadFunction::DownloadsDownloadFunction
~~~cpp
DownloadsDownloadFunction();
~~~

### DownloadsDownloadFunction

DownloadsDownloadFunction
~~~cpp
DownloadsDownloadFunction(const DownloadsDownloadFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsDownloadFunction& operator=(const DownloadsDownloadFunction&) =
      delete;
~~~

### Run

DownloadsDownloadFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsDownloadFunction

DownloadsDownloadFunction::~DownloadsDownloadFunction
~~~cpp
~DownloadsDownloadFunction() override;
~~~

### OnStarted

DownloadsDownloadFunction::OnStarted
~~~cpp
void OnStarted(const base::FilePath& creator_suggested_filename,
                 extensions::api::downloads::FilenameConflictAction
                     creator_conflict_action,
                 download::DownloadItem* item,
                 download::DownloadInterruptReason interrupt_reason);
~~~

### DownloadsDownloadFunction

DownloadsDownloadFunction
~~~cpp
DownloadsDownloadFunction(const DownloadsDownloadFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsDownloadFunction& operator=(const DownloadsDownloadFunction&) =
      delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.download", DOWNLOADS_DOWNLOAD)
~~~
### Run

DownloadsDownloadFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### OnStarted

DownloadsDownloadFunction::OnStarted
~~~cpp
void OnStarted(const base::FilePath& creator_suggested_filename,
                 extensions::api::downloads::FilenameConflictAction
                     creator_conflict_action,
                 download::DownloadItem* item,
                 download::DownloadInterruptReason interrupt_reason);
~~~

## class DownloadsSearchFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.search", DOWNLOADS_SEARCH)
~~~
### DownloadsSearchFunction

DownloadsSearchFunction::DownloadsSearchFunction
~~~cpp
DownloadsSearchFunction();
~~~

### DownloadsSearchFunction

DownloadsSearchFunction
~~~cpp
DownloadsSearchFunction(const DownloadsSearchFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsSearchFunction& operator=(const DownloadsSearchFunction&) = delete;
~~~

### Run

DownloadsSearchFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsSearchFunction

DownloadsSearchFunction::~DownloadsSearchFunction
~~~cpp
~DownloadsSearchFunction() override;
~~~

### DownloadsSearchFunction

DownloadsSearchFunction
~~~cpp
DownloadsSearchFunction(const DownloadsSearchFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsSearchFunction& operator=(const DownloadsSearchFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.search", DOWNLOADS_SEARCH)
~~~
### Run

DownloadsSearchFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsPauseFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.pause", DOWNLOADS_PAUSE)
~~~
### DownloadsPauseFunction

DownloadsPauseFunction::DownloadsPauseFunction
~~~cpp
DownloadsPauseFunction();
~~~

### DownloadsPauseFunction

DownloadsPauseFunction
~~~cpp
DownloadsPauseFunction(const DownloadsPauseFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsPauseFunction& operator=(const DownloadsPauseFunction&) = delete;
~~~

### Run

DownloadsPauseFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsPauseFunction

DownloadsPauseFunction::~DownloadsPauseFunction
~~~cpp
~DownloadsPauseFunction() override;
~~~

### DownloadsPauseFunction

DownloadsPauseFunction
~~~cpp
DownloadsPauseFunction(const DownloadsPauseFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsPauseFunction& operator=(const DownloadsPauseFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.pause", DOWNLOADS_PAUSE)
~~~
### Run

DownloadsPauseFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsResumeFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.resume", DOWNLOADS_RESUME)
~~~
### DownloadsResumeFunction

DownloadsResumeFunction::DownloadsResumeFunction
~~~cpp
DownloadsResumeFunction();
~~~

### DownloadsResumeFunction

DownloadsResumeFunction
~~~cpp
DownloadsResumeFunction(const DownloadsResumeFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsResumeFunction& operator=(const DownloadsResumeFunction&) = delete;
~~~

### Run

DownloadsResumeFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsResumeFunction

DownloadsResumeFunction::~DownloadsResumeFunction
~~~cpp
~DownloadsResumeFunction() override;
~~~

### DownloadsResumeFunction

DownloadsResumeFunction
~~~cpp
DownloadsResumeFunction(const DownloadsResumeFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsResumeFunction& operator=(const DownloadsResumeFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.resume", DOWNLOADS_RESUME)
~~~
### Run

DownloadsResumeFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsCancelFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.cancel", DOWNLOADS_CANCEL)
~~~
### DownloadsCancelFunction

DownloadsCancelFunction::DownloadsCancelFunction
~~~cpp
DownloadsCancelFunction();
~~~

### DownloadsCancelFunction

DownloadsCancelFunction
~~~cpp
DownloadsCancelFunction(const DownloadsCancelFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsCancelFunction& operator=(const DownloadsCancelFunction&) = delete;
~~~

### Run

DownloadsCancelFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsCancelFunction

DownloadsCancelFunction::~DownloadsCancelFunction
~~~cpp
~DownloadsCancelFunction() override;
~~~

### DownloadsCancelFunction

DownloadsCancelFunction
~~~cpp
DownloadsCancelFunction(const DownloadsCancelFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsCancelFunction& operator=(const DownloadsCancelFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.cancel", DOWNLOADS_CANCEL)
~~~
### Run

DownloadsCancelFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsEraseFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.erase", DOWNLOADS_ERASE)
~~~
### DownloadsEraseFunction

DownloadsEraseFunction::DownloadsEraseFunction
~~~cpp
DownloadsEraseFunction();
~~~

### DownloadsEraseFunction

DownloadsEraseFunction
~~~cpp
DownloadsEraseFunction(const DownloadsEraseFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsEraseFunction& operator=(const DownloadsEraseFunction&) = delete;
~~~

### Run

DownloadsEraseFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsEraseFunction

DownloadsEraseFunction::~DownloadsEraseFunction
~~~cpp
~DownloadsEraseFunction() override;
~~~

### DownloadsEraseFunction

DownloadsEraseFunction
~~~cpp
DownloadsEraseFunction(const DownloadsEraseFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsEraseFunction& operator=(const DownloadsEraseFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.erase", DOWNLOADS_ERASE)
~~~
### Run

DownloadsEraseFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsRemoveFileFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.removeFile", DOWNLOADS_REMOVEFILE)
~~~
### DownloadsRemoveFileFunction

DownloadsRemoveFileFunction::DownloadsRemoveFileFunction
~~~cpp
DownloadsRemoveFileFunction();
~~~

### DownloadsRemoveFileFunction

DownloadsRemoveFileFunction
~~~cpp
DownloadsRemoveFileFunction(const DownloadsRemoveFileFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsRemoveFileFunction& operator=(const DownloadsRemoveFileFunction&) =
      delete;
~~~

### Run

DownloadsRemoveFileFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsRemoveFileFunction

DownloadsRemoveFileFunction::~DownloadsRemoveFileFunction
~~~cpp
~DownloadsRemoveFileFunction() override;
~~~

### Done

DownloadsRemoveFileFunction::Done
~~~cpp
void Done(bool success);
~~~

### DownloadsRemoveFileFunction

DownloadsRemoveFileFunction
~~~cpp
DownloadsRemoveFileFunction(const DownloadsRemoveFileFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsRemoveFileFunction& operator=(const DownloadsRemoveFileFunction&) =
      delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.removeFile", DOWNLOADS_REMOVEFILE)
~~~
### Run

DownloadsRemoveFileFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### Done

DownloadsRemoveFileFunction::Done
~~~cpp
void Done(bool success);
~~~

## class DownloadsAcceptDangerFunction

### OnPromptCreatedForTesting

OnPromptCreatedForTesting
~~~cpp
static void OnPromptCreatedForTesting(
      OnPromptCreatedCallback* callback) {
    on_prompt_created_ = callback;
  }
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.acceptDanger", DOWNLOADS_ACCEPTDANGER)
~~~
### DownloadsAcceptDangerFunction

DownloadsAcceptDangerFunction::DownloadsAcceptDangerFunction
~~~cpp
DownloadsAcceptDangerFunction();
~~~

### DownloadsAcceptDangerFunction

DownloadsAcceptDangerFunction
~~~cpp
DownloadsAcceptDangerFunction(const DownloadsAcceptDangerFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsAcceptDangerFunction& operator=(
      const DownloadsAcceptDangerFunction&) = delete;
~~~

### Run

DownloadsAcceptDangerFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsAcceptDangerFunction

DownloadsAcceptDangerFunction::~DownloadsAcceptDangerFunction
~~~cpp
~DownloadsAcceptDangerFunction() override;
~~~

### DangerPromptCallback

DownloadsAcceptDangerFunction::DangerPromptCallback
~~~cpp
void DangerPromptCallback(int download_id,
                            DownloadDangerPrompt::Action action);
~~~

### PromptOrWait

DownloadsAcceptDangerFunction::PromptOrWait
~~~cpp
void PromptOrWait(int download_id, int retries);
~~~

### field error



~~~cpp

static OnPromptCreatedCallback* on_prompt_created_;

~~~


### OnPromptCreatedForTesting

OnPromptCreatedForTesting
~~~cpp
static void OnPromptCreatedForTesting(
      OnPromptCreatedCallback* callback) {
    on_prompt_created_ = callback;
  }
~~~

### DownloadsAcceptDangerFunction

DownloadsAcceptDangerFunction
~~~cpp
DownloadsAcceptDangerFunction(const DownloadsAcceptDangerFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsAcceptDangerFunction& operator=(
      const DownloadsAcceptDangerFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.acceptDanger", DOWNLOADS_ACCEPTDANGER)
~~~
### Run

DownloadsAcceptDangerFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### DangerPromptCallback

DownloadsAcceptDangerFunction::DangerPromptCallback
~~~cpp
void DangerPromptCallback(int download_id,
                            DownloadDangerPrompt::Action action);
~~~

### PromptOrWait

DownloadsAcceptDangerFunction::PromptOrWait
~~~cpp
void PromptOrWait(int download_id, int retries);
~~~

### field error



~~~cpp

static OnPromptCreatedCallback* on_prompt_created_;

~~~


## class DownloadsShowFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.show", DOWNLOADS_SHOW)
~~~
### DownloadsShowFunction

DownloadsShowFunction::DownloadsShowFunction
~~~cpp
DownloadsShowFunction();
~~~

### DownloadsShowFunction

DownloadsShowFunction
~~~cpp
DownloadsShowFunction(const DownloadsShowFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsShowFunction& operator=(const DownloadsShowFunction&) = delete;
~~~

### Run

DownloadsShowFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsShowFunction

DownloadsShowFunction::~DownloadsShowFunction
~~~cpp
~DownloadsShowFunction() override;
~~~

### DownloadsShowFunction

DownloadsShowFunction
~~~cpp
DownloadsShowFunction(const DownloadsShowFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsShowFunction& operator=(const DownloadsShowFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.show", DOWNLOADS_SHOW)
~~~
### Run

DownloadsShowFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsShowDefaultFolderFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION(
      "downloads.showDefaultFolder", DOWNLOADS_SHOWDEFAULTFOLDER)
~~~
### DownloadsShowDefaultFolderFunction

DownloadsShowDefaultFolderFunction::DownloadsShowDefaultFolderFunction
~~~cpp
DownloadsShowDefaultFolderFunction();
~~~

### DownloadsShowDefaultFolderFunction

DownloadsShowDefaultFolderFunction
~~~cpp
DownloadsShowDefaultFolderFunction(
      const DownloadsShowDefaultFolderFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsShowDefaultFolderFunction& operator=(
      const DownloadsShowDefaultFolderFunction&) = delete;
~~~

### Run

DownloadsShowDefaultFolderFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsShowDefaultFolderFunction

DownloadsShowDefaultFolderFunction::~DownloadsShowDefaultFolderFunction
~~~cpp
~DownloadsShowDefaultFolderFunction() override;
~~~

### DownloadsShowDefaultFolderFunction

DownloadsShowDefaultFolderFunction
~~~cpp
DownloadsShowDefaultFolderFunction(
      const DownloadsShowDefaultFolderFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsShowDefaultFolderFunction& operator=(
      const DownloadsShowDefaultFolderFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION(
      "downloads.showDefaultFolder", DOWNLOADS_SHOWDEFAULTFOLDER)
~~~
### Run

DownloadsShowDefaultFolderFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsOpenFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.open", DOWNLOADS_OPEN)
~~~
### DownloadsOpenFunction

DownloadsOpenFunction::DownloadsOpenFunction
~~~cpp
DownloadsOpenFunction();
~~~

### DownloadsOpenFunction

DownloadsOpenFunction
~~~cpp
DownloadsOpenFunction(const DownloadsOpenFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsOpenFunction& operator=(const DownloadsOpenFunction&) = delete;
~~~

### Run

DownloadsOpenFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### set_on_prompt_created_cb_for_testing

set_on_prompt_created_cb_for_testing
~~~cpp
static void set_on_prompt_created_cb_for_testing(
      OnPromptCreatedCallback* on_prompt_created_cb) {
    on_prompt_created_cb_ = on_prompt_created_cb;
  }
~~~

### ~DownloadsOpenFunction

DownloadsOpenFunction::~DownloadsOpenFunction
~~~cpp
~DownloadsOpenFunction() override;
~~~

### OpenPromptDone

DownloadsOpenFunction::OpenPromptDone
~~~cpp
void OpenPromptDone(int download_id, bool accept);
~~~

### field error



~~~cpp

static OnPromptCreatedCallback* on_prompt_created_cb_;

~~~


### DownloadsOpenFunction

DownloadsOpenFunction
~~~cpp
DownloadsOpenFunction(const DownloadsOpenFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsOpenFunction& operator=(const DownloadsOpenFunction&) = delete;
~~~

### set_on_prompt_created_cb_for_testing

set_on_prompt_created_cb_for_testing
~~~cpp
static void set_on_prompt_created_cb_for_testing(
      OnPromptCreatedCallback* on_prompt_created_cb) {
    on_prompt_created_cb_ = on_prompt_created_cb;
  }
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.open", DOWNLOADS_OPEN)
~~~
### Run

DownloadsOpenFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### OpenPromptDone

DownloadsOpenFunction::OpenPromptDone
~~~cpp
void OpenPromptDone(int download_id, bool accept);
~~~

### field error



~~~cpp

static OnPromptCreatedCallback* on_prompt_created_cb_;

~~~


## class DownloadsSetShelfEnabledFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.setShelfEnabled",
                             DOWNLOADS_SETSHELFENABLED)
~~~
### DownloadsSetShelfEnabledFunction

DownloadsSetShelfEnabledFunction::DownloadsSetShelfEnabledFunction
~~~cpp
DownloadsSetShelfEnabledFunction();
~~~

### DownloadsSetShelfEnabledFunction

DownloadsSetShelfEnabledFunction
~~~cpp
DownloadsSetShelfEnabledFunction(const DownloadsSetShelfEnabledFunction&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadsSetShelfEnabledFunction& operator=(
      const DownloadsSetShelfEnabledFunction&) = delete;
~~~

### Run

DownloadsSetShelfEnabledFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsSetShelfEnabledFunction

DownloadsSetShelfEnabledFunction::~DownloadsSetShelfEnabledFunction
~~~cpp
~DownloadsSetShelfEnabledFunction() override;
~~~

### DownloadsSetShelfEnabledFunction

DownloadsSetShelfEnabledFunction
~~~cpp
DownloadsSetShelfEnabledFunction(const DownloadsSetShelfEnabledFunction&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadsSetShelfEnabledFunction& operator=(
      const DownloadsSetShelfEnabledFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.setShelfEnabled",
                             DOWNLOADS_SETSHELFENABLED)
~~~
### Run

DownloadsSetShelfEnabledFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsSetUiOptionsFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.setUiOptions", DOWNLOADS_SETUIOPTIONS)
~~~
### DownloadsSetUiOptionsFunction

DownloadsSetUiOptionsFunction::DownloadsSetUiOptionsFunction
~~~cpp
DownloadsSetUiOptionsFunction();
~~~

### DownloadsSetUiOptionsFunction

DownloadsSetUiOptionsFunction
~~~cpp
DownloadsSetUiOptionsFunction(const DownloadsSetUiOptionsFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsSetUiOptionsFunction& operator=(
      const DownloadsSetUiOptionsFunction&) = delete;
~~~

### Run

DownloadsSetUiOptionsFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### ~DownloadsSetUiOptionsFunction

DownloadsSetUiOptionsFunction::~DownloadsSetUiOptionsFunction
~~~cpp
~DownloadsSetUiOptionsFunction() override;
~~~

### DownloadsSetUiOptionsFunction

DownloadsSetUiOptionsFunction
~~~cpp
DownloadsSetUiOptionsFunction(const DownloadsSetUiOptionsFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsSetUiOptionsFunction& operator=(
      const DownloadsSetUiOptionsFunction&) = delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.setUiOptions", DOWNLOADS_SETUIOPTIONS)
~~~
### Run

DownloadsSetUiOptionsFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

## class DownloadsGetFileIconFunction

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.getFileIcon", DOWNLOADS_GETFILEICON)
~~~
### DownloadsGetFileIconFunction

DownloadsGetFileIconFunction::DownloadsGetFileIconFunction
~~~cpp
DownloadsGetFileIconFunction();
~~~

### DownloadsGetFileIconFunction

DownloadsGetFileIconFunction
~~~cpp
DownloadsGetFileIconFunction(const DownloadsGetFileIconFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsGetFileIconFunction& operator=(const DownloadsGetFileIconFunction&) =
      delete;
~~~

### Run

DownloadsGetFileIconFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### SetIconExtractorForTesting

DownloadsGetFileIconFunction::SetIconExtractorForTesting
~~~cpp
void SetIconExtractorForTesting(DownloadFileIconExtractor* extractor);
~~~

### ~DownloadsGetFileIconFunction

DownloadsGetFileIconFunction::~DownloadsGetFileIconFunction
~~~cpp
~DownloadsGetFileIconFunction() override;
~~~

### OnIconURLExtracted

DownloadsGetFileIconFunction::OnIconURLExtracted
~~~cpp
void OnIconURLExtracted(const std::string& url);
~~~

### path_



~~~cpp

base::FilePath path_;

~~~


### icon_extractor_



~~~cpp

std::unique_ptr<DownloadFileIconExtractor> icon_extractor_;

~~~


### DownloadsGetFileIconFunction

DownloadsGetFileIconFunction
~~~cpp
DownloadsGetFileIconFunction(const DownloadsGetFileIconFunction&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsGetFileIconFunction& operator=(const DownloadsGetFileIconFunction&) =
      delete;
~~~

### N


~~~cpp
DECLARE_EXTENSION_FUNCTION("downloads.getFileIcon", DOWNLOADS_GETFILEICON)
~~~
### Run

DownloadsGetFileIconFunction::Run
~~~cpp
ResponseAction Run() override;
~~~

### SetIconExtractorForTesting

DownloadsGetFileIconFunction::SetIconExtractorForTesting
~~~cpp
void SetIconExtractorForTesting(DownloadFileIconExtractor* extractor);
~~~

### OnIconURLExtracted

DownloadsGetFileIconFunction::OnIconURLExtracted
~~~cpp
void OnIconURLExtracted(const std::string& url);
~~~

### path_



~~~cpp

base::FilePath path_;

~~~


### icon_extractor_



~~~cpp

std::unique_ptr<DownloadFileIconExtractor> icon_extractor_;

~~~


## class ExtensionDownloadsEventRouter
 Observes a single DownloadManager and many DownloadItems and dispatches
 onCreated and onErased events.

### SetDetermineFilenameTimeoutSecondsForTesting

ExtensionDownloadsEventRouter::SetDetermineFilenameTimeoutSecondsForTesting
~~~cpp
static void SetDetermineFilenameTimeoutSecondsForTesting(int s);
~~~

### DetermineFilenameInternal

ExtensionDownloadsEventRouter::DetermineFilenameInternal
~~~cpp
static void DetermineFilenameInternal(
      const base::FilePath& filename,
      extensions::api::downloads::FilenameConflictAction conflict_action,
      const std::string& suggesting_extension_id,
      const base::Time& suggesting_install_time,
      const std::string& incumbent_extension_id,
      const base::Time& incumbent_install_time,
      std::string* winner_extension_id,
      base::FilePath* determined_filename,
      extensions::api::downloads::FilenameConflictAction*
        determined_conflict_action,
      extensions::WarningSet* warnings);
~~~
 The logic for how to handle conflicting filename suggestions from multiple
 extensions is split out here for testing.

### DetermineFilename

ExtensionDownloadsEventRouter::DetermineFilename
~~~cpp
static bool DetermineFilename(
      content::BrowserContext* browser_context,
      bool include_incognito,
      const std::string& ext_id,
      int download_id,
      const base::FilePath& filename,
      extensions::api::downloads::FilenameConflictAction conflict_action,
      std::string* error);
~~~
 A downloads.onDeterminingFilename listener has returned. If the extension
 wishes to override the download's filename, then |filename| will be
 non-empty. |filename| will be interpreted as a relative path, appended to
 the default downloads directory. If the extension wishes to overwrite any
 existing files, then |overwrite| will be true. Returns true on success,
 false otherwise.

### ExtensionDownloadsEventRouter

ExtensionDownloadsEventRouter::ExtensionDownloadsEventRouter
~~~cpp
explicit ExtensionDownloadsEventRouter(
      Profile* profile, content::DownloadManager* manager);
~~~

### ExtensionDownloadsEventRouter

ExtensionDownloadsEventRouter
~~~cpp
ExtensionDownloadsEventRouter(const ExtensionDownloadsEventRouter&) = delete;
~~~

### operator=

operator=
~~~cpp
ExtensionDownloadsEventRouter& operator=(
      const ExtensionDownloadsEventRouter&) = delete;
~~~

### ~ExtensionDownloadsEventRouter

ExtensionDownloadsEventRouter::~ExtensionDownloadsEventRouter
~~~cpp
~ExtensionDownloadsEventRouter() override;
~~~

### SetUiEnabled

ExtensionDownloadsEventRouter::SetUiEnabled
~~~cpp
void SetUiEnabled(const extensions::Extension* extension, bool enabled);
~~~

### IsUiEnabled

ExtensionDownloadsEventRouter::IsUiEnabled
~~~cpp
bool IsUiEnabled() const;
~~~

### OnDeterminingFilename

ExtensionDownloadsEventRouter::OnDeterminingFilename
~~~cpp
void OnDeterminingFilename(download::DownloadItem* item,
                             const base::FilePath& suggested_path,
                             FilenameChangedCallback filename_changed);
~~~
 Called by ChromeDownloadManagerDelegate during the filename determination
 process, allows extensions to change the item's target filename. If no
 extension wants to change the target filename, then |filename_changed| will
 be called with an empty filename and the filename determination process
 will continue as normal. If an extension wants to change the target
 filename, then |filename_changed| will be called with the new filename and
 a flag indicating whether the new file should overwrite any old files of
 the same name.

### OnDownloadCreated

ExtensionDownloadsEventRouter::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~
 AllDownloadItemNotifier::Observer.

### OnDownloadUpdated

ExtensionDownloadsEventRouter::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### OnDownloadRemoved

ExtensionDownloadsEventRouter::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### OnListenerRemoved

ExtensionDownloadsEventRouter::OnListenerRemoved
~~~cpp
void OnListenerRemoved(const extensions::EventListenerInfo& details) override;
~~~
 extensions::EventRouter::Observer.

### CheckForHistoryFilesRemoval

ExtensionDownloadsEventRouter::CheckForHistoryFilesRemoval
~~~cpp
void CheckForHistoryFilesRemoval();
~~~

### DispatchEvent

ExtensionDownloadsEventRouter::DispatchEvent
~~~cpp
void DispatchEvent(events::HistogramValue histogram_value,
                     const std::string& event_name,
                     bool include_incognito,
                     Event::WillDispatchCallback will_dispatch_callback,
                     base::Value json_arg);
~~~

### OnExtensionUnloaded

ExtensionDownloadsEventRouter::OnExtensionUnloaded
~~~cpp
void OnExtensionUnloaded(content::BrowserContext* browser_context,
                           const extensions::Extension* extension,
                           extensions::UnloadedExtensionReason reason) override;
~~~
 extensions::ExtensionRegistryObserver.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### notifier_



~~~cpp

download::AllDownloadItemNotifier notifier_;

~~~


### ui_disabling_extensions_



~~~cpp

std::set<const extensions::Extension*> ui_disabling_extensions_;

~~~


### last_checked_removal_



~~~cpp

base::Time last_checked_removal_;

~~~


### extension_registry_observation_



~~~cpp

base::ScopedObservation<extensions::ExtensionRegistry,
                          extensions::ExtensionRegistryObserver>
      extension_registry_observation_{this};

~~~

 Listen to extension unloaded notifications.

### ExtensionDownloadsEventRouter

ExtensionDownloadsEventRouter
~~~cpp
ExtensionDownloadsEventRouter(const ExtensionDownloadsEventRouter&) = delete;
~~~

### operator=

operator=
~~~cpp
ExtensionDownloadsEventRouter& operator=(
      const ExtensionDownloadsEventRouter&) = delete;
~~~

### SetDetermineFilenameTimeoutSecondsForTesting

ExtensionDownloadsEventRouter::SetDetermineFilenameTimeoutSecondsForTesting
~~~cpp
static void SetDetermineFilenameTimeoutSecondsForTesting(int s);
~~~

### DetermineFilenameInternal

ExtensionDownloadsEventRouter::DetermineFilenameInternal
~~~cpp
static void DetermineFilenameInternal(
      const base::FilePath& filename,
      extensions::api::downloads::FilenameConflictAction conflict_action,
      const std::string& suggesting_extension_id,
      const base::Time& suggesting_install_time,
      const std::string& incumbent_extension_id,
      const base::Time& incumbent_install_time,
      std::string* winner_extension_id,
      base::FilePath* determined_filename,
      extensions::api::downloads::FilenameConflictAction*
        determined_conflict_action,
      extensions::WarningSet* warnings);
~~~
 The logic for how to handle conflicting filename suggestions from multiple
 extensions is split out here for testing.

### DetermineFilename

ExtensionDownloadsEventRouter::DetermineFilename
~~~cpp
static bool DetermineFilename(
      content::BrowserContext* browser_context,
      bool include_incognito,
      const std::string& ext_id,
      int download_id,
      const base::FilePath& filename,
      extensions::api::downloads::FilenameConflictAction conflict_action,
      std::string* error);
~~~
 A downloads.onDeterminingFilename listener has returned. If the extension
 wishes to override the download's filename, then |filename| will be
 non-empty. |filename| will be interpreted as a relative path, appended to
 the default downloads directory. If the extension wishes to overwrite any
 existing files, then |overwrite| will be true. Returns true on success,
 false otherwise.

### SetUiEnabled

ExtensionDownloadsEventRouter::SetUiEnabled
~~~cpp
void SetUiEnabled(const extensions::Extension* extension, bool enabled);
~~~

### IsUiEnabled

ExtensionDownloadsEventRouter::IsUiEnabled
~~~cpp
bool IsUiEnabled() const;
~~~

### OnDeterminingFilename

ExtensionDownloadsEventRouter::OnDeterminingFilename
~~~cpp
void OnDeterminingFilename(download::DownloadItem* item,
                             const base::FilePath& suggested_path,
                             FilenameChangedCallback filename_changed);
~~~
 Called by ChromeDownloadManagerDelegate during the filename determination
 process, allows extensions to change the item's target filename. If no
 extension wants to change the target filename, then |filename_changed| will
 be called with an empty filename and the filename determination process
 will continue as normal. If an extension wants to change the target
 filename, then |filename_changed| will be called with the new filename and
 a flag indicating whether the new file should overwrite any old files of
 the same name.

### OnDownloadCreated

ExtensionDownloadsEventRouter::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~
 AllDownloadItemNotifier::Observer.

### OnDownloadUpdated

ExtensionDownloadsEventRouter::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### OnDownloadRemoved

ExtensionDownloadsEventRouter::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### OnListenerRemoved

ExtensionDownloadsEventRouter::OnListenerRemoved
~~~cpp
void OnListenerRemoved(const extensions::EventListenerInfo& details) override;
~~~
 extensions::EventRouter::Observer.

### CheckForHistoryFilesRemoval

ExtensionDownloadsEventRouter::CheckForHistoryFilesRemoval
~~~cpp
void CheckForHistoryFilesRemoval();
~~~

### DispatchEvent

ExtensionDownloadsEventRouter::DispatchEvent
~~~cpp
void DispatchEvent(events::HistogramValue histogram_value,
                     const std::string& event_name,
                     bool include_incognito,
                     Event::WillDispatchCallback will_dispatch_callback,
                     base::Value json_arg);
~~~

### OnExtensionUnloaded

ExtensionDownloadsEventRouter::OnExtensionUnloaded
~~~cpp
void OnExtensionUnloaded(content::BrowserContext* browser_context,
                           const extensions::Extension* extension,
                           extensions::UnloadedExtensionReason reason) override;
~~~
 extensions::ExtensionRegistryObserver.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### notifier_



~~~cpp

download::AllDownloadItemNotifier notifier_;

~~~


### ui_disabling_extensions_



~~~cpp

std::set<const extensions::Extension*> ui_disabling_extensions_;

~~~


### last_checked_removal_



~~~cpp

base::Time last_checked_removal_;

~~~


### extension_registry_observation_



~~~cpp

base::ScopedObservation<extensions::ExtensionRegistry,
                          extensions::ExtensionRegistryObserver>
      extension_registry_observation_{this};

~~~

 Listen to extension unloaded notifications.
