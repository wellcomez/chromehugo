
## class DownloadControllerClientLacros
 namespace download
 This class receives and forwards download events to Ash. It can only be
 used on the main thread. In the near future, it will also be the receiver for
 calls to pause, cancel, and resume downloads from ash-chrome, hence the name.

### DownloadControllerClientLacros

DownloadControllerClientLacros::DownloadControllerClientLacros
~~~cpp
DownloadControllerClientLacros();
~~~

### DownloadControllerClientLacros

DownloadControllerClientLacros
~~~cpp
DownloadControllerClientLacros(const DownloadControllerClientLacros&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadControllerClientLacros& operator=(
      const DownloadControllerClientLacros&) = delete;
~~~

### ~DownloadControllerClientLacros

DownloadControllerClientLacros::~DownloadControllerClientLacros
~~~cpp
~DownloadControllerClientLacros() override;
~~~

### GetAllDownloads

DownloadControllerClientLacros::GetAllDownloads
~~~cpp
void GetAllDownloads(
      crosapi::mojom::DownloadControllerClient::GetAllDownloadsCallback
          callback) override;
~~~
 crosapi::mojom::DownloadControllerClient:
### Pause

DownloadControllerClientLacros::Pause
~~~cpp
void Pause(const std::string& download_guid) override;
~~~

### Resume

DownloadControllerClientLacros::Resume
~~~cpp
void Resume(const std::string& download_guid, bool user_resume) override;
~~~

### Cancel

DownloadControllerClientLacros::Cancel
~~~cpp
void Cancel(const std::string& download_guid, bool user_cancel) override;
~~~

### SetOpenWhenComplete

DownloadControllerClientLacros::SetOpenWhenComplete
~~~cpp
void SetOpenWhenComplete(const std::string& download_guid,
                           bool open_when_complete) override;
~~~

### OnProfileAdded

DownloadControllerClientLacros::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile) override;
~~~
 ProfileManagerObserver:
### OnProfileManagerDestroying

DownloadControllerClientLacros::OnProfileManagerDestroying
~~~cpp
void OnProfileManagerDestroying() override;
~~~

### OnManagerInitialized

DownloadControllerClientLacros::OnManagerInitialized
~~~cpp
void OnManagerInitialized(content::DownloadManager* manager) override;
~~~
 MultiProfileDownloadNotifier::Client:
### OnManagerGoingDown

DownloadControllerClientLacros::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnDownloadCreated

DownloadControllerClientLacros::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadControllerClientLacros::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadDestroyed

DownloadControllerClientLacros::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(content::DownloadManager* manager,
                           download::DownloadItem* item) override;
~~~

### client_receiver_



~~~cpp

mojo::Receiver<crosapi::mojom::DownloadControllerClient> client_receiver_{
      this};

~~~


### download_notifier_



~~~cpp

MultiProfileDownloadNotifier download_notifier_{
      this, /*wait_for_manager_initialization=*/true};

~~~


### profile_manager_observation_



~~~cpp

base::ScopedObservation<ProfileManager, ProfileManagerObserver>
      profile_manager_observation_{this};

~~~


### DownloadControllerClientLacros

DownloadControllerClientLacros
~~~cpp
DownloadControllerClientLacros(const DownloadControllerClientLacros&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadControllerClientLacros& operator=(
      const DownloadControllerClientLacros&) = delete;
~~~

### GetAllDownloads

DownloadControllerClientLacros::GetAllDownloads
~~~cpp
void GetAllDownloads(
      crosapi::mojom::DownloadControllerClient::GetAllDownloadsCallback
          callback) override;
~~~
 crosapi::mojom::DownloadControllerClient:
### Pause

DownloadControllerClientLacros::Pause
~~~cpp
void Pause(const std::string& download_guid) override;
~~~

### Resume

DownloadControllerClientLacros::Resume
~~~cpp
void Resume(const std::string& download_guid, bool user_resume) override;
~~~

### Cancel

DownloadControllerClientLacros::Cancel
~~~cpp
void Cancel(const std::string& download_guid, bool user_cancel) override;
~~~

### SetOpenWhenComplete

DownloadControllerClientLacros::SetOpenWhenComplete
~~~cpp
void SetOpenWhenComplete(const std::string& download_guid,
                           bool open_when_complete) override;
~~~

### OnProfileAdded

DownloadControllerClientLacros::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile) override;
~~~
 ProfileManagerObserver:
### OnProfileManagerDestroying

DownloadControllerClientLacros::OnProfileManagerDestroying
~~~cpp
void OnProfileManagerDestroying() override;
~~~

### OnManagerInitialized

DownloadControllerClientLacros::OnManagerInitialized
~~~cpp
void OnManagerInitialized(content::DownloadManager* manager) override;
~~~
 MultiProfileDownloadNotifier::Client:
### OnManagerGoingDown

DownloadControllerClientLacros::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnDownloadCreated

DownloadControllerClientLacros::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadControllerClientLacros::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadDestroyed

DownloadControllerClientLacros::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(content::DownloadManager* manager,
                           download::DownloadItem* item) override;
~~~

### client_receiver_



~~~cpp

mojo::Receiver<crosapi::mojom::DownloadControllerClient> client_receiver_{
      this};

~~~


### download_notifier_



~~~cpp

MultiProfileDownloadNotifier download_notifier_{
      this, /*wait_for_manager_initialization=*/true};

~~~


### profile_manager_observation_



~~~cpp

base::ScopedObservation<ProfileManager, ProfileManagerObserver>
      profile_manager_observation_{this};

~~~

