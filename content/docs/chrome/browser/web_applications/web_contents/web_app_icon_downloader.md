
## class WebAppIconDownloader
 Class to help download all icons (including favicons and web app manifest
 icons) for a tab.

### WebAppIconDownloader

WebAppIconDownloader::WebAppIconDownloader
~~~cpp
WebAppIconDownloader(content::WebContents* web_contents,
                       base::flat_set<GURL> extra_favicon_urls,
                       WebAppIconDownloaderCallback callback);
~~~
 |extra_favicon_urls| allows callers to provide icon urls that aren't
 provided by the renderer (e.g touch icons on non-android environments).

### WebAppIconDownloader

WebAppIconDownloader
~~~cpp
WebAppIconDownloader(const WebAppIconDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
WebAppIconDownloader& operator=(const WebAppIconDownloader&) = delete;
~~~

### ~WebAppIconDownloader

WebAppIconDownloader::~WebAppIconDownloader
~~~cpp
~WebAppIconDownloader() override;
~~~

### SkipPageFavicons

WebAppIconDownloader::SkipPageFavicons
~~~cpp
void SkipPageFavicons();
~~~
 Instructs the downloader to not query the page for favicons (e.g. when a
 favicon URL has already been provided in the constructor's
 |extra_favicon_urls| argument).

### FailAllIfAnyFail

WebAppIconDownloader::FailAllIfAnyFail
~~~cpp
void FailAllIfAnyFail();
~~~

### Start

WebAppIconDownloader::Start
~~~cpp
void Start();
~~~

### pending_requests

pending_requests
~~~cpp
size_t pending_requests() const { return in_progress_requests_.size(); }
~~~

### DownloadImage

WebAppIconDownloader::DownloadImage
~~~cpp
int DownloadImage(const GURL& url);
~~~
 Initiates a download of the image at |url| and returns the download id.

###  GetFaviconURLsFromWebContents

 Queries WebContents for the page's current favicon URLs.

~~~cpp
const std::vector<blink::mojom::FaviconURLPtr>&
  GetFaviconURLsFromWebContents();
~~~
### FetchIcons

WebAppIconDownloader::FetchIcons
~~~cpp
void FetchIcons(const std::vector<blink::mojom::FaviconURLPtr>& favicon_urls);
~~~
 Fetches icons for the given urls.

 |callback_| is run when all downloads complete.

### FetchIcons

WebAppIconDownloader::FetchIcons
~~~cpp
void FetchIcons(const base::flat_set<GURL>& urls);
~~~

### DidDownloadFavicon

WebAppIconDownloader::DidDownloadFavicon
~~~cpp
void DidDownloadFavicon(int id,
                          int http_status_code,
                          const GURL& image_url,
                          const std::vector<SkBitmap>& bitmaps,
                          const std::vector<gfx::Size>& original_bitmap_sizes);
~~~
 Icon download callback.

### PrimaryPageChanged

WebAppIconDownloader::PrimaryPageChanged
~~~cpp
void PrimaryPageChanged(content::Page& page) override;
~~~
 content::WebContentsObserver:
### DidUpdateFaviconURL

WebAppIconDownloader::DidUpdateFaviconURL
~~~cpp
void DidUpdateFaviconURL(
      content::RenderFrameHost* rfh,
      const std::vector<blink::mojom::FaviconURLPtr>& candidates) override;
~~~

### WebContentsDestroyed

WebAppIconDownloader::WebContentsDestroyed
~~~cpp
void WebContentsDestroyed() override;
~~~

### CompleteCallback

WebAppIconDownloader::CompleteCallback
~~~cpp
void CompleteCallback();
~~~

### CancelDownloads

WebAppIconDownloader::CancelDownloads
~~~cpp
void CancelDownloads(IconsDownloadedResult result,
                       DownloadedIconsHttpResults icons_http_results);
~~~

### need_favicon_urls_



~~~cpp

bool need_favicon_urls_ = true;

~~~

 Whether we need to fetch favicons from the renderer.

### fail_all_if_any_fail_



~~~cpp

bool fail_all_if_any_fail_ = false;

~~~

 Whether we consider all requests to have failed if any individual URL fails
 to load.

### extra_favicon_urls_



~~~cpp

base::flat_set<GURL> extra_favicon_urls_;

~~~

 URLs that aren't given by WebContentsObserver::DidUpdateFaviconURL() that
 should be used for this favicon. This is necessary in order to get touch
 icons on non-android environments.

### icons_map_



~~~cpp

IconsMap icons_map_;

~~~

 The icons which were downloaded. Populated by FetchIcons().

### icons_http_results_



~~~cpp

DownloadedIconsHttpResults icons_http_results_;

~~~

 The http status codes resulted from url downloading requests.

### in_progress_requests_



~~~cpp

std::set<int> in_progress_requests_;

~~~

 Request ids of in-progress requests.

### processed_urls_



~~~cpp

std::set<GURL> processed_urls_;

~~~

 Urls for which a download has already been initiated. Used to prevent
 duplicate downloads of the same url.

### callback_



~~~cpp

WebAppIconDownloaderCallback callback_;

~~~

 Callback to run on favicon download completion.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<WebAppIconDownloader> weak_ptr_factory_{this};

~~~


### WebAppIconDownloader

WebAppIconDownloader
~~~cpp
WebAppIconDownloader(const WebAppIconDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
WebAppIconDownloader& operator=(const WebAppIconDownloader&) = delete;
~~~

### pending_requests

pending_requests
~~~cpp
size_t pending_requests() const { return in_progress_requests_.size(); }
~~~

### SkipPageFavicons

WebAppIconDownloader::SkipPageFavicons
~~~cpp
void SkipPageFavicons();
~~~
 Instructs the downloader to not query the page for favicons (e.g. when a
 favicon URL has already been provided in the constructor's
 |extra_favicon_urls| argument).

### FailAllIfAnyFail

WebAppIconDownloader::FailAllIfAnyFail
~~~cpp
void FailAllIfAnyFail();
~~~

### Start

WebAppIconDownloader::Start
~~~cpp
void Start();
~~~

### DownloadImage

WebAppIconDownloader::DownloadImage
~~~cpp
int DownloadImage(const GURL& url);
~~~
 Initiates a download of the image at |url| and returns the download id.

###  GetFaviconURLsFromWebContents

 Queries WebContents for the page's current favicon URLs.

~~~cpp
const std::vector<blink::mojom::FaviconURLPtr>&
  GetFaviconURLsFromWebContents();
~~~
### FetchIcons

WebAppIconDownloader::FetchIcons
~~~cpp
void FetchIcons(const std::vector<blink::mojom::FaviconURLPtr>& favicon_urls);
~~~
 Fetches icons for the given urls.

 |callback_| is run when all downloads complete.

### FetchIcons

WebAppIconDownloader::FetchIcons
~~~cpp
void FetchIcons(const base::flat_set<GURL>& urls);
~~~

### DidDownloadFavicon

WebAppIconDownloader::DidDownloadFavicon
~~~cpp
void DidDownloadFavicon(int id,
                          int http_status_code,
                          const GURL& image_url,
                          const std::vector<SkBitmap>& bitmaps,
                          const std::vector<gfx::Size>& original_bitmap_sizes);
~~~
 Icon download callback.

### PrimaryPageChanged

WebAppIconDownloader::PrimaryPageChanged
~~~cpp
void PrimaryPageChanged(content::Page& page) override;
~~~
 content::WebContentsObserver:
### DidUpdateFaviconURL

WebAppIconDownloader::DidUpdateFaviconURL
~~~cpp
void DidUpdateFaviconURL(
      content::RenderFrameHost* rfh,
      const std::vector<blink::mojom::FaviconURLPtr>& candidates) override;
~~~

### WebContentsDestroyed

WebAppIconDownloader::WebContentsDestroyed
~~~cpp
void WebContentsDestroyed() override;
~~~

### CompleteCallback

WebAppIconDownloader::CompleteCallback
~~~cpp
void CompleteCallback();
~~~

### CancelDownloads

WebAppIconDownloader::CancelDownloads
~~~cpp
void CancelDownloads(IconsDownloadedResult result,
                       DownloadedIconsHttpResults icons_http_results);
~~~

### need_favicon_urls_



~~~cpp

bool need_favicon_urls_ = true;

~~~

 Whether we need to fetch favicons from the renderer.

### fail_all_if_any_fail_



~~~cpp

bool fail_all_if_any_fail_ = false;

~~~

 Whether we consider all requests to have failed if any individual URL fails
 to load.

### extra_favicon_urls_



~~~cpp

base::flat_set<GURL> extra_favicon_urls_;

~~~

 URLs that aren't given by WebContentsObserver::DidUpdateFaviconURL() that
 should be used for this favicon. This is necessary in order to get touch
 icons on non-android environments.

### icons_map_



~~~cpp

IconsMap icons_map_;

~~~

 The icons which were downloaded. Populated by FetchIcons().

### icons_http_results_



~~~cpp

DownloadedIconsHttpResults icons_http_results_;

~~~

 The http status codes resulted from url downloading requests.

### in_progress_requests_



~~~cpp

std::set<int> in_progress_requests_;

~~~

 Request ids of in-progress requests.

### processed_urls_



~~~cpp

std::set<GURL> processed_urls_;

~~~

 Urls for which a download has already been initiated. Used to prevent
 duplicate downloads of the same url.

### callback_



~~~cpp

WebAppIconDownloaderCallback callback_;

~~~

 Callback to run on favicon download completion.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<WebAppIconDownloader> weak_ptr_factory_{this};

~~~

