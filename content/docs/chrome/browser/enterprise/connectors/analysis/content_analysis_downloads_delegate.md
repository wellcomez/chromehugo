
## class ContentAnalysisDownloadsDelegate
 A ContentAnalysisDelegateBase implementation meant to be used to display the
 ContentAnalysisDialog for a download that triggered a block or a warning, and
 for which a custom message must be shown to the user.

### ContentAnalysisDownloadsDelegate

ContentAnalysisDownloadsDelegate::ContentAnalysisDownloadsDelegate
~~~cpp
ContentAnalysisDownloadsDelegate(
      const std::u16string& filename,
      const std::u16string& custom_message,
      GURL custom_learn_more_url,
      bool bypass_justification_required,
      base::OnceCallback<void()> open_file_callback,
      base::OnceCallback<void()> discard_file_callback,
      download::DownloadItem* download_item);
~~~

### ~ContentAnalysisDownloadsDelegate

ContentAnalysisDownloadsDelegate::~ContentAnalysisDownloadsDelegate
~~~cpp
~ContentAnalysisDownloadsDelegate() override;
~~~

### BypassWarnings

ContentAnalysisDownloadsDelegate::BypassWarnings
~~~cpp
void BypassWarnings(
      absl::optional<std::u16string> user_justification) override;
~~~
 Called when the user opts to keep the download and open it. Should not be
 called if the result was a "block" since the option shouldn't be available
 in that case.

### Cancel

ContentAnalysisDownloadsDelegate::Cancel
~~~cpp
void Cancel(bool warning) override;
~~~
 Called when the user opts to delete the downloaded file and not open it.

### GetCustomMessage

ContentAnalysisDownloadsDelegate::GetCustomMessage
~~~cpp
absl::optional<std::u16string> GetCustomMessage() const override;
~~~

### GetCustomLearnMoreUrl

ContentAnalysisDownloadsDelegate::GetCustomLearnMoreUrl
~~~cpp
absl::optional<GURL> GetCustomLearnMoreUrl() const override;
~~~

### BypassRequiresJustification

ContentAnalysisDownloadsDelegate::BypassRequiresJustification
~~~cpp
bool BypassRequiresJustification() const override;
~~~

### GetBypassJustificationLabel

ContentAnalysisDownloadsDelegate::GetBypassJustificationLabel
~~~cpp
std::u16string GetBypassJustificationLabel() const override;
~~~

### OverrideCancelButtonText

ContentAnalysisDownloadsDelegate::OverrideCancelButtonText
~~~cpp
absl::optional<std::u16string> OverrideCancelButtonText() const override;
~~~

### ResetCallbacks

ContentAnalysisDownloadsDelegate::ResetCallbacks
~~~cpp
void ResetCallbacks();
~~~
 Resets |open_file_callback_| and |discard_file_callback_|, ensuring actions
 can't be attempted on a file that has already been opened or discarded
 (which may be undefined).

### filename_



~~~cpp

std::u16string filename_;

~~~


### custom_message_



~~~cpp

std::u16string custom_message_;

~~~


### custom_learn_more_url_



~~~cpp

GURL custom_learn_more_url_;

~~~


### bypass_justification_required_



~~~cpp

bool bypass_justification_required_;

~~~


### d


~~~cpp
base::OnceCallback<void()> open_file_callback_;
~~~
### d


~~~cpp
base::OnceCallback<void()> discard_file_callback_;
~~~
### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~


### BypassWarnings

ContentAnalysisDownloadsDelegate::BypassWarnings
~~~cpp
void BypassWarnings(
      absl::optional<std::u16string> user_justification) override;
~~~
 Called when the user opts to keep the download and open it. Should not be
 called if the result was a "block" since the option shouldn't be available
 in that case.

### Cancel

ContentAnalysisDownloadsDelegate::Cancel
~~~cpp
void Cancel(bool warning) override;
~~~
 Called when the user opts to delete the downloaded file and not open it.

### GetCustomMessage

ContentAnalysisDownloadsDelegate::GetCustomMessage
~~~cpp
absl::optional<std::u16string> GetCustomMessage() const override;
~~~

### GetCustomLearnMoreUrl

ContentAnalysisDownloadsDelegate::GetCustomLearnMoreUrl
~~~cpp
absl::optional<GURL> GetCustomLearnMoreUrl() const override;
~~~

### BypassRequiresJustification

ContentAnalysisDownloadsDelegate::BypassRequiresJustification
~~~cpp
bool BypassRequiresJustification() const override;
~~~

### GetBypassJustificationLabel

ContentAnalysisDownloadsDelegate::GetBypassJustificationLabel
~~~cpp
std::u16string GetBypassJustificationLabel() const override;
~~~

### OverrideCancelButtonText

ContentAnalysisDownloadsDelegate::OverrideCancelButtonText
~~~cpp
absl::optional<std::u16string> OverrideCancelButtonText() const override;
~~~

### ResetCallbacks

ContentAnalysisDownloadsDelegate::ResetCallbacks
~~~cpp
void ResetCallbacks();
~~~
 Resets |open_file_callback_| and |discard_file_callback_|, ensuring actions
 can't be attempted on a file that has already been opened or discarded
 (which may be undefined).

### filename_



~~~cpp

std::u16string filename_;

~~~


### custom_message_



~~~cpp

std::u16string custom_message_;

~~~


### custom_learn_more_url_



~~~cpp

GURL custom_learn_more_url_;

~~~


### bypass_justification_required_



~~~cpp

bool bypass_justification_required_;

~~~


### d


~~~cpp
base::OnceCallback<void()> open_file_callback_;
~~~
### d


~~~cpp
base::OnceCallback<void()> discard_file_callback_;
~~~
### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

