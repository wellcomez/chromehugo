
## class DownloadInternalsUIMessageHandler
 Class acting as a controller of the chrome://download-internals WebUI.

### DownloadInternalsUIMessageHandler

DownloadInternalsUIMessageHandler::DownloadInternalsUIMessageHandler
~~~cpp
DownloadInternalsUIMessageHandler();
~~~

### DownloadInternalsUIMessageHandler

DownloadInternalsUIMessageHandler
~~~cpp
DownloadInternalsUIMessageHandler(const DownloadInternalsUIMessageHandler&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadInternalsUIMessageHandler& operator=(
      const DownloadInternalsUIMessageHandler&) = delete;
~~~

### ~DownloadInternalsUIMessageHandler

DownloadInternalsUIMessageHandler::~DownloadInternalsUIMessageHandler
~~~cpp
~DownloadInternalsUIMessageHandler() override;
~~~

### RegisterMessages

DownloadInternalsUIMessageHandler::RegisterMessages
~~~cpp
void RegisterMessages() override;
~~~
 content::WebUIMessageHandler implementation.

### OnServiceStatusChanged

DownloadInternalsUIMessageHandler::OnServiceStatusChanged
~~~cpp
void OnServiceStatusChanged(const base::Value::Dict& service_status) override;
~~~
 download::Logger::Observer implementation.

### OnServiceDownloadsAvailable

DownloadInternalsUIMessageHandler::OnServiceDownloadsAvailable
~~~cpp
void OnServiceDownloadsAvailable(
      const base::Value::List& service_downloads) override;
~~~

### OnServiceDownloadChanged

DownloadInternalsUIMessageHandler::OnServiceDownloadChanged
~~~cpp
void OnServiceDownloadChanged(
      const base::Value::Dict& service_download) override;
~~~

### OnServiceDownloadFailed

DownloadInternalsUIMessageHandler::OnServiceDownloadFailed
~~~cpp
void OnServiceDownloadFailed(
      const base::Value::Dict& service_download) override;
~~~

### OnServiceRequestMade

DownloadInternalsUIMessageHandler::OnServiceRequestMade
~~~cpp
void OnServiceRequestMade(const base::Value::Dict& service_request) override;
~~~

### HandleGetServiceStatus

DownloadInternalsUIMessageHandler::HandleGetServiceStatus
~~~cpp
void HandleGetServiceStatus(const base::Value::List& args);
~~~
 Get the current DownloadService and sub component statuses.

### HandleGetServiceDownloads

DownloadInternalsUIMessageHandler::HandleGetServiceDownloads
~~~cpp
void HandleGetServiceDownloads(const base::Value::List& args);
~~~

### HandleStartDownload

DownloadInternalsUIMessageHandler::HandleStartDownload
~~~cpp
void HandleStartDownload(const base::Value::List& args);
~~~
 Starts a background download.

### download_service_



~~~cpp

raw_ptr<download::BackgroundDownloadService> download_service_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadInternalsUIMessageHandler> weak_ptr_factory_{
      this};

~~~


### DownloadInternalsUIMessageHandler

DownloadInternalsUIMessageHandler
~~~cpp
DownloadInternalsUIMessageHandler(const DownloadInternalsUIMessageHandler&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadInternalsUIMessageHandler& operator=(
      const DownloadInternalsUIMessageHandler&) = delete;
~~~

### RegisterMessages

DownloadInternalsUIMessageHandler::RegisterMessages
~~~cpp
void RegisterMessages() override;
~~~
 content::WebUIMessageHandler implementation.

### OnServiceStatusChanged

DownloadInternalsUIMessageHandler::OnServiceStatusChanged
~~~cpp
void OnServiceStatusChanged(const base::Value::Dict& service_status) override;
~~~
 download::Logger::Observer implementation.

### OnServiceDownloadsAvailable

DownloadInternalsUIMessageHandler::OnServiceDownloadsAvailable
~~~cpp
void OnServiceDownloadsAvailable(
      const base::Value::List& service_downloads) override;
~~~

### OnServiceDownloadChanged

DownloadInternalsUIMessageHandler::OnServiceDownloadChanged
~~~cpp
void OnServiceDownloadChanged(
      const base::Value::Dict& service_download) override;
~~~

### OnServiceDownloadFailed

DownloadInternalsUIMessageHandler::OnServiceDownloadFailed
~~~cpp
void OnServiceDownloadFailed(
      const base::Value::Dict& service_download) override;
~~~

### OnServiceRequestMade

DownloadInternalsUIMessageHandler::OnServiceRequestMade
~~~cpp
void OnServiceRequestMade(const base::Value::Dict& service_request) override;
~~~

### HandleGetServiceStatus

DownloadInternalsUIMessageHandler::HandleGetServiceStatus
~~~cpp
void HandleGetServiceStatus(const base::Value::List& args);
~~~
 Get the current DownloadService and sub component statuses.

### HandleGetServiceDownloads

DownloadInternalsUIMessageHandler::HandleGetServiceDownloads
~~~cpp
void HandleGetServiceDownloads(const base::Value::List& args);
~~~

### HandleStartDownload

DownloadInternalsUIMessageHandler::HandleStartDownload
~~~cpp
void HandleStartDownload(const base::Value::List& args);
~~~
 Starts a background download.

### download_service_



~~~cpp

raw_ptr<download::BackgroundDownloadService> download_service_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadInternalsUIMessageHandler> weak_ptr_factory_{
      this};

~~~

