
## class DownloadInternalsUI
 The WebUI for chrome://download-internals.

### DownloadInternalsUI

DownloadInternalsUI::DownloadInternalsUI
~~~cpp
explicit DownloadInternalsUI(content::WebUI* web_ui);
~~~

### DownloadInternalsUI

DownloadInternalsUI
~~~cpp
DownloadInternalsUI(const DownloadInternalsUI&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadInternalsUI& operator=(const DownloadInternalsUI&) = delete;
~~~

### ~DownloadInternalsUI

DownloadInternalsUI::~DownloadInternalsUI
~~~cpp
~DownloadInternalsUI() override;
~~~

### DownloadInternalsUI

DownloadInternalsUI
~~~cpp
DownloadInternalsUI(const DownloadInternalsUI&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadInternalsUI& operator=(const DownloadInternalsUI&) = delete;
~~~
