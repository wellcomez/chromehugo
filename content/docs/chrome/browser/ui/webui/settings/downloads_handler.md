
## class DownloadsHandler
 Chrome "Downloads" settings page UI handler.

### DownloadsHandler

DownloadsHandler::DownloadsHandler
~~~cpp
explicit DownloadsHandler(Profile* profile);
~~~

### DownloadsHandler

DownloadsHandler
~~~cpp
DownloadsHandler(const DownloadsHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsHandler& operator=(const DownloadsHandler&) = delete;
~~~

### ~DownloadsHandler

DownloadsHandler::~DownloadsHandler
~~~cpp
~DownloadsHandler() override;
~~~

### RegisterMessages

DownloadsHandler::RegisterMessages
~~~cpp
void RegisterMessages() override;
~~~
 SettingsPageUIHandler implementation.

### OnJavascriptAllowed

DownloadsHandler::OnJavascriptAllowed
~~~cpp
void OnJavascriptAllowed() override;
~~~

### OnJavascriptDisallowed

DownloadsHandler::OnJavascriptDisallowed
~~~cpp
void OnJavascriptDisallowed() override;
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadsHandler::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadsHandlerTest, AutoOpenDownloads);
~~~

### HandleInitialize

DownloadsHandler::HandleInitialize
~~~cpp
void HandleInitialize(const base::Value::List& args);
~~~
 Callback for the "initializeDownloads" message. This starts observers and
 retrieves the current browser state.

### SendAutoOpenDownloadsToJavascript

DownloadsHandler::SendAutoOpenDownloadsToJavascript
~~~cpp
void SendAutoOpenDownloadsToJavascript();
~~~

### HandleResetAutoOpenFileTypes

DownloadsHandler::HandleResetAutoOpenFileTypes
~~~cpp
void HandleResetAutoOpenFileTypes(const base::Value::List& args);
~~~
 Resets the list of filetypes that are auto-opened after download.

### HandleSelectDownloadLocation

DownloadsHandler::HandleSelectDownloadLocation
~~~cpp
void HandleSelectDownloadLocation(const base::Value::List& args);
~~~
 Callback for the "selectDownloadLocation" message. This will prompt the
 user for a destination folder using platform-specific APIs.

### FileSelected

DownloadsHandler::FileSelected
~~~cpp
void FileSelected(const base::FilePath& path,
                    int index,
                    void* params) override;
~~~
 SelectFileDialog::Listener implementation.

### FileSelectionCanceled

DownloadsHandler::FileSelectionCanceled
~~~cpp
void FileSelectionCanceled(void* params) override;
~~~

### HandleGetDownloadLocationText

DownloadsHandler::HandleGetDownloadLocationText
~~~cpp
void HandleGetDownloadLocationText(const base::Value::List& args);
~~~
 Callback for the "getDownloadLocationText" message.  Converts actual
 paths in chromeos to values suitable to display to users.

 E.g. /home/chronos/u-<hash>/Downloads => "Downloads".

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### pref_registrar_



~~~cpp

PrefChangeRegistrar pref_registrar_;

~~~


### select_folder_dialog_



~~~cpp

scoped_refptr<ui::SelectFileDialog> select_folder_dialog_;

~~~


### DownloadsHandler

DownloadsHandler
~~~cpp
DownloadsHandler(const DownloadsHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsHandler& operator=(const DownloadsHandler&) = delete;
~~~

### RegisterMessages

DownloadsHandler::RegisterMessages
~~~cpp
void RegisterMessages() override;
~~~
 SettingsPageUIHandler implementation.

### OnJavascriptAllowed

DownloadsHandler::OnJavascriptAllowed
~~~cpp
void OnJavascriptAllowed() override;
~~~

### OnJavascriptDisallowed

DownloadsHandler::OnJavascriptDisallowed
~~~cpp
void OnJavascriptDisallowed() override;
~~~

### HandleInitialize

DownloadsHandler::HandleInitialize
~~~cpp
void HandleInitialize(const base::Value::List& args);
~~~
 Callback for the "initializeDownloads" message. This starts observers and
 retrieves the current browser state.

### SendAutoOpenDownloadsToJavascript

DownloadsHandler::SendAutoOpenDownloadsToJavascript
~~~cpp
void SendAutoOpenDownloadsToJavascript();
~~~

### HandleResetAutoOpenFileTypes

DownloadsHandler::HandleResetAutoOpenFileTypes
~~~cpp
void HandleResetAutoOpenFileTypes(const base::Value::List& args);
~~~
 Resets the list of filetypes that are auto-opened after download.

### HandleSelectDownloadLocation

DownloadsHandler::HandleSelectDownloadLocation
~~~cpp
void HandleSelectDownloadLocation(const base::Value::List& args);
~~~
 Callback for the "selectDownloadLocation" message. This will prompt the
 user for a destination folder using platform-specific APIs.

### FileSelected

DownloadsHandler::FileSelected
~~~cpp
void FileSelected(const base::FilePath& path,
                    int index,
                    void* params) override;
~~~
 SelectFileDialog::Listener implementation.

### FileSelectionCanceled

DownloadsHandler::FileSelectionCanceled
~~~cpp
void FileSelectionCanceled(void* params) override;
~~~

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### pref_registrar_



~~~cpp

PrefChangeRegistrar pref_registrar_;

~~~


### select_folder_dialog_



~~~cpp

scoped_refptr<ui::SelectFileDialog> select_folder_dialog_;

~~~

