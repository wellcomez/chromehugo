
## class DownloadsListTracker
 A class that tracks all downloads activity and keeps a sorted representation
 of the downloads as chrome://downloads wants to display them.

### DownloadsListTracker

DownloadsListTracker::DownloadsListTracker
~~~cpp
DownloadsListTracker(content::DownloadManager* download_manager,
                       mojo::PendingRemote<downloads::mojom::Page> page);
~~~

### DownloadsListTracker

DownloadsListTracker
~~~cpp
DownloadsListTracker(const DownloadsListTracker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsListTracker& operator=(const DownloadsListTracker&) = delete;
~~~

### ~DownloadsListTracker

DownloadsListTracker::~DownloadsListTracker
~~~cpp
~DownloadsListTracker() override;
~~~

### Reset

DownloadsListTracker::Reset
~~~cpp
void Reset();
~~~
 Clears all downloads on the page if currently sending updates and resets
 chunk tracking data.

### SetSearchTerms

DownloadsListTracker::SetSearchTerms
~~~cpp
bool SetSearchTerms(const std::vector<std::string>& search_terms);
~~~
 This class only cares about downloads that match |search_terms|.

 An empty list shows all downloads (the default). Returns true if
 |search_terms| differ from the current ones.

### StartAndSendChunk

DownloadsListTracker::StartAndSendChunk
~~~cpp
void StartAndSendChunk();
~~~
 Starts sending updates and sends a capped amount of downloads. Tracks which
 downloads have been sent. Re-call this to send more.

### Stop

DownloadsListTracker::Stop
~~~cpp
void Stop();
~~~
 Stops sending updates to the page.

### GetMainNotifierManager

DownloadsListTracker::GetMainNotifierManager
~~~cpp
content::DownloadManager* GetMainNotifierManager() const;
~~~

### GetOriginalNotifierManager

DownloadsListTracker::GetOriginalNotifierManager
~~~cpp
content::DownloadManager* GetOriginalNotifierManager() const;
~~~

### OnDownloadCreated

DownloadsListTracker::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~
 AllDownloadItemNotifier::Observer:
### OnDownloadUpdated

DownloadsListTracker::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### OnDownloadRemoved

DownloadsListTracker::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### DownloadsListTracker

DownloadsListTracker::DownloadsListTracker
~~~cpp
DownloadsListTracker(
      content::DownloadManager* download_manager,
      mojo::PendingRemote<downloads::mojom::Page> page,
      base::RepeatingCallback<bool(const download::DownloadItem&)>);
~~~
 Testing constructor.

### CreateDownloadData

DownloadsListTracker::CreateDownloadData
~~~cpp
virtual downloads::mojom::DataPtr CreateDownloadData(
      download::DownloadItem* item) const;
~~~
 Creates a dictionary value that's sent to the page as JSON.

### IsIncognito

DownloadsListTracker::IsIncognito
~~~cpp
bool IsIncognito(const download::DownloadItem& item) const;
~~~
 Exposed for testing.

### GetItemForTesting

DownloadsListTracker::GetItemForTesting
~~~cpp
const download::DownloadItem* GetItemForTesting(size_t index) const;
~~~

### SetChunkSizeForTesting

DownloadsListTracker::SetChunkSizeForTesting
~~~cpp
void SetChunkSizeForTesting(size_t chunk_size);
~~~

###  operator


~~~cpp
struct StartTimeComparator {
    bool operator()(const download::DownloadItem* a,
                    const download::DownloadItem* b) const;
  };
~~~
### Init

DownloadsListTracker::Init
~~~cpp
void Init();
~~~
 Called by both constructors to initialize common state.

### RebuildSortedItems

DownloadsListTracker::RebuildSortedItems
~~~cpp
void RebuildSortedItems();
~~~
 Clears and re-inserts all downloads items into |sorted_items_|.

### ShouldShow

DownloadsListTracker::ShouldShow
~~~cpp
bool ShouldShow(const download::DownloadItem& item) const;
~~~
 Whether |item| should show on the current page.

### GetIndex

DownloadsListTracker::GetIndex
~~~cpp
size_t GetIndex(const SortedSet::iterator& item) const;
~~~
 Returns the index of |item| in |sorted_items_|.

### InsertItem

DownloadsListTracker::InsertItem
~~~cpp
void InsertItem(const SortedSet::iterator& insert);
~~~
 Calls "insertItems" if sending updates and the page knows about |insert|.

### UpdateItem

DownloadsListTracker::UpdateItem
~~~cpp
void UpdateItem(const SortedSet::iterator& update);
~~~
 Calls "updateItem" if sending updates and the page knows about |update|.

### RemoveItem

DownloadsListTracker::RemoveItem
~~~cpp
void RemoveItem(const SortedSet::iterator& remove);
~~~
 Removes the item that corresponds to |remove| and sends "removeItems"
 if sending updates.

### main_notifier_



~~~cpp

download::AllDownloadItemNotifier main_notifier_;

~~~


### original_notifier_



~~~cpp

std::unique_ptr<download::AllDownloadItemNotifier> original_notifier_;

~~~


### page_



~~~cpp

mojo::Remote<downloads::mojom::Page> page_;

~~~


### l

 Callback used to determine if an item should show on the page. Set to
 |ShouldShow()| in default constructor, passed in while testing.

~~~cpp
base::RepeatingCallback<bool(const download::DownloadItem&)> should_show_;
~~~
### sending_updates_



~~~cpp

bool sending_updates_ = false;

~~~

 When this is true, all changes to downloads that affect the page are sent
 via JavaScript.

### sorted_items_



~~~cpp

SortedSet sorted_items_;

~~~


### sent_to_page_



~~~cpp

size_t sent_to_page_ = 0u;

~~~

 The number of items sent to the page so far.

### chunk_size_



~~~cpp

size_t chunk_size_ = 20u;

~~~

 The maximum number of items sent to the page at a time.

### search_terms_



~~~cpp

std::vector<std::u16string> search_terms_;

~~~

 Current search terms.

### DownloadsListTracker

DownloadsListTracker
~~~cpp
DownloadsListTracker(const DownloadsListTracker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsListTracker& operator=(const DownloadsListTracker&) = delete;
~~~

### Reset

DownloadsListTracker::Reset
~~~cpp
void Reset();
~~~
 Clears all downloads on the page if currently sending updates and resets
 chunk tracking data.

### SetSearchTerms

DownloadsListTracker::SetSearchTerms
~~~cpp
bool SetSearchTerms(const std::vector<std::string>& search_terms);
~~~
 This class only cares about downloads that match |search_terms|.

 An empty list shows all downloads (the default). Returns true if
 |search_terms| differ from the current ones.

### StartAndSendChunk

DownloadsListTracker::StartAndSendChunk
~~~cpp
void StartAndSendChunk();
~~~
 Starts sending updates and sends a capped amount of downloads. Tracks which
 downloads have been sent. Re-call this to send more.

### Stop

DownloadsListTracker::Stop
~~~cpp
void Stop();
~~~
 Stops sending updates to the page.

### GetMainNotifierManager

DownloadsListTracker::GetMainNotifierManager
~~~cpp
content::DownloadManager* GetMainNotifierManager() const;
~~~

### GetOriginalNotifierManager

DownloadsListTracker::GetOriginalNotifierManager
~~~cpp
content::DownloadManager* GetOriginalNotifierManager() const;
~~~

### OnDownloadCreated

DownloadsListTracker::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~
 AllDownloadItemNotifier::Observer:
### OnDownloadUpdated

DownloadsListTracker::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### OnDownloadRemoved

DownloadsListTracker::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* download_item) override;
~~~

### CreateDownloadData

DownloadsListTracker::CreateDownloadData
~~~cpp
virtual downloads::mojom::DataPtr CreateDownloadData(
      download::DownloadItem* item) const;
~~~
 Creates a dictionary value that's sent to the page as JSON.

### IsIncognito

DownloadsListTracker::IsIncognito
~~~cpp
bool IsIncognito(const download::DownloadItem& item) const;
~~~
 Exposed for testing.

### GetItemForTesting

DownloadsListTracker::GetItemForTesting
~~~cpp
const download::DownloadItem* GetItemForTesting(size_t index) const;
~~~

### SetChunkSizeForTesting

DownloadsListTracker::SetChunkSizeForTesting
~~~cpp
void SetChunkSizeForTesting(size_t chunk_size);
~~~

###  operator


~~~cpp
struct StartTimeComparator {
    bool operator()(const download::DownloadItem* a,
                    const download::DownloadItem* b) const;
  };
~~~
### Init

DownloadsListTracker::Init
~~~cpp
void Init();
~~~
 Called by both constructors to initialize common state.

### RebuildSortedItems

DownloadsListTracker::RebuildSortedItems
~~~cpp
void RebuildSortedItems();
~~~
 Clears and re-inserts all downloads items into |sorted_items_|.

### ShouldShow

DownloadsListTracker::ShouldShow
~~~cpp
bool ShouldShow(const download::DownloadItem& item) const;
~~~
 Whether |item| should show on the current page.

### GetIndex

DownloadsListTracker::GetIndex
~~~cpp
size_t GetIndex(const SortedSet::iterator& item) const;
~~~
 Returns the index of |item| in |sorted_items_|.

### InsertItem

DownloadsListTracker::InsertItem
~~~cpp
void InsertItem(const SortedSet::iterator& insert);
~~~
 Calls "insertItems" if sending updates and the page knows about |insert|.

### UpdateItem

DownloadsListTracker::UpdateItem
~~~cpp
void UpdateItem(const SortedSet::iterator& update);
~~~
 Calls "updateItem" if sending updates and the page knows about |update|.

### RemoveItem

DownloadsListTracker::RemoveItem
~~~cpp
void RemoveItem(const SortedSet::iterator& remove);
~~~
 Removes the item that corresponds to |remove| and sends "removeItems"
 if sending updates.

### main_notifier_



~~~cpp

download::AllDownloadItemNotifier main_notifier_;

~~~


### original_notifier_



~~~cpp

std::unique_ptr<download::AllDownloadItemNotifier> original_notifier_;

~~~


### page_



~~~cpp

mojo::Remote<downloads::mojom::Page> page_;

~~~


### l

 Callback used to determine if an item should show on the page. Set to
 |ShouldShow()| in default constructor, passed in while testing.

~~~cpp
base::RepeatingCallback<bool(const download::DownloadItem&)> should_show_;
~~~
### sending_updates_



~~~cpp

bool sending_updates_ = false;

~~~

 When this is true, all changes to downloads that affect the page are sent
 via JavaScript.

### sorted_items_



~~~cpp

SortedSet sorted_items_;

~~~


### sent_to_page_



~~~cpp

size_t sent_to_page_ = 0u;

~~~

 The number of items sent to the page so far.

### chunk_size_



~~~cpp

size_t chunk_size_ = 20u;

~~~

 The maximum number of items sent to the page at a time.

### search_terms_



~~~cpp

std::vector<std::u16string> search_terms_;

~~~

 Current search terms.
