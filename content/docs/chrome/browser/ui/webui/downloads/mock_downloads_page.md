
## class MockPage

### MockPage

MockPage::MockPage
~~~cpp
MockPage();
~~~

### ~MockPage

MockPage::~MockPage
~~~cpp
~MockPage() override;
~~~

### BindAndGetRemote

MockPage::BindAndGetRemote
~~~cpp
mojo::PendingRemote<downloads::mojom::Page> BindAndGetRemote();
~~~

### MOCK_METHOD1

MockPage::MOCK_METHOD1
~~~cpp
MOCK_METHOD1(RemoveItem, void(int));
~~~

### MOCK_METHOD2

MockPage::MOCK_METHOD2
~~~cpp
MOCK_METHOD2(UpdateItem, void(int, downloads::mojom::DataPtr));
~~~

### MOCK_METHOD2

MockPage::MOCK_METHOD2
~~~cpp
MOCK_METHOD2(InsertItems, void(int, std::vector<downloads::mojom::DataPtr>));
~~~

### MOCK_METHOD0

MockPage::MOCK_METHOD0
~~~cpp
MOCK_METHOD0(ClearAll, void());
~~~

### receiver_



~~~cpp

mojo::Receiver<downloads::mojom::Page> receiver_{this};

~~~


### BindAndGetRemote

MockPage::BindAndGetRemote
~~~cpp
mojo::PendingRemote<downloads::mojom::Page> BindAndGetRemote();
~~~

### receiver_



~~~cpp

mojo::Receiver<downloads::mojom::Page> receiver_{this};

~~~

