
## class DownloadsDOMHandler
 The handler for Javascript messages related to the "downloads" view,
 also observes changes to the download manager.

 TODO(calamity): Remove WebUIMessageHandler.

### DownloadsDOMHandler

DownloadsDOMHandler::DownloadsDOMHandler
~~~cpp
DownloadsDOMHandler(
      mojo::PendingReceiver<downloads::mojom::PageHandler> receiver,
      mojo::PendingRemote<downloads::mojom::Page> page,
      content::DownloadManager* download_manager,
      content::WebUI* web_ui);
~~~

### DownloadsDOMHandler

DownloadsDOMHandler
~~~cpp
DownloadsDOMHandler(const DownloadsDOMHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsDOMHandler& operator=(const DownloadsDOMHandler&) = delete;
~~~

### ~DownloadsDOMHandler

DownloadsDOMHandler::~DownloadsDOMHandler
~~~cpp
~DownloadsDOMHandler() override;
~~~

### PrimaryMainFrameRenderProcessGone

DownloadsDOMHandler::PrimaryMainFrameRenderProcessGone
~~~cpp
void PrimaryMainFrameRenderProcessGone(
      base::TerminationStatus status) override;
~~~
 WebContentsObserver implementation.

### GetDownloads

DownloadsDOMHandler::GetDownloads
~~~cpp
void GetDownloads(const std::vector<std::string>& search_terms) override;
~~~
 downloads::mojom::PageHandler:
### OpenFileRequiringGesture

DownloadsDOMHandler::OpenFileRequiringGesture
~~~cpp
void OpenFileRequiringGesture(const std::string& id) override;
~~~

### Drag

DownloadsDOMHandler::Drag
~~~cpp
void Drag(const std::string& id) override;
~~~

### SaveDangerousRequiringGesture

DownloadsDOMHandler::SaveDangerousRequiringGesture
~~~cpp
void SaveDangerousRequiringGesture(const std::string& id) override;
~~~

### DiscardDangerous

DownloadsDOMHandler::DiscardDangerous
~~~cpp
void DiscardDangerous(const std::string& id) override;
~~~

### RetryDownload

DownloadsDOMHandler::RetryDownload
~~~cpp
void RetryDownload(const std::string& id) override;
~~~

### Show

DownloadsDOMHandler::Show
~~~cpp
void Show(const std::string& id) override;
~~~

### Pause

DownloadsDOMHandler::Pause
~~~cpp
void Pause(const std::string& id) override;
~~~

### Resume

DownloadsDOMHandler::Resume
~~~cpp
void Resume(const std::string& id) override;
~~~

### Remove

DownloadsDOMHandler::Remove
~~~cpp
void Remove(const std::string& id) override;
~~~

### Undo

DownloadsDOMHandler::Undo
~~~cpp
void Undo() override;
~~~

### Cancel

DownloadsDOMHandler::Cancel
~~~cpp
void Cancel(const std::string& id) override;
~~~

### ClearAll

DownloadsDOMHandler::ClearAll
~~~cpp
void ClearAll() override;
~~~

### OpenDownloadsFolderRequiringGesture

DownloadsDOMHandler::OpenDownloadsFolderRequiringGesture
~~~cpp
void OpenDownloadsFolderRequiringGesture() override;
~~~

### OpenDuringScanningRequiringGesture

DownloadsDOMHandler::OpenDuringScanningRequiringGesture
~~~cpp
void OpenDuringScanningRequiringGesture(const std::string& id) override;
~~~

### ReviewDangerousRequiringGesture

DownloadsDOMHandler::ReviewDangerousRequiringGesture
~~~cpp
void ReviewDangerousRequiringGesture(const std::string& id) override;
~~~

### DeepScan

DownloadsDOMHandler::DeepScan
~~~cpp
void DeepScan(const std::string& id) override;
~~~

### BypassDeepScanRequiringGesture

DownloadsDOMHandler::BypassDeepScanRequiringGesture
~~~cpp
void BypassDeepScanRequiringGesture(const std::string& id) override;
~~~

### GetWebUIWebContents

DownloadsDOMHandler::GetWebUIWebContents
~~~cpp
virtual content::WebContents* GetWebUIWebContents();
~~~
 These methods are for mocking so that most of this class does not actually
 depend on WebUI. The other methods that depend on WebUI are
 RegisterMessages() and HandleDrag().

### FinalizeRemovals

DownloadsDOMHandler::FinalizeRemovals
~~~cpp
void FinalizeRemovals();
~~~
 Actually remove downloads with an ID in |removals_|. This cannot be undone.

### RemoveDownloads

DownloadsDOMHandler::RemoveDownloads
~~~cpp
void RemoveDownloads(const DownloadVector& to_remove);
~~~
 Remove all downloads in |to_remove|. Safe downloads can be revived,
 dangerous ones are immediately removed. Protected for testing.

### GetMainNotifierManager

DownloadsDOMHandler::GetMainNotifierManager
~~~cpp
content::DownloadManager* GetMainNotifierManager() const;
~~~
 Convenience method to call |main_notifier_->GetManager()| while
 null-checking |main_notifier_|.

### GetOriginalNotifierManager

DownloadsDOMHandler::GetOriginalNotifierManager
~~~cpp
content::DownloadManager* GetOriginalNotifierManager() const;
~~~
 Convenience method to call |original_notifier_->GetManager()| while
 null-checking |original_notifier_|.

### ShowDangerPrompt

DownloadsDOMHandler::ShowDangerPrompt
~~~cpp
virtual void ShowDangerPrompt(download::DownloadItem* dangerous);
~~~
 Displays a native prompt asking the user for confirmation after accepting
 the dangerous download specified by |dangerous|. The function returns
 immediately, and will invoke DangerPromptAccepted() asynchronously if the
 user accepts the dangerous download. The native prompt will observe
 |dangerous| until either the dialog is dismissed or |dangerous| is no
 longer an in-progress dangerous download.

### DangerPromptDone

DownloadsDOMHandler::DangerPromptDone
~~~cpp
void DangerPromptDone(int download_id, DownloadDangerPrompt::Action action);
~~~
 Conveys danger acceptance from the DownloadDangerPrompt to the
 DownloadItem.

### IsDeletingHistoryAllowed

DownloadsDOMHandler::IsDeletingHistoryAllowed
~~~cpp
bool IsDeletingHistoryAllowed();
~~~
 Returns true if the records of any downloaded items are allowed (and able)
 to be deleted.

### GetDownloadByStringId

DownloadsDOMHandler::GetDownloadByStringId
~~~cpp
download::DownloadItem* GetDownloadByStringId(const std::string& id);
~~~
 Returns the download that is referred to by a given string |id|.

### GetDownloadById

DownloadsDOMHandler::GetDownloadById
~~~cpp
download::DownloadItem* GetDownloadById(uint32_t id);
~~~
 Returns the download with |id| or NULL if it doesn't exist.

### RemoveDownloadInArgs

DownloadsDOMHandler::RemoveDownloadInArgs
~~~cpp
void RemoveDownloadInArgs(const std::string& id);
~~~
 Removes the download specified by an ID from JavaScript in |args|.

### CheckForRemovedFiles

DownloadsDOMHandler::CheckForRemovedFiles
~~~cpp
void CheckForRemovedFiles();
~~~
 Checks whether a download's file was removed from its original location.

### list_tracker_



~~~cpp

DownloadsListTracker list_tracker_;

~~~


### removals_



~~~cpp

std::vector<IdSet> removals_;

~~~

 IDs of downloads to remove when this handler gets deleted.

### render_process_gone_



~~~cpp

bool render_process_gone_ = false;

~~~

 Whether the render process has gone.

### web_ui_



~~~cpp

raw_ptr<content::WebUI> web_ui_;

~~~


### receiver_



~~~cpp

mojo::Receiver<downloads::mojom::PageHandler> receiver_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadsDOMHandler> weak_ptr_factory_{this};

~~~


### DownloadsDOMHandler

DownloadsDOMHandler
~~~cpp
DownloadsDOMHandler(const DownloadsDOMHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsDOMHandler& operator=(const DownloadsDOMHandler&) = delete;
~~~

### PrimaryMainFrameRenderProcessGone

DownloadsDOMHandler::PrimaryMainFrameRenderProcessGone
~~~cpp
void PrimaryMainFrameRenderProcessGone(
      base::TerminationStatus status) override;
~~~
 WebContentsObserver implementation.

### GetDownloads

DownloadsDOMHandler::GetDownloads
~~~cpp
void GetDownloads(const std::vector<std::string>& search_terms) override;
~~~
 downloads::mojom::PageHandler:
### OpenFileRequiringGesture

DownloadsDOMHandler::OpenFileRequiringGesture
~~~cpp
void OpenFileRequiringGesture(const std::string& id) override;
~~~

### Drag

DownloadsDOMHandler::Drag
~~~cpp
void Drag(const std::string& id) override;
~~~

### SaveDangerousRequiringGesture

DownloadsDOMHandler::SaveDangerousRequiringGesture
~~~cpp
void SaveDangerousRequiringGesture(const std::string& id) override;
~~~

### DiscardDangerous

DownloadsDOMHandler::DiscardDangerous
~~~cpp
void DiscardDangerous(const std::string& id) override;
~~~

### RetryDownload

DownloadsDOMHandler::RetryDownload
~~~cpp
void RetryDownload(const std::string& id) override;
~~~

### Show

DownloadsDOMHandler::Show
~~~cpp
void Show(const std::string& id) override;
~~~

### Pause

DownloadsDOMHandler::Pause
~~~cpp
void Pause(const std::string& id) override;
~~~

### Resume

DownloadsDOMHandler::Resume
~~~cpp
void Resume(const std::string& id) override;
~~~

### Remove

DownloadsDOMHandler::Remove
~~~cpp
void Remove(const std::string& id) override;
~~~

### Undo

DownloadsDOMHandler::Undo
~~~cpp
void Undo() override;
~~~

### Cancel

DownloadsDOMHandler::Cancel
~~~cpp
void Cancel(const std::string& id) override;
~~~

### ClearAll

DownloadsDOMHandler::ClearAll
~~~cpp
void ClearAll() override;
~~~

### OpenDownloadsFolderRequiringGesture

DownloadsDOMHandler::OpenDownloadsFolderRequiringGesture
~~~cpp
void OpenDownloadsFolderRequiringGesture() override;
~~~

### OpenDuringScanningRequiringGesture

DownloadsDOMHandler::OpenDuringScanningRequiringGesture
~~~cpp
void OpenDuringScanningRequiringGesture(const std::string& id) override;
~~~

### ReviewDangerousRequiringGesture

DownloadsDOMHandler::ReviewDangerousRequiringGesture
~~~cpp
void ReviewDangerousRequiringGesture(const std::string& id) override;
~~~

### DeepScan

DownloadsDOMHandler::DeepScan
~~~cpp
void DeepScan(const std::string& id) override;
~~~

### BypassDeepScanRequiringGesture

DownloadsDOMHandler::BypassDeepScanRequiringGesture
~~~cpp
void BypassDeepScanRequiringGesture(const std::string& id) override;
~~~

### GetWebUIWebContents

DownloadsDOMHandler::GetWebUIWebContents
~~~cpp
virtual content::WebContents* GetWebUIWebContents();
~~~
 These methods are for mocking so that most of this class does not actually
 depend on WebUI. The other methods that depend on WebUI are
 RegisterMessages() and HandleDrag().

### FinalizeRemovals

DownloadsDOMHandler::FinalizeRemovals
~~~cpp
void FinalizeRemovals();
~~~
 Actually remove downloads with an ID in |removals_|. This cannot be undone.

### RemoveDownloads

DownloadsDOMHandler::RemoveDownloads
~~~cpp
void RemoveDownloads(const DownloadVector& to_remove);
~~~
 Remove all downloads in |to_remove|. Safe downloads can be revived,
 dangerous ones are immediately removed. Protected for testing.

### GetMainNotifierManager

DownloadsDOMHandler::GetMainNotifierManager
~~~cpp
content::DownloadManager* GetMainNotifierManager() const;
~~~
 Convenience method to call |main_notifier_->GetManager()| while
 null-checking |main_notifier_|.

### GetOriginalNotifierManager

DownloadsDOMHandler::GetOriginalNotifierManager
~~~cpp
content::DownloadManager* GetOriginalNotifierManager() const;
~~~
 Convenience method to call |original_notifier_->GetManager()| while
 null-checking |original_notifier_|.

### ShowDangerPrompt

DownloadsDOMHandler::ShowDangerPrompt
~~~cpp
virtual void ShowDangerPrompt(download::DownloadItem* dangerous);
~~~
 Displays a native prompt asking the user for confirmation after accepting
 the dangerous download specified by |dangerous|. The function returns
 immediately, and will invoke DangerPromptAccepted() asynchronously if the
 user accepts the dangerous download. The native prompt will observe
 |dangerous| until either the dialog is dismissed or |dangerous| is no
 longer an in-progress dangerous download.

### DangerPromptDone

DownloadsDOMHandler::DangerPromptDone
~~~cpp
void DangerPromptDone(int download_id, DownloadDangerPrompt::Action action);
~~~
 Conveys danger acceptance from the DownloadDangerPrompt to the
 DownloadItem.

### IsDeletingHistoryAllowed

DownloadsDOMHandler::IsDeletingHistoryAllowed
~~~cpp
bool IsDeletingHistoryAllowed();
~~~
 Returns true if the records of any downloaded items are allowed (and able)
 to be deleted.

### GetDownloadByStringId

DownloadsDOMHandler::GetDownloadByStringId
~~~cpp
download::DownloadItem* GetDownloadByStringId(const std::string& id);
~~~
 Returns the download that is referred to by a given string |id|.

### GetDownloadById

DownloadsDOMHandler::GetDownloadById
~~~cpp
download::DownloadItem* GetDownloadById(uint32_t id);
~~~
 Returns the download with |id| or NULL if it doesn't exist.

### RemoveDownloadInArgs

DownloadsDOMHandler::RemoveDownloadInArgs
~~~cpp
void RemoveDownloadInArgs(const std::string& id);
~~~
 Removes the download specified by an ID from JavaScript in |args|.

### CheckForRemovedFiles

DownloadsDOMHandler::CheckForRemovedFiles
~~~cpp
void CheckForRemovedFiles();
~~~
 Checks whether a download's file was removed from its original location.

### list_tracker_



~~~cpp

DownloadsListTracker list_tracker_;

~~~


### removals_



~~~cpp

std::vector<IdSet> removals_;

~~~

 IDs of downloads to remove when this handler gets deleted.

### render_process_gone_



~~~cpp

bool render_process_gone_ = false;

~~~

 Whether the render process has gone.

### web_ui_



~~~cpp

raw_ptr<content::WebUI> web_ui_;

~~~


### receiver_



~~~cpp

mojo::Receiver<downloads::mojom::PageHandler> receiver_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadsDOMHandler> weak_ptr_factory_{this};

~~~

