
## class DownloadsUI

### DownloadsUI

DownloadsUI::DownloadsUI
~~~cpp
explicit DownloadsUI(content::WebUI* web_ui);
~~~

### DownloadsUI

DownloadsUI
~~~cpp
DownloadsUI(const DownloadsUI&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsUI& operator=(const DownloadsUI&) = delete;
~~~

### ~DownloadsUI

DownloadsUI::~DownloadsUI
~~~cpp
~DownloadsUI() override;
~~~

### GetFaviconResourceBytes

DownloadsUI::GetFaviconResourceBytes
~~~cpp
static base::RefCountedMemory* GetFaviconResourceBytes(
      ui::ResourceScaleFactor scale_factor);
~~~

### BindInterface

DownloadsUI::BindInterface
~~~cpp
void BindInterface(
      mojo::PendingReceiver<downloads::mojom::PageHandlerFactory> receiver);
~~~
 Instantiates the implementor of the mojom::PageHandlerFactory mojo
 interface passing the pending receiver that will be internally bound.

### CreatePageHandler

DownloadsUI::CreatePageHandler
~~~cpp
void CreatePageHandler(
      mojo::PendingRemote<downloads::mojom::Page> page,
      mojo::PendingReceiver<downloads::mojom::PageHandler> receiver) override;
~~~
 downloads::mojom::PageHandlerFactory:
### page_handler_



~~~cpp

std::unique_ptr<DownloadsDOMHandler> page_handler_;

~~~


### page_factory_receiver_



~~~cpp

mojo::Receiver<downloads::mojom::PageHandlerFactory> page_factory_receiver_{
      this};

~~~


### WEB_UI_CONTROLLER_TYPE_DECL

DownloadsUI::WEB_UI_CONTROLLER_TYPE_DECL
~~~cpp
WEB_UI_CONTROLLER_TYPE_DECL();
~~~

### DownloadsUI

DownloadsUI
~~~cpp
DownloadsUI(const DownloadsUI&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsUI& operator=(const DownloadsUI&) = delete;
~~~

### GetFaviconResourceBytes

DownloadsUI::GetFaviconResourceBytes
~~~cpp
static base::RefCountedMemory* GetFaviconResourceBytes(
      ui::ResourceScaleFactor scale_factor);
~~~

### BindInterface

DownloadsUI::BindInterface
~~~cpp
void BindInterface(
      mojo::PendingReceiver<downloads::mojom::PageHandlerFactory> receiver);
~~~
 Instantiates the implementor of the mojom::PageHandlerFactory mojo
 interface passing the pending receiver that will be internally bound.

### CreatePageHandler

DownloadsUI::CreatePageHandler
~~~cpp
void CreatePageHandler(
      mojo::PendingRemote<downloads::mojom::Page> page,
      mojo::PendingReceiver<downloads::mojom::PageHandler> receiver) override;
~~~
 downloads::mojom::PageHandlerFactory:
### page_handler_



~~~cpp

std::unique_ptr<DownloadsDOMHandler> page_handler_;

~~~


### page_factory_receiver_



~~~cpp

mojo::Receiver<downloads::mojom::PageHandlerFactory> page_factory_receiver_{
      this};

~~~

