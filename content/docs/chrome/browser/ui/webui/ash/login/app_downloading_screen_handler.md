
## class AppDownloadingScreenView

### kScreenId



~~~cpp

inline constexpr static StaticOobeScreenId kScreenId{"app-downloading",
                                                       "AppDownloadingScreen"};

~~~


### ~AppDownloadingScreenView

~AppDownloadingScreenView
~~~cpp
virtual ~AppDownloadingScreenView() = default;
~~~

### Show

Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows the contents of the screen.

### ~AppDownloadingScreenView

~AppDownloadingScreenView
~~~cpp
virtual ~AppDownloadingScreenView() = default;
~~~

### Show

Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows the contents of the screen.

### kScreenId



~~~cpp

inline constexpr static StaticOobeScreenId kScreenId{"app-downloading",
                                                       "AppDownloadingScreen"};

~~~


## class AppDownloadingScreenHandler
 The sole implementation of the AppDownloadingScreenView, using WebUI.

### AppDownloadingScreenHandler

AppDownloadingScreenHandler::AppDownloadingScreenHandler
~~~cpp
AppDownloadingScreenHandler();
~~~

### AppDownloadingScreenHandler

AppDownloadingScreenHandler
~~~cpp
AppDownloadingScreenHandler(const AppDownloadingScreenHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
AppDownloadingScreenHandler& operator=(const AppDownloadingScreenHandler&) =
      delete;
~~~

### ~AppDownloadingScreenHandler

AppDownloadingScreenHandler::~AppDownloadingScreenHandler
~~~cpp
~AppDownloadingScreenHandler() override;
~~~

### DeclareLocalizedValues

AppDownloadingScreenHandler::DeclareLocalizedValues
~~~cpp
void DeclareLocalizedValues(
      ::login::LocalizedValuesBuilder* builder) override;
~~~
 BaseScreenHandler:
### Show

AppDownloadingScreenHandler::Show
~~~cpp
void Show() final;
~~~
 AppDownloadingScreenView:
### AppDownloadingScreenHandler

AppDownloadingScreenHandler
~~~cpp
AppDownloadingScreenHandler(const AppDownloadingScreenHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
AppDownloadingScreenHandler& operator=(const AppDownloadingScreenHandler&) =
      delete;
~~~

### DeclareLocalizedValues

AppDownloadingScreenHandler::DeclareLocalizedValues
~~~cpp
void DeclareLocalizedValues(
      ::login::LocalizedValuesBuilder* builder) override;
~~~
 BaseScreenHandler:
### Show

AppDownloadingScreenHandler::Show
~~~cpp
void Show() final;
~~~
 AppDownloadingScreenView: