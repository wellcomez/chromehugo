
## class DownloadBubbleNavigationHandler

### OpenPrimaryDialog

OpenPrimaryDialog
~~~cpp
virtual void OpenPrimaryDialog() = 0;
~~~
 Primary dialog is either main or partial view.

### OpenSecurityDialog

OpenSecurityDialog
~~~cpp
virtual void OpenSecurityDialog(DownloadBubbleRowView* download_row_view) = 0;
~~~

### CloseDialog

CloseDialog
~~~cpp
virtual void CloseDialog(views::Widget::ClosedReason reason) = 0;
~~~

### ResizeDialog

ResizeDialog
~~~cpp
virtual void ResizeDialog() = 0;
~~~

### OpenPrimaryDialog

OpenPrimaryDialog
~~~cpp
virtual void OpenPrimaryDialog() = 0;
~~~
 Primary dialog is either main or partial view.

### OpenSecurityDialog

OpenSecurityDialog
~~~cpp
virtual void OpenSecurityDialog(DownloadBubbleRowView* download_row_view) = 0;
~~~

### CloseDialog

CloseDialog
~~~cpp
virtual void CloseDialog(views::Widget::ClosedReason reason) = 0;
~~~

### ResizeDialog

ResizeDialog
~~~cpp
virtual void ResizeDialog() = 0;
~~~

## class DownloadToolbarButtonView
 Download icon shown in the trusted area of the toolbar. Its lifetime is tied
 to that of its parent ToolbarView. The icon is made visible when downloads
 are in progress or when a download was initiated in the past 24 hours.

 When there are multiple downloads, a circular badge in the corner of the icon
 displays the number of ongoing downloads.

### METADATA_HEADER

DownloadToolbarButtonView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadToolbarButtonView);
~~~

### DownloadToolbarButtonView

DownloadToolbarButtonView::DownloadToolbarButtonView
~~~cpp
explicit DownloadToolbarButtonView(BrowserView* browser_view);
~~~

### DownloadToolbarButtonView

DownloadToolbarButtonView
~~~cpp
DownloadToolbarButtonView(const DownloadToolbarButtonView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadToolbarButtonView& operator=(const DownloadToolbarButtonView&) =
      delete;
~~~

### ~DownloadToolbarButtonView

DownloadToolbarButtonView::~DownloadToolbarButtonView
~~~cpp
~DownloadToolbarButtonView() override;
~~~

### Show

DownloadToolbarButtonView::Show
~~~cpp
void Show() override;
~~~
 DownloadsDisplay implementation.

### Hide

DownloadToolbarButtonView::Hide
~~~cpp
void Hide() override;
~~~

### IsShowing

DownloadToolbarButtonView::IsShowing
~~~cpp
bool IsShowing() override;
~~~

### Enable

DownloadToolbarButtonView::Enable
~~~cpp
void Enable() override;
~~~

### Disable

DownloadToolbarButtonView::Disable
~~~cpp
void Disable() override;
~~~

### UpdateDownloadIcon

DownloadToolbarButtonView::UpdateDownloadIcon
~~~cpp
void UpdateDownloadIcon(bool show_animation) override;
~~~

### ShowDetails

DownloadToolbarButtonView::ShowDetails
~~~cpp
void ShowDetails() override;
~~~

### HideDetails

DownloadToolbarButtonView::HideDetails
~~~cpp
void HideDetails() override;
~~~

### IsShowingDetails

DownloadToolbarButtonView::IsShowingDetails
~~~cpp
bool IsShowingDetails() override;
~~~

### IsFullscreenWithParentViewHidden

DownloadToolbarButtonView::IsFullscreenWithParentViewHidden
~~~cpp
bool IsFullscreenWithParentViewHidden() override;
~~~

### UpdateIcon

DownloadToolbarButtonView::UpdateIcon
~~~cpp
void UpdateIcon() override;
~~~
 ToolbarButton:
### OnThemeChanged

DownloadToolbarButtonView::OnThemeChanged
~~~cpp
void OnThemeChanged() override;
~~~

### Layout

DownloadToolbarButtonView::Layout
~~~cpp
void Layout() override;
~~~

### ShouldShowInkdropAfterIphInteraction

DownloadToolbarButtonView::ShouldShowInkdropAfterIphInteraction
~~~cpp
bool ShouldShowInkdropAfterIphInteraction() override;
~~~

### OpenPrimaryDialog

DownloadToolbarButtonView::OpenPrimaryDialog
~~~cpp
void OpenPrimaryDialog() override;
~~~
 DownloadBubbleNavigationHandler:
### OpenSecurityDialog

DownloadToolbarButtonView::OpenSecurityDialog
~~~cpp
void OpenSecurityDialog(DownloadBubbleRowView* download_row_view) override;
~~~

### CloseDialog

DownloadToolbarButtonView::CloseDialog
~~~cpp
void CloseDialog(views::Widget::ClosedReason reason) override;
~~~

### ResizeDialog

DownloadToolbarButtonView::ResizeDialog
~~~cpp
void ResizeDialog() override;
~~~

### DeactivateAutoClose

DownloadToolbarButtonView::DeactivateAutoClose
~~~cpp
void DeactivateAutoClose();
~~~
 Deactivates the automatic closing of the partial bubble.

### bubble_controller

bubble_controller
~~~cpp
DownloadBubbleUIController* bubble_controller() {
    return bubble_controller_.get();
  }
~~~

### display_controller

display_controller
~~~cpp
DownloadDisplayController* display_controller() { return controller_.get(); }
~~~

### GetIconColor

DownloadToolbarButtonView::GetIconColor
~~~cpp
SkColor GetIconColor() const;
~~~

### SetIconColor

DownloadToolbarButtonView::SetIconColor
~~~cpp
void SetIconColor(SkColor color);
~~~

### kMaxDownloadCountDisplayed



~~~cpp

static constexpr int kMaxDownloadCountDisplayed = 9;

~~~

 Max download count to show in the badge. Any higher number of downloads
 results in a placeholder ("9+").

### PaintButtonContents

DownloadToolbarButtonView::PaintButtonContents
~~~cpp
void PaintButtonContents(gfx::Canvas* canvas) override;
~~~
 views::Button overrides:
### GetBadgeImage

DownloadToolbarButtonView::GetBadgeImage
~~~cpp
gfx::ImageSkia GetBadgeImage(bool is_active,
                               int progress_download_count,
                               SkColor badge_text_color,
                               SkColor badge_background_color);
~~~

### ButtonPressed

DownloadToolbarButtonView::ButtonPressed
~~~cpp
void ButtonPressed();
~~~

### CreateBubbleDialogDelegate

DownloadToolbarButtonView::CreateBubbleDialogDelegate
~~~cpp
void CreateBubbleDialogDelegate(std::unique_ptr<View> bubble_contents_view);
~~~

### OnBubbleDelegateDeleted

DownloadToolbarButtonView::OnBubbleDelegateDeleted
~~~cpp
void OnBubbleDelegateDeleted();
~~~

### OnPartialViewClosed

DownloadToolbarButtonView::OnPartialViewClosed
~~~cpp
void OnPartialViewClosed();
~~~
 Callback invoked when the partial view is closed.

### CreateAutoCloseTimer

DownloadToolbarButtonView::CreateAutoCloseTimer
~~~cpp
void CreateAutoCloseTimer();
~~~
 Creates a timer to track the auto-close task. Does not start the timer.

### AutoClosePartialView

DownloadToolbarButtonView::AutoClosePartialView
~~~cpp
void AutoClosePartialView();
~~~
 Called to automatically close the partial view, if such closing has not
 been deactivated.

### GetPrimaryView

DownloadToolbarButtonView::GetPrimaryView
~~~cpp
std::unique_ptr<View> GetPrimaryView();
~~~
 Get the primary view, which may be the full or the partial view.

### CreateRowListView

DownloadToolbarButtonView::CreateRowListView
~~~cpp
std::unique_ptr<View> CreateRowListView(
      std::vector<DownloadUIModel::DownloadUIModelPtr> model_list);
~~~
 Create a scrollable row list view for either the full or the partial view.

### ShowPendingDownloadStartedAnimation

DownloadToolbarButtonView::ShowPendingDownloadStartedAnimation
~~~cpp
void ShowPendingDownloadStartedAnimation();
~~~
 If |has_pending_download_started_animation_| is true, shows an animation of
 a download icon moving upwards towards the toolbar icon.

### GetProgressColor

DownloadToolbarButtonView::GetProgressColor
~~~cpp
SkColor GetProgressColor(bool is_disabled, bool is_active) const;
~~~

### browser_



~~~cpp

raw_ptr<Browser> browser_;

~~~


### is_primary_partial_view_



~~~cpp

bool is_primary_partial_view_ = false;

~~~


### controller_



~~~cpp

std::unique_ptr<DownloadDisplayController> controller_;

~~~

 Controller for the DownloadToolbarButton UI.

### bubble_controller_



~~~cpp

std::unique_ptr<DownloadBubbleUIController> bubble_controller_;

~~~

 Controller for keeping track of items for both main view and partial view.

### bubble_delegate_



~~~cpp

raw_ptr<views::BubbleDialogDelegate> bubble_delegate_ = nullptr;

~~~


### primary_view_



~~~cpp

raw_ptr<View> primary_view_ = nullptr;

~~~


### security_view_



~~~cpp

raw_ptr<DownloadBubbleSecurityView> security_view_ = nullptr;

~~~


### has_pending_download_started_animation_



~~~cpp

bool has_pending_download_started_animation_ = false;

~~~

 Marks whether there is a pending download started animation. This is needed
 because the animation should only be triggered after the view has been
 laid out properly, so this provides a way to remember to show the animation
 if needed, when calling Layout().

### auto_close_bubble_timer_



~~~cpp

std::unique_ptr<base::RetainingOneShotTimer> auto_close_bubble_timer_;

~~~

 Tracks the task to automatically close the partial view after some amount
 of time open, to minimize disruption to the user.

### render_texts_



~~~cpp

std::array<std::unique_ptr<gfx::RenderText>, kMaxDownloadCountDisplayed>
      render_texts_{};

~~~

 RenderTexts used for the number in the badge. Stores the text for "n" at
 index n - 1, and stores the text for the placeholder ("9+") at index 0.

 This is done to avoid re-creating the same RenderText on each paint. Text
 color of each RenderText is reset upon each paint.

### badge_image_view_



~~~cpp

raw_ptr<views::ImageView> badge_image_view_ = nullptr;

~~~

 Badge view drawn on top of the rest of the children. It is positioned at
 the bottom right corner of this view's bounds.

### icon_color_



~~~cpp

absl::optional<SkColor> icon_color_;

~~~

 Override for the icon color. Used for PWAs, which don't have full
 ThemeProvider color support.

### scanning_animation_



~~~cpp

gfx::SlideAnimation scanning_animation_{this};

~~~


### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadToolbarButtonView> weak_factory_{this};

~~~


### DownloadToolbarButtonView

DownloadToolbarButtonView
~~~cpp
DownloadToolbarButtonView(const DownloadToolbarButtonView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadToolbarButtonView& operator=(const DownloadToolbarButtonView&) =
      delete;
~~~

### bubble_controller

bubble_controller
~~~cpp
DownloadBubbleUIController* bubble_controller() {
    return bubble_controller_.get();
  }
~~~

### display_controller

display_controller
~~~cpp
DownloadDisplayController* display_controller() { return controller_.get(); }
~~~

### Show

DownloadToolbarButtonView::Show
~~~cpp
void Show() override;
~~~
 DownloadsDisplay implementation.

### Hide

DownloadToolbarButtonView::Hide
~~~cpp
void Hide() override;
~~~

### IsShowing

DownloadToolbarButtonView::IsShowing
~~~cpp
bool IsShowing() override;
~~~

### Enable

DownloadToolbarButtonView::Enable
~~~cpp
void Enable() override;
~~~

### Disable

DownloadToolbarButtonView::Disable
~~~cpp
void Disable() override;
~~~

### UpdateDownloadIcon

DownloadToolbarButtonView::UpdateDownloadIcon
~~~cpp
void UpdateDownloadIcon(bool show_animation) override;
~~~

### ShowDetails

DownloadToolbarButtonView::ShowDetails
~~~cpp
void ShowDetails() override;
~~~

### HideDetails

DownloadToolbarButtonView::HideDetails
~~~cpp
void HideDetails() override;
~~~

### IsShowingDetails

DownloadToolbarButtonView::IsShowingDetails
~~~cpp
bool IsShowingDetails() override;
~~~

### IsFullscreenWithParentViewHidden

DownloadToolbarButtonView::IsFullscreenWithParentViewHidden
~~~cpp
bool IsFullscreenWithParentViewHidden() override;
~~~

### UpdateIcon

DownloadToolbarButtonView::UpdateIcon
~~~cpp
void UpdateIcon() override;
~~~
 ToolbarButton:
### OnThemeChanged

DownloadToolbarButtonView::OnThemeChanged
~~~cpp
void OnThemeChanged() override;
~~~

### Layout

DownloadToolbarButtonView::Layout
~~~cpp
void Layout() override;
~~~

### ShouldShowInkdropAfterIphInteraction

DownloadToolbarButtonView::ShouldShowInkdropAfterIphInteraction
~~~cpp
bool ShouldShowInkdropAfterIphInteraction() override;
~~~

### OpenPrimaryDialog

DownloadToolbarButtonView::OpenPrimaryDialog
~~~cpp
void OpenPrimaryDialog() override;
~~~
 DownloadBubbleNavigationHandler:
### OpenSecurityDialog

DownloadToolbarButtonView::OpenSecurityDialog
~~~cpp
void OpenSecurityDialog(DownloadBubbleRowView* download_row_view) override;
~~~

### CloseDialog

DownloadToolbarButtonView::CloseDialog
~~~cpp
void CloseDialog(views::Widget::ClosedReason reason) override;
~~~

### ResizeDialog

DownloadToolbarButtonView::ResizeDialog
~~~cpp
void ResizeDialog() override;
~~~

### DeactivateAutoClose

DownloadToolbarButtonView::DeactivateAutoClose
~~~cpp
void DeactivateAutoClose();
~~~
 Deactivates the automatic closing of the partial bubble.

### GetIconColor

DownloadToolbarButtonView::GetIconColor
~~~cpp
SkColor GetIconColor() const;
~~~

### SetIconColor

DownloadToolbarButtonView::SetIconColor
~~~cpp
void SetIconColor(SkColor color);
~~~

### kMaxDownloadCountDisplayed



~~~cpp

static constexpr int kMaxDownloadCountDisplayed = 9;

~~~

 Max download count to show in the badge. Any higher number of downloads
 results in a placeholder ("9+").

### PaintButtonContents

DownloadToolbarButtonView::PaintButtonContents
~~~cpp
void PaintButtonContents(gfx::Canvas* canvas) override;
~~~
 views::Button overrides:
### GetBadgeImage

DownloadToolbarButtonView::GetBadgeImage
~~~cpp
gfx::ImageSkia GetBadgeImage(bool is_active,
                               int progress_download_count,
                               SkColor badge_text_color,
                               SkColor badge_background_color);
~~~

### ButtonPressed

DownloadToolbarButtonView::ButtonPressed
~~~cpp
void ButtonPressed();
~~~

### CreateBubbleDialogDelegate

DownloadToolbarButtonView::CreateBubbleDialogDelegate
~~~cpp
void CreateBubbleDialogDelegate(std::unique_ptr<View> bubble_contents_view);
~~~

### OnBubbleDelegateDeleted

DownloadToolbarButtonView::OnBubbleDelegateDeleted
~~~cpp
void OnBubbleDelegateDeleted();
~~~

### OnPartialViewClosed

DownloadToolbarButtonView::OnPartialViewClosed
~~~cpp
void OnPartialViewClosed();
~~~
 Callback invoked when the partial view is closed.

### CreateAutoCloseTimer

DownloadToolbarButtonView::CreateAutoCloseTimer
~~~cpp
void CreateAutoCloseTimer();
~~~
 Creates a timer to track the auto-close task. Does not start the timer.

### AutoClosePartialView

DownloadToolbarButtonView::AutoClosePartialView
~~~cpp
void AutoClosePartialView();
~~~
 Called to automatically close the partial view, if such closing has not
 been deactivated.

### GetPrimaryView

DownloadToolbarButtonView::GetPrimaryView
~~~cpp
std::unique_ptr<View> GetPrimaryView();
~~~
 Get the primary view, which may be the full or the partial view.

### CreateRowListView

DownloadToolbarButtonView::CreateRowListView
~~~cpp
std::unique_ptr<View> CreateRowListView(
      std::vector<DownloadUIModel::DownloadUIModelPtr> model_list);
~~~
 Create a scrollable row list view for either the full or the partial view.

### ShowPendingDownloadStartedAnimation

DownloadToolbarButtonView::ShowPendingDownloadStartedAnimation
~~~cpp
void ShowPendingDownloadStartedAnimation();
~~~
 If |has_pending_download_started_animation_| is true, shows an animation of
 a download icon moving upwards towards the toolbar icon.

### GetProgressColor

DownloadToolbarButtonView::GetProgressColor
~~~cpp
SkColor GetProgressColor(bool is_disabled, bool is_active) const;
~~~

### browser_



~~~cpp

raw_ptr<Browser> browser_;

~~~


### is_primary_partial_view_



~~~cpp

bool is_primary_partial_view_ = false;

~~~


### controller_



~~~cpp

std::unique_ptr<DownloadDisplayController> controller_;

~~~

 Controller for the DownloadToolbarButton UI.

### bubble_controller_



~~~cpp

std::unique_ptr<DownloadBubbleUIController> bubble_controller_;

~~~

 Controller for keeping track of items for both main view and partial view.

### bubble_delegate_



~~~cpp

raw_ptr<views::BubbleDialogDelegate> bubble_delegate_ = nullptr;

~~~


### primary_view_



~~~cpp

raw_ptr<View> primary_view_ = nullptr;

~~~


### security_view_



~~~cpp

raw_ptr<DownloadBubbleSecurityView> security_view_ = nullptr;

~~~


### has_pending_download_started_animation_



~~~cpp

bool has_pending_download_started_animation_ = false;

~~~

 Marks whether there is a pending download started animation. This is needed
 because the animation should only be triggered after the view has been
 laid out properly, so this provides a way to remember to show the animation
 if needed, when calling Layout().

### auto_close_bubble_timer_



~~~cpp

std::unique_ptr<base::RetainingOneShotTimer> auto_close_bubble_timer_;

~~~

 Tracks the task to automatically close the partial view after some amount
 of time open, to minimize disruption to the user.

### render_texts_



~~~cpp

std::array<std::unique_ptr<gfx::RenderText>, kMaxDownloadCountDisplayed>
      render_texts_{};

~~~

 RenderTexts used for the number in the badge. Stores the text for "n" at
 index n - 1, and stores the text for the placeholder ("9+") at index 0.

 This is done to avoid re-creating the same RenderText on each paint. Text
 color of each RenderText is reset upon each paint.

### badge_image_view_



~~~cpp

raw_ptr<views::ImageView> badge_image_view_ = nullptr;

~~~

 Badge view drawn on top of the rest of the children. It is positioned at
 the bottom right corner of this view's bounds.

### icon_color_



~~~cpp

absl::optional<SkColor> icon_color_;

~~~

 Override for the icon color. Used for PWAs, which don't have full
 ThemeProvider color support.

### scanning_animation_



~~~cpp

gfx::SlideAnimation scanning_animation_{this};

~~~


### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadToolbarButtonView> weak_factory_{this};

~~~

