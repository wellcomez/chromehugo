
## class DownloadBubbleSecurityView

### METADATA_HEADER

DownloadBubbleSecurityView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadBubbleSecurityView);
~~~

### DownloadBubbleSecurityView

DownloadBubbleSecurityView::DownloadBubbleSecurityView
~~~cpp
DownloadBubbleSecurityView(
      DownloadBubbleUIController* bubble_controller,
      DownloadBubbleNavigationHandler* navigation_handler,
      views::BubbleDialogDelegate* bubble_delegate);
~~~

### DownloadBubbleSecurityView

DownloadBubbleSecurityView
~~~cpp
DownloadBubbleSecurityView(const DownloadBubbleSecurityView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleSecurityView& operator=(const DownloadBubbleSecurityView&) =
      delete;
~~~

### ~DownloadBubbleSecurityView

DownloadBubbleSecurityView::~DownloadBubbleSecurityView
~~~cpp
~DownloadBubbleSecurityView() override;
~~~

### UpdateSecurityView

DownloadBubbleSecurityView::UpdateSecurityView
~~~cpp
void UpdateSecurityView(DownloadBubbleRowView* download_row_view);
~~~
 Update the security view when a subpage is opened for a particular
 download.

### UpdateAccessibilityTextAndFocus

DownloadBubbleSecurityView::UpdateAccessibilityTextAndFocus
~~~cpp
void UpdateAccessibilityTextAndFocus();
~~~
 Update the view after it is visible, in particular asking for focus and
 announcing accessibility text.

### FRIEND_TEST_ALL_PREFIXES

DownloadBubbleSecurityView::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadBubbleSecurityViewTest,
                           VerifyLogWarningActions);
~~~

### BackButtonPressed

DownloadBubbleSecurityView::BackButtonPressed
~~~cpp
void BackButtonPressed();
~~~

### UpdateHeader

DownloadBubbleSecurityView::UpdateHeader
~~~cpp
void UpdateHeader();
~~~

### AddHeader

DownloadBubbleSecurityView::AddHeader
~~~cpp
void AddHeader();
~~~

### CloseBubble

DownloadBubbleSecurityView::CloseBubble
~~~cpp
void CloseBubble();
~~~

### OnCheckboxClicked

DownloadBubbleSecurityView::OnCheckboxClicked
~~~cpp
void OnCheckboxClicked();
~~~

### UpdateIconAndText

DownloadBubbleSecurityView::UpdateIconAndText
~~~cpp
void UpdateIconAndText();
~~~

### AddIconAndText

DownloadBubbleSecurityView::AddIconAndText
~~~cpp
void AddIconAndText();
~~~

### UpdateButton

DownloadBubbleSecurityView::UpdateButton
~~~cpp
void UpdateButton(DownloadUIModel::BubbleUIInfo::SubpageButton button,
                    bool is_secondary_button,
                    bool has_checkbox,
                    ui::ColorId color_id);
~~~
 Updates the subpage button. Setting initial state and color for enabled
 state, if it is a secondary button.

### UpdateButtons

DownloadBubbleSecurityView::UpdateButtons
~~~cpp
void UpdateButtons();
~~~

### ProcessButtonClick

DownloadBubbleSecurityView::ProcessButtonClick
~~~cpp
void ProcessButtonClick(DownloadCommands::Command command,
                          bool is_secondary_button);
~~~
 |is_secondary_button| checks if the command/action originated from the
 secondary button.

### RecordWarningActionTime

DownloadBubbleSecurityView::RecordWarningActionTime
~~~cpp
void RecordWarningActionTime(bool is_secondary_button);
~~~

### download_row_view_



~~~cpp

raw_ptr<DownloadBubbleRowView> download_row_view_;

~~~


### model_



~~~cpp

DownloadUIModel::DownloadUIModelPtr model_;

~~~


### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_ = nullptr;

~~~


### navigation_handler_



~~~cpp

raw_ptr<DownloadBubbleNavigationHandler> navigation_handler_ = nullptr;

~~~


### bubble_delegate_



~~~cpp

raw_ptr<views::BubbleDialogDelegate, DanglingUntriaged> bubble_delegate_ =
      nullptr;

~~~


### secondary_button_



~~~cpp

raw_ptr<views::LabelButton> secondary_button_ = nullptr;

~~~

 The secondary button is the one that may be protected by the checkbox.

### checkbox_



~~~cpp

raw_ptr<views::Checkbox> checkbox_ = nullptr;

~~~


### title_



~~~cpp

raw_ptr<views::Label> title_ = nullptr;

~~~


### icon_



~~~cpp

raw_ptr<views::ImageView> icon_ = nullptr;

~~~


### styled_label_



~~~cpp

raw_ptr<views::StyledLabel> styled_label_ = nullptr;

~~~


### back_button_



~~~cpp

raw_ptr<views::ImageButton> back_button_ = nullptr;

~~~


### warning_time_



~~~cpp

absl::optional<base::Time> warning_time_;

~~~


### did_log_action_



~~~cpp

bool did_log_action_ = false;

~~~


### DownloadBubbleSecurityView

DownloadBubbleSecurityView
~~~cpp
DownloadBubbleSecurityView(const DownloadBubbleSecurityView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleSecurityView& operator=(const DownloadBubbleSecurityView&) =
      delete;
~~~

### UpdateSecurityView

DownloadBubbleSecurityView::UpdateSecurityView
~~~cpp
void UpdateSecurityView(DownloadBubbleRowView* download_row_view);
~~~
 Update the security view when a subpage is opened for a particular
 download.

### UpdateAccessibilityTextAndFocus

DownloadBubbleSecurityView::UpdateAccessibilityTextAndFocus
~~~cpp
void UpdateAccessibilityTextAndFocus();
~~~
 Update the view after it is visible, in particular asking for focus and
 announcing accessibility text.

### BackButtonPressed

DownloadBubbleSecurityView::BackButtonPressed
~~~cpp
void BackButtonPressed();
~~~

### UpdateHeader

DownloadBubbleSecurityView::UpdateHeader
~~~cpp
void UpdateHeader();
~~~

### AddHeader

DownloadBubbleSecurityView::AddHeader
~~~cpp
void AddHeader();
~~~

### CloseBubble

DownloadBubbleSecurityView::CloseBubble
~~~cpp
void CloseBubble();
~~~

### OnCheckboxClicked

DownloadBubbleSecurityView::OnCheckboxClicked
~~~cpp
void OnCheckboxClicked();
~~~

### UpdateIconAndText

DownloadBubbleSecurityView::UpdateIconAndText
~~~cpp
void UpdateIconAndText();
~~~

### AddIconAndText

DownloadBubbleSecurityView::AddIconAndText
~~~cpp
void AddIconAndText();
~~~

### UpdateButton

DownloadBubbleSecurityView::UpdateButton
~~~cpp
void UpdateButton(DownloadUIModel::BubbleUIInfo::SubpageButton button,
                    bool is_secondary_button,
                    bool has_checkbox,
                    ui::ColorId color_id);
~~~
 Updates the subpage button. Setting initial state and color for enabled
 state, if it is a secondary button.

### UpdateButtons

DownloadBubbleSecurityView::UpdateButtons
~~~cpp
void UpdateButtons();
~~~

### ProcessButtonClick

DownloadBubbleSecurityView::ProcessButtonClick
~~~cpp
void ProcessButtonClick(DownloadCommands::Command command,
                          bool is_secondary_button);
~~~
 |is_secondary_button| checks if the command/action originated from the
 secondary button.

### RecordWarningActionTime

DownloadBubbleSecurityView::RecordWarningActionTime
~~~cpp
void RecordWarningActionTime(bool is_secondary_button);
~~~

### download_row_view_



~~~cpp

raw_ptr<DownloadBubbleRowView> download_row_view_;

~~~


### model_



~~~cpp

DownloadUIModel::DownloadUIModelPtr model_;

~~~


### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_ = nullptr;

~~~


### navigation_handler_



~~~cpp

raw_ptr<DownloadBubbleNavigationHandler> navigation_handler_ = nullptr;

~~~


### bubble_delegate_



~~~cpp

raw_ptr<views::BubbleDialogDelegate, DanglingUntriaged> bubble_delegate_ =
      nullptr;

~~~


### secondary_button_



~~~cpp

raw_ptr<views::LabelButton> secondary_button_ = nullptr;

~~~

 The secondary button is the one that may be protected by the checkbox.

### checkbox_



~~~cpp

raw_ptr<views::Checkbox> checkbox_ = nullptr;

~~~


### title_



~~~cpp

raw_ptr<views::Label> title_ = nullptr;

~~~


### icon_



~~~cpp

raw_ptr<views::ImageView> icon_ = nullptr;

~~~


### styled_label_



~~~cpp

raw_ptr<views::StyledLabel> styled_label_ = nullptr;

~~~


### back_button_



~~~cpp

raw_ptr<views::ImageButton> back_button_ = nullptr;

~~~


### warning_time_



~~~cpp

absl::optional<base::Time> warning_time_;

~~~


### did_log_action_



~~~cpp

bool did_log_action_ = false;

~~~

