
## class DownloadBubbleRowListView
 namespace views
### METADATA_HEADER

DownloadBubbleRowListView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadBubbleRowListView);
~~~

### DownloadBubbleRowListView

DownloadBubbleRowListView::DownloadBubbleRowListView
~~~cpp
DownloadBubbleRowListView(
      bool is_partial_view,
      Browser* browser,
      base::OnceClosure on_mouse_entered_closure = base::DoNothing());
~~~

### ~DownloadBubbleRowListView

DownloadBubbleRowListView::~DownloadBubbleRowListView
~~~cpp
~DownloadBubbleRowListView() override;
~~~

### DownloadBubbleRowListView

DownloadBubbleRowListView
~~~cpp
DownloadBubbleRowListView(const DownloadBubbleRowListView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleRowListView& operator=(const DownloadBubbleRowListView&) =
      delete;
~~~

### OnMouseEntered

DownloadBubbleRowListView::OnMouseEntered
~~~cpp
void OnMouseEntered(const ui::MouseEvent& event) override;
~~~
 views::FlexLayoutView
### IsIncognitoInfoRowEnabled

DownloadBubbleRowListView::IsIncognitoInfoRowEnabled
~~~cpp
bool IsIncognitoInfoRowEnabled();
~~~

### is_partial_view_



~~~cpp

bool is_partial_view_;

~~~


### creation_time_



~~~cpp

base::Time creation_time_;

~~~


### browser_



~~~cpp

raw_ptr<Browser> browser_ = nullptr;

~~~


### info_icon_



~~~cpp

raw_ptr<views::ImageView> info_icon_ = nullptr;

~~~


### on_mouse_entered_closure_



~~~cpp

base::OnceClosure on_mouse_entered_closure_;

~~~

 Callback invoked when the user first hovers over the view.

### DownloadBubbleRowListView

DownloadBubbleRowListView
~~~cpp
DownloadBubbleRowListView(const DownloadBubbleRowListView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleRowListView& operator=(const DownloadBubbleRowListView&) =
      delete;
~~~

### OnMouseEntered

DownloadBubbleRowListView::OnMouseEntered
~~~cpp
void OnMouseEntered(const ui::MouseEvent& event) override;
~~~
 views::FlexLayoutView
### IsIncognitoInfoRowEnabled

DownloadBubbleRowListView::IsIncognitoInfoRowEnabled
~~~cpp
bool IsIncognitoInfoRowEnabled();
~~~

### is_partial_view_



~~~cpp

bool is_partial_view_;

~~~


### creation_time_



~~~cpp

base::Time creation_time_;

~~~


### browser_



~~~cpp

raw_ptr<Browser> browser_ = nullptr;

~~~


### info_icon_



~~~cpp

raw_ptr<views::ImageView> info_icon_ = nullptr;

~~~


### on_mouse_entered_closure_



~~~cpp

base::OnceClosure on_mouse_entered_closure_;

~~~

 Callback invoked when the user first hovers over the view.
