
## class DownloadBubbleRowView

### METADATA_HEADER

DownloadBubbleRowView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadBubbleRowView);
~~~

### DownloadBubbleRowView

DownloadBubbleRowView::DownloadBubbleRowView
~~~cpp
explicit DownloadBubbleRowView(
      DownloadUIModel::DownloadUIModelPtr model,
      DownloadBubbleRowListView* row_list_view,
      DownloadBubbleUIController* bubble_controller,
      DownloadBubbleNavigationHandler* navigation_handler,
      Browser* browser);
~~~

### DownloadBubbleRowView

DownloadBubbleRowView
~~~cpp
DownloadBubbleRowView(const DownloadBubbleRowView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleRowView& operator=(const DownloadBubbleRowView&) = delete;
~~~

### ~DownloadBubbleRowView

DownloadBubbleRowView::~DownloadBubbleRowView
~~~cpp
~DownloadBubbleRowView() override;
~~~

### AddedToWidget

DownloadBubbleRowView::AddedToWidget
~~~cpp
void AddedToWidget() override;
~~~
 Overrides views::View:
### RemovedFromWidget

DownloadBubbleRowView::RemovedFromWidget
~~~cpp
void RemovedFromWidget() override;
~~~

### Layout

DownloadBubbleRowView::Layout
~~~cpp
void Layout() override;
~~~

### GetChildrenInZOrder

DownloadBubbleRowView::GetChildrenInZOrder
~~~cpp
Views GetChildrenInZOrder() override;
~~~

### OnMouseDragged

DownloadBubbleRowView::OnMouseDragged
~~~cpp
bool OnMouseDragged(const ui::MouseEvent& event) override;
~~~

### OnMouseCaptureLost

DownloadBubbleRowView::OnMouseCaptureLost
~~~cpp
void OnMouseCaptureLost() override;
~~~

### OnMouseEntered

DownloadBubbleRowView::OnMouseEntered
~~~cpp
void OnMouseEntered(const ui::MouseEvent& event) override;
~~~

### OnMouseExited

DownloadBubbleRowView::OnMouseExited
~~~cpp
void OnMouseExited(const ui::MouseEvent& event) override;
~~~

### CalculatePreferredSize

DownloadBubbleRowView::CalculatePreferredSize
~~~cpp
gfx::Size CalculatePreferredSize() const override;
~~~

### AddLayerToRegion

DownloadBubbleRowView::AddLayerToRegion
~~~cpp
void AddLayerToRegion(ui::Layer* layer, views::LayerRegion region) override;
~~~

### RemoveLayerFromRegions

DownloadBubbleRowView::RemoveLayerFromRegions
~~~cpp
void RemoveLayerFromRegions(ui::Layer* layer) override;
~~~

### OnWillChangeFocus

DownloadBubbleRowView::OnWillChangeFocus
~~~cpp
void OnWillChangeFocus(views::View* before, views::View* now) override;
~~~
 Overrides views::FocusChangeListener
### OnDidChangeFocus

OnDidChangeFocus
~~~cpp
void OnDidChangeFocus(views::View* before, views::View* now) override {}
~~~

### UpdateRowForHover

DownloadBubbleRowView::UpdateRowForHover
~~~cpp
void UpdateRowForHover(bool hovered);
~~~
 Update the row and its elements for hover and focus events.

### UpdateRowForFocus

DownloadBubbleRowView::UpdateRowForFocus
~~~cpp
void UpdateRowForFocus(bool visible, bool request_focus_on_last_quick_action);
~~~

### OnDownloadOpened

DownloadBubbleRowView::OnDownloadOpened
~~~cpp
void OnDownloadOpened() override;
~~~
 Overrides DownloadUIModel::Delegate:
### OnDownloadUpdated

DownloadBubbleRowView::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated() override;
~~~

### OnDownloadDestroyed

DownloadBubbleRowView::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& id) override;
~~~

### ShowContextMenuForViewImpl

DownloadBubbleRowView::ShowContextMenuForViewImpl
~~~cpp
void ShowContextMenuForViewImpl(View* source,
                                  const gfx::Point& point,
                                  ui::MenuSourceType source_type) override;
~~~
 Overrides views::ContextMenuController:
### AcceleratorPressed

DownloadBubbleRowView::AcceleratorPressed
~~~cpp
bool AcceleratorPressed(const ui::Accelerator& accelerator) override;
~~~
 Overrides ui::AcceleratorTarget
### CanHandleAccelerators

DownloadBubbleRowView::CanHandleAccelerators
~~~cpp
bool CanHandleAccelerators() const override;
~~~

###  GetSecondaryLabelTextForTesting


~~~cpp
const std::u16string& GetSecondaryLabelTextForTesting();
~~~
### model

model
~~~cpp
DownloadUIModel* model() { return model_.get(); }
~~~

### ui_info

ui_info
~~~cpp
DownloadUIModel::BubbleUIInfo& ui_info() { return ui_info_; }
~~~

### SetUIInfoForTesting

SetUIInfoForTesting
~~~cpp
void SetUIInfoForTesting(DownloadUIModel::BubbleUIInfo ui_info) {
    ui_info_ = ui_info;
  }
~~~

### OnDeviceScaleFactorChanged

DownloadBubbleRowView::OnDeviceScaleFactorChanged
~~~cpp
void OnDeviceScaleFactorChanged(float old_device_scale_factor,
                                  float new_device_scale_factor) override;
~~~
 Overrides ui::LayerDelegate:
### AddMainPageButton

DownloadBubbleRowView::AddMainPageButton
~~~cpp
views::MdTextButton* AddMainPageButton(DownloadCommands::Command command,
                                         const std::u16string& button_string);
~~~

### AddQuickAction

DownloadBubbleRowView::AddQuickAction
~~~cpp
views::ImageButton* AddQuickAction(DownloadCommands::Command command);
~~~

### GetActionButtonForCommand

DownloadBubbleRowView::GetActionButtonForCommand
~~~cpp
views::ImageButton* GetActionButtonForCommand(
      DownloadCommands::Command command);
~~~

### GetAccessibleNameForQuickAction

DownloadBubbleRowView::GetAccessibleNameForQuickAction
~~~cpp
std::u16string GetAccessibleNameForQuickAction(
      DownloadCommands::Command command);
~~~

### GetMainPageButton

DownloadBubbleRowView::GetMainPageButton
~~~cpp
views::MdTextButton* GetMainPageButton(DownloadCommands::Command command);
~~~

### GetAccessibleNameForMainPageButton

DownloadBubbleRowView::GetAccessibleNameForMainPageButton
~~~cpp
std::u16string GetAccessibleNameForMainPageButton(
      DownloadCommands::Command command);
~~~

### UpdateBubbleUIInfo

DownloadBubbleRowView::UpdateBubbleUIInfo
~~~cpp
bool UpdateBubbleUIInfo(bool initial_setup);
~~~
 If there is any change in state, update UI info.

 Returns whether the ui info was changed.

### UpdateRow

DownloadBubbleRowView::UpdateRow
~~~cpp
void UpdateRow(bool initial_setup);
~~~
 Update the DownloadBubbleRowView's members.

### UpdateStatusText

DownloadBubbleRowView::UpdateStatusText
~~~cpp
void UpdateStatusText();
~~~

### UpdateButtons

DownloadBubbleRowView::UpdateButtons
~~~cpp
void UpdateButtons();
~~~

### UpdateProgressBar

DownloadBubbleRowView::UpdateProgressBar
~~~cpp
void UpdateProgressBar();
~~~

### UpdateLabels

DownloadBubbleRowView::UpdateLabels
~~~cpp
void UpdateLabels();
~~~

### RecordMetricsOnUpdate

DownloadBubbleRowView::RecordMetricsOnUpdate
~~~cpp
void RecordMetricsOnUpdate();
~~~

### RecordDownloadDisplayed

DownloadBubbleRowView::RecordDownloadDisplayed
~~~cpp
void RecordDownloadDisplayed();
~~~

### StartLoadFileIcon

DownloadBubbleRowView::StartLoadFileIcon
~~~cpp
bool StartLoadFileIcon();
~~~
 Load the appropriate |file_icon_| from the IconManager, or a default icon.

 Returns whether we were able to synchronously set |icon_| to an appropriate
 icon for the file path.

### OnFileIconLoaded

DownloadBubbleRowView::OnFileIconLoaded
~~~cpp
void OnFileIconLoaded(gfx::Image icon);
~~~
 Callback invoked when the IconManager's asynchronous lookup returns.

### SetFileIconAsIcon

DownloadBubbleRowView::SetFileIconAsIcon
~~~cpp
void SetFileIconAsIcon(bool is_default_icon);
~~~
 Sets |icon_| to the image in |file_icon_|.

### SetIcon

DownloadBubbleRowView::SetIcon
~~~cpp
void SetIcon();
~~~
 Set the |icon_|, which may be an override (warning or incognito icon),
 default icon, or loaded from the cache or from IconManager::LoadIcon.

### SetIconFromImage

DownloadBubbleRowView::SetIconFromImage
~~~cpp
void SetIconFromImage(gfx::Image icon);
~~~
 Sets |icon_| to |icon|, regardless of what kind of icon it is.

### SetIconFromImageModel

DownloadBubbleRowView::SetIconFromImageModel
~~~cpp
void SetIconFromImageModel(const ui::ImageModel& icon);
~~~

### OnCancelButtonPressed

DownloadBubbleRowView::OnCancelButtonPressed
~~~cpp
void OnCancelButtonPressed();
~~~

### OnDiscardButtonPressed

DownloadBubbleRowView::OnDiscardButtonPressed
~~~cpp
void OnDiscardButtonPressed();
~~~

### OnMainButtonPressed

DownloadBubbleRowView::OnMainButtonPressed
~~~cpp
void OnMainButtonPressed();
~~~

### AnnounceInProgressAlert

DownloadBubbleRowView::AnnounceInProgressAlert
~~~cpp
void AnnounceInProgressAlert();
~~~

### RegisterAccelerators

DownloadBubbleRowView::RegisterAccelerators
~~~cpp
void RegisterAccelerators(views::FocusManager* focus_manager);
~~~
 Registers/unregisters copy accelerator for copy/paste support.

### UnregisterAccelerators

DownloadBubbleRowView::UnregisterAccelerators
~~~cpp
void UnregisterAccelerators(views::FocusManager* focus_manager);
~~~

### icon_



~~~cpp

raw_ptr<views::ImageView> icon_ = nullptr;

~~~

 The icon for the file. We get platform-specific file type icons from
 IconLoader (see below).

### subpage_icon_



~~~cpp

raw_ptr<views::ImageView> subpage_icon_ = nullptr;

~~~


### subpage_icon_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> subpage_icon_holder_ = nullptr;

~~~


### file_icon_



~~~cpp

gfx::Image file_icon_;

~~~

 The icon for the filetype, fetched from the platform-specific IconLoader.

 This can differ from the image in |icon_| if |icon_| is not the file type
 icon, e.g. if it is the incognito icon or a warning icon. We cache it here
 in case |icon_| is different, because it is used when drag-and-dropping.

 If the IconLoader does not return a file icon, this stores a default icon.

### primary_label_



~~~cpp

raw_ptr<views::Label> primary_label_ = nullptr;

~~~

 The primary label.

### secondary_label_



~~~cpp

raw_ptr<views::Label> secondary_label_ = nullptr;

~~~

 The secondary label.

### cancel_button_



~~~cpp

raw_ptr<views::MdTextButton> cancel_button_ = nullptr;

~~~

 Buttons on the main page.

### discard_button_



~~~cpp

raw_ptr<views::MdTextButton> discard_button_ = nullptr;

~~~


### keep_button_



~~~cpp

raw_ptr<views::MdTextButton> keep_button_ = nullptr;

~~~


### scan_button_



~~~cpp

raw_ptr<views::MdTextButton> scan_button_ = nullptr;

~~~


### open_now_button_



~~~cpp

raw_ptr<views::MdTextButton> open_now_button_ = nullptr;

~~~


### resume_button_



~~~cpp

raw_ptr<views::MdTextButton> resume_button_ = nullptr;

~~~


### review_button_



~~~cpp

raw_ptr<views::MdTextButton> review_button_ = nullptr;

~~~


### retry_button_



~~~cpp

raw_ptr<views::MdTextButton> retry_button_ = nullptr;

~~~


### resume_action_



~~~cpp

raw_ptr<views::ImageButton> resume_action_ = nullptr;

~~~

 Quick Actions on the main page.

### pause_action_



~~~cpp

raw_ptr<views::ImageButton> pause_action_ = nullptr;

~~~


### show_in_folder_action_



~~~cpp

raw_ptr<views::ImageButton> show_in_folder_action_ = nullptr;

~~~


### cancel_action_



~~~cpp

raw_ptr<views::ImageButton> cancel_action_ = nullptr;

~~~


### open_when_complete_action_



~~~cpp

raw_ptr<views::ImageButton> open_when_complete_action_ = nullptr;

~~~


### main_button_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> main_button_holder_ = nullptr;

~~~

 Holder for the main button.

### quick_action_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> quick_action_holder_ = nullptr;

~~~

 Holder for the quick actions.

### progress_bar_



~~~cpp

raw_ptr<views::ProgressBar> progress_bar_ = nullptr;

~~~

 The progress bar for in-progress downloads.

### progress_bar_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> progress_bar_holder_ = nullptr;

~~~


### current_scale_



~~~cpp

float current_scale_ = 1.0f;

~~~

 Device scale factor, used to load icons.

### model_



~~~cpp

DownloadUIModel::DownloadUIModelPtr model_;

~~~

 The model controlling this object's state.

### context_menu_



~~~cpp

std::unique_ptr<DownloadShelfContextMenuView> context_menu_;

~~~

 Reuse the download shelf context menu in the bubble.

### row_list_view_



~~~cpp

raw_ptr<DownloadBubbleRowListView> row_list_view_ = nullptr;

~~~

 Parent row list view.

### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_ = nullptr;

~~~

 Controller for keeping track of downloads.

### navigation_handler_



~~~cpp

raw_ptr<DownloadBubbleNavigationHandler> navigation_handler_ = nullptr;

~~~


### browser_



~~~cpp

raw_ptr<Browser> browser_ = nullptr;

~~~


### mode_



~~~cpp

download::DownloadItemMode mode_;

~~~


### state_



~~~cpp

download::DownloadItem::DownloadState state_;

~~~


### ui_info_



~~~cpp

DownloadUIModel::BubbleUIInfo ui_info_;

~~~


### is_paused_



~~~cpp

bool is_paused_;

~~~


### last_overridden_icon_



~~~cpp

raw_ptr<const gfx::VectorIcon> last_overridden_icon_ = nullptr;

~~~

 The last override icon, e.g. an incognito or warning icon. If this is
 null, we should either use the filetype icon or a default icon.

### has_default_icon_



~~~cpp

bool has_default_icon_ = false;

~~~

 Whether the currently set |icon_| is the default icon.

### transparent_button_



~~~cpp

raw_ptr<views::Button> transparent_button_ = nullptr;

~~~

 Button for transparent button click, inkdrop animations and drag and drop
 events.

### inkdrop_container_



~~~cpp

raw_ptr<views::InkDropContainerView> inkdrop_container_;

~~~


### dragging_



~~~cpp

bool dragging_ = false;

~~~

 Drag and drop:
 Whether we are dragging the download bubble row.

### drag_start_point_



~~~cpp

absl::optional<gfx::Point> drag_start_point_;

~~~

 Position that a possible drag started at.

### has_download_completion_been_logged_



~~~cpp

bool has_download_completion_been_logged_ = false;

~~~

 Whether the download's completion has already been logged. This is used to
 avoid inaccurate repeated logging.

### accessible_alert_in_progress_timer_



~~~cpp

base::RepeatingTimer accessible_alert_in_progress_timer_;

~~~

 A timer for accessible alerts of progress updates
### update_status_text_timer_



~~~cpp

base::RepeatingTimer update_status_text_timer_;

~~~

 A timer for updating the status text string.

### cancelable_task_tracker_



~~~cpp

base::CancelableTaskTracker cancelable_task_tracker_;

~~~

 Tracks tasks requesting file icons.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadBubbleRowView> weak_factory_{this};

~~~


### DownloadBubbleRowView

DownloadBubbleRowView
~~~cpp
DownloadBubbleRowView(const DownloadBubbleRowView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleRowView& operator=(const DownloadBubbleRowView&) = delete;
~~~

### OnDidChangeFocus

OnDidChangeFocus
~~~cpp
void OnDidChangeFocus(views::View* before, views::View* now) override {}
~~~

### model

model
~~~cpp
DownloadUIModel* model() { return model_.get(); }
~~~

### ui_info

ui_info
~~~cpp
DownloadUIModel::BubbleUIInfo& ui_info() { return ui_info_; }
~~~

### SetUIInfoForTesting

SetUIInfoForTesting
~~~cpp
void SetUIInfoForTesting(DownloadUIModel::BubbleUIInfo ui_info) {
    ui_info_ = ui_info;
  }
~~~

### AddedToWidget

DownloadBubbleRowView::AddedToWidget
~~~cpp
void AddedToWidget() override;
~~~
 Overrides views::View:
### RemovedFromWidget

DownloadBubbleRowView::RemovedFromWidget
~~~cpp
void RemovedFromWidget() override;
~~~

### Layout

DownloadBubbleRowView::Layout
~~~cpp
void Layout() override;
~~~

### GetChildrenInZOrder

DownloadBubbleRowView::GetChildrenInZOrder
~~~cpp
Views GetChildrenInZOrder() override;
~~~

### OnMouseDragged

DownloadBubbleRowView::OnMouseDragged
~~~cpp
bool OnMouseDragged(const ui::MouseEvent& event) override;
~~~

### OnMouseCaptureLost

DownloadBubbleRowView::OnMouseCaptureLost
~~~cpp
void OnMouseCaptureLost() override;
~~~

### OnMouseEntered

DownloadBubbleRowView::OnMouseEntered
~~~cpp
void OnMouseEntered(const ui::MouseEvent& event) override;
~~~

### OnMouseExited

DownloadBubbleRowView::OnMouseExited
~~~cpp
void OnMouseExited(const ui::MouseEvent& event) override;
~~~

### CalculatePreferredSize

DownloadBubbleRowView::CalculatePreferredSize
~~~cpp
gfx::Size CalculatePreferredSize() const override;
~~~

### AddLayerToRegion

DownloadBubbleRowView::AddLayerToRegion
~~~cpp
void AddLayerToRegion(ui::Layer* layer, views::LayerRegion region) override;
~~~

### RemoveLayerFromRegions

DownloadBubbleRowView::RemoveLayerFromRegions
~~~cpp
void RemoveLayerFromRegions(ui::Layer* layer) override;
~~~

### OnWillChangeFocus

DownloadBubbleRowView::OnWillChangeFocus
~~~cpp
void OnWillChangeFocus(views::View* before, views::View* now) override;
~~~
 Overrides views::FocusChangeListener
### UpdateRowForHover

DownloadBubbleRowView::UpdateRowForHover
~~~cpp
void UpdateRowForHover(bool hovered);
~~~
 Update the row and its elements for hover and focus events.

### UpdateRowForFocus

DownloadBubbleRowView::UpdateRowForFocus
~~~cpp
void UpdateRowForFocus(bool visible, bool request_focus_on_last_quick_action);
~~~

### OnDownloadOpened

DownloadBubbleRowView::OnDownloadOpened
~~~cpp
void OnDownloadOpened() override;
~~~
 Overrides DownloadUIModel::Delegate:
### OnDownloadUpdated

DownloadBubbleRowView::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated() override;
~~~

### OnDownloadDestroyed

DownloadBubbleRowView::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& id) override;
~~~

### ShowContextMenuForViewImpl

DownloadBubbleRowView::ShowContextMenuForViewImpl
~~~cpp
void ShowContextMenuForViewImpl(View* source,
                                  const gfx::Point& point,
                                  ui::MenuSourceType source_type) override;
~~~
 Overrides views::ContextMenuController:
### AcceleratorPressed

DownloadBubbleRowView::AcceleratorPressed
~~~cpp
bool AcceleratorPressed(const ui::Accelerator& accelerator) override;
~~~
 Overrides ui::AcceleratorTarget
### CanHandleAccelerators

DownloadBubbleRowView::CanHandleAccelerators
~~~cpp
bool CanHandleAccelerators() const override;
~~~

###  GetSecondaryLabelTextForTesting


~~~cpp
const std::u16string& GetSecondaryLabelTextForTesting();
~~~
### OnDeviceScaleFactorChanged

DownloadBubbleRowView::OnDeviceScaleFactorChanged
~~~cpp
void OnDeviceScaleFactorChanged(float old_device_scale_factor,
                                  float new_device_scale_factor) override;
~~~
 Overrides ui::LayerDelegate:
### AddMainPageButton

DownloadBubbleRowView::AddMainPageButton
~~~cpp
views::MdTextButton* AddMainPageButton(DownloadCommands::Command command,
                                         const std::u16string& button_string);
~~~

### AddQuickAction

DownloadBubbleRowView::AddQuickAction
~~~cpp
views::ImageButton* AddQuickAction(DownloadCommands::Command command);
~~~

### GetActionButtonForCommand

DownloadBubbleRowView::GetActionButtonForCommand
~~~cpp
views::ImageButton* GetActionButtonForCommand(
      DownloadCommands::Command command);
~~~

### GetAccessibleNameForQuickAction

DownloadBubbleRowView::GetAccessibleNameForQuickAction
~~~cpp
std::u16string GetAccessibleNameForQuickAction(
      DownloadCommands::Command command);
~~~

### GetMainPageButton

DownloadBubbleRowView::GetMainPageButton
~~~cpp
views::MdTextButton* GetMainPageButton(DownloadCommands::Command command);
~~~

### GetAccessibleNameForMainPageButton

DownloadBubbleRowView::GetAccessibleNameForMainPageButton
~~~cpp
std::u16string GetAccessibleNameForMainPageButton(
      DownloadCommands::Command command);
~~~

### UpdateBubbleUIInfo

DownloadBubbleRowView::UpdateBubbleUIInfo
~~~cpp
bool UpdateBubbleUIInfo(bool initial_setup);
~~~
 If there is any change in state, update UI info.

 Returns whether the ui info was changed.

### UpdateRow

DownloadBubbleRowView::UpdateRow
~~~cpp
void UpdateRow(bool initial_setup);
~~~
 Update the DownloadBubbleRowView's members.

### UpdateStatusText

DownloadBubbleRowView::UpdateStatusText
~~~cpp
void UpdateStatusText();
~~~

### UpdateButtons

DownloadBubbleRowView::UpdateButtons
~~~cpp
void UpdateButtons();
~~~

### UpdateProgressBar

DownloadBubbleRowView::UpdateProgressBar
~~~cpp
void UpdateProgressBar();
~~~

### UpdateLabels

DownloadBubbleRowView::UpdateLabels
~~~cpp
void UpdateLabels();
~~~

### RecordMetricsOnUpdate

DownloadBubbleRowView::RecordMetricsOnUpdate
~~~cpp
void RecordMetricsOnUpdate();
~~~

### RecordDownloadDisplayed

DownloadBubbleRowView::RecordDownloadDisplayed
~~~cpp
void RecordDownloadDisplayed();
~~~

### StartLoadFileIcon

DownloadBubbleRowView::StartLoadFileIcon
~~~cpp
bool StartLoadFileIcon();
~~~
 Load the appropriate |file_icon_| from the IconManager, or a default icon.

 Returns whether we were able to synchronously set |icon_| to an appropriate
 icon for the file path.

### SetFileIconAsIcon

DownloadBubbleRowView::SetFileIconAsIcon
~~~cpp
void SetFileIconAsIcon(bool is_default_icon);
~~~
 Sets |icon_| to the image in |file_icon_|.

### SetIcon

DownloadBubbleRowView::SetIcon
~~~cpp
void SetIcon();
~~~
 Set the |icon_|, which may be an override (warning or incognito icon),
 default icon, or loaded from the cache or from IconManager::LoadIcon.

### SetIconFromImage

DownloadBubbleRowView::SetIconFromImage
~~~cpp
void SetIconFromImage(gfx::Image icon);
~~~
 Sets |icon_| to |icon|, regardless of what kind of icon it is.

### SetIconFromImageModel

DownloadBubbleRowView::SetIconFromImageModel
~~~cpp
void SetIconFromImageModel(const ui::ImageModel& icon);
~~~

### OnCancelButtonPressed

DownloadBubbleRowView::OnCancelButtonPressed
~~~cpp
void OnCancelButtonPressed();
~~~

### OnDiscardButtonPressed

DownloadBubbleRowView::OnDiscardButtonPressed
~~~cpp
void OnDiscardButtonPressed();
~~~

### OnMainButtonPressed

DownloadBubbleRowView::OnMainButtonPressed
~~~cpp
void OnMainButtonPressed();
~~~

### AnnounceInProgressAlert

DownloadBubbleRowView::AnnounceInProgressAlert
~~~cpp
void AnnounceInProgressAlert();
~~~

### RegisterAccelerators

DownloadBubbleRowView::RegisterAccelerators
~~~cpp
void RegisterAccelerators(views::FocusManager* focus_manager);
~~~
 Registers/unregisters copy accelerator for copy/paste support.

### UnregisterAccelerators

DownloadBubbleRowView::UnregisterAccelerators
~~~cpp
void UnregisterAccelerators(views::FocusManager* focus_manager);
~~~

### icon_



~~~cpp

raw_ptr<views::ImageView> icon_ = nullptr;

~~~

 The icon for the file. We get platform-specific file type icons from
 IconLoader (see below).

### subpage_icon_



~~~cpp

raw_ptr<views::ImageView> subpage_icon_ = nullptr;

~~~


### subpage_icon_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> subpage_icon_holder_ = nullptr;

~~~


### file_icon_



~~~cpp

gfx::Image file_icon_;

~~~

 The icon for the filetype, fetched from the platform-specific IconLoader.

 This can differ from the image in |icon_| if |icon_| is not the file type
 icon, e.g. if it is the incognito icon or a warning icon. We cache it here
 in case |icon_| is different, because it is used when drag-and-dropping.

 If the IconLoader does not return a file icon, this stores a default icon.

### primary_label_



~~~cpp

raw_ptr<views::Label> primary_label_ = nullptr;

~~~

 The primary label.

### secondary_label_



~~~cpp

raw_ptr<views::Label> secondary_label_ = nullptr;

~~~

 The secondary label.

### cancel_button_



~~~cpp

raw_ptr<views::MdTextButton> cancel_button_ = nullptr;

~~~

 Buttons on the main page.

### discard_button_



~~~cpp

raw_ptr<views::MdTextButton> discard_button_ = nullptr;

~~~


### keep_button_



~~~cpp

raw_ptr<views::MdTextButton> keep_button_ = nullptr;

~~~


### scan_button_



~~~cpp

raw_ptr<views::MdTextButton> scan_button_ = nullptr;

~~~


### open_now_button_



~~~cpp

raw_ptr<views::MdTextButton> open_now_button_ = nullptr;

~~~


### resume_button_



~~~cpp

raw_ptr<views::MdTextButton> resume_button_ = nullptr;

~~~


### review_button_



~~~cpp

raw_ptr<views::MdTextButton> review_button_ = nullptr;

~~~


### retry_button_



~~~cpp

raw_ptr<views::MdTextButton> retry_button_ = nullptr;

~~~


### resume_action_



~~~cpp

raw_ptr<views::ImageButton> resume_action_ = nullptr;

~~~

 Quick Actions on the main page.

### pause_action_



~~~cpp

raw_ptr<views::ImageButton> pause_action_ = nullptr;

~~~


### show_in_folder_action_



~~~cpp

raw_ptr<views::ImageButton> show_in_folder_action_ = nullptr;

~~~


### cancel_action_



~~~cpp

raw_ptr<views::ImageButton> cancel_action_ = nullptr;

~~~


### open_when_complete_action_



~~~cpp

raw_ptr<views::ImageButton> open_when_complete_action_ = nullptr;

~~~


### main_button_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> main_button_holder_ = nullptr;

~~~

 Holder for the main button.

### quick_action_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> quick_action_holder_ = nullptr;

~~~

 Holder for the quick actions.

### progress_bar_



~~~cpp

raw_ptr<views::ProgressBar> progress_bar_ = nullptr;

~~~

 The progress bar for in-progress downloads.

### progress_bar_holder_



~~~cpp

raw_ptr<views::FlexLayoutView> progress_bar_holder_ = nullptr;

~~~


### current_scale_



~~~cpp

float current_scale_ = 1.0f;

~~~

 Device scale factor, used to load icons.

### model_



~~~cpp

DownloadUIModel::DownloadUIModelPtr model_;

~~~

 The model controlling this object's state.

### context_menu_



~~~cpp

std::unique_ptr<DownloadShelfContextMenuView> context_menu_;

~~~

 Reuse the download shelf context menu in the bubble.

### row_list_view_



~~~cpp

raw_ptr<DownloadBubbleRowListView> row_list_view_ = nullptr;

~~~

 Parent row list view.

### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_ = nullptr;

~~~

 Controller for keeping track of downloads.

### navigation_handler_



~~~cpp

raw_ptr<DownloadBubbleNavigationHandler> navigation_handler_ = nullptr;

~~~


### browser_



~~~cpp

raw_ptr<Browser> browser_ = nullptr;

~~~


### mode_



~~~cpp

download::DownloadItemMode mode_;

~~~


### state_



~~~cpp

download::DownloadItem::DownloadState state_;

~~~


### ui_info_



~~~cpp

DownloadUIModel::BubbleUIInfo ui_info_;

~~~


### is_paused_



~~~cpp

bool is_paused_;

~~~


### last_overridden_icon_



~~~cpp

raw_ptr<const gfx::VectorIcon> last_overridden_icon_ = nullptr;

~~~

 The last override icon, e.g. an incognito or warning icon. If this is
 null, we should either use the filetype icon or a default icon.

### has_default_icon_



~~~cpp

bool has_default_icon_ = false;

~~~

 Whether the currently set |icon_| is the default icon.

### transparent_button_



~~~cpp

raw_ptr<views::Button> transparent_button_ = nullptr;

~~~

 Button for transparent button click, inkdrop animations and drag and drop
 events.

### inkdrop_container_



~~~cpp

raw_ptr<views::InkDropContainerView> inkdrop_container_;

~~~


### dragging_



~~~cpp

bool dragging_ = false;

~~~

 Drag and drop:
 Whether we are dragging the download bubble row.

### drag_start_point_



~~~cpp

absl::optional<gfx::Point> drag_start_point_;

~~~

 Position that a possible drag started at.

### has_download_completion_been_logged_



~~~cpp

bool has_download_completion_been_logged_ = false;

~~~

 Whether the download's completion has already been logged. This is used to
 avoid inaccurate repeated logging.

### accessible_alert_in_progress_timer_



~~~cpp

base::RepeatingTimer accessible_alert_in_progress_timer_;

~~~

 A timer for accessible alerts of progress updates
### update_status_text_timer_



~~~cpp

base::RepeatingTimer update_status_text_timer_;

~~~

 A timer for updating the status text string.

### cancelable_task_tracker_



~~~cpp

base::CancelableTaskTracker cancelable_task_tracker_;

~~~

 Tracks tasks requesting file icons.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadBubbleRowView> weak_factory_{this};

~~~

