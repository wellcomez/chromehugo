
## class DownloadBubbleStartedAnimationViews
 namespace gfx
 An animation that starts halfway down the screen and moves upwards towards
 the download bubble toolbar icon. The animation is piecewise linear, composed
 of 2 phases. During phase one, the icon moves upwards and fades in. During
 phase two, the icon continues moving upwards and fades out.

 TODO(crbug.com/1414062): Investigate writing this using more modern
 frameworks like layers and views animation builder.

### METADATA_HEADER

DownloadBubbleStartedAnimationViews::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadBubbleStartedAnimationViews);
~~~

### DownloadBubbleStartedAnimationViews

DownloadBubbleStartedAnimationViews::DownloadBubbleStartedAnimationViews
~~~cpp
DownloadBubbleStartedAnimationViews(content::WebContents* web_contents,
                                      const gfx::Rect& toolbar_icon_bounds,
                                      SkColor image_foreground_color,
                                      SkColor image_background_color);
~~~

### DownloadBubbleStartedAnimationViews

DownloadBubbleStartedAnimationViews
~~~cpp
DownloadBubbleStartedAnimationViews(
      const DownloadBubbleStartedAnimationViews&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleStartedAnimationViews& operator=(
      const DownloadBubbleStartedAnimationViews&) = delete;
~~~

### ~DownloadBubbleStartedAnimationViews

DownloadBubbleStartedAnimationViews::~DownloadBubbleStartedAnimationViews
~~~cpp
~DownloadBubbleStartedAnimationViews() override;
~~~

### GetX

DownloadBubbleStartedAnimationViews::GetX
~~~cpp
int GetX() const override;
~~~
 DownloadStartedAnimationViews
### GetY

DownloadBubbleStartedAnimationViews::GetY
~~~cpp
int GetY() const override;
~~~

### GetOpacity

DownloadBubbleStartedAnimationViews::GetOpacity
~~~cpp
float GetOpacity() const override;
~~~

### WebContentsTooSmall

DownloadBubbleStartedAnimationViews::WebContentsTooSmall
~~~cpp
bool WebContentsTooSmall(const gfx::Size& image_size) const override;
~~~

### toolbar_icon_bounds_



~~~cpp

gfx::Rect toolbar_icon_bounds_;

~~~

 Bounds of the download bubble icon in the screen coordinate system.

### DownloadBubbleStartedAnimationViews

DownloadBubbleStartedAnimationViews
~~~cpp
DownloadBubbleStartedAnimationViews(
      const DownloadBubbleStartedAnimationViews&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleStartedAnimationViews& operator=(
      const DownloadBubbleStartedAnimationViews&) = delete;
~~~

### GetX

DownloadBubbleStartedAnimationViews::GetX
~~~cpp
int GetX() const override;
~~~
 DownloadStartedAnimationViews
### GetY

DownloadBubbleStartedAnimationViews::GetY
~~~cpp
int GetY() const override;
~~~

### GetOpacity

DownloadBubbleStartedAnimationViews::GetOpacity
~~~cpp
float GetOpacity() const override;
~~~

### WebContentsTooSmall

DownloadBubbleStartedAnimationViews::WebContentsTooSmall
~~~cpp
bool WebContentsTooSmall(const gfx::Size& image_size) const override;
~~~

### toolbar_icon_bounds_



~~~cpp

gfx::Rect toolbar_icon_bounds_;

~~~

 Bounds of the download bubble icon in the screen coordinate system.
