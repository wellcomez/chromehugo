
## class DownloadDialogView

### METADATA_HEADER

DownloadDialogView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadDialogView);
~~~

### DownloadDialogView

DownloadDialogView
~~~cpp
DownloadDialogView(const DownloadDialogView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDialogView& operator=(const DownloadDialogView&) = delete;
~~~

### DownloadDialogView

DownloadDialogView::DownloadDialogView
~~~cpp
DownloadDialogView(raw_ptr<Browser> browser,
                     std::unique_ptr<views::View> row_list_scroll_view,
                     DownloadBubbleNavigationHandler* navigation_handler);
~~~

### CloseBubble

DownloadDialogView::CloseBubble
~~~cpp
void CloseBubble();
~~~

### ShowAllDownloads

DownloadDialogView::ShowAllDownloads
~~~cpp
void ShowAllDownloads();
~~~

### AddHeader

DownloadDialogView::AddHeader
~~~cpp
void AddHeader();
~~~

### AddFooter

DownloadDialogView::AddFooter
~~~cpp
void AddFooter();
~~~

### navigation_handler_



~~~cpp

raw_ptr<DownloadBubbleNavigationHandler> navigation_handler_ = nullptr;

~~~


### browser_



~~~cpp

raw_ptr<Browser> browser_ = nullptr;

~~~


### DownloadDialogView

DownloadDialogView
~~~cpp
DownloadDialogView(const DownloadDialogView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDialogView& operator=(const DownloadDialogView&) = delete;
~~~

### CloseBubble

DownloadDialogView::CloseBubble
~~~cpp
void CloseBubble();
~~~

### ShowAllDownloads

DownloadDialogView::ShowAllDownloads
~~~cpp
void ShowAllDownloads();
~~~

### AddHeader

DownloadDialogView::AddHeader
~~~cpp
void AddHeader();
~~~

### AddFooter

DownloadDialogView::AddFooter
~~~cpp
void AddFooter();
~~~

### navigation_handler_



~~~cpp

raw_ptr<DownloadBubbleNavigationHandler> navigation_handler_ = nullptr;

~~~


### browser_



~~~cpp

raw_ptr<Browser> browser_ = nullptr;

~~~

