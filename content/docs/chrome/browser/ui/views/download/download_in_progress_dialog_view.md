
## class DownloadInProgressDialogView
 Dialog shown when the user tries to exit the browser or all incognito windows
 while a download is in progress.

### METADATA_HEADER

DownloadInProgressDialogView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadInProgressDialogView);
~~~

### DownloadInProgressDialogView

DownloadInProgressDialogView
~~~cpp
DownloadInProgressDialogView(const DownloadInProgressDialogView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadInProgressDialogView& operator=(const DownloadInProgressDialogView&) =
      delete;
~~~

### Show

DownloadInProgressDialogView::Show
~~~cpp
static void Show(gfx::NativeWindow parent_window,
                   int download_count,
                   Browser::DownloadCloseType dialog_type,
                   base::OnceCallback<void(bool)> callback);
~~~
 |dialog_type| should be either DOWNLOAD_CLOSE_BROWSER_SHUTDOWN to indicate
 the user is closing the browser or
 DOWNLOAD_CLOSE_LAST_WINDOW_IN_INCOGNITO_PROFILE to indicate the user is
 closing the last incognito window. |callback| will be called with true if
 the download should be canceled, or false if the download should proceed.

### DownloadInProgressDialogView

DownloadInProgressDialogView::DownloadInProgressDialogView
~~~cpp
DownloadInProgressDialogView(int download_count,
                               Browser::DownloadCloseType dialog_type,
                               base::OnceCallback<void(bool)> callback);
~~~

### ~DownloadInProgressDialogView

DownloadInProgressDialogView::~DownloadInProgressDialogView
~~~cpp
~DownloadInProgressDialogView() override;
~~~

### d


~~~cpp
base::OnceCallback<void(bool)> callback_;
~~~
### DownloadInProgressDialogView

DownloadInProgressDialogView
~~~cpp
DownloadInProgressDialogView(const DownloadInProgressDialogView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadInProgressDialogView& operator=(const DownloadInProgressDialogView&) =
      delete;
~~~

### Show

DownloadInProgressDialogView::Show
~~~cpp
static void Show(gfx::NativeWindow parent_window,
                   int download_count,
                   Browser::DownloadCloseType dialog_type,
                   base::OnceCallback<void(bool)> callback);
~~~
 |dialog_type| should be either DOWNLOAD_CLOSE_BROWSER_SHUTDOWN to indicate
 the user is closing the browser or
 DOWNLOAD_CLOSE_LAST_WINDOW_IN_INCOGNITO_PROFILE to indicate the user is
 closing the last incognito window. |callback| will be called with true if
 the download should be canceled, or false if the download should proceed.

### d


~~~cpp
base::OnceCallback<void(bool)> callback_;
~~~