
## class DownloadItemView
 namespace views
 A view that implements one download on the Download shelf. Each
 DownloadItemView contains an application icon, a text label indicating the
 download's file name, a text label indicating the download's status (such as
 the number of bytes downloaded so far), and a button for canceling an
 in-progress download, or opening the completed download.


 The DownloadItemView lives in the Browser, and has a corresponding
 DownloadController that receives / writes data which lives in the Renderer.

### METADATA_HEADER

DownloadItemView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadItemView);
~~~

### DownloadItemView

DownloadItemView::DownloadItemView
~~~cpp
DownloadItemView(DownloadUIModel::DownloadUIModelPtr model,
                   DownloadShelfView* shelf,
                   views::View* accessible_alert);
~~~

### DownloadItemView

DownloadItemView
~~~cpp
DownloadItemView(const DownloadItemView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadItemView& operator=(const DownloadItemView&) = delete;
~~~

### ~DownloadItemView

DownloadItemView::~DownloadItemView
~~~cpp
~DownloadItemView() override;
~~~

### AddedToWidget

DownloadItemView::AddedToWidget
~~~cpp
void AddedToWidget() override;
~~~
 views::View:
### Layout

DownloadItemView::Layout
~~~cpp
void Layout() override;
~~~

### OnMouseDragged

DownloadItemView::OnMouseDragged
~~~cpp
bool OnMouseDragged(const ui::MouseEvent& event) override;
~~~

### OnMouseCaptureLost

DownloadItemView::OnMouseCaptureLost
~~~cpp
void OnMouseCaptureLost() override;
~~~

### GetTooltipText

DownloadItemView::GetTooltipText
~~~cpp
std::u16string GetTooltipText(const gfx::Point& p) const override;
~~~

### GetAccessibleNodeData

DownloadItemView::GetAccessibleNodeData
~~~cpp
void GetAccessibleNodeData(ui::AXNodeData* node_data) override;
~~~

### ShowContextMenuForViewImpl

DownloadItemView::ShowContextMenuForViewImpl
~~~cpp
void ShowContextMenuForViewImpl(View* source,
                                  const gfx::Point& point,
                                  ui::MenuSourceType source_type) override;
~~~
 views::ContextMenuController:
### OnDownloadUpdated

DownloadItemView::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated() override;
~~~
 DownloadUIModel::Delegate:
### OnDownloadOpened

DownloadItemView::OnDownloadOpened
~~~cpp
void OnDownloadOpened() override;
~~~

### OnDownloadDestroyed

DownloadItemView::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& id) override;
~~~

### AnimationProgressed

DownloadItemView::AnimationProgressed
~~~cpp
void AnimationProgressed(const gfx::Animation* animation) override;
~~~
 views::AnimationDelegateViews:
### AnimationEnded

DownloadItemView::AnimationEnded
~~~cpp
void AnimationEnded(const gfx::Animation* animation) override;
~~~

### model

model
~~~cpp
DownloadUIModel* model() { return model_.get(); }
~~~
 Returns the DownloadUIModel object belonging to this item.

### model

model
~~~cpp
const DownloadUIModel* model() const { return model_.get(); }
~~~

### GetStatusTextForTesting

DownloadItemView::GetStatusTextForTesting
~~~cpp
std::u16string GetStatusTextForTesting() const;
~~~

### OpenItemForTesting

DownloadItemView::OpenItemForTesting
~~~cpp
void OpenItemForTesting();
~~~

### CalculatePreferredSize

DownloadItemView::CalculatePreferredSize
~~~cpp
gfx::Size CalculatePreferredSize() const override;
~~~
 views::View:
### OnPaintBackground

DownloadItemView::OnPaintBackground
~~~cpp
void OnPaintBackground(gfx::Canvas* canvas) override;
~~~

### OnPaint

DownloadItemView::OnPaint
~~~cpp
void OnPaint(gfx::Canvas* canvas) override;
~~~

### OnThemeChanged

DownloadItemView::OnThemeChanged
~~~cpp
void OnThemeChanged() override;
~~~

### OnDeviceScaleFactorChanged

DownloadItemView::OnDeviceScaleFactorChanged
~~~cpp
void OnDeviceScaleFactorChanged(float old_device_scale_factor,
                                  float new_device_scale_factor) override;
~~~
 ui::LayerDelegate:
### field error



~~~cpp

class ContextMenuButton;

~~~


### SetMode

DownloadItemView::SetMode
~~~cpp
void SetMode(download::DownloadItemMode mode);
~~~
 Sets the current mode to |mode| and updates UI appropriately.

### GetMode

DownloadItemView::GetMode
~~~cpp
download::DownloadItemMode GetMode() const;
~~~

### UpdateFilePathAndIcons

DownloadItemView::UpdateFilePathAndIcons
~~~cpp
void UpdateFilePathAndIcons();
~~~
 Updates the file path, and if necessary, begins loading the file icon in
 various sizes. This may eventually result in a callback to
 OnFileIconLoaded().

### StartLoadIcons

DownloadItemView::StartLoadIcons
~~~cpp
void StartLoadIcons();
~~~
 Begins loading the file icon in various sizes.

### UpdateLabels

DownloadItemView::UpdateLabels
~~~cpp
void UpdateLabels();
~~~
 Updates the visibility, text, size, etc. of all labels.

### UpdateButtons

DownloadItemView::UpdateButtons
~~~cpp
void UpdateButtons();
~~~
 Updates the visible and enabled state of all buttons.

### UpdateAccessibleAlertAndAnimationsForNormalMode

DownloadItemView::UpdateAccessibleAlertAndAnimationsForNormalMode
~~~cpp
void UpdateAccessibleAlertAndAnimationsForNormalMode();
~~~
 Updates the accessible alert and animation-related state for normal mode.

### UpdateAccessibleAlert

DownloadItemView::UpdateAccessibleAlert
~~~cpp
void UpdateAccessibleAlert(const std::u16string& alert);
~~~
 Update accessible status text, and announce it if desired.

### UpdateAnimationForDeepScanningMode

DownloadItemView::UpdateAnimationForDeepScanningMode
~~~cpp
void UpdateAnimationForDeepScanningMode();
~~~
 Updates the animation used during deep scanning. The animation is started
 or stopped depending on the current mode.

### GetInProgressAccessibleAlertText

DownloadItemView::GetInProgressAccessibleAlertText
~~~cpp
std::u16string GetInProgressAccessibleAlertText() const;
~~~
 Get the accessible alert text for a download that is currently in progress.

### AnnounceAccessibleAlert

DownloadItemView::AnnounceAccessibleAlert
~~~cpp
void AnnounceAccessibleAlert();
~~~
 Callback for |accessible_update_timer_|, or can be used to ask a screen
 reader to speak the current alert immediately.

### OnFileIconLoaded

DownloadItemView::OnFileIconLoaded
~~~cpp
void OnFileIconLoaded(IconLoader::IconSize icon_size, gfx::Image icon);
~~~
 Sets |file_icon_| to |icon|. Called when the icon manager has loaded the
 normal-size icon for the current file path.

### PaintDownloadProgress

DownloadItemView::PaintDownloadProgress
~~~cpp
void PaintDownloadProgress(gfx::Canvas* canvas,
                             const gfx::RectF& bounds,
                             const base::TimeDelta& indeterminate_progress_time,
                             int percent_done) const;
~~~
 Paint the common download animation progress foreground and background. If
 |percent_done| < 0, the total size is indeterminate.

 |indeterminate_progress_time| is only used in that case.

### GetIcon

DownloadItemView::GetIcon
~~~cpp
ui::ImageModel GetIcon() const;
~~~
 When not in normal mode, returns the current help/warning/error icon.

### GetIconBounds

DownloadItemView::GetIconBounds
~~~cpp
gfx::RectF GetIconBounds() const;
~~~
 When not in nromal mode, returns the bounds of the current icon.

### GetStatusTextAndStyle

DownloadItemView::GetStatusTextAndStyle
~~~cpp
std::pair<std::u16string, int> GetStatusTextAndStyle() const;
~~~
 Returns the text and style to use for the status label.

### GetButtonSize

DownloadItemView::GetButtonSize
~~~cpp
gfx::Size GetButtonSize() const;
~~~
 Returns the size of any button visible next to the label (all visible
 buttons are given the same size).

### ElidedFilename

DownloadItemView::ElidedFilename
~~~cpp
std::u16string ElidedFilename(const views::Label& label) const;
~~~
 Returns the file name to report to the user. It might be elided to fit into
 the text width. |label| dictates the default text style.

### ElidedFilename

DownloadItemView::ElidedFilename
~~~cpp
std::u16string ElidedFilename(const views::StyledLabel& label) const;
~~~

### CenterY

DownloadItemView::CenterY
~~~cpp
int CenterY(int element_height) const;
~~~
 Returns the Y coordinate that centers |element_height| within the current
 height().

### GetLabelWidth

DownloadItemView::GetLabelWidth
~~~cpp
int GetLabelWidth(const views::StyledLabel& label) const;
~~~
 Returns either:
   * 200, if |label| can fit in one line given at most 200 DIP width.

   * The minimum width needed to display |label| on two lines.

### SetDropdownPressed

DownloadItemView::SetDropdownPressed
~~~cpp
void SetDropdownPressed(bool pressed);
~~~
 Sets the state and triggers a repaint.

### GetDropdownPressed

DownloadItemView::GetDropdownPressed
~~~cpp
bool GetDropdownPressed() const;
~~~

### UpdateDropdownButtonImage

DownloadItemView::UpdateDropdownButtonImage
~~~cpp
void UpdateDropdownButtonImage();
~~~
 Sets |dropdown_button_| to have the correct image for the current state.

### OpenButtonPressed

DownloadItemView::OpenButtonPressed
~~~cpp
void OpenButtonPressed();
~~~
 Called when various buttons are pressed.

### DropdownButtonPressed

DownloadItemView::DropdownButtonPressed
~~~cpp
void DropdownButtonPressed(const ui::Event& event);
~~~

### ReviewButtonPressed

DownloadItemView::ReviewButtonPressed
~~~cpp
void ReviewButtonPressed();
~~~

### ShowOpenDialog

DownloadItemView::ShowOpenDialog
~~~cpp
void ShowOpenDialog(content::WebContents* web_contents);
~~~
 Shows an appropriate prompt dialog when the user hits the "open" button
 when not in normal mode.

### ShowContextMenuImpl

DownloadItemView::ShowContextMenuImpl
~~~cpp
void ShowContextMenuImpl(const gfx::Rect& rect,
                           ui::MenuSourceType source_type);
~~~
 Shows the context menu at the specified location. |point| is in the view's
 coordinate system.

### OpenDownloadDuringAsyncScanning

DownloadItemView::OpenDownloadDuringAsyncScanning
~~~cpp
void OpenDownloadDuringAsyncScanning();
~~~
 Opens a file while async scanning is still pending.

### ExecuteCommand

DownloadItemView::ExecuteCommand
~~~cpp
void ExecuteCommand(DownloadCommands::Command command);
~~~
 Forwards |command| to |commands_|; useful for callbacks.

### model_



~~~cpp

const DownloadUIModel::DownloadUIModelPtr model_;

~~~

 The model controlling this object's state.

###  commands_{model

 A utility object to help execute commands on the model.

~~~cpp
DownloadCommands commands_{model()->GetWeakPtr()};
~~~
### shelf_



~~~cpp

const raw_ptr<DownloadShelfView> shelf_;

~~~

 The download shelf that owns us.

### mode_



~~~cpp

download::DownloadItemMode mode_;

~~~

 Mode of the download item view.

### open_button_



~~~cpp

raw_ptr<views::Button> open_button_;

~~~

 The "open download" button. This button is visually transparent and fills
 the entire bounds of the DownloadItemView, to make the DownloadItemView
 itself seem to be clickable while not requiring DownloadItemView itself to
 be a button. This is necessary because buttons are not allowed to have
 children in macOS Accessibility, and to avoid reimplementing much of the
 button logic in DownloadItemView.

### dragging_



~~~cpp

bool dragging_ = false;

~~~

 Whether we are dragging the download button.

### drag_start_point_



~~~cpp

absl::optional<gfx::Point> drag_start_point_;

~~~

 Position that a possible drag started at.

### file_icon_



~~~cpp

gfx::ImageSkia file_icon_;

~~~


### cancelable_task_tracker_



~~~cpp

base::CancelableTaskTracker cancelable_task_tracker_;

~~~

 Tracks in-progress file icon loading tasks.

### file_path_



~~~cpp

base::FilePath file_path_;

~~~

 |file_icon_| is based on the path of the downloaded item.  Store the path
 used, so that we can detect a change in the path and reload the icon.

### file_name_label_



~~~cpp

raw_ptr<views::Label> file_name_label_;

~~~


### status_label_



~~~cpp

raw_ptr<views::Label> status_label_;

~~~


### warning_label_



~~~cpp

raw_ptr<views::StyledLabel> warning_label_;

~~~


### deep_scanning_label_



~~~cpp

raw_ptr<views::StyledLabel> deep_scanning_label_;

~~~


### field error



~~~cpp

views::MdTextButton* open_now_button_;

~~~


### field error



~~~cpp

views::MdTextButton* save_button_;

~~~


### field error



~~~cpp

views::MdTextButton* discard_button_;

~~~


### field error



~~~cpp

views::MdTextButton* scan_button_;

~~~


### field error



~~~cpp

views::MdTextButton* review_button_;

~~~


### dropdown_button_



~~~cpp

raw_ptr<views::ImageButton> dropdown_button_;

~~~


### dropdown_pressed_



~~~cpp

bool dropdown_pressed_ = false;

~~~

 Whether the dropdown is currently pressed.

### context_menu_



~~~cpp

DownloadShelfContextMenuView context_menu_{this};

~~~


### indeterminate_progress_timer_



~~~cpp

base::RepeatingTimer indeterminate_progress_timer_;

~~~


### indeterminate_progress_start_time_



~~~cpp

base::TimeTicks indeterminate_progress_start_time_;

~~~

 The start of the most recent active period of downloading a file of
 indeterminate size.

### indeterminate_progress_time_elapsed_



~~~cpp

base::TimeDelta indeterminate_progress_time_elapsed_;

~~~

 The total active time downloading a file of indeterminate size.

### complete_animation_



~~~cpp

gfx::SlideAnimation complete_animation_{this};

~~~


### scanning_animation_



~~~cpp

gfx::ThrobAnimation scanning_animation_{this};

~~~


### tooltip_text_



~~~cpp

std::u16string tooltip_text_;

~~~

 The tooltip.  Only displayed when not showing a warning dialog.

### accessible_name_



~~~cpp

std::u16string accessible_name_;

~~~


### accessible_alert_



~~~cpp

const raw_ptr<views::View> accessible_alert_;

~~~

 A hidden view for accessible status alerts that are spoken by screen
 readers when a download changes state.

### accessible_alert_timer_



~~~cpp

base::RepeatingTimer accessible_alert_timer_;

~~~

 A timer for accessible alerts that helps reduce the number of similar
 messages spoken in a short period of time.

### announce_accessible_alert_soon_



~~~cpp

bool announce_accessible_alert_soon_ = false;

~~~

 Forces reading the current alert text the next time it updates.

### current_scale_



~~~cpp

float current_scale_;

~~~


### dropdown_button_shown_recorded_



~~~cpp

bool dropdown_button_shown_recorded_ = false;

~~~

 Whether or not a histogram has been emitted recording that the dropdown
 button shown.

### dropdown_button_pressed_recorded_



~~~cpp

bool dropdown_button_pressed_recorded_ = false;

~~~

 Whether or not a histogram has been emitted recording that the dropdown
 button was pressed.

### has_download_completion_been_logged_



~~~cpp

bool has_download_completion_been_logged_ = false;

~~~

 Whether the download's completion has already been logged. This is used to
 avoid inaccurate repeated logging.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadItemView> weak_ptr_factory_{this};

~~~

 Method factory used to delay reenabling of the item when opening the
 downloaded file.

### DownloadItemView

DownloadItemView
~~~cpp
DownloadItemView(const DownloadItemView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadItemView& operator=(const DownloadItemView&) = delete;
~~~

### model

model
~~~cpp
DownloadUIModel* model() { return model_.get(); }
~~~
 Returns the DownloadUIModel object belonging to this item.

### model

model
~~~cpp
const DownloadUIModel* model() const { return model_.get(); }
~~~

### AddedToWidget

DownloadItemView::AddedToWidget
~~~cpp
void AddedToWidget() override;
~~~
 views::View:
### Layout

DownloadItemView::Layout
~~~cpp
void Layout() override;
~~~

### OnMouseDragged

DownloadItemView::OnMouseDragged
~~~cpp
bool OnMouseDragged(const ui::MouseEvent& event) override;
~~~

### OnMouseCaptureLost

DownloadItemView::OnMouseCaptureLost
~~~cpp
void OnMouseCaptureLost() override;
~~~

### GetTooltipText

DownloadItemView::GetTooltipText
~~~cpp
std::u16string GetTooltipText(const gfx::Point& p) const override;
~~~

### GetAccessibleNodeData

DownloadItemView::GetAccessibleNodeData
~~~cpp
void GetAccessibleNodeData(ui::AXNodeData* node_data) override;
~~~

### ShowContextMenuForViewImpl

DownloadItemView::ShowContextMenuForViewImpl
~~~cpp
void ShowContextMenuForViewImpl(View* source,
                                  const gfx::Point& point,
                                  ui::MenuSourceType source_type) override;
~~~
 views::ContextMenuController:
### OnDownloadUpdated

DownloadItemView::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated() override;
~~~
 DownloadUIModel::Delegate:
### OnDownloadOpened

DownloadItemView::OnDownloadOpened
~~~cpp
void OnDownloadOpened() override;
~~~

### OnDownloadDestroyed

DownloadItemView::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& id) override;
~~~

### AnimationProgressed

DownloadItemView::AnimationProgressed
~~~cpp
void AnimationProgressed(const gfx::Animation* animation) override;
~~~
 views::AnimationDelegateViews:
### AnimationEnded

DownloadItemView::AnimationEnded
~~~cpp
void AnimationEnded(const gfx::Animation* animation) override;
~~~

### GetStatusTextForTesting

DownloadItemView::GetStatusTextForTesting
~~~cpp
std::u16string GetStatusTextForTesting() const;
~~~

### OpenItemForTesting

DownloadItemView::OpenItemForTesting
~~~cpp
void OpenItemForTesting();
~~~

### CalculatePreferredSize

DownloadItemView::CalculatePreferredSize
~~~cpp
gfx::Size CalculatePreferredSize() const override;
~~~
 views::View:
### OnPaintBackground

DownloadItemView::OnPaintBackground
~~~cpp
void OnPaintBackground(gfx::Canvas* canvas) override;
~~~

### OnPaint

DownloadItemView::OnPaint
~~~cpp
void OnPaint(gfx::Canvas* canvas) override;
~~~

### OnThemeChanged

DownloadItemView::OnThemeChanged
~~~cpp
void OnThemeChanged() override;
~~~

### OnDeviceScaleFactorChanged

DownloadItemView::OnDeviceScaleFactorChanged
~~~cpp
void OnDeviceScaleFactorChanged(float old_device_scale_factor,
                                  float new_device_scale_factor) override;
~~~
 ui::LayerDelegate:
### field error



~~~cpp

class ContextMenuButton;

~~~


### SetMode

DownloadItemView::SetMode
~~~cpp
void SetMode(download::DownloadItemMode mode);
~~~
 Sets the current mode to |mode| and updates UI appropriately.

### GetMode

DownloadItemView::GetMode
~~~cpp
download::DownloadItemMode GetMode() const;
~~~

### UpdateFilePathAndIcons

DownloadItemView::UpdateFilePathAndIcons
~~~cpp
void UpdateFilePathAndIcons();
~~~
 Updates the file path, and if necessary, begins loading the file icon in
 various sizes. This may eventually result in a callback to
 OnFileIconLoaded().

### StartLoadIcons

DownloadItemView::StartLoadIcons
~~~cpp
void StartLoadIcons();
~~~
 Begins loading the file icon in various sizes.

### UpdateLabels

DownloadItemView::UpdateLabels
~~~cpp
void UpdateLabels();
~~~
 Updates the visibility, text, size, etc. of all labels.

### UpdateButtons

DownloadItemView::UpdateButtons
~~~cpp
void UpdateButtons();
~~~
 Updates the visible and enabled state of all buttons.

### UpdateAccessibleAlertAndAnimationsForNormalMode

DownloadItemView::UpdateAccessibleAlertAndAnimationsForNormalMode
~~~cpp
void UpdateAccessibleAlertAndAnimationsForNormalMode();
~~~
 Updates the accessible alert and animation-related state for normal mode.

### UpdateAccessibleAlert

DownloadItemView::UpdateAccessibleAlert
~~~cpp
void UpdateAccessibleAlert(const std::u16string& alert);
~~~
 Update accessible status text, and announce it if desired.

### UpdateAnimationForDeepScanningMode

DownloadItemView::UpdateAnimationForDeepScanningMode
~~~cpp
void UpdateAnimationForDeepScanningMode();
~~~
 Updates the animation used during deep scanning. The animation is started
 or stopped depending on the current mode.

### GetInProgressAccessibleAlertText

DownloadItemView::GetInProgressAccessibleAlertText
~~~cpp
std::u16string GetInProgressAccessibleAlertText() const;
~~~
 Get the accessible alert text for a download that is currently in progress.

### AnnounceAccessibleAlert

DownloadItemView::AnnounceAccessibleAlert
~~~cpp
void AnnounceAccessibleAlert();
~~~
 Callback for |accessible_update_timer_|, or can be used to ask a screen
 reader to speak the current alert immediately.

### OnFileIconLoaded

DownloadItemView::OnFileIconLoaded
~~~cpp
void OnFileIconLoaded(IconLoader::IconSize icon_size, gfx::Image icon);
~~~
 Sets |file_icon_| to |icon|. Called when the icon manager has loaded the
 normal-size icon for the current file path.

### PaintDownloadProgress

DownloadItemView::PaintDownloadProgress
~~~cpp
void PaintDownloadProgress(gfx::Canvas* canvas,
                             const gfx::RectF& bounds,
                             const base::TimeDelta& indeterminate_progress_time,
                             int percent_done) const;
~~~
 Paint the common download animation progress foreground and background. If
 |percent_done| < 0, the total size is indeterminate.

 |indeterminate_progress_time| is only used in that case.

### GetIcon

DownloadItemView::GetIcon
~~~cpp
ui::ImageModel GetIcon() const;
~~~
 When not in normal mode, returns the current help/warning/error icon.

### GetIconBounds

DownloadItemView::GetIconBounds
~~~cpp
gfx::RectF GetIconBounds() const;
~~~
 When not in nromal mode, returns the bounds of the current icon.

### GetStatusTextAndStyle

DownloadItemView::GetStatusTextAndStyle
~~~cpp
std::pair<std::u16string, int> GetStatusTextAndStyle() const;
~~~
 Returns the text and style to use for the status label.

### GetButtonSize

DownloadItemView::GetButtonSize
~~~cpp
gfx::Size GetButtonSize() const;
~~~
 Returns the size of any button visible next to the label (all visible
 buttons are given the same size).

### ElidedFilename

DownloadItemView::ElidedFilename
~~~cpp
std::u16string ElidedFilename(const views::Label& label) const;
~~~
 Returns the file name to report to the user. It might be elided to fit into
 the text width. |label| dictates the default text style.

### ElidedFilename

DownloadItemView::ElidedFilename
~~~cpp
std::u16string ElidedFilename(const views::StyledLabel& label) const;
~~~

### CenterY

DownloadItemView::CenterY
~~~cpp
int CenterY(int element_height) const;
~~~
 Returns the Y coordinate that centers |element_height| within the current
 height().

### GetLabelWidth

DownloadItemView::GetLabelWidth
~~~cpp
int GetLabelWidth(const views::StyledLabel& label) const;
~~~
 Returns either:
   * 200, if |label| can fit in one line given at most 200 DIP width.

   * The minimum width needed to display |label| on two lines.

### SetDropdownPressed

DownloadItemView::SetDropdownPressed
~~~cpp
void SetDropdownPressed(bool pressed);
~~~
 Sets the state and triggers a repaint.

### GetDropdownPressed

DownloadItemView::GetDropdownPressed
~~~cpp
bool GetDropdownPressed() const;
~~~

### UpdateDropdownButtonImage

DownloadItemView::UpdateDropdownButtonImage
~~~cpp
void UpdateDropdownButtonImage();
~~~
 Sets |dropdown_button_| to have the correct image for the current state.

### OpenButtonPressed

DownloadItemView::OpenButtonPressed
~~~cpp
void OpenButtonPressed();
~~~
 Called when various buttons are pressed.

### DropdownButtonPressed

DownloadItemView::DropdownButtonPressed
~~~cpp
void DropdownButtonPressed(const ui::Event& event);
~~~

### ReviewButtonPressed

DownloadItemView::ReviewButtonPressed
~~~cpp
void ReviewButtonPressed();
~~~

### ShowOpenDialog

DownloadItemView::ShowOpenDialog
~~~cpp
void ShowOpenDialog(content::WebContents* web_contents);
~~~
 Shows an appropriate prompt dialog when the user hits the "open" button
 when not in normal mode.

### ShowContextMenuImpl

DownloadItemView::ShowContextMenuImpl
~~~cpp
void ShowContextMenuImpl(const gfx::Rect& rect,
                           ui::MenuSourceType source_type);
~~~
 Shows the context menu at the specified location. |point| is in the view's
 coordinate system.

### OpenDownloadDuringAsyncScanning

DownloadItemView::OpenDownloadDuringAsyncScanning
~~~cpp
void OpenDownloadDuringAsyncScanning();
~~~
 Opens a file while async scanning is still pending.

### ExecuteCommand

DownloadItemView::ExecuteCommand
~~~cpp
void ExecuteCommand(DownloadCommands::Command command);
~~~
 Forwards |command| to |commands_|; useful for callbacks.

### model_



~~~cpp

const DownloadUIModel::DownloadUIModelPtr model_;

~~~

 The model controlling this object's state.

###  commands_{model

 A utility object to help execute commands on the model.

~~~cpp
DownloadCommands commands_{model()->GetWeakPtr()};
~~~
### shelf_



~~~cpp

const raw_ptr<DownloadShelfView> shelf_;

~~~

 The download shelf that owns us.

### mode_



~~~cpp

download::DownloadItemMode mode_;

~~~

 Mode of the download item view.

### open_button_



~~~cpp

raw_ptr<views::Button> open_button_;

~~~

 The "open download" button. This button is visually transparent and fills
 the entire bounds of the DownloadItemView, to make the DownloadItemView
 itself seem to be clickable while not requiring DownloadItemView itself to
 be a button. This is necessary because buttons are not allowed to have
 children in macOS Accessibility, and to avoid reimplementing much of the
 button logic in DownloadItemView.

### dragging_



~~~cpp

bool dragging_ = false;

~~~

 Whether we are dragging the download button.

### drag_start_point_



~~~cpp

absl::optional<gfx::Point> drag_start_point_;

~~~

 Position that a possible drag started at.

### file_icon_



~~~cpp

gfx::ImageSkia file_icon_;

~~~


### cancelable_task_tracker_



~~~cpp

base::CancelableTaskTracker cancelable_task_tracker_;

~~~

 Tracks in-progress file icon loading tasks.

### file_path_



~~~cpp

base::FilePath file_path_;

~~~

 |file_icon_| is based on the path of the downloaded item.  Store the path
 used, so that we can detect a change in the path and reload the icon.

### file_name_label_



~~~cpp

raw_ptr<views::Label> file_name_label_;

~~~


### status_label_



~~~cpp

raw_ptr<views::Label> status_label_;

~~~


### warning_label_



~~~cpp

raw_ptr<views::StyledLabel> warning_label_;

~~~


### deep_scanning_label_



~~~cpp

raw_ptr<views::StyledLabel> deep_scanning_label_;

~~~


### field error



~~~cpp

views::MdTextButton* open_now_button_;

~~~


### field error



~~~cpp

views::MdTextButton* save_button_;

~~~


### field error



~~~cpp

views::MdTextButton* discard_button_;

~~~


### field error



~~~cpp

views::MdTextButton* scan_button_;

~~~


### field error



~~~cpp

views::MdTextButton* review_button_;

~~~


### dropdown_button_



~~~cpp

raw_ptr<views::ImageButton> dropdown_button_;

~~~


### dropdown_pressed_



~~~cpp

bool dropdown_pressed_ = false;

~~~

 Whether the dropdown is currently pressed.

### context_menu_



~~~cpp

DownloadShelfContextMenuView context_menu_{this};

~~~


### indeterminate_progress_timer_



~~~cpp

base::RepeatingTimer indeterminate_progress_timer_;

~~~


### indeterminate_progress_start_time_



~~~cpp

base::TimeTicks indeterminate_progress_start_time_;

~~~

 The start of the most recent active period of downloading a file of
 indeterminate size.

### indeterminate_progress_time_elapsed_



~~~cpp

base::TimeDelta indeterminate_progress_time_elapsed_;

~~~

 The total active time downloading a file of indeterminate size.

### complete_animation_



~~~cpp

gfx::SlideAnimation complete_animation_{this};

~~~


### scanning_animation_



~~~cpp

gfx::ThrobAnimation scanning_animation_{this};

~~~


### tooltip_text_



~~~cpp

std::u16string tooltip_text_;

~~~

 The tooltip.  Only displayed when not showing a warning dialog.

### accessible_name_



~~~cpp

std::u16string accessible_name_;

~~~


### accessible_alert_



~~~cpp

const raw_ptr<views::View> accessible_alert_;

~~~

 A hidden view for accessible status alerts that are spoken by screen
 readers when a download changes state.

### accessible_alert_timer_



~~~cpp

base::RepeatingTimer accessible_alert_timer_;

~~~

 A timer for accessible alerts that helps reduce the number of similar
 messages spoken in a short period of time.

### announce_accessible_alert_soon_



~~~cpp

bool announce_accessible_alert_soon_ = false;

~~~

 Forces reading the current alert text the next time it updates.

### current_scale_



~~~cpp

float current_scale_;

~~~


### dropdown_button_shown_recorded_



~~~cpp

bool dropdown_button_shown_recorded_ = false;

~~~

 Whether or not a histogram has been emitted recording that the dropdown
 button shown.

### dropdown_button_pressed_recorded_



~~~cpp

bool dropdown_button_pressed_recorded_ = false;

~~~

 Whether or not a histogram has been emitted recording that the dropdown
 button was pressed.

### has_download_completion_been_logged_



~~~cpp

bool has_download_completion_been_logged_ = false;

~~~

 Whether the download's completion has already been logged. This is used to
 avoid inaccurate repeated logging.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadItemView> weak_ptr_factory_{this};

~~~

 Method factory used to delay reenabling of the item when opening the
 downloaded file.
