
## class DownloadShelfView
 DownloadShelfView is a view that contains individual views for each download,
 as well as a close button and a link to show all downloads.


 DownloadShelfView does not hold an infinite number of download views, rather
 it'll automatically remove views once a certain point is reached.

### METADATA_HEADER

DownloadShelfView::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadShelfView);
~~~

### DownloadShelfView

DownloadShelfView::DownloadShelfView
~~~cpp
DownloadShelfView(Browser* browser, BrowserView* parent);
~~~

### DownloadShelfView

DownloadShelfView
~~~cpp
DownloadShelfView(const DownloadShelfView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfView& operator=(const DownloadShelfView&) = delete;
~~~

### ~DownloadShelfView

DownloadShelfView::~DownloadShelfView
~~~cpp
~DownloadShelfView() override;
~~~

### IsShowing

DownloadShelfView::IsShowing
~~~cpp
bool IsShowing() const override;
~~~
 DownloadShelf:
### IsClosing

DownloadShelfView::IsClosing
~~~cpp
bool IsClosing() const override;
~~~

### GetView

DownloadShelfView::GetView
~~~cpp
views::View* GetView() override;
~~~

### CalculatePreferredSize

DownloadShelfView::CalculatePreferredSize
~~~cpp
gfx::Size CalculatePreferredSize() const override;
~~~
 views::AccessiblePaneView:
 TODO(crbug.com/1005568): Replace these with a LayoutManager
### Layout

DownloadShelfView::Layout
~~~cpp
void Layout() override;
~~~

### AnimationProgressed

DownloadShelfView::AnimationProgressed
~~~cpp
void AnimationProgressed(const gfx::Animation* animation) override;
~~~
 views::AnimationDelegateViews:
### AnimationEnded

DownloadShelfView::AnimationEnded
~~~cpp
void AnimationEnded(const gfx::Animation* animation) override;
~~~

### MouseMovedOutOfHost

DownloadShelfView::MouseMovedOutOfHost
~~~cpp
void MouseMovedOutOfHost() override;
~~~
 views::MouseWatcherListener:
### AutoClose

DownloadShelfView::AutoClose
~~~cpp
void AutoClose();
~~~
 Sent from the DownloadItemView when the user opens an item.

### RemoveDownloadView

DownloadShelfView::RemoveDownloadView
~~~cpp
void RemoveDownloadView(views::View* view);
~~~
 Removes a specified download view. The supplied view is deleted after
 it's removed.

### ConfigureButtonForTheme

DownloadShelfView::ConfigureButtonForTheme
~~~cpp
void ConfigureButtonForTheme(views::MdTextButton* button);
~~~
 Updates |button| according to the active theme.

### GetViewOfLastDownloadItemForTesting

DownloadShelfView::GetViewOfLastDownloadItemForTesting
~~~cpp
DownloadItemView* GetViewOfLastDownloadItemForTesting();
~~~

### DoShowDownload

DownloadShelfView::DoShowDownload
~~~cpp
void DoShowDownload(DownloadUIModel::DownloadUIModelPtr download) override;
~~~
 DownloadShelf:
### DoOpen

DownloadShelfView::DoOpen
~~~cpp
void DoOpen() override;
~~~

### DoClose

DownloadShelfView::DoClose
~~~cpp
void DoClose() override;
~~~

### DoHide

DownloadShelfView::DoHide
~~~cpp
void DoHide() override;
~~~

### DoUnhide

DownloadShelfView::DoUnhide
~~~cpp
void DoUnhide() override;
~~~

### OnPaintBorder

DownloadShelfView::OnPaintBorder
~~~cpp
void OnPaintBorder(gfx::Canvas* canvas) override;
~~~
 views::AccessiblePaneView:
### OnThemeChanged

DownloadShelfView::OnThemeChanged
~~~cpp
void OnThemeChanged() override;
~~~

### GetDefaultFocusableChild

DownloadShelfView::GetDefaultFocusableChild
~~~cpp
views::View* GetDefaultFocusableChild() override;
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadShelfView::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadShelfViewTest, ShowAllViewColors);
~~~

### new_item_animation_



~~~cpp

gfx::SlideAnimation new_item_animation_{this};

~~~

 The animation for adding new items to the shelf.

### shelf_animation_



~~~cpp

gfx::SlideAnimation shelf_animation_{this};

~~~

 The show/hide animation for the shelf itself.

### download_views_



~~~cpp

std::vector<DownloadItemView*> download_views_;

~~~

 The download views. These are also child Views, and deleted when
 the DownloadShelfView is deleted.

 TODO(pkasting): Remove this in favor of making these the children of a
 nested view, so they can easily be laid out and iterated.

### show_all_view_



~~~cpp

raw_ptr<views::MdTextButton> show_all_view_;

~~~

 Button for showing all downloads (chrome://downloads).

### close_button_



~~~cpp

raw_ptr<views::ImageButton> close_button_;

~~~

 Button for closing the downloads. This is contained as a child, and
 deleted by View.

### accessible_alert_



~~~cpp

raw_ptr<views::View> accessible_alert_;

~~~

 Hidden view that will contain status text for immediate output by
 screen readers.

### parent_



~~~cpp

raw_ptr<BrowserView> parent_;

~~~

 The window this shelf belongs to.

### last_opened_



~~~cpp

base::Time last_opened_;

~~~

 Time since the last time the download shelf was opened.

### SetLastOpened

DownloadShelfView::SetLastOpened
~~~cpp
void SetLastOpened();
~~~
 Set the time when the download shelf becomes visible.

### RecordShelfVisibleTime

DownloadShelfView::RecordShelfVisibleTime
~~~cpp
void RecordShelfVisibleTime();
~~~
 Emits a histogram recording the time between the shelf being visible
 and it being closed.

###  std::make_unique<views::MouseWatcherViewHost>


~~~cpp
views::MouseWatcher mouse_watcher_{
      std::make_unique<views::MouseWatcherViewHost>(this, gfx::Insets()), this};
~~~
### DownloadShelfView

DownloadShelfView
~~~cpp
DownloadShelfView(const DownloadShelfView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfView& operator=(const DownloadShelfView&) = delete;
~~~

### IsShowing

DownloadShelfView::IsShowing
~~~cpp
bool IsShowing() const override;
~~~
 DownloadShelf:
### IsClosing

DownloadShelfView::IsClosing
~~~cpp
bool IsClosing() const override;
~~~

### GetView

DownloadShelfView::GetView
~~~cpp
views::View* GetView() override;
~~~

### CalculatePreferredSize

DownloadShelfView::CalculatePreferredSize
~~~cpp
gfx::Size CalculatePreferredSize() const override;
~~~
 views::AccessiblePaneView:
 TODO(crbug.com/1005568): Replace these with a LayoutManager
### Layout

DownloadShelfView::Layout
~~~cpp
void Layout() override;
~~~

### AnimationProgressed

DownloadShelfView::AnimationProgressed
~~~cpp
void AnimationProgressed(const gfx::Animation* animation) override;
~~~
 views::AnimationDelegateViews:
### AnimationEnded

DownloadShelfView::AnimationEnded
~~~cpp
void AnimationEnded(const gfx::Animation* animation) override;
~~~

### MouseMovedOutOfHost

DownloadShelfView::MouseMovedOutOfHost
~~~cpp
void MouseMovedOutOfHost() override;
~~~
 views::MouseWatcherListener:
### AutoClose

DownloadShelfView::AutoClose
~~~cpp
void AutoClose();
~~~
 Sent from the DownloadItemView when the user opens an item.

### RemoveDownloadView

DownloadShelfView::RemoveDownloadView
~~~cpp
void RemoveDownloadView(views::View* view);
~~~
 Removes a specified download view. The supplied view is deleted after
 it's removed.

### ConfigureButtonForTheme

DownloadShelfView::ConfigureButtonForTheme
~~~cpp
void ConfigureButtonForTheme(views::MdTextButton* button);
~~~
 Updates |button| according to the active theme.

### GetViewOfLastDownloadItemForTesting

DownloadShelfView::GetViewOfLastDownloadItemForTesting
~~~cpp
DownloadItemView* GetViewOfLastDownloadItemForTesting();
~~~

### DoShowDownload

DownloadShelfView::DoShowDownload
~~~cpp
void DoShowDownload(DownloadUIModel::DownloadUIModelPtr download) override;
~~~
 DownloadShelf:
### DoOpen

DownloadShelfView::DoOpen
~~~cpp
void DoOpen() override;
~~~

### DoClose

DownloadShelfView::DoClose
~~~cpp
void DoClose() override;
~~~

### DoHide

DownloadShelfView::DoHide
~~~cpp
void DoHide() override;
~~~

### DoUnhide

DownloadShelfView::DoUnhide
~~~cpp
void DoUnhide() override;
~~~

### OnPaintBorder

DownloadShelfView::OnPaintBorder
~~~cpp
void OnPaintBorder(gfx::Canvas* canvas) override;
~~~
 views::AccessiblePaneView:
### OnThemeChanged

DownloadShelfView::OnThemeChanged
~~~cpp
void OnThemeChanged() override;
~~~

### GetDefaultFocusableChild

DownloadShelfView::GetDefaultFocusableChild
~~~cpp
views::View* GetDefaultFocusableChild() override;
~~~

### new_item_animation_



~~~cpp

gfx::SlideAnimation new_item_animation_{this};

~~~

 The animation for adding new items to the shelf.

### shelf_animation_



~~~cpp

gfx::SlideAnimation shelf_animation_{this};

~~~

 The show/hide animation for the shelf itself.

### download_views_



~~~cpp

std::vector<DownloadItemView*> download_views_;

~~~

 The download views. These are also child Views, and deleted when
 the DownloadShelfView is deleted.

 TODO(pkasting): Remove this in favor of making these the children of a
 nested view, so they can easily be laid out and iterated.

### show_all_view_



~~~cpp

raw_ptr<views::MdTextButton> show_all_view_;

~~~

 Button for showing all downloads (chrome://downloads).

### close_button_



~~~cpp

raw_ptr<views::ImageButton> close_button_;

~~~

 Button for closing the downloads. This is contained as a child, and
 deleted by View.

### accessible_alert_



~~~cpp

raw_ptr<views::View> accessible_alert_;

~~~

 Hidden view that will contain status text for immediate output by
 screen readers.

### parent_



~~~cpp

raw_ptr<BrowserView> parent_;

~~~

 The window this shelf belongs to.

### last_opened_



~~~cpp

base::Time last_opened_;

~~~

 Time since the last time the download shelf was opened.

### SetLastOpened

DownloadShelfView::SetLastOpened
~~~cpp
void SetLastOpened();
~~~
 Set the time when the download shelf becomes visible.

### RecordShelfVisibleTime

DownloadShelfView::RecordShelfVisibleTime
~~~cpp
void RecordShelfVisibleTime();
~~~
 Emits a histogram recording the time between the shelf being visible
 and it being closed.

###  std::make_unique<views::MouseWatcherViewHost>


~~~cpp
views::MouseWatcher mouse_watcher_{
      std::make_unique<views::MouseWatcherViewHost>(this, gfx::Insets()), this};
~~~