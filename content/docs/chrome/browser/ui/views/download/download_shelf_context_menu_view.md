
## class DownloadShelfContextMenuView

### DownloadShelfContextMenuView

DownloadShelfContextMenuView::DownloadShelfContextMenuView
~~~cpp
explicit DownloadShelfContextMenuView(DownloadItemView* download_item_view);
~~~
 TODO(crbug.com/1191555): Remove dependency on DownloadItemView.

### DownloadShelfContextMenuView

DownloadShelfContextMenuView::DownloadShelfContextMenuView
~~~cpp
explicit DownloadShelfContextMenuView(
      base::WeakPtr<DownloadUIModel> download_ui_model);
~~~

### DownloadShelfContextMenuView

DownloadShelfContextMenuView::DownloadShelfContextMenuView
~~~cpp
DownloadShelfContextMenuView(base::WeakPtr<DownloadUIModel> download_ui_model,
                               DownloadBubbleUIController* bubble_controller);
~~~

### DownloadShelfContextMenuView

DownloadShelfContextMenuView
~~~cpp
DownloadShelfContextMenuView(const DownloadShelfContextMenuView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfContextMenuView& operator=(const DownloadShelfContextMenuView&) =
      delete;
~~~

### ~DownloadShelfContextMenuView

DownloadShelfContextMenuView::~DownloadShelfContextMenuView
~~~cpp
~DownloadShelfContextMenuView() override;
~~~

### close_time

close_time
~~~cpp
base::TimeTicks close_time() const { return close_time_; }
~~~

### Run

DownloadShelfContextMenuView::Run
~~~cpp
void Run(views::Widget* parent_widget,
           const gfx::Rect& rect,
           ui::MenuSourceType source_type,
           base::RepeatingClosure on_menu_closed_callback);
~~~
 |rect| is the bounding area for positioning the menu in screen coordinates.

 The menu will be positioned above or below but not overlapping |rect|.

### SetOnMenuWillShowCallback

DownloadShelfContextMenuView::SetOnMenuWillShowCallback
~~~cpp
void SetOnMenuWillShowCallback(base::OnceClosure on_menu_will_show_callback);
~~~

### OnMenuClosed

DownloadShelfContextMenuView::OnMenuClosed
~~~cpp
void OnMenuClosed(base::RepeatingClosure on_menu_closed_callback);
~~~
 Callback for MenuRunner.

### OnMenuWillShow

DownloadShelfContextMenuView::OnMenuWillShow
~~~cpp
void OnMenuWillShow(ui::SimpleMenuModel* source) override;
~~~

### ExecuteCommand

DownloadShelfContextMenuView::ExecuteCommand
~~~cpp
void ExecuteCommand(int command_id, int event_flags) override;
~~~

### download_item_view_



~~~cpp

raw_ptr<DownloadItemView> download_item_view_ = nullptr;

~~~

 Parent download item view.

 TODO(crbug.com/1191555): Remove dependency on DownloadItemView.

### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_ = nullptr;

~~~

 Use this instead of DownloadItemView to submit download for feedback.

### on_menu_will_show_callback_



~~~cpp

base::OnceClosure on_menu_will_show_callback_;

~~~


### menu_runner_



~~~cpp

std::unique_ptr<views::MenuRunner> menu_runner_;

~~~


### close_time_



~~~cpp

base::TimeTicks close_time_;

~~~

 Time the menu was closed.

### field error



~~~cpp

bool download_commands_executed_recorded_[DownloadCommands::MAX + 1] = {
      false};

~~~

 Determines whether we should record if a DownloadCommand was executed.

### DownloadShelfContextMenuView

DownloadShelfContextMenuView
~~~cpp
DownloadShelfContextMenuView(const DownloadShelfContextMenuView&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfContextMenuView& operator=(const DownloadShelfContextMenuView&) =
      delete;
~~~

### close_time

close_time
~~~cpp
base::TimeTicks close_time() const { return close_time_; }
~~~

### Run

DownloadShelfContextMenuView::Run
~~~cpp
void Run(views::Widget* parent_widget,
           const gfx::Rect& rect,
           ui::MenuSourceType source_type,
           base::RepeatingClosure on_menu_closed_callback);
~~~
 |rect| is the bounding area for positioning the menu in screen coordinates.

 The menu will be positioned above or below but not overlapping |rect|.

### SetOnMenuWillShowCallback

DownloadShelfContextMenuView::SetOnMenuWillShowCallback
~~~cpp
void SetOnMenuWillShowCallback(base::OnceClosure on_menu_will_show_callback);
~~~

### OnMenuClosed

DownloadShelfContextMenuView::OnMenuClosed
~~~cpp
void OnMenuClosed(base::RepeatingClosure on_menu_closed_callback);
~~~
 Callback for MenuRunner.

### OnMenuWillShow

DownloadShelfContextMenuView::OnMenuWillShow
~~~cpp
void OnMenuWillShow(ui::SimpleMenuModel* source) override;
~~~

### ExecuteCommand

DownloadShelfContextMenuView::ExecuteCommand
~~~cpp
void ExecuteCommand(int command_id, int event_flags) override;
~~~

### download_item_view_



~~~cpp

raw_ptr<DownloadItemView> download_item_view_ = nullptr;

~~~

 Parent download item view.

 TODO(crbug.com/1191555): Remove dependency on DownloadItemView.

### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_ = nullptr;

~~~

 Use this instead of DownloadItemView to submit download for feedback.

### on_menu_will_show_callback_



~~~cpp

base::OnceClosure on_menu_will_show_callback_;

~~~


### menu_runner_



~~~cpp

std::unique_ptr<views::MenuRunner> menu_runner_;

~~~


### close_time_



~~~cpp

base::TimeTicks close_time_;

~~~

 Time the menu was closed.

### field error



~~~cpp

bool download_commands_executed_recorded_[DownloadCommands::MAX + 1] = {
      false};

~~~

 Determines whether we should record if a DownloadCommand was executed.
