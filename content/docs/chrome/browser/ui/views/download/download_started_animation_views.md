
## class DownloadStartedAnimationViews
 DownloadStartedAnimationViews creates an animation (which begins running
 immediately) that animates an image within the frame provided on the
 constructor. To use, simply instantiate a subclass using "new"; the class
 cleans itself up when it finishes animating.

### METADATA_HEADER

DownloadStartedAnimationViews::METADATA_HEADER
~~~cpp
METADATA_HEADER(DownloadStartedAnimationViews);
~~~

### DownloadStartedAnimationViews

DownloadStartedAnimationViews::DownloadStartedAnimationViews
~~~cpp
DownloadStartedAnimationViews(content::WebContents* web_contents,
                                base::TimeDelta duration,
                                const ui::ImageModel& image);
~~~

### DownloadStartedAnimationViews

DownloadStartedAnimationViews
~~~cpp
DownloadStartedAnimationViews(const DownloadStartedAnimationViews&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadStartedAnimationViews& operator=(
      const DownloadStartedAnimationViews&) = delete;
~~~

### ~DownloadStartedAnimationViews

~DownloadStartedAnimationViews
~~~cpp
~DownloadStartedAnimationViews() override = default;
~~~

### web_contents_bounds

web_contents_bounds
~~~cpp
const gfx::Rect& web_contents_bounds() const { return web_contents_bounds_; }
~~~

### GetX

GetX
~~~cpp
virtual int GetX() const = 0;
~~~
 Compute the position of the image for the current state.

### GetY

GetY
~~~cpp
virtual int GetY() const = 0;
~~~

### GetWidth

DownloadStartedAnimationViews::GetWidth
~~~cpp
virtual int GetWidth() const;
~~~

### GetHeight

DownloadStartedAnimationViews::GetHeight
~~~cpp
virtual int GetHeight() const;
~~~

### GetOpacity

GetOpacity
~~~cpp
virtual float GetOpacity() const = 0;
~~~

### WebContentsTooSmall

DownloadStartedAnimationViews::WebContentsTooSmall
~~~cpp
virtual bool WebContentsTooSmall(const gfx::Size& image_size) const;
~~~
 Whether the WebContents are too small to display the animation, in which
 case the animation should not be shown.

### Reposition

DownloadStartedAnimationViews::Reposition
~~~cpp
void Reposition();
~~~
 Move the animation to wherever it should currently be.

### Close

DownloadStartedAnimationViews::Close
~~~cpp
void Close();
~~~
 Shut down the animation cleanly.

### AnimateToState

DownloadStartedAnimationViews::AnimateToState
~~~cpp
void AnimateToState(double state) override;
~~~
 gfx::Animation
### popup_



~~~cpp

raw_ptr<views::Widget> popup_ = nullptr;

~~~

 We use a TYPE_POPUP for the popup so that it may float above any windows in
 our UI.

### web_contents_bounds_



~~~cpp

gfx::Rect web_contents_bounds_;

~~~

 The content area at the start of the animation. We store this so that the
 download shelf's resizing of the content area doesn't cause the animation
 to move around. This means that once started, the animation won't move
 with the parent window, but it's so fast that this shouldn't cause too
 much heartbreak.

### DownloadStartedAnimationViews

DownloadStartedAnimationViews
~~~cpp
DownloadStartedAnimationViews(const DownloadStartedAnimationViews&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadStartedAnimationViews& operator=(
      const DownloadStartedAnimationViews&) = delete;
~~~

### ~DownloadStartedAnimationViews

~DownloadStartedAnimationViews
~~~cpp
~DownloadStartedAnimationViews() override = default;
~~~

### web_contents_bounds

web_contents_bounds
~~~cpp
const gfx::Rect& web_contents_bounds() const { return web_contents_bounds_; }
~~~

### GetX

GetX
~~~cpp
virtual int GetX() const = 0;
~~~
 Compute the position of the image for the current state.

### GetY

GetY
~~~cpp
virtual int GetY() const = 0;
~~~

### GetOpacity

GetOpacity
~~~cpp
virtual float GetOpacity() const = 0;
~~~

### GetWidth

DownloadStartedAnimationViews::GetWidth
~~~cpp
virtual int GetWidth() const;
~~~

### GetHeight

DownloadStartedAnimationViews::GetHeight
~~~cpp
virtual int GetHeight() const;
~~~

### WebContentsTooSmall

DownloadStartedAnimationViews::WebContentsTooSmall
~~~cpp
virtual bool WebContentsTooSmall(const gfx::Size& image_size) const;
~~~
 Whether the WebContents are too small to display the animation, in which
 case the animation should not be shown.

### Reposition

DownloadStartedAnimationViews::Reposition
~~~cpp
void Reposition();
~~~
 Move the animation to wherever it should currently be.

### Close

DownloadStartedAnimationViews::Close
~~~cpp
void Close();
~~~
 Shut down the animation cleanly.

### AnimateToState

DownloadStartedAnimationViews::AnimateToState
~~~cpp
void AnimateToState(double state) override;
~~~
 gfx::Animation
### popup_



~~~cpp

raw_ptr<views::Widget> popup_ = nullptr;

~~~

 We use a TYPE_POPUP for the popup so that it may float above any windows in
 our UI.

### web_contents_bounds_



~~~cpp

gfx::Rect web_contents_bounds_;

~~~

 The content area at the start of the animation. We store this so that the
 download shelf's resizing of the content area doesn't cause the animation
 to move around. This means that once started, the animation won't move
 with the parent window, but it's so fast that this shouldn't cause too
 much heartbreak.
