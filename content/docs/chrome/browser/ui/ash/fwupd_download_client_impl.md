
## class FwupdDownloadClientImpl
 Class to provide Profile based information to the ash client.

### FwupdDownloadClientImpl

FwupdDownloadClientImpl::FwupdDownloadClientImpl
~~~cpp
FwupdDownloadClientImpl();
~~~

### FwupdDownloadClientImpl

FwupdDownloadClientImpl
~~~cpp
FwupdDownloadClientImpl(const FwupdDownloadClientImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
FwupdDownloadClientImpl& operator=(const FwupdDownloadClientImpl&) = delete;
~~~

### ~FwupdDownloadClientImpl

FwupdDownloadClientImpl::~FwupdDownloadClientImpl
~~~cpp
~FwupdDownloadClientImpl() override;
~~~

### GetURLLoaderFactory

FwupdDownloadClientImpl::GetURLLoaderFactory
~~~cpp
scoped_refptr<network::SharedURLLoaderFactory> GetURLLoaderFactory() override;
~~~
 ash::FwupdDownloadClient:
### FwupdDownloadClientImpl

FwupdDownloadClientImpl
~~~cpp
FwupdDownloadClientImpl(const FwupdDownloadClientImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
FwupdDownloadClientImpl& operator=(const FwupdDownloadClientImpl&) = delete;
~~~

### GetURLLoaderFactory

FwupdDownloadClientImpl::GetURLLoaderFactory
~~~cpp
scoped_refptr<network::SharedURLLoaderFactory> GetURLLoaderFactory() override;
~~~
 ash::FwupdDownloadClient: