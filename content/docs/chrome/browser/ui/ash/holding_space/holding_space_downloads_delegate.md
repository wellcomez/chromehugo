
## class HoldingSpaceDownloadsDelegate
 A delegate of `HoldingSpaceKeyedService` tasked with monitoring the status of
 of downloads on its behalf.

### HoldingSpaceDownloadsDelegate

HoldingSpaceDownloadsDelegate::HoldingSpaceDownloadsDelegate
~~~cpp
HoldingSpaceDownloadsDelegate(HoldingSpaceKeyedService* service,
                                HoldingSpaceModel* model);
~~~

### HoldingSpaceDownloadsDelegate

HoldingSpaceDownloadsDelegate
~~~cpp
HoldingSpaceDownloadsDelegate(const HoldingSpaceDownloadsDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
HoldingSpaceDownloadsDelegate& operator=(
      const HoldingSpaceDownloadsDelegate&) = delete;
~~~

### ~HoldingSpaceDownloadsDelegate

HoldingSpaceDownloadsDelegate::~HoldingSpaceDownloadsDelegate
~~~cpp
~HoldingSpaceDownloadsDelegate() override;
~~~

### OpenWhenComplete

HoldingSpaceDownloadsDelegate::OpenWhenComplete
~~~cpp
absl::optional<holding_space_metrics::ItemFailureToLaunchReason>
  OpenWhenComplete(const HoldingSpaceItem* item);
~~~
 Attempts to mark the download underlying the given `item` to open when
 complete. Returns `absl::nullopt` on success or the reason if the attempt
 was not successful.

### field error



~~~cpp

class InProgressDownload;

~~~


### field error



~~~cpp

class InProgressAshDownload;

~~~


### field error



~~~cpp

class InProgressLacrosDownload;

~~~


### OnPersistenceRestored

HoldingSpaceDownloadsDelegate::OnPersistenceRestored
~~~cpp
void OnPersistenceRestored() override;
~~~
 HoldingSpaceKeyedServiceDelegate:
### OnHoldingSpaceItemsRemoved

HoldingSpaceDownloadsDelegate::OnHoldingSpaceItemsRemoved
~~~cpp
void OnHoldingSpaceItemsRemoved(
      const std::vector<const HoldingSpaceItem*>& items) override;
~~~

### OnManagerInitialized

HoldingSpaceDownloadsDelegate::OnManagerInitialized
~~~cpp
void OnManagerInitialized(content::DownloadManager* manager) override;
~~~
 MultiProfileDownloadNotifier::Client:
### OnManagerGoingDown

HoldingSpaceDownloadsDelegate::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnDownloadCreated

HoldingSpaceDownloadsDelegate::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

HoldingSpaceDownloadsDelegate::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnMediaStoreUriAdded

HoldingSpaceDownloadsDelegate::OnMediaStoreUriAdded
~~~cpp
void OnMediaStoreUriAdded(
      const GURL& uri,
      const arc::mojom::MediaStoreMetadata& metadata) override;
~~~
 arc::ArcFileSystemBridge::Observer:
### OnLacrosDownloadCreated

HoldingSpaceDownloadsDelegate::OnLacrosDownloadCreated
~~~cpp
void OnLacrosDownloadCreated(
      const crosapi::mojom::DownloadItem& mojo_download_item) override;
~~~
 crosapi::DownloadControllerAsh::DownloadControllerObserver:
### OnLacrosDownloadUpdated

HoldingSpaceDownloadsDelegate::OnLacrosDownloadUpdated
~~~cpp
void OnLacrosDownloadUpdated(
      const crosapi::mojom::DownloadItem& mojo_download_item) override;
~~~

### OnLacrosDownloadsSynced

HoldingSpaceDownloadsDelegate::OnLacrosDownloadsSynced
~~~cpp
void OnLacrosDownloadsSynced(
      std::vector<crosapi::mojom::DownloadItemPtr> mojo_download_items);
~~~
 Invoked when the initial collection of `mojo_download_items` are synced
 from Lacros. Downloads are sorted chronologically by start time.

### OnDownloadUpdated

HoldingSpaceDownloadsDelegate::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(InProgressDownload* in_progress_download,
                         bool invalidate_image);
~~~
 Invoked when the specified `in_progress_download` is updated. If
 `invalidate_image` is `true`, the image for the associated holding space
 item will be explicitly invalidated. This is necessary if, for example, the
 underlying download is transitioning to/from a dangerous or insecure state.

### OnDownloadCompleted

HoldingSpaceDownloadsDelegate::OnDownloadCompleted
~~~cpp
void OnDownloadCompleted(InProgressDownload* in_progress_download);
~~~
 Invoked when the specified `in_progress_download` is completed.

### OnDownloadFailed

HoldingSpaceDownloadsDelegate::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const InProgressDownload* in_progress_download);
~~~
 Invoked when the specified `in_progress_download` fails. This may be due to
 cancellation, interruption, or destruction of the underlying download.

### EraseDownload

HoldingSpaceDownloadsDelegate::EraseDownload
~~~cpp
void EraseDownload(const InProgressDownload* in_progress_download);
~~~
 Invoked to erase the specified `in_progress_download` when it is no longer
 needed either due to completion or failure of the underlying download.

### CreateOrUpdateHoldingSpaceItem

HoldingSpaceDownloadsDelegate::CreateOrUpdateHoldingSpaceItem
~~~cpp
void CreateOrUpdateHoldingSpaceItem(InProgressDownload* in_progress_download,
                                      bool invalidate_image);
~~~
 Creates or updates the holding space item in the model associated with the
 specified `in_progress_download`. If `invalidate_image` is `true`, the
 image for the holding space item will be explicitly invalidated. This is
 necessary if, for example, the underlying download is transitioning to/from
 a dangerous or insecure state.

### Cancel

HoldingSpaceDownloadsDelegate::Cancel
~~~cpp
void Cancel(const HoldingSpaceItem* item, HoldingSpaceCommandId command_id);
~~~
 Attempts to cancel/pause/resume the download underlying the given `item`.

### Pause

HoldingSpaceDownloadsDelegate::Pause
~~~cpp
void Pause(const HoldingSpaceItem* item, HoldingSpaceCommandId command_id);
~~~

### Resume

HoldingSpaceDownloadsDelegate::Resume
~~~cpp
void Resume(const HoldingSpaceItem* item, HoldingSpaceCommandId command_id);
~~~

### in_progress_downloads_



~~~cpp

std::set<std::unique_ptr<InProgressDownload>, base::UniquePtrComparator>
      in_progress_downloads_;

~~~

 The collection of currently in-progress downloads.

### arc_file_system_bridge_observation_



~~~cpp

base::ScopedObservation<arc::ArcFileSystemBridge,
                          arc::ArcFileSystemBridge::Observer>
      arc_file_system_bridge_observation_{this};

~~~


### download_notifier_



~~~cpp

MultiProfileDownloadNotifier download_notifier_{
      this, /*wait_for_manager_initialization=*/true};

~~~

 Notifies this delegate of download events created for the profile
 associated with this delegate's service. If the incognito profile
 integration feature is enabled, the delegate is also notified of download
 events created for incognito profiles spawned from the service's main
 profile.

### weak_factory_



~~~cpp

base::WeakPtrFactory<HoldingSpaceDownloadsDelegate> weak_factory_{this};

~~~


### HoldingSpaceDownloadsDelegate

HoldingSpaceDownloadsDelegate
~~~cpp
HoldingSpaceDownloadsDelegate(const HoldingSpaceDownloadsDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
HoldingSpaceDownloadsDelegate& operator=(
      const HoldingSpaceDownloadsDelegate&) = delete;
~~~

### OpenWhenComplete

HoldingSpaceDownloadsDelegate::OpenWhenComplete
~~~cpp
absl::optional<holding_space_metrics::ItemFailureToLaunchReason>
  OpenWhenComplete(const HoldingSpaceItem* item);
~~~
 Attempts to mark the download underlying the given `item` to open when
 complete. Returns `absl::nullopt` on success or the reason if the attempt
 was not successful.

### field error



~~~cpp

class InProgressDownload;

~~~


### field error



~~~cpp

class InProgressAshDownload;

~~~


### field error



~~~cpp

class InProgressLacrosDownload;

~~~


### OnPersistenceRestored

HoldingSpaceDownloadsDelegate::OnPersistenceRestored
~~~cpp
void OnPersistenceRestored() override;
~~~
 HoldingSpaceKeyedServiceDelegate:
### OnHoldingSpaceItemsRemoved

HoldingSpaceDownloadsDelegate::OnHoldingSpaceItemsRemoved
~~~cpp
void OnHoldingSpaceItemsRemoved(
      const std::vector<const HoldingSpaceItem*>& items) override;
~~~

### OnManagerInitialized

HoldingSpaceDownloadsDelegate::OnManagerInitialized
~~~cpp
void OnManagerInitialized(content::DownloadManager* manager) override;
~~~
 MultiProfileDownloadNotifier::Client:
### OnManagerGoingDown

HoldingSpaceDownloadsDelegate::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnDownloadCreated

HoldingSpaceDownloadsDelegate::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

HoldingSpaceDownloadsDelegate::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnMediaStoreUriAdded

HoldingSpaceDownloadsDelegate::OnMediaStoreUriAdded
~~~cpp
void OnMediaStoreUriAdded(
      const GURL& uri,
      const arc::mojom::MediaStoreMetadata& metadata) override;
~~~
 arc::ArcFileSystemBridge::Observer:
### OnLacrosDownloadCreated

HoldingSpaceDownloadsDelegate::OnLacrosDownloadCreated
~~~cpp
void OnLacrosDownloadCreated(
      const crosapi::mojom::DownloadItem& mojo_download_item) override;
~~~
 crosapi::DownloadControllerAsh::DownloadControllerObserver:
### OnLacrosDownloadUpdated

HoldingSpaceDownloadsDelegate::OnLacrosDownloadUpdated
~~~cpp
void OnLacrosDownloadUpdated(
      const crosapi::mojom::DownloadItem& mojo_download_item) override;
~~~

### OnLacrosDownloadsSynced

HoldingSpaceDownloadsDelegate::OnLacrosDownloadsSynced
~~~cpp
void OnLacrosDownloadsSynced(
      std::vector<crosapi::mojom::DownloadItemPtr> mojo_download_items);
~~~
 Invoked when the initial collection of `mojo_download_items` are synced
 from Lacros. Downloads are sorted chronologically by start time.

### OnDownloadUpdated

HoldingSpaceDownloadsDelegate::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(InProgressDownload* in_progress_download,
                         bool invalidate_image);
~~~
 Invoked when the specified `in_progress_download` is updated. If
 `invalidate_image` is `true`, the image for the associated holding space
 item will be explicitly invalidated. This is necessary if, for example, the
 underlying download is transitioning to/from a dangerous or insecure state.

### OnDownloadCompleted

HoldingSpaceDownloadsDelegate::OnDownloadCompleted
~~~cpp
void OnDownloadCompleted(InProgressDownload* in_progress_download);
~~~
 Invoked when the specified `in_progress_download` is completed.

### OnDownloadFailed

HoldingSpaceDownloadsDelegate::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const InProgressDownload* in_progress_download);
~~~
 Invoked when the specified `in_progress_download` fails. This may be due to
 cancellation, interruption, or destruction of the underlying download.

### EraseDownload

HoldingSpaceDownloadsDelegate::EraseDownload
~~~cpp
void EraseDownload(const InProgressDownload* in_progress_download);
~~~
 Invoked to erase the specified `in_progress_download` when it is no longer
 needed either due to completion or failure of the underlying download.

### CreateOrUpdateHoldingSpaceItem

HoldingSpaceDownloadsDelegate::CreateOrUpdateHoldingSpaceItem
~~~cpp
void CreateOrUpdateHoldingSpaceItem(InProgressDownload* in_progress_download,
                                      bool invalidate_image);
~~~
 Creates or updates the holding space item in the model associated with the
 specified `in_progress_download`. If `invalidate_image` is `true`, the
 image for the holding space item will be explicitly invalidated. This is
 necessary if, for example, the underlying download is transitioning to/from
 a dangerous or insecure state.

### Cancel

HoldingSpaceDownloadsDelegate::Cancel
~~~cpp
void Cancel(const HoldingSpaceItem* item, HoldingSpaceCommandId command_id);
~~~
 Attempts to cancel/pause/resume the download underlying the given `item`.

### Pause

HoldingSpaceDownloadsDelegate::Pause
~~~cpp
void Pause(const HoldingSpaceItem* item, HoldingSpaceCommandId command_id);
~~~

### Resume

HoldingSpaceDownloadsDelegate::Resume
~~~cpp
void Resume(const HoldingSpaceItem* item, HoldingSpaceCommandId command_id);
~~~

### in_progress_downloads_



~~~cpp

std::set<std::unique_ptr<InProgressDownload>, base::UniquePtrComparator>
      in_progress_downloads_;

~~~

 The collection of currently in-progress downloads.

### arc_file_system_bridge_observation_



~~~cpp

base::ScopedObservation<arc::ArcFileSystemBridge,
                          arc::ArcFileSystemBridge::Observer>
      arc_file_system_bridge_observation_{this};

~~~


### download_notifier_



~~~cpp

MultiProfileDownloadNotifier download_notifier_{
      this, /*wait_for_manager_initialization=*/true};

~~~

 Notifies this delegate of download events created for the profile
 associated with this delegate's service. If the incognito profile
 integration feature is enabled, the delegate is also notified of download
 events created for incognito profiles spawned from the service's main
 profile.

### weak_factory_



~~~cpp

base::WeakPtrFactory<HoldingSpaceDownloadsDelegate> weak_factory_{this};

~~~

