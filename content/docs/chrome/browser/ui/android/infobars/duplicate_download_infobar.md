
## class DuplicateDownloadInfoBar
 A native-side implementation of an infobar to ask whether to continue
 downloading if target file already exists.

### CreateInfoBar

DuplicateDownloadInfoBar::CreateInfoBar
~~~cpp
static std::unique_ptr<infobars::InfoBar> CreateInfoBar(
      std::unique_ptr<android::DuplicateDownloadInfoBarDelegate> delegate);
~~~

### DuplicateDownloadInfoBar

DuplicateDownloadInfoBar
~~~cpp
DuplicateDownloadInfoBar(const DuplicateDownloadInfoBar&) = delete;
~~~

### operator=

operator=
~~~cpp
DuplicateDownloadInfoBar& operator=(const DuplicateDownloadInfoBar&) = delete;
~~~

### ~DuplicateDownloadInfoBar

DuplicateDownloadInfoBar::~DuplicateDownloadInfoBar
~~~cpp
~DuplicateDownloadInfoBar() override;
~~~

### RecordDuplicateDownloadInfobarEvent

DuplicateDownloadInfoBar::RecordDuplicateDownloadInfobarEvent
~~~cpp
static void RecordDuplicateDownloadInfobarEvent(
      bool is_offline_page,
      DuplicateDownloadInfobarEvent event);
~~~

### DuplicateDownloadInfoBar

DuplicateDownloadInfoBar::DuplicateDownloadInfoBar
~~~cpp
explicit DuplicateDownloadInfoBar(
      std::unique_ptr<android::DuplicateDownloadInfoBarDelegate> delegate);
~~~

### CreateRenderInfoBar

DuplicateDownloadInfoBar::CreateRenderInfoBar
~~~cpp
base::android::ScopedJavaLocalRef<jobject> CreateRenderInfoBar(
      JNIEnv* env,
      const ResourceIdMapper& resource_id_mapper) override;
~~~
 ConfirmInfoBar:
### GetDelegate

DuplicateDownloadInfoBar::GetDelegate
~~~cpp
android::DuplicateDownloadInfoBarDelegate* GetDelegate();
~~~

### DuplicateDownloadInfoBar

DuplicateDownloadInfoBar
~~~cpp
DuplicateDownloadInfoBar(const DuplicateDownloadInfoBar&) = delete;
~~~

### operator=

operator=
~~~cpp
DuplicateDownloadInfoBar& operator=(const DuplicateDownloadInfoBar&) = delete;
~~~

### CreateInfoBar

DuplicateDownloadInfoBar::CreateInfoBar
~~~cpp
static std::unique_ptr<infobars::InfoBar> CreateInfoBar(
      std::unique_ptr<android::DuplicateDownloadInfoBarDelegate> delegate);
~~~

### RecordDuplicateDownloadInfobarEvent

DuplicateDownloadInfoBar::RecordDuplicateDownloadInfobarEvent
~~~cpp
static void RecordDuplicateDownloadInfobarEvent(
      bool is_offline_page,
      DuplicateDownloadInfobarEvent event);
~~~

### CreateRenderInfoBar

DuplicateDownloadInfoBar::CreateRenderInfoBar
~~~cpp
base::android::ScopedJavaLocalRef<jobject> CreateRenderInfoBar(
      JNIEnv* env,
      const ResourceIdMapper& resource_id_mapper) override;
~~~
 ConfirmInfoBar:
### GetDelegate

DuplicateDownloadInfoBar::GetDelegate
~~~cpp
android::DuplicateDownloadInfoBarDelegate* GetDelegate();
~~~
