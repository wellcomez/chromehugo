### GetDesiredDownloadItemMode

GetDesiredDownloadItemMode
~~~cpp
DownloadItemMode GetDesiredDownloadItemMode(DownloadUIModel* download);
~~~
 Returns the mode that best reflects the current model state.

