
## class OfflinePageDownloadBridge
**
 * Bridge between C++ and Java to handle user initiated download of an offline
 * page. Other user interactions related to offline page are handled by the
 * DownloadUIAdapter.
 */
### OfflinePageDownloadBridge

OfflinePageDownloadBridge::OfflinePageDownloadBridge
~~~cpp
OfflinePageDownloadBridge(JNIEnv* env,
                            const base::android::JavaParamRef<jobject>& obj);
~~~

### OfflinePageDownloadBridge

OfflinePageDownloadBridge
~~~cpp
OfflinePageDownloadBridge(const OfflinePageDownloadBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePageDownloadBridge& operator=(const OfflinePageDownloadBridge&) =
      delete;
~~~

### ~OfflinePageDownloadBridge

OfflinePageDownloadBridge::~OfflinePageDownloadBridge
~~~cpp
~OfflinePageDownloadBridge();
~~~

### Destroy

OfflinePageDownloadBridge::Destroy
~~~cpp
void Destroy(JNIEnv* env, const base::android::JavaParamRef<jobject>& obj);
~~~

### ShowDownloadingToast

OfflinePageDownloadBridge::ShowDownloadingToast
~~~cpp
static void ShowDownloadingToast();
~~~

### weak_java_ref_



~~~cpp

JavaObjectWeakGlobalRef weak_java_ref_;

~~~


### OfflinePageDownloadBridge

OfflinePageDownloadBridge
~~~cpp
OfflinePageDownloadBridge(const OfflinePageDownloadBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePageDownloadBridge& operator=(const OfflinePageDownloadBridge&) =
      delete;
~~~

### Destroy

OfflinePageDownloadBridge::Destroy
~~~cpp
void Destroy(JNIEnv* env, const base::android::JavaParamRef<jobject>& obj);
~~~

### ShowDownloadingToast

OfflinePageDownloadBridge::ShowDownloadingToast
~~~cpp
static void ShowDownloadingToast();
~~~

### weak_java_ref_



~~~cpp

JavaObjectWeakGlobalRef weak_java_ref_;

~~~

