
## class OfflinePageShareHelper
 Helper class to retrieve the offline page content URI for sharing the page
 in download home UI.

### OfflinePageShareHelper

OfflinePageShareHelper::OfflinePageShareHelper
~~~cpp
explicit OfflinePageShareHelper(OfflinePageModel* model);
~~~

### OfflinePageShareHelper

OfflinePageShareHelper
~~~cpp
OfflinePageShareHelper(const OfflinePageShareHelper&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePageShareHelper& operator=(const OfflinePageShareHelper&) = delete;
~~~

### ~OfflinePageShareHelper

OfflinePageShareHelper::~OfflinePageShareHelper
~~~cpp
~OfflinePageShareHelper();
~~~

### GetShareInfo

OfflinePageShareHelper::GetShareInfo
~~~cpp
void GetShareInfo(const ContentId& id, ResultCallback result_cb);
~~~
 Get the share info. Mainly to retrieve the content URI.

### OnPageGetForShare

OfflinePageShareHelper::OnPageGetForShare
~~~cpp
void OnPageGetForShare(const std::vector<OfflinePageItem>& pages);
~~~

### AcquireFileAccessPermission

OfflinePageShareHelper::AcquireFileAccessPermission
~~~cpp
void AcquireFileAccessPermission();
~~~

### OnFileAccessPermissionDone

OfflinePageShareHelper::OnFileAccessPermissionDone
~~~cpp
void OnFileAccessPermissionDone(bool granted);
~~~

### OnPageGetForPublish

OfflinePageShareHelper::OnPageGetForPublish
~~~cpp
void OnPageGetForPublish(const std::vector<OfflinePageItem>& pages);
~~~

### OnPagePublished

OfflinePageShareHelper::OnPagePublished
~~~cpp
void OnPagePublished(const base::FilePath& file_path, SavePageResult result);
~~~

### NotifyCompletion

OfflinePageShareHelper::NotifyCompletion
~~~cpp
void NotifyCompletion(ShareResult result,
                        std::unique_ptr<OfflineItemShareInfo> share_info);
~~~

### model_



~~~cpp

raw_ptr<OfflinePageModel> model_;

~~~

 A keyed service, always valid.

### result_cb_



~~~cpp

ResultCallback result_cb_;

~~~


### content_id_



~~~cpp

ContentId content_id_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<OfflinePageShareHelper> weak_ptr_factory_{this};

~~~


### OfflinePageShareHelper

OfflinePageShareHelper
~~~cpp
OfflinePageShareHelper(const OfflinePageShareHelper&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePageShareHelper& operator=(const OfflinePageShareHelper&) = delete;
~~~

### GetShareInfo

OfflinePageShareHelper::GetShareInfo
~~~cpp
void GetShareInfo(const ContentId& id, ResultCallback result_cb);
~~~
 Get the share info. Mainly to retrieve the content URI.

### OnPageGetForShare

OfflinePageShareHelper::OnPageGetForShare
~~~cpp
void OnPageGetForShare(const std::vector<OfflinePageItem>& pages);
~~~

### AcquireFileAccessPermission

OfflinePageShareHelper::AcquireFileAccessPermission
~~~cpp
void AcquireFileAccessPermission();
~~~

### OnFileAccessPermissionDone

OfflinePageShareHelper::OnFileAccessPermissionDone
~~~cpp
void OnFileAccessPermissionDone(bool granted);
~~~

### OnPageGetForPublish

OfflinePageShareHelper::OnPageGetForPublish
~~~cpp
void OnPageGetForPublish(const std::vector<OfflinePageItem>& pages);
~~~

### OnPagePublished

OfflinePageShareHelper::OnPagePublished
~~~cpp
void OnPagePublished(const base::FilePath& file_path, SavePageResult result);
~~~

### NotifyCompletion

OfflinePageShareHelper::NotifyCompletion
~~~cpp
void NotifyCompletion(ShareResult result,
                        std::unique_ptr<OfflineItemShareInfo> share_info);
~~~

### model_



~~~cpp

raw_ptr<OfflinePageModel> model_;

~~~

 A keyed service, always valid.

### result_cb_



~~~cpp

ResultCallback result_cb_;

~~~


### content_id_



~~~cpp

ContentId content_id_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<OfflinePageShareHelper> weak_ptr_factory_{this};

~~~

