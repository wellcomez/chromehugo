
## class OfflinePageInfoBarDelegate
 An InfoBarDelegate that appears when a user attempt to save offline pages for
 a URL that is already saved.  This piggy-backs off the Download infobar,
 since the UI should be the same between Downloads and Offline Pages in this
 case.  There are two actions: Create New, and Overwrite.  Since Overwrite is
 not straightforward for offline pages, the behavior is to delete ALL other
 pages that are saved for the given URL, then save the newly requested page.

### Create

OfflinePageInfoBarDelegate::Create
~~~cpp
static void Create(base::OnceClosure confirm_continuation,
                     const GURL& page_to_download,
                     bool exists_duplicate_request,
                     content::WebContents* web_contents);
~~~
 Creates an offline page infobar and a delegate and adds the infobar to the
 infobars::ContentInfoBarManager associated with |web_contents|. |page_name|
 is the name shown for this file in the infobar text.

### OfflinePageInfoBarDelegate

OfflinePageInfoBarDelegate
~~~cpp
OfflinePageInfoBarDelegate(const OfflinePageInfoBarDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePageInfoBarDelegate& operator=(const OfflinePageInfoBarDelegate&) =
      delete;
~~~

### ~OfflinePageInfoBarDelegate

OfflinePageInfoBarDelegate::~OfflinePageInfoBarDelegate
~~~cpp
~OfflinePageInfoBarDelegate() override;
~~~

### OfflinePageInfoBarDelegate

OfflinePageInfoBarDelegate::OfflinePageInfoBarDelegate
~~~cpp
OfflinePageInfoBarDelegate(base::OnceClosure confirm_continuation,
                             const std::string& page_name,
                             const GURL& page_to_download,
                             bool duplicate_request_exists);
~~~

### GetIdentifier

OfflinePageInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 DuplicateDownloadInfoBarDelegate:
### EqualsDelegate

OfflinePageInfoBarDelegate::EqualsDelegate
~~~cpp
bool EqualsDelegate(InfoBarDelegate* delegate) const override;
~~~

### Accept

OfflinePageInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

OfflinePageInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### GetFilePath

OfflinePageInfoBarDelegate::GetFilePath
~~~cpp
std::string GetFilePath() const override;
~~~

### IsOfflinePage

OfflinePageInfoBarDelegate::IsOfflinePage
~~~cpp
bool IsOfflinePage() const override;
~~~

### GetPageURL

OfflinePageInfoBarDelegate::GetPageURL
~~~cpp
std::string GetPageURL() const override;
~~~

### ShouldExpire

OfflinePageInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### InfoBarDismissed

OfflinePageInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### DuplicateRequestExists

OfflinePageInfoBarDelegate::DuplicateRequestExists
~~~cpp
bool DuplicateRequestExists() const override;
~~~

### AsOfflinePageInfoBarDelegate

OfflinePageInfoBarDelegate::AsOfflinePageInfoBarDelegate
~~~cpp
OfflinePageInfoBarDelegate* AsOfflinePageInfoBarDelegate() override;
~~~

### confirm_continuation_



~~~cpp

base::OnceClosure confirm_continuation_;

~~~

 Continuation called when the user chooses to create a new file.

### page_name_



~~~cpp

std::string page_name_;

~~~


### page_to_download_



~~~cpp

GURL page_to_download_;

~~~


### duplicate_request_exists_



~~~cpp

bool duplicate_request_exists_;

~~~


### OfflinePageInfoBarDelegate

OfflinePageInfoBarDelegate
~~~cpp
OfflinePageInfoBarDelegate(const OfflinePageInfoBarDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePageInfoBarDelegate& operator=(const OfflinePageInfoBarDelegate&) =
      delete;
~~~

### Create

OfflinePageInfoBarDelegate::Create
~~~cpp
static void Create(base::OnceClosure confirm_continuation,
                     const GURL& page_to_download,
                     bool exists_duplicate_request,
                     content::WebContents* web_contents);
~~~
 Creates an offline page infobar and a delegate and adds the infobar to the
 infobars::ContentInfoBarManager associated with |web_contents|. |page_name|
 is the name shown for this file in the infobar text.

### GetIdentifier

OfflinePageInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 DuplicateDownloadInfoBarDelegate:
### EqualsDelegate

OfflinePageInfoBarDelegate::EqualsDelegate
~~~cpp
bool EqualsDelegate(InfoBarDelegate* delegate) const override;
~~~

### Accept

OfflinePageInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

OfflinePageInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### GetFilePath

OfflinePageInfoBarDelegate::GetFilePath
~~~cpp
std::string GetFilePath() const override;
~~~

### IsOfflinePage

OfflinePageInfoBarDelegate::IsOfflinePage
~~~cpp
bool IsOfflinePage() const override;
~~~

### GetPageURL

OfflinePageInfoBarDelegate::GetPageURL
~~~cpp
std::string GetPageURL() const override;
~~~

### ShouldExpire

OfflinePageInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### InfoBarDismissed

OfflinePageInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### DuplicateRequestExists

OfflinePageInfoBarDelegate::DuplicateRequestExists
~~~cpp
bool DuplicateRequestExists() const override;
~~~

### AsOfflinePageInfoBarDelegate

OfflinePageInfoBarDelegate::AsOfflinePageInfoBarDelegate
~~~cpp
OfflinePageInfoBarDelegate* AsOfflinePageInfoBarDelegate() override;
~~~

### confirm_continuation_



~~~cpp

base::OnceClosure confirm_continuation_;

~~~

 Continuation called when the user chooses to create a new file.

### page_name_



~~~cpp

std::string page_name_;

~~~


### page_to_download_



~~~cpp

GURL page_to_download_;

~~~


### duplicate_request_exists_



~~~cpp

bool duplicate_request_exists_;

~~~

