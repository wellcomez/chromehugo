
## class OfflinePrefetchDownloadClient

### OfflinePrefetchDownloadClient

OfflinePrefetchDownloadClient::OfflinePrefetchDownloadClient
~~~cpp
explicit OfflinePrefetchDownloadClient(SimpleFactoryKey* simple_factory_key);
~~~

### OfflinePrefetchDownloadClient

OfflinePrefetchDownloadClient
~~~cpp
OfflinePrefetchDownloadClient(const OfflinePrefetchDownloadClient&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePrefetchDownloadClient& operator=(
      const OfflinePrefetchDownloadClient&) = delete;
~~~

### ~OfflinePrefetchDownloadClient

OfflinePrefetchDownloadClient::~OfflinePrefetchDownloadClient
~~~cpp
~OfflinePrefetchDownloadClient() override;
~~~

### OnServiceInitialized

OfflinePrefetchDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~
 Overridden from Client:
### OnServiceUnavailable

OfflinePrefetchDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadFailed

OfflinePrefetchDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& completion_info,
                        download::Client::FailureReason reason) override;
~~~

### OnDownloadSucceeded

OfflinePrefetchDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

OfflinePrefetchDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

OfflinePrefetchDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### GetPrefetchDownloader

OfflinePrefetchDownloadClient::GetPrefetchDownloader
~~~cpp
PrefetchDownloader* GetPrefetchDownloader() const;
~~~

### simple_factory_key_



~~~cpp

raw_ptr<SimpleFactoryKey> simple_factory_key_;

~~~


### OfflinePrefetchDownloadClient

OfflinePrefetchDownloadClient
~~~cpp
OfflinePrefetchDownloadClient(const OfflinePrefetchDownloadClient&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflinePrefetchDownloadClient& operator=(
      const OfflinePrefetchDownloadClient&) = delete;
~~~

### OnServiceInitialized

OfflinePrefetchDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~
 Overridden from Client:
### OnServiceUnavailable

OfflinePrefetchDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadFailed

OfflinePrefetchDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& completion_info,
                        download::Client::FailureReason reason) override;
~~~

### OnDownloadSucceeded

OfflinePrefetchDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

OfflinePrefetchDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

OfflinePrefetchDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### GetPrefetchDownloader

OfflinePrefetchDownloadClient::GetPrefetchDownloader
~~~cpp
PrefetchDownloader* GetPrefetchDownloader() const;
~~~

### simple_factory_key_



~~~cpp

raw_ptr<SimpleFactoryKey> simple_factory_key_;

~~~

