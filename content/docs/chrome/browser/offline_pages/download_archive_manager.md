
## class DownloadArchiveManager
 Manages the directories used for OfflinePages.  Dynamically get the directory
 to use for downloads from the Download system.

### DownloadArchiveManager

DownloadArchiveManager::DownloadArchiveManager
~~~cpp
DownloadArchiveManager(
      const base::FilePath& temporary_archives_dir,
      const base::FilePath& private_archives_dir,
      const base::FilePath& public_archives_dir,
      const scoped_refptr<base::SequencedTaskRunner>& task_runner,
      PrefService* prefs);
~~~

### DownloadArchiveManager

DownloadArchiveManager
~~~cpp
DownloadArchiveManager(const DownloadArchiveManager&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadArchiveManager& operator=(const DownloadArchiveManager&) = delete;
~~~

### ~DownloadArchiveManager

DownloadArchiveManager::~DownloadArchiveManager
~~~cpp
~DownloadArchiveManager() override;
~~~

###  GetPublicArchivesDir


~~~cpp
const base::FilePath& GetPublicArchivesDir() override;
~~~
### prefs_



~~~cpp

raw_ptr<PrefService> prefs_;

~~~


### download_archives_dir_



~~~cpp

base::FilePath download_archives_dir_;

~~~


### DownloadArchiveManager

DownloadArchiveManager
~~~cpp
DownloadArchiveManager(const DownloadArchiveManager&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadArchiveManager& operator=(const DownloadArchiveManager&) = delete;
~~~

###  GetPublicArchivesDir


~~~cpp
const base::FilePath& GetPublicArchivesDir() override;
~~~
### prefs_



~~~cpp

raw_ptr<PrefService> prefs_;

~~~


### download_archives_dir_



~~~cpp

base::FilePath download_archives_dir_;

~~~

