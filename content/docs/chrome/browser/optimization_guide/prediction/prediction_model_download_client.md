
## class PredictionModelDownloadClient

### PredictionModelDownloadClient

PredictionModelDownloadClient::PredictionModelDownloadClient
~~~cpp
explicit PredictionModelDownloadClient(Profile* profile);
~~~

### ~PredictionModelDownloadClient

PredictionModelDownloadClient::~PredictionModelDownloadClient
~~~cpp
~PredictionModelDownloadClient() override;
~~~

### PredictionModelDownloadClient

PredictionModelDownloadClient
~~~cpp
PredictionModelDownloadClient(const PredictionModelDownloadClient&) = delete;
~~~

### operator=

operator=
~~~cpp
PredictionModelDownloadClient& operator=(
      const PredictionModelDownloadClient&) = delete;
~~~

### OnServiceInitialized

PredictionModelDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~
 download::Client:
### OnServiceUnavailable

PredictionModelDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

PredictionModelDownloadClient::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadFailed

PredictionModelDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& completion_info,
                        download::Client::FailureReason reason) override;
~~~

### OnDownloadSucceeded

PredictionModelDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

PredictionModelDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

PredictionModelDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### GetPredictionModelDownloadManager

PredictionModelDownloadClient::GetPredictionModelDownloadManager
~~~cpp
PredictionModelDownloadManager* GetPredictionModelDownloadManager();
~~~
 Returns the PredictionModelDownloadManager for the profile.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### PredictionModelDownloadClient

PredictionModelDownloadClient
~~~cpp
PredictionModelDownloadClient(const PredictionModelDownloadClient&) = delete;
~~~

### operator=

operator=
~~~cpp
PredictionModelDownloadClient& operator=(
      const PredictionModelDownloadClient&) = delete;
~~~

### OnServiceInitialized

PredictionModelDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~
 download::Client:
### OnServiceUnavailable

PredictionModelDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

PredictionModelDownloadClient::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadFailed

PredictionModelDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& completion_info,
                        download::Client::FailureReason reason) override;
~~~

### OnDownloadSucceeded

PredictionModelDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

PredictionModelDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

PredictionModelDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### GetPredictionModelDownloadManager

PredictionModelDownloadClient::GetPredictionModelDownloadManager
~~~cpp
PredictionModelDownloadManager* GetPredictionModelDownloadManager();
~~~
 Returns the PredictionModelDownloadManager for the profile.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~

