
## class ProfileDownloader
 Downloads user profile information. The profile picture is decoded in a
 sandboxed process.

### enum PictureStatus

~~~cpp
enum PictureStatus {
    PICTURE_SUCCESS,
    PICTURE_FAILED,
    PICTURE_DEFAULT,
    PICTURE_CACHED,
  }
~~~
### ProfileDownloader

ProfileDownloader::ProfileDownloader
~~~cpp
explicit ProfileDownloader(ProfileDownloaderDelegate* delegate);
~~~

### ProfileDownloader

ProfileDownloader
~~~cpp
ProfileDownloader(const ProfileDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
ProfileDownloader& operator=(const ProfileDownloader&) = delete;
~~~

### ~ProfileDownloader

ProfileDownloader::~ProfileDownloader
~~~cpp
~ProfileDownloader() override;
~~~

### Start

ProfileDownloader::Start
~~~cpp
virtual void Start();
~~~
 Starts downloading profile information if the necessary authorization token
 is ready. If not, subscribes to token service and starts fetching if the
 token is available. Should not be called more than once.

### StartForAccount

ProfileDownloader::StartForAccount
~~~cpp
virtual void StartForAccount(const CoreAccountId& account_id);
~~~
 Starts downloading profile information if the necessary authorization token
 is ready. If not, subscribes to token service and starts fetching if the
 token is available. Should not be called more than once.

### GetProfileFullName

ProfileDownloader::GetProfileFullName
~~~cpp
virtual std::u16string GetProfileFullName() const;
~~~
 On successful download this returns the full name of the user. For example
 "Pat Smith".

### GetProfileGivenName

ProfileDownloader::GetProfileGivenName
~~~cpp
virtual std::u16string GetProfileGivenName() const;
~~~
 On successful download this returns the given name of the user. For example
 if the name is "Pat Smith", the given name is "Pat".

### GetProfileLocale

ProfileDownloader::GetProfileLocale
~~~cpp
virtual std::string GetProfileLocale() const;
~~~
 On successful download this returns G+ locale preference of the user.

### GetProfilePicture

ProfileDownloader::GetProfilePicture
~~~cpp
virtual SkBitmap GetProfilePicture() const;
~~~
 On successful download this returns the profile picture of the user.

 For users with no profile picture set (that is, they have the default
 profile picture) this will return an Null bitmap.

### GetProfilePictureStatus

ProfileDownloader::GetProfilePictureStatus
~~~cpp
virtual PictureStatus GetProfilePictureStatus() const;
~~~
 Gets the profile picture status.

### GetProfilePictureURL

ProfileDownloader::GetProfilePictureURL
~~~cpp
virtual std::string GetProfilePictureURL() const;
~~~
 Gets the URL for the profile picture. This can be cached so that the same
 picture is not downloaded multiple times. This value should only be used
 when the picture status is PICTURE_SUCCESS.

### FRIEND_TEST_ALL_PREFIXES

ProfileDownloader::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(ProfileDownloaderTest, AccountInfoReady);
~~~

### FRIEND_TEST_ALL_PREFIXES

ProfileDownloader::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(ProfileDownloaderTest, AccountInfoNotReady);
~~~

### FRIEND_TEST_ALL_PREFIXES

ProfileDownloader::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(ProfileDownloaderTest,
                           AccountInfoNoPictureDoesNotCrash);
~~~

### FRIEND_TEST_ALL_PREFIXES

ProfileDownloader::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(ProfileDownloaderTest,
                           AccountInfoInvalidPictureURLDoesNotCrash);
~~~

### FetchImageData

ProfileDownloader::FetchImageData
~~~cpp
void FetchImageData();
~~~

### OnURLLoaderComplete

ProfileDownloader::OnURLLoaderComplete
~~~cpp
void OnURLLoaderComplete(std::unique_ptr<std::string> response_body);
~~~

### OnImageDecoded

ProfileDownloader::OnImageDecoded
~~~cpp
void OnImageDecoded(const SkBitmap& decoded_image) override;
~~~
 Overridden from ImageDecoder::ImageRequest:
### OnDecodeImageFailed

ProfileDownloader::OnDecodeImageFailed
~~~cpp
void OnDecodeImageFailed() override;
~~~

### OnExtendedAccountInfoUpdated

ProfileDownloader::OnExtendedAccountInfoUpdated
~~~cpp
void OnExtendedAccountInfoUpdated(const AccountInfo& info) override;
~~~
 Overridden from signin::IdentityManager::Observer:
### OnAccessTokenFetchComplete

ProfileDownloader::OnAccessTokenFetchComplete
~~~cpp
void OnAccessTokenFetchComplete(GoogleServiceAuthError error,
                                  signin::AccessTokenInfo access_token_info);
~~~
 Callback for AccessTokenFetcher.

### StartFetchingImage

ProfileDownloader::StartFetchingImage
~~~cpp
void StartFetchingImage();
~~~
 Issues the first request to get user profile image.

### GetAuthorizationHeader

ProfileDownloader::GetAuthorizationHeader
~~~cpp
const char* GetAuthorizationHeader() const;
~~~
 Gets the authorization header.

### StartFetchingOAuth2AccessToken

ProfileDownloader::StartFetchingOAuth2AccessToken
~~~cpp
void StartFetchingOAuth2AccessToken();
~~~
 Starts fetching OAuth2 access token. This is needed before the GAIA info
 can be downloaded.

### SEQUENCE_CHECKER

ProfileDownloader::SEQUENCE_CHECKER
~~~cpp
SEQUENCE_CHECKER(sequence_checker_);
~~~

### delegate_



~~~cpp

raw_ptr<ProfileDownloaderDelegate> delegate_;

~~~


### account_id_



~~~cpp

CoreAccountId account_id_;

~~~


### auth_token_



~~~cpp

std::string auth_token_;

~~~


### simple_loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> simple_loader_;

~~~


### oauth2_access_token_fetcher_



~~~cpp

std::unique_ptr<signin::AccessTokenFetcher> oauth2_access_token_fetcher_;

~~~


### account_info_



~~~cpp

AccountInfo account_info_;

~~~


### profile_picture_



~~~cpp

SkBitmap profile_picture_;

~~~


### picture_status_



~~~cpp

PictureStatus picture_status_ = PICTURE_FAILED;

~~~


### identity_manager_



~~~cpp

raw_ptr<signin::IdentityManager> identity_manager_;

~~~


### identity_manager_observation_



~~~cpp

base::ScopedObservation<signin::IdentityManager,
                          signin::IdentityManager::Observer>
      identity_manager_observation_{this};

~~~


### waiting_for_account_info_



~~~cpp

bool waiting_for_account_info_ = false;

~~~


### ProfileDownloader

ProfileDownloader
~~~cpp
ProfileDownloader(const ProfileDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
ProfileDownloader& operator=(const ProfileDownloader&) = delete;
~~~

### enum PictureStatus

~~~cpp
enum PictureStatus {
    PICTURE_SUCCESS,
    PICTURE_FAILED,
    PICTURE_DEFAULT,
    PICTURE_CACHED,
  };
~~~
### Start

ProfileDownloader::Start
~~~cpp
virtual void Start();
~~~
 Starts downloading profile information if the necessary authorization token
 is ready. If not, subscribes to token service and starts fetching if the
 token is available. Should not be called more than once.

### StartForAccount

ProfileDownloader::StartForAccount
~~~cpp
virtual void StartForAccount(const CoreAccountId& account_id);
~~~
 Starts downloading profile information if the necessary authorization token
 is ready. If not, subscribes to token service and starts fetching if the
 token is available. Should not be called more than once.

### GetProfileFullName

ProfileDownloader::GetProfileFullName
~~~cpp
virtual std::u16string GetProfileFullName() const;
~~~
 On successful download this returns the full name of the user. For example
 "Pat Smith".

### GetProfileGivenName

ProfileDownloader::GetProfileGivenName
~~~cpp
virtual std::u16string GetProfileGivenName() const;
~~~
 On successful download this returns the given name of the user. For example
 if the name is "Pat Smith", the given name is "Pat".

### GetProfileLocale

ProfileDownloader::GetProfileLocale
~~~cpp
virtual std::string GetProfileLocale() const;
~~~
 On successful download this returns G+ locale preference of the user.

### GetProfilePicture

ProfileDownloader::GetProfilePicture
~~~cpp
virtual SkBitmap GetProfilePicture() const;
~~~
 On successful download this returns the profile picture of the user.

 For users with no profile picture set (that is, they have the default
 profile picture) this will return an Null bitmap.

### GetProfilePictureStatus

ProfileDownloader::GetProfilePictureStatus
~~~cpp
virtual PictureStatus GetProfilePictureStatus() const;
~~~
 Gets the profile picture status.

### GetProfilePictureURL

ProfileDownloader::GetProfilePictureURL
~~~cpp
virtual std::string GetProfilePictureURL() const;
~~~
 Gets the URL for the profile picture. This can be cached so that the same
 picture is not downloaded multiple times. This value should only be used
 when the picture status is PICTURE_SUCCESS.

### FetchImageData

ProfileDownloader::FetchImageData
~~~cpp
void FetchImageData();
~~~

### OnURLLoaderComplete

ProfileDownloader::OnURLLoaderComplete
~~~cpp
void OnURLLoaderComplete(std::unique_ptr<std::string> response_body);
~~~

### OnImageDecoded

ProfileDownloader::OnImageDecoded
~~~cpp
void OnImageDecoded(const SkBitmap& decoded_image) override;
~~~
 Overridden from ImageDecoder::ImageRequest:
### OnDecodeImageFailed

ProfileDownloader::OnDecodeImageFailed
~~~cpp
void OnDecodeImageFailed() override;
~~~

### OnExtendedAccountInfoUpdated

ProfileDownloader::OnExtendedAccountInfoUpdated
~~~cpp
void OnExtendedAccountInfoUpdated(const AccountInfo& info) override;
~~~
 Overridden from signin::IdentityManager::Observer:
### OnAccessTokenFetchComplete

ProfileDownloader::OnAccessTokenFetchComplete
~~~cpp
void OnAccessTokenFetchComplete(GoogleServiceAuthError error,
                                  signin::AccessTokenInfo access_token_info);
~~~
 Callback for AccessTokenFetcher.

### StartFetchingImage

ProfileDownloader::StartFetchingImage
~~~cpp
void StartFetchingImage();
~~~
 Issues the first request to get user profile image.

### GetAuthorizationHeader

ProfileDownloader::GetAuthorizationHeader
~~~cpp
const char* GetAuthorizationHeader() const;
~~~
 Gets the authorization header.

### StartFetchingOAuth2AccessToken

ProfileDownloader::StartFetchingOAuth2AccessToken
~~~cpp
void StartFetchingOAuth2AccessToken();
~~~
 Starts fetching OAuth2 access token. This is needed before the GAIA info
 can be downloaded.

### delegate_



~~~cpp

raw_ptr<ProfileDownloaderDelegate> delegate_;

~~~


### account_id_



~~~cpp

CoreAccountId account_id_;

~~~


### auth_token_



~~~cpp

std::string auth_token_;

~~~


### simple_loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> simple_loader_;

~~~


### oauth2_access_token_fetcher_



~~~cpp

std::unique_ptr<signin::AccessTokenFetcher> oauth2_access_token_fetcher_;

~~~


### account_info_



~~~cpp

AccountInfo account_info_;

~~~


### profile_picture_



~~~cpp

SkBitmap profile_picture_;

~~~


### picture_status_



~~~cpp

PictureStatus picture_status_ = PICTURE_FAILED;

~~~


### identity_manager_



~~~cpp

raw_ptr<signin::IdentityManager> identity_manager_;

~~~


### identity_manager_observation_



~~~cpp

base::ScopedObservation<signin::IdentityManager,
                          signin::IdentityManager::Observer>
      identity_manager_observation_{this};

~~~


### waiting_for_account_info_



~~~cpp

bool waiting_for_account_info_ = false;

~~~

