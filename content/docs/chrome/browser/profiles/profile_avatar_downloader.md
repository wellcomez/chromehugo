
## class ProfileAvatarDownloader

### ProfileAvatarDownloader

ProfileAvatarDownloader::ProfileAvatarDownloader
~~~cpp
ProfileAvatarDownloader(size_t icon_index, FetchCompleteCallback callback);
~~~

### ~ProfileAvatarDownloader

ProfileAvatarDownloader::~ProfileAvatarDownloader
~~~cpp
~ProfileAvatarDownloader() override;
~~~

### Start

ProfileAvatarDownloader::Start
~~~cpp
void Start();
~~~

### OnFetchComplete

ProfileAvatarDownloader::OnFetchComplete
~~~cpp
void OnFetchComplete(const GURL& url, const SkBitmap* bitmap) override;
~~~
 BitmapFetcherDelegate:
### fetcher_



~~~cpp

std::unique_ptr<BitmapFetcher> fetcher_;

~~~

 Downloads the avatar image from a url.

### icon_index_



~~~cpp

size_t icon_index_;

~~~

 Index of the avatar being downloaded.

### callback_



~~~cpp

FetchCompleteCallback callback_;

~~~


### Start

ProfileAvatarDownloader::Start
~~~cpp
void Start();
~~~

### OnFetchComplete

ProfileAvatarDownloader::OnFetchComplete
~~~cpp
void OnFetchComplete(const GURL& url, const SkBitmap* bitmap) override;
~~~
 BitmapFetcherDelegate:
### fetcher_



~~~cpp

std::unique_ptr<BitmapFetcher> fetcher_;

~~~

 Downloads the avatar image from a url.

### icon_index_



~~~cpp

size_t icon_index_;

~~~

 Index of the avatar being downloaded.

### callback_



~~~cpp

FetchCompleteCallback callback_;

~~~

