
## class ProfileDownloaderDelegate
 namespace network
 Reports on success or failure of Profile download. It is OK to delete the
 |ProfileImageDownloader| instance in any of these handlers.

### enum FailureReason

~~~cpp
enum FailureReason {
    TOKEN_ERROR,          // Cannot fetch OAuth2 token.
    NETWORK_ERROR,        // Network failure while downloading profile.
    SERVICE_ERROR,        // Service returned an error or malformed reply.
    IMAGE_DECODE_FAILED,  // Cannot decode fetched image.
    INVALID_PROFILE_PICTURE_URL  // The profile picture URL is invalid.
  }
~~~
### ~ProfileDownloaderDelegate

~ProfileDownloaderDelegate
~~~cpp
virtual ~ProfileDownloaderDelegate() {}
~~~

### NeedsProfilePicture

NeedsProfilePicture
~~~cpp
virtual bool NeedsProfilePicture() const = 0;
~~~
 Whether the delegate need profile picture to be downloaded.

### GetDesiredImageSideLength

GetDesiredImageSideLength
~~~cpp
virtual int GetDesiredImageSideLength() const = 0;
~~~
 Returns the desired side length of the profile image. If 0, returns image
 of the originally uploaded size.

### GetCachedPictureURL

GetCachedPictureURL
~~~cpp
virtual std::string GetCachedPictureURL() const = 0;
~~~
 Returns the cached URL. If the cache URL matches the new image URL
 the image will not be downloaded. Return an empty string when there is no
 cached URL.

### GetIdentityManager

GetIdentityManager
~~~cpp
virtual signin::IdentityManager* GetIdentityManager() = 0;
~~~
 Returns the IdentityManager associated with this download request.

### GetURLLoaderFactory

GetURLLoaderFactory
~~~cpp
virtual network::mojom::URLLoaderFactory* GetURLLoaderFactory() = 0;
~~~
 Returns the URLLoaderFactory to use for this download request.

### OnProfileDownloadSuccess

OnProfileDownloadSuccess
~~~cpp
virtual void OnProfileDownloadSuccess(ProfileDownloader* downloader) = 0;
~~~
 Called when the profile download has completed successfully. Delegate can
 query the downloader for the picture and full name.

### OnProfileDownloadFailure

OnProfileDownloadFailure
~~~cpp
virtual void OnProfileDownloadFailure(
      ProfileDownloader* downloader,
      ProfileDownloaderDelegate::FailureReason reason) = 0;
~~~
 Called when the profile download has failed.

### ~ProfileDownloaderDelegate

~ProfileDownloaderDelegate
~~~cpp
virtual ~ProfileDownloaderDelegate() {}
~~~

### NeedsProfilePicture

NeedsProfilePicture
~~~cpp
virtual bool NeedsProfilePicture() const = 0;
~~~
 Whether the delegate need profile picture to be downloaded.

### GetDesiredImageSideLength

GetDesiredImageSideLength
~~~cpp
virtual int GetDesiredImageSideLength() const = 0;
~~~
 Returns the desired side length of the profile image. If 0, returns image
 of the originally uploaded size.

### GetCachedPictureURL

GetCachedPictureURL
~~~cpp
virtual std::string GetCachedPictureURL() const = 0;
~~~
 Returns the cached URL. If the cache URL matches the new image URL
 the image will not be downloaded. Return an empty string when there is no
 cached URL.

### GetIdentityManager

GetIdentityManager
~~~cpp
virtual signin::IdentityManager* GetIdentityManager() = 0;
~~~
 Returns the IdentityManager associated with this download request.

### GetURLLoaderFactory

GetURLLoaderFactory
~~~cpp
virtual network::mojom::URLLoaderFactory* GetURLLoaderFactory() = 0;
~~~
 Returns the URLLoaderFactory to use for this download request.

### OnProfileDownloadSuccess

OnProfileDownloadSuccess
~~~cpp
virtual void OnProfileDownloadSuccess(ProfileDownloader* downloader) = 0;
~~~
 Called when the profile download has completed successfully. Delegate can
 query the downloader for the picture and full name.

### OnProfileDownloadFailure

OnProfileDownloadFailure
~~~cpp
virtual void OnProfileDownloadFailure(
      ProfileDownloader* downloader,
      ProfileDownloaderDelegate::FailureReason reason) = 0;
~~~
 Called when the profile download has failed.

### enum FailureReason
 Error codes passed to OnProfileDownloadFailure.

~~~cpp
enum FailureReason {
    TOKEN_ERROR,          // Cannot fetch OAuth2 token.
    NETWORK_ERROR,        // Network failure while downloading profile.
    SERVICE_ERROR,        // Service returned an error or malformed reply.
    IMAGE_DECODE_FAILED,  // Cannot decode fetched image.
    INVALID_PROFILE_PICTURE_URL  // The profile picture URL is invalid.
  };
~~~