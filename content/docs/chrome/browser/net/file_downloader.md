
## class FileDownloader
 Helper class to download a file from a given URL and store it in a local
 file. If |overwrite| is true, any existing file will be overwritten;
 otherwise if the local file already exists, this will report success without
 downloading anything.

### enum Result

~~~cpp
enum Result {
    // The file was successfully downloaded.
    DOWNLOADED,
    // A local file at the given path already existed and was kept.
    EXISTS,
    // Downloading failed.
    FAILED
  }
~~~
### FileDownloader

FileDownloader::FileDownloader
~~~cpp
FileDownloader(
      const GURL& url,
      const base::FilePath& path,
      bool overwrite,
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      DownloadFinishedCallback callback,
      const net::NetworkTrafficAnnotationTag& traffic_annotation);
~~~
 Directly starts the download (if necessary) and runs |callback| when done.

 If the instance is destroyed before it is finished, |callback| is not run.

### FileDownloader

FileDownloader
~~~cpp
FileDownloader(const FileDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
FileDownloader& operator=(const FileDownloader&) = delete;
~~~

### ~FileDownloader

FileDownloader::~FileDownloader
~~~cpp
~FileDownloader();
~~~

### IsSuccess

IsSuccess
~~~cpp
static bool IsSuccess(Result result) { return result != FAILED; }
~~~

### OnSimpleDownloadComplete

FileDownloader::OnSimpleDownloadComplete
~~~cpp
void OnSimpleDownloadComplete(base::FilePath response_path);
~~~

### OnFileExistsCheckDone

FileDownloader::OnFileExistsCheckDone
~~~cpp
void OnFileExistsCheckDone(bool exists);
~~~

### OnFileMoveDone

FileDownloader::OnFileMoveDone
~~~cpp
void OnFileMoveDone(bool success);
~~~

### url_loader_factory_



~~~cpp

scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory_;

~~~


### callback_



~~~cpp

DownloadFinishedCallback callback_;

~~~


### simple_url_loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> simple_url_loader_;

~~~


### local_path_



~~~cpp

base::FilePath local_path_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<FileDownloader> weak_ptr_factory_{this};

~~~


### FileDownloader

FileDownloader
~~~cpp
FileDownloader(const FileDownloader&) = delete;
~~~

### operator=

operator=
~~~cpp
FileDownloader& operator=(const FileDownloader&) = delete;
~~~

### IsSuccess

IsSuccess
~~~cpp
static bool IsSuccess(Result result) { return result != FAILED; }
~~~

### enum Result

~~~cpp
enum Result {
    // The file was successfully downloaded.
    DOWNLOADED,
    // A local file at the given path already existed and was kept.
    EXISTS,
    // Downloading failed.
    FAILED
  };
~~~
### OnSimpleDownloadComplete

FileDownloader::OnSimpleDownloadComplete
~~~cpp
void OnSimpleDownloadComplete(base::FilePath response_path);
~~~

### OnFileExistsCheckDone

FileDownloader::OnFileExistsCheckDone
~~~cpp
void OnFileExistsCheckDone(bool exists);
~~~

### OnFileMoveDone

FileDownloader::OnFileMoveDone
~~~cpp
void OnFileMoveDone(bool success);
~~~

### url_loader_factory_



~~~cpp

scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory_;

~~~


### callback_



~~~cpp

DownloadFinishedCallback callback_;

~~~


### simple_url_loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> simple_url_loader_;

~~~


### local_path_



~~~cpp

base::FilePath local_path_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<FileDownloader> weak_ptr_factory_{this};

~~~

