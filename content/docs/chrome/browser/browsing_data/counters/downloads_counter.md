
## class DownloadsCounter
 A BrowsingDataCounter that counts the number of downloads as seen on the
 chrome://downloads page.

### DownloadsCounter

DownloadsCounter::DownloadsCounter
~~~cpp
explicit DownloadsCounter(Profile* profile);
~~~

### DownloadsCounter

DownloadsCounter
~~~cpp
DownloadsCounter(const DownloadsCounter&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsCounter& operator=(const DownloadsCounter&) = delete;
~~~

### ~DownloadsCounter

DownloadsCounter::~DownloadsCounter
~~~cpp
~DownloadsCounter() override;
~~~

### GetPrefName

DownloadsCounter::GetPrefName
~~~cpp
const char* GetPrefName() const override;
~~~

### Count

DownloadsCounter::Count
~~~cpp
void Count() override;
~~~
 BrowsingDataRemover implementation.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### DownloadsCounter

DownloadsCounter
~~~cpp
DownloadsCounter(const DownloadsCounter&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadsCounter& operator=(const DownloadsCounter&) = delete;
~~~

### GetPrefName

DownloadsCounter::GetPrefName
~~~cpp
const char* GetPrefName() const override;
~~~

### Count

DownloadsCounter::Count
~~~cpp
void Count() override;
~~~
 BrowsingDataRemover implementation.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~

