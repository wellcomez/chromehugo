
## class NearbyShareContactDownloaderImpl
 An implementation of the NearbyShareContactDownloader that uses
 NearbyShareClients to make HTTP calls. This class enforces a timeout for all
 HTTP calls but does not retry failures.

 TODO(https://crbug.com/1114516): Use SimpleURLLoader timeout functionality
 after refactoring our Nearby Share HTTP client.

###  Create


~~~cpp
class Factory {
   public:
    static std::unique_ptr<NearbyShareContactDownloader> Create(
        const std::string& device_id,
        base::TimeDelta timeout,
        NearbyShareClientFactory* client_factory,
        SuccessCallback success_callback,
        FailureCallback failure_callback);
    static void SetFactoryForTesting(Factory* test_factory);

   protected:
    virtual ~Factory();
    virtual std::unique_ptr<NearbyShareContactDownloader> CreateInstance(
        const std::string& device_id,
        base::TimeDelta timeout,
        NearbyShareClientFactory* client_factory,
        SuccessCallback success_callback,
        FailureCallback failure_callback) = 0;

   private:
    static Factory* test_factory_;
  };
~~~
### ~NearbyShareContactDownloaderImpl

NearbyShareContactDownloaderImpl::~NearbyShareContactDownloaderImpl
~~~cpp
~NearbyShareContactDownloaderImpl() override;
~~~

### NearbyShareContactDownloaderImpl

NearbyShareContactDownloaderImpl::NearbyShareContactDownloaderImpl
~~~cpp
NearbyShareContactDownloaderImpl(const std::string& device_id,
                                   base::TimeDelta timeout,
                                   NearbyShareClientFactory* client_factory,
                                   SuccessCallback success_callback,
                                   FailureCallback failure_callback);
~~~
 |timeout|: The maximum amount of time to wait between the request and
            response of each HTTP call before failing.

### OnRun

NearbyShareContactDownloaderImpl::OnRun
~~~cpp
void OnRun() override;
~~~
 NearbyShareContactDownloader:
### CallListContactPeople

NearbyShareContactDownloaderImpl::CallListContactPeople
~~~cpp
void CallListContactPeople(
      const absl::optional<std::string>& next_page_token);
~~~

### OnListContactPeopleSuccess

NearbyShareContactDownloaderImpl::OnListContactPeopleSuccess
~~~cpp
void OnListContactPeopleSuccess(
      const nearbyshare::proto::ListContactPeopleResponse& response);
~~~

### OnListContactPeopleFailure

NearbyShareContactDownloaderImpl::OnListContactPeopleFailure
~~~cpp
void OnListContactPeopleFailure(NearbyShareHttpError error);
~~~

### OnListContactPeopleTimeout

NearbyShareContactDownloaderImpl::OnListContactPeopleTimeout
~~~cpp
void OnListContactPeopleTimeout();
~~~

### 


~~~cpp
size_t current_page_number_ = 0;
~~~

### contacts_



~~~cpp

std::vector<nearbyshare::proto::ContactRecord> contacts_;

~~~


### timeout_



~~~cpp

base::TimeDelta timeout_;

~~~


### field error



~~~cpp

NearbyShareClientFactory* client_factory_ = nullptr;

~~~


### start_timestamp_



~~~cpp

base::TimeTicks start_timestamp_;

~~~


### client_



~~~cpp

std::unique_ptr<NearbyShareClient> client_;

~~~


### timer_



~~~cpp

base::OneShotTimer timer_;

~~~


### 


~~~cpp
size_t current_page_number_ = 0;
~~~

###  Create


~~~cpp
class Factory {
   public:
    static std::unique_ptr<NearbyShareContactDownloader> Create(
        const std::string& device_id,
        base::TimeDelta timeout,
        NearbyShareClientFactory* client_factory,
        SuccessCallback success_callback,
        FailureCallback failure_callback);
    static void SetFactoryForTesting(Factory* test_factory);

   protected:
    virtual ~Factory();
    virtual std::unique_ptr<NearbyShareContactDownloader> CreateInstance(
        const std::string& device_id,
        base::TimeDelta timeout,
        NearbyShareClientFactory* client_factory,
        SuccessCallback success_callback,
        FailureCallback failure_callback) = 0;

   private:
    static Factory* test_factory_;
  };
~~~
### OnRun

NearbyShareContactDownloaderImpl::OnRun
~~~cpp
void OnRun() override;
~~~
 NearbyShareContactDownloader:
### CallListContactPeople

NearbyShareContactDownloaderImpl::CallListContactPeople
~~~cpp
void CallListContactPeople(
      const absl::optional<std::string>& next_page_token);
~~~

### OnListContactPeopleSuccess

NearbyShareContactDownloaderImpl::OnListContactPeopleSuccess
~~~cpp
void OnListContactPeopleSuccess(
      const nearbyshare::proto::ListContactPeopleResponse& response);
~~~

### OnListContactPeopleFailure

NearbyShareContactDownloaderImpl::OnListContactPeopleFailure
~~~cpp
void OnListContactPeopleFailure(NearbyShareHttpError error);
~~~

### OnListContactPeopleTimeout

NearbyShareContactDownloaderImpl::OnListContactPeopleTimeout
~~~cpp
void OnListContactPeopleTimeout();
~~~

### contacts_



~~~cpp

std::vector<nearbyshare::proto::ContactRecord> contacts_;

~~~


### timeout_



~~~cpp

base::TimeDelta timeout_;

~~~


### field error



~~~cpp

NearbyShareClientFactory* client_factory_ = nullptr;

~~~


### start_timestamp_



~~~cpp

base::TimeTicks start_timestamp_;

~~~


### client_



~~~cpp

std::unique_ptr<NearbyShareClient> client_;

~~~


### timer_



~~~cpp

base::OneShotTimer timer_;

~~~

