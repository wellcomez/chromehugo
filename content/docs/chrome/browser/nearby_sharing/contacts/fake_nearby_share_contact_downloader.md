
## class FakeNearbyShareContactDownloader
 A fake implementation of NearbyShareContactDownloader, along with a fake
 factory, to be used in tests. Call the exposed base class method Fail() or
 Succeed() with the desired results to invoke the result callback.

###  Factory

 Factory that creates FakeNearbyShareContactDownloader instances. Use in
 NearbyShareContactDownloaderImpl::Factory::SetFactoryForTesting() in unit
 tests.

~~~cpp
class Factory : public NearbyShareContactDownloaderImpl::Factory {
   public:
    Factory();
    ~Factory() override;

    // Returns all FakeNearbyShareContactDownloader instances created by
    // CreateInstance().
    std::vector<FakeNearbyShareContactDownloader*>& instances() {
      return instances_;
    }

    base::TimeDelta latest_timeout() const { return latest_timeout_; }

    NearbyShareClientFactory* latest_client_factory() const {
      return latest_client_factory_;
    }

   private:
    // NearbyShareContactDownloaderImpl::Factory:
    std::unique_ptr<NearbyShareContactDownloader> CreateInstance(
        const std::string& device_id,
        base::TimeDelta timeout,
        NearbyShareClientFactory* client_factory,
        SuccessCallback success_callback,
        FailureCallback failure_callback) override;

    std::vector<FakeNearbyShareContactDownloader*> instances_;
    base::TimeDelta latest_timeout_;
    NearbyShareClientFactory* latest_client_factory_;
  };
~~~
### FakeNearbyShareContactDownloader

FakeNearbyShareContactDownloader::FakeNearbyShareContactDownloader
~~~cpp
FakeNearbyShareContactDownloader(const std::string& device_id,
                                   SuccessCallback success_callback,
                                   FailureCallback failure_callback);
~~~

### ~FakeNearbyShareContactDownloader

FakeNearbyShareContactDownloader::~FakeNearbyShareContactDownloader
~~~cpp
~FakeNearbyShareContactDownloader() override;
~~~

### OnRun

FakeNearbyShareContactDownloader::OnRun
~~~cpp
void OnRun() override;
~~~
 NearbyShareContactDownloader:
###  Factory

 Factory that creates FakeNearbyShareContactDownloader instances. Use in
 NearbyShareContactDownloaderImpl::Factory::SetFactoryForTesting() in unit
 tests.

~~~cpp
class Factory : public NearbyShareContactDownloaderImpl::Factory {
   public:
    Factory();
    ~Factory() override;

    // Returns all FakeNearbyShareContactDownloader instances created by
    // CreateInstance().
    std::vector<FakeNearbyShareContactDownloader*>& instances() {
      return instances_;
    }

    base::TimeDelta latest_timeout() const { return latest_timeout_; }

    NearbyShareClientFactory* latest_client_factory() const {
      return latest_client_factory_;
    }

   private:
    // NearbyShareContactDownloaderImpl::Factory:
    std::unique_ptr<NearbyShareContactDownloader> CreateInstance(
        const std::string& device_id,
        base::TimeDelta timeout,
        NearbyShareClientFactory* client_factory,
        SuccessCallback success_callback,
        FailureCallback failure_callback) override;

    std::vector<FakeNearbyShareContactDownloader*> instances_;
    base::TimeDelta latest_timeout_;
    NearbyShareClientFactory* latest_client_factory_;
  };
~~~
### OnRun

FakeNearbyShareContactDownloader::OnRun
~~~cpp
void OnRun() override;
~~~
 NearbyShareContactDownloader: