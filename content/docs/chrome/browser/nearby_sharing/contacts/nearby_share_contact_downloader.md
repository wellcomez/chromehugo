
## class NearbyShareContactDownloader
 Downloads the user's contact list from the server. NOTE: An instance should
 only be used once. All necessary parameters are passed to the constructor,
 and the download begins when Run() is called.

### NearbyShareContactDownloader

NearbyShareContactDownloader::NearbyShareContactDownloader
~~~cpp
NearbyShareContactDownloader(const std::string& device_id,
                               SuccessCallback success_callback,
                               FailureCallback failure_callback);
~~~
 |device_id|: The ID used by the Nearby server to differentiate multiple
              devices from the same account.

 |success_callback|: Invoked if the full contact list is successfully
                     downloaded.

 |failure_callback|: Invoked if the contact list download fails.

### ~NearbyShareContactDownloader

NearbyShareContactDownloader::~NearbyShareContactDownloader
~~~cpp
virtual ~NearbyShareContactDownloader();
~~~

### Run

NearbyShareContactDownloader::Run
~~~cpp
void Run();
~~~
 Starts the  contact list download.

### device_id

device_id
~~~cpp
const std::string& device_id() const { return device_id_; }
~~~

### OnRun

OnRun
~~~cpp
virtual void OnRun() = 0;
~~~

### Succeed

NearbyShareContactDownloader::Succeed
~~~cpp
void Succeed(std::vector<nearbyshare::proto::ContactRecord> contacts,
               uint32_t num_unreachable_contacts_filtered_out);
~~~
 Invokes the success callback with the input parameters.

### Fail

NearbyShareContactDownloader::Fail
~~~cpp
void Fail();
~~~
 Invokes the failure callback.

### was_run_



~~~cpp

bool was_run_ = false;

~~~


### device_id_



~~~cpp

const std::string device_id_;

~~~


### success_callback_



~~~cpp

SuccessCallback success_callback_;

~~~


### failure_callback_



~~~cpp

FailureCallback failure_callback_;

~~~


### device_id

device_id
~~~cpp
const std::string& device_id() const { return device_id_; }
~~~

### OnRun

OnRun
~~~cpp
virtual void OnRun() = 0;
~~~

### Run

NearbyShareContactDownloader::Run
~~~cpp
void Run();
~~~
 Starts the  contact list download.

### Succeed

NearbyShareContactDownloader::Succeed
~~~cpp
void Succeed(std::vector<nearbyshare::proto::ContactRecord> contacts,
               uint32_t num_unreachable_contacts_filtered_out);
~~~
 Invokes the success callback with the input parameters.

### Fail

NearbyShareContactDownloader::Fail
~~~cpp
void Fail();
~~~
 Invokes the failure callback.

### was_run_



~~~cpp

bool was_run_ = false;

~~~


### device_id_



~~~cpp

const std::string device_id_;

~~~


### success_callback_



~~~cpp

SuccessCallback success_callback_;

~~~


### failure_callback_



~~~cpp

FailureCallback failure_callback_;

~~~

