
## class DownloadMetadataManager
 A browser-wide object that manages the persistent state of metadata
 pertaining to a download.

### DownloadMetadataManager

DownloadMetadataManager::DownloadMetadataManager
~~~cpp
DownloadMetadataManager();
~~~

### DownloadMetadataManager

DownloadMetadataManager
~~~cpp
DownloadMetadataManager(const DownloadMetadataManager&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadMetadataManager& operator=(const DownloadMetadataManager&) = delete;
~~~

### ~DownloadMetadataManager

DownloadMetadataManager::~DownloadMetadataManager
~~~cpp
~DownloadMetadataManager() override;
~~~

### AddDownloadManager

DownloadMetadataManager::AddDownloadManager
~~~cpp
void AddDownloadManager(content::DownloadManager* download_manager);
~~~
 Adds |download_manager| to the set observed by the metadata manager.

### SetRequest

DownloadMetadataManager::SetRequest
~~~cpp
virtual void SetRequest(download::DownloadItem* download,
                          const ClientDownloadRequest* request);
~~~
 Sets |request| as the relevant metadata to persist for |download| upon
 completion. |request| will be persisted when the download completes, or
 discarded if the download is cancelled.

### GetDownloadDetails

DownloadMetadataManager::GetDownloadDetails
~~~cpp
virtual void GetDownloadDetails(content::BrowserContext* browser_context,
                                  GetDownloadDetailsCallback callback);
~~~
 Gets the persisted DownloadDetails for |browser_context|. |callback| will
 be run immediately if the data is available. Otherwise, it will be run
 later on the caller's thread.

### GetDownloadManagerForBrowserContext

DownloadMetadataManager::GetDownloadManagerForBrowserContext
~~~cpp
virtual content::DownloadManager* GetDownloadManagerForBrowserContext(
      content::BrowserContext* context);
~~~
 Returns the DownloadManager for a given BrowserContext. Virtual for tests.

### OnDownloadCreated

DownloadMetadataManager::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* download_manager,
                         download::DownloadItem* item) override;
~~~
 content::DownloadManager:Observer methods.

### ManagerGoingDown

DownloadMetadataManager::ManagerGoingDown
~~~cpp
void ManagerGoingDown(content::DownloadManager* download_manager) override;
~~~

### field error



~~~cpp

class ManagerContext;

~~~


### task_runner_



~~~cpp

const scoped_refptr<base::SequencedTaskRunner> task_runner_;

~~~

 A task runner to which IO tasks are posted.

### contexts_



~~~cpp

ManagerToContextMap contexts_;

~~~

 Contexts for each DownloadManager that has been added and has not yet
 "gone down".

### DownloadMetadataManager

DownloadMetadataManager
~~~cpp
DownloadMetadataManager(const DownloadMetadataManager&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadMetadataManager& operator=(const DownloadMetadataManager&) = delete;
~~~

### AddDownloadManager

DownloadMetadataManager::AddDownloadManager
~~~cpp
void AddDownloadManager(content::DownloadManager* download_manager);
~~~
 Adds |download_manager| to the set observed by the metadata manager.

### SetRequest

DownloadMetadataManager::SetRequest
~~~cpp
virtual void SetRequest(download::DownloadItem* download,
                          const ClientDownloadRequest* request);
~~~
 Sets |request| as the relevant metadata to persist for |download| upon
 completion. |request| will be persisted when the download completes, or
 discarded if the download is cancelled.

### GetDownloadDetails

DownloadMetadataManager::GetDownloadDetails
~~~cpp
virtual void GetDownloadDetails(content::BrowserContext* browser_context,
                                  GetDownloadDetailsCallback callback);
~~~
 Gets the persisted DownloadDetails for |browser_context|. |callback| will
 be run immediately if the data is available. Otherwise, it will be run
 later on the caller's thread.

### GetDownloadManagerForBrowserContext

DownloadMetadataManager::GetDownloadManagerForBrowserContext
~~~cpp
virtual content::DownloadManager* GetDownloadManagerForBrowserContext(
      content::BrowserContext* context);
~~~
 Returns the DownloadManager for a given BrowserContext. Virtual for tests.

### OnDownloadCreated

DownloadMetadataManager::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* download_manager,
                         download::DownloadItem* item) override;
~~~
 content::DownloadManager:Observer methods.

### ManagerGoingDown

DownloadMetadataManager::ManagerGoingDown
~~~cpp
void ManagerGoingDown(content::DownloadManager* download_manager) override;
~~~

### field error



~~~cpp

class ManagerContext;

~~~


### task_runner_



~~~cpp

const scoped_refptr<base::SequencedTaskRunner> task_runner_;

~~~

 A task runner to which IO tasks are posted.

### contexts_



~~~cpp

ManagerToContextMap contexts_;

~~~

 Contexts for each DownloadManager that has been added and has not yet
 "gone down".
