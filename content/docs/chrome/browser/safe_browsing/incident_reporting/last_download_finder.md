
## class LastDownloadFinder
 Finds the most recent executable download and non-executable download by
 any on-the-record profile with history that participates in safe browsing
 extended reporting.

### LastDownloadFinder

LastDownloadFinder
~~~cpp
LastDownloadFinder(const LastDownloadFinder&) = delete;
~~~

### operator=

operator=
~~~cpp
LastDownloadFinder& operator=(const LastDownloadFinder&) = delete;
~~~

### ~LastDownloadFinder

LastDownloadFinder::~LastDownloadFinder
~~~cpp
~LastDownloadFinder() override;
~~~

### Create

LastDownloadFinder::Create
~~~cpp
static std::unique_ptr<LastDownloadFinder> Create(
      DownloadDetailsGetter download_details_getter,
      LastDownloadCallback callback);
~~~
 Initiates an asynchronous search for the most recent downloads. |callback|
 will be run when the search is complete. The returned instance can be
 deleted to terminate the search, in which case |callback| is not invoked.

 Returns NULL without running |callback| if there are no eligible profiles
 to search.

### LastDownloadFinder

LastDownloadFinder::LastDownloadFinder
~~~cpp
LastDownloadFinder();
~~~
 Protected constructor so that unit tests can create a fake finder.

### enum ProfileWaitState

~~~cpp
enum ProfileWaitState {
    WAITING_FOR_METADATA,
    WAITING_FOR_HISTORY,
    WAITING_FOR_NON_BINARY_HISTORY,
  }
~~~
### LastDownloadFinder

LastDownloadFinder::LastDownloadFinder
~~~cpp
LastDownloadFinder(DownloadDetailsGetter download_details_getter,
                     LastDownloadCallback callback);
~~~

### SearchInProfile

LastDownloadFinder::SearchInProfile
~~~cpp
void SearchInProfile(Profile* profile);
~~~
 Adds |profile| to the set of profiles to be searched if it is an
 on-the-record profile with history that participates in safe browsing
 extended reporting. A search for metadata is initiated immediately.

### OnMetadataQuery

LastDownloadFinder::OnMetadataQuery
~~~cpp
void OnMetadataQuery(
      Profile* profile,
      std::unique_ptr<ClientIncidentReport_DownloadDetails> details);
~~~
 DownloadMetadataManager::GetDownloadDetailsCallback. If |details| are
 provided, retrieves them if they are the most relevant results. Otherwise
 begins a search in history. Reports results if there are no more pending
 queries.

### OnDownloadQuery

LastDownloadFinder::OnDownloadQuery
~~~cpp
void OnDownloadQuery(Profile* profile,
                       std::vector<history::DownloadRow> downloads);
~~~
 HistoryService::DownloadQueryCallback. Retrieves the most recent completed
 executable download from |downloads| and reports results if there are no
 more pending queries.

### RemoveProfileAndReportIfDone

LastDownloadFinder::RemoveProfileAndReportIfDone
~~~cpp
void RemoveProfileAndReportIfDone(
      std::map<Profile*, ProfileWaitState>::iterator iter);
~~~
 Removes the profile pointed to by |it| from profile_states_ and reports
 results if there are no more pending queries.

### ReportResults

LastDownloadFinder::ReportResults
~~~cpp
void ReportResults();
~~~
 Invokes the caller-supplied callback with the download found.

### OnProfileAdded

LastDownloadFinder::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile) override;
~~~
 ProfileManagerObserver:
### OnProfileWillBeDestroyed

LastDownloadFinder::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~
 ProfileObserver:
### OnHistoryServiceLoaded

LastDownloadFinder::OnHistoryServiceLoaded
~~~cpp
void OnHistoryServiceLoaded(history::HistoryService* service) override;
~~~
 history::HistoryServiceObserver:
### HistoryServiceBeingDeleted

LastDownloadFinder::HistoryServiceBeingDeleted
~~~cpp
void HistoryServiceBeingDeleted(
      history::HistoryService* history_service) override;
~~~

### download_details_getter_



~~~cpp

DownloadDetailsGetter download_details_getter_;

~~~

 Caller-supplied callback to make an asynchronous request for a profile's
 persistent download details.

### callback_



~~~cpp

LastDownloadCallback callback_;

~~~

 Caller-supplied callback to be invoked when the most recent download is
 found.

### profile_states_



~~~cpp

std::map<Profile*, ProfileWaitState> profile_states_;

~~~

 A mapping of profiles for which a download query is pending to their
 respective states.

### details_



~~~cpp

std::unique_ptr<ClientIncidentReport_DownloadDetails> details_;

~~~

 The most interesting download details retrieved from download metadata.

### most_recent_binary_row_



~~~cpp

history::DownloadRow most_recent_binary_row_;

~~~

 The most recent download, updated progressively as query results arrive.

### most_recent_non_binary_row_



~~~cpp

history::DownloadRow most_recent_non_binary_row_;

~~~

 The most recent non-binary download, updated progressively as query results
 arrive.

### history_service_observations_



~~~cpp

base::ScopedMultiSourceObservation<history::HistoryService,
                                     history::HistoryServiceObserver>
      history_service_observations_{this};

~~~

 HistoryServiceObserver
### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<LastDownloadFinder> weak_ptr_factory_{this};

~~~

 A factory for asynchronous operations on profiles' HistoryService.

### LastDownloadFinder

LastDownloadFinder
~~~cpp
LastDownloadFinder(const LastDownloadFinder&) = delete;
~~~

### operator=

operator=
~~~cpp
LastDownloadFinder& operator=(const LastDownloadFinder&) = delete;
~~~

### Create

LastDownloadFinder::Create
~~~cpp
static std::unique_ptr<LastDownloadFinder> Create(
      DownloadDetailsGetter download_details_getter,
      LastDownloadCallback callback);
~~~
 Initiates an asynchronous search for the most recent downloads. |callback|
 will be run when the search is complete. The returned instance can be
 deleted to terminate the search, in which case |callback| is not invoked.

 Returns NULL without running |callback| if there are no eligible profiles
 to search.

### enum ProfileWaitState

~~~cpp
enum ProfileWaitState {
    WAITING_FOR_METADATA,
    WAITING_FOR_HISTORY,
    WAITING_FOR_NON_BINARY_HISTORY,
  };
~~~
### SearchInProfile

LastDownloadFinder::SearchInProfile
~~~cpp
void SearchInProfile(Profile* profile);
~~~
 Adds |profile| to the set of profiles to be searched if it is an
 on-the-record profile with history that participates in safe browsing
 extended reporting. A search for metadata is initiated immediately.

### OnMetadataQuery

LastDownloadFinder::OnMetadataQuery
~~~cpp
void OnMetadataQuery(
      Profile* profile,
      std::unique_ptr<ClientIncidentReport_DownloadDetails> details);
~~~
 DownloadMetadataManager::GetDownloadDetailsCallback. If |details| are
 provided, retrieves them if they are the most relevant results. Otherwise
 begins a search in history. Reports results if there are no more pending
 queries.

### OnDownloadQuery

LastDownloadFinder::OnDownloadQuery
~~~cpp
void OnDownloadQuery(Profile* profile,
                       std::vector<history::DownloadRow> downloads);
~~~
 HistoryService::DownloadQueryCallback. Retrieves the most recent completed
 executable download from |downloads| and reports results if there are no
 more pending queries.

### RemoveProfileAndReportIfDone

LastDownloadFinder::RemoveProfileAndReportIfDone
~~~cpp
void RemoveProfileAndReportIfDone(
      std::map<Profile*, ProfileWaitState>::iterator iter);
~~~
 Removes the profile pointed to by |it| from profile_states_ and reports
 results if there are no more pending queries.

### ReportResults

LastDownloadFinder::ReportResults
~~~cpp
void ReportResults();
~~~
 Invokes the caller-supplied callback with the download found.

### OnProfileAdded

LastDownloadFinder::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile) override;
~~~
 ProfileManagerObserver:
### OnProfileWillBeDestroyed

LastDownloadFinder::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~
 ProfileObserver:
### OnHistoryServiceLoaded

LastDownloadFinder::OnHistoryServiceLoaded
~~~cpp
void OnHistoryServiceLoaded(history::HistoryService* service) override;
~~~
 history::HistoryServiceObserver:
### HistoryServiceBeingDeleted

LastDownloadFinder::HistoryServiceBeingDeleted
~~~cpp
void HistoryServiceBeingDeleted(
      history::HistoryService* history_service) override;
~~~

### download_details_getter_



~~~cpp

DownloadDetailsGetter download_details_getter_;

~~~

 Caller-supplied callback to make an asynchronous request for a profile's
 persistent download details.

### callback_



~~~cpp

LastDownloadCallback callback_;

~~~

 Caller-supplied callback to be invoked when the most recent download is
 found.

### profile_states_



~~~cpp

std::map<Profile*, ProfileWaitState> profile_states_;

~~~

 A mapping of profiles for which a download query is pending to their
 respective states.

### details_



~~~cpp

std::unique_ptr<ClientIncidentReport_DownloadDetails> details_;

~~~

 The most interesting download details retrieved from download metadata.

### most_recent_binary_row_



~~~cpp

history::DownloadRow most_recent_binary_row_;

~~~

 The most recent download, updated progressively as query results arrive.

### most_recent_non_binary_row_



~~~cpp

history::DownloadRow most_recent_non_binary_row_;

~~~

 The most recent non-binary download, updated progressively as query results
 arrive.

### history_service_observations_



~~~cpp

base::ScopedMultiSourceObservation<history::HistoryService,
                                     history::HistoryServiceObserver>
      history_service_observations_{this};

~~~

 HistoryServiceObserver
### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<LastDownloadFinder> weak_ptr_factory_{this};

~~~

 A factory for asynchronous operations on profiles' HistoryService.
