### GetCertificateAllowlistStrings

GetCertificateAllowlistStrings
~~~cpp
void GetCertificateAllowlistStrings(
    const net::X509Certificate& certificate,
    const net::X509Certificate& issuer,
    std::vector<std::string>* allowlist_strings);
~~~
 Given a certificate and its immediate issuer certificate, generates the
 list of strings that need to be checked against the download allowlist to
 determine whether the certificate is allowlisted.

### GetFileSystemAccessDownloadUrl

GetFileSystemAccessDownloadUrl
~~~cpp
GURL GetFileSystemAccessDownloadUrl(const GURL& frame_url);
~~~

### SelectArchiveEntries

SelectArchiveEntries
~~~cpp
google::protobuf::RepeatedPtrField<ClientDownloadRequest::ArchivedBinary>
SelectArchiveEntries(const google::protobuf::RepeatedPtrField<
                     ClientDownloadRequest::ArchivedBinary>& src_binaries);
~~~
 Determine which entries from `src_binaries` should be sent in the download
 ping.

