### LaunchDocumentAnalysisService

LaunchDocumentAnalysisService
~~~cpp
mojo::PendingRemote<chrome::mojom::DocumentAnalysisService>
LaunchDocumentAnalysisService();
~~~
 Launches a new instance of the DocumentAnalysis service in an isolated,
 sandboxed process and returns a remote interface to control the service.

 The lifetime of the process is tied to that of the remote.

