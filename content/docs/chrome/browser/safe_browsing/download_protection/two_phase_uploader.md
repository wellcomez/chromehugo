
## class TwoPhaseUploader
 Implements the Google two-phase resumable upload protocol.

 Protocol documentation:
 https://developers.google.com/storage/docs/developer-guide#resumable
 Note: This doc is for the Cloud Storage API which specifies the POST body
 must be empty, however the safebrowsing use of the two-phase protocol
 supports sending metadata in the POST request body. We also do not need the
 api-version and authorization headers.

 TODO(mattm): support retry / resume.

 Lives on the UI thread.

### enum State

~~~cpp
enum State {
    STATE_NONE,
    UPLOAD_METADATA,
    UPLOAD_FILE,
    STATE_SUCCESS,
  }
~~~
### ~TwoPhaseUploader

~TwoPhaseUploader
~~~cpp
virtual ~TwoPhaseUploader() {}
~~~

### Create

TwoPhaseUploader::Create
~~~cpp
static std::unique_ptr<TwoPhaseUploader> Create(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const GURL& base_url,
      const std::string& metadata,
      const base::FilePath& file_path,
      FinishCallback finish_callback,
      const net::NetworkTrafficAnnotationTag& traffic_annotation);
~~~
 Create the uploader.  The Start method must be called to begin the upload.

 Network processing will use |url_request_context_getter|.

 The uploaded |file_path| will be read on |file_task_runner|.

 The first phase request will be sent to |base_url|, with |metadata|
 included.

 On success |finish_callback| will be called with state = STATE_SUCCESS and
 the server response in response_data. On failure, state will specify
 which step the failure occurred in, and net_error, response_code, and
 response_data will specify information about the error. |finish_callback|
 will not be called if the upload is cancelled by destructing the
 TwoPhaseUploader object before completion.

### RegisterFactory

RegisterFactory
~~~cpp
static void RegisterFactory(TwoPhaseUploaderFactory* factory) {
    factory_ = factory;
  }
~~~
 Makes the passed |factory| the factory used to instantiate
 a TwoPhaseUploader. Useful for tests.

### Start

Start
~~~cpp
virtual void Start() = 0;
~~~
 Begins the upload process.

### field error



~~~cpp

static TwoPhaseUploaderFactory* factory_;

~~~

 The factory that controls the creation of SafeBrowsingProtocolManager.

 This is used by tests.

### ~TwoPhaseUploader

~TwoPhaseUploader
~~~cpp
virtual ~TwoPhaseUploader() {}
~~~

### RegisterFactory

RegisterFactory
~~~cpp
static void RegisterFactory(TwoPhaseUploaderFactory* factory) {
    factory_ = factory;
  }
~~~
 Makes the passed |factory| the factory used to instantiate
 a TwoPhaseUploader. Useful for tests.

### Start

Start
~~~cpp
virtual void Start() = 0;
~~~
 Begins the upload process.

### enum State

~~~cpp
enum State {
    STATE_NONE,
    UPLOAD_METADATA,
    UPLOAD_FILE,
    STATE_SUCCESS,
  };
~~~
### Create

TwoPhaseUploader::Create
~~~cpp
static std::unique_ptr<TwoPhaseUploader> Create(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const GURL& base_url,
      const std::string& metadata,
      const base::FilePath& file_path,
      FinishCallback finish_callback,
      const net::NetworkTrafficAnnotationTag& traffic_annotation);
~~~
 Create the uploader.  The Start method must be called to begin the upload.

 Network processing will use |url_request_context_getter|.

 The uploaded |file_path| will be read on |file_task_runner|.

 The first phase request will be sent to |base_url|, with |metadata|
 included.

 On success |finish_callback| will be called with state = STATE_SUCCESS and
 the server response in response_data. On failure, state will specify
 which step the failure occurred in, and net_error, response_code, and
 response_data will specify information about the error. |finish_callback|
 will not be called if the upload is cancelled by destructing the
 TwoPhaseUploader object before completion.

### field error



~~~cpp

static TwoPhaseUploaderFactory* factory_;

~~~

 The factory that controls the creation of SafeBrowsingProtocolManager.

 This is used by tests.

## class TwoPhaseUploaderFactory

### ~TwoPhaseUploaderFactory

~TwoPhaseUploaderFactory
~~~cpp
virtual ~TwoPhaseUploaderFactory() {}
~~~

### CreateTwoPhaseUploader

CreateTwoPhaseUploader
~~~cpp
virtual std::unique_ptr<TwoPhaseUploader> CreateTwoPhaseUploader(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const GURL& base_url,
      const std::string& metadata,
      const base::FilePath& file_path,
      TwoPhaseUploader::FinishCallback finish_callback,
      const net::NetworkTrafficAnnotationTag& traffic_annotation) = 0;
~~~

### ~TwoPhaseUploaderFactory

~TwoPhaseUploaderFactory
~~~cpp
virtual ~TwoPhaseUploaderFactory() {}
~~~

### CreateTwoPhaseUploader

CreateTwoPhaseUploader
~~~cpp
virtual std::unique_ptr<TwoPhaseUploader> CreateTwoPhaseUploader(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const GURL& base_url,
      const std::string& metadata,
      const base::FilePath& file_path,
      TwoPhaseUploader::FinishCallback finish_callback,
      const net::NetworkTrafficAnnotationTag& traffic_annotation) = 0;
~~~
