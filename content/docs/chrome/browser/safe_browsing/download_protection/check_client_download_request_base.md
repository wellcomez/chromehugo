
## class CheckClientDownloadRequestBase

### CheckClientDownloadRequestBase

CheckClientDownloadRequestBase::CheckClientDownloadRequestBase
~~~cpp
CheckClientDownloadRequestBase(
      GURL source_url,
      base::FilePath target_file_path,
      content::BrowserContext* browser_context,
      CheckDownloadCallback callback,
      DownloadProtectionService* service,
      scoped_refptr<SafeBrowsingDatabaseManager> database_manager,
      std::unique_ptr<DownloadRequestMaker> download_request_maker);
~~~

### CheckClientDownloadRequestBase

CheckClientDownloadRequestBase
~~~cpp
CheckClientDownloadRequestBase(const CheckClientDownloadRequestBase&) =
      delete;
~~~

### operator=

operator=
~~~cpp
CheckClientDownloadRequestBase& operator=(
      const CheckClientDownloadRequestBase&) = delete;
~~~

### ~CheckClientDownloadRequestBase

CheckClientDownloadRequestBase::~CheckClientDownloadRequestBase
~~~cpp
virtual ~CheckClientDownloadRequestBase();
~~~

### Start

CheckClientDownloadRequestBase::Start
~~~cpp
void Start();
~~~

### service

service
~~~cpp
DownloadProtectionService* service() const { return service_; }
~~~

### FinishRequest

CheckClientDownloadRequestBase::FinishRequest
~~~cpp
void FinishRequest(DownloadCheckResult result,
                     DownloadCheckResultReason reason);
~~~
 Subclasses can call this method to mark the request as finished (for
 example because the download was cancelled) before the safe browsing
 check has completed. This method can end up deleting |this|.

### ShouldSampleAllowlistedDownload

CheckClientDownloadRequestBase::ShouldSampleAllowlistedDownload
~~~cpp
bool ShouldSampleAllowlistedDownload();
~~~

### ShouldSampleUnsupportedFile

CheckClientDownloadRequestBase::ShouldSampleUnsupportedFile
~~~cpp
bool ShouldSampleUnsupportedFile(const base::FilePath& filename);
~~~

### IsDownloadManuallyBlocklisted

CheckClientDownloadRequestBase::IsDownloadManuallyBlocklisted
~~~cpp
bool IsDownloadManuallyBlocklisted(const ClientDownloadRequest& request);
~~~

### OnUrlAllowlistCheckDone

CheckClientDownloadRequestBase::OnUrlAllowlistCheckDone
~~~cpp
void OnUrlAllowlistCheckDone(bool is_allowlisted);
~~~

### OnRequestBuilt

CheckClientDownloadRequestBase::OnRequestBuilt
~~~cpp
void OnRequestBuilt(std::unique_ptr<ClientDownloadRequest> request_proto);
~~~

### StartTimeout

CheckClientDownloadRequestBase::StartTimeout
~~~cpp
void StartTimeout();
~~~

### SendRequest

CheckClientDownloadRequestBase::SendRequest
~~~cpp
void SendRequest();
~~~

### OnURLLoaderComplete

CheckClientDownloadRequestBase::OnURLLoaderComplete
~~~cpp
void OnURLLoaderComplete(std::unique_ptr<std::string> response_body);
~~~

### IsSupportedDownload

IsSupportedDownload
~~~cpp
virtual bool IsSupportedDownload(DownloadCheckResultReason* reason) = 0;
~~~

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual content::BrowserContext* GetBrowserContext() const = 0;
~~~

### IsCancelled

IsCancelled
~~~cpp
virtual bool IsCancelled() = 0;
~~~

### GetWeakPtr

GetWeakPtr
~~~cpp
virtual base::WeakPtr<CheckClientDownloadRequestBase> GetWeakPtr() = 0;
~~~

### NotifySendRequest

NotifySendRequest
~~~cpp
virtual void NotifySendRequest(const ClientDownloadRequest* request) = 0;
~~~
 Called right before a network request is send to the server.

### SetDownloadProtectionData

SetDownloadProtectionData
~~~cpp
virtual void SetDownloadProtectionData(
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict) = 0;
~~~
 TODO(mek): The following three methods are all called after receiving a
 response from the server. Perhaps these should be combined into one hook
 for concrete sub classes to implement, rather than having three separate
 hooks with slightly different logic when they are called.

 Called with the client download response as returned by the server, if one
 was returned and the returned verdict is unsafe (i.e. not safe or unknown).

### MaybeStorePingsForDownload

MaybeStorePingsForDownload
~~~cpp
virtual void MaybeStorePingsForDownload(DownloadCheckResult result,
                                          bool upload_requested,
                                          const std::string& request_data,
                                          const std::string& response_body) = 0;
~~~
 Called when a valid response has been received from the server.

### ShouldUploadBinary

ShouldUploadBinary
~~~cpp
virtual absl::optional<enterprise_connectors::AnalysisSettings>
  ShouldUploadBinary(DownloadCheckResultReason reason) = 0;
~~~
 Returns whether or not the file should be uploaded to Safe Browsing for
 deep scanning. Returns the settings to apply for analysis if the file
 should be uploaded for deep scanning, or absl::nullopt if it should not.

### UploadBinary

UploadBinary
~~~cpp
virtual void UploadBinary(
      DownloadCheckResult result,
      DownloadCheckResultReason reason,
      enterprise_connectors::AnalysisSettings settings) = 0;
~~~
 If ShouldUploadBinary returns settings, actually performs the upload to
 Safe Browsing for deep scanning.

### NotifyRequestFinished

NotifyRequestFinished
~~~cpp
virtual void NotifyRequestFinished(DownloadCheckResult result,
                                     DownloadCheckResultReason reason) = 0;
~~~
 Called whenever a request has completed.

### ShouldPromptForDeepScanning

ShouldPromptForDeepScanning
~~~cpp
virtual bool ShouldPromptForDeepScanning(
      bool server_requests_prompt) const = 0;
~~~
 Called when finishing the download, to decide whether to prompt the user
 for deep scanning or not.

### OnGotAccessToken

CheckClientDownloadRequestBase::OnGotAccessToken
~~~cpp
void OnGotAccessToken(const std::string& access_token);
~~~
 Called when |token_fetcher_| has finished fetching the access token.

### IsAllowlistedByPolicy

IsAllowlistedByPolicy
~~~cpp
virtual bool IsAllowlistedByPolicy() const = 0;
~~~
 Called at the request start to determine if we should bailout due to the
 file being allowlisted by policy
### SanitizeRequest

CheckClientDownloadRequestBase::SanitizeRequest
~~~cpp
void SanitizeRequest();
~~~
 For sampled unsupported file types, replaces all URLs in
 |client_download_request_| with their origin.

### source_url_



~~~cpp

const GURL source_url_;

~~~

 Source URL being downloaded from. This shuold always be set, but could be
 for example an artificial blob: URL if there is no source URL.

### target_file_path_



~~~cpp

const base::FilePath target_file_path_;

~~~


### callback_



~~~cpp

CheckDownloadCallback callback_;

~~~


### timeout_closure_



~~~cpp

base::CancelableOnceClosure timeout_closure_;

~~~

 A cancelable closure used to track the timeout. If we decide to upload the
 file for deep scanning, we want to cancel the timeout so it doesn't trigger
 in the middle of scanning.

### loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> loader_;

~~~


### client_download_request_



~~~cpp

std::unique_ptr<ClientDownloadRequest> client_download_request_;

~~~


### client_download_request_data_



~~~cpp

std::string client_download_request_data_;

~~~


### service_



~~~cpp

const raw_ptr<DownloadProtectionService> service_;

~~~


### database_manager_



~~~cpp

const scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### pingback_enabled_



~~~cpp

const bool pingback_enabled_;

~~~


### request_tracker_



~~~cpp

base::CancelableTaskTracker request_tracker_;

~~~


###  base::TimeTicks::Now

 For HistoryService lookup.

~~~cpp
base::TimeTicks start_time_ = base::TimeTicks::Now();
~~~
### timeout_start_time_



~~~cpp

base::TimeTicks timeout_start_time_;

~~~

 Used for stats.

### request_start_time_



~~~cpp

base::TimeTicks request_start_time_;

~~~


### skipped_url_allowlist_



~~~cpp

bool skipped_url_allowlist_ = false;

~~~


### skipped_certificate_allowlist_



~~~cpp

bool skipped_certificate_allowlist_ = false;

~~~


### sampled_unsupported_file_



~~~cpp

bool sampled_unsupported_file_ = false;

~~~


### is_extended_reporting_



~~~cpp

bool is_extended_reporting_ = false;

~~~


### is_incognito_



~~~cpp

bool is_incognito_ = false;

~~~


### is_enhanced_protection_



~~~cpp

bool is_enhanced_protection_ = false;

~~~


### token_fetcher_



~~~cpp

std::unique_ptr<SafeBrowsingTokenFetcher> token_fetcher_;

~~~

 The token fetcher used to attach OAuth access tokens to requests for
 appropriately consented users.

### access_token_



~~~cpp

std::string access_token_;

~~~

 The OAuth access token for the user profile, if needed in the request.

### download_request_maker_



~~~cpp

std::unique_ptr<DownloadRequestMaker> download_request_maker_;

~~~

 Used to create the download request proto.

### CheckClientDownloadRequestBase

CheckClientDownloadRequestBase
~~~cpp
CheckClientDownloadRequestBase(const CheckClientDownloadRequestBase&) =
      delete;
~~~

### operator=

operator=
~~~cpp
CheckClientDownloadRequestBase& operator=(
      const CheckClientDownloadRequestBase&) = delete;
~~~

### service

service
~~~cpp
DownloadProtectionService* service() const { return service_; }
~~~

### IsSupportedDownload

IsSupportedDownload
~~~cpp
virtual bool IsSupportedDownload(DownloadCheckResultReason* reason) = 0;
~~~

### GetBrowserContext

GetBrowserContext
~~~cpp
virtual content::BrowserContext* GetBrowserContext() const = 0;
~~~

### IsCancelled

IsCancelled
~~~cpp
virtual bool IsCancelled() = 0;
~~~

### GetWeakPtr

GetWeakPtr
~~~cpp
virtual base::WeakPtr<CheckClientDownloadRequestBase> GetWeakPtr() = 0;
~~~

### NotifySendRequest

NotifySendRequest
~~~cpp
virtual void NotifySendRequest(const ClientDownloadRequest* request) = 0;
~~~
 Called right before a network request is send to the server.

### SetDownloadProtectionData

SetDownloadProtectionData
~~~cpp
virtual void SetDownloadProtectionData(
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict) = 0;
~~~
 TODO(mek): The following three methods are all called after receiving a
 response from the server. Perhaps these should be combined into one hook
 for concrete sub classes to implement, rather than having three separate
 hooks with slightly different logic when they are called.

 Called with the client download response as returned by the server, if one
 was returned and the returned verdict is unsafe (i.e. not safe or unknown).

### MaybeStorePingsForDownload

MaybeStorePingsForDownload
~~~cpp
virtual void MaybeStorePingsForDownload(DownloadCheckResult result,
                                          bool upload_requested,
                                          const std::string& request_data,
                                          const std::string& response_body) = 0;
~~~
 Called when a valid response has been received from the server.

### ShouldUploadBinary

ShouldUploadBinary
~~~cpp
virtual absl::optional<enterprise_connectors::AnalysisSettings>
  ShouldUploadBinary(DownloadCheckResultReason reason) = 0;
~~~
 Returns whether or not the file should be uploaded to Safe Browsing for
 deep scanning. Returns the settings to apply for analysis if the file
 should be uploaded for deep scanning, or absl::nullopt if it should not.

### UploadBinary

UploadBinary
~~~cpp
virtual void UploadBinary(
      DownloadCheckResult result,
      DownloadCheckResultReason reason,
      enterprise_connectors::AnalysisSettings settings) = 0;
~~~
 If ShouldUploadBinary returns settings, actually performs the upload to
 Safe Browsing for deep scanning.

### NotifyRequestFinished

NotifyRequestFinished
~~~cpp
virtual void NotifyRequestFinished(DownloadCheckResult result,
                                     DownloadCheckResultReason reason) = 0;
~~~
 Called whenever a request has completed.

### ShouldPromptForDeepScanning

ShouldPromptForDeepScanning
~~~cpp
virtual bool ShouldPromptForDeepScanning(
      bool server_requests_prompt) const = 0;
~~~
 Called when finishing the download, to decide whether to prompt the user
 for deep scanning or not.

### IsAllowlistedByPolicy

IsAllowlistedByPolicy
~~~cpp
virtual bool IsAllowlistedByPolicy() const = 0;
~~~
 Called at the request start to determine if we should bailout due to the
 file being allowlisted by policy
### Start

CheckClientDownloadRequestBase::Start
~~~cpp
void Start();
~~~

### FinishRequest

CheckClientDownloadRequestBase::FinishRequest
~~~cpp
void FinishRequest(DownloadCheckResult result,
                     DownloadCheckResultReason reason);
~~~
 Subclasses can call this method to mark the request as finished (for
 example because the download was cancelled) before the safe browsing
 check has completed. This method can end up deleting |this|.

### ShouldSampleAllowlistedDownload

CheckClientDownloadRequestBase::ShouldSampleAllowlistedDownload
~~~cpp
bool ShouldSampleAllowlistedDownload();
~~~

### ShouldSampleUnsupportedFile

CheckClientDownloadRequestBase::ShouldSampleUnsupportedFile
~~~cpp
bool ShouldSampleUnsupportedFile(const base::FilePath& filename);
~~~

### IsDownloadManuallyBlocklisted

CheckClientDownloadRequestBase::IsDownloadManuallyBlocklisted
~~~cpp
bool IsDownloadManuallyBlocklisted(const ClientDownloadRequest& request);
~~~

### OnUrlAllowlistCheckDone

CheckClientDownloadRequestBase::OnUrlAllowlistCheckDone
~~~cpp
void OnUrlAllowlistCheckDone(bool is_allowlisted);
~~~

### OnRequestBuilt

CheckClientDownloadRequestBase::OnRequestBuilt
~~~cpp
void OnRequestBuilt(std::unique_ptr<ClientDownloadRequest> request_proto);
~~~

### StartTimeout

CheckClientDownloadRequestBase::StartTimeout
~~~cpp
void StartTimeout();
~~~

### SendRequest

CheckClientDownloadRequestBase::SendRequest
~~~cpp
void SendRequest();
~~~

### OnURLLoaderComplete

CheckClientDownloadRequestBase::OnURLLoaderComplete
~~~cpp
void OnURLLoaderComplete(std::unique_ptr<std::string> response_body);
~~~

### OnGotAccessToken

CheckClientDownloadRequestBase::OnGotAccessToken
~~~cpp
void OnGotAccessToken(const std::string& access_token);
~~~
 Called when |token_fetcher_| has finished fetching the access token.

### SanitizeRequest

CheckClientDownloadRequestBase::SanitizeRequest
~~~cpp
void SanitizeRequest();
~~~
 For sampled unsupported file types, replaces all URLs in
 |client_download_request_| with their origin.

### source_url_



~~~cpp

const GURL source_url_;

~~~

 Source URL being downloaded from. This shuold always be set, but could be
 for example an artificial blob: URL if there is no source URL.

### target_file_path_



~~~cpp

const base::FilePath target_file_path_;

~~~


### callback_



~~~cpp

CheckDownloadCallback callback_;

~~~


### timeout_closure_



~~~cpp

base::CancelableOnceClosure timeout_closure_;

~~~

 A cancelable closure used to track the timeout. If we decide to upload the
 file for deep scanning, we want to cancel the timeout so it doesn't trigger
 in the middle of scanning.

### loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> loader_;

~~~


### client_download_request_



~~~cpp

std::unique_ptr<ClientDownloadRequest> client_download_request_;

~~~


### client_download_request_data_



~~~cpp

std::string client_download_request_data_;

~~~


### service_



~~~cpp

const raw_ptr<DownloadProtectionService> service_;

~~~


### database_manager_



~~~cpp

const scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### pingback_enabled_



~~~cpp

const bool pingback_enabled_;

~~~


### request_tracker_



~~~cpp

base::CancelableTaskTracker request_tracker_;

~~~


###  base::TimeTicks::Now

 For HistoryService lookup.

~~~cpp
base::TimeTicks start_time_ = base::TimeTicks::Now();
~~~
### timeout_start_time_



~~~cpp

base::TimeTicks timeout_start_time_;

~~~

 Used for stats.

### request_start_time_



~~~cpp

base::TimeTicks request_start_time_;

~~~


### skipped_url_allowlist_



~~~cpp

bool skipped_url_allowlist_ = false;

~~~


### skipped_certificate_allowlist_



~~~cpp

bool skipped_certificate_allowlist_ = false;

~~~


### sampled_unsupported_file_



~~~cpp

bool sampled_unsupported_file_ = false;

~~~


### is_extended_reporting_



~~~cpp

bool is_extended_reporting_ = false;

~~~


### is_incognito_



~~~cpp

bool is_incognito_ = false;

~~~


### is_enhanced_protection_



~~~cpp

bool is_enhanced_protection_ = false;

~~~


### token_fetcher_



~~~cpp

std::unique_ptr<SafeBrowsingTokenFetcher> token_fetcher_;

~~~

 The token fetcher used to attach OAuth access tokens to requests for
 appropriately consented users.

### access_token_



~~~cpp

std::string access_token_;

~~~

 The OAuth access token for the user profile, if needed in the request.

### download_request_maker_



~~~cpp

std::unique_ptr<DownloadRequestMaker> download_request_maker_;

~~~

 Used to create the download request proto.
