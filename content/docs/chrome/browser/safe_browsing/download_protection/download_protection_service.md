
## class DownloadProtectionService
 This class provides an asynchronous API to check whether a particular
 client download is malicious or not.

### DownloadProtectionService

DownloadProtectionService::DownloadProtectionService
~~~cpp
explicit DownloadProtectionService(SafeBrowsingService* sb_service);
~~~
 Creates a download service.  The service is initially disabled.  You need
 to call SetEnabled() to start it.  |sb_service| owns this object.

### DownloadProtectionService

DownloadProtectionService
~~~cpp
DownloadProtectionService(const DownloadProtectionService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadProtectionService& operator=(const DownloadProtectionService&) =
      delete;
~~~

### ~DownloadProtectionService

DownloadProtectionService::~DownloadProtectionService
~~~cpp
virtual ~DownloadProtectionService();
~~~

### ParseManualBlocklistFlag

DownloadProtectionService::ParseManualBlocklistFlag
~~~cpp
virtual void ParseManualBlocklistFlag();
~~~
 Parse a flag of blocklisted sha256 hashes to check at each download.

 This is used for testing, to hunt for safe-browsing by-pass bugs.

### IsHashManuallyBlocklisted

DownloadProtectionService::IsHashManuallyBlocklisted
~~~cpp
virtual bool IsHashManuallyBlocklisted(const std::string& sha256_hash) const;
~~~
 Return true if this hash value is blocklisted via flag (for testing).

### CheckClientDownload

DownloadProtectionService::CheckClientDownload
~~~cpp
virtual void CheckClientDownload(download::DownloadItem* item,
                                   CheckDownloadRepeatingCallback callback);
~~~
 Checks whether the given client download is likely to be malicious or not.

 The result is delivered asynchronously via the given callback.  This
 method must be called on the UI thread, and the callback will also be
 invoked on the UI thread.  This method must be called once the download
 is finished and written to disk.

### MaybeCheckClientDownload

DownloadProtectionService::MaybeCheckClientDownload
~~~cpp
virtual bool MaybeCheckClientDownload(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback);
~~~
 Checks the user permissions, then calls |CheckClientDownload| if
 appropriate. Returns whether we began scanning.

### ShouldCheckDownloadUrl

DownloadProtectionService::ShouldCheckDownloadUrl
~~~cpp
virtual bool ShouldCheckDownloadUrl(download::DownloadItem* item);
~~~
 Returns whether the download URL should be checked for safety based on user
 prefs.

### CheckDownloadUrl

DownloadProtectionService::CheckDownloadUrl
~~~cpp
virtual void CheckDownloadUrl(download::DownloadItem* item,
                                CheckDownloadCallback callback);
~~~
 Checks whether any of the URLs in the redirect chain of the
 download match the SafeBrowsing bad binary URL list.  The result is
 delivered asynchronously via the given callback.  This method must be
 called on the UI thread, and the callback will also be invoked on the UI
 thread.  Pre-condition: !info.download_url_chain.empty().

### IsSupportedDownload

DownloadProtectionService::IsSupportedDownload
~~~cpp
virtual bool IsSupportedDownload(const download::DownloadItem& item,
                                   const base::FilePath& target_path) const;
~~~
 Returns true iff the download specified by |info| should be scanned by
 CheckClientDownload() for malicious content.

### CheckPPAPIDownloadRequest

DownloadProtectionService::CheckPPAPIDownloadRequest
~~~cpp
virtual void CheckPPAPIDownloadRequest(
      const GURL& requestor_url,
      content::RenderFrameHost* initiating_frame,
      const base::FilePath& default_file_path,
      const std::vector<base::FilePath::StringType>& alternate_extensions,
      Profile* profile,
      CheckDownloadCallback callback);
~~~

### CheckFileSystemAccessWrite

DownloadProtectionService::CheckFileSystemAccessWrite
~~~cpp
virtual void CheckFileSystemAccessWrite(
      std::unique_ptr<content::FileSystemAccessWriteItem> item,
      CheckDownloadCallback callback);
~~~
 Checks whether the given File System Access write operation is likely to be
 malicious or not. The result is delivered asynchronously via the given
 callback.  This method must be called on the UI thread, and the callback
 will also be invoked on the UI thread.  This method must be called once the
 write is finished and data has been written to disk.

### ShowDetailsForDownload

DownloadProtectionService::ShowDetailsForDownload
~~~cpp
void ShowDetailsForDownload(const download::DownloadItem* item,
                              content::PageNavigator* navigator);
~~~
 Display more information to the user regarding the download specified by
 |info|. This method is invoked when the user requests more information
 about a download that was marked as malicious.

### SetEnabled

DownloadProtectionService::SetEnabled
~~~cpp
void SetEnabled(bool enabled);
~~~
 Enables or disables the service.  This is usually called by the
 SafeBrowsingService, which tracks whether any profile uses these services
 at all.  Disabling causes any pending and future requests to have their
 callbacks called with "UNKNOWN" results.

### enabled

enabled
~~~cpp
bool enabled() const { return enabled_; }
~~~

### GetDownloadRequestTimeout

DownloadProtectionService::GetDownloadRequestTimeout
~~~cpp
base::TimeDelta GetDownloadRequestTimeout() const;
~~~
 Returns the timeout that is used by CheckClientDownload().

### MaybeBeginFeedbackForDownload

DownloadProtectionService::MaybeBeginFeedbackForDownload
~~~cpp
bool MaybeBeginFeedbackForDownload(
      Profile* profile,
      download::DownloadItem* download,
      DownloadCommands::Command download_command);
~~~
 Checks the user permissions, and submits the downloaded file if
 appropriate. Returns whether the submission was successful.

### RegisterClientDownloadRequestCallback

DownloadProtectionService::RegisterClientDownloadRequestCallback
~~~cpp
base::CallbackListSubscription RegisterClientDownloadRequestCallback(
      const ClientDownloadRequestCallback& callback);
~~~
 Registers a callback that will be run when a ClientDownloadRequest has
 been formed.

### RegisterFileSystemAccessWriteRequestCallback

DownloadProtectionService::RegisterFileSystemAccessWriteRequestCallback
~~~cpp
base::CallbackListSubscription RegisterFileSystemAccessWriteRequestCallback(
      const FileSystemAccessWriteRequestCallback& callback);
~~~
 Registers a callback that will be run when a FileSystemAccessWriteRequest
 has been formed.

### RegisterPPAPIDownloadRequestCallback

DownloadProtectionService::RegisterPPAPIDownloadRequestCallback
~~~cpp
base::CallbackListSubscription RegisterPPAPIDownloadRequestCallback(
      const PPAPIDownloadRequestCallback& callback);
~~~
 Registers a callback that will be run when a PPAPI ClientDownloadRequest
 has been formed.

### allowlist_sample_rate

allowlist_sample_rate
~~~cpp
double allowlist_sample_rate() const { return allowlist_sample_rate_; }
~~~

### SetDownloadProtectionData

DownloadProtectionService::SetDownloadProtectionData
~~~cpp
static void SetDownloadProtectionData(
      download::DownloadItem* item,
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict);
~~~

### GetDownloadPingToken

DownloadProtectionService::GetDownloadPingToken
~~~cpp
static std::string GetDownloadPingToken(const download::DownloadItem* item);
~~~

### GetDownloadProtectionVerdict

DownloadProtectionService::GetDownloadProtectionVerdict
~~~cpp
static ClientDownloadResponse::Verdict GetDownloadProtectionVerdict(
      const download::DownloadItem* item);
~~~

### GetDownloadProtectionTailoredVerdict

DownloadProtectionService::GetDownloadProtectionTailoredVerdict
~~~cpp
static ClientDownloadResponse::TailoredVerdict
  GetDownloadProtectionTailoredVerdict(const download::DownloadItem* item);
~~~

### MaybeSendDangerousDownloadOpenedReport

DownloadProtectionService::MaybeSendDangerousDownloadOpenedReport
~~~cpp
void MaybeSendDangerousDownloadOpenedReport(download::DownloadItem* item,
                                              bool show_download_in_folder);
~~~
 Sends dangerous download opened report when download is opened or
 shown in folder, and if the following conditions are met:
 (1) it is a dangerous download.

 (2) user is NOT in incognito mode.

 (3) user is opted-in for extended reporting.

### ReportDelayedBypassEvent

DownloadProtectionService::ReportDelayedBypassEvent
~~~cpp
void ReportDelayedBypassEvent(download::DownloadItem* download,
                                download::DownloadDangerType danger_type);
~~~
 Called to trigger a bypass event report for |download|. This is used when
 the async scan verdict is received for a file that was already opened by
 the user while it was being processed, and the verdict ended up being
 "dangerous" or "sensitive".

### UploadForDeepScanning

DownloadProtectionService::UploadForDeepScanning
~~~cpp
void UploadForDeepScanning(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback,
      DeepScanningRequest::DeepScanTrigger trigger,
      DownloadCheckResult download_check_result,
      enterprise_connectors::AnalysisSettings analysis_settings);
~~~
 Uploads `item` to Safe Browsing for deep scanning, using the upload
 service attached to the profile `item` was downloaded in. This is
 non-blocking, and the result we be provided through `callback`. `trigger`
 is used to identify the reason for deep scanning, aka enterprise policy or
 APP. `download_check_result` indicates the previously known SB verdict to
 apply to the download should deep scanning fail. `analysis_settings`
 contains settings to apply throughout scanning (types of scans to do,
 whether to block/allow large files, etc). This must be called on the UI
 thread.

### UploadSavePackageForDeepScanning

DownloadProtectionService::UploadSavePackageForDeepScanning
~~~cpp
void UploadSavePackageForDeepScanning(
      download::DownloadItem* item,
      base::flat_map<base::FilePath, base::FilePath> save_package_files,
      CheckDownloadRepeatingCallback callback,
      enterprise_connectors::AnalysisSettings analysis_settings);
~~~
 Uploads a save package `item` for deep scanning. `save_package_file`
 contains a mapping of on-disk files part of that save package to their
 final paths.

### GetDeepScanningRequests

DownloadProtectionService::GetDeepScanningRequests
~~~cpp
std::vector<DeepScanningRequest*> GetDeepScanningRequests();
~~~
 Returns all the currently active deep scanning requests.

### GetURLLoaderFactory

DownloadProtectionService::GetURLLoaderFactory
~~~cpp
virtual scoped_refptr<network::SharedURLLoaderFactory> GetURLLoaderFactory(
      content::BrowserContext* browser_context);
~~~

### RemovePendingDownloadRequests

DownloadProtectionService::RemovePendingDownloadRequests
~~~cpp
void RemovePendingDownloadRequests(content::BrowserContext* browser_context);
~~~
 Removes all pending download requests that are associated with the
 `browser_context`.

### FRIEND_TEST_ALL_PREFIXES

DownloadProtectionService::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadProtectionServiceTest,
                           TestDownloadRequestTimeout);
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadProtectionService::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadProtectionServiceTest,
                           PPAPIDownloadRequest_InvalidResponse);
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadProtectionService::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadProtectionServiceTest,
                           PPAPIDownloadRequest_Timeout);
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadProtectionService::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadProtectionServiceTest,
                           VerifyReferrerChainWithEmptyNavigationHistory);
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadProtectionService::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadProtectionServiceTest,
                           VerifyReferrerChainLengthForExtendedReporting);
~~~

### field error



~~~cpp

static const void* const kDownloadProtectionDataKey;

~~~


###  DownloadProtectionData

 Helper class for easy setting and getting data related to download
 protection. The data is only set when the server returns an unsafe verdict
 (i.e. not safe or unknown).

~~~cpp
class DownloadProtectionData : public base::SupportsUserData::Data {
   public:
    explicit DownloadProtectionData(
        const std::string& token,
        const ClientDownloadResponse::Verdict& verdict,
        const ClientDownloadResponse::TailoredVerdict& tailored_verdict)
        : token_string_(token),
          verdict_(verdict),
          tailored_verdict_(tailored_verdict) {}

    DownloadProtectionData(const DownloadProtectionData&) = delete;
    DownloadProtectionData& operator=(const DownloadProtectionData&) = delete;

    std::string token_string() { return token_string_; }
    ClientDownloadResponse::Verdict verdict() { return verdict_; }
    ClientDownloadResponse::TailoredVerdict tailored_verdict() {
      return tailored_verdict_;
    }

   private:
    std::string token_string_;
    ClientDownloadResponse::Verdict verdict_;
    ClientDownloadResponse::TailoredVerdict tailored_verdict_;
  };
~~~
### CancelPendingRequests

DownloadProtectionService::CancelPendingRequests
~~~cpp
void CancelPendingRequests();
~~~
 Cancels all requests in |download_requests_|, and empties it, releasing
 the references to the requests.

### RequestFinished

DownloadProtectionService::RequestFinished
~~~cpp
void RequestFinished(CheckClientDownloadRequestBase* request,
                       content::BrowserContext* browser_context,
                       DownloadCheckResult result);
~~~
 Called by a CheckClientDownloadRequest instance when it finishes, to
 remove it from |download_requests_| and to report security sensitive
 events to safe_browsing_metrics_collector.

### RequestFinished

DownloadProtectionService::RequestFinished
~~~cpp
virtual void RequestFinished(DeepScanningRequest* request);
~~~
 Called by a DeepScanningRequest when it finishes, to remove it from
 |deep_scanning_requests_|.

### PPAPIDownloadCheckRequestFinished

DownloadProtectionService::PPAPIDownloadCheckRequestFinished
~~~cpp
void PPAPIDownloadCheckRequestFinished(PPAPIDownloadRequest* request);
~~~

### IdentifyReferrerChain

DownloadProtectionService::IdentifyReferrerChain
~~~cpp
std::unique_ptr<ReferrerChainData> IdentifyReferrerChain(
      const download::DownloadItem& item);
~~~
 Identify referrer chain info of a download. This function also records UMA
 stats of download attribution result.

### IdentifyReferrerChain

DownloadProtectionService::IdentifyReferrerChain
~~~cpp
std::unique_ptr<ReferrerChainData> IdentifyReferrerChain(
      const content::FileSystemAccessWriteItem& item);
~~~
 Identify referrer chain info of a File System Access write. This function
 also records UMA stats of download attribution result.

### AddReferrerChainToPPAPIClientDownloadRequest

DownloadProtectionService::AddReferrerChainToPPAPIClientDownloadRequest
~~~cpp
void AddReferrerChainToPPAPIClientDownloadRequest(
      content::WebContents* web_contents,
      const GURL& initiating_frame_url,
      const content::GlobalRenderFrameHostId&
          initiating_outermost_main_frame_id,
      const GURL& initiating_main_frame_url,
      SessionID tab_id,
      bool has_user_gesture,
      ClientDownloadRequest* out_request);
~~~
 Identify referrer chain of the PPAPI download based on the frame URL where
 the download is initiated. Then add referrer chain info to
 ClientDownloadRequest proto. This function also records UMA stats of
 download attribution result.

### OnDangerousDownloadOpened

DownloadProtectionService::OnDangerousDownloadOpened
~~~cpp
void OnDangerousDownloadOpened(const download::DownloadItem* item,
                                 Profile* profile);
~~~

### GetBinaryUploadService

DownloadProtectionService::GetBinaryUploadService
~~~cpp
virtual BinaryUploadService* GetBinaryUploadService(
      Profile* profile,
      const enterprise_connectors::AnalysisSettings& settings);
~~~
 Get the BinaryUploadService for the given |profile|. Virtual so it can be
 overridden in tests.

### GetNavigationObserverManager

DownloadProtectionService::GetNavigationObserverManager
~~~cpp
SafeBrowsingNavigationObserverManager* GetNavigationObserverManager(
      content::WebContents* web_contents);
~~~
 Get the SafeBrowsingNavigationObserverManager for the given |web_contents|.

### MaybeCheckMetdataAfterDeepScanning

DownloadProtectionService::MaybeCheckMetdataAfterDeepScanning
~~~cpp
void MaybeCheckMetdataAfterDeepScanning(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback,
      DownloadCheckResult result);
~~~
 Callback when deep scanning has finished, but we may want to do the
 metadata check anyway.

### sb_service_



~~~cpp

raw_ptr<SafeBrowsingService> sb_service_;

~~~


### ui_manager_



~~~cpp

scoped_refptr<SafeBrowsingUIManager> ui_manager_;

~~~

 These pointers may be NULL if SafeBrowsing is disabled.

### database_manager_



~~~cpp

scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### context_download_requests_



~~~cpp

base::flat_map<
      content::BrowserContext*,
      base::flat_map<CheckClientDownloadRequestBase*,
                     std::unique_ptr<CheckClientDownloadRequestBase>>>
      context_download_requests_;

~~~

 Set of pending server requests for DownloadManager mediated downloads.

### ppapi_download_requests_



~~~cpp

base::flat_map<PPAPIDownloadRequest*, std::unique_ptr<PPAPIDownloadRequest>>
      ppapi_download_requests_;

~~~

 Set of pending server requests for PPAPI mediated downloads.

### deep_scanning_requests_



~~~cpp

base::flat_map<DeepScanningRequest*, std::unique_ptr<DeepScanningRequest>>
      deep_scanning_requests_;

~~~

 Set of pending server requests for deep scanning.

### enabled_



~~~cpp

bool enabled_;

~~~

 Keeps track of the state of the service.

### binary_feature_extractor_



~~~cpp

scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor_;

~~~

 BinaryFeatureExtractor object, may be overridden for testing.

### download_request_timeout_ms_



~~~cpp

int64_t download_request_timeout_ms_;

~~~


### feedback_service_



~~~cpp

std::unique_ptr<DownloadFeedbackService> feedback_service_;

~~~


### client_download_request_callbacks_



~~~cpp

ClientDownloadRequestCallbackList client_download_request_callbacks_;

~~~

 A list of callbacks to be run on the main thread when a
 ClientDownloadRequest has been formed.

### file_system_access_write_request_callbacks_



~~~cpp

FileSystemAccessWriteRequestCallbackList
      file_system_access_write_request_callbacks_;

~~~

 A list of callbacks to be run on the main thread when a
 FileSystemAccessWriteRequest has been formed.

### ppapi_download_request_callbacks_



~~~cpp

PPAPIDownloadRequestCallbackList ppapi_download_request_callbacks_;

~~~

 A list of callbacks to be run on the main thread when a
 PPAPIDownloadRequest has been formed.

### manual_blocklist_hashes_



~~~cpp

std::set<std::string> manual_blocklist_hashes_;

~~~

 List of 8-byte hashes that are blocklisted manually by flag.

 Normally empty.

### allowlist_sample_rate_



~~~cpp

double allowlist_sample_rate_;

~~~

 Rate of allowlisted downloads we sample to send out download ping.

### download_protection_observer_



~~~cpp

DownloadProtectionObserver download_protection_observer_;

~~~

 DownloadProtectionObserver to send real time reports for dangerous download
 events and handle special user actions on the download.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadProtectionService> weak_ptr_factory_;

~~~


### DownloadProtectionService

DownloadProtectionService
~~~cpp
DownloadProtectionService(const DownloadProtectionService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadProtectionService& operator=(const DownloadProtectionService&) =
      delete;
~~~

### enabled

enabled
~~~cpp
bool enabled() const { return enabled_; }
~~~

### allowlist_sample_rate

allowlist_sample_rate
~~~cpp
double allowlist_sample_rate() const { return allowlist_sample_rate_; }
~~~

### ParseManualBlocklistFlag

DownloadProtectionService::ParseManualBlocklistFlag
~~~cpp
virtual void ParseManualBlocklistFlag();
~~~
 Parse a flag of blocklisted sha256 hashes to check at each download.

 This is used for testing, to hunt for safe-browsing by-pass bugs.

### IsHashManuallyBlocklisted

DownloadProtectionService::IsHashManuallyBlocklisted
~~~cpp
virtual bool IsHashManuallyBlocklisted(const std::string& sha256_hash) const;
~~~
 Return true if this hash value is blocklisted via flag (for testing).

### CheckClientDownload

DownloadProtectionService::CheckClientDownload
~~~cpp
virtual void CheckClientDownload(download::DownloadItem* item,
                                   CheckDownloadRepeatingCallback callback);
~~~
 Checks whether the given client download is likely to be malicious or not.

 The result is delivered asynchronously via the given callback.  This
 method must be called on the UI thread, and the callback will also be
 invoked on the UI thread.  This method must be called once the download
 is finished and written to disk.

### MaybeCheckClientDownload

DownloadProtectionService::MaybeCheckClientDownload
~~~cpp
virtual bool MaybeCheckClientDownload(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback);
~~~
 Checks the user permissions, then calls |CheckClientDownload| if
 appropriate. Returns whether we began scanning.

### ShouldCheckDownloadUrl

DownloadProtectionService::ShouldCheckDownloadUrl
~~~cpp
virtual bool ShouldCheckDownloadUrl(download::DownloadItem* item);
~~~
 Returns whether the download URL should be checked for safety based on user
 prefs.

### CheckDownloadUrl

DownloadProtectionService::CheckDownloadUrl
~~~cpp
virtual void CheckDownloadUrl(download::DownloadItem* item,
                                CheckDownloadCallback callback);
~~~
 Checks whether any of the URLs in the redirect chain of the
 download match the SafeBrowsing bad binary URL list.  The result is
 delivered asynchronously via the given callback.  This method must be
 called on the UI thread, and the callback will also be invoked on the UI
 thread.  Pre-condition: !info.download_url_chain.empty().

### IsSupportedDownload

DownloadProtectionService::IsSupportedDownload
~~~cpp
virtual bool IsSupportedDownload(const download::DownloadItem& item,
                                   const base::FilePath& target_path) const;
~~~
 Returns true iff the download specified by |info| should be scanned by
 CheckClientDownload() for malicious content.

### CheckPPAPIDownloadRequest

DownloadProtectionService::CheckPPAPIDownloadRequest
~~~cpp
virtual void CheckPPAPIDownloadRequest(
      const GURL& requestor_url,
      content::RenderFrameHost* initiating_frame,
      const base::FilePath& default_file_path,
      const std::vector<base::FilePath::StringType>& alternate_extensions,
      Profile* profile,
      CheckDownloadCallback callback);
~~~

### CheckFileSystemAccessWrite

DownloadProtectionService::CheckFileSystemAccessWrite
~~~cpp
virtual void CheckFileSystemAccessWrite(
      std::unique_ptr<content::FileSystemAccessWriteItem> item,
      CheckDownloadCallback callback);
~~~
 Checks whether the given File System Access write operation is likely to be
 malicious or not. The result is delivered asynchronously via the given
 callback.  This method must be called on the UI thread, and the callback
 will also be invoked on the UI thread.  This method must be called once the
 write is finished and data has been written to disk.

### ShowDetailsForDownload

DownloadProtectionService::ShowDetailsForDownload
~~~cpp
void ShowDetailsForDownload(const download::DownloadItem* item,
                              content::PageNavigator* navigator);
~~~
 Display more information to the user regarding the download specified by
 |info|. This method is invoked when the user requests more information
 about a download that was marked as malicious.

### SetEnabled

DownloadProtectionService::SetEnabled
~~~cpp
void SetEnabled(bool enabled);
~~~
 Enables or disables the service.  This is usually called by the
 SafeBrowsingService, which tracks whether any profile uses these services
 at all.  Disabling causes any pending and future requests to have their
 callbacks called with "UNKNOWN" results.

### GetDownloadRequestTimeout

DownloadProtectionService::GetDownloadRequestTimeout
~~~cpp
base::TimeDelta GetDownloadRequestTimeout() const;
~~~
 Returns the timeout that is used by CheckClientDownload().

### MaybeBeginFeedbackForDownload

DownloadProtectionService::MaybeBeginFeedbackForDownload
~~~cpp
bool MaybeBeginFeedbackForDownload(
      Profile* profile,
      download::DownloadItem* download,
      DownloadCommands::Command download_command);
~~~
 Checks the user permissions, and submits the downloaded file if
 appropriate. Returns whether the submission was successful.

### RegisterClientDownloadRequestCallback

DownloadProtectionService::RegisterClientDownloadRequestCallback
~~~cpp
base::CallbackListSubscription RegisterClientDownloadRequestCallback(
      const ClientDownloadRequestCallback& callback);
~~~
 Registers a callback that will be run when a ClientDownloadRequest has
 been formed.

### RegisterFileSystemAccessWriteRequestCallback

DownloadProtectionService::RegisterFileSystemAccessWriteRequestCallback
~~~cpp
base::CallbackListSubscription RegisterFileSystemAccessWriteRequestCallback(
      const FileSystemAccessWriteRequestCallback& callback);
~~~
 Registers a callback that will be run when a FileSystemAccessWriteRequest
 has been formed.

### RegisterPPAPIDownloadRequestCallback

DownloadProtectionService::RegisterPPAPIDownloadRequestCallback
~~~cpp
base::CallbackListSubscription RegisterPPAPIDownloadRequestCallback(
      const PPAPIDownloadRequestCallback& callback);
~~~
 Registers a callback that will be run when a PPAPI ClientDownloadRequest
 has been formed.

### SetDownloadProtectionData

DownloadProtectionService::SetDownloadProtectionData
~~~cpp
static void SetDownloadProtectionData(
      download::DownloadItem* item,
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict);
~~~

### GetDownloadPingToken

DownloadProtectionService::GetDownloadPingToken
~~~cpp
static std::string GetDownloadPingToken(const download::DownloadItem* item);
~~~

### GetDownloadProtectionVerdict

DownloadProtectionService::GetDownloadProtectionVerdict
~~~cpp
static ClientDownloadResponse::Verdict GetDownloadProtectionVerdict(
      const download::DownloadItem* item);
~~~

### GetDownloadProtectionTailoredVerdict

DownloadProtectionService::GetDownloadProtectionTailoredVerdict
~~~cpp
static ClientDownloadResponse::TailoredVerdict
  GetDownloadProtectionTailoredVerdict(const download::DownloadItem* item);
~~~

### MaybeSendDangerousDownloadOpenedReport

DownloadProtectionService::MaybeSendDangerousDownloadOpenedReport
~~~cpp
void MaybeSendDangerousDownloadOpenedReport(download::DownloadItem* item,
                                              bool show_download_in_folder);
~~~
 Sends dangerous download opened report when download is opened or
 shown in folder, and if the following conditions are met:
 (1) it is a dangerous download.

 (2) user is NOT in incognito mode.

 (3) user is opted-in for extended reporting.

### ReportDelayedBypassEvent

DownloadProtectionService::ReportDelayedBypassEvent
~~~cpp
void ReportDelayedBypassEvent(download::DownloadItem* download,
                                download::DownloadDangerType danger_type);
~~~
 Called to trigger a bypass event report for |download|. This is used when
 the async scan verdict is received for a file that was already opened by
 the user while it was being processed, and the verdict ended up being
 "dangerous" or "sensitive".

### UploadForDeepScanning

DownloadProtectionService::UploadForDeepScanning
~~~cpp
void UploadForDeepScanning(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback,
      DeepScanningRequest::DeepScanTrigger trigger,
      DownloadCheckResult download_check_result,
      enterprise_connectors::AnalysisSettings analysis_settings);
~~~
 Uploads `item` to Safe Browsing for deep scanning, using the upload
 service attached to the profile `item` was downloaded in. This is
 non-blocking, and the result we be provided through `callback`. `trigger`
 is used to identify the reason for deep scanning, aka enterprise policy or
 APP. `download_check_result` indicates the previously known SB verdict to
 apply to the download should deep scanning fail. `analysis_settings`
 contains settings to apply throughout scanning (types of scans to do,
 whether to block/allow large files, etc). This must be called on the UI
 thread.

### UploadSavePackageForDeepScanning

DownloadProtectionService::UploadSavePackageForDeepScanning
~~~cpp
void UploadSavePackageForDeepScanning(
      download::DownloadItem* item,
      base::flat_map<base::FilePath, base::FilePath> save_package_files,
      CheckDownloadRepeatingCallback callback,
      enterprise_connectors::AnalysisSettings analysis_settings);
~~~
 Uploads a save package `item` for deep scanning. `save_package_file`
 contains a mapping of on-disk files part of that save package to their
 final paths.

### GetDeepScanningRequests

DownloadProtectionService::GetDeepScanningRequests
~~~cpp
std::vector<DeepScanningRequest*> GetDeepScanningRequests();
~~~
 Returns all the currently active deep scanning requests.

### GetURLLoaderFactory

DownloadProtectionService::GetURLLoaderFactory
~~~cpp
virtual scoped_refptr<network::SharedURLLoaderFactory> GetURLLoaderFactory(
      content::BrowserContext* browser_context);
~~~

### RemovePendingDownloadRequests

DownloadProtectionService::RemovePendingDownloadRequests
~~~cpp
void RemovePendingDownloadRequests(content::BrowserContext* browser_context);
~~~
 Removes all pending download requests that are associated with the
 `browser_context`.

### field error



~~~cpp

static const void* const kDownloadProtectionDataKey;

~~~


###  DownloadProtectionData

 Helper class for easy setting and getting data related to download
 protection. The data is only set when the server returns an unsafe verdict
 (i.e. not safe or unknown).

~~~cpp
class DownloadProtectionData : public base::SupportsUserData::Data {
   public:
    explicit DownloadProtectionData(
        const std::string& token,
        const ClientDownloadResponse::Verdict& verdict,
        const ClientDownloadResponse::TailoredVerdict& tailored_verdict)
        : token_string_(token),
          verdict_(verdict),
          tailored_verdict_(tailored_verdict) {}

    DownloadProtectionData(const DownloadProtectionData&) = delete;
    DownloadProtectionData& operator=(const DownloadProtectionData&) = delete;

    std::string token_string() { return token_string_; }
    ClientDownloadResponse::Verdict verdict() { return verdict_; }
    ClientDownloadResponse::TailoredVerdict tailored_verdict() {
      return tailored_verdict_;
    }

   private:
    std::string token_string_;
    ClientDownloadResponse::Verdict verdict_;
    ClientDownloadResponse::TailoredVerdict tailored_verdict_;
  };
~~~
### CancelPendingRequests

DownloadProtectionService::CancelPendingRequests
~~~cpp
void CancelPendingRequests();
~~~
 Cancels all requests in |download_requests_|, and empties it, releasing
 the references to the requests.

### RequestFinished

DownloadProtectionService::RequestFinished
~~~cpp
void RequestFinished(CheckClientDownloadRequestBase* request,
                       content::BrowserContext* browser_context,
                       DownloadCheckResult result);
~~~
 Called by a CheckClientDownloadRequest instance when it finishes, to
 remove it from |download_requests_| and to report security sensitive
 events to safe_browsing_metrics_collector.

### RequestFinished

DownloadProtectionService::RequestFinished
~~~cpp
virtual void RequestFinished(DeepScanningRequest* request);
~~~
 Called by a DeepScanningRequest when it finishes, to remove it from
 |deep_scanning_requests_|.

### PPAPIDownloadCheckRequestFinished

DownloadProtectionService::PPAPIDownloadCheckRequestFinished
~~~cpp
void PPAPIDownloadCheckRequestFinished(PPAPIDownloadRequest* request);
~~~

### IdentifyReferrerChain

DownloadProtectionService::IdentifyReferrerChain
~~~cpp
std::unique_ptr<ReferrerChainData> IdentifyReferrerChain(
      const download::DownloadItem& item);
~~~
 Identify referrer chain info of a download. This function also records UMA
 stats of download attribution result.

### IdentifyReferrerChain

DownloadProtectionService::IdentifyReferrerChain
~~~cpp
std::unique_ptr<ReferrerChainData> IdentifyReferrerChain(
      const content::FileSystemAccessWriteItem& item);
~~~
 Identify referrer chain info of a File System Access write. This function
 also records UMA stats of download attribution result.

### AddReferrerChainToPPAPIClientDownloadRequest

DownloadProtectionService::AddReferrerChainToPPAPIClientDownloadRequest
~~~cpp
void AddReferrerChainToPPAPIClientDownloadRequest(
      content::WebContents* web_contents,
      const GURL& initiating_frame_url,
      const content::GlobalRenderFrameHostId&
          initiating_outermost_main_frame_id,
      const GURL& initiating_main_frame_url,
      SessionID tab_id,
      bool has_user_gesture,
      ClientDownloadRequest* out_request);
~~~
 Identify referrer chain of the PPAPI download based on the frame URL where
 the download is initiated. Then add referrer chain info to
 ClientDownloadRequest proto. This function also records UMA stats of
 download attribution result.

### OnDangerousDownloadOpened

DownloadProtectionService::OnDangerousDownloadOpened
~~~cpp
void OnDangerousDownloadOpened(const download::DownloadItem* item,
                                 Profile* profile);
~~~

### GetBinaryUploadService

DownloadProtectionService::GetBinaryUploadService
~~~cpp
virtual BinaryUploadService* GetBinaryUploadService(
      Profile* profile,
      const enterprise_connectors::AnalysisSettings& settings);
~~~
 Get the BinaryUploadService for the given |profile|. Virtual so it can be
 overridden in tests.

### GetNavigationObserverManager

DownloadProtectionService::GetNavigationObserverManager
~~~cpp
SafeBrowsingNavigationObserverManager* GetNavigationObserverManager(
      content::WebContents* web_contents);
~~~
 Get the SafeBrowsingNavigationObserverManager for the given |web_contents|.

### MaybeCheckMetdataAfterDeepScanning

DownloadProtectionService::MaybeCheckMetdataAfterDeepScanning
~~~cpp
void MaybeCheckMetdataAfterDeepScanning(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback,
      DownloadCheckResult result);
~~~
 Callback when deep scanning has finished, but we may want to do the
 metadata check anyway.

### sb_service_



~~~cpp

raw_ptr<SafeBrowsingService> sb_service_;

~~~


### ui_manager_



~~~cpp

scoped_refptr<SafeBrowsingUIManager> ui_manager_;

~~~

 These pointers may be NULL if SafeBrowsing is disabled.

### database_manager_



~~~cpp

scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### context_download_requests_



~~~cpp

base::flat_map<
      content::BrowserContext*,
      base::flat_map<CheckClientDownloadRequestBase*,
                     std::unique_ptr<CheckClientDownloadRequestBase>>>
      context_download_requests_;

~~~

 Set of pending server requests for DownloadManager mediated downloads.

### ppapi_download_requests_



~~~cpp

base::flat_map<PPAPIDownloadRequest*, std::unique_ptr<PPAPIDownloadRequest>>
      ppapi_download_requests_;

~~~

 Set of pending server requests for PPAPI mediated downloads.

### deep_scanning_requests_



~~~cpp

base::flat_map<DeepScanningRequest*, std::unique_ptr<DeepScanningRequest>>
      deep_scanning_requests_;

~~~

 Set of pending server requests for deep scanning.

### enabled_



~~~cpp

bool enabled_;

~~~

 Keeps track of the state of the service.

### binary_feature_extractor_



~~~cpp

scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor_;

~~~

 BinaryFeatureExtractor object, may be overridden for testing.

### download_request_timeout_ms_



~~~cpp

int64_t download_request_timeout_ms_;

~~~


### feedback_service_



~~~cpp

std::unique_ptr<DownloadFeedbackService> feedback_service_;

~~~


### client_download_request_callbacks_



~~~cpp

ClientDownloadRequestCallbackList client_download_request_callbacks_;

~~~

 A list of callbacks to be run on the main thread when a
 ClientDownloadRequest has been formed.

### file_system_access_write_request_callbacks_



~~~cpp

FileSystemAccessWriteRequestCallbackList
      file_system_access_write_request_callbacks_;

~~~

 A list of callbacks to be run on the main thread when a
 FileSystemAccessWriteRequest has been formed.

### ppapi_download_request_callbacks_



~~~cpp

PPAPIDownloadRequestCallbackList ppapi_download_request_callbacks_;

~~~

 A list of callbacks to be run on the main thread when a
 PPAPIDownloadRequest has been formed.

### manual_blocklist_hashes_



~~~cpp

std::set<std::string> manual_blocklist_hashes_;

~~~

 List of 8-byte hashes that are blocklisted manually by flag.

 Normally empty.

### allowlist_sample_rate_



~~~cpp

double allowlist_sample_rate_;

~~~

 Rate of allowlisted downloads we sample to send out download ping.

### download_protection_observer_



~~~cpp

DownloadProtectionObserver download_protection_observer_;

~~~

 DownloadProtectionObserver to send real time reports for dangerous download
 events and handle special user actions on the download.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadProtectionService> weak_ptr_factory_;

~~~

