
## class FileAnalyzer
 This class does the file content analysis for a user download, extracting the
 features that will be sent to the SB backend. This class lives on the UI
 thread, which is where the result callback will be invoked.

### enum class

~~~cpp
enum class ArchiveValid { UNSET, VALID, INVALID }
~~~
###  Results

 This struct holds the possible features extracted from a file.

~~~cpp
struct Results {
    Results();
    Results(const Results& other);
    ~Results();

    // When analyzing a ZIP or RAR, the type becomes clarified by content
    // inspection (does it contain binaries/archives?). So we return a type.
    ClientDownloadRequest::DownloadType type;

    // For archive files, whether the archive contains an executable. Has
    // unspecified contents for non-archive files.
    bool archived_executable = false;

    // For archive files, whether the archive contains an archive. Has
    // unspecified contents for non-archive files.
    bool archived_archive = false;

    // For archive files, the features extracted from each contained
    // archive/binary.
    google::protobuf::RepeatedPtrField<ClientDownloadRequest::ArchivedBinary>
        archived_binaries;

    // For executables, information about the signature of the executable.
    ClientDownloadRequest::SignatureInfo signature_info;

    // For executables, information about the file headers.
    ClientDownloadRequest::ImageHeaders image_headers;

#if BUILDFLAG(IS_MAC)
    // For DMG files, the signature of the DMG.
    std::vector<uint8_t> disk_image_signature;

    // For DMG files, any detached code signatures in the DMG.
    google::protobuf::RepeatedPtrField<
        ClientDownloadRequest::DetachedCodeSignature>
        detached_code_signatures;
#endif

    // For office documents, the features and metadata extracted from the file.
    ClientDownloadRequest::DocumentSummary document_summary;

    // For archives, the features and metadata extracted from the file.
    ClientDownloadRequest::ArchiveSummary archive_summary;
  };
~~~
### FileAnalyzer

FileAnalyzer::FileAnalyzer
~~~cpp
explicit FileAnalyzer(
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor);
~~~

### ~FileAnalyzer

FileAnalyzer::~FileAnalyzer
~~~cpp
~FileAnalyzer();
~~~

### Start

FileAnalyzer::Start
~~~cpp
void Start(const base::FilePath& target_path,
             const base::FilePath& tmp_path,
             base::OnceCallback<void(Results)> callback);
~~~

### StartExtractFileFeatures

FileAnalyzer::StartExtractFileFeatures
~~~cpp
void StartExtractFileFeatures();
~~~

### OnFileAnalysisFinished

FileAnalyzer::OnFileAnalysisFinished
~~~cpp
void OnFileAnalysisFinished(FileAnalyzer::Results results);
~~~

### StartExtractZipFeatures

FileAnalyzer::StartExtractZipFeatures
~~~cpp
void StartExtractZipFeatures();
~~~

### OnZipAnalysisFinished

FileAnalyzer::OnZipAnalysisFinished
~~~cpp
void OnZipAnalysisFinished(const ArchiveAnalyzerResults& archive_results);
~~~

### StartExtractRarFeatures

FileAnalyzer::StartExtractRarFeatures
~~~cpp
void StartExtractRarFeatures();
~~~

### OnRarAnalysisFinished

FileAnalyzer::OnRarAnalysisFinished
~~~cpp
void OnRarAnalysisFinished(const ArchiveAnalyzerResults& archive_results);
~~~

### StartExtractDmgFeatures

FileAnalyzer::StartExtractDmgFeatures
~~~cpp
void StartExtractDmgFeatures();
~~~

### ExtractFileOrDmgFeatures

FileAnalyzer::ExtractFileOrDmgFeatures
~~~cpp
void ExtractFileOrDmgFeatures(bool download_file_has_koly_signature);
~~~

### OnDmgAnalysisFinished

FileAnalyzer::OnDmgAnalysisFinished
~~~cpp
void OnDmgAnalysisFinished(
      const safe_browsing::ArchiveAnalyzerResults& archive_results);
~~~

### StartExtractDocumentFeatures

FileAnalyzer::StartExtractDocumentFeatures
~~~cpp
void StartExtractDocumentFeatures();
~~~

### OnDocumentAnalysisFinished

FileAnalyzer::OnDocumentAnalysisFinished
~~~cpp
void OnDocumentAnalysisFinished(
      const DocumentAnalyzerResults& document_results);
~~~

### StartExtractSevenZipFeatures

FileAnalyzer::StartExtractSevenZipFeatures
~~~cpp
void StartExtractSevenZipFeatures();
~~~

### OnSevenZipAnalysisFinished

FileAnalyzer::OnSevenZipAnalysisFinished
~~~cpp
void OnSevenZipAnalysisFinished(
      const ArchiveAnalyzerResults& archive_results);
~~~

### LogAnalysisDurationWithAndWithoutSuffix

FileAnalyzer::LogAnalysisDurationWithAndWithoutSuffix
~~~cpp
void LogAnalysisDurationWithAndWithoutSuffix(const std::string& suffix);
~~~

### target_path_



~~~cpp

base::FilePath target_path_;

~~~


### tmp_path_



~~~cpp

base::FilePath tmp_path_;

~~~


### binary_feature_extractor_



~~~cpp

scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor_;

~~~


### d


~~~cpp
base::OnceCallback<void(Results)> callback_;
~~~
### start_time_



~~~cpp

base::Time start_time_;

~~~


### results_



~~~cpp

Results results_;

~~~


###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedZipAnalyzer, base::OnTaskRunnerDeleter>
      zip_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedRarAnalyzer, base::OnTaskRunnerDeleter>
      rar_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedDMGAnalyzer, base::OnTaskRunnerDeleter>
      dmg_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedDocumentAnalyzer, base::OnTaskRunnerDeleter>
      document_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
### document_analysis_start_time_



~~~cpp

base::TimeTicks document_analysis_start_time_;

~~~


###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedSevenZipAnalyzer, base::OnTaskRunnerDeleter>
      seven_zip_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
### weakptr_factory_



~~~cpp

base::WeakPtrFactory<FileAnalyzer> weakptr_factory_{this};

~~~


### enum class

~~~cpp
enum class ArchiveValid { UNSET, VALID, INVALID };
~~~
###  Results

 This struct holds the possible features extracted from a file.

~~~cpp
struct Results {
    Results();
    Results(const Results& other);
    ~Results();

    // When analyzing a ZIP or RAR, the type becomes clarified by content
    // inspection (does it contain binaries/archives?). So we return a type.
    ClientDownloadRequest::DownloadType type;

    // For archive files, whether the archive contains an executable. Has
    // unspecified contents for non-archive files.
    bool archived_executable = false;

    // For archive files, whether the archive contains an archive. Has
    // unspecified contents for non-archive files.
    bool archived_archive = false;

    // For archive files, the features extracted from each contained
    // archive/binary.
    google::protobuf::RepeatedPtrField<ClientDownloadRequest::ArchivedBinary>
        archived_binaries;

    // For executables, information about the signature of the executable.
    ClientDownloadRequest::SignatureInfo signature_info;

    // For executables, information about the file headers.
    ClientDownloadRequest::ImageHeaders image_headers;

#if BUILDFLAG(IS_MAC)
    // For DMG files, the signature of the DMG.
    std::vector<uint8_t> disk_image_signature;

    // For DMG files, any detached code signatures in the DMG.
    google::protobuf::RepeatedPtrField<
        ClientDownloadRequest::DetachedCodeSignature>
        detached_code_signatures;
#endif

    // For office documents, the features and metadata extracted from the file.
    ClientDownloadRequest::DocumentSummary document_summary;

    // For archives, the features and metadata extracted from the file.
    ClientDownloadRequest::ArchiveSummary archive_summary;
  };
~~~
### Start

FileAnalyzer::Start
~~~cpp
void Start(const base::FilePath& target_path,
             const base::FilePath& tmp_path,
             base::OnceCallback<void(Results)> callback);
~~~

### StartExtractFileFeatures

FileAnalyzer::StartExtractFileFeatures
~~~cpp
void StartExtractFileFeatures();
~~~

### OnFileAnalysisFinished

FileAnalyzer::OnFileAnalysisFinished
~~~cpp
void OnFileAnalysisFinished(FileAnalyzer::Results results);
~~~

### StartExtractZipFeatures

FileAnalyzer::StartExtractZipFeatures
~~~cpp
void StartExtractZipFeatures();
~~~

### OnZipAnalysisFinished

FileAnalyzer::OnZipAnalysisFinished
~~~cpp
void OnZipAnalysisFinished(const ArchiveAnalyzerResults& archive_results);
~~~

### StartExtractRarFeatures

FileAnalyzer::StartExtractRarFeatures
~~~cpp
void StartExtractRarFeatures();
~~~

### OnRarAnalysisFinished

FileAnalyzer::OnRarAnalysisFinished
~~~cpp
void OnRarAnalysisFinished(const ArchiveAnalyzerResults& archive_results);
~~~

### StartExtractSevenZipFeatures

FileAnalyzer::StartExtractSevenZipFeatures
~~~cpp
void StartExtractSevenZipFeatures();
~~~

### OnSevenZipAnalysisFinished

FileAnalyzer::OnSevenZipAnalysisFinished
~~~cpp
void OnSevenZipAnalysisFinished(
      const ArchiveAnalyzerResults& archive_results);
~~~

### LogAnalysisDurationWithAndWithoutSuffix

FileAnalyzer::LogAnalysisDurationWithAndWithoutSuffix
~~~cpp
void LogAnalysisDurationWithAndWithoutSuffix(const std::string& suffix);
~~~

### target_path_



~~~cpp

base::FilePath target_path_;

~~~


### tmp_path_



~~~cpp

base::FilePath tmp_path_;

~~~


### binary_feature_extractor_



~~~cpp

scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor_;

~~~


### d


~~~cpp
base::OnceCallback<void(Results)> callback_;
~~~
### start_time_



~~~cpp

base::Time start_time_;

~~~


### results_



~~~cpp

Results results_;

~~~


###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedZipAnalyzer, base::OnTaskRunnerDeleter>
      zip_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedRarAnalyzer, base::OnTaskRunnerDeleter>
      rar_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
###  base::OnTaskRunnerDeleter


~~~cpp
std::unique_ptr<SandboxedSevenZipAnalyzer, base::OnTaskRunnerDeleter>
      seven_zip_analyzer_{nullptr, base::OnTaskRunnerDeleter(nullptr)};
~~~
### weakptr_factory_



~~~cpp

base::WeakPtrFactory<FileAnalyzer> weakptr_factory_{this};

~~~

