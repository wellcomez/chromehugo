
## class CheckClientDownloadRequest

### CheckClientDownloadRequest

CheckClientDownloadRequest::CheckClientDownloadRequest
~~~cpp
CheckClientDownloadRequest(
      download::DownloadItem* item,
      CheckDownloadRepeatingCallback callback,
      DownloadProtectionService* service,
      scoped_refptr<SafeBrowsingDatabaseManager> database_manager,
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor);
~~~

### CheckClientDownloadRequest

CheckClientDownloadRequest
~~~cpp
CheckClientDownloadRequest(const CheckClientDownloadRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
CheckClientDownloadRequest& operator=(const CheckClientDownloadRequest&) =
      delete;
~~~

### ~CheckClientDownloadRequest

CheckClientDownloadRequest::~CheckClientDownloadRequest
~~~cpp
~CheckClientDownloadRequest() override;
~~~

### OnDownloadDestroyed

CheckClientDownloadRequest::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer:
### OnDownloadUpdated

CheckClientDownloadRequest::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~

### IsSupportedDownload

CheckClientDownloadRequest::IsSupportedDownload
~~~cpp
static bool IsSupportedDownload(const download::DownloadItem& item,
                                  const base::FilePath& target_path,
                                  DownloadCheckResultReason* reason);
~~~

### IsSupportedDownload

CheckClientDownloadRequest::IsSupportedDownload
~~~cpp
bool IsSupportedDownload(DownloadCheckResultReason* reason) override;
~~~
 CheckClientDownloadRequestBase overrides:
### GetBrowserContext

CheckClientDownloadRequest::GetBrowserContext
~~~cpp
content::BrowserContext* GetBrowserContext() const override;
~~~

### IsCancelled

CheckClientDownloadRequest::IsCancelled
~~~cpp
bool IsCancelled() override;
~~~

### GetWeakPtr

CheckClientDownloadRequest::GetWeakPtr
~~~cpp
base::WeakPtr<CheckClientDownloadRequestBase> GetWeakPtr() override;
~~~

### NotifySendRequest

CheckClientDownloadRequest::NotifySendRequest
~~~cpp
void NotifySendRequest(const ClientDownloadRequest* request) override;
~~~

### SetDownloadProtectionData

CheckClientDownloadRequest::SetDownloadProtectionData
~~~cpp
void SetDownloadProtectionData(
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict) override;
~~~

### MaybeStorePingsForDownload

CheckClientDownloadRequest::MaybeStorePingsForDownload
~~~cpp
void MaybeStorePingsForDownload(DownloadCheckResult result,
                                  bool upload_requested,
                                  const std::string& request_data,
                                  const std::string& response_body) override;
~~~

### ShouldUploadBinary

CheckClientDownloadRequest::ShouldUploadBinary
~~~cpp
absl::optional<enterprise_connectors::AnalysisSettings> ShouldUploadBinary(
      DownloadCheckResultReason reason) override;
~~~
 Uploads the binary for deep scanning if the reason and policies indicate
 it should be. ShouldUploadBinary will returns the settings to apply for
 deep scanning if it should occur, or absl::nullopt if no scan should be
 done.

### UploadBinary

CheckClientDownloadRequest::UploadBinary
~~~cpp
void UploadBinary(DownloadCheckResult result,
                    DownloadCheckResultReason reason,
                    enterprise_connectors::AnalysisSettings settings) override;
~~~

### NotifyRequestFinished

CheckClientDownloadRequest::NotifyRequestFinished
~~~cpp
void NotifyRequestFinished(DownloadCheckResult result,
                             DownloadCheckResultReason reason) override;
~~~
 Called when this request is completed.

### ShouldPromptForDeepScanning

CheckClientDownloadRequest::ShouldPromptForDeepScanning
~~~cpp
bool ShouldPromptForDeepScanning(bool server_requests_prompt) const override;
~~~
 Called when finishing the download, to decide whether to prompt the user
 for deep scanning or not.

### IsAllowlistedByPolicy

CheckClientDownloadRequest::IsAllowlistedByPolicy
~~~cpp
bool IsAllowlistedByPolicy() const override;
~~~

### IsUnderAdvancedProtection

CheckClientDownloadRequest::IsUnderAdvancedProtection
~~~cpp
bool IsUnderAdvancedProtection(Profile* profile) const;
~~~

### item_



~~~cpp

raw_ptr<download::DownloadItem> item_;

~~~

 The DownloadItem we are checking. Will be NULL if the request has been
 canceled. Must be accessed only on UI thread.

### callback_



~~~cpp

CheckDownloadRepeatingCallback callback_;

~~~


### upload_start_time_



~~~cpp

base::TimeTicks upload_start_time_;

~~~

 Upload start time used for UMA duration histograms.

### weakptr_factory_



~~~cpp

base::WeakPtrFactory<CheckClientDownloadRequest> weakptr_factory_{this};

~~~


### CheckClientDownloadRequest

CheckClientDownloadRequest
~~~cpp
CheckClientDownloadRequest(const CheckClientDownloadRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
CheckClientDownloadRequest& operator=(const CheckClientDownloadRequest&) =
      delete;
~~~

### OnDownloadDestroyed

CheckClientDownloadRequest::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer:
### OnDownloadUpdated

CheckClientDownloadRequest::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~

### IsSupportedDownload

CheckClientDownloadRequest::IsSupportedDownload
~~~cpp
static bool IsSupportedDownload(const download::DownloadItem& item,
                                  const base::FilePath& target_path,
                                  DownloadCheckResultReason* reason);
~~~

### IsSupportedDownload

CheckClientDownloadRequest::IsSupportedDownload
~~~cpp
bool IsSupportedDownload(DownloadCheckResultReason* reason) override;
~~~
 CheckClientDownloadRequestBase overrides:
### GetBrowserContext

CheckClientDownloadRequest::GetBrowserContext
~~~cpp
content::BrowserContext* GetBrowserContext() const override;
~~~

### IsCancelled

CheckClientDownloadRequest::IsCancelled
~~~cpp
bool IsCancelled() override;
~~~

### GetWeakPtr

CheckClientDownloadRequest::GetWeakPtr
~~~cpp
base::WeakPtr<CheckClientDownloadRequestBase> GetWeakPtr() override;
~~~

### NotifySendRequest

CheckClientDownloadRequest::NotifySendRequest
~~~cpp
void NotifySendRequest(const ClientDownloadRequest* request) override;
~~~

### SetDownloadProtectionData

CheckClientDownloadRequest::SetDownloadProtectionData
~~~cpp
void SetDownloadProtectionData(
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict) override;
~~~

### MaybeStorePingsForDownload

CheckClientDownloadRequest::MaybeStorePingsForDownload
~~~cpp
void MaybeStorePingsForDownload(DownloadCheckResult result,
                                  bool upload_requested,
                                  const std::string& request_data,
                                  const std::string& response_body) override;
~~~

### ShouldUploadBinary

CheckClientDownloadRequest::ShouldUploadBinary
~~~cpp
absl::optional<enterprise_connectors::AnalysisSettings> ShouldUploadBinary(
      DownloadCheckResultReason reason) override;
~~~
 Uploads the binary for deep scanning if the reason and policies indicate
 it should be. ShouldUploadBinary will returns the settings to apply for
 deep scanning if it should occur, or absl::nullopt if no scan should be
 done.

### UploadBinary

CheckClientDownloadRequest::UploadBinary
~~~cpp
void UploadBinary(DownloadCheckResult result,
                    DownloadCheckResultReason reason,
                    enterprise_connectors::AnalysisSettings settings) override;
~~~

### NotifyRequestFinished

CheckClientDownloadRequest::NotifyRequestFinished
~~~cpp
void NotifyRequestFinished(DownloadCheckResult result,
                             DownloadCheckResultReason reason) override;
~~~
 Called when this request is completed.

### ShouldPromptForDeepScanning

CheckClientDownloadRequest::ShouldPromptForDeepScanning
~~~cpp
bool ShouldPromptForDeepScanning(bool server_requests_prompt) const override;
~~~
 Called when finishing the download, to decide whether to prompt the user
 for deep scanning or not.

### IsAllowlistedByPolicy

CheckClientDownloadRequest::IsAllowlistedByPolicy
~~~cpp
bool IsAllowlistedByPolicy() const override;
~~~

### IsUnderAdvancedProtection

CheckClientDownloadRequest::IsUnderAdvancedProtection
~~~cpp
bool IsUnderAdvancedProtection(Profile* profile) const;
~~~

### item_



~~~cpp

raw_ptr<download::DownloadItem> item_;

~~~

 The DownloadItem we are checking. Will be NULL if the request has been
 canceled. Must be accessed only on UI thread.

### callback_



~~~cpp

CheckDownloadRepeatingCallback callback_;

~~~


### upload_start_time_



~~~cpp

base::TimeTicks upload_start_time_;

~~~

 Upload start time used for UMA duration histograms.

### weakptr_factory_



~~~cpp

base::WeakPtrFactory<CheckClientDownloadRequest> weakptr_factory_{this};

~~~

