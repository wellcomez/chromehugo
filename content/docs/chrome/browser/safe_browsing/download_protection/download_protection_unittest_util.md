### ReadTestCertificate

ReadTestCertificate
~~~cpp
scoped_refptr<net::X509Certificate> ReadTestCertificate(
    base::FilePath test_cert_path,
    const std::string& filename);
~~~
 namespace net
