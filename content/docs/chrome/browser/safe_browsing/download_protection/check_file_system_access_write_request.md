
## class CheckFileSystemAccessWriteRequest

### CheckFileSystemAccessWriteRequest

CheckFileSystemAccessWriteRequest::CheckFileSystemAccessWriteRequest
~~~cpp
CheckFileSystemAccessWriteRequest(
      std::unique_ptr<content::FileSystemAccessWriteItem> item,
      CheckDownloadCallback callback,
      DownloadProtectionService* service,
      scoped_refptr<SafeBrowsingDatabaseManager> database_manager,
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor);
~~~

### CheckFileSystemAccessWriteRequest

CheckFileSystemAccessWriteRequest
~~~cpp
CheckFileSystemAccessWriteRequest(const CheckFileSystemAccessWriteRequest&) =
      delete;
~~~

### operator=

operator=
~~~cpp
CheckFileSystemAccessWriteRequest& operator=(
      const CheckFileSystemAccessWriteRequest&) = delete;
~~~

### ~CheckFileSystemAccessWriteRequest

CheckFileSystemAccessWriteRequest::~CheckFileSystemAccessWriteRequest
~~~cpp
~CheckFileSystemAccessWriteRequest() override;
~~~

### IsSupportedDownload

CheckFileSystemAccessWriteRequest::IsSupportedDownload
~~~cpp
bool IsSupportedDownload(DownloadCheckResultReason* reason) override;
~~~
 CheckClientDownloadRequestBase overrides:
### GetBrowserContext

CheckFileSystemAccessWriteRequest::GetBrowserContext
~~~cpp
content::BrowserContext* GetBrowserContext() const override;
~~~

### IsCancelled

CheckFileSystemAccessWriteRequest::IsCancelled
~~~cpp
bool IsCancelled() override;
~~~

### GetWeakPtr

CheckFileSystemAccessWriteRequest::GetWeakPtr
~~~cpp
base::WeakPtr<CheckClientDownloadRequestBase> GetWeakPtr() override;
~~~

### NotifySendRequest

CheckFileSystemAccessWriteRequest::NotifySendRequest
~~~cpp
void NotifySendRequest(const ClientDownloadRequest* request) override;
~~~

### SetDownloadProtectionData

CheckFileSystemAccessWriteRequest::SetDownloadProtectionData
~~~cpp
void SetDownloadProtectionData(
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict) override;
~~~

### MaybeStorePingsForDownload

CheckFileSystemAccessWriteRequest::MaybeStorePingsForDownload
~~~cpp
void MaybeStorePingsForDownload(DownloadCheckResult result,
                                  bool upload_requested,
                                  const std::string& request_data,
                                  const std::string& response_body) override;
~~~

### ShouldUploadBinary

CheckFileSystemAccessWriteRequest::ShouldUploadBinary
~~~cpp
absl::optional<enterprise_connectors::AnalysisSettings> ShouldUploadBinary(
      DownloadCheckResultReason reason) override;
~~~

### UploadBinary

CheckFileSystemAccessWriteRequest::UploadBinary
~~~cpp
void UploadBinary(DownloadCheckResult result,
                    DownloadCheckResultReason reason,
                    enterprise_connectors::AnalysisSettings settings) override;
~~~

### ShouldPromptForDeepScanning

CheckFileSystemAccessWriteRequest::ShouldPromptForDeepScanning
~~~cpp
bool ShouldPromptForDeepScanning(bool server_requests_prompt) const override;
~~~

### NotifyRequestFinished

CheckFileSystemAccessWriteRequest::NotifyRequestFinished
~~~cpp
void NotifyRequestFinished(DownloadCheckResult result,
                             DownloadCheckResultReason reason) override;
~~~

### IsAllowlistedByPolicy

CheckFileSystemAccessWriteRequest::IsAllowlistedByPolicy
~~~cpp
bool IsAllowlistedByPolicy() const override;
~~~

### item_



~~~cpp

const std::unique_ptr<content::FileSystemAccessWriteItem> item_;

~~~


### referrer_chain_data_



~~~cpp

std::unique_ptr<ReferrerChainData> referrer_chain_data_;

~~~


### weakptr_factory_



~~~cpp

base::WeakPtrFactory<CheckFileSystemAccessWriteRequest> weakptr_factory_{
      this};

~~~


### CheckFileSystemAccessWriteRequest

CheckFileSystemAccessWriteRequest
~~~cpp
CheckFileSystemAccessWriteRequest(const CheckFileSystemAccessWriteRequest&) =
      delete;
~~~

### operator=

operator=
~~~cpp
CheckFileSystemAccessWriteRequest& operator=(
      const CheckFileSystemAccessWriteRequest&) = delete;
~~~

### IsSupportedDownload

CheckFileSystemAccessWriteRequest::IsSupportedDownload
~~~cpp
bool IsSupportedDownload(DownloadCheckResultReason* reason) override;
~~~
 CheckClientDownloadRequestBase overrides:
### GetBrowserContext

CheckFileSystemAccessWriteRequest::GetBrowserContext
~~~cpp
content::BrowserContext* GetBrowserContext() const override;
~~~

### IsCancelled

CheckFileSystemAccessWriteRequest::IsCancelled
~~~cpp
bool IsCancelled() override;
~~~

### GetWeakPtr

CheckFileSystemAccessWriteRequest::GetWeakPtr
~~~cpp
base::WeakPtr<CheckClientDownloadRequestBase> GetWeakPtr() override;
~~~

### NotifySendRequest

CheckFileSystemAccessWriteRequest::NotifySendRequest
~~~cpp
void NotifySendRequest(const ClientDownloadRequest* request) override;
~~~

### SetDownloadProtectionData

CheckFileSystemAccessWriteRequest::SetDownloadProtectionData
~~~cpp
void SetDownloadProtectionData(
      const std::string& token,
      const ClientDownloadResponse::Verdict& verdict,
      const ClientDownloadResponse::TailoredVerdict& tailored_verdict) override;
~~~

### MaybeStorePingsForDownload

CheckFileSystemAccessWriteRequest::MaybeStorePingsForDownload
~~~cpp
void MaybeStorePingsForDownload(DownloadCheckResult result,
                                  bool upload_requested,
                                  const std::string& request_data,
                                  const std::string& response_body) override;
~~~

### ShouldUploadBinary

CheckFileSystemAccessWriteRequest::ShouldUploadBinary
~~~cpp
absl::optional<enterprise_connectors::AnalysisSettings> ShouldUploadBinary(
      DownloadCheckResultReason reason) override;
~~~

### UploadBinary

CheckFileSystemAccessWriteRequest::UploadBinary
~~~cpp
void UploadBinary(DownloadCheckResult result,
                    DownloadCheckResultReason reason,
                    enterprise_connectors::AnalysisSettings settings) override;
~~~

### ShouldPromptForDeepScanning

CheckFileSystemAccessWriteRequest::ShouldPromptForDeepScanning
~~~cpp
bool ShouldPromptForDeepScanning(bool server_requests_prompt) const override;
~~~

### NotifyRequestFinished

CheckFileSystemAccessWriteRequest::NotifyRequestFinished
~~~cpp
void NotifyRequestFinished(DownloadCheckResult result,
                             DownloadCheckResultReason reason) override;
~~~

### IsAllowlistedByPolicy

CheckFileSystemAccessWriteRequest::IsAllowlistedByPolicy
~~~cpp
bool IsAllowlistedByPolicy() const override;
~~~

### item_



~~~cpp

const std::unique_ptr<content::FileSystemAccessWriteItem> item_;

~~~


### referrer_chain_data_



~~~cpp

std::unique_ptr<ReferrerChainData> referrer_chain_data_;

~~~


### weakptr_factory_



~~~cpp

base::WeakPtrFactory<CheckFileSystemAccessWriteRequest> weakptr_factory_{
      this};

~~~

