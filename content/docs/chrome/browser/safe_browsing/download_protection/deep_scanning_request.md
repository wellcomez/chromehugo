
## class DeepScanningRequest
 This class encapsulates the process of uploading a file to Safe Browsing for
 deep scanning and reporting the result.

### enum class

~~~cpp
enum class DeepScanTrigger {
    // The trigger is unknown.
    TRIGGER_UNKNOWN = 0,

    // The trigger is the prompt in the download shelf, shown for Advanced
    // Protection users.
    TRIGGER_APP_PROMPT = 1,

    // The trigger is the enterprise policy.
    TRIGGER_POLICY = 2,

    kMaxValue = TRIGGER_POLICY,
  }
~~~
### enum class

~~~cpp
enum class DeepScanType {
    // Scanning was initiated by a normal download from a web page.
    NORMAL = 0,

    // Scanning was initiated by a save package being saved on disk.
    SAVE_PACKAGE = 1,

    kMaxValue = SAVE_PACKAGE,
  }
~~~
###  ~Observer


~~~cpp
class Observer : public base::CheckedObserver {
   public:
    ~Observer() override = default;

    // Called when the DeepScanningRequest chooses to display a modal dialog.
    virtual void OnModalShown(DeepScanningRequest* request) {}

    // Called when the DeepScanningRequest finishes.
    virtual void OnFinish(DeepScanningRequest* request) {}
  };
~~~
### ShouldUploadBinary

DeepScanningRequest::ShouldUploadBinary
~~~cpp
static absl::optional<enterprise_connectors::AnalysisSettings>
  ShouldUploadBinary(download::DownloadItem* item);
~~~
 Checks the current policies to determine whether files must be uploaded by
 policy. Returns the settings to apply to this analysis if it should happen
 or absl::nullopt if no analysis should happen.

### DeepScanningRequest

DeepScanningRequest::DeepScanningRequest
~~~cpp
DeepScanningRequest(download::DownloadItem* item,
                      DeepScanTrigger trigger,
                      DownloadCheckResult pre_scan_download_check_result,
                      CheckDownloadRepeatingCallback callback,
                      DownloadProtectionService* download_service,
                      enterprise_connectors::AnalysisSettings settings);
~~~
 Scan the given `item`, with the given `trigger`. The result of the scanning
 will be provided through `callback`. Take a references to the owning
 `download_service`.

### DeepScanningRequest

DeepScanningRequest::DeepScanningRequest
~~~cpp
DeepScanningRequest(
      download::DownloadItem* item,
      DownloadCheckResult pre_scan_download_check_result,
      CheckDownloadRepeatingCallback callback,
      DownloadProtectionService* download_service,
      enterprise_connectors::AnalysisSettings settings,
      base::flat_map<base::FilePath, base::FilePath> save_package_files);
~~~
 Scan the given `item` that corresponds to a save package, with
 `save_package_page` mapping every currently on-disk file part of that
 package to their final target path. The result of the scanning is provided
 through `callback` once every file has been scanned, and the given result
 is the highest severity one. Takes a reference to the owning
 `download_service`.

### ~DeepScanningRequest

DeepScanningRequest::~DeepScanningRequest
~~~cpp
~DeepScanningRequest() override;
~~~

### Start

DeepScanningRequest::Start
~~~cpp
void Start();
~~~
 Begin the deep scanning request. This must be called on the UI thread.

### AddObserver

DeepScanningRequest::AddObserver
~~~cpp
void AddObserver(Observer* observer);
~~~

### RemoveObserver

DeepScanningRequest::RemoveObserver
~~~cpp
void RemoveObserver(Observer* observer);
~~~

### OnDownloadUpdated

DeepScanningRequest::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer:
### OnDownloadDestroyed

DeepScanningRequest::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~

### StartSingleFileScan

DeepScanningRequest::StartSingleFileScan
~~~cpp
void StartSingleFileScan();
~~~
 Starts the deep scanning request when there is a one-to-one mapping from
 the download item to a file.

### StartSavePackageScan

DeepScanningRequest::StartSavePackageScan
~~~cpp
void StartSavePackageScan();
~~~
 Starts the deep scanning requests when there is a one-to-many mapping from
 the download item to multiple files being scanned as a part of a save
 package.

### OnDownloadRequestReady

DeepScanningRequest::OnDownloadRequestReady
~~~cpp
void OnDownloadRequestReady(
      const base::FilePath& current_path,
      std::unique_ptr<FileAnalysisRequest> deep_scan_request,
      std::unique_ptr<ClientDownloadRequest> download_request);
~~~
 Callback when the |download_request_maker_| is finished assembling the
 download metadata request.

### OnScanComplete

DeepScanningRequest::OnScanComplete
~~~cpp
void OnScanComplete(const base::FilePath& current_path,
                      BinaryUploadService::Result result,
                      enterprise_connectors::ContentAnalysisResponse response);
~~~
 Callbacks for when |binary_upload_service_| finishes uploading.

### MaybeFinishRequest

DeepScanningRequest::MaybeFinishRequest
~~~cpp
void MaybeFinishRequest(DownloadCheckResult result);
~~~
 Called when a single file scanning request has completed. Calls
 FinishRequest if it was the last required one.

### FinishRequest

DeepScanningRequest::FinishRequest
~~~cpp
void FinishRequest(DownloadCheckResult result);
~~~
 Finishes the request, providing the result through |callback_| and
 notifying |download_service_|.

### MaybeShowDeepScanFailureModalDialog

DeepScanningRequest::MaybeShowDeepScanFailureModalDialog
~~~cpp
bool MaybeShowDeepScanFailureModalDialog(base::OnceClosure accept_callback,
                                           base::OnceClosure cancel_callback,
                                           base::OnceClosure close_callback,
                                           base::OnceClosure open_now_callback);
~~~
 Called to attempt to show the modal dialog for scan failure. Returns
 whether the dialog was successfully shown.

### OpenDownload

DeepScanningRequest::OpenDownload
~~~cpp
void OpenDownload();
~~~
 Called to open the download. This is triggered by the timeout modal dialog.

### PopulateRequest

DeepScanningRequest::PopulateRequest
~~~cpp
void PopulateRequest(FileAnalysisRequest* request,
                       Profile* profile,
                       const base::FilePath& path);
~~~
 Populates a request's proto fields with the appropriate data.

### PrepareClientDownloadRequest

DeepScanningRequest::PrepareClientDownloadRequest
~~~cpp
void PrepareClientDownloadRequest(
      const base::FilePath& current_path,
      std::unique_ptr<FileAnalysisRequest> deep_scan_request);
~~~
 Creates a ClientDownloadRequest asynchronously to attach to
 `deep_scan_request`. Once it is obtained, OnDownloadRequestReady is called
 to upload the request for deep scanning.

### OnGotRequestData

DeepScanningRequest::OnGotRequestData
~~~cpp
void OnGotRequestData(const base::FilePath& final_path,
                        const base::FilePath& current_path,
                        std::unique_ptr<FileAnalysisRequest> request,
                        BinaryUploadService::Result result,
                        BinaryUploadService::Request::Data data);
~~~
 Wrapper around OnDownloadRequestReady to facilitate opening files in
 parallel for save package scans.

### ReportOnlyScan

DeepScanningRequest::ReportOnlyScan
~~~cpp
bool ReportOnlyScan();
~~~
 Helper function to simplify checking if the report-only feature is set in
 conjunction with the corresponding policy value.

### AcknowledgeRequest

DeepScanningRequest::AcknowledgeRequest
~~~cpp
void AcknowledgeRequest(EventResult event_result);
~~~
 Acknowledge the request's handling to the service provider.

### item_



~~~cpp

raw_ptr<download::DownloadItem> item_;

~~~

 The download item to scan. This is unowned, and could become nullptr if the
 download is destroyed.

### trigger_



~~~cpp

DeepScanTrigger trigger_;

~~~

 The reason for deep scanning.

### callback_



~~~cpp

CheckDownloadRepeatingCallback callback_;

~~~

 The callback to provide the scan result to.

### download_service_



~~~cpp

raw_ptr<DownloadProtectionService> download_service_;

~~~

 The download protection service that initiated this upload. The
 |download_service_| owns this class.

### upload_start_times_



~~~cpp

base::flat_map<base::FilePath, base::TimeTicks> upload_start_times_;

~~~

 The time when uploading starts. Keyed with the file's current path.

### analysis_settings_



~~~cpp

enterprise_connectors::AnalysisSettings analysis_settings_;

~~~

 The settings to apply to this scan.

### download_request_maker_



~~~cpp

std::unique_ptr<DownloadRequestMaker> download_request_maker_;

~~~

 Used to assemble the download metadata.

### observers_



~~~cpp

base::ObserverList<Observer> observers_;

~~~

 This list of observers of this request.

### save_package_files_



~~~cpp

base::flat_map<base::FilePath, base::FilePath> save_package_files_;

~~~

 Stores a mapping of temporary paths to final paths for save package files.

 This is empty on non-page save scanning requests.

### file_metadata_



~~~cpp

base::flat_map<base::FilePath, enterprise_connectors::FileMetadata>
      file_metadata_;

~~~

 Stores a mapping of a file's current path to its metadata so it can be used
 in reporting events. This is populated from opening the file for save
 package scans, or populated from `item_` for single file scans.

### file_opening_job_



~~~cpp

std::unique_ptr<FileOpeningJob> file_opening_job_;

~~~

 Owner of the FileOpeningJob used to safely open multiple files in parallel
 for save package scans. Always nullptr for non-save package scans.

### pending_scan_requests_



~~~cpp

size_t pending_scan_requests_;

~~~

 The total number of files beings scanned for which OnScanComplete hasn't
 been called. Once this is 0, FinishRequest should be called and `this`
 should be destroyed.

### download_check_result_



~~~cpp

DownloadCheckResult download_check_result_ =
      DownloadCheckResult::DEEP_SCANNED_SAFE;

~~~

 The highest precedence DownloadCheckResult obtained from scanning verdicts.

 This should be updated when a scan completes.

### pre_scan_download_check_result_



~~~cpp

DownloadCheckResult pre_scan_download_check_result_;

~~~

 Cached SB result for the download to be used if deep scanning fails.

### pre_scan_danger_type_



~~~cpp

download::DownloadDangerType pre_scan_danger_type_;

~~~

 Cached danger type for the download to be used by reporting in case
 scanning is skipped for any reason.

### scanning_started_



~~~cpp

bool scanning_started_ = false;

~~~

 Set to true when StartSingleFileScan or StartSavePackageScan is called and
 that scanning has started. This is used so that calls to OnDownloadUpdated
 only ever start the scanning process once.

### d

 Cached callbacks to report scanning results until the final `event_result_`
 is known. The callbacks in this list should be called in FinishRequest.

~~~cpp
base::OnceCallbackList<void(EventResult result)> report_callbacks_;
~~~
### request_tokens_



~~~cpp

std::vector<std::string> request_tokens_;

~~~

 The request tokens of all the requests that make up the user action
 represented by this ContentAnalysisDelegate instance.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DeepScanningRequest> weak_ptr_factory_;

~~~


### enum class
 Enum representing the trigger of the scan request.

 These values are persisted to logs. Entries should not be renumbered and
 numeric values should never be reused.

~~~cpp
enum class DeepScanTrigger {
    // The trigger is unknown.
    TRIGGER_UNKNOWN = 0,

    // The trigger is the prompt in the download shelf, shown for Advanced
    // Protection users.
    TRIGGER_APP_PROMPT = 1,

    // The trigger is the enterprise policy.
    TRIGGER_POLICY = 2,

    kMaxValue = TRIGGER_POLICY,
  };
~~~
### enum class
 Enum representing the type of constructor that initiated scanning.

 These values are persisted to logs. Entries should not be renumbered and
 numeric values should never be reused.

~~~cpp
enum class DeepScanType {
    // Scanning was initiated by a normal download from a web page.
    NORMAL = 0,

    // Scanning was initiated by a save package being saved on disk.
    SAVE_PACKAGE = 1,

    kMaxValue = SAVE_PACKAGE,
  };
~~~
###  ~Observer


~~~cpp
class Observer : public base::CheckedObserver {
   public:
    ~Observer() override = default;

    // Called when the DeepScanningRequest chooses to display a modal dialog.
    virtual void OnModalShown(DeepScanningRequest* request) {}

    // Called when the DeepScanningRequest finishes.
    virtual void OnFinish(DeepScanningRequest* request) {}
  };
~~~
### ShouldUploadBinary

DeepScanningRequest::ShouldUploadBinary
~~~cpp
static absl::optional<enterprise_connectors::AnalysisSettings>
  ShouldUploadBinary(download::DownloadItem* item);
~~~
 Checks the current policies to determine whether files must be uploaded by
 policy. Returns the settings to apply to this analysis if it should happen
 or absl::nullopt if no analysis should happen.

### Start

DeepScanningRequest::Start
~~~cpp
void Start();
~~~
 Begin the deep scanning request. This must be called on the UI thread.

### AddObserver

DeepScanningRequest::AddObserver
~~~cpp
void AddObserver(Observer* observer);
~~~

### RemoveObserver

DeepScanningRequest::RemoveObserver
~~~cpp
void RemoveObserver(Observer* observer);
~~~

### OnDownloadUpdated

DeepScanningRequest::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer:
### OnDownloadDestroyed

DeepScanningRequest::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~

### StartSingleFileScan

DeepScanningRequest::StartSingleFileScan
~~~cpp
void StartSingleFileScan();
~~~
 Starts the deep scanning request when there is a one-to-one mapping from
 the download item to a file.

### StartSavePackageScan

DeepScanningRequest::StartSavePackageScan
~~~cpp
void StartSavePackageScan();
~~~
 Starts the deep scanning requests when there is a one-to-many mapping from
 the download item to multiple files being scanned as a part of a save
 package.

### OnDownloadRequestReady

DeepScanningRequest::OnDownloadRequestReady
~~~cpp
void OnDownloadRequestReady(
      const base::FilePath& current_path,
      std::unique_ptr<FileAnalysisRequest> deep_scan_request,
      std::unique_ptr<ClientDownloadRequest> download_request);
~~~
 Callback when the |download_request_maker_| is finished assembling the
 download metadata request.

### OnScanComplete

DeepScanningRequest::OnScanComplete
~~~cpp
void OnScanComplete(const base::FilePath& current_path,
                      BinaryUploadService::Result result,
                      enterprise_connectors::ContentAnalysisResponse response);
~~~
 Callbacks for when |binary_upload_service_| finishes uploading.

### MaybeFinishRequest

DeepScanningRequest::MaybeFinishRequest
~~~cpp
void MaybeFinishRequest(DownloadCheckResult result);
~~~
 Called when a single file scanning request has completed. Calls
 FinishRequest if it was the last required one.

### FinishRequest

DeepScanningRequest::FinishRequest
~~~cpp
void FinishRequest(DownloadCheckResult result);
~~~
 Finishes the request, providing the result through |callback_| and
 notifying |download_service_|.

### MaybeShowDeepScanFailureModalDialog

DeepScanningRequest::MaybeShowDeepScanFailureModalDialog
~~~cpp
bool MaybeShowDeepScanFailureModalDialog(base::OnceClosure accept_callback,
                                           base::OnceClosure cancel_callback,
                                           base::OnceClosure close_callback,
                                           base::OnceClosure open_now_callback);
~~~
 Called to attempt to show the modal dialog for scan failure. Returns
 whether the dialog was successfully shown.

### OpenDownload

DeepScanningRequest::OpenDownload
~~~cpp
void OpenDownload();
~~~
 Called to open the download. This is triggered by the timeout modal dialog.

### PopulateRequest

DeepScanningRequest::PopulateRequest
~~~cpp
void PopulateRequest(FileAnalysisRequest* request,
                       Profile* profile,
                       const base::FilePath& path);
~~~
 Populates a request's proto fields with the appropriate data.

### PrepareClientDownloadRequest

DeepScanningRequest::PrepareClientDownloadRequest
~~~cpp
void PrepareClientDownloadRequest(
      const base::FilePath& current_path,
      std::unique_ptr<FileAnalysisRequest> deep_scan_request);
~~~
 Creates a ClientDownloadRequest asynchronously to attach to
 `deep_scan_request`. Once it is obtained, OnDownloadRequestReady is called
 to upload the request for deep scanning.

### OnGotRequestData

DeepScanningRequest::OnGotRequestData
~~~cpp
void OnGotRequestData(const base::FilePath& final_path,
                        const base::FilePath& current_path,
                        std::unique_ptr<FileAnalysisRequest> request,
                        BinaryUploadService::Result result,
                        BinaryUploadService::Request::Data data);
~~~
 Wrapper around OnDownloadRequestReady to facilitate opening files in
 parallel for save package scans.

### ReportOnlyScan

DeepScanningRequest::ReportOnlyScan
~~~cpp
bool ReportOnlyScan();
~~~
 Helper function to simplify checking if the report-only feature is set in
 conjunction with the corresponding policy value.

### AcknowledgeRequest

DeepScanningRequest::AcknowledgeRequest
~~~cpp
void AcknowledgeRequest(EventResult event_result);
~~~
 Acknowledge the request's handling to the service provider.

### item_



~~~cpp

raw_ptr<download::DownloadItem> item_;

~~~

 The download item to scan. This is unowned, and could become nullptr if the
 download is destroyed.

### trigger_



~~~cpp

DeepScanTrigger trigger_;

~~~

 The reason for deep scanning.

### callback_



~~~cpp

CheckDownloadRepeatingCallback callback_;

~~~

 The callback to provide the scan result to.

### download_service_



~~~cpp

raw_ptr<DownloadProtectionService> download_service_;

~~~

 The download protection service that initiated this upload. The
 |download_service_| owns this class.

### upload_start_times_



~~~cpp

base::flat_map<base::FilePath, base::TimeTicks> upload_start_times_;

~~~

 The time when uploading starts. Keyed with the file's current path.

### analysis_settings_



~~~cpp

enterprise_connectors::AnalysisSettings analysis_settings_;

~~~

 The settings to apply to this scan.

### download_request_maker_



~~~cpp

std::unique_ptr<DownloadRequestMaker> download_request_maker_;

~~~

 Used to assemble the download metadata.

### observers_



~~~cpp

base::ObserverList<Observer> observers_;

~~~

 This list of observers of this request.

### save_package_files_



~~~cpp

base::flat_map<base::FilePath, base::FilePath> save_package_files_;

~~~

 Stores a mapping of temporary paths to final paths for save package files.

 This is empty on non-page save scanning requests.

### file_metadata_



~~~cpp

base::flat_map<base::FilePath, enterprise_connectors::FileMetadata>
      file_metadata_;

~~~

 Stores a mapping of a file's current path to its metadata so it can be used
 in reporting events. This is populated from opening the file for save
 package scans, or populated from `item_` for single file scans.

### file_opening_job_



~~~cpp

std::unique_ptr<FileOpeningJob> file_opening_job_;

~~~

 Owner of the FileOpeningJob used to safely open multiple files in parallel
 for save package scans. Always nullptr for non-save package scans.

### pending_scan_requests_



~~~cpp

size_t pending_scan_requests_;

~~~

 The total number of files beings scanned for which OnScanComplete hasn't
 been called. Once this is 0, FinishRequest should be called and `this`
 should be destroyed.

### download_check_result_



~~~cpp

DownloadCheckResult download_check_result_ =
      DownloadCheckResult::DEEP_SCANNED_SAFE;

~~~

 The highest precedence DownloadCheckResult obtained from scanning verdicts.

 This should be updated when a scan completes.

### pre_scan_download_check_result_



~~~cpp

DownloadCheckResult pre_scan_download_check_result_;

~~~

 Cached SB result for the download to be used if deep scanning fails.

### pre_scan_danger_type_



~~~cpp

download::DownloadDangerType pre_scan_danger_type_;

~~~

 Cached danger type for the download to be used by reporting in case
 scanning is skipped for any reason.

### scanning_started_



~~~cpp

bool scanning_started_ = false;

~~~

 Set to true when StartSingleFileScan or StartSavePackageScan is called and
 that scanning has started. This is used so that calls to OnDownloadUpdated
 only ever start the scanning process once.

### d

 Cached callbacks to report scanning results until the final `event_result_`
 is known. The callbacks in this list should be called in FinishRequest.

~~~cpp
base::OnceCallbackList<void(EventResult result)> report_callbacks_;
~~~
### request_tokens_



~~~cpp

std::vector<std::string> request_tokens_;

~~~

 The request tokens of all the requests that make up the user action
 represented by this ContentAnalysisDelegate instance.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DeepScanningRequest> weak_ptr_factory_;

~~~

