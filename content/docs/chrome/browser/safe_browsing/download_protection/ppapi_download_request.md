
## class PPAPIDownloadRequest
 A request for checking whether a PPAPI initiated download is safe.


 These are considered different from DownloadManager mediated downloads
 because:

 * The download bytes are produced by the PPAPI plugin *after* the check
   returns due to architectural constraints.


 * Since the download bytes are produced by the PPAPI plugin, there's no
   reliable network request information to associate with the download.


 PPAPIDownloadRequest objects are owned by the DownloadProtectionService
 indicated by |service|.

### enum class

~~~cpp
enum class RequestOutcome : int {
    UNKNOWN,
    REQUEST_DESTROYED,
    UNSUPPORTED_FILE_TYPE,
    TIMEDOUT,
    ALLOWLIST_HIT,
    REQUEST_MALFORMED,
    FETCH_FAILED,
    RESPONSE_MALFORMED,
    SUCCEEDED
  }
~~~
### PPAPIDownloadRequest

PPAPIDownloadRequest::PPAPIDownloadRequest
~~~cpp
PPAPIDownloadRequest(
      const GURL& requestor_url,
      content::RenderFrameHost* initiating_frame,
      const base::FilePath& default_file_path,
      const std::vector<base::FilePath::StringType>& alternate_extensions,
      Profile* profile,
      CheckDownloadCallback callback,
      DownloadProtectionService* service,
      scoped_refptr<SafeBrowsingDatabaseManager> database_manager);
~~~

### PPAPIDownloadRequest

PPAPIDownloadRequest
~~~cpp
PPAPIDownloadRequest(const PPAPIDownloadRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
PPAPIDownloadRequest& operator=(const PPAPIDownloadRequest&) = delete;
~~~

### ~PPAPIDownloadRequest

PPAPIDownloadRequest::~PPAPIDownloadRequest
~~~cpp
~PPAPIDownloadRequest() override;
~~~

### Start

PPAPIDownloadRequest::Start
~~~cpp
void Start();
~~~
 Start the process of checking the download request. The callback passed as
 the |callback| parameter to the constructor will be invoked with the result
 of the check at some point in the future.


 From the this point on, the code is arranged to follow the most common
 workflow.


 Note that |this| should be added to the list of pending requests in the
 associated DownloadProtectionService object *before* calling Start().

 Otherwise a synchronous Finish() call may result in leaking the
 PPAPIDownloadRequest object. This is enforced via a DCHECK in
 DownloadProtectionService.

### GetDownloadRequestUrl

PPAPIDownloadRequest::GetDownloadRequestUrl
~~~cpp
static GURL GetDownloadRequestUrl();
~~~
 Returns the URL that will be used for download requests.

### WebContentsDestroyed

PPAPIDownloadRequest::WebContentsDestroyed
~~~cpp
void WebContentsDestroyed() override;
~~~
 WebContentsObserver implementation
### field error



~~~cpp

static const char kDownloadRequestUrl[];

~~~


### CheckAllowlistsOnSBThread

PPAPIDownloadRequest::CheckAllowlistsOnSBThread
~~~cpp
static void CheckAllowlistsOnSBThread(
      const GURL& requestor_url,
      scoped_refptr<SafeBrowsingDatabaseManager> database_manager,
      base::WeakPtr<PPAPIDownloadRequest> download_request);
~~~
 Allowlist checking needs to the done on the SB thread.

### AllowlistCheckComplete

PPAPIDownloadRequest::AllowlistCheckComplete
~~~cpp
void AllowlistCheckComplete(bool was_on_allowlist);
~~~

### SendRequest

PPAPIDownloadRequest::SendRequest
~~~cpp
void SendRequest();
~~~

### OnURLLoaderComplete

PPAPIDownloadRequest::OnURLLoaderComplete
~~~cpp
void OnURLLoaderComplete(std::unique_ptr<std::string> response_body);
~~~

### OnRequestTimedOut

PPAPIDownloadRequest::OnRequestTimedOut
~~~cpp
void OnRequestTimedOut();
~~~

### Finish

PPAPIDownloadRequest::Finish
~~~cpp
void Finish(RequestOutcome reason, DownloadCheckResult response);
~~~

### DownloadCheckResultFromClientDownloadResponse

PPAPIDownloadRequest::DownloadCheckResultFromClientDownloadResponse
~~~cpp
static DownloadCheckResult DownloadCheckResultFromClientDownloadResponse(
      ClientDownloadResponse::Verdict verdict);
~~~

### GetSupportedFilePath

PPAPIDownloadRequest::GetSupportedFilePath
~~~cpp
static base::FilePath GetSupportedFilePath(
      const base::FilePath& default_file_path,
      const std::vector<base::FilePath::StringType>& alternate_extensions);
~~~
 Given a |default_file_path| and a list of |alternate_extensions|,
 constructs a FilePath with each possible extension and returns one that
 satisfies IsCheckedBinaryFile(). If none are supported, returns an
 empty FilePath.

### loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> loader_;

~~~


### client_download_request_data_



~~~cpp

std::string client_download_request_data_;

~~~


### requestor_url_



~~~cpp

const GURL requestor_url_;

~~~

 URL of document that requested the PPAPI download.

### initiating_frame_url_



~~~cpp

const GURL initiating_frame_url_;

~~~

 URL of the frame that hosts the PPAPI plugin.

### initiating_outermost_main_frame_id_



~~~cpp

const content::GlobalRenderFrameHostId initiating_outermost_main_frame_id_;

~~~

 The id of the initiating outermost main frame.

### initiating_main_frame_url_



~~~cpp

const GURL initiating_main_frame_url_;

~~~

 URL of the tab that contains the initialting_frame.

### tab_id_



~~~cpp

SessionID tab_id_;

~~~

 Tab id that associated with the PPAPI plugin, computed by
 sessions::SessionTabHelper::IdForTab().

### has_user_gesture_



~~~cpp

bool has_user_gesture_;

~~~

 If the user interacted with this PPAPI plugin to trigger the download.

### default_file_path_



~~~cpp

const base::FilePath default_file_path_;

~~~

 Default download path requested by the PPAPI plugin.

### alternate_extensions_



~~~cpp

const std::vector<base::FilePath::StringType> alternate_extensions_;

~~~

 List of alternate extensions provided by the PPAPI plugin. Each extension
 must begin with a leading extension separator.

### callback_



~~~cpp

CheckDownloadCallback callback_;

~~~

 Callback to invoke with the result of the PPAPI download request check.

### service_



~~~cpp

raw_ptr<DownloadProtectionService> service_;

~~~


### database_manager_



~~~cpp

const scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### start_time_



~~~cpp

const base::TimeTicks start_time_;

~~~

 Time request was started.

### supported_path_



~~~cpp

const base::FilePath supported_path_;

~~~

 A download path that is supported by SafeBrowsing. This is determined by
 invoking GetSupportedFilePath(). If non-empty,
 IsCheckedBinaryFile(supported_path_) is always true. This
 path is therefore used as the download target when sending the SafeBrowsing
 ping.

### is_extended_reporting_



~~~cpp

bool is_extended_reporting_;

~~~


### is_enhanced_protection_



~~~cpp

bool is_enhanced_protection_;

~~~


### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### weakptr_factory_



~~~cpp

base::WeakPtrFactory<PPAPIDownloadRequest> weakptr_factory_{this};

~~~


### PPAPIDownloadRequest

PPAPIDownloadRequest
~~~cpp
PPAPIDownloadRequest(const PPAPIDownloadRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
PPAPIDownloadRequest& operator=(const PPAPIDownloadRequest&) = delete;
~~~

### enum class
 The outcome of the request. These values are used for UMA. New values
 should only be added at the end.

~~~cpp
enum class RequestOutcome : int {
    UNKNOWN,
    REQUEST_DESTROYED,
    UNSUPPORTED_FILE_TYPE,
    TIMEDOUT,
    ALLOWLIST_HIT,
    REQUEST_MALFORMED,
    FETCH_FAILED,
    RESPONSE_MALFORMED,
    SUCCEEDED
  };
~~~
### Start

PPAPIDownloadRequest::Start
~~~cpp
void Start();
~~~
 Start the process of checking the download request. The callback passed as
 the |callback| parameter to the constructor will be invoked with the result
 of the check at some point in the future.


 From the this point on, the code is arranged to follow the most common
 workflow.


 Note that |this| should be added to the list of pending requests in the
 associated DownloadProtectionService object *before* calling Start().

 Otherwise a synchronous Finish() call may result in leaking the
 PPAPIDownloadRequest object. This is enforced via a DCHECK in
 DownloadProtectionService.

### GetDownloadRequestUrl

PPAPIDownloadRequest::GetDownloadRequestUrl
~~~cpp
static GURL GetDownloadRequestUrl();
~~~
 Returns the URL that will be used for download requests.

### WebContentsDestroyed

PPAPIDownloadRequest::WebContentsDestroyed
~~~cpp
void WebContentsDestroyed() override;
~~~
 WebContentsObserver implementation
### field error



~~~cpp

static const char kDownloadRequestUrl[];

~~~


### CheckAllowlistsOnSBThread

PPAPIDownloadRequest::CheckAllowlistsOnSBThread
~~~cpp
static void CheckAllowlistsOnSBThread(
      const GURL& requestor_url,
      scoped_refptr<SafeBrowsingDatabaseManager> database_manager,
      base::WeakPtr<PPAPIDownloadRequest> download_request);
~~~
 Allowlist checking needs to the done on the SB thread.

### AllowlistCheckComplete

PPAPIDownloadRequest::AllowlistCheckComplete
~~~cpp
void AllowlistCheckComplete(bool was_on_allowlist);
~~~

### SendRequest

PPAPIDownloadRequest::SendRequest
~~~cpp
void SendRequest();
~~~

### OnURLLoaderComplete

PPAPIDownloadRequest::OnURLLoaderComplete
~~~cpp
void OnURLLoaderComplete(std::unique_ptr<std::string> response_body);
~~~

### OnRequestTimedOut

PPAPIDownloadRequest::OnRequestTimedOut
~~~cpp
void OnRequestTimedOut();
~~~

### Finish

PPAPIDownloadRequest::Finish
~~~cpp
void Finish(RequestOutcome reason, DownloadCheckResult response);
~~~

### DownloadCheckResultFromClientDownloadResponse

PPAPIDownloadRequest::DownloadCheckResultFromClientDownloadResponse
~~~cpp
static DownloadCheckResult DownloadCheckResultFromClientDownloadResponse(
      ClientDownloadResponse::Verdict verdict);
~~~

### GetSupportedFilePath

PPAPIDownloadRequest::GetSupportedFilePath
~~~cpp
static base::FilePath GetSupportedFilePath(
      const base::FilePath& default_file_path,
      const std::vector<base::FilePath::StringType>& alternate_extensions);
~~~
 Given a |default_file_path| and a list of |alternate_extensions|,
 constructs a FilePath with each possible extension and returns one that
 satisfies IsCheckedBinaryFile(). If none are supported, returns an
 empty FilePath.

### loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> loader_;

~~~


### client_download_request_data_



~~~cpp

std::string client_download_request_data_;

~~~


### requestor_url_



~~~cpp

const GURL requestor_url_;

~~~

 URL of document that requested the PPAPI download.

### initiating_frame_url_



~~~cpp

const GURL initiating_frame_url_;

~~~

 URL of the frame that hosts the PPAPI plugin.

### initiating_outermost_main_frame_id_



~~~cpp

const content::GlobalRenderFrameHostId initiating_outermost_main_frame_id_;

~~~

 The id of the initiating outermost main frame.

### initiating_main_frame_url_



~~~cpp

const GURL initiating_main_frame_url_;

~~~

 URL of the tab that contains the initialting_frame.

### tab_id_



~~~cpp

SessionID tab_id_;

~~~

 Tab id that associated with the PPAPI plugin, computed by
 sessions::SessionTabHelper::IdForTab().

### has_user_gesture_



~~~cpp

bool has_user_gesture_;

~~~

 If the user interacted with this PPAPI plugin to trigger the download.

### default_file_path_



~~~cpp

const base::FilePath default_file_path_;

~~~

 Default download path requested by the PPAPI plugin.

### alternate_extensions_



~~~cpp

const std::vector<base::FilePath::StringType> alternate_extensions_;

~~~

 List of alternate extensions provided by the PPAPI plugin. Each extension
 must begin with a leading extension separator.

### callback_



~~~cpp

CheckDownloadCallback callback_;

~~~

 Callback to invoke with the result of the PPAPI download request check.

### service_



~~~cpp

raw_ptr<DownloadProtectionService> service_;

~~~


### database_manager_



~~~cpp

const scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### start_time_



~~~cpp

const base::TimeTicks start_time_;

~~~

 Time request was started.

### supported_path_



~~~cpp

const base::FilePath supported_path_;

~~~

 A download path that is supported by SafeBrowsing. This is determined by
 invoking GetSupportedFilePath(). If non-empty,
 IsCheckedBinaryFile(supported_path_) is always true. This
 path is therefore used as the download target when sending the SafeBrowsing
 ping.

### is_extended_reporting_



~~~cpp

bool is_extended_reporting_;

~~~


### is_enhanced_protection_



~~~cpp

bool is_enhanced_protection_;

~~~


### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### weakptr_factory_



~~~cpp

base::WeakPtrFactory<PPAPIDownloadRequest> weakptr_factory_{this};

~~~

