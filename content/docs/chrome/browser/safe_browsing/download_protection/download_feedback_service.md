
## class DownloadFeedbackService
 Tracks active DownloadFeedback objects, provides interface for storing ping
 data for malicious downloads.

 Lives on the UI thread.

### DownloadFeedbackService

DownloadFeedbackService::DownloadFeedbackService
~~~cpp
DownloadFeedbackService(
      DownloadProtectionService* download_protection_service,
      base::TaskRunner* file_task_runner);
~~~

### DownloadFeedbackService

DownloadFeedbackService
~~~cpp
DownloadFeedbackService(const DownloadFeedbackService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadFeedbackService& operator=(const DownloadFeedbackService&) = delete;
~~~

### ~DownloadFeedbackService

DownloadFeedbackService::~DownloadFeedbackService
~~~cpp
~DownloadFeedbackService();
~~~

### MaybeStorePingsForDownload

DownloadFeedbackService::MaybeStorePingsForDownload
~~~cpp
static void MaybeStorePingsForDownload(DownloadCheckResult result,
                                         bool upload_requested,
                                         download::DownloadItem* download,
                                         const std::string& ping,
                                         const std::string& response);
~~~
 Stores the request and response ping data from the download check, if the
 check result and file size are eligible. This must be called after a
 download has been flagged as un-SAFE in order for the download to be
 enabled for uploading. Some un-SAFE downloads can be marked for
 upload by the server with |upload_requested| if it's needed for better
 classification.

### IsEnabledForDownload

DownloadFeedbackService::IsEnabledForDownload
~~~cpp
static bool IsEnabledForDownload(const download::DownloadItem& download);
~~~
 Test if pings have been stored for |download|.

### GetPingsForDownloadForTesting

DownloadFeedbackService::GetPingsForDownloadForTesting
~~~cpp
static bool GetPingsForDownloadForTesting(
      const download::DownloadItem& download,
      std::string* ping,
      std::string* response);
~~~
 Get the ping values stored in |download|. Returns false if no ping values
 are present.

### BeginFeedbackForDownload

DownloadFeedbackService::BeginFeedbackForDownload
~~~cpp
void BeginFeedbackForDownload(Profile* profile,
                                download::DownloadItem* download,
                                DownloadCommands::Command download_command);
~~~
 Begin download feedback for the given |download| in the given |profile|.

 Then delete download file if |download_command| is DISCARD, or run the KEEP
 command otherwise. This must only be called if IsEnabledForDownload is true
 for |download|.

### BeginFeedbackOrDeleteFile

DownloadFeedbackService::BeginFeedbackOrDeleteFile
~~~cpp
static void BeginFeedbackOrDeleteFile(
      const scoped_refptr<base::TaskRunner>& file_task_runner,
      const base::WeakPtr<DownloadFeedbackService>& service,
      Profile* profile,
      const std::string& ping_request,
      const std::string& ping_response,
      const base::FilePath& path);
~~~

### StartPendingFeedback

DownloadFeedbackService::StartPendingFeedback
~~~cpp
void StartPendingFeedback();
~~~

### BeginFeedback

DownloadFeedbackService::BeginFeedback
~~~cpp
void BeginFeedback(Profile* profile,
                     const std::string& ping_request,
                     const std::string& ping_response,
                     const base::FilePath& path);
~~~

### FeedbackComplete

DownloadFeedbackService::FeedbackComplete
~~~cpp
void FeedbackComplete();
~~~

### download_protection_service_



~~~cpp

raw_ptr<DownloadProtectionService> download_protection_service_;

~~~

 Safe because the DownloadProtectionService owns this.

### file_task_runner_



~~~cpp

scoped_refptr<base::TaskRunner> file_task_runner_;

~~~


### active_feedback_



~~~cpp

base::queue<std::unique_ptr<DownloadFeedback>> active_feedback_;

~~~

 Currently active & pending uploads. The first item is active, remaining
 items are pending.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadFeedbackService> weak_ptr_factory_{this};

~~~


### DownloadFeedbackService

DownloadFeedbackService
~~~cpp
DownloadFeedbackService(const DownloadFeedbackService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadFeedbackService& operator=(const DownloadFeedbackService&) = delete;
~~~

### MaybeStorePingsForDownload

DownloadFeedbackService::MaybeStorePingsForDownload
~~~cpp
static void MaybeStorePingsForDownload(DownloadCheckResult result,
                                         bool upload_requested,
                                         download::DownloadItem* download,
                                         const std::string& ping,
                                         const std::string& response);
~~~
 Stores the request and response ping data from the download check, if the
 check result and file size are eligible. This must be called after a
 download has been flagged as un-SAFE in order for the download to be
 enabled for uploading. Some un-SAFE downloads can be marked for
 upload by the server with |upload_requested| if it's needed for better
 classification.

### IsEnabledForDownload

DownloadFeedbackService::IsEnabledForDownload
~~~cpp
static bool IsEnabledForDownload(const download::DownloadItem& download);
~~~
 Test if pings have been stored for |download|.

### GetPingsForDownloadForTesting

DownloadFeedbackService::GetPingsForDownloadForTesting
~~~cpp
static bool GetPingsForDownloadForTesting(
      const download::DownloadItem& download,
      std::string* ping,
      std::string* response);
~~~
 Get the ping values stored in |download|. Returns false if no ping values
 are present.

### BeginFeedbackForDownload

DownloadFeedbackService::BeginFeedbackForDownload
~~~cpp
void BeginFeedbackForDownload(Profile* profile,
                                download::DownloadItem* download,
                                DownloadCommands::Command download_command);
~~~
 Begin download feedback for the given |download| in the given |profile|.

 Then delete download file if |download_command| is DISCARD, or run the KEEP
 command otherwise. This must only be called if IsEnabledForDownload is true
 for |download|.

### BeginFeedbackOrDeleteFile

DownloadFeedbackService::BeginFeedbackOrDeleteFile
~~~cpp
static void BeginFeedbackOrDeleteFile(
      const scoped_refptr<base::TaskRunner>& file_task_runner,
      const base::WeakPtr<DownloadFeedbackService>& service,
      Profile* profile,
      const std::string& ping_request,
      const std::string& ping_response,
      const base::FilePath& path);
~~~

### StartPendingFeedback

DownloadFeedbackService::StartPendingFeedback
~~~cpp
void StartPendingFeedback();
~~~

### BeginFeedback

DownloadFeedbackService::BeginFeedback
~~~cpp
void BeginFeedback(Profile* profile,
                     const std::string& ping_request,
                     const std::string& ping_response,
                     const base::FilePath& path);
~~~

### FeedbackComplete

DownloadFeedbackService::FeedbackComplete
~~~cpp
void FeedbackComplete();
~~~

### download_protection_service_



~~~cpp

raw_ptr<DownloadProtectionService> download_protection_service_;

~~~

 Safe because the DownloadProtectionService owns this.

### file_task_runner_



~~~cpp

scoped_refptr<base::TaskRunner> file_task_runner_;

~~~


### active_feedback_



~~~cpp

base::queue<std::unique_ptr<DownloadFeedback>> active_feedback_;

~~~

 Currently active & pending uploads. The first item is active, remaining
 items are pending.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadFeedbackService> weak_ptr_factory_{this};

~~~

