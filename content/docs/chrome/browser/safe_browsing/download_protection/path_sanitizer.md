
## class PathSanitizer
 A helper class to sanitize any number of file paths by replacing the portion
 that represents the current user's home directory with "~".

### PathSanitizer

PathSanitizer::PathSanitizer
~~~cpp
PathSanitizer();
~~~

### PathSanitizer

PathSanitizer
~~~cpp
PathSanitizer(const PathSanitizer&) = delete;
~~~

### operator=

operator=
~~~cpp
PathSanitizer& operator=(const PathSanitizer&) = delete;
~~~

###  GetHomeDirectory


~~~cpp
const base::FilePath& GetHomeDirectory() const;
~~~
### StripHomeDirectory

PathSanitizer::StripHomeDirectory
~~~cpp
void StripHomeDirectory(base::FilePath* file_path) const;
~~~

### home_path_



~~~cpp

base::FilePath home_path_;

~~~


### PathSanitizer

PathSanitizer
~~~cpp
PathSanitizer(const PathSanitizer&) = delete;
~~~

### operator=

operator=
~~~cpp
PathSanitizer& operator=(const PathSanitizer&) = delete;
~~~

###  GetHomeDirectory


~~~cpp
const base::FilePath& GetHomeDirectory() const;
~~~
### StripHomeDirectory

PathSanitizer::StripHomeDirectory
~~~cpp
void StripHomeDirectory(base::FilePath* file_path) const;
~~~

### home_path_



~~~cpp

base::FilePath home_path_;

~~~

