
## class DownloadRequestMaker
 This class encapsulate the process of populating all the fields in a Safe
 Browsing download ping.

### field error



~~~cpp

struct TabUrls {
    GURL url;
    GURL referrer;
  };

~~~

 URL and referrer of the window the download was started from.

### CreateFromDownloadItem

DownloadRequestMaker::CreateFromDownloadItem
~~~cpp
static std::unique_ptr<DownloadRequestMaker> CreateFromDownloadItem(
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor,
      download::DownloadItem* item);
~~~

### CreateFromFileSystemAccess

DownloadRequestMaker::CreateFromFileSystemAccess
~~~cpp
static std::unique_ptr<DownloadRequestMaker> CreateFromFileSystemAccess(
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor,
      DownloadProtectionService* service,
      const content::FileSystemAccessWriteItem& item);
~~~

### DownloadRequestMaker

DownloadRequestMaker::DownloadRequestMaker
~~~cpp
DownloadRequestMaker(
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor,
      content::BrowserContext* browser_context,
      TabUrls tab_urls,
      base::FilePath target_file_path,
      base::FilePath full_path,
      GURL source_url,
      std::string sha256_hash,
      int64_t length,
      const std::vector<ClientDownloadRequest::Resource>& resources,
      bool is_user_initiated,
      ReferrerChainData* referrer_chain_data);
~~~

### DownloadRequestMaker

DownloadRequestMaker
~~~cpp
DownloadRequestMaker(const DownloadRequestMaker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadRequestMaker& operator=(const DownloadRequestMaker&) = delete;
~~~

### ~DownloadRequestMaker

DownloadRequestMaker::~DownloadRequestMaker
~~~cpp
~DownloadRequestMaker();
~~~

### Start

DownloadRequestMaker::Start
~~~cpp
void Start(Callback callback);
~~~
 Starts filling in fields in the download ping. Will run the callback with
 the fully-populated ping.

### OnFileFeatureExtractionDone

DownloadRequestMaker::OnFileFeatureExtractionDone
~~~cpp
void OnFileFeatureExtractionDone(FileAnalyzer::Results results);
~~~
 Callback when |file_analyzer_| is done analyzing the download.

### GetTabRedirects

DownloadRequestMaker::GetTabRedirects
~~~cpp
void GetTabRedirects();
~~~
 Helper function to get the tab redirects from the history service.

### OnGotTabRedirects

DownloadRequestMaker::OnGotTabRedirects
~~~cpp
void OnGotTabRedirects(history::RedirectList redirect_list);
~~~
 Callback when the history service has retrieved the tab redirects.

### PopulateTailoredInfo

DownloadRequestMaker::PopulateTailoredInfo
~~~cpp
void PopulateTailoredInfo();
~~~
 Populates the tailored info field for tailored warnings.

### browser_context_



~~~cpp

raw_ptr<content::BrowserContext> browser_context_;

~~~


### request_



~~~cpp

std::unique_ptr<ClientDownloadRequest> request_;

~~~


### binary_feature_extractor_



~~~cpp

const scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor_;

~~~


###  std::make_unique<FileAnalyzer>


~~~cpp
const std::unique_ptr<FileAnalyzer> file_analyzer_ =
      std::make_unique<FileAnalyzer>(binary_feature_extractor_);
~~~
### request_tracker_



~~~cpp

base::CancelableTaskTracker request_tracker_;

~~~


### tab_urls_



~~~cpp

TabUrls tab_urls_;

~~~

 For HistoryService lookup.

 The current URL for the WebContents that initiated the download, and its
 referrer.

### target_file_path_



~~~cpp

const base::FilePath target_file_path_;

~~~

 The ultimate destination for the download.

### full_path_



~~~cpp

const base::FilePath full_path_;

~~~

 The current path to the file contents.

### callback_



~~~cpp

Callback callback_;

~~~


### start_time_



~~~cpp

base::Time start_time_;

~~~

 Start time of a given asynchronous task. Used for metrics.

### weakptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadRequestMaker> weakptr_factory_{this};

~~~


### DownloadRequestMaker

DownloadRequestMaker
~~~cpp
DownloadRequestMaker(const DownloadRequestMaker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadRequestMaker& operator=(const DownloadRequestMaker&) = delete;
~~~

### field error



~~~cpp

struct TabUrls {
    GURL url;
    GURL referrer;
  };

~~~

 URL and referrer of the window the download was started from.

### CreateFromDownloadItem

DownloadRequestMaker::CreateFromDownloadItem
~~~cpp
static std::unique_ptr<DownloadRequestMaker> CreateFromDownloadItem(
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor,
      download::DownloadItem* item);
~~~

### CreateFromFileSystemAccess

DownloadRequestMaker::CreateFromFileSystemAccess
~~~cpp
static std::unique_ptr<DownloadRequestMaker> CreateFromFileSystemAccess(
      scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor,
      DownloadProtectionService* service,
      const content::FileSystemAccessWriteItem& item);
~~~

### Start

DownloadRequestMaker::Start
~~~cpp
void Start(Callback callback);
~~~
 Starts filling in fields in the download ping. Will run the callback with
 the fully-populated ping.

### OnFileFeatureExtractionDone

DownloadRequestMaker::OnFileFeatureExtractionDone
~~~cpp
void OnFileFeatureExtractionDone(FileAnalyzer::Results results);
~~~
 Callback when |file_analyzer_| is done analyzing the download.

### GetTabRedirects

DownloadRequestMaker::GetTabRedirects
~~~cpp
void GetTabRedirects();
~~~
 Helper function to get the tab redirects from the history service.

### OnGotTabRedirects

DownloadRequestMaker::OnGotTabRedirects
~~~cpp
void OnGotTabRedirects(history::RedirectList redirect_list);
~~~
 Callback when the history service has retrieved the tab redirects.

### PopulateTailoredInfo

DownloadRequestMaker::PopulateTailoredInfo
~~~cpp
void PopulateTailoredInfo();
~~~
 Populates the tailored info field for tailored warnings.

### browser_context_



~~~cpp

raw_ptr<content::BrowserContext> browser_context_;

~~~


### request_



~~~cpp

std::unique_ptr<ClientDownloadRequest> request_;

~~~


### binary_feature_extractor_



~~~cpp

const scoped_refptr<BinaryFeatureExtractor> binary_feature_extractor_;

~~~


###  std::make_unique<FileAnalyzer>


~~~cpp
const std::unique_ptr<FileAnalyzer> file_analyzer_ =
      std::make_unique<FileAnalyzer>(binary_feature_extractor_);
~~~
### request_tracker_



~~~cpp

base::CancelableTaskTracker request_tracker_;

~~~


### tab_urls_



~~~cpp

TabUrls tab_urls_;

~~~

 For HistoryService lookup.

 The current URL for the WebContents that initiated the download, and its
 referrer.

### target_file_path_



~~~cpp

const base::FilePath target_file_path_;

~~~

 The ultimate destination for the download.

### full_path_



~~~cpp

const base::FilePath full_path_;

~~~

 The current path to the file contents.

### callback_



~~~cpp

Callback callback_;

~~~


### start_time_



~~~cpp

base::Time start_time_;

~~~

 Start time of a given asynchronous task. Used for metrics.

### weakptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadRequestMaker> weakptr_factory_{this};

~~~

