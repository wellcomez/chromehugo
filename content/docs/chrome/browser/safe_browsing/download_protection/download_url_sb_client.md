
## class DownloadUrlSBClient
 SafeBrowsing::Client class used to lookup the bad binary URL list.

### DownloadUrlSBClient

DownloadUrlSBClient::DownloadUrlSBClient
~~~cpp
DownloadUrlSBClient(
      download::DownloadItem* item,
      DownloadProtectionService* service,
      CheckDownloadCallback callback,
      const scoped_refptr<SafeBrowsingUIManager>& ui_manager,
      const scoped_refptr<SafeBrowsingDatabaseManager>& database_manager);
~~~

### DownloadUrlSBClient

DownloadUrlSBClient
~~~cpp
DownloadUrlSBClient(const DownloadUrlSBClient&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadUrlSBClient& operator=(const DownloadUrlSBClient&) = delete;
~~~

### OnDownloadDestroyed

DownloadUrlSBClient::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 Implements DownloadItem::Observer.

### StartCheck

DownloadUrlSBClient::StartCheck
~~~cpp
void StartCheck();
~~~

### IsDangerous

DownloadUrlSBClient::IsDangerous
~~~cpp
bool IsDangerous(SBThreatType threat_type) const;
~~~

### OnCheckDownloadUrlResult

DownloadUrlSBClient::OnCheckDownloadUrlResult
~~~cpp
void OnCheckDownloadUrlResult(const std::vector<GURL>& url_chain,
                                SBThreatType threat_type) override;
~~~
 Implements SafeBrowsingDatabaseManager::Client.

### ~DownloadUrlSBClient

DownloadUrlSBClient::~DownloadUrlSBClient
~~~cpp
~DownloadUrlSBClient() override;
~~~

### CheckDone

DownloadUrlSBClient::CheckDone
~~~cpp
void CheckDone(SBThreatType threat_type);
~~~

### ReportMalware

DownloadUrlSBClient::ReportMalware
~~~cpp
void ReportMalware(SBThreatType threat_type);
~~~

### IdentifyReferrerChain

DownloadUrlSBClient::IdentifyReferrerChain
~~~cpp
void IdentifyReferrerChain();
~~~

### UpdateDownloadCheckStats

DownloadUrlSBClient::UpdateDownloadCheckStats
~~~cpp
void UpdateDownloadCheckStats(SBStatsType stat_type);
~~~

### item_



~~~cpp

raw_ptr<download::DownloadItem> item_;

~~~

 The DownloadItem we are checking. Must be accessed only on UI thread.

### sha256_hash_



~~~cpp

std::string sha256_hash_;

~~~

 Copies of data from |item_| for access on other threads.

### url_chain_



~~~cpp

std::vector<GURL> url_chain_;

~~~


### referrer_url_



~~~cpp

GURL referrer_url_;

~~~


### total_type_



~~~cpp

const SBStatsType total_type_;

~~~

 Enumeration types for histogram purposes.

### dangerous_type_



~~~cpp

const SBStatsType dangerous_type_;

~~~


### service_



~~~cpp

raw_ptr<DownloadProtectionService, DanglingUntriaged> service_;

~~~


### callback_



~~~cpp

CheckDownloadCallback callback_;

~~~


### ui_manager_



~~~cpp

scoped_refptr<SafeBrowsingUIManager> ui_manager_;

~~~


### start_time_



~~~cpp

base::TimeTicks start_time_;

~~~


### extended_reporting_level_



~~~cpp

ExtendedReportingLevel extended_reporting_level_;

~~~


### is_enhanced_protection_



~~~cpp

bool is_enhanced_protection_;

~~~


### database_manager_



~~~cpp

scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### download_item_observation_



~~~cpp

base::ScopedObservation<download::DownloadItem,
                          download::DownloadItem::Observer>
      download_item_observation_{this};

~~~


### DownloadUrlSBClient

DownloadUrlSBClient
~~~cpp
DownloadUrlSBClient(const DownloadUrlSBClient&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadUrlSBClient& operator=(const DownloadUrlSBClient&) = delete;
~~~

### OnDownloadDestroyed

DownloadUrlSBClient::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 Implements DownloadItem::Observer.

### StartCheck

DownloadUrlSBClient::StartCheck
~~~cpp
void StartCheck();
~~~

### IsDangerous

DownloadUrlSBClient::IsDangerous
~~~cpp
bool IsDangerous(SBThreatType threat_type) const;
~~~

### OnCheckDownloadUrlResult

DownloadUrlSBClient::OnCheckDownloadUrlResult
~~~cpp
void OnCheckDownloadUrlResult(const std::vector<GURL>& url_chain,
                                SBThreatType threat_type) override;
~~~
 Implements SafeBrowsingDatabaseManager::Client.

### CheckDone

DownloadUrlSBClient::CheckDone
~~~cpp
void CheckDone(SBThreatType threat_type);
~~~

### ReportMalware

DownloadUrlSBClient::ReportMalware
~~~cpp
void ReportMalware(SBThreatType threat_type);
~~~

### IdentifyReferrerChain

DownloadUrlSBClient::IdentifyReferrerChain
~~~cpp
void IdentifyReferrerChain();
~~~

### UpdateDownloadCheckStats

DownloadUrlSBClient::UpdateDownloadCheckStats
~~~cpp
void UpdateDownloadCheckStats(SBStatsType stat_type);
~~~

### item_



~~~cpp

raw_ptr<download::DownloadItem> item_;

~~~

 The DownloadItem we are checking. Must be accessed only on UI thread.

### sha256_hash_



~~~cpp

std::string sha256_hash_;

~~~

 Copies of data from |item_| for access on other threads.

### url_chain_



~~~cpp

std::vector<GURL> url_chain_;

~~~


### referrer_url_



~~~cpp

GURL referrer_url_;

~~~


### total_type_



~~~cpp

const SBStatsType total_type_;

~~~

 Enumeration types for histogram purposes.

### dangerous_type_



~~~cpp

const SBStatsType dangerous_type_;

~~~


### service_



~~~cpp

raw_ptr<DownloadProtectionService, DanglingUntriaged> service_;

~~~


### callback_



~~~cpp

CheckDownloadCallback callback_;

~~~


### ui_manager_



~~~cpp

scoped_refptr<SafeBrowsingUIManager> ui_manager_;

~~~


### start_time_



~~~cpp

base::TimeTicks start_time_;

~~~


### extended_reporting_level_



~~~cpp

ExtendedReportingLevel extended_reporting_level_;

~~~


### is_enhanced_protection_



~~~cpp

bool is_enhanced_protection_;

~~~


### database_manager_



~~~cpp

scoped_refptr<SafeBrowsingDatabaseManager> database_manager_;

~~~


### download_item_observation_



~~~cpp

base::ScopedObservation<download::DownloadItem,
                          download::DownloadItem::Observer>
      download_item_observation_{this};

~~~

