
## class DownloadFeedback
 Handles the uploading of a single downloaded binary to the safebrowsing
 download feedback service.

### Create

DownloadFeedback::Create
~~~cpp
static std::unique_ptr<DownloadFeedback> Create(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const base::FilePath& file_path,
      const std::string& ping_request,
      const std::string& ping_response);
~~~
 Takes ownership of the file pointed to be |file_path|, it will be deleted
 when the DownloadFeedback is destructed.

### kMaxUploadSize



~~~cpp

static const int64_t kMaxUploadSize;

~~~

 The largest file size we support uploading.

 Note: changing this will affect the max size of
 SBDownloadFeedback.SizeSuccess and SizeFailure histograms.

### field error



~~~cpp

static const char kSbFeedbackURL[];

~~~

 The URL where the browser sends download feedback requests.

### ~DownloadFeedback

~DownloadFeedback
~~~cpp
virtual ~DownloadFeedback() {}
~~~

### RegisterFactory

RegisterFactory
~~~cpp
static void RegisterFactory(DownloadFeedbackFactory* factory) {
    factory_ = factory;
  }
~~~
 Makes the passed |factory| the factory used to instantiate
 a DownloadFeedback. Useful for tests.

### Start

Start
~~~cpp
virtual void Start(base::OnceClosure finish_callback) = 0;
~~~
 Start uploading the file to the download feedback service.

 |finish_callback| will be called when the upload completes or fails, but is
 not called if the upload is cancelled by deleting the DownloadFeedback
 while the upload is in progress.

### GetPingRequestForTesting

GetPingRequestForTesting
~~~cpp
virtual const std::string& GetPingRequestForTesting() const = 0;
~~~

### GetPingResponseForTesting

GetPingResponseForTesting
~~~cpp
virtual const std::string& GetPingResponseForTesting() const = 0;
~~~

### field error



~~~cpp

static DownloadFeedbackFactory* factory_;

~~~

 The factory that controls the creation of DownloadFeedback objects.

 This is used by tests.

### ~DownloadFeedback

~DownloadFeedback
~~~cpp
virtual ~DownloadFeedback() {}
~~~

### RegisterFactory

RegisterFactory
~~~cpp
static void RegisterFactory(DownloadFeedbackFactory* factory) {
    factory_ = factory;
  }
~~~
 Makes the passed |factory| the factory used to instantiate
 a DownloadFeedback. Useful for tests.

### Start

Start
~~~cpp
virtual void Start(base::OnceClosure finish_callback) = 0;
~~~
 Start uploading the file to the download feedback service.

 |finish_callback| will be called when the upload completes or fails, but is
 not called if the upload is cancelled by deleting the DownloadFeedback
 while the upload is in progress.

### GetPingRequestForTesting

GetPingRequestForTesting
~~~cpp
virtual const std::string& GetPingRequestForTesting() const = 0;
~~~

### GetPingResponseForTesting

GetPingResponseForTesting
~~~cpp
virtual const std::string& GetPingResponseForTesting() const = 0;
~~~

### Create

DownloadFeedback::Create
~~~cpp
static std::unique_ptr<DownloadFeedback> Create(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const base::FilePath& file_path,
      const std::string& ping_request,
      const std::string& ping_response);
~~~
 Takes ownership of the file pointed to be |file_path|, it will be deleted
 when the DownloadFeedback is destructed.

### kMaxUploadSize



~~~cpp

static const int64_t kMaxUploadSize;

~~~

 The largest file size we support uploading.

 Note: changing this will affect the max size of
 SBDownloadFeedback.SizeSuccess and SizeFailure histograms.

### field error



~~~cpp

static const char kSbFeedbackURL[];

~~~

 The URL where the browser sends download feedback requests.

### field error



~~~cpp

static DownloadFeedbackFactory* factory_;

~~~

 The factory that controls the creation of DownloadFeedback objects.

 This is used by tests.

## class DownloadFeedbackFactory

### ~DownloadFeedbackFactory

~DownloadFeedbackFactory
~~~cpp
virtual ~DownloadFeedbackFactory() {}
~~~

### CreateDownloadFeedback

CreateDownloadFeedback
~~~cpp
virtual std::unique_ptr<DownloadFeedback> CreateDownloadFeedback(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const base::FilePath& file_path,
      const std::string& ping_request,
      const std::string& ping_response) = 0;
~~~

### ~DownloadFeedbackFactory

~DownloadFeedbackFactory
~~~cpp
virtual ~DownloadFeedbackFactory() {}
~~~

### CreateDownloadFeedback

CreateDownloadFeedback
~~~cpp
virtual std::unique_ptr<DownloadFeedback> CreateDownloadFeedback(
      scoped_refptr<network::SharedURLLoaderFactory> url_loader_factory,
      base::TaskRunner* file_task_runner,
      const base::FilePath& file_path,
      const std::string& ping_request,
      const std::string& ping_response) = 0;
~~~
