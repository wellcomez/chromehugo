
## class DownloadProtectionObserver
 This class is responsible for observing download events and reporting them as
 appropriate. For save package scans, this also runs the correct callback when
 the user bypasses a scan.

### DownloadProtectionObserver

DownloadProtectionObserver::DownloadProtectionObserver
~~~cpp
DownloadProtectionObserver();
~~~

### DownloadProtectionObserver

DownloadProtectionObserver
~~~cpp
DownloadProtectionObserver(const DownloadProtectionObserver&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadProtectionObserver& operator=(const DownloadProtectionObserver&) =
      delete;
~~~

### ~DownloadProtectionObserver

DownloadProtectionObserver::~DownloadProtectionObserver
~~~cpp
~DownloadProtectionObserver() override;
~~~

### OnProfileAdded

DownloadProtectionObserver::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile) override;
~~~
 ProfileManagerObserver:
### OnOffTheRecordProfileCreated

DownloadProtectionObserver::OnOffTheRecordProfileCreated
~~~cpp
void OnOffTheRecordProfileCreated(Profile* off_the_record) override;
~~~
 ProfileObserver:
### OnProfileWillBeDestroyed

DownloadProtectionObserver::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~

### OnManagerGoingDown

DownloadProtectionObserver::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(
      download::SimpleDownloadManagerCoordinator* coordinator) override;
~~~
 SimpleDownloadManagerCoordinator::Observer:
### OnDownloadCreated

DownloadProtectionObserver::OnDownloadCreated
~~~cpp
void OnDownloadCreated(download::DownloadItem* download) override;
~~~

### OnDownloadDestroyed

DownloadProtectionObserver::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 DownloadItem::Observer:
### OnDownloadUpdated

DownloadProtectionObserver::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~

### OnDownloadRemoved

DownloadProtectionObserver::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(download::DownloadItem* download) override;
~~~

### ReportDelayedBypassEvent

DownloadProtectionObserver::ReportDelayedBypassEvent
~~~cpp
void ReportDelayedBypassEvent(download::DownloadItem* download,
                                download::DownloadDangerType danger_type);
~~~
 Reports the bypass event for this download because it was opened before the
 verdict was available. |danger_type| is the danger type returned by the
 async scan.

### danger_types_



~~~cpp

base::flat_map<download::DownloadItem*, download::DownloadDangerType>
      danger_types_;

~~~


### observed_profiles_



~~~cpp

base::ScopedMultiSourceObservation<Profile, ProfileObserver>
      observed_profiles_{this};

~~~


### observed_coordinators_



~~~cpp

base::ScopedMultiSourceObservation<
      download::SimpleDownloadManagerCoordinator,
      download::SimpleDownloadManagerCoordinator::Observer>
      observed_coordinators_{this};

~~~


### observed_downloads_



~~~cpp

base::ScopedMultiSourceObservation<download::DownloadItem,
                                     download::DownloadItem::Observer>
      observed_downloads_{this};

~~~


### AddBypassEventToPref

DownloadProtectionObserver::AddBypassEventToPref
~~~cpp
void AddBypassEventToPref(download::DownloadItem* download);
~~~

### ReportAndRecordDangerousDownloadWarningBypassed

DownloadProtectionObserver::ReportAndRecordDangerousDownloadWarningBypassed
~~~cpp
void ReportAndRecordDangerousDownloadWarningBypassed(
      download::DownloadItem* download,
      download::DownloadDangerType danger_type);
~~~

### DownloadProtectionObserver

DownloadProtectionObserver
~~~cpp
DownloadProtectionObserver(const DownloadProtectionObserver&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadProtectionObserver& operator=(const DownloadProtectionObserver&) =
      delete;
~~~

### OnProfileAdded

DownloadProtectionObserver::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile) override;
~~~
 ProfileManagerObserver:
### OnOffTheRecordProfileCreated

DownloadProtectionObserver::OnOffTheRecordProfileCreated
~~~cpp
void OnOffTheRecordProfileCreated(Profile* off_the_record) override;
~~~
 ProfileObserver:
### OnProfileWillBeDestroyed

DownloadProtectionObserver::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~

### OnManagerGoingDown

DownloadProtectionObserver::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(
      download::SimpleDownloadManagerCoordinator* coordinator) override;
~~~
 SimpleDownloadManagerCoordinator::Observer:
### OnDownloadCreated

DownloadProtectionObserver::OnDownloadCreated
~~~cpp
void OnDownloadCreated(download::DownloadItem* download) override;
~~~

### OnDownloadDestroyed

DownloadProtectionObserver::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 DownloadItem::Observer:
### OnDownloadUpdated

DownloadProtectionObserver::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~

### OnDownloadRemoved

DownloadProtectionObserver::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(download::DownloadItem* download) override;
~~~

### ReportDelayedBypassEvent

DownloadProtectionObserver::ReportDelayedBypassEvent
~~~cpp
void ReportDelayedBypassEvent(download::DownloadItem* download,
                                download::DownloadDangerType danger_type);
~~~
 Reports the bypass event for this download because it was opened before the
 verdict was available. |danger_type| is the danger type returned by the
 async scan.

### danger_types_



~~~cpp

base::flat_map<download::DownloadItem*, download::DownloadDangerType>
      danger_types_;

~~~


### observed_profiles_



~~~cpp

base::ScopedMultiSourceObservation<Profile, ProfileObserver>
      observed_profiles_{this};

~~~


### observed_coordinators_



~~~cpp

base::ScopedMultiSourceObservation<
      download::SimpleDownloadManagerCoordinator,
      download::SimpleDownloadManagerCoordinator::Observer>
      observed_coordinators_{this};

~~~


### observed_downloads_



~~~cpp

base::ScopedMultiSourceObservation<download::DownloadItem,
                                     download::DownloadItem::Observer>
      observed_downloads_{this};

~~~


### AddBypassEventToPref

DownloadProtectionObserver::AddBypassEventToPref
~~~cpp
void AddBypassEventToPref(download::DownloadItem* download);
~~~

### ReportAndRecordDangerousDownloadWarningBypassed

DownloadProtectionObserver::ReportAndRecordDangerousDownloadWarningBypassed
~~~cpp
void ReportAndRecordDangerousDownloadWarningBypassed(
      download::DownloadItem* download,
      download::DownloadDangerType danger_type);
~~~
