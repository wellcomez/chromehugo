
## class ChromeExtensionMessageFilter
 This class filters out incoming Chrome-specific IPC messages from the
 extension process on the IPC thread.

### ChromeExtensionMessageFilter

ChromeExtensionMessageFilter::ChromeExtensionMessageFilter
~~~cpp
explicit ChromeExtensionMessageFilter(Profile* profile);
~~~

### ChromeExtensionMessageFilter

ChromeExtensionMessageFilter
~~~cpp
ChromeExtensionMessageFilter(const ChromeExtensionMessageFilter&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeExtensionMessageFilter& operator=(const ChromeExtensionMessageFilter&) =
      delete;
~~~

### OnMessageReceived

ChromeExtensionMessageFilter::OnMessageReceived
~~~cpp
bool OnMessageReceived(const IPC::Message& message) override;
~~~
 content::BrowserMessageFilter methods:
### OverrideThreadForMessage

ChromeExtensionMessageFilter::OverrideThreadForMessage
~~~cpp
void OverrideThreadForMessage(const IPC::Message& message,
                                content::BrowserThread::ID* thread) override;
~~~

### OnDestruct

ChromeExtensionMessageFilter::OnDestruct
~~~cpp
void OnDestruct() const override;
~~~

### ~ChromeExtensionMessageFilter

ChromeExtensionMessageFilter::~ChromeExtensionMessageFilter
~~~cpp
~ChromeExtensionMessageFilter() override;
~~~

### OnGetExtMessageBundle

ChromeExtensionMessageFilter::OnGetExtMessageBundle
~~~cpp
void OnGetExtMessageBundle(const std::string& extension_id,
                             IPC::Message* reply_msg);
~~~
 TODO(jamescook): Move these functions into the extensions module. Ideally
 this would be in extensions::ExtensionMessageFilter but that will require
 resolving the ActivityLog dependencies on src/chrome.

 http://crbug.com/339637
### OnGetExtMessageBundleAsync

ChromeExtensionMessageFilter::OnGetExtMessageBundleAsync
~~~cpp
void OnGetExtMessageBundleAsync(
      const std::vector<base::FilePath>& extension_paths,
      const std::string& main_extension_id,
      const std::string& default_locale,
      extension_l10n_util::GzippedMessagesPermission gzip_permission,
      IPC::Message* reply_msg);
~~~

### OnAddAPIActionToExtensionActivityLog

ChromeExtensionMessageFilter::OnAddAPIActionToExtensionActivityLog
~~~cpp
void OnAddAPIActionToExtensionActivityLog(
      const std::string& extension_id,
      const ExtensionHostMsg_APIActionOrEvent_Params& params);
~~~

### OnAddDOMActionToExtensionActivityLog

ChromeExtensionMessageFilter::OnAddDOMActionToExtensionActivityLog
~~~cpp
void OnAddDOMActionToExtensionActivityLog(
      const std::string& extension_id,
      const ExtensionHostMsg_DOMAction_Params& params);
~~~

### OnAddEventToExtensionActivityLog

ChromeExtensionMessageFilter::OnAddEventToExtensionActivityLog
~~~cpp
void OnAddEventToExtensionActivityLog(
      const std::string& extension_id,
      const ExtensionHostMsg_APIActionOrEvent_Params& params);
~~~

### OnProfileWillBeDestroyed

ChromeExtensionMessageFilter::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~
 ProfileObserver:
### ShouldLogExtensionAction

ChromeExtensionMessageFilter::ShouldLogExtensionAction
~~~cpp
bool ShouldLogExtensionAction(const std::string& extension_id) const;
~~~
 Returns true if an action should be logged for the given extension.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~

 The Profile associated with our renderer process.  This should only be
 accessed on the UI thread! Furthermore since this class is refcounted it
 may outlive |profile_|, so make sure to NULL check if in doubt; async
 calls and the like.

### activity_log_



~~~cpp

raw_ptr<extensions::ActivityLog> activity_log_;

~~~

 The ActivityLog associated with the given profile. Also only safe to
 access on the UI thread, and may be null.

### observed_profile_



~~~cpp

base::ScopedObservation<Profile, ProfileObserver> observed_profile_{this};

~~~


### ChromeExtensionMessageFilter

ChromeExtensionMessageFilter
~~~cpp
ChromeExtensionMessageFilter(const ChromeExtensionMessageFilter&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeExtensionMessageFilter& operator=(const ChromeExtensionMessageFilter&) =
      delete;
~~~

### OnMessageReceived

ChromeExtensionMessageFilter::OnMessageReceived
~~~cpp
bool OnMessageReceived(const IPC::Message& message) override;
~~~
 content::BrowserMessageFilter methods:
### OverrideThreadForMessage

ChromeExtensionMessageFilter::OverrideThreadForMessage
~~~cpp
void OverrideThreadForMessage(const IPC::Message& message,
                                content::BrowserThread::ID* thread) override;
~~~

### OnDestruct

ChromeExtensionMessageFilter::OnDestruct
~~~cpp
void OnDestruct() const override;
~~~

### OnGetExtMessageBundle

ChromeExtensionMessageFilter::OnGetExtMessageBundle
~~~cpp
void OnGetExtMessageBundle(const std::string& extension_id,
                             IPC::Message* reply_msg);
~~~
 TODO(jamescook): Move these functions into the extensions module. Ideally
 this would be in extensions::ExtensionMessageFilter but that will require
 resolving the ActivityLog dependencies on src/chrome.

 http://crbug.com/339637
### OnGetExtMessageBundleAsync

ChromeExtensionMessageFilter::OnGetExtMessageBundleAsync
~~~cpp
void OnGetExtMessageBundleAsync(
      const std::vector<base::FilePath>& extension_paths,
      const std::string& main_extension_id,
      const std::string& default_locale,
      extension_l10n_util::GzippedMessagesPermission gzip_permission,
      IPC::Message* reply_msg);
~~~

### OnAddAPIActionToExtensionActivityLog

ChromeExtensionMessageFilter::OnAddAPIActionToExtensionActivityLog
~~~cpp
void OnAddAPIActionToExtensionActivityLog(
      const std::string& extension_id,
      const ExtensionHostMsg_APIActionOrEvent_Params& params);
~~~

### OnAddDOMActionToExtensionActivityLog

ChromeExtensionMessageFilter::OnAddDOMActionToExtensionActivityLog
~~~cpp
void OnAddDOMActionToExtensionActivityLog(
      const std::string& extension_id,
      const ExtensionHostMsg_DOMAction_Params& params);
~~~

### OnAddEventToExtensionActivityLog

ChromeExtensionMessageFilter::OnAddEventToExtensionActivityLog
~~~cpp
void OnAddEventToExtensionActivityLog(
      const std::string& extension_id,
      const ExtensionHostMsg_APIActionOrEvent_Params& params);
~~~

### OnProfileWillBeDestroyed

ChromeExtensionMessageFilter::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~
 ProfileObserver:
### ShouldLogExtensionAction

ChromeExtensionMessageFilter::ShouldLogExtensionAction
~~~cpp
bool ShouldLogExtensionAction(const std::string& extension_id) const;
~~~
 Returns true if an action should be logged for the given extension.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~

 The Profile associated with our renderer process.  This should only be
 accessed on the UI thread! Furthermore since this class is refcounted it
 may outlive |profile_|, so make sure to NULL check if in doubt; async
 calls and the like.

### activity_log_



~~~cpp

raw_ptr<extensions::ActivityLog> activity_log_;

~~~

 The ActivityLog associated with the given profile. Also only safe to
 access on the UI thread, and may be null.

### observed_profile_



~~~cpp

base::ScopedObservation<Profile, ProfileObserver> observed_profile_{this};

~~~

