### 


~~~cpp
interface HistorySwiper : NSObject {
 @private
  // This controller will exist if and only if the UI is in history swipe mode.
  HistoryOverlayController* _historyOverlay;
  // The location of the fingers when the gesture started.
  NSPoint _gestureStartPoint;
  // The current location of the fingers in the gesture.
  NSPoint _gestureCurrentPoint;
  // The total Y distance moved since the beginning of the gesture.
  CGFloat _gestureTotalY;
  // A flag that indicates that there is an ongoing gesture. Only used to
  // determine whether swipe events are coming from a Magic Mouse.
  BOOL _inGesture;
  // A flag that indicates that Chrome is receiving a series of touch events.
  BOOL _receivingTouches;
  // Each time a new gesture begins, we must get a new start point.
  // This ivar determines whether the start point is valid.
  int _gestureStartPointValid;

  // The user's intended direction with the history swipe. Set during the
  // transition from kPending -> kPotential.
  history_swiper::NavigationDirection _historySwipeDirection;

  // Whether the history swipe gesture has its direction inverted. Set during
  // the transition from kPending -> kPotential.
  BOOL _historySwipeDirectionInverted;

  // Whether:
  //  1) When wheel gestures are disabled if the wheel event with phase
  //     NSEventPhaseBegan was consumed by the renderer.
  //  2) When wheel gestures are enabled and if the first gesture
  //     scroll was not consumed by the renderer.
  // This variables defaults to NO for new gestures.
  BOOL _firstScrollUnconsumed;

  // Whether the overscroll has been triggered by renderer and is not disabled
  // by CSSOverscrollBehavior.
  BOOL _overscrollTriggeredByRenderer;

  // Whether we have received a gesture scroll begin and are awiting on the
  // first gesture scroll update to deteremine of the event was consumed by
  // the renderer.
  BOOL _waitingForFirstGestureScroll;

  history_swiper::RecognitionState _recognitionState;

  id<HistorySwiperDelegate> _delegate;

  // Cumulative scroll delta since scroll gesture start. Only valid during
  // scroll gesture handling. Only used to trigger Magic Mouse history swiping.
  NSSize _mouseScrollDelta;
}
~~~

