
## class PepperIsolatedFileSystemMessageFilter
 namespace ppapi
### Create

PepperIsolatedFileSystemMessageFilter::Create
~~~cpp
static PepperIsolatedFileSystemMessageFilter* Create(
      PP_Instance instance,
      content::BrowserPpapiHost* host);
~~~

### PepperIsolatedFileSystemMessageFilter

PepperIsolatedFileSystemMessageFilter
~~~cpp
PepperIsolatedFileSystemMessageFilter(
      const PepperIsolatedFileSystemMessageFilter&) = delete;
~~~

### operator=

operator=
~~~cpp
PepperIsolatedFileSystemMessageFilter& operator=(
      const PepperIsolatedFileSystemMessageFilter&) = delete;
~~~

### OverrideTaskRunnerForMessage

PepperIsolatedFileSystemMessageFilter::OverrideTaskRunnerForMessage
~~~cpp
scoped_refptr<base::SequencedTaskRunner> OverrideTaskRunnerForMessage(
      const IPC::Message& msg) override;
~~~
 ppapi::host::ResourceMessageFilter implementation.

### OnResourceMessageReceived

PepperIsolatedFileSystemMessageFilter::OnResourceMessageReceived
~~~cpp
int32_t OnResourceMessageReceived(
      const IPC::Message& msg,
      ppapi::host::HostMessageContext* context) override;
~~~

### PepperIsolatedFileSystemMessageFilter

PepperIsolatedFileSystemMessageFilter::PepperIsolatedFileSystemMessageFilter
~~~cpp
PepperIsolatedFileSystemMessageFilter(int render_process_id,
                                        const base::FilePath& profile_directory,
                                        const GURL& document_url,
                                        ppapi::host::PpapiHost* ppapi_host_);
~~~

### ~PepperIsolatedFileSystemMessageFilter

PepperIsolatedFileSystemMessageFilter::~PepperIsolatedFileSystemMessageFilter
~~~cpp
~PepperIsolatedFileSystemMessageFilter() override;
~~~

### GetProfile

PepperIsolatedFileSystemMessageFilter::GetProfile
~~~cpp
Profile* GetProfile();
~~~

### CreateCrxFileSystem

PepperIsolatedFileSystemMessageFilter::CreateCrxFileSystem
~~~cpp
storage::IsolatedContext::ScopedFSHandle CreateCrxFileSystem(
      Profile* profile);
~~~
 Returns filesystem id of isolated filesystem if valid, or empty string
 otherwise.  This must run on the UI thread because ProfileManager only
 allows access on that thread.

### OnOpenFileSystem

PepperIsolatedFileSystemMessageFilter::OnOpenFileSystem
~~~cpp
int32_t OnOpenFileSystem(ppapi::host::HostMessageContext* context,
                           PP_IsolatedFileSystemType_Private type);
~~~

### OpenCrxFileSystem

PepperIsolatedFileSystemMessageFilter::OpenCrxFileSystem
~~~cpp
int32_t OpenCrxFileSystem(ppapi::host::HostMessageContext* context);
~~~

### render_process_id_



~~~cpp

const int render_process_id_;

~~~


### profile_directory_



~~~cpp

const base::FilePath profile_directory_;

~~~

 Keep a copy from original thread.

### document_url_



~~~cpp

const GURL document_url_;

~~~


### allowed_crxfs_origins_



~~~cpp

std::set<std::string> allowed_crxfs_origins_;

~~~

 Set of origins that can use CrxFs private APIs from NaCl.

### PepperIsolatedFileSystemMessageFilter

PepperIsolatedFileSystemMessageFilter
~~~cpp
PepperIsolatedFileSystemMessageFilter(
      const PepperIsolatedFileSystemMessageFilter&) = delete;
~~~

### operator=

operator=
~~~cpp
PepperIsolatedFileSystemMessageFilter& operator=(
      const PepperIsolatedFileSystemMessageFilter&) = delete;
~~~

### Create

PepperIsolatedFileSystemMessageFilter::Create
~~~cpp
static PepperIsolatedFileSystemMessageFilter* Create(
      PP_Instance instance,
      content::BrowserPpapiHost* host);
~~~

### OverrideTaskRunnerForMessage

PepperIsolatedFileSystemMessageFilter::OverrideTaskRunnerForMessage
~~~cpp
scoped_refptr<base::SequencedTaskRunner> OverrideTaskRunnerForMessage(
      const IPC::Message& msg) override;
~~~
 ppapi::host::ResourceMessageFilter implementation.

### OnResourceMessageReceived

PepperIsolatedFileSystemMessageFilter::OnResourceMessageReceived
~~~cpp
int32_t OnResourceMessageReceived(
      const IPC::Message& msg,
      ppapi::host::HostMessageContext* context) override;
~~~

### GetProfile

PepperIsolatedFileSystemMessageFilter::GetProfile
~~~cpp
Profile* GetProfile();
~~~

### CreateCrxFileSystem

PepperIsolatedFileSystemMessageFilter::CreateCrxFileSystem
~~~cpp
storage::IsolatedContext::ScopedFSHandle CreateCrxFileSystem(
      Profile* profile);
~~~
 Returns filesystem id of isolated filesystem if valid, or empty string
 otherwise.  This must run on the UI thread because ProfileManager only
 allows access on that thread.

### OnOpenFileSystem

PepperIsolatedFileSystemMessageFilter::OnOpenFileSystem
~~~cpp
int32_t OnOpenFileSystem(ppapi::host::HostMessageContext* context,
                           PP_IsolatedFileSystemType_Private type);
~~~

### OpenCrxFileSystem

PepperIsolatedFileSystemMessageFilter::OpenCrxFileSystem
~~~cpp
int32_t OpenCrxFileSystem(ppapi::host::HostMessageContext* context);
~~~

### render_process_id_



~~~cpp

const int render_process_id_;

~~~


### profile_directory_



~~~cpp

const base::FilePath profile_directory_;

~~~

 Keep a copy from original thread.

### document_url_



~~~cpp

const GURL document_url_;

~~~


### allowed_crxfs_origins_



~~~cpp

std::set<std::string> allowed_crxfs_origins_;

~~~

 Set of origins that can use CrxFs private APIs from NaCl.
