
## class ChromeBrowserPepperHostFactory
 namespace content
### ChromeBrowserPepperHostFactory

ChromeBrowserPepperHostFactory::ChromeBrowserPepperHostFactory
~~~cpp
explicit ChromeBrowserPepperHostFactory(content::BrowserPpapiHost* host);
~~~
 Non-owning pointer to the filter must outlive this class.

### ChromeBrowserPepperHostFactory

ChromeBrowserPepperHostFactory
~~~cpp
ChromeBrowserPepperHostFactory(const ChromeBrowserPepperHostFactory&) =
      delete;
~~~

### operator=

operator=
~~~cpp
ChromeBrowserPepperHostFactory& operator=(
      const ChromeBrowserPepperHostFactory&) = delete;
~~~

### ~ChromeBrowserPepperHostFactory

ChromeBrowserPepperHostFactory::~ChromeBrowserPepperHostFactory
~~~cpp
~ChromeBrowserPepperHostFactory() override;
~~~

### CreateResourceHost

ChromeBrowserPepperHostFactory::CreateResourceHost
~~~cpp
std::unique_ptr<ppapi::host::ResourceHost> CreateResourceHost(
      ppapi::host::PpapiHost* host,
      PP_Resource resource,
      PP_Instance instance,
      const IPC::Message& message) override;
~~~

### host_



~~~cpp

raw_ptr<content::BrowserPpapiHost> host_;

~~~

 Non-owning pointer.

### ChromeBrowserPepperHostFactory

ChromeBrowserPepperHostFactory
~~~cpp
ChromeBrowserPepperHostFactory(const ChromeBrowserPepperHostFactory&) =
      delete;
~~~

### operator=

operator=
~~~cpp
ChromeBrowserPepperHostFactory& operator=(
      const ChromeBrowserPepperHostFactory&) = delete;
~~~

### CreateResourceHost

ChromeBrowserPepperHostFactory::CreateResourceHost
~~~cpp
std::unique_ptr<ppapi::host::ResourceHost> CreateResourceHost(
      ppapi::host::PpapiHost* host,
      PP_Resource resource,
      PP_Instance instance,
      const IPC::Message& message) override;
~~~

### host_



~~~cpp

raw_ptr<content::BrowserPpapiHost> host_;

~~~

 Non-owning pointer.
