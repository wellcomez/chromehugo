### 


~~~cpp
interface ChromeRenderWidgetHostViewMacDelegate
    : NSObject<RenderWidgetHostViewMacDelegate> {
 @private
  raw_ptr<content::RenderWidgetHost> _renderWidgetHost;  // weak

  // Responsible for 2-finger swipes history navigation.
  base::scoped_nsobject<HistorySwiper> _historySwiper;
}
~~~

