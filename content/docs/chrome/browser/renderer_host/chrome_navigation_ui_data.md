
## class ChromeNavigationUIData
 Contains data that is passed from the UI thread to the IO thread at the
 beginning of each navigation. The class is instantiated on the UI thread,
 then a copy created using Clone is passed to the content::ResourceRequestInfo
 on the IO thread.

### ChromeNavigationUIData

ChromeNavigationUIData::ChromeNavigationUIData
~~~cpp
ChromeNavigationUIData();
~~~

### ChromeNavigationUIData

ChromeNavigationUIData::ChromeNavigationUIData
~~~cpp
explicit ChromeNavigationUIData(content::NavigationHandle* navigation_handle);
~~~

### ChromeNavigationUIData

ChromeNavigationUIData
~~~cpp
ChromeNavigationUIData(const ChromeNavigationUIData&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeNavigationUIData& operator=(const ChromeNavigationUIData&) = delete;
~~~

### ~ChromeNavigationUIData

ChromeNavigationUIData::~ChromeNavigationUIData
~~~cpp
~ChromeNavigationUIData() override;
~~~

### CreateForMainFrameNavigation

ChromeNavigationUIData::CreateForMainFrameNavigation
~~~cpp
static std::unique_ptr<ChromeNavigationUIData> CreateForMainFrameNavigation(
      content::WebContents* web_contents,
      WindowOpenDisposition disposition,
      bool is_using_https_as_default_scheme);
~~~
 Creates an instance of ChromeNavigationUIData associated with the given
 |web_contents| with the given |disposition|.

 If |is_using_https_as_default_scheme|, this is a typed main frame
 navigation where the omnibox used HTTPS as the default URL scheme because
 the user didn't type a scheme (e.g. they entered "example.com" and we
 are navigating to https://example.com).

### Clone

ChromeNavigationUIData::Clone
~~~cpp
std::unique_ptr<content::NavigationUIData> Clone() override;
~~~
 Creates a new ChromeNavigationUIData that is a deep copy of the original.

 Any changes to the original after the clone is created will not be
 reflected in the clone.  All owned data members are deep copied.

### SetExtensionNavigationUIData

ChromeNavigationUIData::SetExtensionNavigationUIData
~~~cpp
void SetExtensionNavigationUIData(
      std::unique_ptr<extensions::ExtensionNavigationUIData> extension_data);
~~~

### GetExtensionNavigationUIData

GetExtensionNavigationUIData
~~~cpp
extensions::ExtensionNavigationUIData* GetExtensionNavigationUIData() const {
    return extension_data_.get();
  }
~~~

### SetOfflinePageNavigationUIData

ChromeNavigationUIData::SetOfflinePageNavigationUIData
~~~cpp
void SetOfflinePageNavigationUIData(
      std::unique_ptr<offline_pages::OfflinePageNavigationUIData>
          offline_page_data);
~~~

### GetOfflinePageNavigationUIData

GetOfflinePageNavigationUIData
~~~cpp
offline_pages::OfflinePageNavigationUIData* GetOfflinePageNavigationUIData()
      const {
    return offline_page_data_.get();
  }
~~~

### window_open_disposition

window_open_disposition
~~~cpp
WindowOpenDisposition window_open_disposition() const { return disposition_; }
~~~

### is_no_state_prefetching

is_no_state_prefetching
~~~cpp
bool is_no_state_prefetching() const { return is_no_state_prefetching_; }
~~~

### prerender_histogram_prefix

prerender_histogram_prefix
~~~cpp
const std::string& prerender_histogram_prefix() {
    return prerender_histogram_prefix_;
  }
~~~

### is_using_https_as_default_scheme

is_using_https_as_default_scheme
~~~cpp
bool is_using_https_as_default_scheme() const {
    return is_using_https_as_default_scheme_;
  }
~~~

### bookmark_id

bookmark_id
~~~cpp
absl::optional<int64_t> bookmark_id() { return bookmark_id_; }
~~~

### set_bookmark_id

set_bookmark_id
~~~cpp
void set_bookmark_id(absl::optional<int64_t> id) { bookmark_id_ = id; }
~~~

### extension_data_



~~~cpp

std::unique_ptr<extensions::ExtensionNavigationUIData> extension_data_;

~~~

 Manages the lifetime of optional ExtensionNavigationUIData information.

### offline_page_data_



~~~cpp

std::unique_ptr<offline_pages::OfflinePageNavigationUIData>
      offline_page_data_;

~~~

 Manages the lifetime of optional OfflinePageNavigationUIData information.

### disposition_



~~~cpp

WindowOpenDisposition disposition_;

~~~


### is_no_state_prefetching_



~~~cpp

bool is_no_state_prefetching_ = false;

~~~


### prerender_histogram_prefix_



~~~cpp

std::string prerender_histogram_prefix_;

~~~


### is_using_https_as_default_scheme_



~~~cpp

bool is_using_https_as_default_scheme_ = false;

~~~

 True if the navigation was initiated by typing in the omnibox but the typed
 text didn't have a scheme such as http or https (e.g. google.com), and
 https was used as the default scheme for the navigation. This is used by
 TypedNavigationUpgradeThrottle to determine if the navigation should be
 observed and fall back to using http scheme if necessary.

### bookmark_id_



~~~cpp

absl::optional<int64_t> bookmark_id_ = absl::nullopt;

~~~

 Id of the bookmark which started this navigation.

### ChromeNavigationUIData

ChromeNavigationUIData
~~~cpp
ChromeNavigationUIData(const ChromeNavigationUIData&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeNavigationUIData& operator=(const ChromeNavigationUIData&) = delete;
~~~

### window_open_disposition

window_open_disposition
~~~cpp
WindowOpenDisposition window_open_disposition() const { return disposition_; }
~~~

### is_no_state_prefetching

is_no_state_prefetching
~~~cpp
bool is_no_state_prefetching() const { return is_no_state_prefetching_; }
~~~

### prerender_histogram_prefix

prerender_histogram_prefix
~~~cpp
const std::string& prerender_histogram_prefix() {
    return prerender_histogram_prefix_;
  }
~~~

### is_using_https_as_default_scheme

is_using_https_as_default_scheme
~~~cpp
bool is_using_https_as_default_scheme() const {
    return is_using_https_as_default_scheme_;
  }
~~~

### bookmark_id

bookmark_id
~~~cpp
absl::optional<int64_t> bookmark_id() { return bookmark_id_; }
~~~

### set_bookmark_id

set_bookmark_id
~~~cpp
void set_bookmark_id(absl::optional<int64_t> id) { bookmark_id_ = id; }
~~~

### CreateForMainFrameNavigation

ChromeNavigationUIData::CreateForMainFrameNavigation
~~~cpp
static std::unique_ptr<ChromeNavigationUIData> CreateForMainFrameNavigation(
      content::WebContents* web_contents,
      WindowOpenDisposition disposition,
      bool is_using_https_as_default_scheme);
~~~
 Creates an instance of ChromeNavigationUIData associated with the given
 |web_contents| with the given |disposition|.

 If |is_using_https_as_default_scheme|, this is a typed main frame
 navigation where the omnibox used HTTPS as the default URL scheme because
 the user didn't type a scheme (e.g. they entered "example.com" and we
 are navigating to https://example.com).

### Clone

ChromeNavigationUIData::Clone
~~~cpp
std::unique_ptr<content::NavigationUIData> Clone() override;
~~~
 Creates a new ChromeNavigationUIData that is a deep copy of the original.

 Any changes to the original after the clone is created will not be
 reflected in the clone.  All owned data members are deep copied.

### disposition_



~~~cpp

WindowOpenDisposition disposition_;

~~~


### is_no_state_prefetching_



~~~cpp

bool is_no_state_prefetching_ = false;

~~~


### prerender_histogram_prefix_



~~~cpp

std::string prerender_histogram_prefix_;

~~~


### is_using_https_as_default_scheme_



~~~cpp

bool is_using_https_as_default_scheme_ = false;

~~~

 True if the navigation was initiated by typing in the omnibox but the typed
 text didn't have a scheme such as http or https (e.g. google.com), and
 https was used as the default scheme for the navigation. This is used by
 TypedNavigationUpgradeThrottle to determine if the navigation should be
 observed and fall back to using http scheme if necessary.

### bookmark_id_



~~~cpp

absl::optional<int64_t> bookmark_id_ = absl::nullopt;

~~~

 Id of the bookmark which started this navigation.
