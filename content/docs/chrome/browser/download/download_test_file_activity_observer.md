
## class DownloadTestFileActivityObserver
 Observes and overrides file chooser dialog and open activity for a profile.

 By default, once attached to a profile, this class overrides the default file
 related activity by replacing the ChromeDownloadManagerDelegate associated
 with |profile|.

 NOTE: Again, this overrides the ChromeDownloadManagerDelegate for |profile|.

### DownloadTestFileActivityObserver

DownloadTestFileActivityObserver::DownloadTestFileActivityObserver
~~~cpp
explicit DownloadTestFileActivityObserver(Profile* profile);
~~~
 Attaches to |profile|. By default file chooser dialogs will be disabled
 once attached. Call EnableFileChooser() to re-enable.

### ~DownloadTestFileActivityObserver

DownloadTestFileActivityObserver::~DownloadTestFileActivityObserver
~~~cpp
~DownloadTestFileActivityObserver();
~~~

### EnableFileChooser

DownloadTestFileActivityObserver::EnableFileChooser
~~~cpp
void EnableFileChooser(bool enable);
~~~
 Sets whether the file chooser dialog is enabled. If |enable| is false, any
 attempt to display a file chooser dialog will cause the download to be
 canceled. Otherwise, attempting to display a file chooser dialog will
 result in the download continuing with the suggested path.

### TestAndResetDidShowFileChooser

DownloadTestFileActivityObserver::TestAndResetDidShowFileChooser
~~~cpp
bool TestAndResetDidShowFileChooser();
~~~
 Returns true if a file chooser dialog was displayed since the last time
 this method was called.

### field error



~~~cpp

class MockDownloadManagerDelegate;

~~~


### test_delegate_



~~~cpp

base::WeakPtr<MockDownloadManagerDelegate> test_delegate_;

~~~


### EnableFileChooser

DownloadTestFileActivityObserver::EnableFileChooser
~~~cpp
void EnableFileChooser(bool enable);
~~~
 Sets whether the file chooser dialog is enabled. If |enable| is false, any
 attempt to display a file chooser dialog will cause the download to be
 canceled. Otherwise, attempting to display a file chooser dialog will
 result in the download continuing with the suggested path.

### TestAndResetDidShowFileChooser

DownloadTestFileActivityObserver::TestAndResetDidShowFileChooser
~~~cpp
bool TestAndResetDidShowFileChooser();
~~~
 Returns true if a file chooser dialog was displayed since the last time
 this method was called.

### field error



~~~cpp

class MockDownloadManagerDelegate;

~~~


### test_delegate_



~~~cpp

base::WeakPtr<MockDownloadManagerDelegate> test_delegate_;

~~~

