### RecordDownloadSource

RecordDownloadSource
~~~cpp
void RecordDownloadSource(ChromeDownloadSource source);
~~~
 Record initiation of a download from a specific source.

### RecordDangerousDownloadWarningShown

RecordDangerousDownloadWarningShown
~~~cpp
void RecordDangerousDownloadWarningShown(
    download::DownloadDangerType danger_type,
    const base::FilePath& file_path,
    bool is_https,
    bool has_user_gesture);
~~~
 Record that a download warning was shown.

### RecordDownloadOpen

RecordDownloadOpen
~~~cpp
void RecordDownloadOpen(ChromeDownloadOpenMethod open_method,
                        const std::string& mime_type_string);
~~~
 Record that a download was opened.

### RecordDownloadOpenButtonPressed

RecordDownloadOpenButtonPressed
~~~cpp
void RecordDownloadOpenButtonPressed(bool is_download_completed);
~~~
 TODO(crbug.com/1372476): Remove this function after debugging.

 Record that a download open button was pressed, either on download shelf or
 download bubble.

### RecordDatabaseAvailability

RecordDatabaseAvailability
~~~cpp
void RecordDatabaseAvailability(bool is_available);
~~~
 Record if the database is available to provide the next download id before
 starting all downloads.

### RecordDownloadPathGeneration

RecordDownloadPathGeneration
~~~cpp
void RecordDownloadPathGeneration(DownloadPathGenerationEvent event,
                                  bool is_transient);
~~~
 Record download path generation event in target determination process.

### RecordDownloadPathValidation

RecordDownloadPathValidation
~~~cpp
void RecordDownloadPathValidation(download::PathValidationResult result,
                                  bool is_transient);
~~~
 Record path validation result.

### RecordDownloadCancelReason

RecordDownloadCancelReason
~~~cpp
void RecordDownloadCancelReason(DownloadCancelReason reason);
~~~
 Record download cancel reason.

### RecordDownloadShelfDragInfo

RecordDownloadShelfDragInfo
~~~cpp
void RecordDownloadShelfDragInfo(DownloadDragInfo drag_info);
~~~
 Records either when a drag event is initiated by the user or, as a point of
 reference, when a download completes on the shelf/bubble.

### RecordDownloadBubbleDragInfo

RecordDownloadBubbleDragInfo
~~~cpp
void RecordDownloadBubbleDragInfo(DownloadDragInfo drag_info);
~~~

### RecordDownloadStartPerProfileType

RecordDownloadStartPerProfileType
~~~cpp
void RecordDownloadStartPerProfileType(Profile* profile);
~~~

### RecordDownloadPromptStatus

RecordDownloadPromptStatus
~~~cpp
void RecordDownloadPromptStatus(DownloadPromptStatus status);
~~~
 Records whether the download dialog is shown to the user.

### RecordDownloadNotificationSuppressed

RecordDownloadNotificationSuppressed
~~~cpp
void RecordDownloadNotificationSuppressed();
~~~
 Records that a notification for a download was suppressed.

### DownloadCommandToShelfAction

DownloadCommandToShelfAction
~~~cpp
DownloadShelfContextMenuAction DownloadCommandToShelfAction(
    DownloadCommands::Command download_command,
    bool clicked);
~~~

