
## class DownloadPermissionRequest
 A permission request that presents the user with a choice to allow or deny
 multiple downloads from the same site. This confirmation step protects
 against "carpet-bombing", where a malicious site forces multiple downloads on
 an unsuspecting user.

### DownloadPermissionRequest

DownloadPermissionRequest::DownloadPermissionRequest
~~~cpp
DownloadPermissionRequest(
      base::WeakPtr<DownloadRequestLimiter::TabDownloadState> host,
      const url::Origin& requesting_origin);
~~~

### DownloadPermissionRequest

DownloadPermissionRequest
~~~cpp
DownloadPermissionRequest(const DownloadPermissionRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadPermissionRequest& operator=(const DownloadPermissionRequest&) =
      delete;
~~~

### ~DownloadPermissionRequest

DownloadPermissionRequest::~DownloadPermissionRequest
~~~cpp
~DownloadPermissionRequest() override;
~~~

### PermissionDecided

DownloadPermissionRequest::PermissionDecided
~~~cpp
void PermissionDecided(ContentSetting result,
                         bool is_one_time,
                         bool is_final_decision);
~~~

### DeleteRequest

DownloadPermissionRequest::DeleteRequest
~~~cpp
void DeleteRequest();
~~~

### host_



~~~cpp

base::WeakPtr<DownloadRequestLimiter::TabDownloadState> host_;

~~~


### requesting_origin_



~~~cpp

url::Origin requesting_origin_;

~~~


### DownloadPermissionRequest

DownloadPermissionRequest
~~~cpp
DownloadPermissionRequest(const DownloadPermissionRequest&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadPermissionRequest& operator=(const DownloadPermissionRequest&) =
      delete;
~~~

### PermissionDecided

DownloadPermissionRequest::PermissionDecided
~~~cpp
void PermissionDecided(ContentSetting result,
                         bool is_one_time,
                         bool is_final_decision);
~~~

### DeleteRequest

DownloadPermissionRequest::DeleteRequest
~~~cpp
void DeleteRequest();
~~~

### host_



~~~cpp

base::WeakPtr<DownloadRequestLimiter::TabDownloadState> host_;

~~~


### requesting_origin_



~~~cpp

url::Origin requesting_origin_;

~~~

