
## class BackgroundDownloadServiceFactory
 namespace download
 BackgroundDownloadServiceFactory is the main client class for interaction
 with the download component.

### GetInstance

BackgroundDownloadServiceFactory::GetInstance
~~~cpp
static BackgroundDownloadServiceFactory* GetInstance();
~~~
 Returns singleton instance of DownloadServiceFactory.

### GetForKey

BackgroundDownloadServiceFactory::GetForKey
~~~cpp
static download::BackgroundDownloadService* GetForKey(SimpleFactoryKey* key);
~~~
 Returns the DownloadService associated with |key|.

### BackgroundDownloadServiceFactory

BackgroundDownloadServiceFactory
~~~cpp
BackgroundDownloadServiceFactory(const BackgroundDownloadServiceFactory&) =
      delete;
~~~

### operator=

operator=
~~~cpp
BackgroundDownloadServiceFactory& operator=(
      const BackgroundDownloadServiceFactory&) = delete;
~~~

### BackgroundDownloadServiceFactory

BackgroundDownloadServiceFactory::BackgroundDownloadServiceFactory
~~~cpp
BackgroundDownloadServiceFactory();
~~~

### ~BackgroundDownloadServiceFactory

BackgroundDownloadServiceFactory::~BackgroundDownloadServiceFactory
~~~cpp
~BackgroundDownloadServiceFactory() override;
~~~

### BuildServiceInstanceFor

BackgroundDownloadServiceFactory::BuildServiceInstanceFor
~~~cpp
std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      SimpleFactoryKey* key) const override;
~~~
 SimpleKeyedServiceFactory overrides:
### GetKeyToUse

BackgroundDownloadServiceFactory::GetKeyToUse
~~~cpp
SimpleFactoryKey* GetKeyToUse(SimpleFactoryKey* key) const override;
~~~

### BackgroundDownloadServiceFactory

BackgroundDownloadServiceFactory
~~~cpp
BackgroundDownloadServiceFactory(const BackgroundDownloadServiceFactory&) =
      delete;
~~~

### operator=

operator=
~~~cpp
BackgroundDownloadServiceFactory& operator=(
      const BackgroundDownloadServiceFactory&) = delete;
~~~

### GetInstance

BackgroundDownloadServiceFactory::GetInstance
~~~cpp
static BackgroundDownloadServiceFactory* GetInstance();
~~~
 Returns singleton instance of DownloadServiceFactory.

### GetForKey

BackgroundDownloadServiceFactory::GetForKey
~~~cpp
static download::BackgroundDownloadService* GetForKey(SimpleFactoryKey* key);
~~~
 Returns the DownloadService associated with |key|.

### BuildServiceInstanceFor

BackgroundDownloadServiceFactory::BuildServiceInstanceFor
~~~cpp
std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      SimpleFactoryKey* key) const override;
~~~
 SimpleKeyedServiceFactory overrides:
### GetKeyToUse

BackgroundDownloadServiceFactory::GetKeyToUse
~~~cpp
SimpleFactoryKey* GetKeyToUse(SimpleFactoryKey* key) const override;
~~~
