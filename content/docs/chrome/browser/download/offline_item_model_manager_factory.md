
## class OfflineItemModelManagerFactory
 namespace content
 This class is the main access point for an OfflineItemModelManager.  It is
 responsible for building the OfflineItemModelManager and associating it with
 a particular content::BrowserContext.

### GetInstance

OfflineItemModelManagerFactory::GetInstance
~~~cpp
static OfflineItemModelManagerFactory* GetInstance();
~~~
 Returns a singleton instance of an OfflineItemModelManagerFactory.

### GetForBrowserContext

OfflineItemModelManagerFactory::GetForBrowserContext
~~~cpp
static OfflineItemModelManager* GetForBrowserContext(
      content::BrowserContext* context);
~~~
 Returns the OfflineItemModelManager associated with |context| or creates
 and associates one if it doesn't exist.

### OfflineItemModelManagerFactory

OfflineItemModelManagerFactory
~~~cpp
OfflineItemModelManagerFactory(const OfflineItemModelManagerFactory&) =
      delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemModelManagerFactory& operator=(
      const OfflineItemModelManagerFactory&) = delete;
~~~

### OfflineItemModelManagerFactory

OfflineItemModelManagerFactory::OfflineItemModelManagerFactory
~~~cpp
OfflineItemModelManagerFactory();
~~~

### ~OfflineItemModelManagerFactory

OfflineItemModelManagerFactory::~OfflineItemModelManagerFactory
~~~cpp
~OfflineItemModelManagerFactory() override;
~~~

### BuildServiceInstanceFor

OfflineItemModelManagerFactory::BuildServiceInstanceFor
~~~cpp
KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
~~~
 BrowserContextKeyedServiceFactory implementation.

### OfflineItemModelManagerFactory

OfflineItemModelManagerFactory
~~~cpp
OfflineItemModelManagerFactory(const OfflineItemModelManagerFactory&) =
      delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemModelManagerFactory& operator=(
      const OfflineItemModelManagerFactory&) = delete;
~~~

### GetInstance

OfflineItemModelManagerFactory::GetInstance
~~~cpp
static OfflineItemModelManagerFactory* GetInstance();
~~~
 Returns a singleton instance of an OfflineItemModelManagerFactory.

### GetForBrowserContext

OfflineItemModelManagerFactory::GetForBrowserContext
~~~cpp
static OfflineItemModelManager* GetForBrowserContext(
      content::BrowserContext* context);
~~~
 Returns the OfflineItemModelManager associated with |context| or creates
 and associates one if it doesn't exist.

### BuildServiceInstanceFor

OfflineItemModelManagerFactory::BuildServiceInstanceFor
~~~cpp
KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
~~~
 BrowserContextKeyedServiceFactory implementation.
