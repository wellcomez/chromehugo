### DownloadToDrive

DownloadToDrive
~~~cpp
bool DownloadToDrive(const base::FilePath::StringType& string_value,
                     const policy::PolicyHandlerParameters& parameters);
~~~
 Returns whether |string_value| points to a directory in Drive or not.

### ExpandDrivePolicyVariable

ExpandDrivePolicyVariable
~~~cpp
bool ExpandDrivePolicyVariable(Profile* profile,
                               const base::FilePath& old_path,
                               base::FilePath* new_path);
~~~
 Expands the google drive policy variable to the drive root path. This cannot
 be done in ExpandDownloadDirectoryPath() as that gets invoked in a policy
 handler, which are run before the profile is registered.

### ExpandDownloadDirectoryPath

ExpandDownloadDirectoryPath
~~~cpp
base::FilePath::StringType ExpandDownloadDirectoryPath(
    const base::FilePath::StringType& string_value,
    const policy::PolicyHandlerParameters& parameters);
~~~
 BUILDFLAG(IS_CHROMEOS)
 Expands path variables in the download directory path |string_value|.

