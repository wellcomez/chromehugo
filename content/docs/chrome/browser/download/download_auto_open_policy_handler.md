
## class DownloadAutoOpenPolicyHandler
 namespace policy
### DownloadAutoOpenPolicyHandler

DownloadAutoOpenPolicyHandler::DownloadAutoOpenPolicyHandler
~~~cpp
explicit DownloadAutoOpenPolicyHandler(const policy::Schema& chrome_schema);
~~~

### ~DownloadAutoOpenPolicyHandler

DownloadAutoOpenPolicyHandler::~DownloadAutoOpenPolicyHandler
~~~cpp
~DownloadAutoOpenPolicyHandler() override;
~~~

### DownloadAutoOpenPolicyHandler

DownloadAutoOpenPolicyHandler
~~~cpp
DownloadAutoOpenPolicyHandler(const DownloadAutoOpenPolicyHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadAutoOpenPolicyHandler& operator=(
      const DownloadAutoOpenPolicyHandler&) = delete;
~~~

### CheckPolicySettings

DownloadAutoOpenPolicyHandler::CheckPolicySettings
~~~cpp
bool CheckPolicySettings(const policy::PolicyMap& policies,
                           policy::PolicyErrorMap* errors) override;
~~~
 SchemaValidatingPolicyHandler:
### ApplyPolicySettings

DownloadAutoOpenPolicyHandler::ApplyPolicySettings
~~~cpp
void ApplyPolicySettings(const policy::PolicyMap& policies,
                           PrefValueMap* prefs) override;
~~~

### DownloadAutoOpenPolicyHandler

DownloadAutoOpenPolicyHandler
~~~cpp
DownloadAutoOpenPolicyHandler(const DownloadAutoOpenPolicyHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadAutoOpenPolicyHandler& operator=(
      const DownloadAutoOpenPolicyHandler&) = delete;
~~~

### CheckPolicySettings

DownloadAutoOpenPolicyHandler::CheckPolicySettings
~~~cpp
bool CheckPolicySettings(const policy::PolicyMap& policies,
                           policy::PolicyErrorMap* errors) override;
~~~
 SchemaValidatingPolicyHandler:
### ApplyPolicySettings

DownloadAutoOpenPolicyHandler::ApplyPolicySettings
~~~cpp
void ApplyPolicySettings(const policy::PolicyMap& policies,
                           PrefValueMap* prefs) override;
~~~
