
## class OfflineItemModel
 Implementation of DownloadUIModel that wrappers around a |OfflineItem|.

### Wrap

OfflineItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(OfflineItemModelManager* manager,
                                 const OfflineItem& offline_item);
~~~

### Wrap

OfflineItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(
      OfflineItemModelManager* manager,
      const OfflineItem& offline_item,
      std::unique_ptr<DownloadUIModel::StatusTextBuilderBase>
          status_text_builder);
~~~

### OfflineItemModel

OfflineItemModel::OfflineItemModel
~~~cpp
OfflineItemModel(OfflineItemModelManager* manager,
                   const OfflineItem& offline_item);
~~~
 Constructs a OfflineItemModel.

### OfflineItemModel

OfflineItemModel::OfflineItemModel
~~~cpp
OfflineItemModel(OfflineItemModelManager* manager,
                   const OfflineItem& offline_item,
                   std::unique_ptr<DownloadUIModel::StatusTextBuilderBase>
                       status_text_builder);
~~~

### OfflineItemModel

OfflineItemModel
~~~cpp
OfflineItemModel(const OfflineItemModel&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemModel& operator=(const OfflineItemModel&) = delete;
~~~

### ~OfflineItemModel

OfflineItemModel::~OfflineItemModel
~~~cpp
~OfflineItemModel() override;
~~~

### profile

OfflineItemModel::profile
~~~cpp
Profile* profile() const override;
~~~
 DownloadUIModel implementation.

### GetContentId

OfflineItemModel::GetContentId
~~~cpp
ContentId GetContentId() const override;
~~~

### GetCompletedBytes

OfflineItemModel::GetCompletedBytes
~~~cpp
int64_t GetCompletedBytes() const override;
~~~

### GetTotalBytes

OfflineItemModel::GetTotalBytes
~~~cpp
int64_t GetTotalBytes() const override;
~~~

### PercentComplete

OfflineItemModel::PercentComplete
~~~cpp
int PercentComplete() const override;
~~~

### WasUINotified

OfflineItemModel::WasUINotified
~~~cpp
bool WasUINotified() const override;
~~~

### SetWasUINotified

OfflineItemModel::SetWasUINotified
~~~cpp
void SetWasUINotified(bool should_notify) override;
~~~

### WasActionedOn

OfflineItemModel::WasActionedOn
~~~cpp
bool WasActionedOn() const override;
~~~

### SetActionedOn

OfflineItemModel::SetActionedOn
~~~cpp
void SetActionedOn(bool actioned_on) override;
~~~

### GetFileNameToReportUser

OfflineItemModel::GetFileNameToReportUser
~~~cpp
base::FilePath GetFileNameToReportUser() const override;
~~~

### GetTargetFilePath

OfflineItemModel::GetTargetFilePath
~~~cpp
base::FilePath GetTargetFilePath() const override;
~~~

### OpenDownload

OfflineItemModel::OpenDownload
~~~cpp
void OpenDownload() override;
~~~

### Pause

OfflineItemModel::Pause
~~~cpp
void Pause() override;
~~~

### Resume

OfflineItemModel::Resume
~~~cpp
void Resume() override;
~~~

### Cancel

OfflineItemModel::Cancel
~~~cpp
void Cancel(bool user_cancel) override;
~~~

### Remove

OfflineItemModel::Remove
~~~cpp
void Remove() override;
~~~

### GetState

OfflineItemModel::GetState
~~~cpp
download::DownloadItem::DownloadState GetState() const override;
~~~

### IsPaused

OfflineItemModel::IsPaused
~~~cpp
bool IsPaused() const override;
~~~

### TimeRemaining

OfflineItemModel::TimeRemaining
~~~cpp
bool TimeRemaining(base::TimeDelta* remaining) const override;
~~~

### GetStartTime

OfflineItemModel::GetStartTime
~~~cpp
base::Time GetStartTime() const override;
~~~

### GetEndTime

OfflineItemModel::GetEndTime
~~~cpp
base::Time GetEndTime() const override;
~~~

### IsDone

OfflineItemModel::IsDone
~~~cpp
bool IsDone() const override;
~~~

### GetFullPath

OfflineItemModel::GetFullPath
~~~cpp
base::FilePath GetFullPath() const override;
~~~

### CanResume

OfflineItemModel::CanResume
~~~cpp
bool CanResume() const override;
~~~

### AllDataSaved

OfflineItemModel::AllDataSaved
~~~cpp
bool AllDataSaved() const override;
~~~

### GetFileExternallyRemoved

OfflineItemModel::GetFileExternallyRemoved
~~~cpp
bool GetFileExternallyRemoved() const override;
~~~

### GetURL

OfflineItemModel::GetURL
~~~cpp
GURL GetURL() const override;
~~~

### ShouldRemoveFromShelfWhenComplete

OfflineItemModel::ShouldRemoveFromShelfWhenComplete
~~~cpp
bool ShouldRemoveFromShelfWhenComplete() const override;
~~~

### GetLastFailState

OfflineItemModel::GetLastFailState
~~~cpp
offline_items_collection::FailState GetLastFailState() const override;
~~~

### GetOriginalURL

OfflineItemModel::GetOriginalURL
~~~cpp
GURL GetOriginalURL() const override;
~~~

### ShouldPromoteOrigin

OfflineItemModel::ShouldPromoteOrigin
~~~cpp
bool ShouldPromoteOrigin() const override;
~~~

### IsCommandEnabled

OfflineItemModel::IsCommandEnabled
~~~cpp
bool IsCommandEnabled(const DownloadCommands* download_commands,
                        DownloadCommands::Command command) const override;
~~~

### IsCommandChecked

OfflineItemModel::IsCommandChecked
~~~cpp
bool IsCommandChecked(const DownloadCommands* download_commands,
                        DownloadCommands::Command command) const override;
~~~

### ExecuteCommand

OfflineItemModel::ExecuteCommand
~~~cpp
void ExecuteCommand(DownloadCommands* download_commands,
                      DownloadCommands::Command command) override;
~~~

### GetProvider

OfflineItemModel::GetProvider
~~~cpp
OfflineContentProvider* GetProvider() const;
~~~

### OnItemRemoved

OfflineItemModel::OnItemRemoved
~~~cpp
void OnItemRemoved(const ContentId& id) override;
~~~
 FilteredOfflineItemObserver::Observer overrides.

### OnItemUpdated

OfflineItemModel::OnItemUpdated
~~~cpp
void OnItemUpdated(const OfflineItem& item,
                     const absl::optional<UpdateDelta>& update_delta) override;
~~~

### GetMimeType

OfflineItemModel::GetMimeType
~~~cpp
std::string GetMimeType() const override;
~~~
 DownloadUIModel implementation.

### manager_



~~~cpp

raw_ptr<OfflineItemModelManager> manager_;

~~~


### offline_item_observer_



~~~cpp

std::unique_ptr<FilteredOfflineItemObserver> offline_item_observer_;

~~~


### offline_item_



~~~cpp

std::unique_ptr<OfflineItem> offline_item_;

~~~


### OfflineItemModel

OfflineItemModel
~~~cpp
OfflineItemModel(const OfflineItemModel&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemModel& operator=(const OfflineItemModel&) = delete;
~~~

### Wrap

OfflineItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(OfflineItemModelManager* manager,
                                 const OfflineItem& offline_item);
~~~

### Wrap

OfflineItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(
      OfflineItemModelManager* manager,
      const OfflineItem& offline_item,
      std::unique_ptr<DownloadUIModel::StatusTextBuilderBase>
          status_text_builder);
~~~

### profile

OfflineItemModel::profile
~~~cpp
Profile* profile() const override;
~~~
 DownloadUIModel implementation.

### GetContentId

OfflineItemModel::GetContentId
~~~cpp
ContentId GetContentId() const override;
~~~

### GetCompletedBytes

OfflineItemModel::GetCompletedBytes
~~~cpp
int64_t GetCompletedBytes() const override;
~~~

### GetTotalBytes

OfflineItemModel::GetTotalBytes
~~~cpp
int64_t GetTotalBytes() const override;
~~~

### PercentComplete

OfflineItemModel::PercentComplete
~~~cpp
int PercentComplete() const override;
~~~

### WasUINotified

OfflineItemModel::WasUINotified
~~~cpp
bool WasUINotified() const override;
~~~

### SetWasUINotified

OfflineItemModel::SetWasUINotified
~~~cpp
void SetWasUINotified(bool should_notify) override;
~~~

### WasActionedOn

OfflineItemModel::WasActionedOn
~~~cpp
bool WasActionedOn() const override;
~~~

### SetActionedOn

OfflineItemModel::SetActionedOn
~~~cpp
void SetActionedOn(bool actioned_on) override;
~~~

### GetFileNameToReportUser

OfflineItemModel::GetFileNameToReportUser
~~~cpp
base::FilePath GetFileNameToReportUser() const override;
~~~

### GetTargetFilePath

OfflineItemModel::GetTargetFilePath
~~~cpp
base::FilePath GetTargetFilePath() const override;
~~~

### OpenDownload

OfflineItemModel::OpenDownload
~~~cpp
void OpenDownload() override;
~~~

### Pause

OfflineItemModel::Pause
~~~cpp
void Pause() override;
~~~

### Resume

OfflineItemModel::Resume
~~~cpp
void Resume() override;
~~~

### Cancel

OfflineItemModel::Cancel
~~~cpp
void Cancel(bool user_cancel) override;
~~~

### Remove

OfflineItemModel::Remove
~~~cpp
void Remove() override;
~~~

### GetState

OfflineItemModel::GetState
~~~cpp
download::DownloadItem::DownloadState GetState() const override;
~~~

### IsPaused

OfflineItemModel::IsPaused
~~~cpp
bool IsPaused() const override;
~~~

### TimeRemaining

OfflineItemModel::TimeRemaining
~~~cpp
bool TimeRemaining(base::TimeDelta* remaining) const override;
~~~

### GetStartTime

OfflineItemModel::GetStartTime
~~~cpp
base::Time GetStartTime() const override;
~~~

### GetEndTime

OfflineItemModel::GetEndTime
~~~cpp
base::Time GetEndTime() const override;
~~~

### IsDone

OfflineItemModel::IsDone
~~~cpp
bool IsDone() const override;
~~~

### GetFullPath

OfflineItemModel::GetFullPath
~~~cpp
base::FilePath GetFullPath() const override;
~~~

### CanResume

OfflineItemModel::CanResume
~~~cpp
bool CanResume() const override;
~~~

### AllDataSaved

OfflineItemModel::AllDataSaved
~~~cpp
bool AllDataSaved() const override;
~~~

### GetFileExternallyRemoved

OfflineItemModel::GetFileExternallyRemoved
~~~cpp
bool GetFileExternallyRemoved() const override;
~~~

### GetURL

OfflineItemModel::GetURL
~~~cpp
GURL GetURL() const override;
~~~

### ShouldRemoveFromShelfWhenComplete

OfflineItemModel::ShouldRemoveFromShelfWhenComplete
~~~cpp
bool ShouldRemoveFromShelfWhenComplete() const override;
~~~

### GetLastFailState

OfflineItemModel::GetLastFailState
~~~cpp
offline_items_collection::FailState GetLastFailState() const override;
~~~

### GetOriginalURL

OfflineItemModel::GetOriginalURL
~~~cpp
GURL GetOriginalURL() const override;
~~~

### ShouldPromoteOrigin

OfflineItemModel::ShouldPromoteOrigin
~~~cpp
bool ShouldPromoteOrigin() const override;
~~~

### GetProvider

OfflineItemModel::GetProvider
~~~cpp
OfflineContentProvider* GetProvider() const;
~~~

### OnItemRemoved

OfflineItemModel::OnItemRemoved
~~~cpp
void OnItemRemoved(const ContentId& id) override;
~~~
 FilteredOfflineItemObserver::Observer overrides.

### OnItemUpdated

OfflineItemModel::OnItemUpdated
~~~cpp
void OnItemUpdated(const OfflineItem& item,
                     const absl::optional<UpdateDelta>& update_delta) override;
~~~

### GetMimeType

OfflineItemModel::GetMimeType
~~~cpp
std::string GetMimeType() const override;
~~~
 DownloadUIModel implementation.

### manager_



~~~cpp

raw_ptr<OfflineItemModelManager> manager_;

~~~


### offline_item_observer_



~~~cpp

std::unique_ptr<FilteredOfflineItemObserver> offline_item_observer_;

~~~


### offline_item_



~~~cpp

std::unique_ptr<OfflineItem> offline_item_;

~~~

