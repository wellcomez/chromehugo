
## class TrustedSourcesManager
 Identifies if a URL is from a trusted source.

### TrustedSourcesManager

TrustedSourcesManager
~~~cpp
TrustedSourcesManager(const TrustedSourcesManager&) = delete;
~~~

### operator=

operator=
~~~cpp
TrustedSourcesManager& operator=(const TrustedSourcesManager&) = delete;
~~~

### ~TrustedSourcesManager

TrustedSourcesManager::~TrustedSourcesManager
~~~cpp
virtual ~TrustedSourcesManager();
~~~

### Create

TrustedSourcesManager::Create
~~~cpp
static std::unique_ptr<TrustedSourcesManager> Create();
~~~
 Creates a platform-dependent instance of TrustedSourcesManager.


 A trusted sources manager has a list of sources that can be trusted with
 downloads, extracted from the kTrustedDownloadSources command line switch.

 An example usage is to specify that files downloaded from trusted sites
 don't need to be scanned by SafeBrowsing when the
 SafeBrowsingForTrustedSourcesEnabled policy is set to false.


 On creation the list of trusted sources is NULL.


 If the platform is Windows, the kTrustedDownloadSources value is ignored,
 the security zone mapping is used instead to determine whether the source
 is trusted or not.


### IsFromTrustedSource

TrustedSourcesManager::IsFromTrustedSource
~~~cpp
virtual bool IsFromTrustedSource(const GURL& url) const;
~~~
 Returns true if the source of this URL is part of the trusted sources.

### TrustedSourcesManager

TrustedSourcesManager::TrustedSourcesManager
~~~cpp
TrustedSourcesManager();
~~~
 Must use Create.

### matcher_



~~~cpp

net::SchemeHostPortMatcher matcher_;

~~~


### TrustedSourcesManager

TrustedSourcesManager
~~~cpp
TrustedSourcesManager(const TrustedSourcesManager&) = delete;
~~~

### operator=

operator=
~~~cpp
TrustedSourcesManager& operator=(const TrustedSourcesManager&) = delete;
~~~

### Create

TrustedSourcesManager::Create
~~~cpp
static std::unique_ptr<TrustedSourcesManager> Create();
~~~
 Creates a platform-dependent instance of TrustedSourcesManager.


 A trusted sources manager has a list of sources that can be trusted with
 downloads, extracted from the kTrustedDownloadSources command line switch.

 An example usage is to specify that files downloaded from trusted sites
 don't need to be scanned by SafeBrowsing when the
 SafeBrowsingForTrustedSourcesEnabled policy is set to false.


 On creation the list of trusted sources is NULL.


 If the platform is Windows, the kTrustedDownloadSources value is ignored,
 the security zone mapping is used instead to determine whether the source
 is trusted or not.


### IsFromTrustedSource

TrustedSourcesManager::IsFromTrustedSource
~~~cpp
virtual bool IsFromTrustedSource(const GURL& url) const;
~~~
 Returns true if the source of this URL is part of the trusted sources.

### matcher_



~~~cpp

net::SchemeHostPortMatcher matcher_;

~~~

