
## class ChromeDownloadManagerDelegate
 This is the Chrome side helper for the download system.

### ChromeDownloadManagerDelegate

ChromeDownloadManagerDelegate::ChromeDownloadManagerDelegate
~~~cpp
explicit ChromeDownloadManagerDelegate(Profile* profile);
~~~

### ChromeDownloadManagerDelegate

ChromeDownloadManagerDelegate
~~~cpp
ChromeDownloadManagerDelegate(const ChromeDownloadManagerDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeDownloadManagerDelegate& operator=(
      const ChromeDownloadManagerDelegate&) = delete;
~~~

### ~ChromeDownloadManagerDelegate

ChromeDownloadManagerDelegate::~ChromeDownloadManagerDelegate
~~~cpp
~ChromeDownloadManagerDelegate() override;
~~~

### DisableSafeBrowsing

ChromeDownloadManagerDelegate::DisableSafeBrowsing
~~~cpp
static void DisableSafeBrowsing(download::DownloadItem* item);
~~~
 Should be called before the first call to ShouldCompleteDownload() to
 disable SafeBrowsing checks for |item|.

### IsDangerTypeBlocked

ChromeDownloadManagerDelegate::IsDangerTypeBlocked
~~~cpp
static bool IsDangerTypeBlocked(download::DownloadDangerType danger_type);
~~~
 True when |danger_type| is one that is blocked for policy reasons (e.g.

 "file too large") as opposed to malicious content reasons.

### SetDownloadManager

ChromeDownloadManagerDelegate::SetDownloadManager
~~~cpp
void SetDownloadManager(content::DownloadManager* dm);
~~~

### ShowDownloadDialog

ChromeDownloadManagerDelegate::ShowDownloadDialog
~~~cpp
void ShowDownloadDialog(gfx::NativeWindow native_window,
                          int64_t total_bytes,
                          DownloadLocationDialogType dialog_type,
                          const base::FilePath& suggested_path,
                          DownloadDialogBridge::DialogCallback callback);
~~~

### SetDownloadDialogBridgeForTesting

ChromeDownloadManagerDelegate::SetDownloadDialogBridgeForTesting
~~~cpp
void SetDownloadDialogBridgeForTesting(DownloadDialogBridge* bridge);
~~~

### GetDownloadIdReceiverCallback

ChromeDownloadManagerDelegate::GetDownloadIdReceiverCallback
~~~cpp
content::DownloadIdCallback GetDownloadIdReceiverCallback();
~~~
 Callbacks passed to GetNextId() will not be called until the returned
 callback is called.

### Shutdown

ChromeDownloadManagerDelegate::Shutdown
~~~cpp
void Shutdown() override;
~~~
 content::DownloadManagerDelegate
### GetNextId

ChromeDownloadManagerDelegate::GetNextId
~~~cpp
void GetNextId(content::DownloadIdCallback callback) override;
~~~

### DetermineDownloadTarget

ChromeDownloadManagerDelegate::DetermineDownloadTarget
~~~cpp
bool DetermineDownloadTarget(
      download::DownloadItem* item,
      content::DownloadTargetCallback* callback) override;
~~~

### ShouldAutomaticallyOpenFile

ChromeDownloadManagerDelegate::ShouldAutomaticallyOpenFile
~~~cpp
bool ShouldAutomaticallyOpenFile(const GURL& url,
                                   const base::FilePath& path) override;
~~~

### ShouldAutomaticallyOpenFileByPolicy

ChromeDownloadManagerDelegate::ShouldAutomaticallyOpenFileByPolicy
~~~cpp
bool ShouldAutomaticallyOpenFileByPolicy(const GURL& url,
                                           const base::FilePath& path) override;
~~~

### ShouldCompleteDownload

ChromeDownloadManagerDelegate::ShouldCompleteDownload
~~~cpp
bool ShouldCompleteDownload(download::DownloadItem* item,
                              base::OnceClosure complete_callback) override;
~~~

### ShouldOpenDownload

ChromeDownloadManagerDelegate::ShouldOpenDownload
~~~cpp
bool ShouldOpenDownload(
      download::DownloadItem* item,
      content::DownloadOpenDelayedCallback callback) override;
~~~

### InterceptDownloadIfApplicable

ChromeDownloadManagerDelegate::InterceptDownloadIfApplicable
~~~cpp
bool InterceptDownloadIfApplicable(
      const GURL& url,
      const std::string& user_agent,
      const std::string& content_disposition,
      const std::string& mime_type,
      const std::string& request_origin,
      int64_t content_length,
      bool is_transient,
      content::WebContents* web_contents) override;
~~~

### GetSaveDir

ChromeDownloadManagerDelegate::GetSaveDir
~~~cpp
void GetSaveDir(content::BrowserContext* browser_context,
                  base::FilePath* website_save_dir,
                  base::FilePath* download_save_dir) override;
~~~

### ChooseSavePath

ChromeDownloadManagerDelegate::ChooseSavePath
~~~cpp
void ChooseSavePath(content::WebContents* web_contents,
                      const base::FilePath& suggested_path,
                      const base::FilePath::StringType& default_extension,
                      bool can_save_as_complete,
                      content::SavePackagePathPickedCallback callback) override;
~~~

### SanitizeSavePackageResourceName

ChromeDownloadManagerDelegate::SanitizeSavePackageResourceName
~~~cpp
void SanitizeSavePackageResourceName(base::FilePath* filename,
                                       const GURL& source_url) override;
~~~

### SanitizeDownloadParameters

ChromeDownloadManagerDelegate::SanitizeDownloadParameters
~~~cpp
void SanitizeDownloadParameters(
      download::DownloadUrlParameters* params) override;
~~~

### OpenDownload

ChromeDownloadManagerDelegate::OpenDownload
~~~cpp
void OpenDownload(download::DownloadItem* download) override;
~~~

### ShowDownloadInShell

ChromeDownloadManagerDelegate::ShowDownloadInShell
~~~cpp
void ShowDownloadInShell(download::DownloadItem* download) override;
~~~

### ApplicationClientIdForFileScanning

ChromeDownloadManagerDelegate::ApplicationClientIdForFileScanning
~~~cpp
std::string ApplicationClientIdForFileScanning() override;
~~~

### CheckDownloadAllowed

ChromeDownloadManagerDelegate::CheckDownloadAllowed
~~~cpp
void CheckDownloadAllowed(
      const content::WebContents::Getter& web_contents_getter,
      const GURL& url,
      const std::string& request_method,
      absl::optional<url::Origin> request_initiator,
      bool from_download_cross_origin_redirect,
      bool content_initiated,
      content::CheckDownloadAllowedCallback check_download_allowed_cb) override;
~~~

### GetQuarantineConnectionCallback

ChromeDownloadManagerDelegate::GetQuarantineConnectionCallback
~~~cpp
download::QuarantineConnectionCallback GetQuarantineConnectionCallback()
      override;
~~~

### CheckSavePackageAllowed

ChromeDownloadManagerDelegate::CheckSavePackageAllowed
~~~cpp
void CheckSavePackageAllowed(
      download::DownloadItem* download_item,
      base::flat_map<base::FilePath, base::FilePath> save_package_files,
      content::SavePackageAllowedCallback callback) override;
~~~

### OpenDownloadUsingPlatformHandler

ChromeDownloadManagerDelegate::OpenDownloadUsingPlatformHandler
~~~cpp
void OpenDownloadUsingPlatformHandler(download::DownloadItem* download);
~~~
 Opens a download using the platform handler. DownloadItem::OpenDownload,
 which ends up being handled by OpenDownload(), will open a download in the
 browser if doing so is preferred.

### download_prefs

download_prefs
~~~cpp
DownloadPrefs* download_prefs() { return download_prefs_.get(); }
~~~

###  SafeBrowsingState

 The state of a safebrowsing check.

~~~cpp
class SafeBrowsingState : public DownloadCompletionBlocker {
   public:
    SafeBrowsingState() = default;

    SafeBrowsingState(const SafeBrowsingState&) = delete;
    SafeBrowsingState& operator=(const SafeBrowsingState&) = delete;

    ~SafeBrowsingState() override;

    // String pointer used for identifying safebrowing data associated with
    // a download item.
    static const char kSafeBrowsingUserDataKey[];
  };
~~~
### CheckClientDownloadDone

ChromeDownloadManagerDelegate::CheckClientDownloadDone
~~~cpp
void CheckClientDownloadDone(uint32_t download_id,
                               safe_browsing::DownloadCheckResult result);
~~~
 Callback function after the DownloadProtectionService completes.

### CheckSavePackageScanningDone

ChromeDownloadManagerDelegate::CheckSavePackageScanningDone
~~~cpp
void CheckSavePackageScanningDone(uint32_t download_id,
                                    safe_browsing::DownloadCheckResult result);
~~~
 Callback function after scanning completes for a save package.

### GetWeakPtr

ChromeDownloadManagerDelegate::GetWeakPtr
~~~cpp
base::WeakPtr<ChromeDownloadManagerDelegate> GetWeakPtr();
~~~
 FULL_SAFE_BROWSING
### ConnectToQuarantineService

ChromeDownloadManagerDelegate::ConnectToQuarantineService
~~~cpp
static void ConnectToQuarantineService(
      mojo::PendingReceiver<quarantine::mojom::Quarantine> receiver);
~~~

### ShouldBlockFile

ChromeDownloadManagerDelegate::ShouldBlockFile
~~~cpp
bool ShouldBlockFile(download::DownloadItem* item,
                       download::DownloadDangerType danger_type) const;
~~~
 Return true if the downloaded file should be blocked based on the current
 download restriction pref, the file type, and |danger_type|.

### ScheduleCancelForEphemeralWarning

ChromeDownloadManagerDelegate::ScheduleCancelForEphemeralWarning
~~~cpp
void ScheduleCancelForEphemeralWarning(const std::string& guid);
~~~
 Schedules the ephemeral warning download to be canceled. It will only be
 canceled if it continues to be an ephemeral warning that hasn't been acted
 on when the scheduled time arrives.

### IsOpenInBrowserPreferreredForFile

ChromeDownloadManagerDelegate::IsOpenInBrowserPreferreredForFile
~~~cpp
virtual bool IsOpenInBrowserPreferreredForFile(const base::FilePath& path);
~~~
 Returns true if |path| should open in the browser.

### GetDownloadProtectionService

ChromeDownloadManagerDelegate::GetDownloadProtectionService
~~~cpp
virtual safe_browsing::DownloadProtectionService*
      GetDownloadProtectionService();
~~~

### ShowFilePickerForDownload

ChromeDownloadManagerDelegate::ShowFilePickerForDownload
~~~cpp
virtual void ShowFilePickerForDownload(
      download::DownloadItem* download,
      const base::FilePath& suggested_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback);
~~~
 Show file picker for |download|.

### GetInsecureDownloadStatus

ChromeDownloadManagerDelegate::GetInsecureDownloadStatus
~~~cpp
void GetInsecureDownloadStatus(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      GetInsecureDownloadStatusCallback callback) override;
~~~
 DownloadTargetDeterminerDelegate. Protected for testing.

### NotifyExtensions

ChromeDownloadManagerDelegate::NotifyExtensions
~~~cpp
void NotifyExtensions(download::DownloadItem* download,
                        const base::FilePath& suggested_virtual_path,
                        NotifyExtensionsCallback callback) override;
~~~

### ReserveVirtualPath

ChromeDownloadManagerDelegate::ReserveVirtualPath
~~~cpp
void ReserveVirtualPath(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      bool create_directory,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      ReservedPathCallback callback) override;
~~~

### RequestIncognitoWarningConfirmation

ChromeDownloadManagerDelegate::RequestIncognitoWarningConfirmation
~~~cpp
void RequestIncognitoWarningConfirmation(
      IncognitoWarningConfirmationCallback) override;
~~~

### RequestConfirmation

ChromeDownloadManagerDelegate::RequestConfirmation
~~~cpp
void RequestConfirmation(download::DownloadItem* download,
                           const base::FilePath& suggested_virtual_path,
                           DownloadConfirmationReason reason,
                           ConfirmationCallback callback) override;
~~~

### DetermineLocalPath

ChromeDownloadManagerDelegate::DetermineLocalPath
~~~cpp
void DetermineLocalPath(download::DownloadItem* download,
                          const base::FilePath& virtual_path,
                          download::LocalPathCallback callback) override;
~~~

### CheckDownloadUrl

ChromeDownloadManagerDelegate::CheckDownloadUrl
~~~cpp
void CheckDownloadUrl(download::DownloadItem* download,
                        const base::FilePath& suggested_virtual_path,
                        CheckDownloadUrlCallback callback) override;
~~~

### GetFileMimeType

ChromeDownloadManagerDelegate::GetFileMimeType
~~~cpp
void GetFileMimeType(const base::FilePath& path,
                       GetFileMimeTypeCallback callback) override;
~~~

### OnDownloadCanceled

ChromeDownloadManagerDelegate::OnDownloadCanceled
~~~cpp
virtual void OnDownloadCanceled(download::DownloadItem* download,
                                  bool has_no_external_storage);
~~~

### OnConfirmationCallbackComplete

ChromeDownloadManagerDelegate::OnConfirmationCallbackComplete
~~~cpp
void OnConfirmationCallbackComplete(
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      DownloadConfirmationResult result,
      const base::FilePath& virtual_path);
~~~
 Called when the file picker returns the confirmation result.

### download_manager_



~~~cpp

raw_ptr<content::DownloadManager> download_manager_ = nullptr;

~~~

 So that test classes that inherit from this for override purposes
 can call back into the DownloadManager.

### FRIEND_TEST_ALL_PREFIXES

ChromeDownloadManagerDelegate::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(ChromeDownloadManagerDelegateTest,
                           RequestConfirmation_Android);
~~~

### FRIEND_TEST_ALL_PREFIXES

ChromeDownloadManagerDelegate::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(ChromeDownloadManagerDelegateTest,
                           CancelAllEphemeralWarnings);
~~~

### ShowFilePicker

ChromeDownloadManagerDelegate::ShowFilePicker
~~~cpp
void ShowFilePicker(
      const std::string& guid,
      const base::FilePath& suggested_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback);
~~~
 Called to show a file picker for download with |guid|
### OnInstallerDone

ChromeDownloadManagerDelegate::OnInstallerDone
~~~cpp
void OnInstallerDone(
      const base::UnguessableToken& token,
      content::DownloadOpenDelayedCallback callback,
      const absl::optional<extensions::CrxInstallError>& error);
~~~
 Called when CrxInstaller in running_crx_installs_ finishes installation.

### IsDownloadReadyForCompletion

ChromeDownloadManagerDelegate::IsDownloadReadyForCompletion
~~~cpp
bool IsDownloadReadyForCompletion(
      download::DownloadItem* item,
      base::OnceClosure internal_complete_callback);
~~~
 Internal gateways for ShouldCompleteDownload().

### ShouldCompleteDownloadInternal

ChromeDownloadManagerDelegate::ShouldCompleteDownloadInternal
~~~cpp
void ShouldCompleteDownloadInternal(uint32_t download_id,
                                      base::OnceClosure user_complete_callback);
~~~

### SetNextId

ChromeDownloadManagerDelegate::SetNextId
~~~cpp
void SetNextId(uint32_t id);
~~~
 Sets the next download id based on download database records, and runs all
 cached id callbacks.

### ReturnNextId

ChromeDownloadManagerDelegate::ReturnNextId
~~~cpp
void ReturnNextId(content::DownloadIdCallback callback);
~~~
 Runs the |callback| with next id. Results in the download being started.

### OnDownloadTargetDetermined

ChromeDownloadManagerDelegate::OnDownloadTargetDetermined
~~~cpp
void OnDownloadTargetDetermined(
      uint32_t download_id,
      content::DownloadTargetCallback callback,
      std::unique_ptr<DownloadTargetInfo> target_info);
~~~

### MaybeSendDangerousDownloadOpenedReport

ChromeDownloadManagerDelegate::MaybeSendDangerousDownloadOpenedReport
~~~cpp
void MaybeSendDangerousDownloadOpenedReport(download::DownloadItem* download,
                                              bool show_download_in_folder);
~~~

### OnCheckDownloadAllowedComplete

ChromeDownloadManagerDelegate::OnCheckDownloadAllowedComplete
~~~cpp
void OnCheckDownloadAllowedComplete(
      content::CheckDownloadAllowedCallback check_download_allowed_cb,
      bool storage_permission_granted,
      bool allow);
~~~

### IsMostRecentDownloadItemAtFilePath

ChromeDownloadManagerDelegate::IsMostRecentDownloadItemAtFilePath
~~~cpp
bool IsMostRecentDownloadItemAtFilePath(download::DownloadItem* download);
~~~
 Returns whether this is the most recent download in the rare event where
 multiple downloads are associated with the same file path.

### CancelForEphemeralWarning

ChromeDownloadManagerDelegate::CancelForEphemeralWarning
~~~cpp
void CancelForEphemeralWarning(const std::string& guid);
~~~
 Cancels a download if it's still an ephemeral warning (and has not been
 acted on by the user).

### CancelAllEphemeralWarnings

ChromeDownloadManagerDelegate::CancelAllEphemeralWarnings
~~~cpp
void CancelAllEphemeralWarnings();
~~~
 If the browser doesn't shut down cleanly, there can be ephemeral warnings
 that were not cleaned up. This function cleans them up on startup, when the
 download manager is initialized.

### OnManagerInitialized

ChromeDownloadManagerDelegate::OnManagerInitialized
~~~cpp
void OnManagerInitialized() override;
~~~
 content::DownloadManager::Observer
### GenerateUniqueFileNameDone

ChromeDownloadManagerDelegate::GenerateUniqueFileNameDone
~~~cpp
void GenerateUniqueFileNameDone(
      gfx::NativeWindow native_window,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      download::PathValidationResult result,
      const base::FilePath& target_path);
~~~
 Called after a unique file name is generated in the case that there is a
 TARGET_CONFLICT and the new file name should be displayed to the user.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### download_dialog_bridge_



~~~cpp

std::unique_ptr<DownloadDialogBridge> download_dialog_bridge_;

~~~


### download_message_bridge_



~~~cpp

std::unique_ptr<DownloadMessageBridge> download_message_bridge_;

~~~


### next_download_id_



~~~cpp

uint32_t next_download_id_;

~~~

 If history database fails to initialize, this will always be kInvalidId.

 Otherwise, the first available download id is assigned from history
 database, and incremented by one for each download.

### next_id_retrieved_



~~~cpp

bool next_id_retrieved_;

~~~

 Whether |next_download_id_| is retrieved from history db.

### id_callbacks_



~~~cpp

IdCallbackVector id_callbacks_;

~~~

 The |GetNextId| callbacks that may be cached before loading the download
 database.

### download_prefs_



~~~cpp

std::unique_ptr<DownloadPrefs> download_prefs_;

~~~


### running_crx_installs_



~~~cpp

std::map<base::UnguessableToken, scoped_refptr<extensions::CrxInstaller>>
      running_crx_installs_;

~~~

 CRX installs that are currently in progress.

### file_picker_callbacks_



~~~cpp

std::deque<base::OnceClosure> file_picker_callbacks_;

~~~

 Outstanding callbacks to open file selection dialog.

### is_file_picker_showing_



~~~cpp

bool is_file_picker_showing_;

~~~

 Whether a file picker dialog is showing.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<ChromeDownloadManagerDelegate> weak_ptr_factory_{this};

~~~


### ChromeDownloadManagerDelegate

ChromeDownloadManagerDelegate
~~~cpp
ChromeDownloadManagerDelegate(const ChromeDownloadManagerDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeDownloadManagerDelegate& operator=(
      const ChromeDownloadManagerDelegate&) = delete;
~~~

### download_prefs

download_prefs
~~~cpp
DownloadPrefs* download_prefs() { return download_prefs_.get(); }
~~~

### DisableSafeBrowsing

ChromeDownloadManagerDelegate::DisableSafeBrowsing
~~~cpp
static void DisableSafeBrowsing(download::DownloadItem* item);
~~~
 Should be called before the first call to ShouldCompleteDownload() to
 disable SafeBrowsing checks for |item|.

### IsDangerTypeBlocked

ChromeDownloadManagerDelegate::IsDangerTypeBlocked
~~~cpp
static bool IsDangerTypeBlocked(download::DownloadDangerType danger_type);
~~~
 True when |danger_type| is one that is blocked for policy reasons (e.g.

 "file too large") as opposed to malicious content reasons.

### SetDownloadManager

ChromeDownloadManagerDelegate::SetDownloadManager
~~~cpp
void SetDownloadManager(content::DownloadManager* dm);
~~~

### GetDownloadIdReceiverCallback

ChromeDownloadManagerDelegate::GetDownloadIdReceiverCallback
~~~cpp
content::DownloadIdCallback GetDownloadIdReceiverCallback();
~~~
 Callbacks passed to GetNextId() will not be called until the returned
 callback is called.

### Shutdown

ChromeDownloadManagerDelegate::Shutdown
~~~cpp
void Shutdown() override;
~~~
 content::DownloadManagerDelegate
### GetNextId

ChromeDownloadManagerDelegate::GetNextId
~~~cpp
void GetNextId(content::DownloadIdCallback callback) override;
~~~

### DetermineDownloadTarget

ChromeDownloadManagerDelegate::DetermineDownloadTarget
~~~cpp
bool DetermineDownloadTarget(
      download::DownloadItem* item,
      content::DownloadTargetCallback* callback) override;
~~~

### ShouldAutomaticallyOpenFile

ChromeDownloadManagerDelegate::ShouldAutomaticallyOpenFile
~~~cpp
bool ShouldAutomaticallyOpenFile(const GURL& url,
                                   const base::FilePath& path) override;
~~~

### ShouldAutomaticallyOpenFileByPolicy

ChromeDownloadManagerDelegate::ShouldAutomaticallyOpenFileByPolicy
~~~cpp
bool ShouldAutomaticallyOpenFileByPolicy(const GURL& url,
                                           const base::FilePath& path) override;
~~~

### ShouldCompleteDownload

ChromeDownloadManagerDelegate::ShouldCompleteDownload
~~~cpp
bool ShouldCompleteDownload(download::DownloadItem* item,
                              base::OnceClosure complete_callback) override;
~~~

### ShouldOpenDownload

ChromeDownloadManagerDelegate::ShouldOpenDownload
~~~cpp
bool ShouldOpenDownload(
      download::DownloadItem* item,
      content::DownloadOpenDelayedCallback callback) override;
~~~

### InterceptDownloadIfApplicable

ChromeDownloadManagerDelegate::InterceptDownloadIfApplicable
~~~cpp
bool InterceptDownloadIfApplicable(
      const GURL& url,
      const std::string& user_agent,
      const std::string& content_disposition,
      const std::string& mime_type,
      const std::string& request_origin,
      int64_t content_length,
      bool is_transient,
      content::WebContents* web_contents) override;
~~~

### GetSaveDir

ChromeDownloadManagerDelegate::GetSaveDir
~~~cpp
void GetSaveDir(content::BrowserContext* browser_context,
                  base::FilePath* website_save_dir,
                  base::FilePath* download_save_dir) override;
~~~

### ChooseSavePath

ChromeDownloadManagerDelegate::ChooseSavePath
~~~cpp
void ChooseSavePath(content::WebContents* web_contents,
                      const base::FilePath& suggested_path,
                      const base::FilePath::StringType& default_extension,
                      bool can_save_as_complete,
                      content::SavePackagePathPickedCallback callback) override;
~~~

### SanitizeSavePackageResourceName

ChromeDownloadManagerDelegate::SanitizeSavePackageResourceName
~~~cpp
void SanitizeSavePackageResourceName(base::FilePath* filename,
                                       const GURL& source_url) override;
~~~

### SanitizeDownloadParameters

ChromeDownloadManagerDelegate::SanitizeDownloadParameters
~~~cpp
void SanitizeDownloadParameters(
      download::DownloadUrlParameters* params) override;
~~~

### OpenDownload

ChromeDownloadManagerDelegate::OpenDownload
~~~cpp
void OpenDownload(download::DownloadItem* download) override;
~~~

### ShowDownloadInShell

ChromeDownloadManagerDelegate::ShowDownloadInShell
~~~cpp
void ShowDownloadInShell(download::DownloadItem* download) override;
~~~

### ApplicationClientIdForFileScanning

ChromeDownloadManagerDelegate::ApplicationClientIdForFileScanning
~~~cpp
std::string ApplicationClientIdForFileScanning() override;
~~~

### CheckDownloadAllowed

ChromeDownloadManagerDelegate::CheckDownloadAllowed
~~~cpp
void CheckDownloadAllowed(
      const content::WebContents::Getter& web_contents_getter,
      const GURL& url,
      const std::string& request_method,
      absl::optional<url::Origin> request_initiator,
      bool from_download_cross_origin_redirect,
      bool content_initiated,
      content::CheckDownloadAllowedCallback check_download_allowed_cb) override;
~~~

### GetQuarantineConnectionCallback

ChromeDownloadManagerDelegate::GetQuarantineConnectionCallback
~~~cpp
download::QuarantineConnectionCallback GetQuarantineConnectionCallback()
      override;
~~~

### CheckSavePackageAllowed

ChromeDownloadManagerDelegate::CheckSavePackageAllowed
~~~cpp
void CheckSavePackageAllowed(
      download::DownloadItem* download_item,
      base::flat_map<base::FilePath, base::FilePath> save_package_files,
      content::SavePackageAllowedCallback callback) override;
~~~

### OpenDownloadUsingPlatformHandler

ChromeDownloadManagerDelegate::OpenDownloadUsingPlatformHandler
~~~cpp
void OpenDownloadUsingPlatformHandler(download::DownloadItem* download);
~~~
 Opens a download using the platform handler. DownloadItem::OpenDownload,
 which ends up being handled by OpenDownload(), will open a download in the
 browser if doing so is preferred.

### GetWeakPtr

ChromeDownloadManagerDelegate::GetWeakPtr
~~~cpp
base::WeakPtr<ChromeDownloadManagerDelegate> GetWeakPtr();
~~~
 FULL_SAFE_BROWSING
### ConnectToQuarantineService

ChromeDownloadManagerDelegate::ConnectToQuarantineService
~~~cpp
static void ConnectToQuarantineService(
      mojo::PendingReceiver<quarantine::mojom::Quarantine> receiver);
~~~

### ShouldBlockFile

ChromeDownloadManagerDelegate::ShouldBlockFile
~~~cpp
bool ShouldBlockFile(download::DownloadItem* item,
                       download::DownloadDangerType danger_type) const;
~~~
 Return true if the downloaded file should be blocked based on the current
 download restriction pref, the file type, and |danger_type|.

### IsOpenInBrowserPreferreredForFile

ChromeDownloadManagerDelegate::IsOpenInBrowserPreferreredForFile
~~~cpp
virtual bool IsOpenInBrowserPreferreredForFile(const base::FilePath& path);
~~~
 Returns true if |path| should open in the browser.

### ShowFilePickerForDownload

ChromeDownloadManagerDelegate::ShowFilePickerForDownload
~~~cpp
virtual void ShowFilePickerForDownload(
      download::DownloadItem* download,
      const base::FilePath& suggested_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback);
~~~
 Show file picker for |download|.

### GetInsecureDownloadStatus

ChromeDownloadManagerDelegate::GetInsecureDownloadStatus
~~~cpp
void GetInsecureDownloadStatus(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      GetInsecureDownloadStatusCallback callback) override;
~~~
 DownloadTargetDeterminerDelegate. Protected for testing.

### NotifyExtensions

ChromeDownloadManagerDelegate::NotifyExtensions
~~~cpp
void NotifyExtensions(download::DownloadItem* download,
                        const base::FilePath& suggested_virtual_path,
                        NotifyExtensionsCallback callback) override;
~~~

### ReserveVirtualPath

ChromeDownloadManagerDelegate::ReserveVirtualPath
~~~cpp
void ReserveVirtualPath(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      bool create_directory,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      ReservedPathCallback callback) override;
~~~

### RequestConfirmation

ChromeDownloadManagerDelegate::RequestConfirmation
~~~cpp
void RequestConfirmation(download::DownloadItem* download,
                           const base::FilePath& suggested_virtual_path,
                           DownloadConfirmationReason reason,
                           ConfirmationCallback callback) override;
~~~

### DetermineLocalPath

ChromeDownloadManagerDelegate::DetermineLocalPath
~~~cpp
void DetermineLocalPath(download::DownloadItem* download,
                          const base::FilePath& virtual_path,
                          download::LocalPathCallback callback) override;
~~~

### CheckDownloadUrl

ChromeDownloadManagerDelegate::CheckDownloadUrl
~~~cpp
void CheckDownloadUrl(download::DownloadItem* download,
                        const base::FilePath& suggested_virtual_path,
                        CheckDownloadUrlCallback callback) override;
~~~

### GetFileMimeType

ChromeDownloadManagerDelegate::GetFileMimeType
~~~cpp
void GetFileMimeType(const base::FilePath& path,
                       GetFileMimeTypeCallback callback) override;
~~~

### OnConfirmationCallbackComplete

ChromeDownloadManagerDelegate::OnConfirmationCallbackComplete
~~~cpp
void OnConfirmationCallbackComplete(
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      DownloadConfirmationResult result,
      const base::FilePath& virtual_path);
~~~
 Called when the file picker returns the confirmation result.

### download_manager_



~~~cpp

raw_ptr<content::DownloadManager> download_manager_ = nullptr;

~~~

 So that test classes that inherit from this for override purposes
 can call back into the DownloadManager.

### ShowFilePicker

ChromeDownloadManagerDelegate::ShowFilePicker
~~~cpp
void ShowFilePicker(
      const std::string& guid,
      const base::FilePath& suggested_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback);
~~~
 Called to show a file picker for download with |guid|
### IsDownloadReadyForCompletion

ChromeDownloadManagerDelegate::IsDownloadReadyForCompletion
~~~cpp
bool IsDownloadReadyForCompletion(
      download::DownloadItem* item,
      base::OnceClosure internal_complete_callback);
~~~
 Internal gateways for ShouldCompleteDownload().

### ShouldCompleteDownloadInternal

ChromeDownloadManagerDelegate::ShouldCompleteDownloadInternal
~~~cpp
void ShouldCompleteDownloadInternal(uint32_t download_id,
                                      base::OnceClosure user_complete_callback);
~~~

### SetNextId

ChromeDownloadManagerDelegate::SetNextId
~~~cpp
void SetNextId(uint32_t id);
~~~
 Sets the next download id based on download database records, and runs all
 cached id callbacks.

### ReturnNextId

ChromeDownloadManagerDelegate::ReturnNextId
~~~cpp
void ReturnNextId(content::DownloadIdCallback callback);
~~~
 Runs the |callback| with next id. Results in the download being started.

### OnDownloadTargetDetermined

ChromeDownloadManagerDelegate::OnDownloadTargetDetermined
~~~cpp
void OnDownloadTargetDetermined(
      uint32_t download_id,
      content::DownloadTargetCallback callback,
      std::unique_ptr<DownloadTargetInfo> target_info);
~~~

### MaybeSendDangerousDownloadOpenedReport

ChromeDownloadManagerDelegate::MaybeSendDangerousDownloadOpenedReport
~~~cpp
void MaybeSendDangerousDownloadOpenedReport(download::DownloadItem* download,
                                              bool show_download_in_folder);
~~~

### OnCheckDownloadAllowedComplete

ChromeDownloadManagerDelegate::OnCheckDownloadAllowedComplete
~~~cpp
void OnCheckDownloadAllowedComplete(
      content::CheckDownloadAllowedCallback check_download_allowed_cb,
      bool storage_permission_granted,
      bool allow);
~~~

### IsMostRecentDownloadItemAtFilePath

ChromeDownloadManagerDelegate::IsMostRecentDownloadItemAtFilePath
~~~cpp
bool IsMostRecentDownloadItemAtFilePath(download::DownloadItem* download);
~~~
 Returns whether this is the most recent download in the rare event where
 multiple downloads are associated with the same file path.

### OnManagerInitialized

ChromeDownloadManagerDelegate::OnManagerInitialized
~~~cpp
void OnManagerInitialized() override;
~~~
 content::DownloadManager::Observer
### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### next_download_id_



~~~cpp

uint32_t next_download_id_;

~~~

 If history database fails to initialize, this will always be kInvalidId.

 Otherwise, the first available download id is assigned from history
 database, and incremented by one for each download.

### next_id_retrieved_



~~~cpp

bool next_id_retrieved_;

~~~

 Whether |next_download_id_| is retrieved from history db.

### id_callbacks_



~~~cpp

IdCallbackVector id_callbacks_;

~~~

 The |GetNextId| callbacks that may be cached before loading the download
 database.

### download_prefs_



~~~cpp

std::unique_ptr<DownloadPrefs> download_prefs_;

~~~


### file_picker_callbacks_



~~~cpp

std::deque<base::OnceClosure> file_picker_callbacks_;

~~~

 Outstanding callbacks to open file selection dialog.

### is_file_picker_showing_



~~~cpp

bool is_file_picker_showing_;

~~~

 Whether a file picker dialog is showing.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<ChromeDownloadManagerDelegate> weak_ptr_factory_{this};

~~~

