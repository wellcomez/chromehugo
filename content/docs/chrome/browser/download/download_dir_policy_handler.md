
## class DownloadDirPolicyHandler
 namespace policy
 ConfigurationPolicyHandler for the DownloadDirectory policy.

### DownloadDirPolicyHandler

DownloadDirPolicyHandler::DownloadDirPolicyHandler
~~~cpp
DownloadDirPolicyHandler();
~~~

### DownloadDirPolicyHandler

DownloadDirPolicyHandler
~~~cpp
DownloadDirPolicyHandler(const DownloadDirPolicyHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDirPolicyHandler& operator=(const DownloadDirPolicyHandler&) = delete;
~~~

### ~DownloadDirPolicyHandler

DownloadDirPolicyHandler::~DownloadDirPolicyHandler
~~~cpp
~DownloadDirPolicyHandler() override;
~~~

### CheckPolicySettings

DownloadDirPolicyHandler::CheckPolicySettings
~~~cpp
bool CheckPolicySettings(const policy::PolicyMap& policies,
                           policy::PolicyErrorMap* errors) override;
~~~
 ConfigurationPolicyHandler methods:
### ApplyPolicySettingsWithParameters

DownloadDirPolicyHandler::ApplyPolicySettingsWithParameters
~~~cpp
void ApplyPolicySettingsWithParameters(
      const policy::PolicyMap& policies,
      const policy::PolicyHandlerParameters& parameters,
      PrefValueMap* prefs) override;
~~~

### ApplyPolicySettings

DownloadDirPolicyHandler::ApplyPolicySettings
~~~cpp
void ApplyPolicySettings(const policy::PolicyMap& policies,
                           PrefValueMap* prefs) override;
~~~

### DownloadDirPolicyHandler

DownloadDirPolicyHandler
~~~cpp
DownloadDirPolicyHandler(const DownloadDirPolicyHandler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDirPolicyHandler& operator=(const DownloadDirPolicyHandler&) = delete;
~~~

### CheckPolicySettings

DownloadDirPolicyHandler::CheckPolicySettings
~~~cpp
bool CheckPolicySettings(const policy::PolicyMap& policies,
                           policy::PolicyErrorMap* errors) override;
~~~
 ConfigurationPolicyHandler methods:
### ApplyPolicySettingsWithParameters

DownloadDirPolicyHandler::ApplyPolicySettingsWithParameters
~~~cpp
void ApplyPolicySettingsWithParameters(
      const policy::PolicyMap& policies,
      const policy::PolicyHandlerParameters& parameters,
      PrefValueMap* prefs) override;
~~~

### ApplyPolicySettings

DownloadDirPolicyHandler::ApplyPolicySettings
~~~cpp
void ApplyPolicySettings(const policy::PolicyMap& policies,
                           PrefValueMap* prefs) override;
~~~
