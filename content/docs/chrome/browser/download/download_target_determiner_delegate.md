
## class DownloadTargetDeterminerDelegate
 Delegate for DownloadTargetDeterminer. The delegate isn't owned by
 DownloadTargetDeterminer and is expected to outlive it.

### GetInsecureDownloadStatus

GetInsecureDownloadStatus
~~~cpp
virtual void GetInsecureDownloadStatus(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      GetInsecureDownloadStatusCallback callback) = 0;
~~~
 Returns whether the download should be warned/blocked based on its insecure
 download status, and if so, what kind of warning/blocking should be used.

### NotifyExtensions

NotifyExtensions
~~~cpp
virtual void NotifyExtensions(download::DownloadItem* download,
                                const base::FilePath& virtual_path,
                                NotifyExtensionsCallback callback) = 0;
~~~
 Notifies extensions of the impending filename determination. |virtual_path|
 is the current suggested virtual path. The |callback| should be invoked to
 indicate whether any extensions wish to override the path.

### ReserveVirtualPath

ReserveVirtualPath
~~~cpp
virtual void ReserveVirtualPath(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      bool create_directory,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      ReservedPathCallback callback) = 0;
~~~
 Reserve |virtual_path|. This is expected to check the following:
 - Whether |virtual_path| can be written to by the user. If not, the
   |virtual_path| can be changed to writeable path if necessary.

 - If |conflict_action| is UNIQUIFY then |virtual_path| should be
   modified so that the new path is writeable and unique. If
   |conflict_action| is PROMPT, then in the event of a conflict,
   |callback| should be invoked with |success| set to |false| in
   order to force a prompt. |virtual_path| may or may not be
   modified in the latter case.

 - If |create_directory| is true, then the parent directory of
   |virtual_path| should be created if it doesn't exist.


 |callback| should be invoked on completion with the results.

### RequestConfirmation

RequestConfirmation
~~~cpp
virtual void RequestConfirmation(download::DownloadItem* download,
                                   const base::FilePath& virtual_path,
                                   DownloadConfirmationReason reason,
                                   ConfirmationCallback callback) = 0;
~~~
 Display a prompt to the user requesting that a download target be chosen.

 Should invoke |callback| upon completion.

### RequestIncognitoWarningConfirmation

RequestIncognitoWarningConfirmation
~~~cpp
virtual void RequestIncognitoWarningConfirmation(
      IncognitoWarningConfirmationCallback callback) = 0;
~~~
 Display a message prompt to the user containing an incognito warning.

 Should invoke |callback| upon completion.

### DetermineLocalPath

DetermineLocalPath
~~~cpp
virtual void DetermineLocalPath(download::DownloadItem* download,
                                  const base::FilePath& virtual_path,
                                  download::LocalPathCallback callback) = 0;
~~~
 If |virtual_path| is not a local path, should return a possibly temporary
 local path to use for storing the downloaded file. If |virtual_path| is
 already local, then it should return the same path. |callback| should be
 invoked to return the path.

### CheckDownloadUrl

CheckDownloadUrl
~~~cpp
virtual void CheckDownloadUrl(download::DownloadItem* download,
                                const base::FilePath& virtual_path,
                                CheckDownloadUrlCallback callback) = 0;
~~~
 Check whether the download URL is malicious and invoke |callback| with a
 suggested danger type for the download.

### GetFileMimeType

GetFileMimeType
~~~cpp
virtual void GetFileMimeType(const base::FilePath& path,
                               GetFileMimeTypeCallback callback) = 0;
~~~
 Get the MIME type for the given file.

### ~DownloadTargetDeterminerDelegate

DownloadTargetDeterminerDelegate::~DownloadTargetDeterminerDelegate
~~~cpp
virtual ~DownloadTargetDeterminerDelegate();
~~~

### GetInsecureDownloadStatus

GetInsecureDownloadStatus
~~~cpp
virtual void GetInsecureDownloadStatus(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      GetInsecureDownloadStatusCallback callback) = 0;
~~~
 Returns whether the download should be warned/blocked based on its insecure
 download status, and if so, what kind of warning/blocking should be used.

### NotifyExtensions

NotifyExtensions
~~~cpp
virtual void NotifyExtensions(download::DownloadItem* download,
                                const base::FilePath& virtual_path,
                                NotifyExtensionsCallback callback) = 0;
~~~
 Notifies extensions of the impending filename determination. |virtual_path|
 is the current suggested virtual path. The |callback| should be invoked to
 indicate whether any extensions wish to override the path.

### ReserveVirtualPath

ReserveVirtualPath
~~~cpp
virtual void ReserveVirtualPath(
      download::DownloadItem* download,
      const base::FilePath& virtual_path,
      bool create_directory,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      ReservedPathCallback callback) = 0;
~~~
 Reserve |virtual_path|. This is expected to check the following:
 - Whether |virtual_path| can be written to by the user. If not, the
   |virtual_path| can be changed to writeable path if necessary.

 - If |conflict_action| is UNIQUIFY then |virtual_path| should be
   modified so that the new path is writeable and unique. If
   |conflict_action| is PROMPT, then in the event of a conflict,
   |callback| should be invoked with |success| set to |false| in
   order to force a prompt. |virtual_path| may or may not be
   modified in the latter case.

 - If |create_directory| is true, then the parent directory of
   |virtual_path| should be created if it doesn't exist.


 |callback| should be invoked on completion with the results.

### RequestConfirmation

RequestConfirmation
~~~cpp
virtual void RequestConfirmation(download::DownloadItem* download,
                                   const base::FilePath& virtual_path,
                                   DownloadConfirmationReason reason,
                                   ConfirmationCallback callback) = 0;
~~~
 Display a prompt to the user requesting that a download target be chosen.

 Should invoke |callback| upon completion.

### DetermineLocalPath

DetermineLocalPath
~~~cpp
virtual void DetermineLocalPath(download::DownloadItem* download,
                                  const base::FilePath& virtual_path,
                                  download::LocalPathCallback callback) = 0;
~~~
 If |virtual_path| is not a local path, should return a possibly temporary
 local path to use for storing the downloaded file. If |virtual_path| is
 already local, then it should return the same path. |callback| should be
 invoked to return the path.

### CheckDownloadUrl

CheckDownloadUrl
~~~cpp
virtual void CheckDownloadUrl(download::DownloadItem* download,
                                const base::FilePath& virtual_path,
                                CheckDownloadUrlCallback callback) = 0;
~~~
 Check whether the download URL is malicious and invoke |callback| with a
 suggested danger type for the download.

### GetFileMimeType

GetFileMimeType
~~~cpp
virtual void GetFileMimeType(const base::FilePath& path,
                               GetFileMimeTypeCallback callback) = 0;
~~~
 Get the MIME type for the given file.
