
## class SavePackageFilePicker
 Handles showing a dialog to the user to ask for the filename to save a page.

### SavePackageFilePicker

SavePackageFilePicker::SavePackageFilePicker
~~~cpp
SavePackageFilePicker(content::WebContents* web_contents,
                        const base::FilePath& suggested_path,
                        const base::FilePath::StringType& default_extension,
                        bool can_save_as_complete,
                        DownloadPrefs* download_prefs,
                        content::SavePackagePathPickedCallback callback);
~~~

### SavePackageFilePicker

SavePackageFilePicker
~~~cpp
SavePackageFilePicker(const SavePackageFilePicker&) = delete;
~~~

### operator=

operator=
~~~cpp
SavePackageFilePicker& operator=(const SavePackageFilePicker&) = delete;
~~~

### ~SavePackageFilePicker

SavePackageFilePicker::~SavePackageFilePicker
~~~cpp
~SavePackageFilePicker() override;
~~~

### SetShouldPromptUser

SavePackageFilePicker::SetShouldPromptUser
~~~cpp
static void SetShouldPromptUser(bool should_prompt);
~~~
 Used to disable prompting the user for a directory/filename of the saved
 web page.  This is available for testing.

### FileSelected

SavePackageFilePicker::FileSelected
~~~cpp
void FileSelected(const base::FilePath& path,
                    int index,
                    void* unused_params) override;
~~~
 SelectFileDialog::Listener implementation.

### FileSelectionCanceled

SavePackageFilePicker::FileSelectionCanceled
~~~cpp
void FileSelectionCanceled(void* unused_params) override;
~~~

### ShouldSaveAsOnlyHTML

SavePackageFilePicker::ShouldSaveAsOnlyHTML
~~~cpp
bool ShouldSaveAsOnlyHTML(content::WebContents* web_contents) const;
~~~

### ShouldSaveAsMHTML

SavePackageFilePicker::ShouldSaveAsMHTML
~~~cpp
bool ShouldSaveAsMHTML() const;
~~~

### render_process_id_



~~~cpp

int render_process_id_;

~~~

 Used to look up the renderer process for this request to get the context.

### can_save_as_complete_



~~~cpp

bool can_save_as_complete_;

~~~

 Whether the web page can be saved as a complete HTML file.

### download_prefs_



~~~cpp

raw_ptr<DownloadPrefs> download_prefs_;

~~~


### callback_



~~~cpp

content::SavePackagePathPickedCallback callback_;

~~~


### save_types_



~~~cpp

std::vector<content::SavePageType> save_types_;

~~~


### select_file_dialog_



~~~cpp

scoped_refptr<ui::SelectFileDialog> select_file_dialog_;

~~~

 For managing select file dialogs.

### SavePackageFilePicker

SavePackageFilePicker
~~~cpp
SavePackageFilePicker(const SavePackageFilePicker&) = delete;
~~~

### operator=

operator=
~~~cpp
SavePackageFilePicker& operator=(const SavePackageFilePicker&) = delete;
~~~

### SetShouldPromptUser

SavePackageFilePicker::SetShouldPromptUser
~~~cpp
static void SetShouldPromptUser(bool should_prompt);
~~~
 Used to disable prompting the user for a directory/filename of the saved
 web page.  This is available for testing.

### FileSelected

SavePackageFilePicker::FileSelected
~~~cpp
void FileSelected(const base::FilePath& path,
                    int index,
                    void* unused_params) override;
~~~
 SelectFileDialog::Listener implementation.

### FileSelectionCanceled

SavePackageFilePicker::FileSelectionCanceled
~~~cpp
void FileSelectionCanceled(void* unused_params) override;
~~~

### ShouldSaveAsOnlyHTML

SavePackageFilePicker::ShouldSaveAsOnlyHTML
~~~cpp
bool ShouldSaveAsOnlyHTML(content::WebContents* web_contents) const;
~~~

### ShouldSaveAsMHTML

SavePackageFilePicker::ShouldSaveAsMHTML
~~~cpp
bool ShouldSaveAsMHTML() const;
~~~

### render_process_id_



~~~cpp

int render_process_id_;

~~~

 Used to look up the renderer process for this request to get the context.

### can_save_as_complete_



~~~cpp

bool can_save_as_complete_;

~~~

 Whether the web page can be saved as a complete HTML file.

### download_prefs_



~~~cpp

raw_ptr<DownloadPrefs> download_prefs_;

~~~


### callback_



~~~cpp

content::SavePackagePathPickedCallback callback_;

~~~


### save_types_



~~~cpp

std::vector<content::SavePageType> save_types_;

~~~


### select_file_dialog_



~~~cpp

scoped_refptr<ui::SelectFileDialog> select_file_dialog_;

~~~

 For managing select file dialogs.
