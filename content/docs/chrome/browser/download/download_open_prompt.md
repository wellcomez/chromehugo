
## class DownloadOpenPrompt
 Prompts the user for whether to open a DownloadItem using native UI. This
 step is necessary to prevent a malicious extension from opening any
 downloaded file.

### CreateDownloadOpenConfirmationDialog

DownloadOpenPrompt::CreateDownloadOpenConfirmationDialog
~~~cpp
static DownloadOpenPrompt* CreateDownloadOpenConfirmationDialog(
      content::WebContents* web_contents,
      const std::string& extension_name,
      const base::FilePath& file_path,
      OpenCallback open_callback);
~~~
 Creates the open confirmation dialog and returns this object.

### DownloadOpenPrompt

DownloadOpenPrompt
~~~cpp
DownloadOpenPrompt(const DownloadOpenPrompt&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadOpenPrompt& operator=(const DownloadOpenPrompt&) = delete;
~~~

### AcceptConfirmationDialogForTesting

DownloadOpenPrompt::AcceptConfirmationDialogForTesting
~~~cpp
static void AcceptConfirmationDialogForTesting(
      DownloadOpenPrompt* download_danger_prompt);
~~~
 Called to accept the confirmation dialog for testing.

### DownloadOpenPrompt

DownloadOpenPrompt::DownloadOpenPrompt
~~~cpp
DownloadOpenPrompt();
~~~

### ~DownloadOpenPrompt

DownloadOpenPrompt::~DownloadOpenPrompt
~~~cpp
~DownloadOpenPrompt();
~~~

### DownloadOpenPrompt

DownloadOpenPrompt
~~~cpp
DownloadOpenPrompt(const DownloadOpenPrompt&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadOpenPrompt& operator=(const DownloadOpenPrompt&) = delete;
~~~

### CreateDownloadOpenConfirmationDialog

DownloadOpenPrompt::CreateDownloadOpenConfirmationDialog
~~~cpp
static DownloadOpenPrompt* CreateDownloadOpenConfirmationDialog(
      content::WebContents* web_contents,
      const std::string& extension_name,
      const base::FilePath& file_path,
      OpenCallback open_callback);
~~~
 Creates the open confirmation dialog and returns this object.

### AcceptConfirmationDialogForTesting

DownloadOpenPrompt::AcceptConfirmationDialogForTesting
~~~cpp
static void AcceptConfirmationDialogForTesting(
      DownloadOpenPrompt* download_danger_prompt);
~~~
 Called to accept the confirmation dialog for testing.
