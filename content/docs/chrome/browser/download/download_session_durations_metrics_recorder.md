
## class DownloadSessionDurationsMetricsRecorder

### DownloadSessionDurationsMetricsRecorder

DownloadSessionDurationsMetricsRecorder::DownloadSessionDurationsMetricsRecorder
~~~cpp
DownloadSessionDurationsMetricsRecorder();
~~~

### ~DownloadSessionDurationsMetricsRecorder

~DownloadSessionDurationsMetricsRecorder
~~~cpp
~DownloadSessionDurationsMetricsRecorder() = default;
~~~

### DownloadSessionDurationsMetricsRecorder

DownloadSessionDurationsMetricsRecorder
~~~cpp
DownloadSessionDurationsMetricsRecorder(
      const DownloadSessionDurationsMetricsRecorder&) = delete;
~~~

### OnSessionStarted

DownloadSessionDurationsMetricsRecorder::OnSessionStarted
~~~cpp
void OnSessionStarted(base::TimeTicks session_start);
~~~
 Called from DesktopProfileSessionDurationsService
### OnSessionEnded

DownloadSessionDurationsMetricsRecorder::OnSessionEnded
~~~cpp
void OnSessionEnded(base::TimeTicks session_end);
~~~

### session_start_



~~~cpp

base::TimeTicks session_start_;

~~~


### is_bubble_showing_when_session_end_



~~~cpp

bool is_bubble_showing_when_session_end_ = false;

~~~


### ~DownloadSessionDurationsMetricsRecorder

~DownloadSessionDurationsMetricsRecorder
~~~cpp
~DownloadSessionDurationsMetricsRecorder() = default;
~~~

### DownloadSessionDurationsMetricsRecorder

DownloadSessionDurationsMetricsRecorder
~~~cpp
DownloadSessionDurationsMetricsRecorder(
      const DownloadSessionDurationsMetricsRecorder&) = delete;
~~~

### OnSessionStarted

DownloadSessionDurationsMetricsRecorder::OnSessionStarted
~~~cpp
void OnSessionStarted(base::TimeTicks session_start);
~~~
 Called from DesktopProfileSessionDurationsService
### OnSessionEnded

DownloadSessionDurationsMetricsRecorder::OnSessionEnded
~~~cpp
void OnSessionEnded(base::TimeTicks session_end);
~~~

### session_start_



~~~cpp

base::TimeTicks session_start_;

~~~


### is_bubble_showing_when_session_end_



~~~cpp

bool is_bubble_showing_when_session_end_ = false;

~~~

