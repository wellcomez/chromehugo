
## class DownloadCommands

### enum Command

~~~cpp
enum Command {
    SHOW_IN_FOLDER = 1,   // Open a folder view window with the item selected.
    OPEN_WHEN_COMPLETE,   // Open the download when it's finished.
    ALWAYS_OPEN_TYPE,     // Default this file extension to always open.
    PLATFORM_OPEN,        // Open using platform handler.
    CANCEL,               // Cancel the download.
    PAUSE,                // Pause a download.
    RESUME,               // Resume a download.
    DISCARD,              // Discard the malicious download.
    KEEP,                 // Keep the malicious download.
    LEARN_MORE_SCANNING,  // Show information about download scanning.
    LEARN_MORE_INTERRUPTED,  // Show information about interrupted downloads.
    LEARN_MORE_INSECURE_DOWNLOAD,  // Show info about insecure downloads.
    COPY_TO_CLIPBOARD,             // Copy the contents to the clipboard.
    DEEP_SCAN,             // Send file to Safe Browsing for deep scanning.
    BYPASS_DEEP_SCANNING,  // Bypass the prompt to deep scan.
    REVIEW,                // Show enterprise download review dialog.
    RETRY,                 // Retry the download.
    MAX
  }
~~~
### DownloadCommands

DownloadCommands::DownloadCommands
~~~cpp
explicit DownloadCommands(base::WeakPtr<DownloadUIModel> model);
~~~
 |model| must outlive DownloadCommands.

 TODO(shaktisahu): Investigate if model lifetime is shorter than |this|.

### DownloadCommands

DownloadCommands
~~~cpp
DownloadCommands(const DownloadCommands&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCommands& operator=(const DownloadCommands&) = delete;
~~~

### ~DownloadCommands

DownloadCommands::~DownloadCommands
~~~cpp
virtual ~DownloadCommands();
~~~

### IsCommandEnabled

DownloadCommands::IsCommandEnabled
~~~cpp
bool IsCommandEnabled(Command command) const;
~~~

### IsCommandChecked

DownloadCommands::IsCommandChecked
~~~cpp
bool IsCommandChecked(Command command) const;
~~~

### IsCommandVisible

DownloadCommands::IsCommandVisible
~~~cpp
bool IsCommandVisible(Command command) const;
~~~

### ExecuteCommand

DownloadCommands::ExecuteCommand
~~~cpp
void ExecuteCommand(Command command);
~~~

### IsDownloadPdf

DownloadCommands::IsDownloadPdf
~~~cpp
bool IsDownloadPdf() const;
~~~

### CanOpenPdfInSystemViewer

DownloadCommands::CanOpenPdfInSystemViewer
~~~cpp
bool CanOpenPdfInSystemViewer() const;
~~~

### GetBrowser

DownloadCommands::GetBrowser
~~~cpp
Browser* GetBrowser() const;
~~~

### GetLearnMoreURLForInterruptedDownload

DownloadCommands::GetLearnMoreURLForInterruptedDownload
~~~cpp
GURL GetLearnMoreURLForInterruptedDownload() const;
~~~

### CopyFileAsImageToClipboard

DownloadCommands::CopyFileAsImageToClipboard
~~~cpp
void CopyFileAsImageToClipboard();
~~~

### CanBeCopiedToClipboard

DownloadCommands::CanBeCopiedToClipboard
~~~cpp
bool CanBeCopiedToClipboard() const;
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadCommands::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(
      DownloadCommandsTest,
      GetLearnMoreURLForInterruptedDownload_ContainsContext);
~~~

### model_



~~~cpp

base::WeakPtr<DownloadUIModel> model_;

~~~


### task_runner_



~~~cpp

scoped_refptr<base::SequencedTaskRunner> task_runner_;

~~~


### DownloadCommands

DownloadCommands
~~~cpp
DownloadCommands(const DownloadCommands&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCommands& operator=(const DownloadCommands&) = delete;
~~~

### enum Command

~~~cpp
enum Command {
    SHOW_IN_FOLDER = 1,   // Open a folder view window with the item selected.
    OPEN_WHEN_COMPLETE,   // Open the download when it's finished.
    ALWAYS_OPEN_TYPE,     // Default this file extension to always open.
    PLATFORM_OPEN,        // Open using platform handler.
    CANCEL,               // Cancel the download.
    PAUSE,                // Pause a download.
    RESUME,               // Resume a download.
    DISCARD,              // Discard the malicious download.
    KEEP,                 // Keep the malicious download.
    LEARN_MORE_SCANNING,  // Show information about download scanning.
    LEARN_MORE_INTERRUPTED,  // Show information about interrupted downloads.
    LEARN_MORE_INSECURE_DOWNLOAD,  // Show info about insecure downloads.
    COPY_TO_CLIPBOARD,             // Copy the contents to the clipboard.
    DEEP_SCAN,             // Send file to Safe Browsing for deep scanning.
    BYPASS_DEEP_SCANNING,  // Bypass the prompt to deep scan.
    REVIEW,                // Show enterprise download review dialog.
    RETRY,                 // Retry the download.
    MAX
  };
~~~
### IsCommandEnabled

DownloadCommands::IsCommandEnabled
~~~cpp
bool IsCommandEnabled(Command command) const;
~~~

### IsCommandChecked

DownloadCommands::IsCommandChecked
~~~cpp
bool IsCommandChecked(Command command) const;
~~~

### IsCommandVisible

DownloadCommands::IsCommandVisible
~~~cpp
bool IsCommandVisible(Command command) const;
~~~

### ExecuteCommand

DownloadCommands::ExecuteCommand
~~~cpp
void ExecuteCommand(Command command);
~~~

### GetLearnMoreURLForInterruptedDownload

DownloadCommands::GetLearnMoreURLForInterruptedDownload
~~~cpp
GURL GetLearnMoreURLForInterruptedDownload() const;
~~~

### CopyFileAsImageToClipboard

DownloadCommands::CopyFileAsImageToClipboard
~~~cpp
void CopyFileAsImageToClipboard();
~~~

### CanBeCopiedToClipboard

DownloadCommands::CanBeCopiedToClipboard
~~~cpp
bool CanBeCopiedToClipboard() const;
~~~

### model_



~~~cpp

base::WeakPtr<DownloadUIModel> model_;

~~~


### task_runner_



~~~cpp

scoped_refptr<base::SequencedTaskRunner> task_runner_;

~~~

