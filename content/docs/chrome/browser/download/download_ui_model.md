
## class DownloadUIModel
 namespace gfx
 This class is an abstraction for common UI tasks and properties associated
 with a download.

###  ~StatusTextBuilderBase

 Abstract base class for building StatusText
~~~cpp
class StatusTextBuilderBase {
   public:
    virtual ~StatusTextBuilderBase() = default;
    void SetModel(DownloadUIModel* model);

    // Returns a short one-line status string for the download.
    std::u16string GetStatusText(
        download::DownloadItem::DownloadState state) const;

    std::u16string GetCompletedRemovedOrSavedStatusText() const;
    // Returns a string indicating the status of an in-progress download.
    virtual std::u16string GetInProgressStatusText() const = 0;

    // Returns a string indicating the status of a completed download.
    virtual std::u16string GetCompletedStatusText() const = 0;

    // Returns a string representation of the current download progress sizes.
    // If the total size of the download is known, this string looks like:
    // "100/200 MB" where the numerator is the transferred size and the
    // denominator is the total size. If the total isn't known, returns the
    // transferred size as a string (e.g.: "100 MB").
    virtual std::u16string GetProgressSizesString() const = 0;

    // Returns a string indicating the status of an interrupted download.
    virtual std::u16string GetInterruptedStatusText(
        offline_items_collection::FailState fail_state) const;

    // Returns a short string indicating why the download failed.
    virtual std::u16string GetFailStateMessage(
        offline_items_collection::FailState fail_state) const;

    // Unknowned model to create statuses.
    raw_ptr<DownloadUIModel> model_ = nullptr;
  };
~~~
###  GetInProgressStatusText

 Used in Download shelf and page, default option.

~~~cpp
class StatusTextBuilder : public StatusTextBuilderBase {
   public:
    std::u16string GetInProgressStatusText() const override;
    std::u16string GetCompletedStatusText() const override;
    std::u16string GetProgressSizesString() const override;
  };
~~~
###  GetInProgressStatusText

 Used in Download bubble.

~~~cpp
class BubbleStatusTextBuilder : public StatusTextBuilderBase {
   public:
    std::u16string GetInProgressStatusText() const override;
    std::u16string GetCompletedStatusText() const override;
    std::u16string GetInterruptedStatusText(
        offline_items_collection::FailState fail_state) const override;
    std::u16string GetProgressSizesString() const override;

   private:
    FRIEND_TEST_ALL_PREFIXES(DownloadItemModelTest,
                             GetBubbleStatusMessageWithBytes);

    static std::u16string GetBubbleStatusMessageWithBytes(
        const std::u16string& bytes_substring,
        const std::u16string& detail_message,
        bool is_active);
    std::u16string GetBubbleWarningStatusText() const;
  };
~~~
###  SubpageButton


~~~cpp
struct BubbleUIInfo {
    struct SubpageButton {
      DownloadCommands::Command command;
      std::u16string label;
      bool is_prominent = false;

      SubpageButton(DownloadCommands::Command command,
                    std::u16string label,
                    bool is_prominent);
    };

    struct QuickAction {
      DownloadCommands::Command command;
      std::u16string hover_text;
      raw_ptr<const gfx::VectorIcon> icon = nullptr;
      QuickAction(DownloadCommands::Command command,
                  const std::u16string& hover_text,
                  const gfx::VectorIcon* icon);
    };

    // has a progress bar and a cancel button.
    bool has_progress_bar = false;
    bool is_progress_bar_looping = false;
    // kColorAlertHighSeverity, kColorAlertMediumSeverityIcon, or
    // kColorSecondaryForeground
    ui::ColorId secondary_color = ui::kColorSecondaryForeground;
    // Color used for alert text, which may be different from |secondary_color|,
    // used for icons. If this is nullopt, |secondary_color| will be used for
    // text.
    absl::optional<ui::ColorId> secondary_text_color = absl::nullopt;

    // Override icon
    raw_ptr<const gfx::VectorIcon> icon_model_override = nullptr;

    // Subpage summary of the download warning
    bool has_subpage = false;
    std::u16string warning_summary;

    // Label for the checkbox, empty if no checkbox is needed
    bool has_checkbox = false;
    std::u16string checkbox_label;

    // The command for the primary button
    absl::optional<DownloadCommands::Command> primary_button_command;

    // List of quick actions
    std::vector<QuickAction> quick_actions;

    // Subpage buttons
    std::vector<SubpageButton> subpage_buttons;

    // The subpage exists if the summary exists.
    explicit BubbleUIInfo(const std::u16string& summary);
    // If no subpage, the progress bar may exist.
    explicit BubbleUIInfo(bool has_progress_bar);
    BubbleUIInfo();
    ~BubbleUIInfo();
    BubbleUIInfo(const BubbleUIInfo&);
    BubbleUIInfo& AddIconAndColor(const gfx::VectorIcon& vector_icon,
                                  ui::ColorId color_id);
    BubbleUIInfo& AddSecondaryTextColor(ui::ColorId color_id);
    BubbleUIInfo& AddPrimaryButton(DownloadCommands::Command command);
    BubbleUIInfo& AddCheckbox(const std::u16string& label);
    // Add button to the subpage. Only two buttons are supported.
    // The first one added is the primary, and the second one the secondary.
    // The checkbox, if present, controls the secondary.
    BubbleUIInfo& AddSubpageButton(const std::u16string& label,
                                   DownloadCommands::Command command,
                                   bool is_prominent);
    BubbleUIInfo& SetProgressBarLooping();
    BubbleUIInfo& AddQuickAction(DownloadCommands::Command command,
                                 const std::u16string& label,
                                 const gfx::VectorIcon* icon);
    ui::ColorId GetColorForSecondaryText() const;
  };
~~~
### DownloadUIModel

DownloadUIModel::DownloadUIModel
~~~cpp
DownloadUIModel();
~~~

### DownloadUIModel

DownloadUIModel::DownloadUIModel
~~~cpp
explicit DownloadUIModel(
      std::unique_ptr<StatusTextBuilderBase> status_text_builder);
~~~

### DownloadUIModel

DownloadUIModel
~~~cpp
DownloadUIModel(const DownloadUIModel&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadUIModel& operator=(const DownloadUIModel&) = delete;
~~~

### ~DownloadUIModel

DownloadUIModel::~DownloadUIModel
~~~cpp
virtual ~DownloadUIModel();
~~~

###  OnDownloadUpdated

 Delegate for a single DownloadUIModel.

~~~cpp
class Delegate {
   public:
    virtual void OnDownloadUpdated() {}
    virtual void OnDownloadOpened() {}
    virtual void OnDownloadDestroyed(const ContentId& id) {}

    virtual ~Delegate() = default;
  };
~~~
### SetDelegate

DownloadUIModel::SetDelegate
~~~cpp
void SetDelegate(Delegate* delegate);
~~~

### GetWeakPtr

DownloadUIModel::GetWeakPtr
~~~cpp
base::WeakPtr<DownloadUIModel> GetWeakPtr();
~~~

### HasSupportedImageMimeType

DownloadUIModel::HasSupportedImageMimeType
~~~cpp
bool HasSupportedImageMimeType() const;
~~~
 Does this download have a MIME type (either explicit or inferred from its
 extension) suggesting that it is a supported image type?
### GetProgressSizesString

DownloadUIModel::GetProgressSizesString
~~~cpp
std::u16string GetProgressSizesString() const;
~~~
 Returns a string representation of the current download progress sizes.

 If the total size of the download is known, this string looks like:
 "100/200 MB" where the numerator is the transferred size and the
 denominator is the total size. If the total isn't known, returns the
 transferred size as a string (e.g.: "100 MB").

### GetInterruptDescription

DownloadUIModel::GetInterruptDescription
~~~cpp
std::u16string GetInterruptDescription() const;
~~~
 Returns a long descriptive string for a download that's in the INTERRUPTED
 state. For other downloads, the returned string will be empty.

### GetHistoryPageStatusText

DownloadUIModel::GetHistoryPageStatusText
~~~cpp
std::u16string GetHistoryPageStatusText() const;
~~~
 Returns a status string for the download history page.

### GetStatusText

DownloadUIModel::GetStatusText
~~~cpp
std::u16string GetStatusText() const;
~~~
 Returns a short one-line status string for the download.

### GetStatusTextForLabel

DownloadUIModel::GetStatusTextForLabel
~~~cpp
std::u16string GetStatusTextForLabel(const gfx::FontList& font_list,
                                       float available_pixel_width) const;
~~~

### GetTooltipText

DownloadUIModel::GetTooltipText
~~~cpp
std::u16string GetTooltipText() const;
~~~
 Returns a string suitable for use as a tooltip. For a regular download, the
 tooltip is the filename. For an interrupted download, the string states the
 filename and a short description of the reason for interruption. For
 example:
    Report.pdf
    Network disconnected
### GetWarningText

DownloadUIModel::GetWarningText
~~~cpp
std::u16string GetWarningText(const std::u16string& filename,
                                size_t* offset) const;
~~~
 Get the warning text to display for a dangerous download. |filename| is the
 (possibly-elided) filename; if it is present in the resulting string,
 |offset| will be set to the starting position of the filename.

### GetWarningConfirmButtonText

DownloadUIModel::GetWarningConfirmButtonText
~~~cpp
std::u16string GetWarningConfirmButtonText() const;
~~~
 Get the caption text for a button for confirming a dangerous download
 warning.

### GetShowInFolderText

DownloadUIModel::GetShowInFolderText
~~~cpp
std::u16string GetShowInFolderText() const;
~~~
 Get the text to display for the button to show item in folder on download
 history page.

### profile

DownloadUIModel::profile
~~~cpp
virtual Profile* profile() const;
~~~
 Returns the profile associated with this download.

### GetContentId

DownloadUIModel::GetContentId
~~~cpp
virtual ContentId GetContentId() const;
~~~
 Returns the content id associated with this download.

### GetTabProgressStatusText

DownloadUIModel::GetTabProgressStatusText
~~~cpp
virtual std::u16string GetTabProgressStatusText() const;
~~~
 Returns the localized status text for an in-progress download. This
 is the progress status used in the WebUI interface.

### GetCompletedBytes

DownloadUIModel::GetCompletedBytes
~~~cpp
virtual int64_t GetCompletedBytes() const;
~~~
 Get the number of bytes that has completed so far.

### GetTotalBytes

DownloadUIModel::GetTotalBytes
~~~cpp
virtual int64_t GetTotalBytes() const;
~~~
 Get the total number of bytes for this download. Should return 0 if the
 total size of the download is not known. Virtual for testing.

### PercentComplete

DownloadUIModel::PercentComplete
~~~cpp
virtual int PercentComplete() const;
~~~
 Rough percent complete. Returns -1 if the progress is unknown.

### IsDangerous

DownloadUIModel::IsDangerous
~~~cpp
virtual bool IsDangerous() const;
~~~
 Is this considered a dangerous download?
### MightBeMalicious

DownloadUIModel::MightBeMalicious
~~~cpp
virtual bool MightBeMalicious() const;
~~~
 Is this considered a malicious download? Implies IsDangerous().

### IsMalicious

DownloadUIModel::IsMalicious
~~~cpp
virtual bool IsMalicious() const;
~~~
 Is this considered a malicious download with very high confidence?
 Implies IsDangerous() and MightBeMalicious().

### IsInsecure

DownloadUIModel::IsInsecure
~~~cpp
virtual bool IsInsecure() const;
~~~
 Is this download an insecure download, but not something more severe?
 Implies IsDangerous() and !IsMalicious().

### ShouldRemoveFromShelfWhenComplete

DownloadUIModel::ShouldRemoveFromShelfWhenComplete
~~~cpp
virtual bool ShouldRemoveFromShelfWhenComplete() const;
~~~
 Returns |true| if this download is expected to complete successfully and
 thereafter be removed from the shelf.  Downloads that are opened
 automatically or are temporary will be removed from the shelf on successful
 completion.


 Returns |false| if the download is not expected to complete (interrupted,
 cancelled, dangerous, malicious), or won't be removed on completion.


 Since the expectation of successful completion may change, the return value
 of this function will change over the course of a download.

### ShouldShowDownloadStartedAnimation

DownloadUIModel::ShouldShowDownloadStartedAnimation
~~~cpp
virtual bool ShouldShowDownloadStartedAnimation() const;
~~~
 Returns |true| if the download started animation (big download arrow
 animates down towards the shelf) should be displayed for this download.

 Downloads that were initiated via "Save As" or are extension installs don't
 show the animation.

### ShouldShowInShelf

DownloadUIModel::ShouldShowInShelf
~~~cpp
virtual bool ShouldShowInShelf() const;
~~~
 Returns |true| if this download should be displayed in the downloads shelf.

### SetShouldShowInShelf

DownloadUIModel::SetShouldShowInShelf
~~~cpp
virtual void SetShouldShowInShelf(bool should_show);
~~~
 Change whether the download should be displayed on the downloads
 shelf. Setting this is only effective if the download hasn't already been
 displayed in the shelf.

### ShouldNotifyUI

DownloadUIModel::ShouldNotifyUI
~~~cpp
virtual bool ShouldNotifyUI() const;
~~~
 Returns |true| if the UI should be notified when the download is ready to
 be presented in the UI. Note that this is independent of
 ShouldShowInShelf() since there might be actions other than showing in the
 shelf that the UI must perform.

### WasUINotified

DownloadUIModel::WasUINotified
~~~cpp
virtual bool WasUINotified() const;
~~~
 Returns |true| if the UI has been notified about this download. By default,
 this value is |false| and should be changed explicitly using
 SetWasUINotified().

### SetWasUINotified

DownloadUIModel::SetWasUINotified
~~~cpp
virtual void SetWasUINotified(bool should_notify);
~~~
 Change what's returned by WasUINotified().

### WasActionedOn

DownloadUIModel::WasActionedOn
~~~cpp
virtual bool WasActionedOn() const;
~~~
 Returns |true| if the download was actioned on. This governs if the
 download should be shown in the Download Bubble's partial view.

### SetActionedOn

DownloadUIModel::SetActionedOn
~~~cpp
virtual void SetActionedOn(bool actioned_on);
~~~
 Change what's returned by WasActionedOn().

### WasUIWarningShown

DownloadUIModel::WasUIWarningShown
~~~cpp
virtual bool WasUIWarningShown() const;
~~~
 Returns |true| if the Download Bubble UI has shown this download warning.

 By default, this value is |false| and should be changed explicitly using
 SetWasUIWarningShown().

### SetWasUIWarningShown

DownloadUIModel::SetWasUIWarningShown
~~~cpp
virtual void SetWasUIWarningShown(bool was_ui_warning_shown);
~~~
 Change what's returned by WasUIWarningShown().

### GetEphemeralWarningUiShownTime

DownloadUIModel::GetEphemeralWarningUiShownTime
~~~cpp
virtual absl::optional<base::Time> GetEphemeralWarningUiShownTime() const;
~~~
 If this is an ephemeral warning, returns when the bubble first displayed
 the warning. If the warning has not yet shown (or this isn't an ephemeral
 warning), it returns no value. This does not persist across restarts.

### SetEphemeralWarningUiShownTime

DownloadUIModel::SetEphemeralWarningUiShownTime
~~~cpp
virtual void SetEphemeralWarningUiShownTime(absl::optional<base::Time> time);
~~~

### ShouldPreferOpeningInBrowser

DownloadUIModel::ShouldPreferOpeningInBrowser
~~~cpp
virtual bool ShouldPreferOpeningInBrowser();
~~~
 Returns |true| if opening in the browser is preferred for this download. If
 |false|, the download should be opened with the system default application.

### SetShouldPreferOpeningInBrowser

DownloadUIModel::SetShouldPreferOpeningInBrowser
~~~cpp
virtual void SetShouldPreferOpeningInBrowser(bool preference);
~~~
 Change what's returned by ShouldPreferOpeningInBrowser to |preference|.

### GetDangerLevel

DownloadUIModel::GetDangerLevel
~~~cpp
virtual safe_browsing::DownloadFileType::DangerLevel GetDangerLevel() const;
~~~
 Return the danger level determined during download target determination.

 The value returned here is independent of the danger level as determined by
 the Safe Browsing.

### SetDangerLevel

DownloadUIModel::SetDangerLevel
~~~cpp
virtual void SetDangerLevel(
      safe_browsing::DownloadFileType::DangerLevel danger_level);
~~~
 Change what's returned by GetDangerLevel().

### GetInsecureDownloadStatus

DownloadUIModel::GetInsecureDownloadStatus
~~~cpp
virtual download::DownloadItem::InsecureDownloadStatus
  GetInsecureDownloadStatus() const;
~~~
 Return the mixed content status determined during download target
 determination.

### OpenUsingPlatformHandler

DownloadUIModel::OpenUsingPlatformHandler
~~~cpp
virtual void OpenUsingPlatformHandler();
~~~
 Open the download using the platform handler for the download. The behavior
 of this method will be different from DownloadItem::OpenDownload() if
 ShouldPreferOpeningInBrowser().

### IsBeingRevived

DownloadUIModel::IsBeingRevived
~~~cpp
virtual bool IsBeingRevived() const;
~~~
 Whether the download was removed and this is currently being undone.

### SetIsBeingRevived

DownloadUIModel::SetIsBeingRevived
~~~cpp
virtual void SetIsBeingRevived(bool is_being_revived);
~~~
 Set whether the download is being revived.

### GetDownloadItem

DownloadUIModel::GetDownloadItem
~~~cpp
virtual const download::DownloadItem* GetDownloadItem() const;
~~~
 Returns the DownloadItem if this is a regular download, or nullptr
 otherwise.

### GetDownloadItem

DownloadUIModel::GetDownloadItem
~~~cpp
download::DownloadItem* GetDownloadItem();
~~~

### GetFileNameToReportUser

DownloadUIModel::GetFileNameToReportUser
~~~cpp
virtual base::FilePath GetFileNameToReportUser() const;
~~~
 Returns the file-name that should be reported to the user.

### GetTargetFilePath

DownloadUIModel::GetTargetFilePath
~~~cpp
virtual base::FilePath GetTargetFilePath() const;
~~~
 Target path of an in-progress download.

 May be empty if the target path hasn't yet been determined.

### OpenDownload

DownloadUIModel::OpenDownload
~~~cpp
virtual void OpenDownload();
~~~
 Opens the file associated with this download.  If the download is
 still in progress, marks the download to be opened when it is complete.

### GetState

DownloadUIModel::GetState
~~~cpp
virtual download::DownloadItem::DownloadState GetState() const;
~~~
 Returns the current state of the download.

### IsPaused

DownloadUIModel::IsPaused
~~~cpp
virtual bool IsPaused() const;
~~~
 Returns whether the download is currently paused.

### GetDangerType

DownloadUIModel::GetDangerType
~~~cpp
virtual download::DownloadDangerType GetDangerType() const;
~~~
 Returns the danger type associated with this download.

### GetOpenWhenComplete

DownloadUIModel::GetOpenWhenComplete
~~~cpp
virtual bool GetOpenWhenComplete() const;
~~~
 Returns true if the download will be auto-opened when complete.

### IsOpenWhenCompleteByPolicy

DownloadUIModel::IsOpenWhenCompleteByPolicy
~~~cpp
virtual bool IsOpenWhenCompleteByPolicy() const;
~~~
 Returns true if the download will be auto-opened when complete by policy.

### TimeRemaining

DownloadUIModel::TimeRemaining
~~~cpp
virtual bool TimeRemaining(base::TimeDelta* remaining) const;
~~~
 Simple calculation of the amount of time remaining to completion. Fills
 |*remaining| with the amount of time remaining if successful. Fails and
 returns false if we do not have the number of bytes or the download speed,
 and so can't give an estimate.

### GetStartTime

DownloadUIModel::GetStartTime
~~~cpp
virtual base::Time GetStartTime() const;
~~~
 Returns the creation time for a download.

### GetEndTime

DownloadUIModel::GetEndTime
~~~cpp
virtual base::Time GetEndTime() const;
~~~
 Returns the end/completion time for a completed download. base::Time()
 if the download has not completed yet.

### GetOpened

DownloadUIModel::GetOpened
~~~cpp
virtual bool GetOpened() const;
~~~
 Returns true if the download has been opened.

### SetOpened

DownloadUIModel::SetOpened
~~~cpp
virtual void SetOpened(bool opened);
~~~
 Marks the download as having been opened (without actually opening it).

### IsDone

DownloadUIModel::IsDone
~~~cpp
virtual bool IsDone() const;
~~~
 Returns true if the download is in a terminal state. This includes
 completed downloads, cancelled downloads, and interrupted downloads that
 can't be resumed.

### Pause

DownloadUIModel::Pause
~~~cpp
virtual void Pause();
~~~
 Pauses a download.  Will have no effect if the download is already
 paused.

### Resume

DownloadUIModel::Resume
~~~cpp
virtual void Resume();
~~~
 Resumes a download that has been paused or interrupted. Will have no effect
 if the download is neither. Only does something if CanResume() returns
 true.

### Cancel

DownloadUIModel::Cancel
~~~cpp
virtual void Cancel(bool user_cancel);
~~~
 Cancels the download operation. Set |user_cancel| to true if the
 cancellation was triggered by an explicit user action.

### Remove

DownloadUIModel::Remove
~~~cpp
virtual void Remove();
~~~
 Removes the download from the views and history. If the download was
 in-progress or interrupted, then the intermediate file will also be
 deleted.

### SetOpenWhenComplete

DownloadUIModel::SetOpenWhenComplete
~~~cpp
virtual void SetOpenWhenComplete(bool open);
~~~
 Marks the download to be auto-opened when completed.

### GetFullPath

DownloadUIModel::GetFullPath
~~~cpp
virtual base::FilePath GetFullPath() const;
~~~
 Returns the full path to the downloaded or downloading file. This is the
 path to the physical file, if one exists.

### CanResume

DownloadUIModel::CanResume
~~~cpp
virtual bool CanResume() const;
~~~
 Returns whether the download can be resumed.

### AllDataSaved

DownloadUIModel::AllDataSaved
~~~cpp
virtual bool AllDataSaved() const;
~~~
 Returns whether this download has saved all of its data.

### GetFileExternallyRemoved

DownloadUIModel::GetFileExternallyRemoved
~~~cpp
virtual bool GetFileExternallyRemoved() const;
~~~
 Returns whether the file associated with the download has been removed by
 external action.

### GetURL

DownloadUIModel::GetURL
~~~cpp
virtual GURL GetURL() const;
~~~
 Returns the URL represented by this download.

### HasUserGesture

DownloadUIModel::HasUserGesture
~~~cpp
virtual bool HasUserGesture() const;
~~~
 Returns whether the download request was initiated in response to a user
 gesture.

### GetLastFailState

DownloadUIModel::GetLastFailState
~~~cpp
virtual offline_items_collection::FailState GetLastFailState() const;
~~~
 Returns the most recent failure reason for this download. Returns
 |FailState::NO_FAILURE| if there is no previous failure reason.

### GetOriginalURL

DownloadUIModel::GetOriginalURL
~~~cpp
virtual GURL GetOriginalURL() const;
~~~
 Returns the URL of the orginiating request.

### ShouldPromoteOrigin

DownloadUIModel::ShouldPromoteOrigin
~~~cpp
virtual bool ShouldPromoteOrigin() const;
~~~
 Whether the Origin should be clearly displayed in the notification for
 security reasons.

### IsCommandEnabled

DownloadUIModel::IsCommandEnabled
~~~cpp
virtual bool IsCommandEnabled(const DownloadCommands* download_commands,
                                DownloadCommands::Command command) const;
~~~
 Methods related to DownloadCommands.

 Returns whether the given download command is enabled for this download.

### IsCommandChecked

DownloadUIModel::IsCommandChecked
~~~cpp
virtual bool IsCommandChecked(const DownloadCommands* download_commands,
                                DownloadCommands::Command command) const;
~~~
 Returns whether the given download command is checked for this download.

### ExecuteCommand

DownloadUIModel::ExecuteCommand
~~~cpp
virtual void ExecuteCommand(DownloadCommands* download_commands,
                              DownloadCommands::Command command);
~~~
 Executes the given download command on this download.

### GetBubbleUIInfo

DownloadUIModel::GetBubbleUIInfo
~~~cpp
BubbleUIInfo GetBubbleUIInfo(bool is_download_bubble_v2) const;
~~~
 Gets the information about the download bubbles subpage.

### GetBubbleUIInfoForInterrupted

DownloadUIModel::GetBubbleUIInfoForInterrupted
~~~cpp
BubbleUIInfo GetBubbleUIInfoForInterrupted(
      offline_items_collection::FailState fail_state) const;
~~~

### GetBubbleUIInfoForInProgressOrComplete

DownloadUIModel::GetBubbleUIInfoForInProgressOrComplete
~~~cpp
BubbleUIInfo GetBubbleUIInfoForInProgressOrComplete(
      bool is_download_bubble_v2) const;
~~~

### GetBubbleUIInfoForTailoredWarning

DownloadUIModel::GetBubbleUIInfoForTailoredWarning
~~~cpp
virtual BubbleUIInfo GetBubbleUIInfoForTailoredWarning() const;
~~~

### ShouldShowInBubble

DownloadUIModel::ShouldShowInBubble
~~~cpp
virtual bool ShouldShowInBubble() const;
~~~
 Returns |true| if this download should be displayed in the download bubble.

### ShouldShowTailoredWarning

DownloadUIModel::ShouldShowTailoredWarning
~~~cpp
virtual bool ShouldShowTailoredWarning() const;
~~~
 Should this download trigger a tailored warning?
### IsEphemeralWarning

DownloadUIModel::IsEphemeralWarning
~~~cpp
virtual bool IsEphemeralWarning() const;
~~~
 Ephemeral warnings are ones that are quickly removed from the bubble if the
 user has not acted on them, and later deleted altogether. Is this that kind
 of warning?
### CompleteSafeBrowsingScan

DownloadUIModel::CompleteSafeBrowsingScan
~~~cpp
virtual void CompleteSafeBrowsingScan();
~~~
 Complete the Safe Browsing scan early.

### ReviewScanningVerdict

DownloadUIModel::ReviewScanningVerdict
~~~cpp
virtual void ReviewScanningVerdict(content::WebContents* web_contents);
~~~
 Open a dialog to review a scan verdict.

### ShouldShowDropdown

DownloadUIModel::ShouldShowDropdown
~~~cpp
virtual bool ShouldShowDropdown() const;
~~~
 Whether the dropdown menu button should be shown or not.

### DetermineAndSetShouldPreferOpeningInBrowser

DownloadUIModel::DetermineAndSetShouldPreferOpeningInBrowser
~~~cpp
virtual void DetermineAndSetShouldPreferOpeningInBrowser(
      const base::FilePath& target_path,
      bool is_filetype_handled_safely);
~~~
 Determines if a download should be preferably opened in the browser instead
 of the platform. Use |is_filetype_handled_safely| indicating if opening a
 file of this type is safe in the current BrowserContext, |target_path| to
 see if files of this type should be opened in the browser, and set whether
 the download should be preferred opening in the browser.

### GetInProgressAccessibleAlertText

DownloadUIModel::GetInProgressAccessibleAlertText
~~~cpp
virtual std::u16string GetInProgressAccessibleAlertText() const;
~~~
 Returns the accessible alert text that should be announced when the
 download is in progress.

### GetMimeType

DownloadUIModel::GetMimeType
~~~cpp
virtual std::string GetMimeType() const;
~~~
 Returns the MIME type of the download.

### IsExtensionDownload

DownloadUIModel::IsExtensionDownload
~~~cpp
virtual bool IsExtensionDownload() const;
~~~
 Returns whether the download is triggered by an extension.

### delegate_



~~~cpp

raw_ptr<Delegate> delegate_ = nullptr;

~~~


### IsBubbleV2Enabled

DownloadUIModel::IsBubbleV2Enabled
~~~cpp
bool IsBubbleV2Enabled() const;
~~~
 Returns whether the DownloadBubbleV2 functionality is enabled.

### set_clock_for_testing

DownloadUIModel::set_clock_for_testing
~~~cpp
void set_clock_for_testing(base::Clock* clock);
~~~

### set_status_text_builder_for_testing

DownloadUIModel::set_status_text_builder_for_testing
~~~cpp
void set_status_text_builder_for_testing(bool for_bubble);
~~~

### set_is_bubble_v2_enabled_for_testing

DownloadUIModel::set_is_bubble_v2_enabled_for_testing
~~~cpp
void set_is_bubble_v2_enabled_for_testing(bool is_enabled);
~~~
 The following two methods exist for simpler unit testing.

 Setting an override for whether the DownloadBubbleV2 functionality is
 enabled.

###  base::DefaultClock::GetInstance

 Unowned Clock to override the time of "Now".

~~~cpp
raw_ptr<base::Clock> clock_ = base::DefaultClock::GetInstance();
~~~
### is_bubble_V2_enabled_for_testing_



~~~cpp

absl::optional<bool> is_bubble_V2_enabled_for_testing_;

~~~


### status_text_builder_



~~~cpp

std::unique_ptr<StatusTextBuilderBase> status_text_builder_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadUIModel> weak_ptr_factory_{this};

~~~


### DownloadUIModel

DownloadUIModel
~~~cpp
DownloadUIModel(const DownloadUIModel&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadUIModel& operator=(const DownloadUIModel&) = delete;
~~~

###  ~StatusTextBuilderBase

 Abstract base class for building StatusText
~~~cpp
class StatusTextBuilderBase {
   public:
    virtual ~StatusTextBuilderBase() = default;
    void SetModel(DownloadUIModel* model);

    // Returns a short one-line status string for the download.
    std::u16string GetStatusText(
        download::DownloadItem::DownloadState state) const;

    std::u16string GetCompletedRemovedOrSavedStatusText() const;
    // Returns a string indicating the status of an in-progress download.
    virtual std::u16string GetInProgressStatusText() const = 0;

    // Returns a string indicating the status of a completed download.
    virtual std::u16string GetCompletedStatusText() const = 0;

    // Returns a string representation of the current download progress sizes.
    // If the total size of the download is known, this string looks like:
    // "100/200 MB" where the numerator is the transferred size and the
    // denominator is the total size. If the total isn't known, returns the
    // transferred size as a string (e.g.: "100 MB").
    virtual std::u16string GetProgressSizesString() const = 0;

    // Returns a string indicating the status of an interrupted download.
    virtual std::u16string GetInterruptedStatusText(
        offline_items_collection::FailState fail_state) const;

    // Returns a short string indicating why the download failed.
    virtual std::u16string GetFailStateMessage(
        offline_items_collection::FailState fail_state) const;

    // Unknowned model to create statuses.
    raw_ptr<DownloadUIModel> model_ = nullptr;
  };
~~~
###  GetInProgressStatusText

 Used in Download shelf and page, default option.

~~~cpp
class StatusTextBuilder : public StatusTextBuilderBase {
   public:
    std::u16string GetInProgressStatusText() const override;
    std::u16string GetCompletedStatusText() const override;
    std::u16string GetProgressSizesString() const override;
  };
~~~
###  GetInProgressStatusText

 Used in Download bubble.

~~~cpp
class BubbleStatusTextBuilder : public StatusTextBuilderBase {
   public:
    std::u16string GetInProgressStatusText() const override;
    std::u16string GetCompletedStatusText() const override;
    std::u16string GetInterruptedStatusText(
        offline_items_collection::FailState fail_state) const override;
    std::u16string GetProgressSizesString() const override;

   private:
    FRIEND_TEST_ALL_PREFIXES(DownloadItemModelTest,
                             GetBubbleStatusMessageWithBytes);

    static std::u16string GetBubbleStatusMessageWithBytes(
        const std::u16string& bytes_substring,
        const std::u16string& detail_message,
        bool is_active);
    std::u16string GetBubbleWarningStatusText() const;
  };
~~~
###  OnDownloadUpdated

 Delegate for a single DownloadUIModel.

~~~cpp
class Delegate {
   public:
    virtual void OnDownloadUpdated() {}
    virtual void OnDownloadOpened() {}
    virtual void OnDownloadDestroyed(const ContentId& id) {}

    virtual ~Delegate() = default;
  };
~~~
### SetDelegate

DownloadUIModel::SetDelegate
~~~cpp
void SetDelegate(Delegate* delegate);
~~~

### GetWeakPtr

DownloadUIModel::GetWeakPtr
~~~cpp
base::WeakPtr<DownloadUIModel> GetWeakPtr();
~~~

### HasSupportedImageMimeType

DownloadUIModel::HasSupportedImageMimeType
~~~cpp
bool HasSupportedImageMimeType() const;
~~~
 Does this download have a MIME type (either explicit or inferred from its
 extension) suggesting that it is a supported image type?
### GetProgressSizesString

DownloadUIModel::GetProgressSizesString
~~~cpp
std::u16string GetProgressSizesString() const;
~~~
 Returns a string representation of the current download progress sizes.

 If the total size of the download is known, this string looks like:
 "100/200 MB" where the numerator is the transferred size and the
 denominator is the total size. If the total isn't known, returns the
 transferred size as a string (e.g.: "100 MB").

### GetInterruptDescription

DownloadUIModel::GetInterruptDescription
~~~cpp
std::u16string GetInterruptDescription() const;
~~~
 Returns a long descriptive string for a download that's in the INTERRUPTED
 state. For other downloads, the returned string will be empty.

### GetHistoryPageStatusText

DownloadUIModel::GetHistoryPageStatusText
~~~cpp
std::u16string GetHistoryPageStatusText() const;
~~~
 Returns a status string for the download history page.

### GetStatusText

DownloadUIModel::GetStatusText
~~~cpp
std::u16string GetStatusText() const;
~~~
 Returns a short one-line status string for the download.

### GetTooltipText

DownloadUIModel::GetTooltipText
~~~cpp
std::u16string GetTooltipText() const;
~~~
 Returns a string suitable for use as a tooltip. For a regular download, the
 tooltip is the filename. For an interrupted download, the string states the
 filename and a short description of the reason for interruption. For
 example:
    Report.pdf
    Network disconnected
### GetWarningText

DownloadUIModel::GetWarningText
~~~cpp
std::u16string GetWarningText(const std::u16string& filename,
                                size_t* offset) const;
~~~
 Get the warning text to display for a dangerous download. |filename| is the
 (possibly-elided) filename; if it is present in the resulting string,
 |offset| will be set to the starting position of the filename.

### GetWarningConfirmButtonText

DownloadUIModel::GetWarningConfirmButtonText
~~~cpp
std::u16string GetWarningConfirmButtonText() const;
~~~
 Get the caption text for a button for confirming a dangerous download
 warning.

### GetShowInFolderText

DownloadUIModel::GetShowInFolderText
~~~cpp
std::u16string GetShowInFolderText() const;
~~~
 Get the text to display for the button to show item in folder on download
 history page.

### profile

DownloadUIModel::profile
~~~cpp
virtual Profile* profile() const;
~~~
 Returns the profile associated with this download.

### GetContentId

DownloadUIModel::GetContentId
~~~cpp
virtual ContentId GetContentId() const;
~~~
 Returns the content id associated with this download.

### GetTabProgressStatusText

DownloadUIModel::GetTabProgressStatusText
~~~cpp
virtual std::u16string GetTabProgressStatusText() const;
~~~
 Returns the localized status text for an in-progress download. This
 is the progress status used in the WebUI interface.

### GetCompletedBytes

DownloadUIModel::GetCompletedBytes
~~~cpp
virtual int64_t GetCompletedBytes() const;
~~~
 Get the number of bytes that has completed so far.

### GetTotalBytes

DownloadUIModel::GetTotalBytes
~~~cpp
virtual int64_t GetTotalBytes() const;
~~~
 Get the total number of bytes for this download. Should return 0 if the
 total size of the download is not known. Virtual for testing.

### PercentComplete

DownloadUIModel::PercentComplete
~~~cpp
virtual int PercentComplete() const;
~~~
 Rough percent complete. Returns -1 if the progress is unknown.

### IsDangerous

DownloadUIModel::IsDangerous
~~~cpp
virtual bool IsDangerous() const;
~~~
 Is this considered a dangerous download?
### MightBeMalicious

DownloadUIModel::MightBeMalicious
~~~cpp
virtual bool MightBeMalicious() const;
~~~
 Is this considered a malicious download? Implies IsDangerous().

### IsMalicious

DownloadUIModel::IsMalicious
~~~cpp
virtual bool IsMalicious() const;
~~~
 Is this considered a malicious download with very high confidence?
 Implies IsDangerous() and MightBeMalicious().

### IsInsecure

DownloadUIModel::IsInsecure
~~~cpp
virtual bool IsInsecure() const;
~~~
 Is this download an insecure download, but not something more severe?
 Implies IsDangerous() and !IsMalicious().

### ShouldRemoveFromShelfWhenComplete

DownloadUIModel::ShouldRemoveFromShelfWhenComplete
~~~cpp
virtual bool ShouldRemoveFromShelfWhenComplete() const;
~~~
 Returns |true| if this download is expected to complete successfully and
 thereafter be removed from the shelf.  Downloads that are opened
 automatically or are temporary will be removed from the shelf on successful
 completion.


 Returns |false| if the download is not expected to complete (interrupted,
 cancelled, dangerous, malicious), or won't be removed on completion.


 Since the expectation of successful completion may change, the return value
 of this function will change over the course of a download.

### ShouldShowDownloadStartedAnimation

DownloadUIModel::ShouldShowDownloadStartedAnimation
~~~cpp
virtual bool ShouldShowDownloadStartedAnimation() const;
~~~
 Returns |true| if the download started animation (big download arrow
 animates down towards the shelf) should be displayed for this download.

 Downloads that were initiated via "Save As" or are extension installs don't
 show the animation.

### ShouldShowInShelf

DownloadUIModel::ShouldShowInShelf
~~~cpp
virtual bool ShouldShowInShelf() const;
~~~
 Returns |true| if this download should be displayed in the downloads shelf.

### SetShouldShowInShelf

DownloadUIModel::SetShouldShowInShelf
~~~cpp
virtual void SetShouldShowInShelf(bool should_show);
~~~
 Change whether the download should be displayed on the downloads
 shelf. Setting this is only effective if the download hasn't already been
 displayed in the shelf.

### ShouldNotifyUI

DownloadUIModel::ShouldNotifyUI
~~~cpp
virtual bool ShouldNotifyUI() const;
~~~
 Returns |true| if the UI should be notified when the download is ready to
 be presented in the UI. Note that this is independent of
 ShouldShowInShelf() since there might be actions other than showing in the
 shelf that the UI must perform.

### WasUINotified

DownloadUIModel::WasUINotified
~~~cpp
virtual bool WasUINotified() const;
~~~
 Returns |true| if the UI has been notified about this download. By default,
 this value is |false| and should be changed explicitly using
 SetWasUINotified().

### SetWasUINotified

DownloadUIModel::SetWasUINotified
~~~cpp
virtual void SetWasUINotified(bool should_notify);
~~~
 Change what's returned by WasUINotified().

### WasActionedOn

DownloadUIModel::WasActionedOn
~~~cpp
virtual bool WasActionedOn() const;
~~~
 Returns |true| if the download was actioned on. This governs if the
 download should be shown in the Download Bubble's partial view.

### SetActionedOn

DownloadUIModel::SetActionedOn
~~~cpp
virtual void SetActionedOn(bool actioned_on);
~~~
 Change what's returned by WasActionedOn().

### WasUIWarningShown

DownloadUIModel::WasUIWarningShown
~~~cpp
virtual bool WasUIWarningShown() const;
~~~
 Returns |true| if the Download Bubble UI has shown this download warning.

 By default, this value is |false| and should be changed explicitly using
 SetWasUIWarningShown().

### SetWasUIWarningShown

DownloadUIModel::SetWasUIWarningShown
~~~cpp
virtual void SetWasUIWarningShown(bool was_ui_warning_shown);
~~~
 Change what's returned by WasUIWarningShown().

### GetEphemeralWarningUiShownTime

DownloadUIModel::GetEphemeralWarningUiShownTime
~~~cpp
virtual absl::optional<base::Time> GetEphemeralWarningUiShownTime() const;
~~~
 If this is an ephemeral warning, returns when the bubble first displayed
 the warning. If the warning has not yet shown (or this isn't an ephemeral
 warning), it returns no value. This does not persist across restarts.

### SetEphemeralWarningUiShownTime

DownloadUIModel::SetEphemeralWarningUiShownTime
~~~cpp
virtual void SetEphemeralWarningUiShownTime(absl::optional<base::Time> time);
~~~

### ShouldPreferOpeningInBrowser

DownloadUIModel::ShouldPreferOpeningInBrowser
~~~cpp
virtual bool ShouldPreferOpeningInBrowser();
~~~
 Returns |true| if opening in the browser is preferred for this download. If
 |false|, the download should be opened with the system default application.

### SetShouldPreferOpeningInBrowser

DownloadUIModel::SetShouldPreferOpeningInBrowser
~~~cpp
virtual void SetShouldPreferOpeningInBrowser(bool preference);
~~~
 Change what's returned by ShouldPreferOpeningInBrowser to |preference|.

### GetDangerLevel

DownloadUIModel::GetDangerLevel
~~~cpp
virtual safe_browsing::DownloadFileType::DangerLevel GetDangerLevel() const;
~~~
 Return the danger level determined during download target determination.

 The value returned here is independent of the danger level as determined by
 the Safe Browsing.

### SetDangerLevel

DownloadUIModel::SetDangerLevel
~~~cpp
virtual void SetDangerLevel(
      safe_browsing::DownloadFileType::DangerLevel danger_level);
~~~
 Change what's returned by GetDangerLevel().

### GetInsecureDownloadStatus

DownloadUIModel::GetInsecureDownloadStatus
~~~cpp
virtual download::DownloadItem::InsecureDownloadStatus
  GetInsecureDownloadStatus() const;
~~~
 Return the mixed content status determined during download target
 determination.

### OpenUsingPlatformHandler

DownloadUIModel::OpenUsingPlatformHandler
~~~cpp
virtual void OpenUsingPlatformHandler();
~~~
 Open the download using the platform handler for the download. The behavior
 of this method will be different from DownloadItem::OpenDownload() if
 ShouldPreferOpeningInBrowser().

### IsBeingRevived

DownloadUIModel::IsBeingRevived
~~~cpp
virtual bool IsBeingRevived() const;
~~~
 Whether the download was removed and this is currently being undone.

### SetIsBeingRevived

DownloadUIModel::SetIsBeingRevived
~~~cpp
virtual void SetIsBeingRevived(bool is_being_revived);
~~~
 Set whether the download is being revived.

### GetDownloadItem

DownloadUIModel::GetDownloadItem
~~~cpp
virtual const download::DownloadItem* GetDownloadItem() const;
~~~
 Returns the DownloadItem if this is a regular download, or nullptr
 otherwise.

### GetDownloadItem

DownloadUIModel::GetDownloadItem
~~~cpp
download::DownloadItem* GetDownloadItem();
~~~

### GetFileNameToReportUser

DownloadUIModel::GetFileNameToReportUser
~~~cpp
virtual base::FilePath GetFileNameToReportUser() const;
~~~
 Returns the file-name that should be reported to the user.

### GetTargetFilePath

DownloadUIModel::GetTargetFilePath
~~~cpp
virtual base::FilePath GetTargetFilePath() const;
~~~
 Target path of an in-progress download.

 May be empty if the target path hasn't yet been determined.

### OpenDownload

DownloadUIModel::OpenDownload
~~~cpp
virtual void OpenDownload();
~~~
 Opens the file associated with this download.  If the download is
 still in progress, marks the download to be opened when it is complete.

### GetState

DownloadUIModel::GetState
~~~cpp
virtual download::DownloadItem::DownloadState GetState() const;
~~~
 Returns the current state of the download.

### IsPaused

DownloadUIModel::IsPaused
~~~cpp
virtual bool IsPaused() const;
~~~
 Returns whether the download is currently paused.

### GetDangerType

DownloadUIModel::GetDangerType
~~~cpp
virtual download::DownloadDangerType GetDangerType() const;
~~~
 Returns the danger type associated with this download.

### GetOpenWhenComplete

DownloadUIModel::GetOpenWhenComplete
~~~cpp
virtual bool GetOpenWhenComplete() const;
~~~
 Returns true if the download will be auto-opened when complete.

### IsOpenWhenCompleteByPolicy

DownloadUIModel::IsOpenWhenCompleteByPolicy
~~~cpp
virtual bool IsOpenWhenCompleteByPolicy() const;
~~~
 Returns true if the download will be auto-opened when complete by policy.

### TimeRemaining

DownloadUIModel::TimeRemaining
~~~cpp
virtual bool TimeRemaining(base::TimeDelta* remaining) const;
~~~
 Simple calculation of the amount of time remaining to completion. Fills
 |*remaining| with the amount of time remaining if successful. Fails and
 returns false if we do not have the number of bytes or the download speed,
 and so can't give an estimate.

### GetStartTime

DownloadUIModel::GetStartTime
~~~cpp
virtual base::Time GetStartTime() const;
~~~
 Returns the creation time for a download.

### GetEndTime

DownloadUIModel::GetEndTime
~~~cpp
virtual base::Time GetEndTime() const;
~~~
 Returns the end/completion time for a completed download. base::Time()
 if the download has not completed yet.

### GetOpened

DownloadUIModel::GetOpened
~~~cpp
virtual bool GetOpened() const;
~~~
 Returns true if the download has been opened.

### SetOpened

DownloadUIModel::SetOpened
~~~cpp
virtual void SetOpened(bool opened);
~~~
 Marks the download as having been opened (without actually opening it).

### IsDone

DownloadUIModel::IsDone
~~~cpp
virtual bool IsDone() const;
~~~
 Returns true if the download is in a terminal state. This includes
 completed downloads, cancelled downloads, and interrupted downloads that
 can't be resumed.

### Pause

DownloadUIModel::Pause
~~~cpp
virtual void Pause();
~~~
 Pauses a download.  Will have no effect if the download is already
 paused.

### Resume

DownloadUIModel::Resume
~~~cpp
virtual void Resume();
~~~
 Resumes a download that has been paused or interrupted. Will have no effect
 if the download is neither. Only does something if CanResume() returns
 true.

### Cancel

DownloadUIModel::Cancel
~~~cpp
virtual void Cancel(bool user_cancel);
~~~
 Cancels the download operation. Set |user_cancel| to true if the
 cancellation was triggered by an explicit user action.

### Remove

DownloadUIModel::Remove
~~~cpp
virtual void Remove();
~~~
 Removes the download from the views and history. If the download was
 in-progress or interrupted, then the intermediate file will also be
 deleted.

### SetOpenWhenComplete

DownloadUIModel::SetOpenWhenComplete
~~~cpp
virtual void SetOpenWhenComplete(bool open);
~~~
 Marks the download to be auto-opened when completed.

### GetFullPath

DownloadUIModel::GetFullPath
~~~cpp
virtual base::FilePath GetFullPath() const;
~~~
 Returns the full path to the downloaded or downloading file. This is the
 path to the physical file, if one exists.

### CanResume

DownloadUIModel::CanResume
~~~cpp
virtual bool CanResume() const;
~~~
 Returns whether the download can be resumed.

### AllDataSaved

DownloadUIModel::AllDataSaved
~~~cpp
virtual bool AllDataSaved() const;
~~~
 Returns whether this download has saved all of its data.

### GetFileExternallyRemoved

DownloadUIModel::GetFileExternallyRemoved
~~~cpp
virtual bool GetFileExternallyRemoved() const;
~~~
 Returns whether the file associated with the download has been removed by
 external action.

### GetURL

DownloadUIModel::GetURL
~~~cpp
virtual GURL GetURL() const;
~~~
 Returns the URL represented by this download.

### HasUserGesture

DownloadUIModel::HasUserGesture
~~~cpp
virtual bool HasUserGesture() const;
~~~
 Returns whether the download request was initiated in response to a user
 gesture.

### GetLastFailState

DownloadUIModel::GetLastFailState
~~~cpp
virtual offline_items_collection::FailState GetLastFailState() const;
~~~
 Returns the most recent failure reason for this download. Returns
 |FailState::NO_FAILURE| if there is no previous failure reason.

### GetOriginalURL

DownloadUIModel::GetOriginalURL
~~~cpp
virtual GURL GetOriginalURL() const;
~~~
 Returns the URL of the orginiating request.

### ShouldPromoteOrigin

DownloadUIModel::ShouldPromoteOrigin
~~~cpp
virtual bool ShouldPromoteOrigin() const;
~~~
 Whether the Origin should be clearly displayed in the notification for
 security reasons.

### ShouldShowDropdown

DownloadUIModel::ShouldShowDropdown
~~~cpp
virtual bool ShouldShowDropdown() const;
~~~
 Whether the dropdown menu button should be shown or not.

### DetermineAndSetShouldPreferOpeningInBrowser

DownloadUIModel::DetermineAndSetShouldPreferOpeningInBrowser
~~~cpp
virtual void DetermineAndSetShouldPreferOpeningInBrowser(
      const base::FilePath& target_path,
      bool is_filetype_handled_safely);
~~~
 Determines if a download should be preferably opened in the browser instead
 of the platform. Use |is_filetype_handled_safely| indicating if opening a
 file of this type is safe in the current BrowserContext, |target_path| to
 see if files of this type should be opened in the browser, and set whether
 the download should be preferred opening in the browser.

### GetInProgressAccessibleAlertText

DownloadUIModel::GetInProgressAccessibleAlertText
~~~cpp
virtual std::u16string GetInProgressAccessibleAlertText() const;
~~~
 Returns the accessible alert text that should be announced when the
 download is in progress.

### GetMimeType

DownloadUIModel::GetMimeType
~~~cpp
virtual std::string GetMimeType() const;
~~~
 Returns the MIME type of the download.

### IsExtensionDownload

DownloadUIModel::IsExtensionDownload
~~~cpp
virtual bool IsExtensionDownload() const;
~~~
 Returns whether the download is triggered by an extension.

### delegate_



~~~cpp

raw_ptr<Delegate> delegate_ = nullptr;

~~~


### set_clock_for_testing

DownloadUIModel::set_clock_for_testing
~~~cpp
void set_clock_for_testing(base::Clock* clock);
~~~

### set_status_text_builder_for_testing

DownloadUIModel::set_status_text_builder_for_testing
~~~cpp
void set_status_text_builder_for_testing(bool for_bubble);
~~~

###  base::DefaultClock::GetInstance

 Unowned Clock to override the time of "Now".

~~~cpp
raw_ptr<base::Clock> clock_ = base::DefaultClock::GetInstance();
~~~
### status_text_builder_



~~~cpp

std::unique_ptr<StatusTextBuilderBase> status_text_builder_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadUIModel> weak_ptr_factory_{this};

~~~

