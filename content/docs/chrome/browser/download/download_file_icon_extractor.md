
## class DownloadFileIconExtractor
 Helper class for DownloadsGetFileIconFunction. Only used for a single icon
 extraction.

### ~DownloadFileIconExtractor

~DownloadFileIconExtractor
~~~cpp
virtual ~DownloadFileIconExtractor() {}
~~~

### ExtractIconURLForPath

ExtractIconURLForPath
~~~cpp
virtual bool ExtractIconURLForPath(const base::FilePath& path,
                                     float scale,
                                     IconLoader::IconSize icon_size,
                                     IconURLCallback callback) = 0;
~~~
 Should return false if the request was invalid.  If the return value is
 true, then |callback| should be called with the result.

### ~DownloadFileIconExtractor

~DownloadFileIconExtractor
~~~cpp
virtual ~DownloadFileIconExtractor() {}
~~~

### ExtractIconURLForPath

ExtractIconURLForPath
~~~cpp
virtual bool ExtractIconURLForPath(const base::FilePath& path,
                                     float scale,
                                     IconLoader::IconSize icon_size,
                                     IconURLCallback callback) = 0;
~~~
 Should return false if the request was invalid.  If the return value is
 true, then |callback| should be called with the result.
