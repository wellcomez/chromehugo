
## class MultiProfileDownloadNotifier
 namespace download
###  Client


~~~cpp
class Client {
   public:
    Client() = default;
    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;
    virtual ~Client() = default;

    // Each method is called with the relevant download manager for use in a
    // client's auxiliary data structures, if applicable.
    virtual void OnManagerInitialized(content::DownloadManager* manager) {}
    virtual void OnManagerGoingDown(content::DownloadManager* manager) {}
    virtual void OnDownloadCreated(content::DownloadManager* manager,
                                   download::DownloadItem* item) {}
    virtual void OnDownloadUpdated(content::DownloadManager* manager,
                                   download::DownloadItem* item) {}
    virtual void OnDownloadDestroyed(content::DownloadManager* manager,
                                     download::DownloadItem* item) {}

    // Specifies whether the client wants to be notified for downloads created
    // on `profile`. This function is called before observing any profile,
    // regardless of whether `profile` was added automatically as the
    // off-the-record child of an observed profile or the client added it
    // explicitly using `AddProfile()`.
    virtual bool ShouldObserveProfile(Profile* profile);
  };
~~~
### MultiProfileDownloadNotifier

MultiProfileDownloadNotifier::MultiProfileDownloadNotifier
~~~cpp
MultiProfileDownloadNotifier(Client* client,
                               bool wait_for_manager_initialization);
~~~
 `wait_for_manager_initialization` controls whether `client` will be
 notified about downloads belonging to `Profile`s with uninitialized
 `DownloadManager`s.

### MultiProfileDownloadNotifier

MultiProfileDownloadNotifier
~~~cpp
MultiProfileDownloadNotifier(const MultiProfileDownloadNotifier&) = delete;
~~~

### operator=

operator=
~~~cpp
MultiProfileDownloadNotifier& operator=(const MultiProfileDownloadNotifier&) =
      delete;
~~~

### ~MultiProfileDownloadNotifier

MultiProfileDownloadNotifier::~MultiProfileDownloadNotifier
~~~cpp
~MultiProfileDownloadNotifier() override;
~~~

### AddProfile

MultiProfileDownloadNotifier::AddProfile
~~~cpp
void AddProfile(Profile* profile);
~~~
 Creates a download notifier for the download manager associated with
 `profile` if one does not already exist.

### GetAllDownloads

MultiProfileDownloadNotifier::GetAllDownloads
~~~cpp
std::vector<download::DownloadItem*> GetAllDownloads();
~~~
 Returns all downloads for all observed profiles.

### GetDownloadByGuid

MultiProfileDownloadNotifier::GetDownloadByGuid
~~~cpp
download::DownloadItem* GetDownloadByGuid(const std::string& guid);
~~~
 Searches all download notifiers for an observed `DownloadItem` matching
 `guid`. Returns the item if found or nullptr if none exists. Note that this
 function will return a matching download item even if it belongs to an
 uninitialized manager and `wait_for_manager_initialization_` is true.

### OnOffTheRecordProfileCreated

MultiProfileDownloadNotifier::OnOffTheRecordProfileCreated
~~~cpp
void OnOffTheRecordProfileCreated(Profile* off_the_record) override;
~~~
 ProfileObserver:
### OnProfileWillBeDestroyed

MultiProfileDownloadNotifier::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~

### OnManagerInitialized

MultiProfileDownloadNotifier::OnManagerInitialized
~~~cpp
void OnManagerInitialized(content::DownloadManager* manager) override;
~~~
 download::AllDownloadItemNotifier::Observer:
### OnManagerGoingDown

MultiProfileDownloadNotifier::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnDownloadCreated

MultiProfileDownloadNotifier::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

MultiProfileDownloadNotifier::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadDestroyed

MultiProfileDownloadNotifier::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(content::DownloadManager* manager,
                           download::DownloadItem* item) override;
~~~

### IsManagerReady

MultiProfileDownloadNotifier::IsManagerReady
~~~cpp
bool IsManagerReady(content::DownloadManager* manager);
~~~
 Helper function that makes sure a `DownloadManager` is initialized if
 `client_` requires it to be.

### client_



~~~cpp

const raw_ptr<MultiProfileDownloadNotifier::Client> client_;

~~~


### wait_for_manager_initialization_



~~~cpp

const bool wait_for_manager_initialization_;

~~~


### download_notifiers_



~~~cpp

std::set<std::unique_ptr<download::AllDownloadItemNotifier>,
           base::UniquePtrComparator>
      download_notifiers_;

~~~


### profile_observer_



~~~cpp

base::ScopedMultiSourceObservation<Profile, ProfileObserver>
      profile_observer_{this};

~~~


### MultiProfileDownloadNotifier

MultiProfileDownloadNotifier
~~~cpp
MultiProfileDownloadNotifier(const MultiProfileDownloadNotifier&) = delete;
~~~

### operator=

operator=
~~~cpp
MultiProfileDownloadNotifier& operator=(const MultiProfileDownloadNotifier&) =
      delete;
~~~

###  Client


~~~cpp
class Client {
   public:
    Client() = default;
    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;
    virtual ~Client() = default;

    // Each method is called with the relevant download manager for use in a
    // client's auxiliary data structures, if applicable.
    virtual void OnManagerInitialized(content::DownloadManager* manager) {}
    virtual void OnManagerGoingDown(content::DownloadManager* manager) {}
    virtual void OnDownloadCreated(content::DownloadManager* manager,
                                   download::DownloadItem* item) {}
    virtual void OnDownloadUpdated(content::DownloadManager* manager,
                                   download::DownloadItem* item) {}
    virtual void OnDownloadDestroyed(content::DownloadManager* manager,
                                     download::DownloadItem* item) {}

    // Specifies whether the client wants to be notified for downloads created
    // on `profile`. This function is called before observing any profile,
    // regardless of whether `profile` was added automatically as the
    // off-the-record child of an observed profile or the client added it
    // explicitly using `AddProfile()`.
    virtual bool ShouldObserveProfile(Profile* profile);
  };
~~~
### AddProfile

MultiProfileDownloadNotifier::AddProfile
~~~cpp
void AddProfile(Profile* profile);
~~~
 Creates a download notifier for the download manager associated with
 `profile` if one does not already exist.

### GetAllDownloads

MultiProfileDownloadNotifier::GetAllDownloads
~~~cpp
std::vector<download::DownloadItem*> GetAllDownloads();
~~~
 Returns all downloads for all observed profiles.

### GetDownloadByGuid

MultiProfileDownloadNotifier::GetDownloadByGuid
~~~cpp
download::DownloadItem* GetDownloadByGuid(const std::string& guid);
~~~
 Searches all download notifiers for an observed `DownloadItem` matching
 `guid`. Returns the item if found or nullptr if none exists. Note that this
 function will return a matching download item even if it belongs to an
 uninitialized manager and `wait_for_manager_initialization_` is true.

### OnOffTheRecordProfileCreated

MultiProfileDownloadNotifier::OnOffTheRecordProfileCreated
~~~cpp
void OnOffTheRecordProfileCreated(Profile* off_the_record) override;
~~~
 ProfileObserver:
### OnProfileWillBeDestroyed

MultiProfileDownloadNotifier::OnProfileWillBeDestroyed
~~~cpp
void OnProfileWillBeDestroyed(Profile* profile) override;
~~~

### OnManagerInitialized

MultiProfileDownloadNotifier::OnManagerInitialized
~~~cpp
void OnManagerInitialized(content::DownloadManager* manager) override;
~~~
 download::AllDownloadItemNotifier::Observer:
### OnManagerGoingDown

MultiProfileDownloadNotifier::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnDownloadCreated

MultiProfileDownloadNotifier::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

MultiProfileDownloadNotifier::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadDestroyed

MultiProfileDownloadNotifier::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(content::DownloadManager* manager,
                           download::DownloadItem* item) override;
~~~

### IsManagerReady

MultiProfileDownloadNotifier::IsManagerReady
~~~cpp
bool IsManagerReady(content::DownloadManager* manager);
~~~
 Helper function that makes sure a `DownloadManager` is initialized if
 `client_` requires it to be.

### client_



~~~cpp

const raw_ptr<MultiProfileDownloadNotifier::Client> client_;

~~~


### wait_for_manager_initialization_



~~~cpp

const bool wait_for_manager_initialization_;

~~~


### download_notifiers_



~~~cpp

std::set<std::unique_ptr<download::AllDownloadItemNotifier>,
           base::UniquePtrComparator>
      download_notifiers_;

~~~


### profile_observer_



~~~cpp

base::ScopedMultiSourceObservation<Profile, ProfileObserver>
      profile_observer_{this};

~~~

