
## class DownloadNotificationManager

### DownloadNotificationManager

DownloadNotificationManager::DownloadNotificationManager
~~~cpp
explicit DownloadNotificationManager(Profile* profile);
~~~

### DownloadNotificationManager

DownloadNotificationManager
~~~cpp
DownloadNotificationManager(const DownloadNotificationManager&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadNotificationManager& operator=(const DownloadNotificationManager&) =
      delete;
~~~

### ~DownloadNotificationManager

DownloadNotificationManager::~DownloadNotificationManager
~~~cpp
~DownloadNotificationManager() override;
~~~

### OnNewDownloadReady

DownloadNotificationManager::OnNewDownloadReady
~~~cpp
void OnNewDownloadReady(download::DownloadItem* item) override;
~~~
 DownloadUIController::Delegate overrides.

### OnDownloadDestroyed

DownloadNotificationManager::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& contentId) override;
~~~
 DownloadItemNotification::Observer overrides.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### items_



~~~cpp

std::map<ContentId, std::unique_ptr<DownloadItemNotification>> items_;

~~~


### DownloadNotificationManager

DownloadNotificationManager
~~~cpp
DownloadNotificationManager(const DownloadNotificationManager&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadNotificationManager& operator=(const DownloadNotificationManager&) =
      delete;
~~~

### OnNewDownloadReady

DownloadNotificationManager::OnNewDownloadReady
~~~cpp
void OnNewDownloadReady(download::DownloadItem* item) override;
~~~
 DownloadUIController::Delegate overrides.

### OnDownloadDestroyed

DownloadNotificationManager::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& contentId) override;
~~~
 DownloadItemNotification::Observer overrides.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### items_



~~~cpp

std::map<ContentId, std::unique_ptr<DownloadItemNotification>> items_;

~~~

