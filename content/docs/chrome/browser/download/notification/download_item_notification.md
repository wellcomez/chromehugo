
## class DownloadItemNotification
 Handles the notification on ChromeOS for one download item.

### DownloadItemNotification

DownloadItemNotification::DownloadItemNotification
~~~cpp
DownloadItemNotification(Profile* profile,
                           DownloadUIModel::DownloadUIModelPtr item);
~~~

### DownloadItemNotification

DownloadItemNotification
~~~cpp
DownloadItemNotification(const DownloadItemNotification&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadItemNotification& operator=(const DownloadItemNotification&) = delete;
~~~

### ~DownloadItemNotification

DownloadItemNotification::~DownloadItemNotification
~~~cpp
~DownloadItemNotification() override;
~~~

###  OnDownloadDestroyed

 Observer for this notification.

~~~cpp
class Observer {
   public:
    virtual void OnDownloadDestroyed(const ContentId& contentId) {}
  };
~~~
### SetObserver

DownloadItemNotification::SetObserver
~~~cpp
void SetObserver(Observer* observer);
~~~
 Set an observer for this notification.

### GetDownload

DownloadItemNotification::GetDownload
~~~cpp
DownloadUIModel* GetDownload();
~~~

### OnDownloadUpdated

DownloadItemNotification::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated() override;
~~~
 DownloadUIModel::Delegate overrides.

### OnDownloadDestroyed

DownloadItemNotification::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& id) override;
~~~

### DisablePopup

DownloadItemNotification::DisablePopup
~~~cpp
void DisablePopup();
~~~
 Disables popup by setting low priority.

### Close

DownloadItemNotification::Close
~~~cpp
void Close(bool by_user) override;
~~~
 NotificationObserver:
### Click

DownloadItemNotification::Click
~~~cpp
void Click(const absl::optional<int>& button_index,
             const absl::optional<std::u16string>& reply) override;
~~~

### enum ImageDecodeStatus

~~~cpp
enum ImageDecodeStatus { NOT_STARTED, IN_PROGRESS, DONE, FAILED, NOT_IMAGE }
~~~
### enum NotificationUpdateType

~~~cpp
enum NotificationUpdateType { ADD, UPDATE, UPDATE_AND_POPUP }
~~~
### GetNotificationId

DownloadItemNotification::GetNotificationId
~~~cpp
std::string GetNotificationId() const;
~~~

### CloseNotification

DownloadItemNotification::CloseNotification
~~~cpp
void CloseNotification();
~~~

### Update

DownloadItemNotification::Update
~~~cpp
void Update();
~~~

### UpdateNotificationData

DownloadItemNotification::UpdateNotificationData
~~~cpp
void UpdateNotificationData(bool display, bool force_pop_up);
~~~

### GetNotificationIconColor

DownloadItemNotification::GetNotificationIconColor
~~~cpp
SkColor GetNotificationIconColor();
~~~

### OnImageLoaded

DownloadItemNotification::OnImageLoaded
~~~cpp
void OnImageLoaded(std::string image_data);
~~~
 Set preview image of the notification. Must be called on IO thread.

### OnImageCropped

DownloadItemNotification::OnImageCropped
~~~cpp
void OnImageCropped(const SkBitmap& image);
~~~

### OnImageDecoded

DownloadItemNotification::OnImageDecoded
~~~cpp
void OnImageDecoded(const SkBitmap& decoded_image) override;
~~~
 ImageDecoder::ImageRequest overrides:
### OnDecodeImageFailed

DownloadItemNotification::OnDecodeImageFailed
~~~cpp
void OnDecodeImageFailed() override;
~~~

### GetTitle

DownloadItemNotification::GetTitle
~~~cpp
std::u16string GetTitle() const;
~~~
 Returns a short one-line status string for the download.

### GetCommandLabel

DownloadItemNotification::GetCommandLabel
~~~cpp
std::u16string GetCommandLabel(DownloadCommands::Command command) const;
~~~
 Returns a short one-line status string for a download command.

### GetWarningStatusString

DownloadItemNotification::GetWarningStatusString
~~~cpp
std::u16string GetWarningStatusString() const;
~~~
 Get the warning text to notify a dangerous download. Should only be called
 if IsDangerous() is true.

### GetInProgressSubStatusString

DownloadItemNotification::GetInProgressSubStatusString
~~~cpp
std::u16string GetInProgressSubStatusString() const;
~~~
 Get the sub status text of the current in-progress download status. Should
 be called only for downloads in progress.

### GetSubStatusString

DownloadItemNotification::GetSubStatusString
~~~cpp
std::u16string GetSubStatusString() const;
~~~
 Get the sub status text. Can be called for downloads in all states.

 If the state does not have sub status string, it returns empty string.

### GetStatusString

DownloadItemNotification::GetStatusString
~~~cpp
std::u16string GetStatusString() const;
~~~
 Get the status text.

### IsScanning

DownloadItemNotification::IsScanning
~~~cpp
bool IsScanning() const;
~~~

### AllowedToOpenWhileScanning

DownloadItemNotification::AllowedToOpenWhileScanning
~~~cpp
bool AllowedToOpenWhileScanning() const;
~~~

### IsGalleryAppPdfEditNotificationEligible

DownloadItemNotification::IsGalleryAppPdfEditNotificationEligible
~~~cpp
bool IsGalleryAppPdfEditNotificationEligible() const;
~~~

### GetBrowser

DownloadItemNotification::GetBrowser
~~~cpp
Browser* GetBrowser() const;
~~~

### profile

DownloadItemNotification::profile
~~~cpp
Profile* profile() const;
~~~

### GetExtraActions

DownloadItemNotification::GetExtraActions
~~~cpp
std::unique_ptr<std::vector<DownloadCommands::Command>> GetExtraActions()
      const;
~~~
 Returns the list of possible extra (all except the default) actions.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~

 The profile associated with this notification.

### observer_



~~~cpp

raw_ptr<Observer> observer_;

~~~

 Observer of this notification.

### show_next_



~~~cpp

bool show_next_ = true;

~~~

 Flag to show the notification on next update. If true, the notification
 goes visible. The initial value is true so it gets shown on initial update.

### closed_



~~~cpp

bool closed_ = false;

~~~

 Flag if the notification has been closed or not. Setting this flag
 prevents updates after close.

### suppressed_



~~~cpp

bool suppressed_ = false;

~~~

 Flag if the notification has been suppressed or not. A notification being
 suppressed means that there is some special restriction imposed which is
 preventing a notification that would otherwise display from doing so, e.g.

 holding space in-progress downloads integration causes suppression of most
 download in-progress notifications.

### in_review_



~~~cpp

bool in_review_ = false;

~~~

 Flag to indicate that a review dialog is open for the user to accept or
 bypass an enterprise warning on the download. If this is true, the "Review"
 button should be removed from the notification.

### previous_download_state_



~~~cpp

download::DownloadItem::DownloadState previous_download_state_ =
      download::DownloadItem::MAX_DOWNLOAD_STATE;

~~~


### previous_dangerous_state_



~~~cpp

bool previous_dangerous_state_ = false;

~~~

 As uninitialized state
### previous_insecure_state_



~~~cpp

bool previous_insecure_state_ = false;

~~~


### notification_



~~~cpp

std::unique_ptr<message_center::Notification> notification_;

~~~


### item_



~~~cpp

DownloadUIModel::DownloadUIModelPtr item_;

~~~


### button_actions_



~~~cpp

std::unique_ptr<std::vector<DownloadCommands::Command>> button_actions_;

~~~


### image_decode_status_



~~~cpp

ImageDecodeStatus image_decode_status_ = NOT_STARTED;

~~~

 Status of the preview image decode.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadItemNotification> weak_factory_{this};

~~~


### DownloadItemNotification

DownloadItemNotification
~~~cpp
DownloadItemNotification(const DownloadItemNotification&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadItemNotification& operator=(const DownloadItemNotification&) = delete;
~~~

###  OnDownloadDestroyed

 Observer for this notification.

~~~cpp
class Observer {
   public:
    virtual void OnDownloadDestroyed(const ContentId& contentId) {}
  };
~~~
### SetObserver

DownloadItemNotification::SetObserver
~~~cpp
void SetObserver(Observer* observer);
~~~
 Set an observer for this notification.

### GetDownload

DownloadItemNotification::GetDownload
~~~cpp
DownloadUIModel* GetDownload();
~~~

### OnDownloadUpdated

DownloadItemNotification::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated() override;
~~~
 DownloadUIModel::Delegate overrides.

### OnDownloadDestroyed

DownloadItemNotification::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(const ContentId& id) override;
~~~

### DisablePopup

DownloadItemNotification::DisablePopup
~~~cpp
void DisablePopup();
~~~
 Disables popup by setting low priority.

### Close

DownloadItemNotification::Close
~~~cpp
void Close(bool by_user) override;
~~~
 NotificationObserver:
### Click

DownloadItemNotification::Click
~~~cpp
void Click(const absl::optional<int>& button_index,
             const absl::optional<std::u16string>& reply) override;
~~~

### enum ImageDecodeStatus

~~~cpp
enum ImageDecodeStatus { NOT_STARTED, IN_PROGRESS, DONE, FAILED, NOT_IMAGE };
~~~
### enum NotificationUpdateType

~~~cpp
enum NotificationUpdateType { ADD, UPDATE, UPDATE_AND_POPUP };
~~~
### GetNotificationId

DownloadItemNotification::GetNotificationId
~~~cpp
std::string GetNotificationId() const;
~~~

### CloseNotification

DownloadItemNotification::CloseNotification
~~~cpp
void CloseNotification();
~~~

### Update

DownloadItemNotification::Update
~~~cpp
void Update();
~~~

### UpdateNotificationData

DownloadItemNotification::UpdateNotificationData
~~~cpp
void UpdateNotificationData(bool display, bool force_pop_up);
~~~

### GetNotificationIconColor

DownloadItemNotification::GetNotificationIconColor
~~~cpp
SkColor GetNotificationIconColor();
~~~

### OnImageLoaded

DownloadItemNotification::OnImageLoaded
~~~cpp
void OnImageLoaded(std::string image_data);
~~~
 Set preview image of the notification. Must be called on IO thread.

### OnImageCropped

DownloadItemNotification::OnImageCropped
~~~cpp
void OnImageCropped(const SkBitmap& image);
~~~

### OnImageDecoded

DownloadItemNotification::OnImageDecoded
~~~cpp
void OnImageDecoded(const SkBitmap& decoded_image) override;
~~~
 ImageDecoder::ImageRequest overrides:
### OnDecodeImageFailed

DownloadItemNotification::OnDecodeImageFailed
~~~cpp
void OnDecodeImageFailed() override;
~~~

### GetTitle

DownloadItemNotification::GetTitle
~~~cpp
std::u16string GetTitle() const;
~~~
 Returns a short one-line status string for the download.

### GetCommandLabel

DownloadItemNotification::GetCommandLabel
~~~cpp
std::u16string GetCommandLabel(DownloadCommands::Command command) const;
~~~
 Returns a short one-line status string for a download command.

### GetWarningStatusString

DownloadItemNotification::GetWarningStatusString
~~~cpp
std::u16string GetWarningStatusString() const;
~~~
 Get the warning text to notify a dangerous download. Should only be called
 if IsDangerous() is true.

### GetInProgressSubStatusString

DownloadItemNotification::GetInProgressSubStatusString
~~~cpp
std::u16string GetInProgressSubStatusString() const;
~~~
 Get the sub status text of the current in-progress download status. Should
 be called only for downloads in progress.

### GetSubStatusString

DownloadItemNotification::GetSubStatusString
~~~cpp
std::u16string GetSubStatusString() const;
~~~
 Get the sub status text. Can be called for downloads in all states.

 If the state does not have sub status string, it returns empty string.

### GetStatusString

DownloadItemNotification::GetStatusString
~~~cpp
std::u16string GetStatusString() const;
~~~
 Get the status text.

### IsScanning

DownloadItemNotification::IsScanning
~~~cpp
bool IsScanning() const;
~~~

### AllowedToOpenWhileScanning

DownloadItemNotification::AllowedToOpenWhileScanning
~~~cpp
bool AllowedToOpenWhileScanning() const;
~~~

### IsGalleryAppPdfEditNotificationEligible

DownloadItemNotification::IsGalleryAppPdfEditNotificationEligible
~~~cpp
bool IsGalleryAppPdfEditNotificationEligible() const;
~~~

### GetBrowser

DownloadItemNotification::GetBrowser
~~~cpp
Browser* GetBrowser() const;
~~~

### profile

DownloadItemNotification::profile
~~~cpp
Profile* profile() const;
~~~

### GetExtraActions

DownloadItemNotification::GetExtraActions
~~~cpp
std::unique_ptr<std::vector<DownloadCommands::Command>> GetExtraActions()
      const;
~~~
 Returns the list of possible extra (all except the default) actions.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~

 The profile associated with this notification.

### observer_



~~~cpp

raw_ptr<Observer> observer_;

~~~

 Observer of this notification.

### show_next_



~~~cpp

bool show_next_ = true;

~~~

 Flag to show the notification on next update. If true, the notification
 goes visible. The initial value is true so it gets shown on initial update.

### closed_



~~~cpp

bool closed_ = false;

~~~

 Flag if the notification has been closed or not. Setting this flag
 prevents updates after close.

### suppressed_



~~~cpp

bool suppressed_ = false;

~~~

 Flag if the notification has been suppressed or not. A notification being
 suppressed means that there is some special restriction imposed which is
 preventing a notification that would otherwise display from doing so, e.g.

 holding space in-progress downloads integration causes suppression of most
 download in-progress notifications.

### in_review_



~~~cpp

bool in_review_ = false;

~~~

 Flag to indicate that a review dialog is open for the user to accept or
 bypass an enterprise warning on the download. If this is true, the "Review"
 button should be removed from the notification.

### previous_download_state_



~~~cpp

download::DownloadItem::DownloadState previous_download_state_ =
      download::DownloadItem::MAX_DOWNLOAD_STATE;

~~~


### previous_dangerous_state_



~~~cpp

bool previous_dangerous_state_ = false;

~~~

 As uninitialized state
### previous_insecure_state_



~~~cpp

bool previous_insecure_state_ = false;

~~~


### notification_



~~~cpp

std::unique_ptr<message_center::Notification> notification_;

~~~


### item_



~~~cpp

DownloadUIModel::DownloadUIModelPtr item_;

~~~


### button_actions_



~~~cpp

std::unique_ptr<std::vector<DownloadCommands::Command>> button_actions_;

~~~


### image_decode_status_



~~~cpp

ImageDecodeStatus image_decode_status_ = NOT_STARTED;

~~~

 Status of the preview image decode.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadItemNotification> weak_factory_{this};

~~~

