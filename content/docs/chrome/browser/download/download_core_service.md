
## class DownloadCoreService
 Abstract base class for the download core service; see
 DownloadCoreServiceImpl for implementation.

### DownloadCoreService

DownloadCoreService::DownloadCoreService
~~~cpp
DownloadCoreService();
~~~

### DownloadCoreService

DownloadCoreService
~~~cpp
DownloadCoreService(const DownloadCoreService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCoreService& operator=(const DownloadCoreService&) = delete;
~~~

### ~DownloadCoreService

DownloadCoreService::~DownloadCoreService
~~~cpp
~DownloadCoreService() override;
~~~

### GetDownloadManagerDelegate

GetDownloadManagerDelegate
~~~cpp
virtual ChromeDownloadManagerDelegate* GetDownloadManagerDelegate() = 0;
~~~
 Get the download manager delegate, creating it if it doesn't already exist.

### GetDownloadUIController

GetDownloadUIController
~~~cpp
virtual DownloadUIController* GetDownloadUIController() = 0;
~~~
 Get the download UI controller, return nullptr if it doesn't already exist.

### GetDownloadHistory

GetDownloadHistory
~~~cpp
virtual DownloadHistory* GetDownloadHistory() = 0;
~~~
 Get the interface to the history system. Returns NULL if profile is
 incognito or if the DownloadManager hasn't been created yet or if there is
 no HistoryService for profile. Virtual for testing.

### GetExtensionEventRouter

GetExtensionEventRouter
~~~cpp
virtual extensions::ExtensionDownloadsEventRouter*
  GetExtensionEventRouter() = 0;
~~~

### HasCreatedDownloadManager

HasCreatedDownloadManager
~~~cpp
virtual bool HasCreatedDownloadManager() = 0;
~~~
 Has a download manager been created?
### NonMaliciousDownloadCount

NonMaliciousDownloadCount
~~~cpp
virtual int NonMaliciousDownloadCount() const = 0;
~~~
 Number of non-malicious downloads associated with this instance of the
 service.

### CancelDownloads

CancelDownloads
~~~cpp
virtual void CancelDownloads() = 0;
~~~
 Cancels all in-progress downloads for this profile.

### NonMaliciousDownloadCountAllProfiles

DownloadCoreService::NonMaliciousDownloadCountAllProfiles
~~~cpp
static int NonMaliciousDownloadCountAllProfiles();
~~~
 Number of non-malicious downloads associated with all profiles.

### CancelAllDownloads

DownloadCoreService::CancelAllDownloads
~~~cpp
static void CancelAllDownloads();
~~~
 Cancels all in-progress downloads for all profiles.

### SetDownloadManagerDelegateForTesting

SetDownloadManagerDelegateForTesting
~~~cpp
virtual void SetDownloadManagerDelegateForTesting(
      std::unique_ptr<ChromeDownloadManagerDelegate> delegate) = 0;
~~~
 Sets the DownloadManagerDelegate associated with this object and
 its DownloadManager.  Takes ownership of |delegate|, and destroys
 the previous delegate.  For testing.

### SetDownloadHistoryForTesting

SetDownloadHistoryForTesting
~~~cpp
virtual void SetDownloadHistoryForTesting(
      std::unique_ptr<DownloadHistory> download_history) {}
~~~
 Sets the DownloadHistory associated with this object and
 its DownloadManager. Takes ownership of |download_history|, and destroys
 the previous delegate.  For testing.

### IsDownloadUiEnabled

IsDownloadUiEnabled
~~~cpp
virtual bool IsDownloadUiEnabled() = 0;
~~~
 Returns false if at least one extension has disabled the UI, true
 otherwise.

### DownloadCoreService

DownloadCoreService
~~~cpp
DownloadCoreService(const DownloadCoreService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCoreService& operator=(const DownloadCoreService&) = delete;
~~~

### GetDownloadManagerDelegate

GetDownloadManagerDelegate
~~~cpp
virtual ChromeDownloadManagerDelegate* GetDownloadManagerDelegate() = 0;
~~~
 Get the download manager delegate, creating it if it doesn't already exist.

### GetDownloadUIController

GetDownloadUIController
~~~cpp
virtual DownloadUIController* GetDownloadUIController() = 0;
~~~
 Get the download UI controller, return nullptr if it doesn't already exist.

### GetDownloadHistory

GetDownloadHistory
~~~cpp
virtual DownloadHistory* GetDownloadHistory() = 0;
~~~
 Get the interface to the history system. Returns NULL if profile is
 incognito or if the DownloadManager hasn't been created yet or if there is
 no HistoryService for profile. Virtual for testing.

### HasCreatedDownloadManager

HasCreatedDownloadManager
~~~cpp
virtual bool HasCreatedDownloadManager() = 0;
~~~
 Has a download manager been created?
### NonMaliciousDownloadCount

NonMaliciousDownloadCount
~~~cpp
virtual int NonMaliciousDownloadCount() const = 0;
~~~
 Number of non-malicious downloads associated with this instance of the
 service.

### CancelDownloads

CancelDownloads
~~~cpp
virtual void CancelDownloads() = 0;
~~~
 Cancels all in-progress downloads for this profile.

### SetDownloadManagerDelegateForTesting

SetDownloadManagerDelegateForTesting
~~~cpp
virtual void SetDownloadManagerDelegateForTesting(
      std::unique_ptr<ChromeDownloadManagerDelegate> delegate) = 0;
~~~
 Sets the DownloadManagerDelegate associated with this object and
 its DownloadManager.  Takes ownership of |delegate|, and destroys
 the previous delegate.  For testing.

### SetDownloadHistoryForTesting

SetDownloadHistoryForTesting
~~~cpp
virtual void SetDownloadHistoryForTesting(
      std::unique_ptr<DownloadHistory> download_history) {}
~~~
 Sets the DownloadHistory associated with this object and
 its DownloadManager. Takes ownership of |download_history|, and destroys
 the previous delegate.  For testing.

### IsDownloadUiEnabled

IsDownloadUiEnabled
~~~cpp
virtual bool IsDownloadUiEnabled() = 0;
~~~
 Returns false if at least one extension has disabled the UI, true
 otherwise.

### NonMaliciousDownloadCountAllProfiles

DownloadCoreService::NonMaliciousDownloadCountAllProfiles
~~~cpp
static int NonMaliciousDownloadCountAllProfiles();
~~~
 Number of non-malicious downloads associated with all profiles.

### CancelAllDownloads

DownloadCoreService::CancelAllDownloads
~~~cpp
static void CancelAllDownloads();
~~~
 Cancels all in-progress downloads for all profiles.
