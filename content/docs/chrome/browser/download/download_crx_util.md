### SetMockInstallPromptForTesting

SetMockInstallPromptForTesting
~~~cpp
void SetMockInstallPromptForTesting(
    std::unique_ptr<ExtensionInstallPrompt> mock_prompt);
~~~
 Allow tests to install a mock ExtensionInstallPrompt object, to fake
 user clicks on the permissions dialog.

### CreateCrxInstaller

CreateCrxInstaller
~~~cpp
scoped_refptr<extensions::CrxInstaller> CreateCrxInstaller(
    Profile* profile,
    const download::DownloadItem& download_item);
~~~
 Create and pre-configure a CrxInstaller for a given |download_item|.

### IsExtensionDownload

IsExtensionDownload
~~~cpp
bool IsExtensionDownload(const download::DownloadItem& download_item);
~~~
 Returns true if this is an extension download. This also considers user
 scripts to be extension downloads, since we convert those automatically.

### IsTrustedExtensionDownload

IsTrustedExtensionDownload
~~~cpp
bool IsTrustedExtensionDownload(Profile* profile,
                                const download::DownloadItem& item);
~~~
 Checks whether a download is an extension from a whitelisted site in prefs.

### OverrideOffstoreInstallAllowedForTesting

OverrideOffstoreInstallAllowedForTesting
~~~cpp
std::unique_ptr<base::AutoReset<bool>> OverrideOffstoreInstallAllowedForTesting(
    bool allowed);
~~~
 Allows tests to override whether offstore extension installs are allowed
 for testing purposes.

### OffStoreInstallAllowedByPrefs

OffStoreInstallAllowedByPrefs
~~~cpp
bool OffStoreInstallAllowedByPrefs(Profile* profile,
                                   const download::DownloadItem& item);
~~~
 Returns true if an offstore extension download should be allowed to proceed.

 Takes into consideration what's set in
 OverrideOffstoreInstallAllowedForTesting
