
## class DownloadShelfContextMenu
 This class is responsible for the download shelf context menu. Platform
 specific subclasses are responsible for creating and running the menu.


 The DownloadItem corresponding to the context menu is observed for removal or
 destruction.

### WantsContextMenu

DownloadShelfContextMenu::WantsContextMenu
~~~cpp
static bool WantsContextMenu(DownloadUIModel* download_model);
~~~
 Only show a context menu for a dangerous download if it is malicious.

### DownloadShelfContextMenu

DownloadShelfContextMenu
~~~cpp
DownloadShelfContextMenu(const DownloadShelfContextMenu&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfContextMenu& operator=(const DownloadShelfContextMenu&) = delete;
~~~

### ~DownloadShelfContextMenu

DownloadShelfContextMenu::~DownloadShelfContextMenu
~~~cpp
~DownloadShelfContextMenu() override;
~~~

### OnDownloadDestroyed

DownloadShelfContextMenu::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed();
~~~
 Called when download is destroyed.

### DownloadShelfContextMenu

DownloadShelfContextMenu::DownloadShelfContextMenu
~~~cpp
explicit DownloadShelfContextMenu(base::WeakPtr<DownloadUIModel> download);
~~~

### GetMenuModel

DownloadShelfContextMenu::GetMenuModel
~~~cpp
ui::SimpleMenuModel* GetMenuModel();
~~~
 Returns the correct menu model depending on the state of the download item.

 Returns nullptr if the download was destroyed.

### IsCommandIdEnabled

DownloadShelfContextMenu::IsCommandIdEnabled
~~~cpp
bool IsCommandIdEnabled(int command_id) const override;
~~~
 ui::SimpleMenuModel::Delegate:
### IsCommandIdChecked

DownloadShelfContextMenu::IsCommandIdChecked
~~~cpp
bool IsCommandIdChecked(int command_id) const override;
~~~

### IsCommandIdVisible

DownloadShelfContextMenu::IsCommandIdVisible
~~~cpp
bool IsCommandIdVisible(int command_id) const override;
~~~

### ExecuteCommand

DownloadShelfContextMenu::ExecuteCommand
~~~cpp
void ExecuteCommand(int command_id, int event_flags) override;
~~~

### IsItemForCommandIdDynamic

DownloadShelfContextMenu::IsItemForCommandIdDynamic
~~~cpp
bool IsItemForCommandIdDynamic(int command_id) const override;
~~~

### GetLabelForCommandId

DownloadShelfContextMenu::GetLabelForCommandId
~~~cpp
std::u16string GetLabelForCommandId(int command_id) const override;
~~~

### GetDownload

GetDownload
~~~cpp
DownloadUIModel* GetDownload() { return download_.get(); }
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadShelfContextMenu::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadShelfContextMenuTest,
                           InvalidDownloadWontCrashContextMenu);
~~~

### FRIEND_TEST_ALL_PREFIXES

DownloadShelfContextMenu::FRIEND_TEST_ALL_PREFIXES
~~~cpp
FRIEND_TEST_ALL_PREFIXES(DownloadShelfContextMenuTest, RecordCommandsEnabled);
~~~

### DetachFromDownloadItem

DownloadShelfContextMenu::DetachFromDownloadItem
~~~cpp
void DetachFromDownloadItem();
~~~
 Detaches self from |download_item_|. Called when the DownloadItem is
 destroyed or when this object is being destroyed.

### GetInProgressMenuModel

DownloadShelfContextMenu::GetInProgressMenuModel
~~~cpp
ui::SimpleMenuModel* GetInProgressMenuModel(bool is_download);
~~~

### GetInProgressPausedMenuModel

DownloadShelfContextMenu::GetInProgressPausedMenuModel
~~~cpp
ui::SimpleMenuModel* GetInProgressPausedMenuModel(bool is_download);
~~~

### GetFinishedMenuModel

DownloadShelfContextMenu::GetFinishedMenuModel
~~~cpp
ui::SimpleMenuModel* GetFinishedMenuModel(bool is_download);
~~~

### GetInterruptedMenuModel

DownloadShelfContextMenu::GetInterruptedMenuModel
~~~cpp
ui::SimpleMenuModel* GetInterruptedMenuModel(bool is_download);
~~~

### GetMaybeMaliciousMenuModel

DownloadShelfContextMenu::GetMaybeMaliciousMenuModel
~~~cpp
ui::SimpleMenuModel* GetMaybeMaliciousMenuModel(bool is_download);
~~~

### GetMaliciousMenuModel

DownloadShelfContextMenu::GetMaliciousMenuModel
~~~cpp
ui::SimpleMenuModel* GetMaliciousMenuModel(bool is_download);
~~~

### GetDeepScanningMenuModel

DownloadShelfContextMenu::GetDeepScanningMenuModel
~~~cpp
ui::SimpleMenuModel* GetDeepScanningMenuModel(bool is_download);
~~~

### GetInsecureDownloadMenuModel

DownloadShelfContextMenu::GetInsecureDownloadMenuModel
~~~cpp
ui::SimpleMenuModel* GetInsecureDownloadMenuModel();
~~~

### AddAutoOpenToMenu

DownloadShelfContextMenu::AddAutoOpenToMenu
~~~cpp
void AddAutoOpenToMenu(ui::SimpleMenuModel* model);
~~~

### RecordCommandsEnabled

DownloadShelfContextMenu::RecordCommandsEnabled
~~~cpp
void RecordCommandsEnabled(ui::SimpleMenuModel* model);
~~~

### in_progress_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> in_progress_download_menu_model_;

~~~

 We show slightly different menus if the download is in progress vs. if the
 download has finished.

### in_progress_download_paused_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> in_progress_download_paused_menu_model_;

~~~


### finished_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> finished_download_menu_model_;

~~~


### interrupted_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> interrupted_download_menu_model_;

~~~


### maybe_malicious_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> maybe_malicious_download_menu_model_;

~~~


### malicious_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> malicious_download_menu_model_;

~~~


### deep_scanning_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> deep_scanning_menu_model_;

~~~


### insecure_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> insecure_download_menu_model_;

~~~


### download_commands_enabled_recorded_



~~~cpp

bool download_commands_enabled_recorded_ = false;

~~~

 Whether or not a histogram has been emitted recording which
 Download commands were enabled
### download_



~~~cpp

base::WeakPtr<DownloadUIModel> download_;

~~~

 Information source.

 Use WeakPtr because the context menu may outlive |download_|.

### download_commands_



~~~cpp

std::unique_ptr<DownloadCommands> download_commands_;

~~~


### DownloadShelfContextMenu

DownloadShelfContextMenu
~~~cpp
DownloadShelfContextMenu(const DownloadShelfContextMenu&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfContextMenu& operator=(const DownloadShelfContextMenu&) = delete;
~~~

### GetDownload

GetDownload
~~~cpp
DownloadUIModel* GetDownload() { return download_.get(); }
~~~

### WantsContextMenu

DownloadShelfContextMenu::WantsContextMenu
~~~cpp
static bool WantsContextMenu(DownloadUIModel* download_model);
~~~
 Only show a context menu for a dangerous download if it is malicious.

### OnDownloadDestroyed

DownloadShelfContextMenu::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed();
~~~
 Called when download is destroyed.

### GetMenuModel

DownloadShelfContextMenu::GetMenuModel
~~~cpp
ui::SimpleMenuModel* GetMenuModel();
~~~
 Returns the correct menu model depending on the state of the download item.

 Returns nullptr if the download was destroyed.

### IsCommandIdEnabled

DownloadShelfContextMenu::IsCommandIdEnabled
~~~cpp
bool IsCommandIdEnabled(int command_id) const override;
~~~
 ui::SimpleMenuModel::Delegate:
### IsCommandIdChecked

DownloadShelfContextMenu::IsCommandIdChecked
~~~cpp
bool IsCommandIdChecked(int command_id) const override;
~~~

### IsCommandIdVisible

DownloadShelfContextMenu::IsCommandIdVisible
~~~cpp
bool IsCommandIdVisible(int command_id) const override;
~~~

### ExecuteCommand

DownloadShelfContextMenu::ExecuteCommand
~~~cpp
void ExecuteCommand(int command_id, int event_flags) override;
~~~

### IsItemForCommandIdDynamic

DownloadShelfContextMenu::IsItemForCommandIdDynamic
~~~cpp
bool IsItemForCommandIdDynamic(int command_id) const override;
~~~

### GetLabelForCommandId

DownloadShelfContextMenu::GetLabelForCommandId
~~~cpp
std::u16string GetLabelForCommandId(int command_id) const override;
~~~

### DetachFromDownloadItem

DownloadShelfContextMenu::DetachFromDownloadItem
~~~cpp
void DetachFromDownloadItem();
~~~
 Detaches self from |download_item_|. Called when the DownloadItem is
 destroyed or when this object is being destroyed.

### GetInProgressMenuModel

DownloadShelfContextMenu::GetInProgressMenuModel
~~~cpp
ui::SimpleMenuModel* GetInProgressMenuModel(bool is_download);
~~~

### GetInProgressPausedMenuModel

DownloadShelfContextMenu::GetInProgressPausedMenuModel
~~~cpp
ui::SimpleMenuModel* GetInProgressPausedMenuModel(bool is_download);
~~~

### GetFinishedMenuModel

DownloadShelfContextMenu::GetFinishedMenuModel
~~~cpp
ui::SimpleMenuModel* GetFinishedMenuModel(bool is_download);
~~~

### GetInterruptedMenuModel

DownloadShelfContextMenu::GetInterruptedMenuModel
~~~cpp
ui::SimpleMenuModel* GetInterruptedMenuModel(bool is_download);
~~~

### GetMaybeMaliciousMenuModel

DownloadShelfContextMenu::GetMaybeMaliciousMenuModel
~~~cpp
ui::SimpleMenuModel* GetMaybeMaliciousMenuModel(bool is_download);
~~~

### GetMaliciousMenuModel

DownloadShelfContextMenu::GetMaliciousMenuModel
~~~cpp
ui::SimpleMenuModel* GetMaliciousMenuModel(bool is_download);
~~~

### GetDeepScanningMenuModel

DownloadShelfContextMenu::GetDeepScanningMenuModel
~~~cpp
ui::SimpleMenuModel* GetDeepScanningMenuModel(bool is_download);
~~~

### GetInsecureDownloadMenuModel

DownloadShelfContextMenu::GetInsecureDownloadMenuModel
~~~cpp
ui::SimpleMenuModel* GetInsecureDownloadMenuModel();
~~~

### AddAutoOpenToMenu

DownloadShelfContextMenu::AddAutoOpenToMenu
~~~cpp
void AddAutoOpenToMenu(ui::SimpleMenuModel* model);
~~~

### RecordCommandsEnabled

DownloadShelfContextMenu::RecordCommandsEnabled
~~~cpp
void RecordCommandsEnabled(ui::SimpleMenuModel* model);
~~~

### in_progress_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> in_progress_download_menu_model_;

~~~

 We show slightly different menus if the download is in progress vs. if the
 download has finished.

### in_progress_download_paused_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> in_progress_download_paused_menu_model_;

~~~


### finished_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> finished_download_menu_model_;

~~~


### interrupted_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> interrupted_download_menu_model_;

~~~


### maybe_malicious_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> maybe_malicious_download_menu_model_;

~~~


### malicious_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> malicious_download_menu_model_;

~~~


### deep_scanning_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> deep_scanning_menu_model_;

~~~


### insecure_download_menu_model_



~~~cpp

std::unique_ptr<ui::SimpleMenuModel> insecure_download_menu_model_;

~~~


### download_commands_enabled_recorded_



~~~cpp

bool download_commands_enabled_recorded_ = false;

~~~

 Whether or not a histogram has been emitted recording which
 Download commands were enabled
### download_



~~~cpp

base::WeakPtr<DownloadUIModel> download_;

~~~

 Information source.

 Use WeakPtr because the context menu may outlive |download_|.

### download_commands_



~~~cpp

std::unique_ptr<DownloadCommands> download_commands_;

~~~

