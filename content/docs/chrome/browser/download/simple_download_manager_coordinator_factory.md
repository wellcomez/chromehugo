
## class SimpleDownloadManagerCoordinatorFactory
 namespace download
 A factory for SimpleDownloadManagerCoordinator. It can be used to create
 the SimpleDownloadManagerCoordinator before full browser process is created.

### GetInstance

SimpleDownloadManagerCoordinatorFactory::GetInstance
~~~cpp
static SimpleDownloadManagerCoordinatorFactory* GetInstance();
~~~
 Returns singleton instance of SimpleDownloadManagerCoordinatorFactory.

### GetForKey

SimpleDownloadManagerCoordinatorFactory::GetForKey
~~~cpp
static download::SimpleDownloadManagerCoordinator* GetForKey(
      SimpleFactoryKey* key);
~~~
 Returns SimpleDownloadManagerCoordinator associated with |key|.

### SimpleDownloadManagerCoordinatorFactory

SimpleDownloadManagerCoordinatorFactory
~~~cpp
SimpleDownloadManagerCoordinatorFactory(
      const SimpleDownloadManagerCoordinatorFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
SimpleDownloadManagerCoordinatorFactory& operator=(
      const SimpleDownloadManagerCoordinatorFactory&) = delete;
~~~

### SimpleDownloadManagerCoordinatorFactory

SimpleDownloadManagerCoordinatorFactory::SimpleDownloadManagerCoordinatorFactory
~~~cpp
SimpleDownloadManagerCoordinatorFactory();
~~~

### ~SimpleDownloadManagerCoordinatorFactory

SimpleDownloadManagerCoordinatorFactory::~SimpleDownloadManagerCoordinatorFactory
~~~cpp
~SimpleDownloadManagerCoordinatorFactory() override;
~~~

### BuildServiceInstanceFor

SimpleDownloadManagerCoordinatorFactory::BuildServiceInstanceFor
~~~cpp
std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      SimpleFactoryKey* key) const override;
~~~
 SimpleKeyedServiceFactory overrides.

### GetKeyToUse

SimpleDownloadManagerCoordinatorFactory::GetKeyToUse
~~~cpp
SimpleFactoryKey* GetKeyToUse(SimpleFactoryKey* key) const override;
~~~

### SimpleDownloadManagerCoordinatorFactory

SimpleDownloadManagerCoordinatorFactory
~~~cpp
SimpleDownloadManagerCoordinatorFactory(
      const SimpleDownloadManagerCoordinatorFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
SimpleDownloadManagerCoordinatorFactory& operator=(
      const SimpleDownloadManagerCoordinatorFactory&) = delete;
~~~

### GetInstance

SimpleDownloadManagerCoordinatorFactory::GetInstance
~~~cpp
static SimpleDownloadManagerCoordinatorFactory* GetInstance();
~~~
 Returns singleton instance of SimpleDownloadManagerCoordinatorFactory.

### GetForKey

SimpleDownloadManagerCoordinatorFactory::GetForKey
~~~cpp
static download::SimpleDownloadManagerCoordinator* GetForKey(
      SimpleFactoryKey* key);
~~~
 Returns SimpleDownloadManagerCoordinator associated with |key|.

### BuildServiceInstanceFor

SimpleDownloadManagerCoordinatorFactory::BuildServiceInstanceFor
~~~cpp
std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      SimpleFactoryKey* key) const override;
~~~
 SimpleKeyedServiceFactory overrides.

### GetKeyToUse

SimpleDownloadManagerCoordinatorFactory::GetKeyToUse
~~~cpp
SimpleFactoryKey* GetKeyToUse(SimpleFactoryKey* key) const override;
~~~
