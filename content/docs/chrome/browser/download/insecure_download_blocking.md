### GetDLBlockingHistogramName

GetDLBlockingHistogramName
~~~cpp
inline std::string GetDLBlockingHistogramName(const std::string& initiator,
                                              const std::string& download) {
  return std::string(kInsecureDownloadExtensionHistogramBase)
      .append(".")
      .append(initiator)
      .append(".")
      .append(download);
}
~~~
 Convenience function to assemble a histogram name for download blocking.

 |initiator| is one of kInsecureDownloadExtensionInitiator* above.

 |download| is one of kInsecureDownloadHistogramTarget* above.

### GetInsecureDownloadStatusForDownload

GetInsecureDownloadStatusForDownload
~~~cpp
download::DownloadItem::InsecureDownloadStatus
GetInsecureDownloadStatusForDownload(Profile* profile,
                                     const base::FilePath& path,
                                     const download::DownloadItem* item);
~~~
 Returns the correct insecure download blocking behavior for the given
 |item| saved to |path|.  Controlled by kTreatUnsafeDownloadsAsActive.

