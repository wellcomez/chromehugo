
## class DownloadPrefs
 Stores all download-related preferences.

### enum class

~~~cpp
enum class DownloadRestriction {
    NONE = 0,
    DANGEROUS_FILES = 1,
    POTENTIALLY_DANGEROUS_FILES = 2,
    ALL_FILES = 3,
    // MALICIOUS_FILES has a stricter definition of harmful file than
    // DANGEROUS_FILES and does not block based on file extension.
    MALICIOUS_FILES = 4,
  }
~~~
### DownloadPrefs

DownloadPrefs::DownloadPrefs
~~~cpp
explicit DownloadPrefs(Profile* profile);
~~~

### DownloadPrefs

DownloadPrefs
~~~cpp
DownloadPrefs(const DownloadPrefs&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadPrefs& operator=(const DownloadPrefs&) = delete;
~~~

### ~DownloadPrefs

DownloadPrefs::~DownloadPrefs
~~~cpp
~DownloadPrefs();
~~~

### RegisterProfilePrefs

DownloadPrefs::RegisterProfilePrefs
~~~cpp
static void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry);
~~~

###  GetDefaultDownloadDirectory

 Returns the default download directory.

~~~cpp
static const base::FilePath& GetDefaultDownloadDirectory();
~~~
### GetDefaultDownloadDirectoryForProfile

DownloadPrefs::GetDefaultDownloadDirectoryForProfile
~~~cpp
base::FilePath GetDefaultDownloadDirectoryForProfile() const;
~~~
 Returns the default download directory for the current profile.

### FromDownloadManager

DownloadPrefs::FromDownloadManager
~~~cpp
static DownloadPrefs* FromDownloadManager(
      content::DownloadManager* download_manager);
~~~
 Returns the DownloadPrefs corresponding to the given DownloadManager
 or BrowserContext.

### FromBrowserContext

DownloadPrefs::FromBrowserContext
~~~cpp
static DownloadPrefs* FromBrowserContext(
      content::BrowserContext* browser_context);
~~~

### IsFromTrustedSource

DownloadPrefs::IsFromTrustedSource
~~~cpp
bool IsFromTrustedSource(const download::DownloadItem& item);
~~~
 Identify whether the downloaded item was downloaded from a trusted source.

### DownloadPath

DownloadPrefs::DownloadPath
~~~cpp
base::FilePath DownloadPath() const;
~~~

### SetDownloadPath

DownloadPrefs::SetDownloadPath
~~~cpp
void SetDownloadPath(const base::FilePath& path);
~~~

### SaveFilePath

DownloadPrefs::SaveFilePath
~~~cpp
base::FilePath SaveFilePath() const;
~~~

### SetSaveFilePath

DownloadPrefs::SetSaveFilePath
~~~cpp
void SetSaveFilePath(const base::FilePath& path);
~~~

### save_file_type

save_file_type
~~~cpp
int save_file_type() const { return *save_file_type_; }
~~~

### SetSaveFileType

DownloadPrefs::SetSaveFileType
~~~cpp
void SetSaveFileType(int type);
~~~

### GetLastCompleteTime

DownloadPrefs::GetLastCompleteTime
~~~cpp
base::Time GetLastCompleteTime();
~~~

### SetLastCompleteTime

DownloadPrefs::SetLastCompleteTime
~~~cpp
void SetLastCompleteTime(const base::Time& last_complete_time);
~~~

### download_restriction

download_restriction
~~~cpp
DownloadRestriction download_restriction() const {
    return static_cast<DownloadRestriction>(*download_restriction_);
  }
~~~

### safebrowsing_for_trusted_sources_enabled

safebrowsing_for_trusted_sources_enabled
~~~cpp
bool safebrowsing_for_trusted_sources_enabled() const {
    return *safebrowsing_for_trusted_sources_enabled_;
  }
~~~

### PromptForDownload

DownloadPrefs::PromptForDownload
~~~cpp
bool PromptForDownload() const;
~~~
 Returns true if the prompt_for_download preference has been set and the
 download location is not managed (which means the user shouldn't be able
 to choose another download location).

### IsDownloadPathManaged

DownloadPrefs::IsDownloadPathManaged
~~~cpp
bool IsDownloadPathManaged() const;
~~~
 Returns true if the download path preference is managed.

### IsAutoOpenByUserUsed

DownloadPrefs::IsAutoOpenByUserUsed
~~~cpp
bool IsAutoOpenByUserUsed() const;
~~~
 Returns true if there is at least one file extension registered
 by the user for auto-open.

### IsAutoOpenEnabled

DownloadPrefs::IsAutoOpenEnabled
~~~cpp
bool IsAutoOpenEnabled(const GURL& url, const base::FilePath& path) const;
~~~
 Returns true if |path| should be opened automatically.

### IsAutoOpenByPolicy

DownloadPrefs::IsAutoOpenByPolicy
~~~cpp
bool IsAutoOpenByPolicy(const GURL& url, const base::FilePath& path) const;
~~~
 Returns true if |path| should be opened automatically by policy.

### EnableAutoOpenByUserBasedOnExtension

DownloadPrefs::EnableAutoOpenByUserBasedOnExtension
~~~cpp
bool EnableAutoOpenByUserBasedOnExtension(const base::FilePath& file_name);
~~~
 Enables automatically opening all downloads with the same file type as
 |file_name|. Returns true on success. The call may fail if |file_name|
 either doesn't have an extension (hence the file type cannot be
 determined), or if the file type is one that is disallowed from being
 opened automatically. See IsAllowedToOpenAutomatically() for details on the
 latter.

### DisableAutoOpenByUserBasedOnExtension

DownloadPrefs::DisableAutoOpenByUserBasedOnExtension
~~~cpp
void DisableAutoOpenByUserBasedOnExtension(const base::FilePath& file_name);
~~~
 Disables auto-open based on file extension.

### SetShouldOpenPdfInSystemReader

DownloadPrefs::SetShouldOpenPdfInSystemReader
~~~cpp
void SetShouldOpenPdfInSystemReader(bool should_open);
~~~
 Store the user preference to disk. If |should_open| is true, also disable
 the built-in PDF plugin. If |should_open| is false, enable the PDF plugin.

### ShouldOpenPdfInSystemReader

DownloadPrefs::ShouldOpenPdfInSystemReader
~~~cpp
bool ShouldOpenPdfInSystemReader() const;
~~~
 Return whether the user prefers to open PDF downloads in the platform's
 default reader.

### ResetAutoOpenByUser

DownloadPrefs::ResetAutoOpenByUser
~~~cpp
void ResetAutoOpenByUser();
~~~

### SkipSanitizeDownloadTargetPathForTesting

DownloadPrefs::SkipSanitizeDownloadTargetPathForTesting
~~~cpp
void SkipSanitizeDownloadTargetPathForTesting();
~~~
 If this is called, the download target path will not be sanitized going
 forward - whatever has been passed to SetDownloadPath will be used.

### PromptForDuplicateFile

DownloadPrefs::PromptForDuplicateFile
~~~cpp
bool PromptForDuplicateFile() const;
~~~
 Returns true if the download_duplicate_file_prompt_enabled pref is set and
 the new download bubble UI is enabled. Returns false on Android.

### SaveAutoOpenState

DownloadPrefs::SaveAutoOpenState
~~~cpp
void SaveAutoOpenState();
~~~

### CanPlatformEnableAutoOpenForPdf

DownloadPrefs::CanPlatformEnableAutoOpenForPdf
~~~cpp
bool CanPlatformEnableAutoOpenForPdf() const;
~~~

### SanitizeDownloadTargetPath

DownloadPrefs::SanitizeDownloadTargetPath
~~~cpp
base::FilePath SanitizeDownloadTargetPath(const base::FilePath& path) const;
~~~
 Checks whether |path| is a valid download target path. If it is, returns
 it as is. If it isn't returns the default download directory.

### UpdateAutoOpenByPolicy

DownloadPrefs::UpdateAutoOpenByPolicy
~~~cpp
void UpdateAutoOpenByPolicy();
~~~

### UpdateAllowedURLsForOpenByPolicy

DownloadPrefs::UpdateAllowedURLsForOpenByPolicy
~~~cpp
void UpdateAllowedURLsForOpenByPolicy();
~~~

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### prompt_for_download_



~~~cpp

BooleanPrefMember prompt_for_download_;

~~~


### prompt_for_download_android_



~~~cpp

IntegerPrefMember prompt_for_download_android_;

~~~


### download_path_



~~~cpp

FilePathPrefMember download_path_;

~~~


### save_file_path_



~~~cpp

FilePathPrefMember save_file_path_;

~~~


### save_file_type_



~~~cpp

IntegerPrefMember save_file_type_;

~~~


### download_restriction_



~~~cpp

IntegerPrefMember download_restriction_;

~~~


### download_bubble_enabled_



~~~cpp

BooleanPrefMember download_bubble_enabled_;

~~~


### prompt_for_duplicate_file_



~~~cpp

BooleanPrefMember prompt_for_duplicate_file_;

~~~


### safebrowsing_for_trusted_sources_enabled_



~~~cpp

BooleanPrefMember safebrowsing_for_trusted_sources_enabled_;

~~~


### pref_change_registrar_



~~~cpp

PrefChangeRegistrar pref_change_registrar_;

~~~


### trusted_sources_manager_



~~~cpp

std::unique_ptr<TrustedSourcesManager> trusted_sources_manager_;

~~~

 To identify if a download URL is from a trusted source.

###  operator

 Set of file extensions to open at download completion.

~~~cpp
struct AutoOpenCompareFunctor {
    bool operator()(const base::FilePath::StringType& a,
                    const base::FilePath::StringType& b) const;
  };
~~~
### auto_open_by_user_



~~~cpp

AutoOpenSet auto_open_by_user_;

~~~


### auto_open_by_policy_



~~~cpp

AutoOpenSet auto_open_by_policy_;

~~~


### auto_open_allowed_by_urls_



~~~cpp

std::unique_ptr<policy::URLBlocklist> auto_open_allowed_by_urls_;

~~~


### should_open_pdf_in_system_reader_



~~~cpp

bool should_open_pdf_in_system_reader_;

~~~


### skip_sanitize_download_target_path_for_testing_



~~~cpp

bool skip_sanitize_download_target_path_for_testing_ = false;

~~~

 If this is true, SanitizeDownloadTargetPath will always return the passed
 path verbatim.

### DownloadPrefs

DownloadPrefs
~~~cpp
DownloadPrefs(const DownloadPrefs&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadPrefs& operator=(const DownloadPrefs&) = delete;
~~~

### save_file_type

save_file_type
~~~cpp
int save_file_type() const { return *save_file_type_; }
~~~

### download_restriction

download_restriction
~~~cpp
DownloadRestriction download_restriction() const {
    return static_cast<DownloadRestriction>(*download_restriction_);
  }
~~~

### safebrowsing_for_trusted_sources_enabled

safebrowsing_for_trusted_sources_enabled
~~~cpp
bool safebrowsing_for_trusted_sources_enabled() const {
    return *safebrowsing_for_trusted_sources_enabled_;
  }
~~~

### enum class

~~~cpp
enum class DownloadRestriction {
    NONE = 0,
    DANGEROUS_FILES = 1,
    POTENTIALLY_DANGEROUS_FILES = 2,
    ALL_FILES = 3,
    // MALICIOUS_FILES has a stricter definition of harmful file than
    // DANGEROUS_FILES and does not block based on file extension.
    MALICIOUS_FILES = 4,
  };
~~~
### RegisterProfilePrefs

DownloadPrefs::RegisterProfilePrefs
~~~cpp
static void RegisterProfilePrefs(user_prefs::PrefRegistrySyncable* registry);
~~~

###  GetDefaultDownloadDirectory

 Returns the default download directory.

~~~cpp
static const base::FilePath& GetDefaultDownloadDirectory();
~~~
### GetDefaultDownloadDirectoryForProfile

DownloadPrefs::GetDefaultDownloadDirectoryForProfile
~~~cpp
base::FilePath GetDefaultDownloadDirectoryForProfile() const;
~~~
 Returns the default download directory for the current profile.

### FromDownloadManager

DownloadPrefs::FromDownloadManager
~~~cpp
static DownloadPrefs* FromDownloadManager(
      content::DownloadManager* download_manager);
~~~
 Returns the DownloadPrefs corresponding to the given DownloadManager
 or BrowserContext.

### FromBrowserContext

DownloadPrefs::FromBrowserContext
~~~cpp
static DownloadPrefs* FromBrowserContext(
      content::BrowserContext* browser_context);
~~~

### IsFromTrustedSource

DownloadPrefs::IsFromTrustedSource
~~~cpp
bool IsFromTrustedSource(const download::DownloadItem& item);
~~~
 Identify whether the downloaded item was downloaded from a trusted source.

### DownloadPath

DownloadPrefs::DownloadPath
~~~cpp
base::FilePath DownloadPath() const;
~~~

### SetDownloadPath

DownloadPrefs::SetDownloadPath
~~~cpp
void SetDownloadPath(const base::FilePath& path);
~~~

### SaveFilePath

DownloadPrefs::SaveFilePath
~~~cpp
base::FilePath SaveFilePath() const;
~~~

### SetSaveFilePath

DownloadPrefs::SetSaveFilePath
~~~cpp
void SetSaveFilePath(const base::FilePath& path);
~~~

### SetSaveFileType

DownloadPrefs::SetSaveFileType
~~~cpp
void SetSaveFileType(int type);
~~~

### GetLastCompleteTime

DownloadPrefs::GetLastCompleteTime
~~~cpp
base::Time GetLastCompleteTime();
~~~

### SetLastCompleteTime

DownloadPrefs::SetLastCompleteTime
~~~cpp
void SetLastCompleteTime(const base::Time& last_complete_time);
~~~

### PromptForDownload

DownloadPrefs::PromptForDownload
~~~cpp
bool PromptForDownload() const;
~~~
 Returns true if the prompt_for_download preference has been set and the
 download location is not managed (which means the user shouldn't be able
 to choose another download location).

### IsDownloadPathManaged

DownloadPrefs::IsDownloadPathManaged
~~~cpp
bool IsDownloadPathManaged() const;
~~~
 Returns true if the download path preference is managed.

### IsAutoOpenByUserUsed

DownloadPrefs::IsAutoOpenByUserUsed
~~~cpp
bool IsAutoOpenByUserUsed() const;
~~~
 Returns true if there is at least one file extension registered
 by the user for auto-open.

### IsAutoOpenEnabled

DownloadPrefs::IsAutoOpenEnabled
~~~cpp
bool IsAutoOpenEnabled(const GURL& url, const base::FilePath& path) const;
~~~
 Returns true if |path| should be opened automatically.

### IsAutoOpenByPolicy

DownloadPrefs::IsAutoOpenByPolicy
~~~cpp
bool IsAutoOpenByPolicy(const GURL& url, const base::FilePath& path) const;
~~~
 Returns true if |path| should be opened automatically by policy.

### EnableAutoOpenByUserBasedOnExtension

DownloadPrefs::EnableAutoOpenByUserBasedOnExtension
~~~cpp
bool EnableAutoOpenByUserBasedOnExtension(const base::FilePath& file_name);
~~~
 Enables automatically opening all downloads with the same file type as
 |file_name|. Returns true on success. The call may fail if |file_name|
 either doesn't have an extension (hence the file type cannot be
 determined), or if the file type is one that is disallowed from being
 opened automatically. See IsAllowedToOpenAutomatically() for details on the
 latter.

### DisableAutoOpenByUserBasedOnExtension

DownloadPrefs::DisableAutoOpenByUserBasedOnExtension
~~~cpp
void DisableAutoOpenByUserBasedOnExtension(const base::FilePath& file_name);
~~~
 Disables auto-open based on file extension.

### ResetAutoOpenByUser

DownloadPrefs::ResetAutoOpenByUser
~~~cpp
void ResetAutoOpenByUser();
~~~

### SkipSanitizeDownloadTargetPathForTesting

DownloadPrefs::SkipSanitizeDownloadTargetPathForTesting
~~~cpp
void SkipSanitizeDownloadTargetPathForTesting();
~~~
 If this is called, the download target path will not be sanitized going
 forward - whatever has been passed to SetDownloadPath will be used.

### PromptForDuplicateFile

DownloadPrefs::PromptForDuplicateFile
~~~cpp
bool PromptForDuplicateFile() const;
~~~
 Returns true if the download_duplicate_file_prompt_enabled pref is set and
 the new download bubble UI is enabled. Returns false on Android.

### SaveAutoOpenState

DownloadPrefs::SaveAutoOpenState
~~~cpp
void SaveAutoOpenState();
~~~

### CanPlatformEnableAutoOpenForPdf

DownloadPrefs::CanPlatformEnableAutoOpenForPdf
~~~cpp
bool CanPlatformEnableAutoOpenForPdf() const;
~~~

### SanitizeDownloadTargetPath

DownloadPrefs::SanitizeDownloadTargetPath
~~~cpp
base::FilePath SanitizeDownloadTargetPath(const base::FilePath& path) const;
~~~
 Checks whether |path| is a valid download target path. If it is, returns
 it as is. If it isn't returns the default download directory.

### UpdateAutoOpenByPolicy

DownloadPrefs::UpdateAutoOpenByPolicy
~~~cpp
void UpdateAutoOpenByPolicy();
~~~

### UpdateAllowedURLsForOpenByPolicy

DownloadPrefs::UpdateAllowedURLsForOpenByPolicy
~~~cpp
void UpdateAllowedURLsForOpenByPolicy();
~~~

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### prompt_for_download_



~~~cpp

BooleanPrefMember prompt_for_download_;

~~~


### download_path_



~~~cpp

FilePathPrefMember download_path_;

~~~


### save_file_path_



~~~cpp

FilePathPrefMember save_file_path_;

~~~


### save_file_type_



~~~cpp

IntegerPrefMember save_file_type_;

~~~


### download_restriction_



~~~cpp

IntegerPrefMember download_restriction_;

~~~


### download_bubble_enabled_



~~~cpp

BooleanPrefMember download_bubble_enabled_;

~~~


### prompt_for_duplicate_file_



~~~cpp

BooleanPrefMember prompt_for_duplicate_file_;

~~~


### safebrowsing_for_trusted_sources_enabled_



~~~cpp

BooleanPrefMember safebrowsing_for_trusted_sources_enabled_;

~~~


### pref_change_registrar_



~~~cpp

PrefChangeRegistrar pref_change_registrar_;

~~~


### trusted_sources_manager_



~~~cpp

std::unique_ptr<TrustedSourcesManager> trusted_sources_manager_;

~~~

 To identify if a download URL is from a trusted source.

###  operator

 Set of file extensions to open at download completion.

~~~cpp
struct AutoOpenCompareFunctor {
    bool operator()(const base::FilePath::StringType& a,
                    const base::FilePath::StringType& b) const;
  };
~~~
### auto_open_by_user_



~~~cpp

AutoOpenSet auto_open_by_user_;

~~~


### auto_open_by_policy_



~~~cpp

AutoOpenSet auto_open_by_policy_;

~~~


### auto_open_allowed_by_urls_



~~~cpp

std::unique_ptr<policy::URLBlocklist> auto_open_allowed_by_urls_;

~~~


### skip_sanitize_download_target_path_for_testing_



~~~cpp

bool skip_sanitize_download_target_path_for_testing_ = false;

~~~

 If this is true, SanitizeDownloadTargetPath will always return the passed
 path verbatim.
