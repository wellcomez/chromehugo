
## class DownloadCoreServiceImpl
 Owning class for ChromeDownloadManagerDelegate.

### DownloadCoreServiceImpl

DownloadCoreServiceImpl::DownloadCoreServiceImpl
~~~cpp
explicit DownloadCoreServiceImpl(Profile* profile);
~~~

### DownloadCoreServiceImpl

DownloadCoreServiceImpl
~~~cpp
DownloadCoreServiceImpl(const DownloadCoreServiceImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCoreServiceImpl& operator=(const DownloadCoreServiceImpl&) = delete;
~~~

### ~DownloadCoreServiceImpl

DownloadCoreServiceImpl::~DownloadCoreServiceImpl
~~~cpp
~DownloadCoreServiceImpl() override;
~~~

### GetDownloadManagerDelegate

DownloadCoreServiceImpl::GetDownloadManagerDelegate
~~~cpp
ChromeDownloadManagerDelegate* GetDownloadManagerDelegate() override;
~~~
 DownloadCoreService
### GetDownloadHistory

DownloadCoreServiceImpl::GetDownloadHistory
~~~cpp
DownloadHistory* GetDownloadHistory() override;
~~~

### GetExtensionEventRouter

DownloadCoreServiceImpl::GetExtensionEventRouter
~~~cpp
extensions::ExtensionDownloadsEventRouter* GetExtensionEventRouter() override;
~~~

### HasCreatedDownloadManager

DownloadCoreServiceImpl::HasCreatedDownloadManager
~~~cpp
bool HasCreatedDownloadManager() override;
~~~

### NonMaliciousDownloadCount

DownloadCoreServiceImpl::NonMaliciousDownloadCount
~~~cpp
int NonMaliciousDownloadCount() const override;
~~~

### CancelDownloads

DownloadCoreServiceImpl::CancelDownloads
~~~cpp
void CancelDownloads() override;
~~~

### SetDownloadManagerDelegateForTesting

DownloadCoreServiceImpl::SetDownloadManagerDelegateForTesting
~~~cpp
void SetDownloadManagerDelegateForTesting(
      std::unique_ptr<ChromeDownloadManagerDelegate> delegate) override;
~~~

### IsDownloadUiEnabled

DownloadCoreServiceImpl::IsDownloadUiEnabled
~~~cpp
bool IsDownloadUiEnabled() override;
~~~

### GetDownloadUIController

DownloadCoreServiceImpl::GetDownloadUIController
~~~cpp
DownloadUIController* GetDownloadUIController() override;
~~~

### SetDownloadHistoryForTesting

DownloadCoreServiceImpl::SetDownloadHistoryForTesting
~~~cpp
void SetDownloadHistoryForTesting(
      std::unique_ptr<DownloadHistory> download_history) override;
~~~

### Shutdown

DownloadCoreServiceImpl::Shutdown
~~~cpp
void Shutdown() override;
~~~
 KeyedService
### download_manager_created_



~~~cpp

bool download_manager_created_;

~~~


### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### manager_delegate_



~~~cpp

std::unique_ptr<ChromeDownloadManagerDelegate> manager_delegate_;

~~~

 ChromeDownloadManagerDelegate may be the target of callbacks from
 the history service/DB thread and must be kept alive for those
 callbacks.

### download_history_



~~~cpp

std::unique_ptr<DownloadHistory> download_history_;

~~~


### download_ui_



~~~cpp

std::unique_ptr<DownloadUIController> download_ui_;

~~~

 The UI controller is responsible for observing the download manager and
 notifying the UI of any new downloads. Its lifetime matches that of the
 associated download manager.

 Note on destruction order: download_ui_ depends on download_history_ and
 should be destroyed before the latter.

### download_shelf_controller_



~~~cpp

std::unique_ptr<DownloadShelfController> download_shelf_controller_;

~~~


### extension_event_router_



~~~cpp

std::unique_ptr<extensions::ExtensionDownloadsEventRouter>
      extension_event_router_;

~~~

 The ExtensionDownloadsEventRouter dispatches download creation, change, and
 erase events to extensions. Like ChromeDownloadManagerDelegate, it's a
 chrome-level concept and its lifetime should match DownloadManager. There
 should be a separate EDER for on-record and off-record managers.

 There does not appear to be a separate ExtensionSystem for on-record and
 off-record profiles, so ExtensionSystem cannot own the EDER.

### DownloadCoreServiceImpl

DownloadCoreServiceImpl
~~~cpp
DownloadCoreServiceImpl(const DownloadCoreServiceImpl&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCoreServiceImpl& operator=(const DownloadCoreServiceImpl&) = delete;
~~~

### GetDownloadManagerDelegate

DownloadCoreServiceImpl::GetDownloadManagerDelegate
~~~cpp
ChromeDownloadManagerDelegate* GetDownloadManagerDelegate() override;
~~~
 DownloadCoreService
### GetDownloadHistory

DownloadCoreServiceImpl::GetDownloadHistory
~~~cpp
DownloadHistory* GetDownloadHistory() override;
~~~

### HasCreatedDownloadManager

DownloadCoreServiceImpl::HasCreatedDownloadManager
~~~cpp
bool HasCreatedDownloadManager() override;
~~~

### NonMaliciousDownloadCount

DownloadCoreServiceImpl::NonMaliciousDownloadCount
~~~cpp
int NonMaliciousDownloadCount() const override;
~~~

### CancelDownloads

DownloadCoreServiceImpl::CancelDownloads
~~~cpp
void CancelDownloads() override;
~~~

### SetDownloadManagerDelegateForTesting

DownloadCoreServiceImpl::SetDownloadManagerDelegateForTesting
~~~cpp
void SetDownloadManagerDelegateForTesting(
      std::unique_ptr<ChromeDownloadManagerDelegate> delegate) override;
~~~

### IsDownloadUiEnabled

DownloadCoreServiceImpl::IsDownloadUiEnabled
~~~cpp
bool IsDownloadUiEnabled() override;
~~~

### GetDownloadUIController

DownloadCoreServiceImpl::GetDownloadUIController
~~~cpp
DownloadUIController* GetDownloadUIController() override;
~~~

### SetDownloadHistoryForTesting

DownloadCoreServiceImpl::SetDownloadHistoryForTesting
~~~cpp
void SetDownloadHistoryForTesting(
      std::unique_ptr<DownloadHistory> download_history) override;
~~~

### Shutdown

DownloadCoreServiceImpl::Shutdown
~~~cpp
void Shutdown() override;
~~~
 KeyedService
### download_manager_created_



~~~cpp

bool download_manager_created_;

~~~


### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### manager_delegate_



~~~cpp

std::unique_ptr<ChromeDownloadManagerDelegate> manager_delegate_;

~~~

 ChromeDownloadManagerDelegate may be the target of callbacks from
 the history service/DB thread and must be kept alive for those
 callbacks.

### download_history_



~~~cpp

std::unique_ptr<DownloadHistory> download_history_;

~~~


### download_ui_



~~~cpp

std::unique_ptr<DownloadUIController> download_ui_;

~~~

 The UI controller is responsible for observing the download manager and
 notifying the UI of any new downloads. Its lifetime matches that of the
 associated download manager.

 Note on destruction order: download_ui_ depends on download_history_ and
 should be destroyed before the latter.
