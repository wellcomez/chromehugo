---
tags:
  - download
---

## class DownloadItemModel
 namespace content
 Implementation of DownloadUIModel that wrappers around a |DownloadItem*|. As
 such, the caller is expected to ensure that the |download| passed into the
 constructor outlives this |DownloadItemModel|. In addition, multiple
 DownloadItemModel objects could be wrapping the same DownloadItem.

### Wrap

DownloadItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(download::DownloadItem* download);
~~~

### Wrap

DownloadItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(
      download::DownloadItem* download,
      std::unique_ptr<DownloadUIModel::StatusTextBuilderBase>
          status_text_builder);
~~~

### DownloadItemModel

DownloadItemModel::DownloadItemModel
~~~cpp
explicit DownloadItemModel(download::DownloadItem* download);
~~~
 Constructs a DownloadItemModel. The caller must ensure that |download|
 outlives this object.

### DownloadItemModel

DownloadItemModel::DownloadItemModel
~~~cpp
DownloadItemModel(download::DownloadItem* download,
                    std::unique_ptr<DownloadUIModel::StatusTextBuilderBase>
                        status_text_builder);
~~~

### DownloadItemModel

DownloadItemModel
~~~cpp
DownloadItemModel(const DownloadItemModel&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadItemModel& operator=(const DownloadItemModel&) = delete;
~~~

### ~DownloadItemModel

DownloadItemModel::~DownloadItemModel
~~~cpp
~DownloadItemModel() override;
~~~

### GetContentId

DownloadItemModel::GetContentId
~~~cpp
ContentId GetContentId() const override;
~~~
 DownloadUIModel implementation.

### profile

DownloadItemModel::profile
~~~cpp
Profile* profile() const override;
~~~

### GetTabProgressStatusText

DownloadItemModel::GetTabProgressStatusText
~~~cpp
std::u16string GetTabProgressStatusText() const override;
~~~

### GetCompletedBytes

DownloadItemModel::GetCompletedBytes
~~~cpp
int64_t GetCompletedBytes() const override;
~~~

### GetTotalBytes

DownloadItemModel::GetTotalBytes
~~~cpp
int64_t GetTotalBytes() const override;
~~~

### PercentComplete

DownloadItemModel::PercentComplete
~~~cpp
int PercentComplete() const override;
~~~

### IsDangerous

DownloadItemModel::IsDangerous
~~~cpp
bool IsDangerous() const override;
~~~

### MightBeMalicious

DownloadItemModel::MightBeMalicious
~~~cpp
bool MightBeMalicious() const override;
~~~

### IsMalicious

DownloadItemModel::IsMalicious
~~~cpp
bool IsMalicious() const override;
~~~

### IsInsecure

DownloadItemModel::IsInsecure
~~~cpp
bool IsInsecure() const override;
~~~

### ShouldRemoveFromShelfWhenComplete

DownloadItemModel::ShouldRemoveFromShelfWhenComplete
~~~cpp
bool ShouldRemoveFromShelfWhenComplete() const override;
~~~

### ShouldShowDownloadStartedAnimation

DownloadItemModel::ShouldShowDownloadStartedAnimation
~~~cpp
bool ShouldShowDownloadStartedAnimation() const override;
~~~

### ShouldShowInShelf

DownloadItemModel::ShouldShowInShelf
~~~cpp
bool ShouldShowInShelf() const override;
~~~

### SetShouldShowInShelf

DownloadItemModel::SetShouldShowInShelf
~~~cpp
void SetShouldShowInShelf(bool should_show) override;
~~~

### ShouldNotifyUI

DownloadItemModel::ShouldNotifyUI
~~~cpp
bool ShouldNotifyUI() const override;
~~~

### WasUINotified

DownloadItemModel::WasUINotified
~~~cpp
bool WasUINotified() const override;
~~~

### SetWasUINotified

DownloadItemModel::SetWasUINotified
~~~cpp
void SetWasUINotified(bool should_notify) override;
~~~

### WasActionedOn

DownloadItemModel::WasActionedOn
~~~cpp
bool WasActionedOn() const override;
~~~

### SetActionedOn

DownloadItemModel::SetActionedOn
~~~cpp
void SetActionedOn(bool actioned_on) override;
~~~

### WasUIWarningShown

DownloadItemModel::WasUIWarningShown
~~~cpp
bool WasUIWarningShown() const override;
~~~

### SetWasUIWarningShown

DownloadItemModel::SetWasUIWarningShown
~~~cpp
void SetWasUIWarningShown(bool should_notify) override;
~~~

### GetEphemeralWarningUiShownTime

DownloadItemModel::GetEphemeralWarningUiShownTime
~~~cpp
absl::optional<base::Time> GetEphemeralWarningUiShownTime() const override;
~~~

### SetEphemeralWarningUiShownTime

DownloadItemModel::SetEphemeralWarningUiShownTime
~~~cpp
void SetEphemeralWarningUiShownTime(absl::optional<base::Time> time) override;
~~~

### ShouldPreferOpeningInBrowser

DownloadItemModel::ShouldPreferOpeningInBrowser
~~~cpp
bool ShouldPreferOpeningInBrowser() override;
~~~

### SetShouldPreferOpeningInBrowser

DownloadItemModel::SetShouldPreferOpeningInBrowser
~~~cpp
void SetShouldPreferOpeningInBrowser(bool preference) override;
~~~

### GetDangerLevel

DownloadItemModel::GetDangerLevel
~~~cpp
safe_browsing::DownloadFileType::DangerLevel GetDangerLevel() const override;
~~~

### SetDangerLevel

DownloadItemModel::SetDangerLevel
~~~cpp
void SetDangerLevel(
      safe_browsing::DownloadFileType::DangerLevel danger_level) override;
~~~

### GetInsecureDownloadStatus

DownloadItemModel::GetInsecureDownloadStatus
~~~cpp
download::DownloadItem::InsecureDownloadStatus GetInsecureDownloadStatus()
      const override;
~~~

### OpenUsingPlatformHandler

DownloadItemModel::OpenUsingPlatformHandler
~~~cpp
void OpenUsingPlatformHandler() override;
~~~

### IsBeingRevived

DownloadItemModel::IsBeingRevived
~~~cpp
bool IsBeingRevived() const override;
~~~

### SetIsBeingRevived

DownloadItemModel::SetIsBeingRevived
~~~cpp
void SetIsBeingRevived(bool is_being_revived) override;
~~~

### GetDownloadItem

DownloadItemModel::GetDownloadItem
~~~cpp
const download::DownloadItem* GetDownloadItem() const override;
~~~

### GetFileNameToReportUser

DownloadItemModel::GetFileNameToReportUser
~~~cpp
base::FilePath GetFileNameToReportUser() const override;
~~~

### GetTargetFilePath

DownloadItemModel::GetTargetFilePath
~~~cpp
base::FilePath GetTargetFilePath() const override;
~~~

### OpenDownload

DownloadItemModel::OpenDownload
~~~cpp
void OpenDownload() override;
~~~

### GetState

DownloadItemModel::GetState
~~~cpp
download::DownloadItem::DownloadState GetState() const override;
~~~

### IsPaused

DownloadItemModel::IsPaused
~~~cpp
bool IsPaused() const override;
~~~

### GetDangerType

DownloadItemModel::GetDangerType
~~~cpp
download::DownloadDangerType GetDangerType() const override;
~~~

### GetOpenWhenComplete

DownloadItemModel::GetOpenWhenComplete
~~~cpp
bool GetOpenWhenComplete() const override;
~~~

### IsOpenWhenCompleteByPolicy

DownloadItemModel::IsOpenWhenCompleteByPolicy
~~~cpp
bool IsOpenWhenCompleteByPolicy() const override;
~~~

### TimeRemaining

DownloadItemModel::TimeRemaining
~~~cpp
bool TimeRemaining(base::TimeDelta* remaining) const override;
~~~

### GetStartTime

DownloadItemModel::GetStartTime
~~~cpp
base::Time GetStartTime() const override;
~~~

### GetEndTime

DownloadItemModel::GetEndTime
~~~cpp
base::Time GetEndTime() const override;
~~~

### GetOpened

DownloadItemModel::GetOpened
~~~cpp
bool GetOpened() const override;
~~~

### SetOpened

DownloadItemModel::SetOpened
~~~cpp
void SetOpened(bool opened) override;
~~~

### IsDone

DownloadItemModel::IsDone
~~~cpp
bool IsDone() const override;
~~~

### Pause

DownloadItemModel::Pause
~~~cpp
void Pause() override;
~~~

### Cancel

DownloadItemModel::Cancel
~~~cpp
void Cancel(bool user_cancel) override;
~~~

### Resume

DownloadItemModel::Resume
~~~cpp
void Resume() override;
~~~

### Remove

DownloadItemModel::Remove
~~~cpp
void Remove() override;
~~~

### SetOpenWhenComplete

DownloadItemModel::SetOpenWhenComplete
~~~cpp
void SetOpenWhenComplete(bool open) override;
~~~

### GetFullPath

DownloadItemModel::GetFullPath
~~~cpp
base::FilePath GetFullPath() const override;
~~~

### CanResume

DownloadItemModel::CanResume
~~~cpp
bool CanResume() const override;
~~~

### AllDataSaved

DownloadItemModel::AllDataSaved
~~~cpp
bool AllDataSaved() const override;
~~~

### GetFileExternallyRemoved

DownloadItemModel::GetFileExternallyRemoved
~~~cpp
bool GetFileExternallyRemoved() const override;
~~~

### GetURL

DownloadItemModel::GetURL
~~~cpp
GURL GetURL() const override;
~~~

### HasUserGesture

DownloadItemModel::HasUserGesture
~~~cpp
bool HasUserGesture() const override;
~~~

### GetLastFailState

DownloadItemModel::GetLastFailState
~~~cpp
offline_items_collection::FailState GetLastFailState() const override;
~~~

### IsCommandEnabled

DownloadItemModel::IsCommandEnabled
~~~cpp
bool IsCommandEnabled(const DownloadCommands* download_commands,
                        DownloadCommands::Command command) const override;
~~~

### IsCommandChecked

DownloadItemModel::IsCommandChecked
~~~cpp
bool IsCommandChecked(const DownloadCommands* download_commands,
                        DownloadCommands::Command command) const override;
~~~

### ExecuteCommand

DownloadItemModel::ExecuteCommand
~~~cpp
void ExecuteCommand(DownloadCommands* download_commands,
                      DownloadCommands::Command command) override;
~~~

### GetBubbleUIInfoForTailoredWarning

DownloadItemModel::GetBubbleUIInfoForTailoredWarning
~~~cpp
BubbleUIInfo GetBubbleUIInfoForTailoredWarning() const override;
~~~

### ShouldShowTailoredWarning

DownloadItemModel::ShouldShowTailoredWarning
~~~cpp
bool ShouldShowTailoredWarning() const override;
~~~

### ShouldShowInBubble

DownloadItemModel::ShouldShowInBubble
~~~cpp
bool ShouldShowInBubble() const override;
~~~

### IsEphemeralWarning

DownloadItemModel::IsEphemeralWarning
~~~cpp
bool IsEphemeralWarning() const override;
~~~

### CompleteSafeBrowsingScan

DownloadItemModel::CompleteSafeBrowsingScan
~~~cpp
void CompleteSafeBrowsingScan() override;
~~~

### ReviewScanningVerdict

DownloadItemModel::ReviewScanningVerdict
~~~cpp
void ReviewScanningVerdict(content::WebContents* web_contents) override;
~~~

### ShouldShowDropdown

DownloadItemModel::ShouldShowDropdown
~~~cpp
bool ShouldShowDropdown() const override;
~~~

### DetermineAndSetShouldPreferOpeningInBrowser

DownloadItemModel::DetermineAndSetShouldPreferOpeningInBrowser
~~~cpp
void DetermineAndSetShouldPreferOpeningInBrowser(
      const base::FilePath& target_path,
      bool is_filetype_handled_safely) override;
~~~

### OnDownloadUpdated

DownloadItemModel::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer implementation.

### OnDownloadOpened

DownloadItemModel::OnDownloadOpened
~~~cpp
void OnDownloadOpened(download::DownloadItem* download) override;
~~~

### OnDownloadDestroyed

DownloadItemModel::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~

### GetMimeType

DownloadItemModel::GetMimeType
~~~cpp
std::string GetMimeType() const override;
~~~
 DownloadUIModel implementation.

### IsExtensionDownload

DownloadItemModel::IsExtensionDownload
~~~cpp
bool IsExtensionDownload() const override;
~~~

### download_



~~~cpp

raw_ptr<download::DownloadItem> download_;

~~~

 The DownloadItem that this model represents. Note that DownloadItemModel
 itself shouldn't maintain any state since there can be more than one
 DownloadItemModel in use with the same DownloadItem.

### DownloadItemModel

DownloadItemModel
~~~cpp
DownloadItemModel(const DownloadItemModel&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadItemModel& operator=(const DownloadItemModel&) = delete;
~~~

### Wrap

DownloadItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(download::DownloadItem* download);
~~~

### Wrap

DownloadItemModel::Wrap
~~~cpp
static DownloadUIModelPtr Wrap(
      download::DownloadItem* download,
      std::unique_ptr<DownloadUIModel::StatusTextBuilderBase>
          status_text_builder);
~~~

### GetContentId

DownloadItemModel::GetContentId
~~~cpp
ContentId GetContentId() const override;
~~~
 DownloadUIModel implementation.

### profile

DownloadItemModel::profile
~~~cpp
Profile* profile() const override;
~~~

### GetTabProgressStatusText

DownloadItemModel::GetTabProgressStatusText
~~~cpp
std::u16string GetTabProgressStatusText() const override;
~~~

### GetCompletedBytes

DownloadItemModel::GetCompletedBytes
~~~cpp
int64_t GetCompletedBytes() const override;
~~~

### GetTotalBytes

DownloadItemModel::GetTotalBytes
~~~cpp
int64_t GetTotalBytes() const override;
~~~

### PercentComplete

DownloadItemModel::PercentComplete
~~~cpp
int PercentComplete() const override;
~~~

### IsDangerous

DownloadItemModel::IsDangerous
~~~cpp
bool IsDangerous() const override;
~~~

### MightBeMalicious

DownloadItemModel::MightBeMalicious
~~~cpp
bool MightBeMalicious() const override;
~~~

### IsMalicious

DownloadItemModel::IsMalicious
~~~cpp
bool IsMalicious() const override;
~~~

### IsInsecure

DownloadItemModel::IsInsecure
~~~cpp
bool IsInsecure() const override;
~~~

### ShouldRemoveFromShelfWhenComplete

DownloadItemModel::ShouldRemoveFromShelfWhenComplete
~~~cpp
bool ShouldRemoveFromShelfWhenComplete() const override;
~~~

### ShouldShowDownloadStartedAnimation

DownloadItemModel::ShouldShowDownloadStartedAnimation
~~~cpp
bool ShouldShowDownloadStartedAnimation() const override;
~~~

### ShouldShowInShelf

DownloadItemModel::ShouldShowInShelf
~~~cpp
bool ShouldShowInShelf() const override;
~~~

### SetShouldShowInShelf

DownloadItemModel::SetShouldShowInShelf
~~~cpp
void SetShouldShowInShelf(bool should_show) override;
~~~

### ShouldNotifyUI

DownloadItemModel::ShouldNotifyUI
~~~cpp
bool ShouldNotifyUI() const override;
~~~

### WasUINotified

DownloadItemModel::WasUINotified
~~~cpp
bool WasUINotified() const override;
~~~

### SetWasUINotified

DownloadItemModel::SetWasUINotified
~~~cpp
void SetWasUINotified(bool should_notify) override;
~~~

### WasActionedOn

DownloadItemModel::WasActionedOn
~~~cpp
bool WasActionedOn() const override;
~~~

### SetActionedOn

DownloadItemModel::SetActionedOn
~~~cpp
void SetActionedOn(bool actioned_on) override;
~~~

### WasUIWarningShown

DownloadItemModel::WasUIWarningShown
~~~cpp
bool WasUIWarningShown() const override;
~~~

### SetWasUIWarningShown

DownloadItemModel::SetWasUIWarningShown
~~~cpp
void SetWasUIWarningShown(bool should_notify) override;
~~~

### GetEphemeralWarningUiShownTime

DownloadItemModel::GetEphemeralWarningUiShownTime
~~~cpp
absl::optional<base::Time> GetEphemeralWarningUiShownTime() const override;
~~~

### SetEphemeralWarningUiShownTime

DownloadItemModel::SetEphemeralWarningUiShownTime
~~~cpp
void SetEphemeralWarningUiShownTime(absl::optional<base::Time> time) override;
~~~

### ShouldPreferOpeningInBrowser

DownloadItemModel::ShouldPreferOpeningInBrowser
~~~cpp
bool ShouldPreferOpeningInBrowser() override;
~~~

### SetShouldPreferOpeningInBrowser

DownloadItemModel::SetShouldPreferOpeningInBrowser
~~~cpp
void SetShouldPreferOpeningInBrowser(bool preference) override;
~~~

### GetDangerLevel

DownloadItemModel::GetDangerLevel
~~~cpp
safe_browsing::DownloadFileType::DangerLevel GetDangerLevel() const override;
~~~

### SetDangerLevel

DownloadItemModel::SetDangerLevel
~~~cpp
void SetDangerLevel(
      safe_browsing::DownloadFileType::DangerLevel danger_level) override;
~~~

### GetInsecureDownloadStatus

DownloadItemModel::GetInsecureDownloadStatus
~~~cpp
download::DownloadItem::InsecureDownloadStatus GetInsecureDownloadStatus()
      const override;
~~~

### OpenUsingPlatformHandler

DownloadItemModel::OpenUsingPlatformHandler
~~~cpp
void OpenUsingPlatformHandler() override;
~~~

### IsBeingRevived

DownloadItemModel::IsBeingRevived
~~~cpp
bool IsBeingRevived() const override;
~~~

### SetIsBeingRevived

DownloadItemModel::SetIsBeingRevived
~~~cpp
void SetIsBeingRevived(bool is_being_revived) override;
~~~

### GetDownloadItem

DownloadItemModel::GetDownloadItem
~~~cpp
const download::DownloadItem* GetDownloadItem() const override;
~~~

### GetFileNameToReportUser

DownloadItemModel::GetFileNameToReportUser
~~~cpp
base::FilePath GetFileNameToReportUser() const override;
~~~

### GetTargetFilePath

DownloadItemModel::GetTargetFilePath
~~~cpp
base::FilePath GetTargetFilePath() const override;
~~~

### OpenDownload

DownloadItemModel::OpenDownload
~~~cpp
void OpenDownload() override;
~~~

### GetState

DownloadItemModel::GetState
~~~cpp
download::DownloadItem::DownloadState GetState() const override;
~~~

### IsPaused

DownloadItemModel::IsPaused
~~~cpp
bool IsPaused() const override;
~~~

### GetDangerType

DownloadItemModel::GetDangerType
~~~cpp
download::DownloadDangerType GetDangerType() const override;
~~~

### GetOpenWhenComplete

DownloadItemModel::GetOpenWhenComplete
~~~cpp
bool GetOpenWhenComplete() const override;
~~~

### IsOpenWhenCompleteByPolicy

DownloadItemModel::IsOpenWhenCompleteByPolicy
~~~cpp
bool IsOpenWhenCompleteByPolicy() const override;
~~~

### TimeRemaining

DownloadItemModel::TimeRemaining
~~~cpp
bool TimeRemaining(base::TimeDelta* remaining) const override;
~~~

### GetStartTime

DownloadItemModel::GetStartTime
~~~cpp
base::Time GetStartTime() const override;
~~~

### GetEndTime

DownloadItemModel::GetEndTime
~~~cpp
base::Time GetEndTime() const override;
~~~

### GetOpened

DownloadItemModel::GetOpened
~~~cpp
bool GetOpened() const override;
~~~

### SetOpened

DownloadItemModel::SetOpened
~~~cpp
void SetOpened(bool opened) override;
~~~

### IsDone

DownloadItemModel::IsDone
~~~cpp
bool IsDone() const override;
~~~

### Pause

DownloadItemModel::Pause
~~~cpp
void Pause() override;
~~~

### Cancel

DownloadItemModel::Cancel
~~~cpp
void Cancel(bool user_cancel) override;
~~~

### Resume

DownloadItemModel::Resume
~~~cpp
void Resume() override;
~~~

### Remove

DownloadItemModel::Remove
~~~cpp
void Remove() override;
~~~

### SetOpenWhenComplete

DownloadItemModel::SetOpenWhenComplete
~~~cpp
void SetOpenWhenComplete(bool open) override;
~~~

### GetFullPath

DownloadItemModel::GetFullPath
~~~cpp
base::FilePath GetFullPath() const override;
~~~

### CanResume

DownloadItemModel::CanResume
~~~cpp
bool CanResume() const override;
~~~

### AllDataSaved

DownloadItemModel::AllDataSaved
~~~cpp
bool AllDataSaved() const override;
~~~

### GetFileExternallyRemoved

DownloadItemModel::GetFileExternallyRemoved
~~~cpp
bool GetFileExternallyRemoved() const override;
~~~

### GetURL

DownloadItemModel::GetURL
~~~cpp
GURL GetURL() const override;
~~~

### HasUserGesture

DownloadItemModel::HasUserGesture
~~~cpp
bool HasUserGesture() const override;
~~~

### GetLastFailState

DownloadItemModel::GetLastFailState
~~~cpp
offline_items_collection::FailState GetLastFailState() const override;
~~~

### ShouldShowDropdown

DownloadItemModel::ShouldShowDropdown
~~~cpp
bool ShouldShowDropdown() const override;
~~~

### DetermineAndSetShouldPreferOpeningInBrowser

DownloadItemModel::DetermineAndSetShouldPreferOpeningInBrowser
~~~cpp
void DetermineAndSetShouldPreferOpeningInBrowser(
      const base::FilePath& target_path,
      bool is_filetype_handled_safely) override;
~~~

### OnDownloadUpdated

DownloadItemModel::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer implementation.

### OnDownloadOpened

DownloadItemModel::OnDownloadOpened
~~~cpp
void OnDownloadOpened(download::DownloadItem* download) override;
~~~

### OnDownloadDestroyed

DownloadItemModel::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~

### GetMimeType

DownloadItemModel::GetMimeType
~~~cpp
std::string GetMimeType() const override;
~~~
 DownloadUIModel implementation.

### IsExtensionDownload

DownloadItemModel::IsExtensionDownload
~~~cpp
bool IsExtensionDownload() const override;
~~~

### download_



~~~cpp

raw_ptr<download::DownloadItem> download_;

~~~

 The DownloadItem that this model represents. Note that DownloadItemModel
 itself shouldn't maintain any state since there can be more than one
 DownloadItemModel in use with the same DownloadItem.
