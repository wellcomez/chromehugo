
## class DownloadCoreServiceFactory
 Singleton that owns all DownloadCoreServices and associates them with
 Profiles. Listens for the Profile's destruction notification and cleans up
 the associated DownloadCoreService.

### GetForBrowserContext

DownloadCoreServiceFactory::GetForBrowserContext
~~~cpp
static DownloadCoreService* GetForBrowserContext(
      content::BrowserContext* context);
~~~
 Returns the DownloadCoreService for |context|, creating if not yet created.

### GetInstance

DownloadCoreServiceFactory::GetInstance
~~~cpp
static DownloadCoreServiceFactory* GetInstance();
~~~

### BuildServiceInstanceFor

DownloadCoreServiceFactory::BuildServiceInstanceFor
~~~cpp
KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* profile) const override;
~~~
 BrowserContextKeyedServiceFactory:
### DownloadCoreServiceFactory

DownloadCoreServiceFactory::DownloadCoreServiceFactory
~~~cpp
DownloadCoreServiceFactory();
~~~

### ~DownloadCoreServiceFactory

DownloadCoreServiceFactory::~DownloadCoreServiceFactory
~~~cpp
~DownloadCoreServiceFactory() override;
~~~

### GetForBrowserContext

DownloadCoreServiceFactory::GetForBrowserContext
~~~cpp
static DownloadCoreService* GetForBrowserContext(
      content::BrowserContext* context);
~~~
 Returns the DownloadCoreService for |context|, creating if not yet created.

### GetInstance

DownloadCoreServiceFactory::GetInstance
~~~cpp
static DownloadCoreServiceFactory* GetInstance();
~~~

### BuildServiceInstanceFor

DownloadCoreServiceFactory::BuildServiceInstanceFor
~~~cpp
KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* profile) const override;
~~~
 BrowserContextKeyedServiceFactory: