
## class DownloadHistory
 namespace history
 Observes a single DownloadManager and all its DownloadItems, keeping the
 DownloadDatabase up to date.

###  HistoryAdapter

 Caller must guarantee that HistoryService outlives HistoryAdapter.

~~~cpp
class HistoryAdapter {
   public:
    explicit HistoryAdapter(history::HistoryService* history);

    HistoryAdapter(const HistoryAdapter&) = delete;
    HistoryAdapter& operator=(const HistoryAdapter&) = delete;

    virtual ~HistoryAdapter();

    virtual void QueryDownloads(
        history::HistoryService::DownloadQueryCallback callback);

    virtual void CreateDownload(
        const history::DownloadRow& info,
        history::HistoryService::DownloadCreateCallback callback);

    virtual void UpdateDownload(const history::DownloadRow& data,
                                bool should_commit_immediately);

    virtual void RemoveDownloads(const std::set<uint32_t>& ids);

   private:
    raw_ptr<history::HistoryService> history_;
  };
~~~
###  Observer


~~~cpp
class Observer {
   public:
    Observer();
    virtual ~Observer();

    // Fires when a download is added to or updated in the database, just after
    // the task is posted to the history thread.
    virtual void OnDownloadStored(download::DownloadItem* item,
                                  const history::DownloadRow& info) {}

    // Fires when RemoveDownloads messages are sent to the DB thread.
    virtual void OnDownloadsRemoved(const IdSet& ids) {}

    // Fires when the DownloadHistory completes the initial history query.
    // Unlike the other observer methods, this one is invoked if the initial
    // history query has already completed by the time the caller calls
    // AddObserver().
    virtual void OnHistoryQueryComplete() {}

    // Fires when the DownloadHistory is being destroyed so that implementors
    // can RemoveObserver() and nullify their DownloadHistory*s.
    virtual void OnDownloadHistoryDestroyed() {}
  };
~~~
### IsPersisted

DownloadHistory::IsPersisted
~~~cpp
static bool IsPersisted(const download::DownloadItem* item);
~~~
 Returns true if the download is persisted. Not reliable when called from
 within a DownloadManager::Observer::OnDownloadCreated handler since the
 persisted state may not yet have been updated for a download that was
 restored from history.

### DownloadHistory

DownloadHistory::DownloadHistory
~~~cpp
DownloadHistory(content::DownloadManager* manager,
                  std::unique_ptr<HistoryAdapter> history);
~~~
 Neither |manager| nor |history| may be NULL.

 DownloadService creates DownloadHistory some time after DownloadManager is
 created and destroys DownloadHistory as DownloadManager is shutting down.

### DownloadHistory

DownloadHistory
~~~cpp
DownloadHistory(const DownloadHistory&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadHistory& operator=(const DownloadHistory&) = delete;
~~~

### ~DownloadHistory

DownloadHistory::~DownloadHistory
~~~cpp
~DownloadHistory() override;
~~~

### AddObserver

DownloadHistory::AddObserver
~~~cpp
void AddObserver(Observer* observer);
~~~

### RemoveObserver

DownloadHistory::RemoveObserver
~~~cpp
void RemoveObserver(Observer* observer);
~~~

### QueryCallback

DownloadHistory::QueryCallback
~~~cpp
void QueryCallback(std::vector<history::DownloadRow> rows);
~~~
 Callback from |history_| containing all entries in the downloads database
 table.

### LoadHistoryDownloads

DownloadHistory::LoadHistoryDownloads
~~~cpp
void LoadHistoryDownloads(const std::vector<history::DownloadRow>& rows);
~~~
 Called to create all history downloads.

### MaybeAddToHistory

DownloadHistory::MaybeAddToHistory
~~~cpp
void MaybeAddToHistory(download::DownloadItem* item);
~~~
 May add |item| to |history_|.

### ItemAdded

DownloadHistory::ItemAdded
~~~cpp
void ItemAdded(uint32_t id, const history::DownloadRow& info, bool success);
~~~
 Callback from |history_| when an item was successfully inserted into the
 database.

### OnDownloadCreated

DownloadHistory::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~
 AllDownloadItemNotifier::Observer
### OnDownloadUpdated

DownloadHistory::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadOpened

DownloadHistory::OnDownloadOpened
~~~cpp
void OnDownloadOpened(content::DownloadManager* manager,
                        download::DownloadItem* item) override;
~~~

### OnDownloadRemoved

DownloadHistory::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### ScheduleRemoveDownload

DownloadHistory::ScheduleRemoveDownload
~~~cpp
void ScheduleRemoveDownload(uint32_t download_id);
~~~
 Schedule a record to be removed from |history_| the next time
 RemoveDownloadsBatch() runs. Schedule RemoveDownloadsBatch() to be run soon
 if it isn't already scheduled.

### RemoveDownloadsBatch

DownloadHistory::RemoveDownloadsBatch
~~~cpp
void RemoveDownloadsBatch();
~~~
 Removes all |removing_ids_| from |history_|.

### OnDownloadRestoredFromHistory

DownloadHistory::OnDownloadRestoredFromHistory
~~~cpp
void OnDownloadRestoredFromHistory(download::DownloadItem* item);
~~~
 Called when a download was restored from history.

### NeedToUpdateDownloadHistory

DownloadHistory::NeedToUpdateDownloadHistory
~~~cpp
bool NeedToUpdateDownloadHistory(download::DownloadItem* item);
~~~
 Check whether an download item needs be updated or added to history DB.

### notifier_



~~~cpp

download::AllDownloadItemNotifier notifier_;

~~~


### history_



~~~cpp

std::unique_ptr<HistoryAdapter> history_;

~~~


### loading_id_



~~~cpp

uint32_t loading_id_;

~~~

 Identifier of the item being created in QueryCallback(), matched up with
 created items in OnDownloadCreated() so that the item is not re-added to
 the database.

### removing_ids_



~~~cpp

IdSet removing_ids_;

~~~

 Identifiers of items that are scheduled for removal from history, to
 facilitate batching removals together for database efficiency.

### removed_while_adding_



~~~cpp

IdSet removed_while_adding_;

~~~

 |GetId()|s of items that were removed while they were being added, so that
 they can be removed when the database finishes adding them.

 TODO(benjhayden) Can this be removed now that it doesn't need to wait for
 the db_handle, and can rely on PostTask sequentiality?
### initial_history_query_complete_



~~~cpp

bool initial_history_query_complete_;

~~~


### observers_



~~~cpp

base::ObserverList<Observer>::Unchecked observers_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadHistory> weak_ptr_factory_{this};

~~~


### DownloadHistory

DownloadHistory
~~~cpp
DownloadHistory(const DownloadHistory&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadHistory& operator=(const DownloadHistory&) = delete;
~~~

###  HistoryAdapter

 Caller must guarantee that HistoryService outlives HistoryAdapter.

~~~cpp
class HistoryAdapter {
   public:
    explicit HistoryAdapter(history::HistoryService* history);

    HistoryAdapter(const HistoryAdapter&) = delete;
    HistoryAdapter& operator=(const HistoryAdapter&) = delete;

    virtual ~HistoryAdapter();

    virtual void QueryDownloads(
        history::HistoryService::DownloadQueryCallback callback);

    virtual void CreateDownload(
        const history::DownloadRow& info,
        history::HistoryService::DownloadCreateCallback callback);

    virtual void UpdateDownload(const history::DownloadRow& data,
                                bool should_commit_immediately);

    virtual void RemoveDownloads(const std::set<uint32_t>& ids);

   private:
    raw_ptr<history::HistoryService> history_;
  };
~~~
###  Observer


~~~cpp
class Observer {
   public:
    Observer();
    virtual ~Observer();

    // Fires when a download is added to or updated in the database, just after
    // the task is posted to the history thread.
    virtual void OnDownloadStored(download::DownloadItem* item,
                                  const history::DownloadRow& info) {}

    // Fires when RemoveDownloads messages are sent to the DB thread.
    virtual void OnDownloadsRemoved(const IdSet& ids) {}

    // Fires when the DownloadHistory completes the initial history query.
    // Unlike the other observer methods, this one is invoked if the initial
    // history query has already completed by the time the caller calls
    // AddObserver().
    virtual void OnHistoryQueryComplete() {}

    // Fires when the DownloadHistory is being destroyed so that implementors
    // can RemoveObserver() and nullify their DownloadHistory*s.
    virtual void OnDownloadHistoryDestroyed() {}
  };
~~~
### IsPersisted

DownloadHistory::IsPersisted
~~~cpp
static bool IsPersisted(const download::DownloadItem* item);
~~~
 Returns true if the download is persisted. Not reliable when called from
 within a DownloadManager::Observer::OnDownloadCreated handler since the
 persisted state may not yet have been updated for a download that was
 restored from history.

### AddObserver

DownloadHistory::AddObserver
~~~cpp
void AddObserver(Observer* observer);
~~~

### RemoveObserver

DownloadHistory::RemoveObserver
~~~cpp
void RemoveObserver(Observer* observer);
~~~

### QueryCallback

DownloadHistory::QueryCallback
~~~cpp
void QueryCallback(std::vector<history::DownloadRow> rows);
~~~
 Callback from |history_| containing all entries in the downloads database
 table.

### LoadHistoryDownloads

DownloadHistory::LoadHistoryDownloads
~~~cpp
void LoadHistoryDownloads(const std::vector<history::DownloadRow>& rows);
~~~
 Called to create all history downloads.

### MaybeAddToHistory

DownloadHistory::MaybeAddToHistory
~~~cpp
void MaybeAddToHistory(download::DownloadItem* item);
~~~
 May add |item| to |history_|.

### ItemAdded

DownloadHistory::ItemAdded
~~~cpp
void ItemAdded(uint32_t id, const history::DownloadRow& info, bool success);
~~~
 Callback from |history_| when an item was successfully inserted into the
 database.

### OnDownloadCreated

DownloadHistory::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~
 AllDownloadItemNotifier::Observer
### OnDownloadUpdated

DownloadHistory::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadOpened

DownloadHistory::OnDownloadOpened
~~~cpp
void OnDownloadOpened(content::DownloadManager* manager,
                        download::DownloadItem* item) override;
~~~

### OnDownloadRemoved

DownloadHistory::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### ScheduleRemoveDownload

DownloadHistory::ScheduleRemoveDownload
~~~cpp
void ScheduleRemoveDownload(uint32_t download_id);
~~~
 Schedule a record to be removed from |history_| the next time
 RemoveDownloadsBatch() runs. Schedule RemoveDownloadsBatch() to be run soon
 if it isn't already scheduled.

### RemoveDownloadsBatch

DownloadHistory::RemoveDownloadsBatch
~~~cpp
void RemoveDownloadsBatch();
~~~
 Removes all |removing_ids_| from |history_|.

### OnDownloadRestoredFromHistory

DownloadHistory::OnDownloadRestoredFromHistory
~~~cpp
void OnDownloadRestoredFromHistory(download::DownloadItem* item);
~~~
 Called when a download was restored from history.

### NeedToUpdateDownloadHistory

DownloadHistory::NeedToUpdateDownloadHistory
~~~cpp
bool NeedToUpdateDownloadHistory(download::DownloadItem* item);
~~~
 Check whether an download item needs be updated or added to history DB.

### notifier_



~~~cpp

download::AllDownloadItemNotifier notifier_;

~~~


### history_



~~~cpp

std::unique_ptr<HistoryAdapter> history_;

~~~


### loading_id_



~~~cpp

uint32_t loading_id_;

~~~

 Identifier of the item being created in QueryCallback(), matched up with
 created items in OnDownloadCreated() so that the item is not re-added to
 the database.

### removing_ids_



~~~cpp

IdSet removing_ids_;

~~~

 Identifiers of items that are scheduled for removal from history, to
 facilitate batching removals together for database efficiency.

### removed_while_adding_



~~~cpp

IdSet removed_while_adding_;

~~~

 |GetId()|s of items that were removed while they were being added, so that
 they can be removed when the database finishes adding them.

 TODO(benjhayden) Can this be removed now that it doesn't need to wait for
 the db_handle, and can rely on PostTask sequentiality?
### initial_history_query_complete_



~~~cpp

bool initial_history_query_complete_;

~~~


### observers_



~~~cpp

base::ObserverList<Observer>::Unchecked observers_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadHistory> weak_ptr_factory_{this};

~~~

