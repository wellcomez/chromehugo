
## class TestDownloadShelf
 An implementation of DownloadShelf for testing.

### TestDownloadShelf

TestDownloadShelf::TestDownloadShelf
~~~cpp
explicit TestDownloadShelf(Profile* profile);
~~~

### TestDownloadShelf

TestDownloadShelf
~~~cpp
TestDownloadShelf(const TestDownloadShelf&) = delete;
~~~

### operator=

operator=
~~~cpp
TestDownloadShelf& operator=(const TestDownloadShelf&) = delete;
~~~

### ~TestDownloadShelf

TestDownloadShelf::~TestDownloadShelf
~~~cpp
~TestDownloadShelf() override;
~~~

### IsShowing

TestDownloadShelf::IsShowing
~~~cpp
bool IsShowing() const override;
~~~
 DownloadShelf:
### IsClosing

TestDownloadShelf::IsClosing
~~~cpp
bool IsClosing() const override;
~~~

### GetView

TestDownloadShelf::GetView
~~~cpp
views::View* GetView() override;
~~~

### did_add_download

did_add_download
~~~cpp
bool did_add_download() const { return did_add_download_; }
~~~

### DoShowDownload

TestDownloadShelf::DoShowDownload
~~~cpp
void DoShowDownload(DownloadUIModel::DownloadUIModelPtr download) override;
~~~

### DoOpen

TestDownloadShelf::DoOpen
~~~cpp
void DoOpen() override;
~~~

### DoClose

TestDownloadShelf::DoClose
~~~cpp
void DoClose() override;
~~~

### DoHide

TestDownloadShelf::DoHide
~~~cpp
void DoHide() override;
~~~

### DoUnhide

TestDownloadShelf::DoUnhide
~~~cpp
void DoUnhide() override;
~~~

### GetTransientDownloadShowDelay

TestDownloadShelf::GetTransientDownloadShowDelay
~~~cpp
base::TimeDelta GetTransientDownloadShowDelay() const override;
~~~

### is_showing_



~~~cpp

bool is_showing_ = false;

~~~


### did_add_download_



~~~cpp

bool did_add_download_ = false;

~~~


### TestDownloadShelf

TestDownloadShelf
~~~cpp
TestDownloadShelf(const TestDownloadShelf&) = delete;
~~~

### operator=

operator=
~~~cpp
TestDownloadShelf& operator=(const TestDownloadShelf&) = delete;
~~~

### did_add_download

did_add_download
~~~cpp
bool did_add_download() const { return did_add_download_; }
~~~

### IsShowing

TestDownloadShelf::IsShowing
~~~cpp
bool IsShowing() const override;
~~~
 DownloadShelf:
### IsClosing

TestDownloadShelf::IsClosing
~~~cpp
bool IsClosing() const override;
~~~

### GetView

TestDownloadShelf::GetView
~~~cpp
views::View* GetView() override;
~~~

### DoShowDownload

TestDownloadShelf::DoShowDownload
~~~cpp
void DoShowDownload(DownloadUIModel::DownloadUIModelPtr download) override;
~~~

### DoOpen

TestDownloadShelf::DoOpen
~~~cpp
void DoOpen() override;
~~~

### DoClose

TestDownloadShelf::DoClose
~~~cpp
void DoClose() override;
~~~

### DoHide

TestDownloadShelf::DoHide
~~~cpp
void DoHide() override;
~~~

### DoUnhide

TestDownloadShelf::DoUnhide
~~~cpp
void DoUnhide() override;
~~~

### GetTransientDownloadShowDelay

TestDownloadShelf::GetTransientDownloadShowDelay
~~~cpp
base::TimeDelta GetTransientDownloadShowDelay() const override;
~~~

### is_showing_



~~~cpp

bool is_showing_ = false;

~~~


### did_add_download_



~~~cpp

bool did_add_download_ = false;

~~~

