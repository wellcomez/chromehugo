### DragDownloadItem

DragDownloadItem
~~~cpp
void DragDownloadItem(const download::DownloadItem* download,
                      const gfx::Image* icon,
                      gfx::NativeView view);
~~~
 Helper function for download views to use when acting as a drag source for a
 DownloadItem. If `icon` is null, then on Aura no image will accompany the
 drag, and on the Mac the OS will automatically provide an icon. `view` is
 required for macOS, and on Aura it can be null.

