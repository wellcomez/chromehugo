
## class DownloadFilePicker
 Handles showing a dialog to the user to ask for the filename for a download.

### DownloadFilePicker

DownloadFilePicker
~~~cpp
DownloadFilePicker(const DownloadFilePicker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadFilePicker& operator=(const DownloadFilePicker&) = delete;
~~~

### ShowFilePicker

DownloadFilePicker::ShowFilePicker
~~~cpp
static void ShowFilePicker(download::DownloadItem* item,
                             const base::FilePath& suggested_path,
                             ConfirmationCallback callback);
~~~
 Display a file picker dialog for |item|. The |suggested_path| will be used
 as the initial path displayed to the user. |callback| will always be
 invoked even if |item| is destroyed prior to the file picker completing.

### DownloadFilePicker

DownloadFilePicker::DownloadFilePicker
~~~cpp
DownloadFilePicker(download::DownloadItem* item,
                     const base::FilePath& suggested_path,
                     ConfirmationCallback callback);
~~~

### ~DownloadFilePicker

DownloadFilePicker::~DownloadFilePicker
~~~cpp
~DownloadFilePicker() override;
~~~

### OnFileSelected

DownloadFilePicker::OnFileSelected
~~~cpp
void OnFileSelected(const base::FilePath& virtual_path);
~~~
 Runs |file_selected_callback_| with |virtual_path| and then deletes this
 object.

### FileSelected

DownloadFilePicker::FileSelected
~~~cpp
void FileSelected(const base::FilePath& path,
                    int index,
                    void* params) override;
~~~
 SelectFileDialog::Listener implementation.

### FileSelectionCanceled

DownloadFilePicker::FileSelectionCanceled
~~~cpp
void FileSelectionCanceled(void* params) override;
~~~

### OnDownloadDestroyed

DownloadFilePicker::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 DownloadItem::Observer
### suggested_path_



~~~cpp

base::FilePath suggested_path_;

~~~

 Initially suggested path.

### file_selected_callback_



~~~cpp

ConfirmationCallback file_selected_callback_;

~~~

 Callback invoked when a file selection is complete.

### select_file_dialog_



~~~cpp

scoped_refptr<ui::SelectFileDialog> select_file_dialog_;

~~~

 For managing select file dialogs.

### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

 The item to be downloaded.

### DownloadFilePicker

DownloadFilePicker
~~~cpp
DownloadFilePicker(const DownloadFilePicker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadFilePicker& operator=(const DownloadFilePicker&) = delete;
~~~

### ShowFilePicker

DownloadFilePicker::ShowFilePicker
~~~cpp
static void ShowFilePicker(download::DownloadItem* item,
                             const base::FilePath& suggested_path,
                             ConfirmationCallback callback);
~~~
 Display a file picker dialog for |item|. The |suggested_path| will be used
 as the initial path displayed to the user. |callback| will always be
 invoked even if |item| is destroyed prior to the file picker completing.

### OnFileSelected

DownloadFilePicker::OnFileSelected
~~~cpp
void OnFileSelected(const base::FilePath& virtual_path);
~~~
 Runs |file_selected_callback_| with |virtual_path| and then deletes this
 object.

### FileSelected

DownloadFilePicker::FileSelected
~~~cpp
void FileSelected(const base::FilePath& path,
                    int index,
                    void* params) override;
~~~
 SelectFileDialog::Listener implementation.

### FileSelectionCanceled

DownloadFilePicker::FileSelectionCanceled
~~~cpp
void FileSelectionCanceled(void* params) override;
~~~

### OnDownloadDestroyed

DownloadFilePicker::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 DownloadItem::Observer
### suggested_path_



~~~cpp

base::FilePath suggested_path_;

~~~

 Initially suggested path.

### file_selected_callback_



~~~cpp

ConfirmationCallback file_selected_callback_;

~~~

 Callback invoked when a file selection is complete.

### select_file_dialog_



~~~cpp

scoped_refptr<ui::SelectFileDialog> select_file_dialog_;

~~~

 For managing select file dialogs.

### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

 The item to be downloaded.
