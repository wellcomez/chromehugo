
## class DownloadShelfController
 Class for notifying UI when an OfflineItem should be displayed.

### DownloadShelfController

DownloadShelfController::DownloadShelfController
~~~cpp
explicit DownloadShelfController(Profile* profile);
~~~

### DownloadShelfController

DownloadShelfController
~~~cpp
DownloadShelfController(const DownloadShelfController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfController& operator=(const DownloadShelfController&) = delete;
~~~

### ~DownloadShelfController

DownloadShelfController::~DownloadShelfController
~~~cpp
~DownloadShelfController() override;
~~~

### OnItemsAdded

DownloadShelfController::OnItemsAdded
~~~cpp
void OnItemsAdded(
      const OfflineContentProvider::OfflineItemList& items) override;
~~~
 OfflineContentProvider::Observer implementation.

### OnItemRemoved

DownloadShelfController::OnItemRemoved
~~~cpp
void OnItemRemoved(const ContentId& id) override;
~~~

### OnItemUpdated

DownloadShelfController::OnItemUpdated
~~~cpp
void OnItemUpdated(const OfflineItem& item,
                     const absl::optional<UpdateDelta>& update_delta) override;
~~~

### OnContentProviderGoingDown

DownloadShelfController::OnContentProviderGoingDown
~~~cpp
void OnContentProviderGoingDown() override;
~~~

### OnNewOfflineItemReady

DownloadShelfController::OnNewOfflineItemReady
~~~cpp
void OnNewOfflineItemReady(DownloadUIModel::DownloadUIModelPtr model);
~~~
 Called when a new OfflineItem is to be displayed on UI.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### aggregator_



~~~cpp

raw_ptr<OfflineContentAggregator> aggregator_;

~~~


### observation_



~~~cpp

base::ScopedObservation<OfflineContentProvider,
                          OfflineContentProvider::Observer>
      observation_{this};

~~~


### DownloadShelfController

DownloadShelfController
~~~cpp
DownloadShelfController(const DownloadShelfController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelfController& operator=(const DownloadShelfController&) = delete;
~~~

### OnItemsAdded

DownloadShelfController::OnItemsAdded
~~~cpp
void OnItemsAdded(
      const OfflineContentProvider::OfflineItemList& items) override;
~~~
 OfflineContentProvider::Observer implementation.

### OnItemRemoved

DownloadShelfController::OnItemRemoved
~~~cpp
void OnItemRemoved(const ContentId& id) override;
~~~

### OnItemUpdated

DownloadShelfController::OnItemUpdated
~~~cpp
void OnItemUpdated(const OfflineItem& item,
                     const absl::optional<UpdateDelta>& update_delta) override;
~~~

### OnContentProviderGoingDown

DownloadShelfController::OnContentProviderGoingDown
~~~cpp
void OnContentProviderGoingDown() override;
~~~

### OnNewOfflineItemReady

DownloadShelfController::OnNewOfflineItemReady
~~~cpp
void OnNewOfflineItemReady(DownloadUIModel::DownloadUIModelPtr model);
~~~
 Called when a new OfflineItem is to be displayed on UI.

### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### aggregator_



~~~cpp

raw_ptr<OfflineContentAggregator> aggregator_;

~~~


### observation_



~~~cpp

base::ScopedObservation<OfflineContentProvider,
                          OfflineContentProvider::Observer>
      observation_{this};

~~~

