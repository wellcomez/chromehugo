
## class DownloadCompletionBlocker
 A subsystem may use a DownloadCompletionBlocker in conjunction with
 DownloadManagerDelegate::ShouldCompleteDownload() in order to block the
 completion of a DownloadItem. CompleteDownload() will run the most recent
 callback set.

### DownloadCompletionBlocker

DownloadCompletionBlocker::DownloadCompletionBlocker
~~~cpp
DownloadCompletionBlocker();
~~~

### DownloadCompletionBlocker

DownloadCompletionBlocker
~~~cpp
DownloadCompletionBlocker(const DownloadCompletionBlocker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCompletionBlocker& operator=(const DownloadCompletionBlocker&) =
      delete;
~~~

### ~DownloadCompletionBlocker

DownloadCompletionBlocker::~DownloadCompletionBlocker
~~~cpp
~DownloadCompletionBlocker() override;
~~~

### is_complete

is_complete
~~~cpp
bool is_complete() const { return is_complete_; }
~~~

### set_callback

set_callback
~~~cpp
void set_callback(base::OnceClosure callback) {
    if (!is_complete())
      callback_ = std::move(callback);
  }
~~~

### CompleteDownload

DownloadCompletionBlocker::CompleteDownload
~~~cpp
virtual void CompleteDownload();
~~~
 Mark this download item as complete with respect to this blocker. (Other
 blockers may continue to block the item.) Run |callback_|. This method may
 only be called once, so |callback_| will only be called once.  Subclasses
 must call the base implementation if they override this method.

### is_complete_



~~~cpp

bool is_complete_;

~~~


### callback_



~~~cpp

base::OnceClosure callback_;

~~~


### DownloadCompletionBlocker

DownloadCompletionBlocker
~~~cpp
DownloadCompletionBlocker(const DownloadCompletionBlocker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCompletionBlocker& operator=(const DownloadCompletionBlocker&) =
      delete;
~~~

### is_complete

is_complete
~~~cpp
bool is_complete() const { return is_complete_; }
~~~

### set_callback

set_callback
~~~cpp
void set_callback(base::OnceClosure callback) {
    if (!is_complete())
      callback_ = std::move(callback);
  }
~~~

### CompleteDownload

DownloadCompletionBlocker::CompleteDownload
~~~cpp
virtual void CompleteDownload();
~~~
 Mark this download item as complete with respect to this blocker. (Other
 blockers may continue to block the item.) Run |callback_|. This method may
 only be called once, so |callback_| will only be called once.  Subclasses
 must call the base implementation if they override this method.

### is_complete_



~~~cpp

bool is_complete_;

~~~


### callback_



~~~cpp

base::OnceClosure callback_;

~~~

