
## class InsecureDownloadDialogBridge
 Class for showing dialogs to asks whether user wants to download an insecure
 URL.

### GetInstance

InsecureDownloadDialogBridge::GetInstance
~~~cpp
static InsecureDownloadDialogBridge* GetInstance();
~~~

### InsecureDownloadDialogBridge

InsecureDownloadDialogBridge::InsecureDownloadDialogBridge
~~~cpp
InsecureDownloadDialogBridge();
~~~

### InsecureDownloadDialogBridge

InsecureDownloadDialogBridge
~~~cpp
InsecureDownloadDialogBridge(const InsecureDownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
InsecureDownloadDialogBridge& operator=(const InsecureDownloadDialogBridge&) =
      delete;
~~~

### ~InsecureDownloadDialogBridge

InsecureDownloadDialogBridge::~InsecureDownloadDialogBridge
~~~cpp
~InsecureDownloadDialogBridge() override;
~~~

### CreateDialog

InsecureDownloadDialogBridge::CreateDialog
~~~cpp
void CreateDialog(download::DownloadItem* download,
                    const base::FilePath& base_name,
                    ui::WindowAndroid* window_android,
                    InsecureDownloadDialogCallback callback);
~~~
 Called to create and show a dialog for an insecure download.

### OnConfirmed

InsecureDownloadDialogBridge::OnConfirmed
~~~cpp
void OnConfirmed(JNIEnv* env, jlong callback_id, jboolean accepted);
~~~
 Called from Java via JNI.

### download_items_



~~~cpp

std::vector<download::DownloadItem*> download_items_;

~~~

 Download items that are requesting the dialog. Could get deleted while
 the dialog is showing.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~

 Validator for all JNI callbacks.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.

### InsecureDownloadDialogBridge

InsecureDownloadDialogBridge
~~~cpp
InsecureDownloadDialogBridge(const InsecureDownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
InsecureDownloadDialogBridge& operator=(const InsecureDownloadDialogBridge&) =
      delete;
~~~

### GetInstance

InsecureDownloadDialogBridge::GetInstance
~~~cpp
static InsecureDownloadDialogBridge* GetInstance();
~~~

### CreateDialog

InsecureDownloadDialogBridge::CreateDialog
~~~cpp
void CreateDialog(download::DownloadItem* download,
                    const base::FilePath& base_name,
                    ui::WindowAndroid* window_android,
                    InsecureDownloadDialogCallback callback);
~~~
 Called to create and show a dialog for an insecure download.

### OnConfirmed

InsecureDownloadDialogBridge::OnConfirmed
~~~cpp
void OnConfirmed(JNIEnv* env, jlong callback_id, jboolean accepted);
~~~
 Called from Java via JNI.

### download_items_



~~~cpp

std::vector<download::DownloadItem*> download_items_;

~~~

 Download items that are requesting the dialog. Could get deleted while
 the dialog is showing.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~

 Validator for all JNI callbacks.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.
