
## class DangerousDownloadInfoBarDelegate
 An infobar that asks if user wants to download a dangerous file.

 Note that this infobar does not expire if the user subsequently navigates,
 since such navigations won't automatically cancel the underlying download.

### Create

DangerousDownloadInfoBarDelegate::Create
~~~cpp
static void Create(infobars::ContentInfoBarManager* infobar_manager,
                     download::DownloadItem* download_item);
~~~

### DangerousDownloadInfoBarDelegate

DangerousDownloadInfoBarDelegate
~~~cpp
DangerousDownloadInfoBarDelegate(const DangerousDownloadInfoBarDelegate&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DangerousDownloadInfoBarDelegate& operator=(
      const DangerousDownloadInfoBarDelegate&) = delete;
~~~

### ~DangerousDownloadInfoBarDelegate

DangerousDownloadInfoBarDelegate::~DangerousDownloadInfoBarDelegate
~~~cpp
~DangerousDownloadInfoBarDelegate() override;
~~~

### OnDownloadDestroyed

DangerousDownloadInfoBarDelegate::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer:
### DangerousDownloadInfoBarDelegate

DangerousDownloadInfoBarDelegate::DangerousDownloadInfoBarDelegate
~~~cpp
explicit DangerousDownloadInfoBarDelegate(
      download::DownloadItem* download_item);
~~~

### GetIdentifier

DangerousDownloadInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 ConfirmInfoBarDelegate:
### GetIconId

DangerousDownloadInfoBarDelegate::GetIconId
~~~cpp
int GetIconId() const override;
~~~

### ShouldExpire

DangerousDownloadInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### InfoBarDismissed

DangerousDownloadInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### GetMessageText

DangerousDownloadInfoBarDelegate::GetMessageText
~~~cpp
std::u16string GetMessageText() const override;
~~~

### Accept

DangerousDownloadInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

DangerousDownloadInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

 The download item that is requesting the infobar. Could get deleted while
 the infobar is showing.

### message_text_



~~~cpp

std::u16string message_text_;

~~~


### DangerousDownloadInfoBarDelegate

DangerousDownloadInfoBarDelegate
~~~cpp
DangerousDownloadInfoBarDelegate(const DangerousDownloadInfoBarDelegate&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DangerousDownloadInfoBarDelegate& operator=(
      const DangerousDownloadInfoBarDelegate&) = delete;
~~~

### Create

DangerousDownloadInfoBarDelegate::Create
~~~cpp
static void Create(infobars::ContentInfoBarManager* infobar_manager,
                     download::DownloadItem* download_item);
~~~

### OnDownloadDestroyed

DangerousDownloadInfoBarDelegate::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer:
### GetIdentifier

DangerousDownloadInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 ConfirmInfoBarDelegate:
### GetIconId

DangerousDownloadInfoBarDelegate::GetIconId
~~~cpp
int GetIconId() const override;
~~~

### ShouldExpire

DangerousDownloadInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### InfoBarDismissed

DangerousDownloadInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### GetMessageText

DangerousDownloadInfoBarDelegate::GetMessageText
~~~cpp
std::u16string GetMessageText() const override;
~~~

### Accept

DangerousDownloadInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

DangerousDownloadInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

 The download item that is requesting the infobar. Could get deleted while
 the infobar is showing.

### message_text_



~~~cpp

std::u16string message_text_;

~~~

