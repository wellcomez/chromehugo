
## class InsecureDownloadInfoBarDelegate
 An infobar that asks if user wants to download an insecurely delivered file.

 Note that this infobar does not expire if the user subsequently navigates,
 since such navigations won't automatically cancel the underlying download.

### Create

InsecureDownloadInfoBarDelegate::Create
~~~cpp
static void Create(
      infobars::ContentInfoBarManager* infobar_manager,
      const base::FilePath& basename,
      download::DownloadItem::InsecureDownloadStatus insecure_download_status,
      ResultCallback callback);
~~~

### InsecureDownloadInfoBarDelegate

InsecureDownloadInfoBarDelegate
~~~cpp
InsecureDownloadInfoBarDelegate(const InsecureDownloadInfoBarDelegate&) =
      delete;
~~~

### operator=

operator=
~~~cpp
InsecureDownloadInfoBarDelegate& operator=(
      const InsecureDownloadInfoBarDelegate&) = delete;
~~~

### ~InsecureDownloadInfoBarDelegate

InsecureDownloadInfoBarDelegate::~InsecureDownloadInfoBarDelegate
~~~cpp
~InsecureDownloadInfoBarDelegate() override;
~~~

### InsecureDownloadInfoBarDelegate

InsecureDownloadInfoBarDelegate::InsecureDownloadInfoBarDelegate
~~~cpp
explicit InsecureDownloadInfoBarDelegate(
      const base::FilePath& basename,
      download::DownloadItem::InsecureDownloadStatus insecure_download_status,
      ResultCallback callback);
~~~

### GetIdentifier

InsecureDownloadInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 ConfirmInfoBarDelegate:
### GetIconId

InsecureDownloadInfoBarDelegate::GetIconId
~~~cpp
int GetIconId() const override;
~~~

### ShouldExpire

InsecureDownloadInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### InfoBarDismissed

InsecureDownloadInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### GetMessageText

InsecureDownloadInfoBarDelegate::GetMessageText
~~~cpp
std::u16string GetMessageText() const override;
~~~

### GetButtonLabel

InsecureDownloadInfoBarDelegate::GetButtonLabel
~~~cpp
std::u16string GetButtonLabel(InfoBarButton button) const override;
~~~

### Accept

InsecureDownloadInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

InsecureDownloadInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### PostReply

InsecureDownloadInfoBarDelegate::PostReply
~~~cpp
void PostReply(bool should_download);
~~~
 Calls callback_ with the appropriate result.

### message_text_



~~~cpp

std::u16string message_text_;

~~~


### insecure_download_status_



~~~cpp

download::DownloadItem::InsecureDownloadStatus insecure_download_status_;

~~~


### callback_



~~~cpp

ResultCallback callback_;

~~~


### InsecureDownloadInfoBarDelegate

InsecureDownloadInfoBarDelegate
~~~cpp
InsecureDownloadInfoBarDelegate(const InsecureDownloadInfoBarDelegate&) =
      delete;
~~~

### operator=

operator=
~~~cpp
InsecureDownloadInfoBarDelegate& operator=(
      const InsecureDownloadInfoBarDelegate&) = delete;
~~~

### Create

InsecureDownloadInfoBarDelegate::Create
~~~cpp
static void Create(
      infobars::ContentInfoBarManager* infobar_manager,
      const base::FilePath& basename,
      download::DownloadItem::InsecureDownloadStatus insecure_download_status,
      ResultCallback callback);
~~~

### GetIdentifier

InsecureDownloadInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 ConfirmInfoBarDelegate:
### GetIconId

InsecureDownloadInfoBarDelegate::GetIconId
~~~cpp
int GetIconId() const override;
~~~

### ShouldExpire

InsecureDownloadInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### InfoBarDismissed

InsecureDownloadInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### GetMessageText

InsecureDownloadInfoBarDelegate::GetMessageText
~~~cpp
std::u16string GetMessageText() const override;
~~~

### GetButtonLabel

InsecureDownloadInfoBarDelegate::GetButtonLabel
~~~cpp
std::u16string GetButtonLabel(InfoBarButton button) const override;
~~~

### Accept

InsecureDownloadInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

InsecureDownloadInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### PostReply

InsecureDownloadInfoBarDelegate::PostReply
~~~cpp
void PostReply(bool should_download);
~~~
 Calls callback_ with the appropriate result.

### message_text_



~~~cpp

std::u16string message_text_;

~~~


### insecure_download_status_



~~~cpp

download::DownloadItem::InsecureDownloadStatus insecure_download_status_;

~~~


### callback_



~~~cpp

ResultCallback callback_;

~~~

