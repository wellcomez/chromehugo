
## class DownloadStartupUtils
 Native side of DownloadStartupUtils.java.

### DownloadStartupUtils

DownloadStartupUtils
~~~cpp
DownloadStartupUtils() = delete;
~~~

### DownloadStartupUtils

DownloadStartupUtils
~~~cpp
DownloadStartupUtils(const DownloadStartupUtils&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadStartupUtils& operator=(const DownloadStartupUtils&) = delete;
~~~

### EnsureDownloadSystemInitialized

DownloadStartupUtils::EnsureDownloadSystemInitialized
~~~cpp
static ProfileKey* EnsureDownloadSystemInitialized(ProfileKey* profile_key);
~~~
 Ensures that the download system is initialized for the targeted profile.

 If |profile_key| is null, reduced mode will be assumed. The returned value
 is the ProfileKey that was used.

### DownloadStartupUtils

DownloadStartupUtils
~~~cpp
DownloadStartupUtils() = delete;
~~~

### DownloadStartupUtils

DownloadStartupUtils
~~~cpp
DownloadStartupUtils(const DownloadStartupUtils&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadStartupUtils& operator=(const DownloadStartupUtils&) = delete;
~~~

### EnsureDownloadSystemInitialized

DownloadStartupUtils::EnsureDownloadSystemInitialized
~~~cpp
static ProfileKey* EnsureDownloadSystemInitialized(ProfileKey* profile_key);
~~~
 Ensures that the download system is initialized for the targeted profile.

 If |profile_key| is null, reduced mode will be assumed. The returned value
 is the ProfileKey that was used.
