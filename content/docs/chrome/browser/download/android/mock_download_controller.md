
## class MockDownloadController
 Mock implementation of the DownloadController.

### MockDownloadController

MockDownloadController::MockDownloadController
~~~cpp
MockDownloadController();
~~~

### MockDownloadController

MockDownloadController
~~~cpp
MockDownloadController(const MockDownloadController&) = delete;
~~~

### operator=

operator=
~~~cpp
MockDownloadController& operator=(const MockDownloadController&) = delete;
~~~

### ~MockDownloadController

MockDownloadController::~MockDownloadController
~~~cpp
~MockDownloadController() override;
~~~

### OnDownloadStarted

MockDownloadController::OnDownloadStarted
~~~cpp
void OnDownloadStarted(download::DownloadItem* download_item) override;
~~~
 DownloadControllerBase implementation.

### StartContextMenuDownload

MockDownloadController::StartContextMenuDownload
~~~cpp
void StartContextMenuDownload(const content::ContextMenuParams& params,
                                content::WebContents* web_contents,
                                bool is_link) override;
~~~

### AcquireFileAccessPermission

MockDownloadController::AcquireFileAccessPermission
~~~cpp
void AcquireFileAccessPermission(
      const content::WebContents::Getter& wc_getter,
      AcquireFileAccessPermissionCallback callback) override;
~~~

### SetApproveFileAccessRequestForTesting

MockDownloadController::SetApproveFileAccessRequestForTesting
~~~cpp
void SetApproveFileAccessRequestForTesting(bool approve) override;
~~~

### CreateAndroidDownload

MockDownloadController::CreateAndroidDownload
~~~cpp
void CreateAndroidDownload(const content::WebContents::Getter& wc_getter,
                             const DownloadInfo& info) override;
~~~

### AboutToResumeDownload

MockDownloadController::AboutToResumeDownload
~~~cpp
void AboutToResumeDownload(download::DownloadItem* download_item) override;
~~~

### approve_file_access_request_



~~~cpp

bool approve_file_access_request_;

~~~


### MockDownloadController

MockDownloadController
~~~cpp
MockDownloadController(const MockDownloadController&) = delete;
~~~

### operator=

operator=
~~~cpp
MockDownloadController& operator=(const MockDownloadController&) = delete;
~~~

### OnDownloadStarted

MockDownloadController::OnDownloadStarted
~~~cpp
void OnDownloadStarted(download::DownloadItem* download_item) override;
~~~
 DownloadControllerBase implementation.

### StartContextMenuDownload

MockDownloadController::StartContextMenuDownload
~~~cpp
void StartContextMenuDownload(const content::ContextMenuParams& params,
                                content::WebContents* web_contents,
                                bool is_link) override;
~~~

### AcquireFileAccessPermission

MockDownloadController::AcquireFileAccessPermission
~~~cpp
void AcquireFileAccessPermission(
      const content::WebContents::Getter& wc_getter,
      AcquireFileAccessPermissionCallback callback) override;
~~~

### SetApproveFileAccessRequestForTesting

MockDownloadController::SetApproveFileAccessRequestForTesting
~~~cpp
void SetApproveFileAccessRequestForTesting(bool approve) override;
~~~

### CreateAndroidDownload

MockDownloadController::CreateAndroidDownload
~~~cpp
void CreateAndroidDownload(const content::WebContents::Getter& wc_getter,
                             const DownloadInfo& info) override;
~~~

### AboutToResumeDownload

MockDownloadController::AboutToResumeDownload
~~~cpp
void AboutToResumeDownload(download::DownloadItem* download_item) override;
~~~

### approve_file_access_request_



~~~cpp

bool approve_file_access_request_;

~~~

