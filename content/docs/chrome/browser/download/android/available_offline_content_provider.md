
## class AvailableOfflineContentProvider
 Provides access to items available while offline.

### AvailableOfflineContentProvider

AvailableOfflineContentProvider::AvailableOfflineContentProvider
~~~cpp
explicit AvailableOfflineContentProvider(int render_process_host_id);
~~~
 Public for testing.

### AvailableOfflineContentProvider

AvailableOfflineContentProvider
~~~cpp
AvailableOfflineContentProvider(const AvailableOfflineContentProvider&) =
      delete;
~~~

### operator=

operator=
~~~cpp
AvailableOfflineContentProvider& operator=(
      const AvailableOfflineContentProvider&) = delete;
~~~

### ~AvailableOfflineContentProvider

AvailableOfflineContentProvider::~AvailableOfflineContentProvider
~~~cpp
~AvailableOfflineContentProvider() override;
~~~

### List

AvailableOfflineContentProvider::List
~~~cpp
void List(ListCallback callback) override;
~~~
 chrome::mojom::AvailableOfflineContentProvider methods.

### LaunchItem

AvailableOfflineContentProvider::LaunchItem
~~~cpp
void LaunchItem(const std::string& item_id,
                  const std::string& name_space) override;
~~~

### LaunchDownloadsPage

AvailableOfflineContentProvider::LaunchDownloadsPage
~~~cpp
void LaunchDownloadsPage(bool open_prefetched_articles_tab) override;
~~~

### ListVisibilityChanged

AvailableOfflineContentProvider::ListVisibilityChanged
~~~cpp
void ListVisibilityChanged(bool is_visible) override;
~~~

### Create

AvailableOfflineContentProvider::Create
~~~cpp
static void Create(
      int render_process_host_id,
      mojo::PendingReceiver<chrome::mojom::AvailableOfflineContentProvider>
          receiver);
~~~

### ListFinalize

AvailableOfflineContentProvider::ListFinalize
~~~cpp
void ListFinalize(
      AvailableOfflineContentProvider::ListCallback callback,
      offline_items_collection::OfflineContentAggregator* aggregator,
      const std::vector<offline_items_collection::OfflineItem>& all_items);
~~~

### GetProfile

AvailableOfflineContentProvider::GetProfile
~~~cpp
Profile* GetProfile();
~~~

### SetSelfOwnedReceiver

AvailableOfflineContentProvider::SetSelfOwnedReceiver
~~~cpp
void SetSelfOwnedReceiver(const mojo::SelfOwnedReceiverRef<
                            chrome::mojom::AvailableOfflineContentProvider>&
                                provider_self_owned_receiver);
~~~

### CloseSelfOwnedReceiverIfNeeded

AvailableOfflineContentProvider::CloseSelfOwnedReceiverIfNeeded
~~~cpp
void CloseSelfOwnedReceiverIfNeeded();
~~~

### render_process_host_id_



~~~cpp

const int render_process_host_id_;

~~~


### provider_self_owned_receiver_



~~~cpp

mojo::SelfOwnedReceiverRef<chrome::mojom::AvailableOfflineContentProvider>
      provider_self_owned_receiver_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<AvailableOfflineContentProvider> weak_ptr_factory_{this};

~~~


### AvailableOfflineContentProvider

AvailableOfflineContentProvider
~~~cpp
AvailableOfflineContentProvider(const AvailableOfflineContentProvider&) =
      delete;
~~~

### operator=

operator=
~~~cpp
AvailableOfflineContentProvider& operator=(
      const AvailableOfflineContentProvider&) = delete;
~~~

### List

AvailableOfflineContentProvider::List
~~~cpp
void List(ListCallback callback) override;
~~~
 chrome::mojom::AvailableOfflineContentProvider methods.

### LaunchItem

AvailableOfflineContentProvider::LaunchItem
~~~cpp
void LaunchItem(const std::string& item_id,
                  const std::string& name_space) override;
~~~

### LaunchDownloadsPage

AvailableOfflineContentProvider::LaunchDownloadsPage
~~~cpp
void LaunchDownloadsPage(bool open_prefetched_articles_tab) override;
~~~

### ListVisibilityChanged

AvailableOfflineContentProvider::ListVisibilityChanged
~~~cpp
void ListVisibilityChanged(bool is_visible) override;
~~~

### Create

AvailableOfflineContentProvider::Create
~~~cpp
static void Create(
      int render_process_host_id,
      mojo::PendingReceiver<chrome::mojom::AvailableOfflineContentProvider>
          receiver);
~~~

### ListFinalize

AvailableOfflineContentProvider::ListFinalize
~~~cpp
void ListFinalize(
      AvailableOfflineContentProvider::ListCallback callback,
      offline_items_collection::OfflineContentAggregator* aggregator,
      const std::vector<offline_items_collection::OfflineItem>& all_items);
~~~

### GetProfile

AvailableOfflineContentProvider::GetProfile
~~~cpp
Profile* GetProfile();
~~~

### SetSelfOwnedReceiver

AvailableOfflineContentProvider::SetSelfOwnedReceiver
~~~cpp
void SetSelfOwnedReceiver(const mojo::SelfOwnedReceiverRef<
                            chrome::mojom::AvailableOfflineContentProvider>&
                                provider_self_owned_receiver);
~~~

### CloseSelfOwnedReceiverIfNeeded

AvailableOfflineContentProvider::CloseSelfOwnedReceiverIfNeeded
~~~cpp
void CloseSelfOwnedReceiverIfNeeded();
~~~

### render_process_host_id_



~~~cpp

const int render_process_host_id_;

~~~


### provider_self_owned_receiver_



~~~cpp

mojo::SelfOwnedReceiverRef<chrome::mojom::AvailableOfflineContentProvider>
      provider_self_owned_receiver_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<AvailableOfflineContentProvider> weak_ptr_factory_{this};

~~~

