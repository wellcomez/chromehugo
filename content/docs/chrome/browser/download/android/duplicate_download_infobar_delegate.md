
## class DuplicateDownloadInfoBarDelegate
 An infobar that asks if user wants to continue downloading when there is
 already a duplicate file in storage. If user chooses to proceed,
 a new file will be created.

 Note that this infobar does not expire if the user subsequently navigates,
 since such navigations won't automatically cancel the underlying download.

### GetFilePath

GetFilePath
~~~cpp
virtual std::string GetFilePath() const = 0;
~~~
 Gets the file path to be downloaded.

### IsOfflinePage

DuplicateDownloadInfoBarDelegate::IsOfflinePage
~~~cpp
virtual bool IsOfflinePage() const;
~~~
 Whether the download is for offline page.

### GetPageURL

DuplicateDownloadInfoBarDelegate::GetPageURL
~~~cpp
virtual std::string GetPageURL() const;
~~~

### GetOTRProfileID

DuplicateDownloadInfoBarDelegate::GetOTRProfileID
~~~cpp
virtual absl::optional<Profile::OTRProfileID> GetOTRProfileID() const;
~~~
 The OTRProfileID of the download. Null if for regular mode.

### DuplicateRequestExists

DuplicateDownloadInfoBarDelegate::DuplicateRequestExists
~~~cpp
virtual bool DuplicateRequestExists() const;
~~~
 Whether the duplicate is an in-progress request or completed download.

### GetMessageText

DuplicateDownloadInfoBarDelegate::GetMessageText
~~~cpp
std::u16string GetMessageText() const override;
~~~
 ConfirmInfoBarDelegate implementation.

### ShouldExpire

DuplicateDownloadInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~

### GetFilePath

GetFilePath
~~~cpp
virtual std::string GetFilePath() const = 0;
~~~
 Gets the file path to be downloaded.

### IsOfflinePage

DuplicateDownloadInfoBarDelegate::IsOfflinePage
~~~cpp
virtual bool IsOfflinePage() const;
~~~
 Whether the download is for offline page.

### GetPageURL

DuplicateDownloadInfoBarDelegate::GetPageURL
~~~cpp
virtual std::string GetPageURL() const;
~~~

### GetOTRProfileID

DuplicateDownloadInfoBarDelegate::GetOTRProfileID
~~~cpp
virtual absl::optional<Profile::OTRProfileID> GetOTRProfileID() const;
~~~
 The OTRProfileID of the download. Null if for regular mode.

### DuplicateRequestExists

DuplicateDownloadInfoBarDelegate::DuplicateRequestExists
~~~cpp
virtual bool DuplicateRequestExists() const;
~~~
 Whether the duplicate is an in-progress request or completed download.

### GetMessageText

DuplicateDownloadInfoBarDelegate::GetMessageText
~~~cpp
std::u16string GetMessageText() const override;
~~~
 ConfirmInfoBarDelegate implementation.

### ShouldExpire

DuplicateDownloadInfoBarDelegate::ShouldExpire
~~~cpp
bool ShouldExpire(const NavigationDetails& details) const override;
~~~
