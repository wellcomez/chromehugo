
## class ChromeDuplicateDownloadInfoBarDelegate
 An infobar delegate that starts from the given file path.

### ChromeDuplicateDownloadInfoBarDelegate

ChromeDuplicateDownloadInfoBarDelegate
~~~cpp
ChromeDuplicateDownloadInfoBarDelegate(
      const ChromeDuplicateDownloadInfoBarDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeDuplicateDownloadInfoBarDelegate& operator=(
      const ChromeDuplicateDownloadInfoBarDelegate&) = delete;
~~~

### ~ChromeDuplicateDownloadInfoBarDelegate

ChromeDuplicateDownloadInfoBarDelegate::~ChromeDuplicateDownloadInfoBarDelegate
~~~cpp
~ChromeDuplicateDownloadInfoBarDelegate() override;
~~~

### Create

ChromeDuplicateDownloadInfoBarDelegate::Create
~~~cpp
static void Create(infobars::ContentInfoBarManager* infobar_manager,
                     download::DownloadItem* download_item,
                     const base::FilePath& file_path,
                     DownloadTargetDeterminerDelegate::ConfirmationCallback
                         file_selected_callback);
~~~

### OnDownloadDestroyed

ChromeDuplicateDownloadInfoBarDelegate::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer
### ChromeDuplicateDownloadInfoBarDelegate

ChromeDuplicateDownloadInfoBarDelegate::ChromeDuplicateDownloadInfoBarDelegate
~~~cpp
ChromeDuplicateDownloadInfoBarDelegate(
      download::DownloadItem* download_item,
      const base::FilePath& file_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback);
~~~

### GetIdentifier

ChromeDuplicateDownloadInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 DownloadOverwriteInfoBarDelegate:
### Accept

ChromeDuplicateDownloadInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

ChromeDuplicateDownloadInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### GetFilePath

ChromeDuplicateDownloadInfoBarDelegate::GetFilePath
~~~cpp
std::string GetFilePath() const override;
~~~

### InfoBarDismissed

ChromeDuplicateDownloadInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### GetOTRProfileID

ChromeDuplicateDownloadInfoBarDelegate::GetOTRProfileID
~~~cpp
absl::optional<Profile::OTRProfileID> GetOTRProfileID() const override;
~~~

### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

 The download item that is requesting the infobar. Could get deleted while
 the infobar is showing.

### file_path_



~~~cpp

base::FilePath file_path_;

~~~

 The target file path to be downloaded. This is used to show users the
 file name that will be used.

### file_selected_callback_



~~~cpp

DownloadTargetDeterminerDelegate::ConfirmationCallback
      file_selected_callback_;

~~~

 A callback to download target determiner to notify that file selection
 is made (or cancelled).

### ChromeDuplicateDownloadInfoBarDelegate

ChromeDuplicateDownloadInfoBarDelegate
~~~cpp
ChromeDuplicateDownloadInfoBarDelegate(
      const ChromeDuplicateDownloadInfoBarDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
ChromeDuplicateDownloadInfoBarDelegate& operator=(
      const ChromeDuplicateDownloadInfoBarDelegate&) = delete;
~~~

### Create

ChromeDuplicateDownloadInfoBarDelegate::Create
~~~cpp
static void Create(infobars::ContentInfoBarManager* infobar_manager,
                     download::DownloadItem* download_item,
                     const base::FilePath& file_path,
                     DownloadTargetDeterminerDelegate::ConfirmationCallback
                         file_selected_callback);
~~~

### OnDownloadDestroyed

ChromeDuplicateDownloadInfoBarDelegate::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer
### GetIdentifier

ChromeDuplicateDownloadInfoBarDelegate::GetIdentifier
~~~cpp
infobars::InfoBarDelegate::InfoBarIdentifier GetIdentifier() const override;
~~~
 DownloadOverwriteInfoBarDelegate:
### Accept

ChromeDuplicateDownloadInfoBarDelegate::Accept
~~~cpp
bool Accept() override;
~~~

### Cancel

ChromeDuplicateDownloadInfoBarDelegate::Cancel
~~~cpp
bool Cancel() override;
~~~

### GetFilePath

ChromeDuplicateDownloadInfoBarDelegate::GetFilePath
~~~cpp
std::string GetFilePath() const override;
~~~

### InfoBarDismissed

ChromeDuplicateDownloadInfoBarDelegate::InfoBarDismissed
~~~cpp
void InfoBarDismissed() override;
~~~

### GetOTRProfileID

ChromeDuplicateDownloadInfoBarDelegate::GetOTRProfileID
~~~cpp
absl::optional<Profile::OTRProfileID> GetOTRProfileID() const override;
~~~

### download_item_



~~~cpp

raw_ptr<download::DownloadItem> download_item_;

~~~

 The download item that is requesting the infobar. Could get deleted while
 the infobar is showing.

### file_path_



~~~cpp

base::FilePath file_path_;

~~~

 The target file path to be downloaded. This is used to show users the
 file name that will be used.

### file_selected_callback_



~~~cpp

DownloadTargetDeterminerDelegate::ConfirmationCallback
      file_selected_callback_;

~~~

 A callback to download target determiner to notify that file selection
 is made (or cancelled).
