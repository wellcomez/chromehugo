
## class DownloadMessageBridge
 namespace content
 Class for showing message to warn the user about incognito download.

### DownloadMessageBridge

DownloadMessageBridge::DownloadMessageBridge
~~~cpp
DownloadMessageBridge();
~~~

### DownloadMessageBridge

DownloadMessageBridge
~~~cpp
DownloadMessageBridge(const DownloadMessageBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadMessageBridge& operator=(const DownloadMessageBridge&) = delete;
~~~

### ~DownloadMessageBridge

DownloadMessageBridge::~DownloadMessageBridge
~~~cpp
~DownloadMessageBridge() override;
~~~

### ShowIncognitoDownloadMessage

DownloadMessageBridge::ShowIncognitoDownloadMessage
~~~cpp
void ShowIncognitoDownloadMessage(DownloadMessageRequestCallback callback);
~~~

### OnConfirmed

DownloadMessageBridge::OnConfirmed
~~~cpp
void OnConfirmed(JNIEnv* env, jlong callback_id, jboolean accepted);
~~~
 Called from Java via JNI.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~

 Validator for all JNI callbacks.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.

### DownloadMessageBridge

DownloadMessageBridge
~~~cpp
DownloadMessageBridge(const DownloadMessageBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadMessageBridge& operator=(const DownloadMessageBridge&) = delete;
~~~

### ShowIncognitoDownloadMessage

DownloadMessageBridge::ShowIncognitoDownloadMessage
~~~cpp
void ShowIncognitoDownloadMessage(DownloadMessageRequestCallback callback);
~~~

### OnConfirmed

DownloadMessageBridge::OnConfirmed
~~~cpp
void OnConfirmed(JNIEnv* env, jlong callback_id, jboolean accepted);
~~~
 Called from Java via JNI.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~

 Validator for all JNI callbacks.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.
