
## class DownloadManagerService
 namespace download
 Native side of DownloadManagerService.java. The native object is owned by its
 Java object.

### CreateAutoResumptionHandler

DownloadManagerService::CreateAutoResumptionHandler
~~~cpp
static void CreateAutoResumptionHandler();
~~~

### OnDownloadCanceled

DownloadManagerService::OnDownloadCanceled
~~~cpp
static void OnDownloadCanceled(download::DownloadItem* download,
                                 bool has_no_external_storage);
~~~

### GetInstance

DownloadManagerService::GetInstance
~~~cpp
static DownloadManagerService* GetInstance();
~~~

### CreateJavaDownloadInfo

DownloadManagerService::CreateJavaDownloadInfo
~~~cpp
static base::android::ScopedJavaLocalRef<jobject> CreateJavaDownloadInfo(
      JNIEnv* env,
      download::DownloadItem* item);
~~~

### DownloadManagerService

DownloadManagerService::DownloadManagerService
~~~cpp
DownloadManagerService();
~~~

### DownloadManagerService

DownloadManagerService
~~~cpp
DownloadManagerService(const DownloadManagerService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadManagerService& operator=(const DownloadManagerService&) = delete;
~~~

### ~DownloadManagerService

DownloadManagerService::~DownloadManagerService
~~~cpp
~DownloadManagerService() override;
~~~

### Init

DownloadManagerService::Init
~~~cpp
void Init(JNIEnv* env, jobject obj, bool is_profile_added);
~~~
 Called to Initialize this object. If |is_profile_added| is false,
 it means only a minimal browser is launched. OnProfileAdded() will
 be called later when the profile is added.

### OnProfileAdded

DownloadManagerService::OnProfileAdded
~~~cpp
void OnProfileAdded(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jobject>& j_profile);
~~~
 Called when the profile is added to the ProfileManager and fully
 initialized.

### OnProfileAdded

DownloadManagerService::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile);
~~~

### HandleOMADownload

DownloadManagerService::HandleOMADownload
~~~cpp
void HandleOMADownload(download::DownloadItem* download,
                         int64_t system_download_id);
~~~
 Called to handle subsequent steps, after a download was determined as a OMA
 download type.

### OpenDownload

DownloadManagerService::OpenDownload
~~~cpp
void OpenDownload(download::DownloadItem* download, int source);
~~~
 Called to open a given download item.

### OpenDownload

DownloadManagerService::OpenDownload
~~~cpp
void OpenDownload(JNIEnv* env,
                    jobject obj,
                    const JavaParamRef<jstring>& jdownload_guid,
                    const JavaParamRef<jobject>& j_profile_key,
                    jint source);
~~~
 Called to open a download item whose GUID is equal to |jdownload_guid|.

### ResumeDownload

DownloadManagerService::ResumeDownload
~~~cpp
void ResumeDownload(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jstring>& jdownload_guid,
                      const JavaParamRef<jobject>& j_profile_key,
                      bool has_user_gesture);
~~~
 Called to resume downloading the item that has GUID equal to
 |jdownload_guid|..

### RetryDownload

DownloadManagerService::RetryDownload
~~~cpp
void RetryDownload(JNIEnv* env,
                     jobject obj,
                     const JavaParamRef<jstring>& jdownload_guid,
                     const JavaParamRef<jobject>& j_profile_key,
                     bool has_user_gesture);
~~~
 Called to retry a download.

### CancelDownload

DownloadManagerService::CancelDownload
~~~cpp
void CancelDownload(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jstring>& jdownload_guid,
                      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to cancel a download item that has GUID equal to |jdownload_guid|.

 If the DownloadItem is not yet created, retry after a while.

### PauseDownload

DownloadManagerService::PauseDownload
~~~cpp
void PauseDownload(JNIEnv* env,
                     jobject obj,
                     const JavaParamRef<jstring>& jdownload_guid,
                     const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to pause a download item that has GUID equal to |jdownload_guid|.

 If the DownloadItem is not yet created, do nothing as it is already paused.

### RemoveDownload

DownloadManagerService::RemoveDownload
~~~cpp
void RemoveDownload(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jstring>& jdownload_guid,
                      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to remove a download item that has GUID equal to |jdownload_guid|.

### RenameDownload

DownloadManagerService::RenameDownload
~~~cpp
void RenameDownload(JNIEnv* env,
                      const JavaParamRef<jobject>& obj,
                      const JavaParamRef<jstring>& id,
                      const JavaParamRef<jstring>& name,
                      const JavaParamRef<jobject>& callback,
                      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to rename a download item that has GUID equal to |id|.

### IsDownloadOpenableInBrowser

DownloadManagerService::IsDownloadOpenableInBrowser
~~~cpp
bool IsDownloadOpenableInBrowser(JNIEnv* env,
                                   jobject obj,
                                   const JavaParamRef<jstring>& jdownload_guid,
                                   const JavaParamRef<jobject>& j_profile_key);
~~~
 Returns whether or not the given download can be opened by the browser.

### GetAllDownloads

DownloadManagerService::GetAllDownloads
~~~cpp
void GetAllDownloads(JNIEnv* env,
                       const JavaParamRef<jobject>& obj,
                       const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to request that the DownloadManagerService return data about all
 downloads in the user's history.

### CheckForExternallyRemovedDownloads

DownloadManagerService::CheckForExternallyRemovedDownloads
~~~cpp
void CheckForExternallyRemovedDownloads(
      JNIEnv* env,
      const JavaParamRef<jobject>& obj,
      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to check if the files associated with any downloads have been
 removed by an external action.

### UpdateLastAccessTime

DownloadManagerService::UpdateLastAccessTime
~~~cpp
void UpdateLastAccessTime(JNIEnv* env,
                            const JavaParamRef<jobject>& obj,
                            const JavaParamRef<jstring>& jdownload_guid,
                            const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to update the last access time associated with a download.

### OnDownloadsInitialized

DownloadManagerService::OnDownloadsInitialized
~~~cpp
void OnDownloadsInitialized(
      download::SimpleDownloadManagerCoordinator* coordinator,
      bool active_downloads_only) override;
~~~
 AllDownloadEventNotifier::Observer methods.

### OnManagerGoingDown

DownloadManagerService::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(
      download::SimpleDownloadManagerCoordinator* coordinator) override;
~~~

### OnDownloadCreated

DownloadManagerService::OnDownloadCreated
~~~cpp
void OnDownloadCreated(
      download::SimpleDownloadManagerCoordinator* coordinator,
      download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadManagerService::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(
      download::SimpleDownloadManagerCoordinator* coordinator,
      download::DownloadItem* item) override;
~~~

### OnDownloadRemoved

DownloadManagerService::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(
      download::SimpleDownloadManagerCoordinator* coordinator,
      download::DownloadItem* item) override;
~~~

### OnOffTheRecordProfileCreated

DownloadManagerService::OnOffTheRecordProfileCreated
~~~cpp
void OnOffTheRecordProfileCreated(Profile* off_the_record) override;
~~~
 ProfileObserver:
### CreateInterruptedDownloadForTest

DownloadManagerService::CreateInterruptedDownloadForTest
~~~cpp
void CreateInterruptedDownloadForTest(
      JNIEnv* env,
      jobject obj,
      const JavaParamRef<jstring>& jurl,
      const JavaParamRef<jstring>& jdownload_guid,
      const JavaParamRef<jstring>& jtarget_path);
~~~
 Called by the java code to create and insert an interrupted download to
 |in_progress_manager_| for testing purpose.

### RetrieveInProgressDownloadManager

DownloadManagerService::RetrieveInProgressDownloadManager
~~~cpp
std::unique_ptr<download::InProgressDownloadManager>
  RetrieveInProgressDownloadManager(content::BrowserContext* context);
~~~
 Retrives the in-progress manager and give up the ownership.

### GetDownload

DownloadManagerService::GetDownload
~~~cpp
download::DownloadItem* GetDownload(const std::string& download_guid,
                                      ProfileKey* profile_key);
~~~
 Gets a download item from DownloadManager or InProgressManager.

### RecordFirstBackgroundInterruptReason

DownloadManagerService::RecordFirstBackgroundInterruptReason
~~~cpp
void RecordFirstBackgroundInterruptReason(
      JNIEnv* env,
      const JavaParamRef<jobject>& obj,
      const JavaParamRef<jstring>& jdownload_guid,
      jboolean download_started);
~~~
 Helper method to record the interrupt reason UMA for the first background
 download.

### OpenDownloadsPage

DownloadManagerService::OpenDownloadsPage
~~~cpp
void OpenDownloadsPage(Profile* profile,
                         DownloadOpenSource download_open_source);
~~~
 Open the download page the given profile, and the source of the opening
 action is |download_open_source|.

### enum DownloadAction

~~~cpp
enum DownloadAction { RESUME, RETRY, PAUSE, CANCEL, REMOVE, UNKNOWN }
~~~
###  DownloadActionParams

 Holds params provided to the download function calls.

~~~cpp
struct DownloadActionParams {
    explicit DownloadActionParams(DownloadAction download_action);
    DownloadActionParams(DownloadAction download_action, bool user_gesture);
    DownloadActionParams(const DownloadActionParams& other);

    ~DownloadActionParams() = default;

    DownloadAction action;
    bool has_user_gesture;
  };
~~~
### ResumeDownloadInternal

DownloadManagerService::ResumeDownloadInternal
~~~cpp
void ResumeDownloadInternal(const std::string& download_guid,
                              ProfileKey* profile_key,
                              bool has_user_gesture);
~~~
 Helper function to start the download resumption.

### RetryDownloadInternal

DownloadManagerService::RetryDownloadInternal
~~~cpp
void RetryDownloadInternal(const std::string& download_guid,
                             ProfileKey* profile_key,
                             bool has_user_gesture);
~~~
 Helper function to retry the download.

### CancelDownloadInternal

DownloadManagerService::CancelDownloadInternal
~~~cpp
void CancelDownloadInternal(const std::string& download_guid,
                              ProfileKey* profile_key);
~~~
 Helper function to cancel a download.

### PauseDownloadInternal

DownloadManagerService::PauseDownloadInternal
~~~cpp
void PauseDownloadInternal(const std::string& download_guid,
                             ProfileKey* profile_key);
~~~
 Helper function to pause a download.

### RemoveDownloadInternal

DownloadManagerService::RemoveDownloadInternal
~~~cpp
void RemoveDownloadInternal(const std::string& download_guid,
                              ProfileKey* profile_key);
~~~
 Helper function to remove a download.

### GetAllDownloadsInternal

DownloadManagerService::GetAllDownloadsInternal
~~~cpp
void GetAllDownloadsInternal(ProfileKey* profile_key);
~~~
 Helper function to send info about all downloads to the Java-side.

### OnResumptionFailed

DownloadManagerService::OnResumptionFailed
~~~cpp
void OnResumptionFailed(const std::string& download_guid);
~~~
 Called to notify the java side that download resumption failed.

### OnResumptionFailedInternal

DownloadManagerService::OnResumptionFailedInternal
~~~cpp
void OnResumptionFailedInternal(const std::string& download_guid);
~~~

### OnPendingDownloadsLoaded

DownloadManagerService::OnPendingDownloadsLoaded
~~~cpp
void OnPendingDownloadsLoaded();
~~~
 Called when all pending downloads are loaded.

### set_resume_callback_for_testing

set_resume_callback_for_testing
~~~cpp
void set_resume_callback_for_testing(ResumeCallback resume_cb) {
    resume_callback_for_testing_ = std::move(resume_cb);
  }
~~~

### ResetCoordinatorIfNeeded

DownloadManagerService::ResetCoordinatorIfNeeded
~~~cpp
void ResetCoordinatorIfNeeded(ProfileKey* profile_key);
~~~
 Helper method to reset the SimpleDownloadManagerCoordinator if needed.

### UpdateCoordinator

DownloadManagerService::UpdateCoordinator
~~~cpp
void UpdateCoordinator(
      download::SimpleDownloadManagerCoordinator* coordinator,
      ProfileKey* profile_key);
~~~
 Helper method to reset the SimpleDownloadManagerCoordinator for a given
 profile type.

### GetDownloadManager

DownloadManagerService::GetDownloadManager
~~~cpp
content::DownloadManager* GetDownloadManager(ProfileKey* profile_key);
~~~
 Called to get the content::DownloadManager instance.

### GetCoordinator

DownloadManagerService::GetCoordinator
~~~cpp
download::SimpleDownloadManagerCoordinator* GetCoordinator(
      ProfileKey* profile_key);
~~~
 Retrieves the SimpleDownloadManagerCoordinator this object is listening to.

### InitializeForProfile

DownloadManagerService::InitializeForProfile
~~~cpp
void InitializeForProfile(ProfileKey* profile_key);
~~~

### java_ref_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_ref_;

~~~

 Reference to the Java object.

### is_manager_initialized_



~~~cpp

bool is_manager_initialized_;

~~~


### is_pending_downloads_loaded_



~~~cpp

bool is_pending_downloads_loaded_;

~~~


### profiles_with_pending_get_downloads_actions_



~~~cpp

std::vector<ProfileKey*> profiles_with_pending_get_downloads_actions_;

~~~


### pending_actions_



~~~cpp

PendingDownloadActions pending_actions_;

~~~


### EnqueueDownloadAction

DownloadManagerService::EnqueueDownloadAction
~~~cpp
void EnqueueDownloadAction(const std::string& download_guid,
                             const DownloadActionParams& params);
~~~

### resume_callback_for_testing_



~~~cpp

ResumeCallback resume_callback_for_testing_;

~~~


### observed_profiles_



~~~cpp

base::ScopedMultiSourceObservation<Profile, ProfileObserver>
      observed_profiles_{this};

~~~


### coordinators_



~~~cpp

Coordinators coordinators_;

~~~


### DownloadManagerService

DownloadManagerService
~~~cpp
DownloadManagerService(const DownloadManagerService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadManagerService& operator=(const DownloadManagerService&) = delete;
~~~

### set_resume_callback_for_testing

set_resume_callback_for_testing
~~~cpp
void set_resume_callback_for_testing(ResumeCallback resume_cb) {
    resume_callback_for_testing_ = std::move(resume_cb);
  }
~~~

### CreateAutoResumptionHandler

DownloadManagerService::CreateAutoResumptionHandler
~~~cpp
static void CreateAutoResumptionHandler();
~~~

### OnDownloadCanceled

DownloadManagerService::OnDownloadCanceled
~~~cpp
static void OnDownloadCanceled(download::DownloadItem* download,
                                 bool has_no_external_storage);
~~~

### GetInstance

DownloadManagerService::GetInstance
~~~cpp
static DownloadManagerService* GetInstance();
~~~

### CreateJavaDownloadInfo

DownloadManagerService::CreateJavaDownloadInfo
~~~cpp
static base::android::ScopedJavaLocalRef<jobject> CreateJavaDownloadInfo(
      JNIEnv* env,
      download::DownloadItem* item);
~~~

### Init

DownloadManagerService::Init
~~~cpp
void Init(JNIEnv* env, jobject obj, bool is_profile_added);
~~~
 Called to Initialize this object. If |is_profile_added| is false,
 it means only a minimal browser is launched. OnProfileAdded() will
 be called later when the profile is added.

### OnProfileAdded

DownloadManagerService::OnProfileAdded
~~~cpp
void OnProfileAdded(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jobject>& j_profile);
~~~
 Called when the profile is added to the ProfileManager and fully
 initialized.

### OnProfileAdded

DownloadManagerService::OnProfileAdded
~~~cpp
void OnProfileAdded(Profile* profile);
~~~

### HandleOMADownload

DownloadManagerService::HandleOMADownload
~~~cpp
void HandleOMADownload(download::DownloadItem* download,
                         int64_t system_download_id);
~~~
 Called to handle subsequent steps, after a download was determined as a OMA
 download type.

### OpenDownload

DownloadManagerService::OpenDownload
~~~cpp
void OpenDownload(download::DownloadItem* download, int source);
~~~
 Called to open a given download item.

### OpenDownload

DownloadManagerService::OpenDownload
~~~cpp
void OpenDownload(JNIEnv* env,
                    jobject obj,
                    const JavaParamRef<jstring>& jdownload_guid,
                    const JavaParamRef<jobject>& j_profile_key,
                    jint source);
~~~
 Called to open a download item whose GUID is equal to |jdownload_guid|.

### ResumeDownload

DownloadManagerService::ResumeDownload
~~~cpp
void ResumeDownload(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jstring>& jdownload_guid,
                      const JavaParamRef<jobject>& j_profile_key,
                      bool has_user_gesture);
~~~
 Called to resume downloading the item that has GUID equal to
 |jdownload_guid|..

### RetryDownload

DownloadManagerService::RetryDownload
~~~cpp
void RetryDownload(JNIEnv* env,
                     jobject obj,
                     const JavaParamRef<jstring>& jdownload_guid,
                     const JavaParamRef<jobject>& j_profile_key,
                     bool has_user_gesture);
~~~
 Called to retry a download.

### CancelDownload

DownloadManagerService::CancelDownload
~~~cpp
void CancelDownload(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jstring>& jdownload_guid,
                      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to cancel a download item that has GUID equal to |jdownload_guid|.

 If the DownloadItem is not yet created, retry after a while.

### PauseDownload

DownloadManagerService::PauseDownload
~~~cpp
void PauseDownload(JNIEnv* env,
                     jobject obj,
                     const JavaParamRef<jstring>& jdownload_guid,
                     const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to pause a download item that has GUID equal to |jdownload_guid|.

 If the DownloadItem is not yet created, do nothing as it is already paused.

### RemoveDownload

DownloadManagerService::RemoveDownload
~~~cpp
void RemoveDownload(JNIEnv* env,
                      jobject obj,
                      const JavaParamRef<jstring>& jdownload_guid,
                      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to remove a download item that has GUID equal to |jdownload_guid|.

### RenameDownload

DownloadManagerService::RenameDownload
~~~cpp
void RenameDownload(JNIEnv* env,
                      const JavaParamRef<jobject>& obj,
                      const JavaParamRef<jstring>& id,
                      const JavaParamRef<jstring>& name,
                      const JavaParamRef<jobject>& callback,
                      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to rename a download item that has GUID equal to |id|.

### IsDownloadOpenableInBrowser

DownloadManagerService::IsDownloadOpenableInBrowser
~~~cpp
bool IsDownloadOpenableInBrowser(JNIEnv* env,
                                   jobject obj,
                                   const JavaParamRef<jstring>& jdownload_guid,
                                   const JavaParamRef<jobject>& j_profile_key);
~~~
 Returns whether or not the given download can be opened by the browser.

### GetAllDownloads

DownloadManagerService::GetAllDownloads
~~~cpp
void GetAllDownloads(JNIEnv* env,
                       const JavaParamRef<jobject>& obj,
                       const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to request that the DownloadManagerService return data about all
 downloads in the user's history.

### CheckForExternallyRemovedDownloads

DownloadManagerService::CheckForExternallyRemovedDownloads
~~~cpp
void CheckForExternallyRemovedDownloads(
      JNIEnv* env,
      const JavaParamRef<jobject>& obj,
      const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to check if the files associated with any downloads have been
 removed by an external action.

### UpdateLastAccessTime

DownloadManagerService::UpdateLastAccessTime
~~~cpp
void UpdateLastAccessTime(JNIEnv* env,
                            const JavaParamRef<jobject>& obj,
                            const JavaParamRef<jstring>& jdownload_guid,
                            const JavaParamRef<jobject>& j_profile_key);
~~~
 Called to update the last access time associated with a download.

### OnDownloadsInitialized

DownloadManagerService::OnDownloadsInitialized
~~~cpp
void OnDownloadsInitialized(
      download::SimpleDownloadManagerCoordinator* coordinator,
      bool active_downloads_only) override;
~~~
 AllDownloadEventNotifier::Observer methods.

### OnManagerGoingDown

DownloadManagerService::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(
      download::SimpleDownloadManagerCoordinator* coordinator) override;
~~~

### OnDownloadCreated

DownloadManagerService::OnDownloadCreated
~~~cpp
void OnDownloadCreated(
      download::SimpleDownloadManagerCoordinator* coordinator,
      download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadManagerService::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(
      download::SimpleDownloadManagerCoordinator* coordinator,
      download::DownloadItem* item) override;
~~~

### OnDownloadRemoved

DownloadManagerService::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(
      download::SimpleDownloadManagerCoordinator* coordinator,
      download::DownloadItem* item) override;
~~~

### OnOffTheRecordProfileCreated

DownloadManagerService::OnOffTheRecordProfileCreated
~~~cpp
void OnOffTheRecordProfileCreated(Profile* off_the_record) override;
~~~
 ProfileObserver:
### CreateInterruptedDownloadForTest

DownloadManagerService::CreateInterruptedDownloadForTest
~~~cpp
void CreateInterruptedDownloadForTest(
      JNIEnv* env,
      jobject obj,
      const JavaParamRef<jstring>& jurl,
      const JavaParamRef<jstring>& jdownload_guid,
      const JavaParamRef<jstring>& jtarget_path);
~~~
 Called by the java code to create and insert an interrupted download to
 |in_progress_manager_| for testing purpose.

### RetrieveInProgressDownloadManager

DownloadManagerService::RetrieveInProgressDownloadManager
~~~cpp
std::unique_ptr<download::InProgressDownloadManager>
  RetrieveInProgressDownloadManager(content::BrowserContext* context);
~~~
 Retrives the in-progress manager and give up the ownership.

### GetDownload

DownloadManagerService::GetDownload
~~~cpp
download::DownloadItem* GetDownload(const std::string& download_guid,
                                      ProfileKey* profile_key);
~~~
 Gets a download item from DownloadManager or InProgressManager.

### RecordFirstBackgroundInterruptReason

DownloadManagerService::RecordFirstBackgroundInterruptReason
~~~cpp
void RecordFirstBackgroundInterruptReason(
      JNIEnv* env,
      const JavaParamRef<jobject>& obj,
      const JavaParamRef<jstring>& jdownload_guid,
      jboolean download_started);
~~~
 Helper method to record the interrupt reason UMA for the first background
 download.

### OpenDownloadsPage

DownloadManagerService::OpenDownloadsPage
~~~cpp
void OpenDownloadsPage(Profile* profile,
                         DownloadOpenSource download_open_source);
~~~
 Open the download page the given profile, and the source of the opening
 action is |download_open_source|.

### enum DownloadAction

~~~cpp
enum DownloadAction { RESUME, RETRY, PAUSE, CANCEL, REMOVE, UNKNOWN };
~~~
###  DownloadActionParams

 Holds params provided to the download function calls.

~~~cpp
struct DownloadActionParams {
    explicit DownloadActionParams(DownloadAction download_action);
    DownloadActionParams(DownloadAction download_action, bool user_gesture);
    DownloadActionParams(const DownloadActionParams& other);

    ~DownloadActionParams() = default;

    DownloadAction action;
    bool has_user_gesture;
  };
~~~
### ResumeDownloadInternal

DownloadManagerService::ResumeDownloadInternal
~~~cpp
void ResumeDownloadInternal(const std::string& download_guid,
                              ProfileKey* profile_key,
                              bool has_user_gesture);
~~~
 Helper function to start the download resumption.

### RetryDownloadInternal

DownloadManagerService::RetryDownloadInternal
~~~cpp
void RetryDownloadInternal(const std::string& download_guid,
                             ProfileKey* profile_key,
                             bool has_user_gesture);
~~~
 Helper function to retry the download.

### CancelDownloadInternal

DownloadManagerService::CancelDownloadInternal
~~~cpp
void CancelDownloadInternal(const std::string& download_guid,
                              ProfileKey* profile_key);
~~~
 Helper function to cancel a download.

### PauseDownloadInternal

DownloadManagerService::PauseDownloadInternal
~~~cpp
void PauseDownloadInternal(const std::string& download_guid,
                             ProfileKey* profile_key);
~~~
 Helper function to pause a download.

### RemoveDownloadInternal

DownloadManagerService::RemoveDownloadInternal
~~~cpp
void RemoveDownloadInternal(const std::string& download_guid,
                              ProfileKey* profile_key);
~~~
 Helper function to remove a download.

### GetAllDownloadsInternal

DownloadManagerService::GetAllDownloadsInternal
~~~cpp
void GetAllDownloadsInternal(ProfileKey* profile_key);
~~~
 Helper function to send info about all downloads to the Java-side.

### OnResumptionFailed

DownloadManagerService::OnResumptionFailed
~~~cpp
void OnResumptionFailed(const std::string& download_guid);
~~~
 Called to notify the java side that download resumption failed.

### OnResumptionFailedInternal

DownloadManagerService::OnResumptionFailedInternal
~~~cpp
void OnResumptionFailedInternal(const std::string& download_guid);
~~~

### OnPendingDownloadsLoaded

DownloadManagerService::OnPendingDownloadsLoaded
~~~cpp
void OnPendingDownloadsLoaded();
~~~
 Called when all pending downloads are loaded.

### ResetCoordinatorIfNeeded

DownloadManagerService::ResetCoordinatorIfNeeded
~~~cpp
void ResetCoordinatorIfNeeded(ProfileKey* profile_key);
~~~
 Helper method to reset the SimpleDownloadManagerCoordinator if needed.

### UpdateCoordinator

DownloadManagerService::UpdateCoordinator
~~~cpp
void UpdateCoordinator(
      download::SimpleDownloadManagerCoordinator* coordinator,
      ProfileKey* profile_key);
~~~
 Helper method to reset the SimpleDownloadManagerCoordinator for a given
 profile type.

### GetDownloadManager

DownloadManagerService::GetDownloadManager
~~~cpp
content::DownloadManager* GetDownloadManager(ProfileKey* profile_key);
~~~
 Called to get the content::DownloadManager instance.

### GetCoordinator

DownloadManagerService::GetCoordinator
~~~cpp
download::SimpleDownloadManagerCoordinator* GetCoordinator(
      ProfileKey* profile_key);
~~~
 Retrieves the SimpleDownloadManagerCoordinator this object is listening to.

### InitializeForProfile

DownloadManagerService::InitializeForProfile
~~~cpp
void InitializeForProfile(ProfileKey* profile_key);
~~~

### java_ref_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_ref_;

~~~

 Reference to the Java object.

### is_manager_initialized_



~~~cpp

bool is_manager_initialized_;

~~~


### is_pending_downloads_loaded_



~~~cpp

bool is_pending_downloads_loaded_;

~~~


### profiles_with_pending_get_downloads_actions_



~~~cpp

std::vector<ProfileKey*> profiles_with_pending_get_downloads_actions_;

~~~


### pending_actions_



~~~cpp

PendingDownloadActions pending_actions_;

~~~


### EnqueueDownloadAction

DownloadManagerService::EnqueueDownloadAction
~~~cpp
void EnqueueDownloadAction(const std::string& download_guid,
                             const DownloadActionParams& params);
~~~

### resume_callback_for_testing_



~~~cpp

ResumeCallback resume_callback_for_testing_;

~~~


### observed_profiles_



~~~cpp

base::ScopedMultiSourceObservation<Profile, ProfileObserver>
      observed_profiles_{this};

~~~


### coordinators_



~~~cpp

Coordinators coordinators_;

~~~

