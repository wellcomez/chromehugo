
## class DownloadDialogUtils
 Helper class for download dialogs.

### FindAndRemoveDownload

DownloadDialogUtils::FindAndRemoveDownload
~~~cpp
static download::DownloadItem* FindAndRemoveDownload(
      std::vector<download::DownloadItem*>* downloads,
      const std::string& download_guid);
~~~
 Helper method to find a download from a list of downloads based on its
 GUID, and remove it from the list.

### CreateNewFileDone

DownloadDialogUtils::CreateNewFileDone
~~~cpp
static void CreateNewFileDone(
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      download::PathValidationResult result,
      const base::FilePath& target_path);
~~~
 Called when a new file was created and inform |callback| about
 the result and the new path.

### GetDisplayURLForPageURL

DownloadDialogUtils::GetDisplayURLForPageURL
~~~cpp
static std::string GetDisplayURLForPageURL(const GURL& page_url);
~~~
 Called to get an elided URL for a page URL, so that it can be displayed
 on duplicate inforbar or dialog.

### FindAndRemoveDownload

DownloadDialogUtils::FindAndRemoveDownload
~~~cpp
static download::DownloadItem* FindAndRemoveDownload(
      std::vector<download::DownloadItem*>* downloads,
      const std::string& download_guid);
~~~
 Helper method to find a download from a list of downloads based on its
 GUID, and remove it from the list.

### CreateNewFileDone

DownloadDialogUtils::CreateNewFileDone
~~~cpp
static void CreateNewFileDone(
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      download::PathValidationResult result,
      const base::FilePath& target_path);
~~~
 Called when a new file was created and inform |callback| about
 the result and the new path.

### GetDisplayURLForPageURL

DownloadDialogUtils::GetDisplayURLForPageURL
~~~cpp
static std::string GetDisplayURLForPageURL(const GURL& page_url);
~~~
 Called to get an elided URL for a page URL, so that it can be displayed
 on duplicate inforbar or dialog.
