
## class DownloadManagerBridge
 This class pairs with DownloadManagerBridge on Java side, that handles all
 the android DownloadManager related functionalities. Both classes have only
 static functions.

### DownloadManagerBridge

DownloadManagerBridge
~~~cpp
DownloadManagerBridge(const DownloadManagerBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadManagerBridge& operator=(const DownloadManagerBridge&) = delete;
~~~

### AddCompletedDownload

DownloadManagerBridge::AddCompletedDownload
~~~cpp
static void AddCompletedDownload(DownloadItem* download,
                                   AddCompletedDownloadCallback callback);
~~~

### RemoveCompletedDownload

DownloadManagerBridge::RemoveCompletedDownload
~~~cpp
static void RemoveCompletedDownload(DownloadItem* download);
~~~

### DownloadManagerBridge

DownloadManagerBridge
~~~cpp
DownloadManagerBridge(const DownloadManagerBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadManagerBridge& operator=(const DownloadManagerBridge&) = delete;
~~~

### AddCompletedDownload

DownloadManagerBridge::AddCompletedDownload
~~~cpp
static void AddCompletedDownload(DownloadItem* download,
                                   AddCompletedDownloadCallback callback);
~~~

### RemoveCompletedDownload

DownloadManagerBridge::RemoveCompletedDownload
~~~cpp
static void RemoveCompletedDownload(DownloadItem* download);
~~~
