
## class DuplicateDownloadDialogBridgeDelegate
 namespace content
 Class for showing dialogs to asks whether user wants to download a file
 that already exists on disk
### GetInstance

DuplicateDownloadDialogBridgeDelegate::GetInstance
~~~cpp
static DuplicateDownloadDialogBridgeDelegate* GetInstance();
~~~
 static
### DuplicateDownloadDialogBridgeDelegate

DuplicateDownloadDialogBridgeDelegate::DuplicateDownloadDialogBridgeDelegate
~~~cpp
DuplicateDownloadDialogBridgeDelegate();
~~~

### DuplicateDownloadDialogBridgeDelegate

DuplicateDownloadDialogBridgeDelegate
~~~cpp
DuplicateDownloadDialogBridgeDelegate(
      const DuplicateDownloadDialogBridgeDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
DuplicateDownloadDialogBridgeDelegate& operator=(
      const DuplicateDownloadDialogBridgeDelegate&) = delete;
~~~

### ~DuplicateDownloadDialogBridgeDelegate

DuplicateDownloadDialogBridgeDelegate::~DuplicateDownloadDialogBridgeDelegate
~~~cpp
~DuplicateDownloadDialogBridgeDelegate() override;
~~~

### CreateDialog

DuplicateDownloadDialogBridgeDelegate::CreateDialog
~~~cpp
void CreateDialog(download::DownloadItem* download_item,
                    const base::FilePath& file_path,
                    content::WebContents* web_contents,
                    DownloadTargetDeterminerDelegate::ConfirmationCallback
                        file_selected_callback);
~~~
 Called to create and show a dialog for a duplicate download.

### OnConfirmed

DuplicateDownloadDialogBridgeDelegate::OnConfirmed
~~~cpp
void OnConfirmed(
      const std::string& download_guid,
      const base::FilePath& file_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      bool accepted);
~~~
 Called from Java via JNI.

### OnDownloadDestroyed

DuplicateDownloadDialogBridgeDelegate::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer:
### download_items_



~~~cpp

std::vector<download::DownloadItem*> download_items_;

~~~

 Download items that are requesting the dialog. Could get deleted while
 the dialog is showing.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DuplicateDownloadDialogBridgeDelegate> weak_factory_{
      this};

~~~


### DuplicateDownloadDialogBridgeDelegate

DuplicateDownloadDialogBridgeDelegate
~~~cpp
DuplicateDownloadDialogBridgeDelegate(
      const DuplicateDownloadDialogBridgeDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
DuplicateDownloadDialogBridgeDelegate& operator=(
      const DuplicateDownloadDialogBridgeDelegate&) = delete;
~~~

### GetInstance

DuplicateDownloadDialogBridgeDelegate::GetInstance
~~~cpp
static DuplicateDownloadDialogBridgeDelegate* GetInstance();
~~~
 static
### CreateDialog

DuplicateDownloadDialogBridgeDelegate::CreateDialog
~~~cpp
void CreateDialog(download::DownloadItem* download_item,
                    const base::FilePath& file_path,
                    content::WebContents* web_contents,
                    DownloadTargetDeterminerDelegate::ConfirmationCallback
                        file_selected_callback);
~~~
 Called to create and show a dialog for a duplicate download.

### OnConfirmed

DuplicateDownloadDialogBridgeDelegate::OnConfirmed
~~~cpp
void OnConfirmed(
      const std::string& download_guid,
      const base::FilePath& file_path,
      DownloadTargetDeterminerDelegate::ConfirmationCallback callback,
      bool accepted);
~~~
 Called from Java via JNI.

### OnDownloadDestroyed

DuplicateDownloadDialogBridgeDelegate::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer:
### download_items_



~~~cpp

std::vector<download::DownloadItem*> download_items_;

~~~

 Download items that are requesting the dialog. Could get deleted while
 the dialog is showing.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DuplicateDownloadDialogBridgeDelegate> weak_factory_{
      this};

~~~

