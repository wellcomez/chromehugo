
## class DownloadUtils
 Native side of DownloadUtils.java.

### GetUriStringForPath

DownloadUtils::GetUriStringForPath
~~~cpp
static base::FilePath GetUriStringForPath(const base::FilePath& file_path);
~~~

### GetAutoResumptionSizeLimit

DownloadUtils::GetAutoResumptionSizeLimit
~~~cpp
static int GetAutoResumptionSizeLimit();
~~~

### OpenDownload

DownloadUtils::OpenDownload
~~~cpp
static void OpenDownload(download::DownloadItem* item,
                           DownloadOpenSource open_source);
~~~

### RemapGenericMimeType

DownloadUtils::RemapGenericMimeType
~~~cpp
static std::string RemapGenericMimeType(const std::string& mime_type,
                                          const GURL& url,
                                          const std::string& file_name);
~~~

### ShouldAutoOpenDownload

DownloadUtils::ShouldAutoOpenDownload
~~~cpp
static bool ShouldAutoOpenDownload(download::DownloadItem* item);
~~~

### IsOmaDownloadDescription

DownloadUtils::IsOmaDownloadDescription
~~~cpp
static bool IsOmaDownloadDescription(const std::string& mime_type);
~~~

### ShowDownloadManager

DownloadUtils::ShowDownloadManager
~~~cpp
static void ShowDownloadManager(bool show_prefetched_content,
                                  DownloadOpenSource open_source);
~~~
 Called to show the download manager, with a choice to focus on prefetched
 content instead of regular downloads. |download_open_source| is the source
 of the action.

### IsDownloadUserInitiated

DownloadUtils::IsDownloadUserInitiated
~~~cpp
static bool IsDownloadUserInitiated(download::DownloadItem* download);
~~~

### GetUriStringForPath

DownloadUtils::GetUriStringForPath
~~~cpp
static base::FilePath GetUriStringForPath(const base::FilePath& file_path);
~~~

### GetAutoResumptionSizeLimit

DownloadUtils::GetAutoResumptionSizeLimit
~~~cpp
static int GetAutoResumptionSizeLimit();
~~~

### OpenDownload

DownloadUtils::OpenDownload
~~~cpp
static void OpenDownload(download::DownloadItem* item,
                           DownloadOpenSource open_source);
~~~

### RemapGenericMimeType

DownloadUtils::RemapGenericMimeType
~~~cpp
static std::string RemapGenericMimeType(const std::string& mime_type,
                                          const GURL& url,
                                          const std::string& file_name);
~~~

### ShouldAutoOpenDownload

DownloadUtils::ShouldAutoOpenDownload
~~~cpp
static bool ShouldAutoOpenDownload(download::DownloadItem* item);
~~~

### IsOmaDownloadDescription

DownloadUtils::IsOmaDownloadDescription
~~~cpp
static bool IsOmaDownloadDescription(const std::string& mime_type);
~~~

### ShowDownloadManager

DownloadUtils::ShowDownloadManager
~~~cpp
static void ShowDownloadManager(bool show_prefetched_content,
                                  DownloadOpenSource open_source);
~~~
 Called to show the download manager, with a choice to focus on prefetched
 content instead of regular downloads. |download_open_source| is the source
 of the action.

### IsDownloadUserInitiated

DownloadUtils::IsDownloadUserInitiated
~~~cpp
static bool IsDownloadUserInitiated(download::DownloadItem* download);
~~~
