
## class DownloadCallbackValidator
 Helper class used to validate callbacks that passed to Java
 side are used correctly.

### DownloadCallbackValidator

DownloadCallbackValidator::DownloadCallbackValidator
~~~cpp
DownloadCallbackValidator();
~~~

### ~DownloadCallbackValidator

DownloadCallbackValidator::~DownloadCallbackValidator
~~~cpp
~DownloadCallbackValidator();
~~~

### DownloadCallbackValidator

DownloadCallbackValidator
~~~cpp
DownloadCallbackValidator(const DownloadCallbackValidator&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCallbackValidator& operator=(const DownloadCallbackValidator&) =
      delete;
~~~

### AddJavaCallback

DownloadCallbackValidator::AddJavaCallback
~~~cpp
void AddJavaCallback(intptr_t callback_id);
~~~
 Adds a java callback id that will be called later.

### ValidateAndClearJavaCallback

DownloadCallbackValidator::ValidateAndClearJavaCallback
~~~cpp
bool ValidateAndClearJavaCallback(intptr_t callback_id);
~~~
 Validate the java callback id is valid, and remove it from
 |callback_ids|.

### callback_ids_



~~~cpp

std::set<intptr_t> callback_ids_;

~~~

 Callback IDs, used for validation purpose.

### DownloadCallbackValidator

DownloadCallbackValidator
~~~cpp
DownloadCallbackValidator(const DownloadCallbackValidator&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadCallbackValidator& operator=(const DownloadCallbackValidator&) =
      delete;
~~~

### AddJavaCallback

DownloadCallbackValidator::AddJavaCallback
~~~cpp
void AddJavaCallback(intptr_t callback_id);
~~~
 Adds a java callback id that will be called later.

### ValidateAndClearJavaCallback

DownloadCallbackValidator::ValidateAndClearJavaCallback
~~~cpp
bool ValidateAndClearJavaCallback(intptr_t callback_id);
~~~
 Validate the java callback id is valid, and remove it from
 |callback_ids|.

### callback_ids_



~~~cpp

std::set<intptr_t> callback_ids_;

~~~

 Callback IDs, used for validation purpose.
