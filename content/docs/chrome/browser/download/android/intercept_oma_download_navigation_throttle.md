
## class InterceptOMADownloadNavigationThrottle
 Used to intercept the OMA DRM download from navigation and pass it to Android
 DownloadManager.

### Create

InterceptOMADownloadNavigationThrottle::Create
~~~cpp
static std::unique_ptr<content::NavigationThrottle> Create(
      content::NavigationHandle* handle);
~~~

### InterceptOMADownloadNavigationThrottle

InterceptOMADownloadNavigationThrottle
~~~cpp
InterceptOMADownloadNavigationThrottle(
      const InterceptOMADownloadNavigationThrottle&) = delete;
~~~

### operator=

operator=
~~~cpp
InterceptOMADownloadNavigationThrottle& operator=(
      const InterceptOMADownloadNavigationThrottle&) = delete;
~~~

### ~InterceptOMADownloadNavigationThrottle

InterceptOMADownloadNavigationThrottle::~InterceptOMADownloadNavigationThrottle
~~~cpp
~InterceptOMADownloadNavigationThrottle() override;
~~~

### WillProcessResponse

InterceptOMADownloadNavigationThrottle::WillProcessResponse
~~~cpp
content::NavigationThrottle::ThrottleCheckResult WillProcessResponse()
      override;
~~~
 content::NavigationThrottle:
### GetNameForLogging

InterceptOMADownloadNavigationThrottle::GetNameForLogging
~~~cpp
const char* GetNameForLogging() override;
~~~

### InterceptOMADownloadNavigationThrottle

InterceptOMADownloadNavigationThrottle::InterceptOMADownloadNavigationThrottle
~~~cpp
explicit InterceptOMADownloadNavigationThrottle(
      content::NavigationHandle* handle);
~~~

### InterceptDownload

InterceptOMADownloadNavigationThrottle::InterceptDownload
~~~cpp
void InterceptDownload();
~~~
 Helper method to intercept the download.

### InterceptOMADownloadNavigationThrottle

InterceptOMADownloadNavigationThrottle
~~~cpp
InterceptOMADownloadNavigationThrottle(
      const InterceptOMADownloadNavigationThrottle&) = delete;
~~~

### operator=

operator=
~~~cpp
InterceptOMADownloadNavigationThrottle& operator=(
      const InterceptOMADownloadNavigationThrottle&) = delete;
~~~

### Create

InterceptOMADownloadNavigationThrottle::Create
~~~cpp
static std::unique_ptr<content::NavigationThrottle> Create(
      content::NavigationHandle* handle);
~~~

### WillProcessResponse

InterceptOMADownloadNavigationThrottle::WillProcessResponse
~~~cpp
content::NavigationThrottle::ThrottleCheckResult WillProcessResponse()
      override;
~~~
 content::NavigationThrottle:
### GetNameForLogging

InterceptOMADownloadNavigationThrottle::GetNameForLogging
~~~cpp
const char* GetNameForLogging() override;
~~~

### InterceptDownload

InterceptOMADownloadNavigationThrottle::InterceptDownload
~~~cpp
void InterceptDownload();
~~~
 Helper method to intercept the download.
