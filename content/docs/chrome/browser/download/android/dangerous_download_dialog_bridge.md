
## class DangerousDownloadDialogBridge
 Class for showing dialogs to asks whether user wants to download a dangerous
 file.

### DangerousDownloadDialogBridge

DangerousDownloadDialogBridge::DangerousDownloadDialogBridge
~~~cpp
DangerousDownloadDialogBridge();
~~~

### DangerousDownloadDialogBridge

DangerousDownloadDialogBridge
~~~cpp
DangerousDownloadDialogBridge(const DangerousDownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DangerousDownloadDialogBridge& operator=(
      const DangerousDownloadDialogBridge&) = delete;
~~~

### ~DangerousDownloadDialogBridge

DangerousDownloadDialogBridge::~DangerousDownloadDialogBridge
~~~cpp
~DangerousDownloadDialogBridge() override;
~~~

### Show

DangerousDownloadDialogBridge::Show
~~~cpp
void Show(download::DownloadItem* download_item,
            ui::WindowAndroid* window_android);
~~~
 Called to create and show a dialog for a dangerous download.

### Accepted

DangerousDownloadDialogBridge::Accepted
~~~cpp
void Accepted(JNIEnv* env,
                const base::android::JavaParamRef<jstring>& jdownload_guid);
~~~
 Called from Java via JNI.

### Cancelled

DangerousDownloadDialogBridge::Cancelled
~~~cpp
void Cancelled(JNIEnv* env,
                 const base::android::JavaParamRef<jstring>& jdownload_guid);
~~~
 Called from Java via JNI.

### OnDownloadDestroyed

DangerousDownloadDialogBridge::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer:
### download_items_



~~~cpp

std::vector<download::DownloadItem*> download_items_;

~~~

 Download items that are requesting the dialog. Could get deleted while
 the dialog is showing.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.

### DangerousDownloadDialogBridge

DangerousDownloadDialogBridge
~~~cpp
DangerousDownloadDialogBridge(const DangerousDownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DangerousDownloadDialogBridge& operator=(
      const DangerousDownloadDialogBridge&) = delete;
~~~

### Show

DangerousDownloadDialogBridge::Show
~~~cpp
void Show(download::DownloadItem* download_item,
            ui::WindowAndroid* window_android);
~~~
 Called to create and show a dialog for a dangerous download.

### Accepted

DangerousDownloadDialogBridge::Accepted
~~~cpp
void Accepted(JNIEnv* env,
                const base::android::JavaParamRef<jstring>& jdownload_guid);
~~~
 Called from Java via JNI.

### Cancelled

DangerousDownloadDialogBridge::Cancelled
~~~cpp
void Cancelled(JNIEnv* env,
                 const base::android::JavaParamRef<jstring>& jdownload_guid);
~~~
 Called from Java via JNI.

### OnDownloadDestroyed

DangerousDownloadDialogBridge::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download_item) override;
~~~
 download::DownloadItem::Observer:
### download_items_



~~~cpp

std::vector<download::DownloadItem*> download_items_;

~~~

 Download items that are requesting the dialog. Could get deleted while
 the dialog is showing.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.
