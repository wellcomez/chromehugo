
## class DuplicateDownloadDialogBridge
 namespace content
 Class for showing dialogs to asks whether user wants to download a file
 that already exists on disk
### GetInstance

DuplicateDownloadDialogBridge::GetInstance
~~~cpp
static DuplicateDownloadDialogBridge* GetInstance();
~~~
 static
### DuplicateDownloadDialogBridge

DuplicateDownloadDialogBridge::DuplicateDownloadDialogBridge
~~~cpp
DuplicateDownloadDialogBridge();
~~~

### DuplicateDownloadDialogBridge

DuplicateDownloadDialogBridge
~~~cpp
DuplicateDownloadDialogBridge(const DuplicateDownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DuplicateDownloadDialogBridge& operator=(
      const DuplicateDownloadDialogBridge&) = delete;
~~~

### ~DuplicateDownloadDialogBridge

DuplicateDownloadDialogBridge::~DuplicateDownloadDialogBridge
~~~cpp
~DuplicateDownloadDialogBridge() override;
~~~

### Show

DuplicateDownloadDialogBridge::Show
~~~cpp
void Show(const std::string& file_path,
            const std::string& page_url,
            int64_t total_bytes,
            bool duplicate_request_exists,
            content::WebContents* web_contents,
            DuplicateDownloadDialogCallback callback);
~~~
 Called to create and show a dialog for a duplicate download.

### OnConfirmed

DuplicateDownloadDialogBridge::OnConfirmed
~~~cpp
void OnConfirmed(JNIEnv* env, jlong callback_id, jboolean accepted);
~~~
 Called from Java via JNI.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~

 Validator for all JNI callbacks.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.

### DuplicateDownloadDialogBridge

DuplicateDownloadDialogBridge
~~~cpp
DuplicateDownloadDialogBridge(const DuplicateDownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DuplicateDownloadDialogBridge& operator=(
      const DuplicateDownloadDialogBridge&) = delete;
~~~

### GetInstance

DuplicateDownloadDialogBridge::GetInstance
~~~cpp
static DuplicateDownloadDialogBridge* GetInstance();
~~~
 static
### Show

DuplicateDownloadDialogBridge::Show
~~~cpp
void Show(const std::string& file_path,
            const std::string& page_url,
            int64_t total_bytes,
            bool duplicate_request_exists,
            content::WebContents* web_contents,
            DuplicateDownloadDialogCallback callback);
~~~
 Called to create and show a dialog for a duplicate download.

### OnConfirmed

DuplicateDownloadDialogBridge::OnConfirmed
~~~cpp
void OnConfirmed(JNIEnv* env, jlong callback_id, jboolean accepted);
~~~
 Called from Java via JNI.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~

 Validator for all JNI callbacks.

### java_object_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_object_;

~~~

 The corresponding java object.
