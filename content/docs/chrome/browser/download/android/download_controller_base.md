### GetWebContents

GetWebContents
~~~cpp
content::WebContents* GetWebContents(int render_process_id, int render_view_id);
~~~

## class DownloadControllerBase
 Interface to request GET downloads and send notifications for POST
 downloads.

### Get

DownloadControllerBase::Get
~~~cpp
static DownloadControllerBase* Get();
~~~
 Returns the singleton instance of the DownloadControllerBase.

### SetDownloadControllerBase

DownloadControllerBase::SetDownloadControllerBase
~~~cpp
static void SetDownloadControllerBase(
      DownloadControllerBase* download_controller);
~~~
 Called to set the DownloadControllerBase instance.

### StartContextMenuDownload

StartContextMenuDownload
~~~cpp
virtual void StartContextMenuDownload(
      const content::ContextMenuParams& params,
      content::WebContents* web_contents,
      bool is_link) = 0;
~~~
 Called when a download is initiated by context menu.

### AcquireFileAccessPermission

AcquireFileAccessPermission
~~~cpp
virtual void AcquireFileAccessPermission(
      const content::WebContents::Getter& wc_getter,
      AcquireFileAccessPermissionCallback callback) = 0;
~~~
 Called to prompt the user for file access permission. When finished,
 |callback| will be executed.

### SetApproveFileAccessRequestForTesting

SetApproveFileAccessRequestForTesting
~~~cpp
virtual void SetApproveFileAccessRequestForTesting(bool approve) {}
~~~
 Called by unit test to approve or disapprove file access request.

### CreateAndroidDownload

CreateAndroidDownload
~~~cpp
virtual void CreateAndroidDownload(
      const content::WebContents::Getter& wc_getter,
      const DownloadInfo& info) = 0;
~~~
 Starts a new download request with Android DownloadManager. Can be called
 on any thread.

### AboutToResumeDownload

AboutToResumeDownload
~~~cpp
virtual void AboutToResumeDownload(download::DownloadItem* download_item) = 0;
~~~
 Called before resuming a download.

### ~DownloadControllerBase

~DownloadControllerBase
~~~cpp
~DownloadControllerBase() override {}
~~~

### field error



~~~cpp

static DownloadControllerBase* download_controller_;

~~~


### StartContextMenuDownload

StartContextMenuDownload
~~~cpp
virtual void StartContextMenuDownload(
      const content::ContextMenuParams& params,
      content::WebContents* web_contents,
      bool is_link) = 0;
~~~
 Called when a download is initiated by context menu.

### AcquireFileAccessPermission

AcquireFileAccessPermission
~~~cpp
virtual void AcquireFileAccessPermission(
      const content::WebContents::Getter& wc_getter,
      AcquireFileAccessPermissionCallback callback) = 0;
~~~
 Called to prompt the user for file access permission. When finished,
 |callback| will be executed.

### SetApproveFileAccessRequestForTesting

SetApproveFileAccessRequestForTesting
~~~cpp
virtual void SetApproveFileAccessRequestForTesting(bool approve) {}
~~~
 Called by unit test to approve or disapprove file access request.

### CreateAndroidDownload

CreateAndroidDownload
~~~cpp
virtual void CreateAndroidDownload(
      const content::WebContents::Getter& wc_getter,
      const DownloadInfo& info) = 0;
~~~
 Starts a new download request with Android DownloadManager. Can be called
 on any thread.

### AboutToResumeDownload

AboutToResumeDownload
~~~cpp
virtual void AboutToResumeDownload(download::DownloadItem* download_item) = 0;
~~~
 Called before resuming a download.

### ~DownloadControllerBase

~DownloadControllerBase
~~~cpp
~DownloadControllerBase() override {}
~~~

### Get

DownloadControllerBase::Get
~~~cpp
static DownloadControllerBase* Get();
~~~
 Returns the singleton instance of the DownloadControllerBase.

### SetDownloadControllerBase

DownloadControllerBase::SetDownloadControllerBase
~~~cpp
static void SetDownloadControllerBase(
      DownloadControllerBase* download_controller);
~~~
 Called to set the DownloadControllerBase instance.

### field error



~~~cpp

static DownloadControllerBase* download_controller_;

~~~

