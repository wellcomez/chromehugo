
## class DownloadDialogBridge
 Used to show a dialog for the user to select download details, such as file
 location, file name. and download start time.

 TODO(xingliu): Move logic out of the bridge, and write a test.

### DownloadDialogBridge

DownloadDialogBridge::DownloadDialogBridge
~~~cpp
DownloadDialogBridge();
~~~

### DownloadDialogBridge

DownloadDialogBridge
~~~cpp
DownloadDialogBridge(const DownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDialogBridge& operator=(const DownloadDialogBridge&) = delete;
~~~

### ~DownloadDialogBridge

DownloadDialogBridge::~DownloadDialogBridge
~~~cpp
virtual ~DownloadDialogBridge();
~~~

### ShowDialog

DownloadDialogBridge::ShowDialog
~~~cpp
virtual void ShowDialog(
      gfx::NativeWindow native_window,
      int64_t total_bytes,
      net::NetworkChangeNotifier::ConnectionType connection_type,
      DownloadLocationDialogType dialog_type,
      const base::FilePath& suggested_path,
      bool is_incognito,
      DialogCallback dialog_callback);
~~~
 Shows the download dialog.

### OnComplete

DownloadDialogBridge::OnComplete
~~~cpp
void OnComplete(JNIEnv* env,
                  const base::android::JavaParamRef<jobject>& obj,
                  const base::android::JavaParamRef<jstring>& returned_path);
~~~

### OnCanceled

DownloadDialogBridge::OnCanceled
~~~cpp
void OnCanceled(JNIEnv* env, const base::android::JavaParamRef<jobject>& obj);
~~~

### CompleteSelection

DownloadDialogBridge::CompleteSelection
~~~cpp
void CompleteSelection(DownloadDialogResult result);
~~~
 Called when the user finished the selections from download dialog.

### is_dialog_showing_



~~~cpp

bool is_dialog_showing_;

~~~


### java_obj_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_obj_;

~~~


### dialog_callback_



~~~cpp

DialogCallback dialog_callback_;

~~~


### DownloadDialogBridge

DownloadDialogBridge
~~~cpp
DownloadDialogBridge(const DownloadDialogBridge&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDialogBridge& operator=(const DownloadDialogBridge&) = delete;
~~~

### ShowDialog

DownloadDialogBridge::ShowDialog
~~~cpp
virtual void ShowDialog(
      gfx::NativeWindow native_window,
      int64_t total_bytes,
      net::NetworkChangeNotifier::ConnectionType connection_type,
      DownloadLocationDialogType dialog_type,
      const base::FilePath& suggested_path,
      bool is_incognito,
      DialogCallback dialog_callback);
~~~
 Shows the download dialog.

### OnComplete

DownloadDialogBridge::OnComplete
~~~cpp
void OnComplete(JNIEnv* env,
                  const base::android::JavaParamRef<jobject>& obj,
                  const base::android::JavaParamRef<jstring>& returned_path);
~~~

### OnCanceled

DownloadDialogBridge::OnCanceled
~~~cpp
void OnCanceled(JNIEnv* env, const base::android::JavaParamRef<jobject>& obj);
~~~

### CompleteSelection

DownloadDialogBridge::CompleteSelection
~~~cpp
void CompleteSelection(DownloadDialogResult result);
~~~
 Called when the user finished the selections from download dialog.

### is_dialog_showing_



~~~cpp

bool is_dialog_showing_;

~~~


### java_obj_



~~~cpp

base::android::ScopedJavaGlobalRef<jobject> java_obj_;

~~~


### dialog_callback_



~~~cpp

DialogCallback dialog_callback_;

~~~

