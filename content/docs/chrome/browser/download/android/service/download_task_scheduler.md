
## class DownloadTaskScheduler
 DownloadTaskScheduler is the utility class to schedule various types of
 background tasks with the OS as of when required by the download service.

### DownloadTaskScheduler

DownloadTaskScheduler::DownloadTaskScheduler
~~~cpp
DownloadTaskScheduler();
~~~

### DownloadTaskScheduler

DownloadTaskScheduler
~~~cpp
DownloadTaskScheduler(const DownloadTaskScheduler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadTaskScheduler& operator=(const DownloadTaskScheduler&) = delete;
~~~

### ~DownloadTaskScheduler

DownloadTaskScheduler::~DownloadTaskScheduler
~~~cpp
~DownloadTaskScheduler() override;
~~~

### ScheduleTask

DownloadTaskScheduler::ScheduleTask
~~~cpp
void ScheduleTask(DownloadTaskType task_type,
                    bool require_unmetered_network,
                    bool require_charging,
                    int optimal_battery_percentage,
                    int64_t window_start_time_seconds,
                    int64_t window_end_time_seconds) override;
~~~
 TaskScheduler implementation.

### CancelTask

DownloadTaskScheduler::CancelTask
~~~cpp
void CancelTask(DownloadTaskType task_type) override;
~~~

### DownloadTaskScheduler

DownloadTaskScheduler
~~~cpp
DownloadTaskScheduler(const DownloadTaskScheduler&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadTaskScheduler& operator=(const DownloadTaskScheduler&) = delete;
~~~

### ScheduleTask

DownloadTaskScheduler::ScheduleTask
~~~cpp
void ScheduleTask(DownloadTaskType task_type,
                    bool require_unmetered_network,
                    bool require_charging,
                    int optimal_battery_percentage,
                    int64_t window_start_time_seconds,
                    int64_t window_end_time_seconds) override;
~~~
 TaskScheduler implementation.

### CancelTask

DownloadTaskScheduler::CancelTask
~~~cpp
void CancelTask(DownloadTaskType task_type) override;
~~~
