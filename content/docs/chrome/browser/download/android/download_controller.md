
## class DownloadController

### GetInstance

DownloadController::GetInstance
~~~cpp
static DownloadController* GetInstance();
~~~

### DownloadController

DownloadController
~~~cpp
DownloadController(const DownloadController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadController& operator=(const DownloadController&) = delete;
~~~

### AcquireFileAccessPermission

DownloadController::AcquireFileAccessPermission
~~~cpp
void AcquireFileAccessPermission(
      const content::WebContents::Getter& wc_getter,
      AcquireFileAccessPermissionCallback callback) override;
~~~
 DownloadControllerBase implementation.

### CreateAndroidDownload

DownloadController::CreateAndroidDownload
~~~cpp
void CreateAndroidDownload(const content::WebContents::Getter& wc_getter,
                             const DownloadInfo& info) override;
~~~

### AboutToResumeDownload

DownloadController::AboutToResumeDownload
~~~cpp
void AboutToResumeDownload(download::DownloadItem* download_item) override;
~~~

### enum StoragePermissionType

~~~cpp
enum StoragePermissionType {
    STORAGE_PERMISSION_REQUESTED = 0,
    STORAGE_PERMISSION_NO_ACTION_NEEDED,
    STORAGE_PERMISSION_GRANTED,
    STORAGE_PERMISSION_DENIED,
    STORAGE_PERMISSION_NO_WEB_CONTENTS,
    STORAGE_PERMISSION_MAX
  }
~~~
### RecordStoragePermission

DownloadController::RecordStoragePermission
~~~cpp
static void RecordStoragePermission(StoragePermissionType type);
~~~

### CloseTabIfEmpty

DownloadController::CloseTabIfEmpty
~~~cpp
static void CloseTabIfEmpty(content::WebContents* web_contents,
                              download::DownloadItem* download);
~~~
 Close the |web_contents| for |download|. |download| could be null
 if the download is created by Android DownloadManager.

### validator

validator
~~~cpp
DownloadCallbackValidator* validator() { return &validator_; }
~~~

### DownloadController

DownloadController::DownloadController
~~~cpp
DownloadController();
~~~

### ~DownloadController

DownloadController::~DownloadController
~~~cpp
~DownloadController() override;
~~~

### OnDownloadStarted

DownloadController::OnDownloadStarted
~~~cpp
void OnDownloadStarted(download::DownloadItem* download_item) override;
~~~
 DownloadControllerBase implementation.

### StartContextMenuDownload

DownloadController::StartContextMenuDownload
~~~cpp
void StartContextMenuDownload(const content::ContextMenuParams& params,
                                content::WebContents* web_contents,
                                bool is_link) override;
~~~

### OnDownloadUpdated

DownloadController::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* item) override;
~~~
 DownloadItem::Observer interface.

### OnDangerousDownload

DownloadController::OnDangerousDownload
~~~cpp
void OnDangerousDownload(download::DownloadItem* item);
~~~
 The download item contains dangerous file types.

### StartAndroidDownload

DownloadController::StartAndroidDownload
~~~cpp
void StartAndroidDownload(const content::WebContents::Getter& wc_getter,
                            const DownloadInfo& info);
~~~
 Helper methods to start android download on UI thread.

### StartAndroidDownloadInternal

DownloadController::StartAndroidDownloadInternal
~~~cpp
void StartAndroidDownloadInternal(
      const content::WebContents::Getter& wc_getter,
      const DownloadInfo& info,
      bool allowed);
~~~

### IsInterruptedDownloadAutoResumable

DownloadController::IsInterruptedDownloadAutoResumable
~~~cpp
bool IsInterruptedDownloadAutoResumable(
      download::DownloadItem* download_item);
~~~
 Check if an interrupted download item can be auto resumed.

### GetProfileKey

DownloadController::GetProfileKey
~~~cpp
ProfileKey* GetProfileKey(download::DownloadItem* download_item);
~~~
 Get profile key from download item.

### default_file_name_



~~~cpp

std::string default_file_name_;

~~~


### strong_validators_map_



~~~cpp

StrongValidatorsMap strong_validators_map_;

~~~

 Stores the previous strong validators before a download is resumed. If the
 strong validators change after resumption starts, the download will restart
 from the beginning and all downloaded data will be lost.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~


### dangerous_download_bridge_



~~~cpp

std::unique_ptr<DangerousDownloadDialogBridge> dangerous_download_bridge_;

~~~


### DownloadController

DownloadController
~~~cpp
DownloadController(const DownloadController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadController& operator=(const DownloadController&) = delete;
~~~

### validator

validator
~~~cpp
DownloadCallbackValidator* validator() { return &validator_; }
~~~

### GetInstance

DownloadController::GetInstance
~~~cpp
static DownloadController* GetInstance();
~~~

### AcquireFileAccessPermission

DownloadController::AcquireFileAccessPermission
~~~cpp
void AcquireFileAccessPermission(
      const content::WebContents::Getter& wc_getter,
      AcquireFileAccessPermissionCallback callback) override;
~~~
 DownloadControllerBase implementation.

### CreateAndroidDownload

DownloadController::CreateAndroidDownload
~~~cpp
void CreateAndroidDownload(const content::WebContents::Getter& wc_getter,
                             const DownloadInfo& info) override;
~~~

### AboutToResumeDownload

DownloadController::AboutToResumeDownload
~~~cpp
void AboutToResumeDownload(download::DownloadItem* download_item) override;
~~~

### enum StoragePermissionType
 UMA histogram enum for download storage permission requests. Keep this
 in sync with MobileDownloadStoragePermission in histograms.xml. This should
 be append only.

~~~cpp
enum StoragePermissionType {
    STORAGE_PERMISSION_REQUESTED = 0,
    STORAGE_PERMISSION_NO_ACTION_NEEDED,
    STORAGE_PERMISSION_GRANTED,
    STORAGE_PERMISSION_DENIED,
    STORAGE_PERMISSION_NO_WEB_CONTENTS,
    STORAGE_PERMISSION_MAX
  };
~~~
### RecordStoragePermission

DownloadController::RecordStoragePermission
~~~cpp
static void RecordStoragePermission(StoragePermissionType type);
~~~

### CloseTabIfEmpty

DownloadController::CloseTabIfEmpty
~~~cpp
static void CloseTabIfEmpty(content::WebContents* web_contents,
                              download::DownloadItem* download);
~~~
 Close the |web_contents| for |download|. |download| could be null
 if the download is created by Android DownloadManager.

### OnDownloadStarted

DownloadController::OnDownloadStarted
~~~cpp
void OnDownloadStarted(download::DownloadItem* download_item) override;
~~~
 DownloadControllerBase implementation.

### StartContextMenuDownload

DownloadController::StartContextMenuDownload
~~~cpp
void StartContextMenuDownload(const content::ContextMenuParams& params,
                                content::WebContents* web_contents,
                                bool is_link) override;
~~~

### OnDownloadUpdated

DownloadController::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(download::DownloadItem* item) override;
~~~
 DownloadItem::Observer interface.

### OnDangerousDownload

DownloadController::OnDangerousDownload
~~~cpp
void OnDangerousDownload(download::DownloadItem* item);
~~~
 The download item contains dangerous file types.

### StartAndroidDownload

DownloadController::StartAndroidDownload
~~~cpp
void StartAndroidDownload(const content::WebContents::Getter& wc_getter,
                            const DownloadInfo& info);
~~~
 Helper methods to start android download on UI thread.

### StartAndroidDownloadInternal

DownloadController::StartAndroidDownloadInternal
~~~cpp
void StartAndroidDownloadInternal(
      const content::WebContents::Getter& wc_getter,
      const DownloadInfo& info,
      bool allowed);
~~~

### IsInterruptedDownloadAutoResumable

DownloadController::IsInterruptedDownloadAutoResumable
~~~cpp
bool IsInterruptedDownloadAutoResumable(
      download::DownloadItem* download_item);
~~~
 Check if an interrupted download item can be auto resumed.

### GetProfileKey

DownloadController::GetProfileKey
~~~cpp
ProfileKey* GetProfileKey(download::DownloadItem* download_item);
~~~
 Get profile key from download item.

### default_file_name_



~~~cpp

std::string default_file_name_;

~~~


### strong_validators_map_



~~~cpp

StrongValidatorsMap strong_validators_map_;

~~~

 Stores the previous strong validators before a download is resumed. If the
 strong validators change after resumption starts, the download will restart
 from the beginning and all downloaded data will be lost.

### validator_



~~~cpp

DownloadCallbackValidator validator_;

~~~


### dangerous_download_bridge_



~~~cpp

std::unique_ptr<DangerousDownloadDialogBridge> dangerous_download_bridge_;

~~~

