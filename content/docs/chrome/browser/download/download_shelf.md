
## class DownloadShelf
 This is an abstract base class for platform specific download shelf
 implementations.

### DownloadShelf

DownloadShelf::DownloadShelf
~~~cpp
DownloadShelf(Browser* browser, Profile* profile);
~~~

### DownloadShelf

DownloadShelf
~~~cpp
DownloadShelf(const DownloadShelf&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelf& operator=(const DownloadShelf&) = delete;
~~~

### ~DownloadShelf

DownloadShelf::~DownloadShelf
~~~cpp
virtual ~DownloadShelf();
~~~

### IsShowing

IsShowing
~~~cpp
virtual bool IsShowing() const = 0;
~~~
 The browser view needs to know when we are going away to properly return
 the resize corner size to WebKit so that we don't draw on top of it.

 This returns the showing state of our animation which is set to true at
 the beginning Show and false at the beginning of a Hide.

### IsClosing

IsClosing
~~~cpp
virtual bool IsClosing() const = 0;
~~~
 Returns whether the download shelf is showing the close animation.

### AddDownload

DownloadShelf::AddDownload
~~~cpp
void AddDownload(DownloadUIModel::DownloadUIModelPtr download);
~~~
 A new download has started. Add it to our shelf and show the download
 started animation.


 Some downloads are removed from the shelf on completion (See
 DownloadItemModel::ShouldRemoveFromShelfWhenComplete()). These transient
 downloads are added to the shelf after a delay. If the download completes
 before the delay duration, it will not be added to the shelf at all.

### Open

DownloadShelf::Open
~~~cpp
void Open();
~~~
 Opens the shelf.

### Close

DownloadShelf::Close
~~~cpp
void Close();
~~~
 Closes the shelf.

### Hide

DownloadShelf::Hide
~~~cpp
void Hide();
~~~
 Closes the shelf and prevents it from reopening until Unhide() is called.

### Unhide

DownloadShelf::Unhide
~~~cpp
void Unhide();
~~~
 Allows the shelf to open after a previous call to Hide().  Opens the shelf
 if, had Hide() not been called, it would currently be open.

### browser

browser
~~~cpp
Browser* browser() { return browser_; }
~~~

### GetView

GetView
~~~cpp
virtual views::View* GetView() = 0;
~~~

### is_hidden

is_hidden
~~~cpp
bool is_hidden() const { return is_hidden_; }
~~~

### DoShowDownload

DoShowDownload
~~~cpp
virtual void DoShowDownload(DownloadUIModel::DownloadUIModelPtr download) = 0;
~~~

### DoOpen

DoOpen
~~~cpp
virtual void DoOpen() = 0;
~~~

### DoClose

DoClose
~~~cpp
virtual void DoClose() = 0;
~~~

### DoHide

DoHide
~~~cpp
virtual void DoHide() = 0;
~~~

### DoUnhide

DoUnhide
~~~cpp
virtual void DoUnhide() = 0;
~~~

### GetTransientDownloadShowDelay

DownloadShelf::GetTransientDownloadShowDelay
~~~cpp
virtual base::TimeDelta GetTransientDownloadShowDelay() const;
~~~
 Time delay to wait before adding a transient download to the shelf.

 Protected virtual for testing.

### profile

profile
~~~cpp
Profile* profile() { return profile_; }
~~~

### ShowDownload

DownloadShelf::ShowDownload
~~~cpp
void ShowDownload(DownloadUIModel::DownloadUIModelPtr download);
~~~
 Shows the download on the shelf immediately. Also displays the download
 started animation if necessary.

### ShowDownloadById

DownloadShelf::ShowDownloadById
~~~cpp
void ShowDownloadById(const offline_items_collection::ContentId& id);
~~~
 Similar to ShowDownload() but refers to the download using an ID.

### OnGetDownloadDoneForOfflineItem

DownloadShelf::OnGetDownloadDoneForOfflineItem
~~~cpp
void OnGetDownloadDoneForOfflineItem(
      const absl::optional<offline_items_collection::OfflineItem>& item);
~~~
 Callback used by ShowDownloadById() to trigger ShowDownload() once |item|
 has been fetched.

### browser_



~~~cpp

const raw_ptr<Browser> browser_;

~~~


### profile_



~~~cpp

const raw_ptr<Profile> profile_;

~~~


### should_show_on_unhide_



~~~cpp

bool should_show_on_unhide_ = false;

~~~


### is_hidden_



~~~cpp

bool is_hidden_ = false;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadShelf> weak_ptr_factory_{this};

~~~


### DownloadShelf

DownloadShelf
~~~cpp
DownloadShelf(const DownloadShelf&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadShelf& operator=(const DownloadShelf&) = delete;
~~~

### IsShowing

IsShowing
~~~cpp
virtual bool IsShowing() const = 0;
~~~
 The browser view needs to know when we are going away to properly return
 the resize corner size to WebKit so that we don't draw on top of it.

 This returns the showing state of our animation which is set to true at
 the beginning Show and false at the beginning of a Hide.

### IsClosing

IsClosing
~~~cpp
virtual bool IsClosing() const = 0;
~~~
 Returns whether the download shelf is showing the close animation.

### browser

browser
~~~cpp
Browser* browser() { return browser_; }
~~~

### GetView

GetView
~~~cpp
virtual views::View* GetView() = 0;
~~~

### is_hidden

is_hidden
~~~cpp
bool is_hidden() const { return is_hidden_; }
~~~

### DoShowDownload

DoShowDownload
~~~cpp
virtual void DoShowDownload(DownloadUIModel::DownloadUIModelPtr download) = 0;
~~~

### DoOpen

DoOpen
~~~cpp
virtual void DoOpen() = 0;
~~~

### DoClose

DoClose
~~~cpp
virtual void DoClose() = 0;
~~~

### DoHide

DoHide
~~~cpp
virtual void DoHide() = 0;
~~~

### DoUnhide

DoUnhide
~~~cpp
virtual void DoUnhide() = 0;
~~~

### profile

profile
~~~cpp
Profile* profile() { return profile_; }
~~~

### AddDownload

DownloadShelf::AddDownload
~~~cpp
void AddDownload(DownloadUIModel::DownloadUIModelPtr download);
~~~
 A new download has started. Add it to our shelf and show the download
 started animation.


 Some downloads are removed from the shelf on completion (See
 DownloadItemModel::ShouldRemoveFromShelfWhenComplete()). These transient
 downloads are added to the shelf after a delay. If the download completes
 before the delay duration, it will not be added to the shelf at all.

### Open

DownloadShelf::Open
~~~cpp
void Open();
~~~
 Opens the shelf.

### Close

DownloadShelf::Close
~~~cpp
void Close();
~~~
 Closes the shelf.

### Hide

DownloadShelf::Hide
~~~cpp
void Hide();
~~~
 Closes the shelf and prevents it from reopening until Unhide() is called.

### Unhide

DownloadShelf::Unhide
~~~cpp
void Unhide();
~~~
 Allows the shelf to open after a previous call to Hide().  Opens the shelf
 if, had Hide() not been called, it would currently be open.

### GetTransientDownloadShowDelay

DownloadShelf::GetTransientDownloadShowDelay
~~~cpp
virtual base::TimeDelta GetTransientDownloadShowDelay() const;
~~~
 Time delay to wait before adding a transient download to the shelf.

 Protected virtual for testing.

### ShowDownload

DownloadShelf::ShowDownload
~~~cpp
void ShowDownload(DownloadUIModel::DownloadUIModelPtr download);
~~~
 Shows the download on the shelf immediately. Also displays the download
 started animation if necessary.

### ShowDownloadById

DownloadShelf::ShowDownloadById
~~~cpp
void ShowDownloadById(const offline_items_collection::ContentId& id);
~~~
 Similar to ShowDownload() but refers to the download using an ID.

### OnGetDownloadDoneForOfflineItem

DownloadShelf::OnGetDownloadDoneForOfflineItem
~~~cpp
void OnGetDownloadDoneForOfflineItem(
      const absl::optional<offline_items_collection::OfflineItem>& item);
~~~
 Callback used by ShowDownloadById() to trigger ShowDownload() once |item|
 has been fetched.

### browser_



~~~cpp

const raw_ptr<Browser> browser_;

~~~


### profile_



~~~cpp

const raw_ptr<Profile> profile_;

~~~


### should_show_on_unhide_



~~~cpp

bool should_show_on_unhide_ = false;

~~~


### is_hidden_



~~~cpp

bool is_hidden_ = false;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadShelf> weak_ptr_factory_{this};

~~~

