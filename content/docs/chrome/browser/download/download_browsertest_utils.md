### DownloadManagerForBrowser

DownloadManagerForBrowser
~~~cpp
content::DownloadManager* DownloadManagerForBrowser(Browser* browser);
~~~
 Gets the download manager for a browser.

## class DownloadTestObserverResumable
 DownloadTestObserver subclass that observes one download until it transitions
 from a non-resumable state to a resumable state a specified number of
 times. Note that this observer can only observe a single download.

### DownloadTestObserverResumable

DownloadTestObserverResumable::DownloadTestObserverResumable
~~~cpp
DownloadTestObserverResumable(content::DownloadManager* download_manager,
                                size_t transition_count);
~~~
 Construct a new observer. |transition_count| is the number of times the
 download should transition from a non-resumable state to a resumable state.

### DownloadTestObserverResumable

DownloadTestObserverResumable
~~~cpp
DownloadTestObserverResumable(const DownloadTestObserverResumable&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadTestObserverResumable& operator=(
      const DownloadTestObserverResumable&) = delete;
~~~

### ~DownloadTestObserverResumable

DownloadTestObserverResumable::~DownloadTestObserverResumable
~~~cpp
~DownloadTestObserverResumable() override;
~~~

### IsDownloadInFinalState

DownloadTestObserverResumable::IsDownloadInFinalState
~~~cpp
bool IsDownloadInFinalState(download::DownloadItem* download) override;
~~~

### was_previously_resumable_



~~~cpp

bool was_previously_resumable_ = false;

~~~


### transitions_left_



~~~cpp

size_t transitions_left_;

~~~


### DownloadTestObserverResumable

DownloadTestObserverResumable
~~~cpp
DownloadTestObserverResumable(const DownloadTestObserverResumable&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadTestObserverResumable& operator=(
      const DownloadTestObserverResumable&) = delete;
~~~

### IsDownloadInFinalState

DownloadTestObserverResumable::IsDownloadInFinalState
~~~cpp
bool IsDownloadInFinalState(download::DownloadItem* download) override;
~~~

### was_previously_resumable_



~~~cpp

bool was_previously_resumable_ = false;

~~~


### transitions_left_



~~~cpp

size_t transitions_left_;

~~~


## class DownloadTestObserverNotInProgress
 DownloadTestObserver subclass that observes a download until it transitions
 from IN_PROGRESS to another state, but only after StartObserving() is called.

### DownloadTestObserverNotInProgress

DownloadTestObserverNotInProgress::DownloadTestObserverNotInProgress
~~~cpp
DownloadTestObserverNotInProgress(content::DownloadManager* download_manager,
                                    size_t count);
~~~

### DownloadTestObserverNotInProgress

DownloadTestObserverNotInProgress
~~~cpp
DownloadTestObserverNotInProgress(const DownloadTestObserverNotInProgress&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadTestObserverNotInProgress& operator=(
      const DownloadTestObserverNotInProgress&) = delete;
~~~

### ~DownloadTestObserverNotInProgress

DownloadTestObserverNotInProgress::~DownloadTestObserverNotInProgress
~~~cpp
~DownloadTestObserverNotInProgress() override;
~~~

### StartObserving

DownloadTestObserverNotInProgress::StartObserving
~~~cpp
void StartObserving();
~~~

### IsDownloadInFinalState

DownloadTestObserverNotInProgress::IsDownloadInFinalState
~~~cpp
bool IsDownloadInFinalState(download::DownloadItem* download) override;
~~~

### started_observing_



~~~cpp

bool started_observing_;

~~~


### DownloadTestObserverNotInProgress

DownloadTestObserverNotInProgress
~~~cpp
DownloadTestObserverNotInProgress(const DownloadTestObserverNotInProgress&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadTestObserverNotInProgress& operator=(
      const DownloadTestObserverNotInProgress&) = delete;
~~~

### StartObserving

DownloadTestObserverNotInProgress::StartObserving
~~~cpp
void StartObserving();
~~~

### IsDownloadInFinalState

DownloadTestObserverNotInProgress::IsDownloadInFinalState
~~~cpp
bool IsDownloadInFinalState(download::DownloadItem* download) override;
~~~

### started_observing_



~~~cpp

bool started_observing_;

~~~


## class DownloadTestBase

### enum DownloadMethod

~~~cpp
enum DownloadMethod { DOWNLOAD_NAVIGATE, DOWNLOAD_DIRECT }
~~~
###  DI::GetURL

 Information passed in to |DownloadFileCheckErrors()|.

~~~cpp
struct DownloadInfo {
    const char* starting_url;           // URL for initiating the download.
    const char* expected_download_url;  // Expected value of DI::GetURL(). Can
                                        // be different if |starting_url|
                                        // initiates a download from another
                                        // URL.
    DownloadMethod download_method;     // Navigation or Direct.
    // Download interrupt reason (NONE is OK).
    download::DownloadInterruptReason reason;
    bool show_download_item;  // True if the download item appears on the shelf.
    bool should_redirect_to_documents;  // True if we save it in "My Documents".
  };
~~~
### field error



~~~cpp

struct FileErrorInjectInfo {
    DownloadInfo download_info;
    content::TestFileErrorInjector::FileErrorInfo error_info;
  };

~~~


### field error



~~~cpp

static constexpr char kDownloadTest1Path[] = "download-test1.lib";

~~~


### DownloadTestBase

DownloadTestBase::DownloadTestBase
~~~cpp
DownloadTestBase();
~~~

### ~DownloadTestBase

DownloadTestBase::~DownloadTestBase
~~~cpp
~DownloadTestBase() override;
~~~

### SetUpOnMainThread

DownloadTestBase::SetUpOnMainThread
~~~cpp
void SetUpOnMainThread() override;
~~~

### SetUpCommandLine

DownloadTestBase::SetUpCommandLine
~~~cpp
void SetUpCommandLine(base::CommandLine* command_line) override;
~~~

### TearDownOnMainThread

DownloadTestBase::TearDownOnMainThread
~~~cpp
void TearDownOnMainThread() override;
~~~

### CheckTestDir

DownloadTestBase::CheckTestDir
~~~cpp
bool CheckTestDir();
~~~

### InitialSetup

DownloadTestBase::InitialSetup
~~~cpp
bool InitialSetup();
~~~
 Returning false indicates a failure of the setup, and should be asserted
 in the caller.

### enum SizeTestType

~~~cpp
enum SizeTestType {
    SIZE_TEST_TYPE_KNOWN,
    SIZE_TEST_TYPE_UNKNOWN,
  }
~~~
### GetTestDataDirectory

DownloadTestBase::GetTestDataDirectory
~~~cpp
base::FilePath GetTestDataDirectory();
~~~

### OriginFile

DownloadTestBase::OriginFile
~~~cpp
base::FilePath OriginFile(const base::FilePath& file);
~~~
 Location of the file source (the place from which it is downloaded).

### DestinationFile

DownloadTestBase::DestinationFile
~~~cpp
base::FilePath DestinationFile(Browser* browser, const base::FilePath& file);
~~~
 Location of the file destination (place to which it is downloaded).

### test_response_handler

DownloadTestBase::test_response_handler
~~~cpp
content::TestDownloadResponseHandler* test_response_handler();
~~~

### GetDownloadPrefs

DownloadTestBase::GetDownloadPrefs
~~~cpp
DownloadPrefs* GetDownloadPrefs(Browser* browser);
~~~

### GetDownloadDirectory

DownloadTestBase::GetDownloadDirectory
~~~cpp
base::FilePath GetDownloadDirectory(Browser* browser);
~~~

### CreateWaiter

DownloadTestBase::CreateWaiter
~~~cpp
content::DownloadTestObserver* CreateWaiter(Browser* browser,
                                              int num_downloads);
~~~
 Create a DownloadTestObserverTerminal that will wait for the
 specified number of downloads to finish.

### CreateInProgressWaiter

DownloadTestBase::CreateInProgressWaiter
~~~cpp
content::DownloadTestObserver* CreateInProgressWaiter(Browser* browser,
                                                        int num_downloads);
~~~
 Create a DownloadTestObserverInProgress that will wait for the
 specified number of downloads to start.

### DangerousDownloadWaiter

DownloadTestBase::DangerousDownloadWaiter
~~~cpp
content::DownloadTestObserver* DangerousDownloadWaiter(
      Browser* browser,
      int num_downloads,
      content::DownloadTestObserver::DangerousDownloadAction
          dangerous_download_action);
~~~
 Create a DownloadTestObserverTerminal that will wait for the
 specified number of downloads to finish, or for
 a dangerous download warning to be shown.

### CheckDownloadStatesForBrowser

DownloadTestBase::CheckDownloadStatesForBrowser
~~~cpp
void CheckDownloadStatesForBrowser(
      Browser* browser,
      size_t num,
      download::DownloadItem::DownloadState state);
~~~

### CheckDownloadStates

DownloadTestBase::CheckDownloadStates
~~~cpp
void CheckDownloadStates(size_t num,
                           download::DownloadItem::DownloadState state);
~~~

### VerifyNoDownloads

DownloadTestBase::VerifyNoDownloads
~~~cpp
bool VerifyNoDownloads() const;
~~~

### DownloadAndWaitWithDisposition

DownloadTestBase::DownloadAndWaitWithDisposition
~~~cpp
void DownloadAndWaitWithDisposition(Browser* browser,
                                      const GURL& url,
                                      WindowOpenDisposition disposition,
                                      int browser_test_flags);
~~~
 Download |url|, then wait for the download to finish.

 |disposition| indicates where the navigation occurs (current tab, new
 foreground tab, etc).

 |browser_test_flags| indicate what to wait for, and is an OR of 0 or more
 values in the ui_test_utils::BrowserTestWaitFlags enum.

### DownloadAndWait

DownloadTestBase::DownloadAndWait
~~~cpp
void DownloadAndWait(Browser* browser, const GURL& url);
~~~
 Download a file in the current tab, then wait for the download to finish.

### CheckDownload

DownloadTestBase::CheckDownload
~~~cpp
bool CheckDownload(Browser* browser,
                     const base::FilePath& downloaded_filename,
                     const base::FilePath& origin_filename);
~~~
 Should only be called when the download is known to have finished
 (in error or not).

 Returning false indicates a failure of the function, and should be asserted
 in the caller.

### CheckDownloadFullPaths

DownloadTestBase::CheckDownloadFullPaths
~~~cpp
bool CheckDownloadFullPaths(Browser* browser,
                              const base::FilePath& downloaded_file,
                              const base::FilePath& origin_file);
~~~
 A version of CheckDownload that allows complete path specification.

### CreateInProgressDownloadObserver

DownloadTestBase::CreateInProgressDownloadObserver
~~~cpp
content::DownloadTestObserver* CreateInProgressDownloadObserver(
      size_t download_count);
~~~

### CreateSlowTestDownload

DownloadTestBase::CreateSlowTestDownload
~~~cpp
download::DownloadItem* CreateSlowTestDownload();
~~~

### RunSizeTest

DownloadTestBase::RunSizeTest
~~~cpp
bool RunSizeTest(Browser* browser,
                   SizeTestType type,
                   const std::string& partial_indication,
                   const std::string& total_indication);
~~~

### GetDownloads

DownloadTestBase::GetDownloads
~~~cpp
void GetDownloads(Browser* browser,
                    std::vector<download::DownloadItem*>* downloads) const;
~~~

### ExpectWindowCountAfterDownload

DownloadTestBase::ExpectWindowCountAfterDownload
~~~cpp
static void ExpectWindowCountAfterDownload(size_t expected);
~~~

### EnableFileChooser

DownloadTestBase::EnableFileChooser
~~~cpp
void EnableFileChooser(bool enable);
~~~

### DidShowFileChooser

DownloadTestBase::DidShowFileChooser
~~~cpp
bool DidShowFileChooser();
~~~

### VerifyFile

DownloadTestBase::VerifyFile
~~~cpp
bool VerifyFile(const base::FilePath& path,
                  const std::string& value,
                  const int64_t file_size);
~~~
 Checks that |path| is has |file_size| bytes, and matches the |value|
 string.

### DownloadFilesCheckErrorsSetup

DownloadTestBase::DownloadFilesCheckErrorsSetup
~~~cpp
void DownloadFilesCheckErrorsSetup();
~~~
 Attempts to download a file, based on information in |download_info|.

 If a Select File dialog opens, will automatically choose the default.

### DownloadFilesCheckErrors

DownloadTestBase::DownloadFilesCheckErrors
~~~cpp
void DownloadFilesCheckErrors(size_t count, DownloadInfo* download_info);
~~~
 Attempts to download a set of files, based on information in the
 |download_info| array.  |count| is the number of files.

 If a Select File dialog appears, it will choose the default and return
 immediately.

### DownloadFilesCheckErrorsLoopBody

DownloadTestBase::DownloadFilesCheckErrorsLoopBody
~~~cpp
void DownloadFilesCheckErrorsLoopBody(const DownloadInfo& download_info,
                                        size_t i);
~~~

### DownloadInsertFilesErrorCheckErrors

DownloadTestBase::DownloadInsertFilesErrorCheckErrors
~~~cpp
void DownloadInsertFilesErrorCheckErrors(size_t count,
                                           FileErrorInjectInfo* info);
~~~

### DownloadInsertFilesErrorCheckErrorsLoopBody

DownloadTestBase::DownloadInsertFilesErrorCheckErrorsLoopBody
~~~cpp
void DownloadInsertFilesErrorCheckErrorsLoopBody(
      scoped_refptr<content::TestFileErrorInjector> injector,
      const FileErrorInjectInfo& info,
      size_t i);
~~~

### DownloadFilesToReadonlyFolder

DownloadTestBase::DownloadFilesToReadonlyFolder
~~~cpp
void DownloadFilesToReadonlyFolder(size_t count, DownloadInfo* download_info);
~~~
 Attempts to download a file to a read-only folder, based on information
 in |download_info|.

### StartMockDownloadAndInjectError

DownloadTestBase::StartMockDownloadAndInjectError
~~~cpp
download::DownloadItem* StartMockDownloadAndInjectError(
      content::TestFileErrorInjector* error_injector,
      download::DownloadInterruptReason error);
~~~
 This method:
 * Starts a mock download by navigating to embedded test server URL.

 * Injects |error| on the first write using |error_injector|.

 * Waits for the download to be interrupted.

 * Clears the errors on |error_injector|.

 * Returns the resulting interrupted download.

### test_dir_



~~~cpp

base::FilePath test_dir_;

~~~

 Location of the test data.

### test_response_handler_



~~~cpp

content::TestDownloadResponseHandler test_response_handler_;

~~~


### file_activity_observer_



~~~cpp

std::unique_ptr<DownloadTestFileActivityObserver> file_activity_observer_;

~~~


### ignore_content_verifier_



~~~cpp

extensions::ScopedIgnoreContentVerifierForTest ignore_content_verifier_;

~~~


### ignore_install_verification_



~~~cpp

extensions::ScopedInstallVerifierBypassForTest ignore_install_verification_;

~~~


### enum DownloadMethod
 Choice of navigation or direct fetch.  Used by |DownloadFileCheckErrors()|.

~~~cpp
enum DownloadMethod { DOWNLOAD_NAVIGATE, DOWNLOAD_DIRECT };
~~~
###  DI::GetURL

 Information passed in to |DownloadFileCheckErrors()|.

~~~cpp
struct DownloadInfo {
    const char* starting_url;           // URL for initiating the download.
    const char* expected_download_url;  // Expected value of DI::GetURL(). Can
                                        // be different if |starting_url|
                                        // initiates a download from another
                                        // URL.
    DownloadMethod download_method;     // Navigation or Direct.
    // Download interrupt reason (NONE is OK).
    download::DownloadInterruptReason reason;
    bool show_download_item;  // True if the download item appears on the shelf.
    bool should_redirect_to_documents;  // True if we save it in "My Documents".
  };
~~~
### field error



~~~cpp

struct FileErrorInjectInfo {
    DownloadInfo download_info;
    content::TestFileErrorInjector::FileErrorInfo error_info;
  };

~~~


### field error



~~~cpp

static constexpr char kDownloadTest1Path[] = "download-test1.lib";

~~~


### SetUpOnMainThread

DownloadTestBase::SetUpOnMainThread
~~~cpp
void SetUpOnMainThread() override;
~~~

### SetUpCommandLine

DownloadTestBase::SetUpCommandLine
~~~cpp
void SetUpCommandLine(base::CommandLine* command_line) override;
~~~

### TearDownOnMainThread

DownloadTestBase::TearDownOnMainThread
~~~cpp
void TearDownOnMainThread() override;
~~~

### CheckTestDir

DownloadTestBase::CheckTestDir
~~~cpp
bool CheckTestDir();
~~~

### InitialSetup

DownloadTestBase::InitialSetup
~~~cpp
bool InitialSetup();
~~~
 Returning false indicates a failure of the setup, and should be asserted
 in the caller.

### enum SizeTestType

~~~cpp
enum SizeTestType {
    SIZE_TEST_TYPE_KNOWN,
    SIZE_TEST_TYPE_UNKNOWN,
  };
~~~
### GetTestDataDirectory

DownloadTestBase::GetTestDataDirectory
~~~cpp
base::FilePath GetTestDataDirectory();
~~~

### OriginFile

DownloadTestBase::OriginFile
~~~cpp
base::FilePath OriginFile(const base::FilePath& file);
~~~
 Location of the file source (the place from which it is downloaded).

### DestinationFile

DownloadTestBase::DestinationFile
~~~cpp
base::FilePath DestinationFile(Browser* browser, const base::FilePath& file);
~~~
 Location of the file destination (place to which it is downloaded).

### test_response_handler

DownloadTestBase::test_response_handler
~~~cpp
content::TestDownloadResponseHandler* test_response_handler();
~~~

### GetDownloadPrefs

DownloadTestBase::GetDownloadPrefs
~~~cpp
DownloadPrefs* GetDownloadPrefs(Browser* browser);
~~~

### GetDownloadDirectory

DownloadTestBase::GetDownloadDirectory
~~~cpp
base::FilePath GetDownloadDirectory(Browser* browser);
~~~

### CreateWaiter

DownloadTestBase::CreateWaiter
~~~cpp
content::DownloadTestObserver* CreateWaiter(Browser* browser,
                                              int num_downloads);
~~~
 Create a DownloadTestObserverTerminal that will wait for the
 specified number of downloads to finish.

### CreateInProgressWaiter

DownloadTestBase::CreateInProgressWaiter
~~~cpp
content::DownloadTestObserver* CreateInProgressWaiter(Browser* browser,
                                                        int num_downloads);
~~~
 Create a DownloadTestObserverInProgress that will wait for the
 specified number of downloads to start.

### DangerousDownloadWaiter

DownloadTestBase::DangerousDownloadWaiter
~~~cpp
content::DownloadTestObserver* DangerousDownloadWaiter(
      Browser* browser,
      int num_downloads,
      content::DownloadTestObserver::DangerousDownloadAction
          dangerous_download_action);
~~~
 Create a DownloadTestObserverTerminal that will wait for the
 specified number of downloads to finish, or for
 a dangerous download warning to be shown.

### CheckDownloadStatesForBrowser

DownloadTestBase::CheckDownloadStatesForBrowser
~~~cpp
void CheckDownloadStatesForBrowser(
      Browser* browser,
      size_t num,
      download::DownloadItem::DownloadState state);
~~~

### CheckDownloadStates

DownloadTestBase::CheckDownloadStates
~~~cpp
void CheckDownloadStates(size_t num,
                           download::DownloadItem::DownloadState state);
~~~

### VerifyNoDownloads

DownloadTestBase::VerifyNoDownloads
~~~cpp
bool VerifyNoDownloads() const;
~~~

### DownloadAndWaitWithDisposition

DownloadTestBase::DownloadAndWaitWithDisposition
~~~cpp
void DownloadAndWaitWithDisposition(Browser* browser,
                                      const GURL& url,
                                      WindowOpenDisposition disposition,
                                      int browser_test_flags);
~~~
 Download |url|, then wait for the download to finish.

 |disposition| indicates where the navigation occurs (current tab, new
 foreground tab, etc).

 |browser_test_flags| indicate what to wait for, and is an OR of 0 or more
 values in the ui_test_utils::BrowserTestWaitFlags enum.

### DownloadAndWait

DownloadTestBase::DownloadAndWait
~~~cpp
void DownloadAndWait(Browser* browser, const GURL& url);
~~~
 Download a file in the current tab, then wait for the download to finish.

### CheckDownload

DownloadTestBase::CheckDownload
~~~cpp
bool CheckDownload(Browser* browser,
                     const base::FilePath& downloaded_filename,
                     const base::FilePath& origin_filename);
~~~
 Should only be called when the download is known to have finished
 (in error or not).

 Returning false indicates a failure of the function, and should be asserted
 in the caller.

### CheckDownloadFullPaths

DownloadTestBase::CheckDownloadFullPaths
~~~cpp
bool CheckDownloadFullPaths(Browser* browser,
                              const base::FilePath& downloaded_file,
                              const base::FilePath& origin_file);
~~~
 A version of CheckDownload that allows complete path specification.

### CreateInProgressDownloadObserver

DownloadTestBase::CreateInProgressDownloadObserver
~~~cpp
content::DownloadTestObserver* CreateInProgressDownloadObserver(
      size_t download_count);
~~~

### CreateSlowTestDownload

DownloadTestBase::CreateSlowTestDownload
~~~cpp
download::DownloadItem* CreateSlowTestDownload();
~~~

### RunSizeTest

DownloadTestBase::RunSizeTest
~~~cpp
bool RunSizeTest(Browser* browser,
                   SizeTestType type,
                   const std::string& partial_indication,
                   const std::string& total_indication);
~~~

### GetDownloads

DownloadTestBase::GetDownloads
~~~cpp
void GetDownloads(Browser* browser,
                    std::vector<download::DownloadItem*>* downloads) const;
~~~

### ExpectWindowCountAfterDownload

DownloadTestBase::ExpectWindowCountAfterDownload
~~~cpp
static void ExpectWindowCountAfterDownload(size_t expected);
~~~

### EnableFileChooser

DownloadTestBase::EnableFileChooser
~~~cpp
void EnableFileChooser(bool enable);
~~~

### DidShowFileChooser

DownloadTestBase::DidShowFileChooser
~~~cpp
bool DidShowFileChooser();
~~~

### VerifyFile

DownloadTestBase::VerifyFile
~~~cpp
bool VerifyFile(const base::FilePath& path,
                  const std::string& value,
                  const int64_t file_size);
~~~
 Checks that |path| is has |file_size| bytes, and matches the |value|
 string.

### DownloadFilesCheckErrorsSetup

DownloadTestBase::DownloadFilesCheckErrorsSetup
~~~cpp
void DownloadFilesCheckErrorsSetup();
~~~
 Attempts to download a file, based on information in |download_info|.

 If a Select File dialog opens, will automatically choose the default.

### DownloadFilesCheckErrors

DownloadTestBase::DownloadFilesCheckErrors
~~~cpp
void DownloadFilesCheckErrors(size_t count, DownloadInfo* download_info);
~~~
 Attempts to download a set of files, based on information in the
 |download_info| array.  |count| is the number of files.

 If a Select File dialog appears, it will choose the default and return
 immediately.

### DownloadFilesCheckErrorsLoopBody

DownloadTestBase::DownloadFilesCheckErrorsLoopBody
~~~cpp
void DownloadFilesCheckErrorsLoopBody(const DownloadInfo& download_info,
                                        size_t i);
~~~

### DownloadInsertFilesErrorCheckErrors

DownloadTestBase::DownloadInsertFilesErrorCheckErrors
~~~cpp
void DownloadInsertFilesErrorCheckErrors(size_t count,
                                           FileErrorInjectInfo* info);
~~~

### DownloadInsertFilesErrorCheckErrorsLoopBody

DownloadTestBase::DownloadInsertFilesErrorCheckErrorsLoopBody
~~~cpp
void DownloadInsertFilesErrorCheckErrorsLoopBody(
      scoped_refptr<content::TestFileErrorInjector> injector,
      const FileErrorInjectInfo& info,
      size_t i);
~~~

### DownloadFilesToReadonlyFolder

DownloadTestBase::DownloadFilesToReadonlyFolder
~~~cpp
void DownloadFilesToReadonlyFolder(size_t count, DownloadInfo* download_info);
~~~
 Attempts to download a file to a read-only folder, based on information
 in |download_info|.

### StartMockDownloadAndInjectError

DownloadTestBase::StartMockDownloadAndInjectError
~~~cpp
download::DownloadItem* StartMockDownloadAndInjectError(
      content::TestFileErrorInjector* error_injector,
      download::DownloadInterruptReason error);
~~~
 This method:
 * Starts a mock download by navigating to embedded test server URL.

 * Injects |error| on the first write using |error_injector|.

 * Waits for the download to be interrupted.

 * Clears the errors on |error_injector|.

 * Returns the resulting interrupted download.

### test_dir_



~~~cpp

base::FilePath test_dir_;

~~~

 Location of the test data.

### test_response_handler_



~~~cpp

content::TestDownloadResponseHandler test_response_handler_;

~~~


### file_activity_observer_



~~~cpp

std::unique_ptr<DownloadTestFileActivityObserver> file_activity_observer_;

~~~


### ignore_content_verifier_



~~~cpp

extensions::ScopedIgnoreContentVerifierForTest ignore_content_verifier_;

~~~


### ignore_install_verification_



~~~cpp

extensions::ScopedInstallVerifierBypassForTest ignore_install_verification_;

~~~

