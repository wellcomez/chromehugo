
## class DownloadBubbleUIController
 This handles the window-level logic for controlling the download bubble.

 There is one instance of this class per browser window, and it is owned by
 the download toolbar button.

### DownloadBubbleUIController

DownloadBubbleUIController::DownloadBubbleUIController
~~~cpp
explicit DownloadBubbleUIController(Browser* browser);
~~~

### DownloadBubbleUIController

DownloadBubbleUIController::DownloadBubbleUIController
~~~cpp
DownloadBubbleUIController(Browser* browser,
                             DownloadBubbleUpdateService* update_service);
~~~
 Used to inject a custom DownloadBubbleUpdateService for testing. Prefer
 the constructor above which uses that of the profile.

### DownloadBubbleUIController

DownloadBubbleUIController
~~~cpp
DownloadBubbleUIController(const DownloadBubbleUIController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleUIController& operator=(const DownloadBubbleUIController&) =
      delete;
~~~

### ~DownloadBubbleUIController

DownloadBubbleUIController::~DownloadBubbleUIController
~~~cpp
~DownloadBubbleUIController();
~~~

### OnDownloadItemAdded

DownloadBubbleUIController::OnDownloadItemAdded
~~~cpp
void OnDownloadItemAdded(download::DownloadItem* item,
                           bool may_show_animation);
~~~
 These methods are called to notify the UI of new events.

 |may_show_animation| is whether the window this controller belongs to may
 show the animation. (Whether the animation is actually shown may depend on
 the download and the device's graphics capabilities.) We don't show an
 animation for offline items. Notifications for created/added download items
 generally come from the DownloadUIController(Delegate) (except for crx
 downloads, which come via the DownloadBubbleUpdateService), and the rest
 are called from DownloadBubbleUpdateService.

### OnDownloadItemUpdated

DownloadBubbleUIController::OnDownloadItemUpdated
~~~cpp
void OnDownloadItemUpdated(download::DownloadItem* item);
~~~

### OnDownloadItemRemoved

DownloadBubbleUIController::OnDownloadItemRemoved
~~~cpp
void OnDownloadItemRemoved(download::DownloadItem* item);
~~~

### OnOfflineItemsAdded

DownloadBubbleUIController::OnOfflineItemsAdded
~~~cpp
void OnOfflineItemsAdded(
      const OfflineContentProvider::OfflineItemList& items);
~~~

### OnOfflineItemUpdated

DownloadBubbleUIController::OnOfflineItemUpdated
~~~cpp
void OnOfflineItemUpdated(const OfflineItem& item);
~~~

### OnOfflineItemRemoved

DownloadBubbleUIController::OnOfflineItemRemoved
~~~cpp
void OnOfflineItemRemoved(const ContentId& id);
~~~

### GetMainView

DownloadBubbleUIController::GetMainView
~~~cpp
std::vector<DownloadUIModel::DownloadUIModelPtr> GetMainView();
~~~
 Get the entries for the main view of the Download Bubble. The main view
 contains all the recent downloads (finished within the last 24 hours).

### GetPartialView

DownloadBubbleUIController::GetPartialView
~~~cpp
std::vector<DownloadUIModel::DownloadUIModelPtr> GetPartialView();
~~~
 Get the entries for the partial view of the Download Bubble. The partial
 view contains in-progress and uninteracted downloads, meant to capture the
 user's recent tasks. This can only be opened by the browser in the event of
 new downloads, and user action only creates a main view.

### ProcessDownloadButtonPress

DownloadBubbleUIController::ProcessDownloadButtonPress
~~~cpp
void ProcessDownloadButtonPress(DownloadUIModel* model,
                                  DownloadCommands::Command command,
                                  bool is_main_view);
~~~
 Process button press on the bubble.

### HandleButtonPressed

DownloadBubbleUIController::HandleButtonPressed
~~~cpp
void HandleButtonPressed();
~~~
 Notify when a download toolbar button (in any window) is pressed.

### ShouldShowIncognitoIcon

DownloadBubbleUIController::ShouldShowIncognitoIcon
~~~cpp
bool ShouldShowIncognitoIcon(const DownloadUIModel* model) const;
~~~
 Returns whether the incognito icon should be shown for the download.

### ScheduleCancelForEphemeralWarning

DownloadBubbleUIController::ScheduleCancelForEphemeralWarning
~~~cpp
void ScheduleCancelForEphemeralWarning(const std::string& guid);
~~~
 Schedules the ephemeral warning download to be canceled. It will only be
 canceled if it continues to be an ephemeral warning that hasn't been acted
 on when the scheduled time arrives.

### HideDownloadUi

DownloadBubbleUIController::HideDownloadUi
~~~cpp
void HideDownloadUi();
~~~
 Force the controller to hide the download UI entirely, including the bubble
 and the toolbar icon. This function should only be called if the event is
 triggered outside of normal download events that are not listened by
 observers.

### RecordDownloadBubbleInteraction

DownloadBubbleUIController::RecordDownloadBubbleInteraction
~~~cpp
void RecordDownloadBubbleInteraction();
~~~
 Records that the download bubble was interacted with. This only records
 the fact that an interaction occurred, and should not be used
 quantitatively to count the number of such interactions.

### GetDownloadDisplayController

GetDownloadDisplayController
~~~cpp
DownloadDisplayController* GetDownloadDisplayController() {
    return display_controller_;
  }
~~~
 Returns the DownloadDisplayController. Should always return a valid
 controller.

### SetDownloadDisplayController

SetDownloadDisplayController
~~~cpp
void SetDownloadDisplayController(DownloadDisplayController* controller) {
    display_controller_ = controller;
  }
~~~

### update_service

update_service
~~~cpp
DownloadBubbleUpdateService* update_service() { return update_service_; }
~~~

### GetDownloadUIModels

DownloadBubbleUIController::GetDownloadUIModels
~~~cpp
std::vector<DownloadUIModel::DownloadUIModelPtr> GetDownloadUIModels(
      bool is_main_view);
~~~
 Common method for getting main and partial views.

### RetryDownload

DownloadBubbleUIController::RetryDownload
~~~cpp
void RetryDownload(DownloadUIModel* model, DownloadCommands::Command command);
~~~
 Kick off retrying an eligible interrupted download.

### browser_



~~~cpp

raw_ptr<Browser, DanglingUntriaged> browser_;

~~~


### profile_



~~~cpp

raw_ptr<Profile, DanglingUntriaged> profile_;

~~~


### update_service_



~~~cpp

raw_ptr<DownloadBubbleUpdateService> update_service_;

~~~


### offline_manager_



~~~cpp

raw_ptr<OfflineItemModelManager, DanglingUntriaged> offline_manager_;

~~~


### display_controller_



~~~cpp

raw_ptr<DownloadDisplayController, DanglingUntriaged> display_controller_;

~~~

 DownloadDisplayController and DownloadBubbleUIController have the same
 lifetime. Both are owned, constructed together, and destructed together by
 DownloadToolbarButtonView. If one is valid, so is the other.

### last_partial_view_shown_time_



~~~cpp

absl::optional<base::Time> last_partial_view_shown_time_ = absl::nullopt;

~~~


### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadBubbleUIController> weak_factory_{this};

~~~


### DownloadBubbleUIController

DownloadBubbleUIController
~~~cpp
DownloadBubbleUIController(const DownloadBubbleUIController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleUIController& operator=(const DownloadBubbleUIController&) =
      delete;
~~~

### GetDownloadDisplayController

GetDownloadDisplayController
~~~cpp
DownloadDisplayController* GetDownloadDisplayController() {
    return display_controller_;
  }
~~~
 Returns the DownloadDisplayController. Should always return a valid
 controller.

### SetDownloadDisplayController

SetDownloadDisplayController
~~~cpp
void SetDownloadDisplayController(DownloadDisplayController* controller) {
    display_controller_ = controller;
  }
~~~

### update_service

update_service
~~~cpp
DownloadBubbleUpdateService* update_service() { return update_service_; }
~~~

### OnDownloadItemAdded

DownloadBubbleUIController::OnDownloadItemAdded
~~~cpp
void OnDownloadItemAdded(download::DownloadItem* item,
                           bool may_show_animation);
~~~
 These methods are called to notify the UI of new events.

 |may_show_animation| is whether the window this controller belongs to may
 show the animation. (Whether the animation is actually shown may depend on
 the download and the device's graphics capabilities.) We don't show an
 animation for offline items. Notifications for created/added download items
 generally come from the DownloadUIController(Delegate) (except for crx
 downloads, which come via the DownloadBubbleUpdateService), and the rest
 are called from DownloadBubbleUpdateService.

### OnDownloadItemUpdated

DownloadBubbleUIController::OnDownloadItemUpdated
~~~cpp
void OnDownloadItemUpdated(download::DownloadItem* item);
~~~

### OnDownloadItemRemoved

DownloadBubbleUIController::OnDownloadItemRemoved
~~~cpp
void OnDownloadItemRemoved(download::DownloadItem* item);
~~~

### OnOfflineItemsAdded

DownloadBubbleUIController::OnOfflineItemsAdded
~~~cpp
void OnOfflineItemsAdded(
      const OfflineContentProvider::OfflineItemList& items);
~~~

### OnOfflineItemUpdated

DownloadBubbleUIController::OnOfflineItemUpdated
~~~cpp
void OnOfflineItemUpdated(const OfflineItem& item);
~~~

### OnOfflineItemRemoved

DownloadBubbleUIController::OnOfflineItemRemoved
~~~cpp
void OnOfflineItemRemoved(const ContentId& id);
~~~

### GetMainView

DownloadBubbleUIController::GetMainView
~~~cpp
std::vector<DownloadUIModel::DownloadUIModelPtr> GetMainView();
~~~
 Get the entries for the main view of the Download Bubble. The main view
 contains all the recent downloads (finished within the last 24 hours).

### GetPartialView

DownloadBubbleUIController::GetPartialView
~~~cpp
std::vector<DownloadUIModel::DownloadUIModelPtr> GetPartialView();
~~~
 Get the entries for the partial view of the Download Bubble. The partial
 view contains in-progress and uninteracted downloads, meant to capture the
 user's recent tasks. This can only be opened by the browser in the event of
 new downloads, and user action only creates a main view.

### ProcessDownloadButtonPress

DownloadBubbleUIController::ProcessDownloadButtonPress
~~~cpp
void ProcessDownloadButtonPress(DownloadUIModel* model,
                                  DownloadCommands::Command command,
                                  bool is_main_view);
~~~
 Process button press on the bubble.

### HandleButtonPressed

DownloadBubbleUIController::HandleButtonPressed
~~~cpp
void HandleButtonPressed();
~~~
 Notify when a download toolbar button (in any window) is pressed.

### ShouldShowIncognitoIcon

DownloadBubbleUIController::ShouldShowIncognitoIcon
~~~cpp
bool ShouldShowIncognitoIcon(const DownloadUIModel* model) const;
~~~
 Returns whether the incognito icon should be shown for the download.

### ScheduleCancelForEphemeralWarning

DownloadBubbleUIController::ScheduleCancelForEphemeralWarning
~~~cpp
void ScheduleCancelForEphemeralWarning(const std::string& guid);
~~~
 Schedules the ephemeral warning download to be canceled. It will only be
 canceled if it continues to be an ephemeral warning that hasn't been acted
 on when the scheduled time arrives.

### HideDownloadUi

DownloadBubbleUIController::HideDownloadUi
~~~cpp
void HideDownloadUi();
~~~
 Force the controller to hide the download UI entirely, including the bubble
 and the toolbar icon. This function should only be called if the event is
 triggered outside of normal download events that are not listened by
 observers.

### RecordDownloadBubbleInteraction

DownloadBubbleUIController::RecordDownloadBubbleInteraction
~~~cpp
void RecordDownloadBubbleInteraction();
~~~
 Records that the download bubble was interacted with. This only records
 the fact that an interaction occurred, and should not be used
 quantitatively to count the number of such interactions.

### GetDownloadUIModels

DownloadBubbleUIController::GetDownloadUIModels
~~~cpp
std::vector<DownloadUIModel::DownloadUIModelPtr> GetDownloadUIModels(
      bool is_main_view);
~~~
 Common method for getting main and partial views.

### RetryDownload

DownloadBubbleUIController::RetryDownload
~~~cpp
void RetryDownload(DownloadUIModel* model, DownloadCommands::Command command);
~~~
 Kick off retrying an eligible interrupted download.

### browser_



~~~cpp

raw_ptr<Browser, DanglingUntriaged> browser_;

~~~


### profile_



~~~cpp

raw_ptr<Profile, DanglingUntriaged> profile_;

~~~


### update_service_



~~~cpp

raw_ptr<DownloadBubbleUpdateService> update_service_;

~~~


### offline_manager_



~~~cpp

raw_ptr<OfflineItemModelManager, DanglingUntriaged> offline_manager_;

~~~


### display_controller_



~~~cpp

raw_ptr<DownloadDisplayController, DanglingUntriaged> display_controller_;

~~~

 DownloadDisplayController and DownloadBubbleUIController have the same
 lifetime. Both are owned, constructed together, and destructed together by
 DownloadToolbarButtonView. If one is valid, so is the other.

### last_partial_view_shown_time_



~~~cpp

absl::optional<base::Time> last_partial_view_shown_time_ = absl::nullopt;

~~~


### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadBubbleUIController> weak_factory_{this};

~~~

