
## class DownloadDisplayController
 Used to control the DownloadToolbar Button, through the DownloadDisplay
 interface. Supports both regular Download and Offline items. When in the
 future OfflineItems include regular Download on Desktop platforms,
 we can remove AllDownloadItemNotifier::Observer.

 TODO(chlily): Consolidate this with DownloadBubbleUIController.

### DownloadDisplayController

DownloadDisplayController::DownloadDisplayController
~~~cpp
DownloadDisplayController(DownloadDisplay* display,
                            Browser* browser,
                            DownloadBubbleUIController* bubble_controller);
~~~

### DownloadDisplayController

DownloadDisplayController
~~~cpp
DownloadDisplayController(const DownloadDisplayController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDisplayController& operator=(const DownloadDisplayController&) =
      delete;
~~~

### ~DownloadDisplayController

DownloadDisplayController::~DownloadDisplayController
~~~cpp
~DownloadDisplayController() override;
~~~

###  

 Information extracted from iterating over all models, to avoid having to do
 so multiple times.

~~~cpp
struct AllDownloadUIModelsInfo {
    // Number of models that would be returned to display.
    size_t all_models_size = 0;
    // The last time that a download was completed. Will be null if no downloads
    // were completed.
    base::Time last_completed_time;
    // Whether there are any downloads actively doing deep scanning.
    bool has_deep_scanning = false;
    // Whether any downloads are unactioned.
    bool has_unactioned = false;
    // From the button UI's perspective, whether the download is considered in
    // progress. Consider dangerous downloads as completed, because we don't
    // want to encourage users to interact with them.
    int in_progress_count = 0;
    // Count of in-progress downloads (by the above definition) that are paused.
    int paused_count = 0;
  };
~~~
### field error



~~~cpp

struct ProgressInfo {
    bool progress_certain = true;
    int progress_percentage = 0;
    int download_count = 0;
  };

~~~


### field error



~~~cpp

struct IconInfo {
    download::DownloadIconState icon_state =
        download::DownloadIconState::kComplete;
    bool is_active = false;
  };

~~~


### GetProgress

DownloadDisplayController::GetProgress
~~~cpp
ProgressInfo GetProgress();
~~~
 Returns a ProgressInfo where |download_count| is the number of currently
 active downloads. If we know the final size of all downloads,
 |progress_certain| is true. |progress_percentage| is the percentage
 complete of all in-progress downloads. Forwards to the
 DownloadBubbleUpdateService.

### GetIconInfo

DownloadDisplayController::GetIconInfo
~~~cpp
IconInfo GetIconInfo();
~~~
 Returns an IconInfo that contains current state of the icon.

### IsDisplayShowingDetails

DownloadDisplayController::IsDisplayShowingDetails
~~~cpp
bool IsDisplayShowingDetails();
~~~
 Returns whether the display is showing details.

### OnButtonPressed

DownloadDisplayController::OnButtonPressed
~~~cpp
void OnButtonPressed();
~~~
 Notifies the controller that the button is pressed. Called by `display_`.

### HandleButtonPressed

DownloadDisplayController::HandleButtonPressed
~~~cpp
void HandleButtonPressed();
~~~
 Handles the button pressed event. Called by the profile level controller.

### OnNewItem

DownloadDisplayController::OnNewItem
~~~cpp
virtual void OnNewItem(bool show_animation);
~~~
 Common methods for new downloads or new offline items.

 These methods are virtual so that they can be overridden for fake
 controllers in testing.

 Called from bubble controller when new item(s) are added.

 |show_animation| specifies whether a small animated arrow should be shown.

### OnUpdatedItem

DownloadDisplayController::OnUpdatedItem
~~~cpp
virtual void OnUpdatedItem(bool is_done,
                             bool is_pending_deep_scanning,
                             bool may_show_details);
~~~
 Called from bubble controller when an item is updated, with |is_done|
 indicating if it was marked done, |is_pending_deep_scanning| indicating
 whether it is dangerous and pending deep scanning, and with
 |may_show_details| indicating whether the partial view can be shown.

 (Whether the partial view is actually shown may depend on the state of the
 other downloads.)
### OnRemovedItem

DownloadDisplayController::OnRemovedItem
~~~cpp
virtual void OnRemovedItem(const ContentId& id);
~~~
 Called from bubble controller when an item is deleted.

### HideToolbarButton

DownloadDisplayController::HideToolbarButton
~~~cpp
void HideToolbarButton();
~~~
 Asks `display_` to hide the toolbar button. Does nothing if the toolbar
 button is already hidden.

### HideBubble

DownloadDisplayController::HideBubble
~~~cpp
void HideBubble();
~~~
 Asks `display_` to hide the toolbar button details. Does nothing if the
 details are already hidden.

### ListenToFullScreenChanges

DownloadDisplayController::ListenToFullScreenChanges
~~~cpp
void ListenToFullScreenChanges();
~~~
 Start listening to full screen changes. This is separate from the
 constructor as the exclusive access manager is constructed after
 BrowserWindow.

### OnFullscreenStateChanged

DownloadDisplayController::OnFullscreenStateChanged
~~~cpp
void OnFullscreenStateChanged() override;
~~~
 FullScreenObserver
### OnResume

DownloadDisplayController::OnResume
~~~cpp
void OnResume() override;
~~~
 PowerSuspendObserver
### download_display_for_testing

download_display_for_testing
~~~cpp
DownloadDisplay* download_display_for_testing() { return display_; }
~~~
 Returns the DownloadDisplay. Should always return a valid display.

###  UpdateButtonStateFromAllModelsInfo

 Gets info about all models to display, then updates the toolbar button
 state accordingly. Returns the info about all models.

~~~cpp
const AllDownloadUIModelsInfo& UpdateButtonStateFromAllModelsInfo();
~~~
### ScheduleToolbarDisappearance

DownloadDisplayController::ScheduleToolbarDisappearance
~~~cpp
void ScheduleToolbarDisappearance(base::TimeDelta interval);
~~~
 Stops and restarts `icon_disappearance_timer_`. The toolbar button will
 be hidden after the `interval`.

### ScheduleToolbarInactive

DownloadDisplayController::ScheduleToolbarInactive
~~~cpp
void ScheduleToolbarInactive(base::TimeDelta interval);
~~~
 Stops and restarts `icon_inactive_timer_`. The toolbar button will
 be changed to inactive state after the `interval`.

### ShowToolbarButton

DownloadDisplayController::ShowToolbarButton
~~~cpp
void ShowToolbarButton();
~~~
 Asks `display_` to show the toolbar button. Does nothing if the toolbar
 button is already showing.

### UpdateToolbarButtonState

DownloadDisplayController::UpdateToolbarButtonState
~~~cpp
void UpdateToolbarButtonState(const AllDownloadUIModelsInfo& info);
~~~
 Updates the icon state of the `display_`.

### UpdateDownloadIconToInactive

DownloadDisplayController::UpdateDownloadIconToInactive
~~~cpp
void UpdateDownloadIconToInactive();
~~~
 Asks `display_` to make the download icon inactive.

### MaybeShowButtonWhenCreated

DownloadDisplayController::MaybeShowButtonWhenCreated
~~~cpp
virtual void MaybeShowButtonWhenCreated();
~~~
 Decides whether the toolbar button should be shown when it is created.

### HasRecentCompleteDownload

DownloadDisplayController::HasRecentCompleteDownload
~~~cpp
bool HasRecentCompleteDownload(base::TimeDelta interval,
                                 base::Time last_complete_time);
~~~
 Whether the last download complete time is less than `interval` ago.

### GetLastCompleteTime

DownloadDisplayController::GetLastCompleteTime
~~~cpp
base::Time GetLastCompleteTime(
      base::Time last_completed_time_from_current_models) const;
~~~

### display_



~~~cpp

raw_ptr<DownloadDisplay> const display_;

~~~

 The pointer is created in ToolbarView and owned by ToolbarView.

### browser_



~~~cpp

raw_ptr<Browser> browser_;

~~~


### observation_



~~~cpp

base::ScopedObservation<FullscreenController, FullscreenObserver>
      observation_{this};

~~~


### icon_disappearance_timer_



~~~cpp

base::OneShotTimer icon_disappearance_timer_;

~~~


### icon_inactive_timer_



~~~cpp

base::OneShotTimer icon_inactive_timer_;

~~~


### icon_info_



~~~cpp

IconInfo icon_info_;

~~~


### fullscreen_notification_shown_



~~~cpp

bool fullscreen_notification_shown_ = false;

~~~


### details_shown_while_fullscreen_



~~~cpp

bool details_shown_while_fullscreen_ = false;

~~~


### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_;

~~~

 DownloadDisplayController and DownloadBubbleUIController have the same
 lifetime. Both are owned, constructed together, and destructed together by
 DownloadToolbarButtonView. If one is valid, so is the other.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadDisplayController> weak_factory_{this};

~~~


### DownloadDisplayController

DownloadDisplayController
~~~cpp
DownloadDisplayController(const DownloadDisplayController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadDisplayController& operator=(const DownloadDisplayController&) =
      delete;
~~~

### download_display_for_testing

download_display_for_testing
~~~cpp
DownloadDisplay* download_display_for_testing() { return display_; }
~~~
 Returns the DownloadDisplay. Should always return a valid display.

###  

 Information extracted from iterating over all models, to avoid having to do
 so multiple times.

~~~cpp
struct AllDownloadUIModelsInfo {
    // Number of models that would be returned to display.
    size_t all_models_size = 0;
    // The last time that a download was completed. Will be null if no downloads
    // were completed.
    base::Time last_completed_time;
    // Whether there are any downloads actively doing deep scanning.
    bool has_deep_scanning = false;
    // Whether any downloads are unactioned.
    bool has_unactioned = false;
    // From the button UI's perspective, whether the download is considered in
    // progress. Consider dangerous downloads as completed, because we don't
    // want to encourage users to interact with them.
    int in_progress_count = 0;
    // Count of in-progress downloads (by the above definition) that are paused.
    int paused_count = 0;
  };
~~~
### field error



~~~cpp

struct ProgressInfo {
    bool progress_certain = true;
    int progress_percentage = 0;
    int download_count = 0;
  };

~~~


### field error



~~~cpp

struct IconInfo {
    download::DownloadIconState icon_state =
        download::DownloadIconState::kComplete;
    bool is_active = false;
  };

~~~


### GetProgress

DownloadDisplayController::GetProgress
~~~cpp
ProgressInfo GetProgress();
~~~
 Returns a ProgressInfo where |download_count| is the number of currently
 active downloads. If we know the final size of all downloads,
 |progress_certain| is true. |progress_percentage| is the percentage
 complete of all in-progress downloads. Forwards to the
 DownloadBubbleUpdateService.

### GetIconInfo

DownloadDisplayController::GetIconInfo
~~~cpp
IconInfo GetIconInfo();
~~~
 Returns an IconInfo that contains current state of the icon.

### IsDisplayShowingDetails

DownloadDisplayController::IsDisplayShowingDetails
~~~cpp
bool IsDisplayShowingDetails();
~~~
 Returns whether the display is showing details.

### OnButtonPressed

DownloadDisplayController::OnButtonPressed
~~~cpp
void OnButtonPressed();
~~~
 Notifies the controller that the button is pressed. Called by `display_`.

### HandleButtonPressed

DownloadDisplayController::HandleButtonPressed
~~~cpp
void HandleButtonPressed();
~~~
 Handles the button pressed event. Called by the profile level controller.

### OnNewItem

DownloadDisplayController::OnNewItem
~~~cpp
virtual void OnNewItem(bool show_animation);
~~~
 Common methods for new downloads or new offline items.

 These methods are virtual so that they can be overridden for fake
 controllers in testing.

 Called from bubble controller when new item(s) are added.

 |show_animation| specifies whether a small animated arrow should be shown.

### OnUpdatedItem

DownloadDisplayController::OnUpdatedItem
~~~cpp
virtual void OnUpdatedItem(bool is_done,
                             bool is_pending_deep_scanning,
                             bool may_show_details);
~~~
 Called from bubble controller when an item is updated, with |is_done|
 indicating if it was marked done, |is_pending_deep_scanning| indicating
 whether it is dangerous and pending deep scanning, and with
 |may_show_details| indicating whether the partial view can be shown.

 (Whether the partial view is actually shown may depend on the state of the
 other downloads.)
### OnRemovedItem

DownloadDisplayController::OnRemovedItem
~~~cpp
virtual void OnRemovedItem(const ContentId& id);
~~~
 Called from bubble controller when an item is deleted.

### HideToolbarButton

DownloadDisplayController::HideToolbarButton
~~~cpp
void HideToolbarButton();
~~~
 Asks `display_` to hide the toolbar button. Does nothing if the toolbar
 button is already hidden.

### HideBubble

DownloadDisplayController::HideBubble
~~~cpp
void HideBubble();
~~~
 Asks `display_` to hide the toolbar button details. Does nothing if the
 details are already hidden.

### ListenToFullScreenChanges

DownloadDisplayController::ListenToFullScreenChanges
~~~cpp
void ListenToFullScreenChanges();
~~~
 Start listening to full screen changes. This is separate from the
 constructor as the exclusive access manager is constructed after
 BrowserWindow.

### OnFullscreenStateChanged

DownloadDisplayController::OnFullscreenStateChanged
~~~cpp
void OnFullscreenStateChanged() override;
~~~
 FullScreenObserver
### OnResume

DownloadDisplayController::OnResume
~~~cpp
void OnResume() override;
~~~
 PowerSuspendObserver
###  UpdateButtonStateFromAllModelsInfo

 Gets info about all models to display, then updates the toolbar button
 state accordingly. Returns the info about all models.

~~~cpp
const AllDownloadUIModelsInfo& UpdateButtonStateFromAllModelsInfo();
~~~
### ScheduleToolbarDisappearance

DownloadDisplayController::ScheduleToolbarDisappearance
~~~cpp
void ScheduleToolbarDisappearance(base::TimeDelta interval);
~~~
 Stops and restarts `icon_disappearance_timer_`. The toolbar button will
 be hidden after the `interval`.

### ScheduleToolbarInactive

DownloadDisplayController::ScheduleToolbarInactive
~~~cpp
void ScheduleToolbarInactive(base::TimeDelta interval);
~~~
 Stops and restarts `icon_inactive_timer_`. The toolbar button will
 be changed to inactive state after the `interval`.

### ShowToolbarButton

DownloadDisplayController::ShowToolbarButton
~~~cpp
void ShowToolbarButton();
~~~
 Asks `display_` to show the toolbar button. Does nothing if the toolbar
 button is already showing.

### UpdateToolbarButtonState

DownloadDisplayController::UpdateToolbarButtonState
~~~cpp
void UpdateToolbarButtonState(const AllDownloadUIModelsInfo& info);
~~~
 Updates the icon state of the `display_`.

### UpdateDownloadIconToInactive

DownloadDisplayController::UpdateDownloadIconToInactive
~~~cpp
void UpdateDownloadIconToInactive();
~~~
 Asks `display_` to make the download icon inactive.

### MaybeShowButtonWhenCreated

DownloadDisplayController::MaybeShowButtonWhenCreated
~~~cpp
virtual void MaybeShowButtonWhenCreated();
~~~
 Decides whether the toolbar button should be shown when it is created.

### HasRecentCompleteDownload

DownloadDisplayController::HasRecentCompleteDownload
~~~cpp
bool HasRecentCompleteDownload(base::TimeDelta interval,
                                 base::Time last_complete_time);
~~~
 Whether the last download complete time is less than `interval` ago.

### GetLastCompleteTime

DownloadDisplayController::GetLastCompleteTime
~~~cpp
base::Time GetLastCompleteTime(
      base::Time last_completed_time_from_current_models) const;
~~~

### display_



~~~cpp

raw_ptr<DownloadDisplay> const display_;

~~~

 The pointer is created in ToolbarView and owned by ToolbarView.

### browser_



~~~cpp

raw_ptr<Browser> browser_;

~~~


### observation_



~~~cpp

base::ScopedObservation<FullscreenController, FullscreenObserver>
      observation_{this};

~~~


### icon_disappearance_timer_



~~~cpp

base::OneShotTimer icon_disappearance_timer_;

~~~


### icon_inactive_timer_



~~~cpp

base::OneShotTimer icon_inactive_timer_;

~~~


### icon_info_



~~~cpp

IconInfo icon_info_;

~~~


### fullscreen_notification_shown_



~~~cpp

bool fullscreen_notification_shown_ = false;

~~~


### details_shown_while_fullscreen_



~~~cpp

bool details_shown_while_fullscreen_ = false;

~~~


### bubble_controller_



~~~cpp

raw_ptr<DownloadBubbleUIController> bubble_controller_;

~~~

 DownloadDisplayController and DownloadBubbleUIController have the same
 lifetime. Both are owned, constructed together, and destructed together by
 DownloadToolbarButtonView. If one is valid, so is the other.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadDisplayController> weak_factory_{this};

~~~

