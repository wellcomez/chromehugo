
## class DownloadDisplay

### Show

Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows the download display.

### Hide

Hide
~~~cpp
virtual void Hide() = 0;
~~~
 Hides the download display.

### IsShowing

IsShowing
~~~cpp
virtual bool IsShowing() = 0;
~~~
 Returns whether or not the download display is visible.

### Enable

Enable
~~~cpp
virtual void Enable() = 0;
~~~
 Enables potential actions resulting from clicking the download display.

### Disable

Disable
~~~cpp
virtual void Disable() = 0;
~~~
 Disables potential actions resulting from clicking the download display.

### UpdateDownloadIcon

UpdateDownloadIcon
~~~cpp
virtual void UpdateDownloadIcon(bool show_animation) = 0;
~~~
 Updates the download icon.

 If |show_animation| is true, an animated icon will be shown.

### ShowDetails

ShowDetails
~~~cpp
virtual void ShowDetails() = 0;
~~~
 Shows detailed information on the download display. It can be a popup or
 dialog or partial view, essentially anything other than the main view.

### HideDetails

HideDetails
~~~cpp
virtual void HideDetails() = 0;
~~~
 Hide the detailed information on the download display.

### IsShowingDetails

IsShowingDetails
~~~cpp
virtual bool IsShowingDetails() = 0;
~~~
 Returns whether the details are visible.

### IsFullscreenWithParentViewHidden

IsFullscreenWithParentViewHidden
~~~cpp
virtual bool IsFullscreenWithParentViewHidden() = 0;
~~~
 Returns whether it is currently in fullscreen and the view that hosts the
 download display is hidden.

### ~DownloadDisplay

DownloadDisplay::~DownloadDisplay
~~~cpp
virtual ~DownloadDisplay();
~~~

### Show

Show
~~~cpp
virtual void Show() = 0;
~~~
 Shows the download display.

### Hide

Hide
~~~cpp
virtual void Hide() = 0;
~~~
 Hides the download display.

### IsShowing

IsShowing
~~~cpp
virtual bool IsShowing() = 0;
~~~
 Returns whether or not the download display is visible.

### Enable

Enable
~~~cpp
virtual void Enable() = 0;
~~~
 Enables potential actions resulting from clicking the download display.

### Disable

Disable
~~~cpp
virtual void Disable() = 0;
~~~
 Disables potential actions resulting from clicking the download display.

### UpdateDownloadIcon

UpdateDownloadIcon
~~~cpp
virtual void UpdateDownloadIcon(bool show_animation) = 0;
~~~
 Updates the download icon.

 If |show_animation| is true, an animated icon will be shown.

### ShowDetails

ShowDetails
~~~cpp
virtual void ShowDetails() = 0;
~~~
 Shows detailed information on the download display. It can be a popup or
 dialog or partial view, essentially anything other than the main view.

### HideDetails

HideDetails
~~~cpp
virtual void HideDetails() = 0;
~~~
 Hide the detailed information on the download display.

### IsShowingDetails

IsShowingDetails
~~~cpp
virtual bool IsShowingDetails() = 0;
~~~
 Returns whether the details are visible.

### IsFullscreenWithParentViewHidden

IsFullscreenWithParentViewHidden
~~~cpp
virtual bool IsFullscreenWithParentViewHidden() = 0;
~~~
 Returns whether it is currently in fullscreen and the view that hosts the
 download display is hidden.
