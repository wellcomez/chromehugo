### GetItemStartTime

GetItemStartTime
~~~cpp
base::Time GetItemStartTime(const download::DownloadItem* item);
~~~

### GetItemStartTime

GetItemStartTime
~~~cpp
base::Time GetItemStartTime(const offline_items_collection::OfflineItem& item);
~~~

### GetItemId

GetItemId
~~~cpp
const std::string& GetItemId(const download::DownloadItem* item);
~~~

### GetItemId

GetItemId
~~~cpp
const offline_items_collection::ContentId& GetItemId(
    const offline_items_collection::OfflineItem& item);
~~~

### ItemIsRecent

ItemIsRecent
~~~cpp
bool ItemIsRecent(const download::DownloadItem* item, base::Time cutoff_time);
~~~
 Whether the download is more recent than |cutoff_time|.

### ItemIsRecent

ItemIsRecent
~~~cpp
bool ItemIsRecent(const offline_items_collection::OfflineItem& item,
                  base::Time cutoff_time);
~~~

### DownloadUIModelIsRecent

DownloadUIModelIsRecent
~~~cpp
bool DownloadUIModelIsRecent(const DownloadUIModel* model,
                             base::Time cutoff_time);
~~~

### IsPendingDeepScanning

IsPendingDeepScanning
~~~cpp
bool IsPendingDeepScanning(const download::DownloadItem* item);
~~~
 Whether the download is in progress and pending deep scanning.

### IsPendingDeepScanning

IsPendingDeepScanning
~~~cpp
bool IsPendingDeepScanning(const DownloadUIModel* model);
~~~

### IsItemInProgress

IsItemInProgress
~~~cpp
bool IsItemInProgress(const download::DownloadItem* item);
~~~
 Whether the download is considered in-progress from the UI's point of view.

 Consider dangerous downloads as completed, because we don't want to encourage
 users to interact with them. However, consider downloads pending scanning as
 in progress, because we do want users to scan potential dangerous downloads.

 Items that are paused count as in-progress.

### IsItemInProgress

IsItemInProgress
~~~cpp
bool IsItemInProgress(const offline_items_collection::OfflineItem& item);
~~~

### IsModelInProgress

IsModelInProgress
~~~cpp
bool IsModelInProgress(const DownloadUIModel* model);
~~~

### IsItemPaused

IsItemPaused
~~~cpp
bool IsItemPaused(const download::DownloadItem* item);
~~~
 Whether the item is paused.

### IsItemPaused

IsItemPaused
~~~cpp
bool IsItemPaused(const offline_items_collection::OfflineItem& item);
~~~

### FindBrowserToShowAnimation

FindBrowserToShowAnimation
~~~cpp
Browser* FindBrowserToShowAnimation(download::DownloadItem* item,
                                    Profile* profile);
~~~
 Finds the browser most appropriate to show the "download started" animation
 in.

