
## class DownloadBubbleUpdateServiceFactory

### DownloadBubbleUpdateServiceFactory

DownloadBubbleUpdateServiceFactory
~~~cpp
DownloadBubbleUpdateServiceFactory(
      const DownloadBubbleUpdateServiceFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleUpdateServiceFactory& operator=(
      const DownloadBubbleUpdateServiceFactory&) = delete;
~~~

### GetInstance

DownloadBubbleUpdateServiceFactory::GetInstance
~~~cpp
static DownloadBubbleUpdateServiceFactory* GetInstance();
~~~
 Returns the singleton instance of DownloadBubbleUpdateServiceFactory.

### GetForProfile

DownloadBubbleUpdateServiceFactory::GetForProfile
~~~cpp
static DownloadBubbleUpdateService* GetForProfile(Profile* profile);
~~~
 Returns the DownloadBubbleUpdateService associated with |profile|.

### DownloadBubbleUpdateServiceFactory

DownloadBubbleUpdateServiceFactory::DownloadBubbleUpdateServiceFactory
~~~cpp
DownloadBubbleUpdateServiceFactory();
~~~

### ~DownloadBubbleUpdateServiceFactory

DownloadBubbleUpdateServiceFactory::~DownloadBubbleUpdateServiceFactory
~~~cpp
~DownloadBubbleUpdateServiceFactory() override;
~~~

### BuildServiceInstanceFor

DownloadBubbleUpdateServiceFactory::BuildServiceInstanceFor
~~~cpp
KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
~~~
 BrowserContextKeyedServiceFactory:
### DownloadBubbleUpdateServiceFactory

DownloadBubbleUpdateServiceFactory
~~~cpp
DownloadBubbleUpdateServiceFactory(
      const DownloadBubbleUpdateServiceFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleUpdateServiceFactory& operator=(
      const DownloadBubbleUpdateServiceFactory&) = delete;
~~~

### GetInstance

DownloadBubbleUpdateServiceFactory::GetInstance
~~~cpp
static DownloadBubbleUpdateServiceFactory* GetInstance();
~~~
 Returns the singleton instance of DownloadBubbleUpdateServiceFactory.

### GetForProfile

DownloadBubbleUpdateServiceFactory::GetForProfile
~~~cpp
static DownloadBubbleUpdateService* GetForProfile(Profile* profile);
~~~
 Returns the DownloadBubbleUpdateService associated with |profile|.

### BuildServiceInstanceFor

DownloadBubbleUpdateServiceFactory::BuildServiceInstanceFor
~~~cpp
KeyedService* BuildServiceInstanceFor(
      content::BrowserContext* context) const override;
~~~
 BrowserContextKeyedServiceFactory: