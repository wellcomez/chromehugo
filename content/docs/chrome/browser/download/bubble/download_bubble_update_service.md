
## class DownloadBubbleUpdateService
 namespace download
 Caches download items and offline items in sorted order, so that UI updates
 can be processed more quickly without fetching and sorting all items every
 time. Passes notifications on to the window-level UI controllers.

###  operator<

 Defines sort priority for items.

~~~cpp
struct ItemSortKey {
    enum State {
      kInProgressActive = 0,
      kInProgressPaused = 1,
      kNotInProgress = 2,
    };

    bool operator<(const ItemSortKey& other) const;
    bool operator==(const ItemSortKey& other) const;
    bool operator!=(const ItemSortKey& other) const;
    bool operator>(const ItemSortKey& other) const;

    // Active in-progress items come before paused items, which come before
    // not-in-progress items.
    State state;
    // Within each state, items are sorted in reverse chronological order by
    // start time (most recent first).
    base::Time start_time;
  };
~~~
### DownloadBubbleUpdateService

DownloadBubbleUpdateService::DownloadBubbleUpdateService
~~~cpp
explicit DownloadBubbleUpdateService(Profile* profile);
~~~

### DownloadBubbleUpdateService

DownloadBubbleUpdateService
~~~cpp
DownloadBubbleUpdateService(const DownloadBubbleUpdateService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleUpdateService& operator=(const DownloadBubbleUpdateService&) =
      delete;
~~~

### ~DownloadBubbleUpdateService

DownloadBubbleUpdateService::~DownloadBubbleUpdateService
~~~cpp
~DownloadBubbleUpdateService() override;
~~~

### GetAllModelsToDisplay

DownloadBubbleUpdateService::GetAllModelsToDisplay
~~~cpp
virtual bool GetAllModelsToDisplay(
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models,
      bool force_backfill_download_items = false);
~~~
 Gets models for the top GetMaxNumItemsToShow() combined download items
 and offline items, in sorted order. May cause items to be pruned from the
 cache, if they have grown too old to be included. May trigger backfilling
 the caches, but does not wait for backfill results, unless
 |force_backfill_download_items| is true (in which case download items will
 be backfilled synchronously if necessary; offline items will not be
 backfilled synchronously). |models| is cleared. Returns whether results are
 complete. Results may not be complete if there might be more items to be
 returned after backfilling. Virtual for testing.

###  GetAllModelsInfo

 Returns information relevant to the display state of the download button.

 Does not prune the cache or backfill missing items. May be slightly
 inaccurate in edge cases. Virtual for testing.

~~~cpp
virtual const DownloadDisplayController::AllDownloadUIModelsInfo&
  GetAllModelsInfo();
~~~
### GetProgressInfo

DownloadBubbleUpdateService::GetProgressInfo
~~~cpp
virtual DownloadDisplayController::ProgressInfo GetProgressInfo() const;
~~~
 Computes progress info based on in-progress downloads. Does not prune the
 cache or backfill missing items, so the returned progress info may be
 slightly inaccurate in edge cases. This is ok, as it is only for the
 purpose of showing a progress ring around the icon, which is not precise
 anyway. Virtual for testing.

### Initialize

DownloadBubbleUpdateService::Initialize
~~~cpp
void Initialize(content::DownloadManager* manager);
~~~
 Initializes AllDownloadItemNotifier for the current profile, and
 initializes caches. This is called when the manager is ready, to signal
 that the DownloadBubbleUpdateService should begin tracking downloads. This
 starts initialization of both the download items and the offline items.

 Should only be called once.

### InitializeOriginalNotifier

DownloadBubbleUpdateService::InitializeOriginalNotifier
~~~cpp
void InitializeOriginalNotifier(content::DownloadManager* manager);
~~~
 Initializes the AllDownloadItemNotifier for the original profile, if
 |profile_| is off the record. May trigger re-initialization of the download
 items cache.

### GetDownloadManager

DownloadBubbleUpdateService::GetDownloadManager
~~~cpp
content::DownloadManager* GetDownloadManager();
~~~
 Get the DownloadManager that |download_item_notifier_| is listening to.

### IsInitialized

DownloadBubbleUpdateService::IsInitialized
~~~cpp
virtual bool IsInitialized() const;
~~~
 Virtual for testing.

### Shutdown

DownloadBubbleUpdateService::Shutdown
~~~cpp
void Shutdown() override;
~~~
 KeyedService:
### OnDownloadCreated

DownloadBubbleUpdateService::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~
 download::AllDownloadItemNotifier::Observer:
### OnDownloadUpdated

DownloadBubbleUpdateService::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadRemoved

DownloadBubbleUpdateService::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnManagerGoingDown

DownloadBubbleUpdateService::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnItemsAdded

DownloadBubbleUpdateService::OnItemsAdded
~~~cpp
void OnItemsAdded(
      const offline_items_collection::OfflineContentProvider::OfflineItemList&
          items) override;
~~~
 offline_items_collection::OfflineContentProvider::Observer:
### OnItemRemoved

DownloadBubbleUpdateService::OnItemRemoved
~~~cpp
void OnItemRemoved(const offline_items_collection::ContentId& id) override;
~~~

### OnItemUpdated

DownloadBubbleUpdateService::OnItemUpdated
~~~cpp
void OnItemUpdated(
      const offline_items_collection::OfflineItem& item,
      const absl::optional<offline_items_collection::UpdateDelta>& update_delta)
      override;
~~~

### OnContentProviderGoingDown

DownloadBubbleUpdateService::OnContentProviderGoingDown
~~~cpp
void OnContentProviderGoingDown() override;
~~~

### set_max_num_items_to_show_for_testing

set_max_num_items_to_show_for_testing
~~~cpp
void set_max_num_items_to_show_for_testing(size_t max) {
    max_num_items_to_show_for_testing_ = max;
  }
~~~
 Logic in this class assumes the max is at least 2.

### set_extra_items_to_cache_for_testing

set_extra_items_to_cache_for_testing
~~~cpp
void set_extra_items_to_cache_for_testing(size_t items) {
    extra_items_to_cache_for_testing_ = items;
  }
~~~

### download_item_notifier_for_testing

download_item_notifier_for_testing
~~~cpp
download::AllDownloadItemNotifier& download_item_notifier_for_testing() {
    return *download_item_notifier_;
  }
~~~

### original_download_item_notifier_for_testing

original_download_item_notifier_for_testing
~~~cpp
download::AllDownloadItemNotifier&
  original_download_item_notifier_for_testing() {
    return *original_download_item_notifier_;
  }
~~~

### GetMaxNumItemsToShow

DownloadBubbleUpdateService::GetMaxNumItemsToShow
~~~cpp
size_t GetMaxNumItemsToShow() const;
~~~
 Returns the max number of combined download items and offline items that
 will be returned from GetAllModelsToDisplay().

### GetNumItemsToCache

DownloadBubbleUpdateService::GetNumItemsToCache
~~~cpp
size_t GetNumItemsToCache() const;
~~~
 Returns the max number of items (of each type) to cache. This is slightly
 more than the max number of items to show.

### IsCacheAtMax

DownloadBubbleUpdateService::IsCacheAtMax
~~~cpp
bool IsCacheAtMax(const SortedItems<Item>& cache);
~~~

### MaybeAddDownloadItemToCache

DownloadBubbleUpdateService::MaybeAddDownloadItemToCache
~~~cpp
bool MaybeAddDownloadItemToCache(download::DownloadItem* item, bool is_new);
~~~
 Adds an item to the cache if it is recent enough and meets other criteria
 for showing in the bubble. If adding an item makes the map size exceed the
 maximum, this removes excess items from the end of the map. Returns whether
 the item was stored as the last item in the map. If |item| was already in
 the cache, this does nothing. |is_new| is whether the item is a newly added
 item (as opposed to an updated one). May mark the item model as
 not-actioned-on if the item is new.

### MaybeAddOfflineItemToCache

DownloadBubbleUpdateService::MaybeAddOfflineItemToCache
~~~cpp
bool MaybeAddOfflineItemToCache(
      const offline_items_collection::OfflineItem& item,
      bool is_new);
~~~

### AddItemToCacheImpl

DownloadBubbleUpdateService::AddItemToCacheImpl
~~~cpp
bool AddItemToCacheImpl(Item item,
                          SortedItems<Item>& cache,
                          IterMap<Id, Item>& iter_map);
~~~

### RemoveDownloadItemFromCache

DownloadBubbleUpdateService::RemoveDownloadItemFromCache
~~~cpp
bool RemoveDownloadItemFromCache(download::DownloadItem* item);
~~~
 Removes an item from the maps. Note: If the cache size was already at the
 limit, and removing an item brings it under that limit, we must then get
 all items in order to backfill the newly created space. (See
 Backfill*Items() below.) Returns whether item was removed. If |item| is not
 already in the cache, this does nothing.

### RemoveOfflineItemFromCache

DownloadBubbleUpdateService::RemoveOfflineItemFromCache
~~~cpp
bool RemoveOfflineItemFromCache(
      const offline_items_collection::ContentId& id);
~~~

### RemoveItemFromCacheImpl

DownloadBubbleUpdateService::RemoveItemFromCacheImpl
~~~cpp
bool RemoveItemFromCacheImpl(const Id& id,
                               SortedItems<Item>& cache,
                               IterMap<Id, Item>& iter_map);
~~~

### RemoveItemFromCacheByIter

DownloadBubbleUpdateService::RemoveItemFromCacheByIter
~~~cpp
SortedItems<Item>::iterator RemoveItemFromCacheByIter(
      SortedItems<Item>::iterator iter,
      SortedItems<Item>& cache,
      IterMap<Id, Item>& iter_map);
~~~

### StartBackfillDownloadItems

DownloadBubbleUpdateService::StartBackfillDownloadItems
~~~cpp
void StartBackfillDownloadItems(const ItemSortKey& last_key);
~~~
 Gets all items from the DownloadManager/ContentProvider, finds the top
 items that sort at or after |last_key| and adds them to the cache such that
 the total number of items does not exceed the max. The Start*() versions
 just post a task to kick off backfilling while the other two perform the
 backfilling synchronously. Note that it is ok if other additions/deletions
 happen while the backfill task is queued. If an item is inserted before
 last_key then it would have been there anyway. If an item is inserted
 after last_key, it is the same as if it were added during backfilling. If
 an item is removed before last_key, then there is just more space to
 backfill.

### BackfillDownloadItems

DownloadBubbleUpdateService::BackfillDownloadItems
~~~cpp
void BackfillDownloadItems(const ItemSortKey& last_key);
~~~

### StartBackfillOfflineItems

DownloadBubbleUpdateService::StartBackfillOfflineItems
~~~cpp
void StartBackfillOfflineItems(const ItemSortKey& last_key);
~~~

### BackfillOfflineItems

DownloadBubbleUpdateService::BackfillOfflineItems
~~~cpp
void BackfillOfflineItems(
      const ItemSortKey& last_key,
      const std::vector<offline_items_collection::OfflineItem>& all_items);
~~~

### BackfillItemsImpl

DownloadBubbleUpdateService::BackfillItemsImpl
~~~cpp
void BackfillItemsImpl(const ItemSortKey& last_key,
                         const std::vector<Item>& items,
                         SortedItems<Item>& cache,
                         IterMap<Id, Item>& iter_map);
~~~

### InitializeDownloadItemsCache

DownloadBubbleUpdateService::InitializeDownloadItemsCache
~~~cpp
void InitializeDownloadItemsCache();
~~~
 Populate the cache from items fetched from the download manager or
 offline content manager.

### StartInitializeOfflineItemsCache

DownloadBubbleUpdateService::StartInitializeOfflineItemsCache
~~~cpp
void StartInitializeOfflineItemsCache();
~~~

### InitializeOfflineItemsCache

DownloadBubbleUpdateService::InitializeOfflineItemsCache
~~~cpp
void InitializeOfflineItemsCache(
      const std::vector<offline_items_collection::OfflineItem>& all_items);
~~~

### GetAllDownloadItems

DownloadBubbleUpdateService::GetAllDownloadItems
~~~cpp
std::vector<download::DownloadItem*> GetAllDownloadItems();
~~~
 Gets download items from profile and original profile.

### GetOfflineManager

DownloadBubbleUpdateService::GetOfflineManager
~~~cpp
OfflineItemModelManager* GetOfflineManager() const;
~~~

### MaybeAddDownloadItemModel

DownloadBubbleUpdateService::MaybeAddDownloadItemModel
~~~cpp
bool MaybeAddDownloadItemModel(
      download::DownloadItem* item,
      base::Time cutoff_time,
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models);
~~~
 Wraps an item into a DownloadUIModel and possibly adds it to |models|, if
 it is new enough (newer than |cutoff_time|) and meets other criteria.

 Returns whether model was eligible to be added, regardless of whether it
 was added (it may not have been added if |models| was at the size limit).

### MaybeAddOfflineItemModel

DownloadBubbleUpdateService::MaybeAddOfflineItemModel
~~~cpp
bool MaybeAddOfflineItemModel(
      const offline_items_collection::OfflineItem& item,
      base::Time cutoff_time,
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models);
~~~

### AppendBackfilledDownloadItems

DownloadBubbleUpdateService::AppendBackfilledDownloadItems
~~~cpp
void AppendBackfilledDownloadItems(
      const ItemSortKey& last_key,
      base::Time cutoff_time,
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models);
~~~
 Append newly backfilled download items to |models|. |last_key| is the last
 key that was processed before backfilling. May prune any expired items
 (i.e. items older than |cutoff_time|).

### OnDelayedCrxDownloadCreated

DownloadBubbleUpdateService::OnDelayedCrxDownloadCreated
~~~cpp
void OnDelayedCrxDownloadCreated(const std::string& guid);
~~~
 Called when a crx download has waited out its 2 second delay. Adds the
 item to the cache if it's not already done, and notifies window-level
 controllers.

### UpdateAllModelsInfo

DownloadBubbleUpdateService::UpdateAllModelsInfo
~~~cpp
void UpdateAllModelsInfo();
~~~
 Updates |all_models_info_| based on the current contents of the cache.

 This is kept updated as items are added or removed from the cache.

### ConsistencyCheckCaches

DownloadBubbleUpdateService::ConsistencyCheckCaches
~~~cpp
bool ConsistencyCheckCaches() const;
~~~
 Checks that the cache data structures are consistent.

### ConsistencyCheckImpl

DownloadBubbleUpdateService::ConsistencyCheckImpl
~~~cpp
bool ConsistencyCheckImpl(const SortedItems<Item>& cache,
                            const IterMap<Id, Item>& iter_map) const;
~~~

### profile_



~~~cpp

const raw_ptr<Profile> profile_ = nullptr;

~~~

 DCHECK_IS_ON()
 Profile corresponding to this object.

### original_profile_



~~~cpp

const raw_ptr<Profile> original_profile_ = nullptr;

~~~

 Null if the profile is not OTR.

### max_num_items_to_show_for_testing_



~~~cpp

absl::optional<size_t> max_num_items_to_show_for_testing_;

~~~

 Override for the number of combined items to return.

### extra_items_to_cache_for_testing_



~~~cpp

absl::optional<size_t> extra_items_to_cache_for_testing_;

~~~

 Override for the number of extra items to cache.

### download_items_



~~~cpp

SortedDownloadItems download_items_;

~~~

 Caches the current most-relevant items in sorted order. Size of each map
 will generally be limited to GetMaxNumItemsToShow (except during addition
 of an item). Note: These are multimaps because, in theory, multiple items
 might have the same sort key. The cache manipulation logic in this class
 accounts for this by assuming that, if there's not enough space for all the
 items with the last key, then caching an arbitrary subset of them is fine.

### offline_items_



~~~cpp

SortedOfflineItems offline_items_;

~~~


### download_items_iter_map_



~~~cpp

DownloadItemIterMap download_items_iter_map_;

~~~

 Holds iterators pointing into the above two maps, allowing lookup of an
 item by GUID or ContentId.

### offline_items_iter_map_



~~~cpp

OfflineItemIterMap offline_items_iter_map_;

~~~


### download_item_notifier_



~~~cpp

std::unique_ptr<download::AllDownloadItemNotifier> download_item_notifier_;

~~~

 Notifier for the current profile's DownloadManager. Null until initialized
 in Initialize().

### original_download_item_notifier_



~~~cpp

std::unique_ptr<download::AllDownloadItemNotifier>
      original_download_item_notifier_;

~~~

 Null if the profile is not OTR. Null until the original profile initiates
 a download. If the profile is OTR, this holds a notifier for the original
 profile.

### offline_items_initialized_



~~~cpp

bool offline_items_initialized_ = false;

~~~


### offline_item_callbacks_



~~~cpp

std::vector<base::OnceClosure> offline_item_callbacks_;

~~~

 Holds functions queued up while offline items were being initialized.

### offline_content_provider_shut_down_



~~~cpp

bool offline_content_provider_shut_down_ = false;

~~~


### delayed_crx_guids_



~~~cpp

std::set<std::string> delayed_crx_guids_;

~~~

 Set of GUIDs for extension/theme (crx) downloads that are pending notifying
 the UI. GUIDs are added here when the download begins, and are removed
 when the 2 second delay is up.

### all_models_info_



~~~cpp

DownloadDisplayController::AllDownloadUIModelsInfo all_models_info_;

~~~

 Holds the latest info about all models, relevant to the display state of
 the download toolbar icon.

### offline_content_provider_observation_



~~~cpp

base::ScopedObservation<
      offline_items_collection::OfflineContentProvider,
      offline_items_collection::OfflineContentProvider::Observer>
      offline_content_provider_observation_{this};

~~~

 Observes the offline content provider.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadBubbleUpdateService> weak_factory_{this};

~~~


### DownloadBubbleUpdateService

DownloadBubbleUpdateService
~~~cpp
DownloadBubbleUpdateService(const DownloadBubbleUpdateService&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadBubbleUpdateService& operator=(const DownloadBubbleUpdateService&) =
      delete;
~~~

### set_max_num_items_to_show_for_testing

set_max_num_items_to_show_for_testing
~~~cpp
void set_max_num_items_to_show_for_testing(size_t max) {
    max_num_items_to_show_for_testing_ = max;
  }
~~~
 Logic in this class assumes the max is at least 2.

### set_extra_items_to_cache_for_testing

set_extra_items_to_cache_for_testing
~~~cpp
void set_extra_items_to_cache_for_testing(size_t items) {
    extra_items_to_cache_for_testing_ = items;
  }
~~~

### download_item_notifier_for_testing

download_item_notifier_for_testing
~~~cpp
download::AllDownloadItemNotifier& download_item_notifier_for_testing() {
    return *download_item_notifier_;
  }
~~~

### original_download_item_notifier_for_testing

original_download_item_notifier_for_testing
~~~cpp
download::AllDownloadItemNotifier&
  original_download_item_notifier_for_testing() {
    return *original_download_item_notifier_;
  }
~~~

###  operator<

 Defines sort priority for items.

~~~cpp
struct ItemSortKey {
    enum State {
      kInProgressActive = 0,
      kInProgressPaused = 1,
      kNotInProgress = 2,
    };

    bool operator<(const ItemSortKey& other) const;
    bool operator==(const ItemSortKey& other) const;
    bool operator!=(const ItemSortKey& other) const;
    bool operator>(const ItemSortKey& other) const;

    // Active in-progress items come before paused items, which come before
    // not-in-progress items.
    State state;
    // Within each state, items are sorted in reverse chronological order by
    // start time (most recent first).
    base::Time start_time;
  };
~~~
### GetAllModelsToDisplay

DownloadBubbleUpdateService::GetAllModelsToDisplay
~~~cpp
virtual bool GetAllModelsToDisplay(
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models,
      bool force_backfill_download_items = false);
~~~
 Gets models for the top GetMaxNumItemsToShow() combined download items
 and offline items, in sorted order. May cause items to be pruned from the
 cache, if they have grown too old to be included. May trigger backfilling
 the caches, but does not wait for backfill results, unless
 |force_backfill_download_items| is true (in which case download items will
 be backfilled synchronously if necessary; offline items will not be
 backfilled synchronously). |models| is cleared. Returns whether results are
 complete. Results may not be complete if there might be more items to be
 returned after backfilling. Virtual for testing.

###  GetAllModelsInfo

 Returns information relevant to the display state of the download button.

 Does not prune the cache or backfill missing items. May be slightly
 inaccurate in edge cases. Virtual for testing.

~~~cpp
virtual const DownloadDisplayController::AllDownloadUIModelsInfo&
  GetAllModelsInfo();
~~~
### GetProgressInfo

DownloadBubbleUpdateService::GetProgressInfo
~~~cpp
virtual DownloadDisplayController::ProgressInfo GetProgressInfo() const;
~~~
 Computes progress info based on in-progress downloads. Does not prune the
 cache or backfill missing items, so the returned progress info may be
 slightly inaccurate in edge cases. This is ok, as it is only for the
 purpose of showing a progress ring around the icon, which is not precise
 anyway. Virtual for testing.

### Initialize

DownloadBubbleUpdateService::Initialize
~~~cpp
void Initialize(content::DownloadManager* manager);
~~~
 Initializes AllDownloadItemNotifier for the current profile, and
 initializes caches. This is called when the manager is ready, to signal
 that the DownloadBubbleUpdateService should begin tracking downloads. This
 starts initialization of both the download items and the offline items.

 Should only be called once.

### InitializeOriginalNotifier

DownloadBubbleUpdateService::InitializeOriginalNotifier
~~~cpp
void InitializeOriginalNotifier(content::DownloadManager* manager);
~~~
 Initializes the AllDownloadItemNotifier for the original profile, if
 |profile_| is off the record. May trigger re-initialization of the download
 items cache.

### GetDownloadManager

DownloadBubbleUpdateService::GetDownloadManager
~~~cpp
content::DownloadManager* GetDownloadManager();
~~~
 Get the DownloadManager that |download_item_notifier_| is listening to.

### IsInitialized

DownloadBubbleUpdateService::IsInitialized
~~~cpp
virtual bool IsInitialized() const;
~~~
 Virtual for testing.

### Shutdown

DownloadBubbleUpdateService::Shutdown
~~~cpp
void Shutdown() override;
~~~
 KeyedService:
### OnDownloadCreated

DownloadBubbleUpdateService::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~
 download::AllDownloadItemNotifier::Observer:
### OnDownloadUpdated

DownloadBubbleUpdateService::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadRemoved

DownloadBubbleUpdateService::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnManagerGoingDown

DownloadBubbleUpdateService::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~

### OnItemsAdded

DownloadBubbleUpdateService::OnItemsAdded
~~~cpp
void OnItemsAdded(
      const offline_items_collection::OfflineContentProvider::OfflineItemList&
          items) override;
~~~
 offline_items_collection::OfflineContentProvider::Observer:
### OnItemRemoved

DownloadBubbleUpdateService::OnItemRemoved
~~~cpp
void OnItemRemoved(const offline_items_collection::ContentId& id) override;
~~~

### OnItemUpdated

DownloadBubbleUpdateService::OnItemUpdated
~~~cpp
void OnItemUpdated(
      const offline_items_collection::OfflineItem& item,
      const absl::optional<offline_items_collection::UpdateDelta>& update_delta)
      override;
~~~

### OnContentProviderGoingDown

DownloadBubbleUpdateService::OnContentProviderGoingDown
~~~cpp
void OnContentProviderGoingDown() override;
~~~

### GetMaxNumItemsToShow

DownloadBubbleUpdateService::GetMaxNumItemsToShow
~~~cpp
size_t GetMaxNumItemsToShow() const;
~~~
 Returns the max number of combined download items and offline items that
 will be returned from GetAllModelsToDisplay().

### GetNumItemsToCache

DownloadBubbleUpdateService::GetNumItemsToCache
~~~cpp
size_t GetNumItemsToCache() const;
~~~
 Returns the max number of items (of each type) to cache. This is slightly
 more than the max number of items to show.

### MaybeAddDownloadItemToCache

DownloadBubbleUpdateService::MaybeAddDownloadItemToCache
~~~cpp
bool MaybeAddDownloadItemToCache(download::DownloadItem* item, bool is_new);
~~~
 Adds an item to the cache if it is recent enough and meets other criteria
 for showing in the bubble. If adding an item makes the map size exceed the
 maximum, this removes excess items from the end of the map. Returns whether
 the item was stored as the last item in the map. If |item| was already in
 the cache, this does nothing. |is_new| is whether the item is a newly added
 item (as opposed to an updated one). May mark the item model as
 not-actioned-on if the item is new.

### MaybeAddOfflineItemToCache

DownloadBubbleUpdateService::MaybeAddOfflineItemToCache
~~~cpp
bool MaybeAddOfflineItemToCache(
      const offline_items_collection::OfflineItem& item,
      bool is_new);
~~~

### RemoveDownloadItemFromCache

DownloadBubbleUpdateService::RemoveDownloadItemFromCache
~~~cpp
bool RemoveDownloadItemFromCache(download::DownloadItem* item);
~~~
 Removes an item from the maps. Note: If the cache size was already at the
 limit, and removing an item brings it under that limit, we must then get
 all items in order to backfill the newly created space. (See
 Backfill*Items() below.) Returns whether item was removed. If |item| is not
 already in the cache, this does nothing.

### RemoveOfflineItemFromCache

DownloadBubbleUpdateService::RemoveOfflineItemFromCache
~~~cpp
bool RemoveOfflineItemFromCache(
      const offline_items_collection::ContentId& id);
~~~

### StartBackfillDownloadItems

DownloadBubbleUpdateService::StartBackfillDownloadItems
~~~cpp
void StartBackfillDownloadItems(const ItemSortKey& last_key);
~~~
 Gets all items from the DownloadManager/ContentProvider, finds the top
 items that sort at or after |last_key| and adds them to the cache such that
 the total number of items does not exceed the max. The Start*() versions
 just post a task to kick off backfilling while the other two perform the
 backfilling synchronously. Note that it is ok if other additions/deletions
 happen while the backfill task is queued. If an item is inserted before
 last_key then it would have been there anyway. If an item is inserted
 after last_key, it is the same as if it were added during backfilling. If
 an item is removed before last_key, then there is just more space to
 backfill.

### BackfillDownloadItems

DownloadBubbleUpdateService::BackfillDownloadItems
~~~cpp
void BackfillDownloadItems(const ItemSortKey& last_key);
~~~

### StartBackfillOfflineItems

DownloadBubbleUpdateService::StartBackfillOfflineItems
~~~cpp
void StartBackfillOfflineItems(const ItemSortKey& last_key);
~~~

### BackfillOfflineItems

DownloadBubbleUpdateService::BackfillOfflineItems
~~~cpp
void BackfillOfflineItems(
      const ItemSortKey& last_key,
      const std::vector<offline_items_collection::OfflineItem>& all_items);
~~~

### InitializeDownloadItemsCache

DownloadBubbleUpdateService::InitializeDownloadItemsCache
~~~cpp
void InitializeDownloadItemsCache();
~~~
 Populate the cache from items fetched from the download manager or
 offline content manager.

### StartInitializeOfflineItemsCache

DownloadBubbleUpdateService::StartInitializeOfflineItemsCache
~~~cpp
void StartInitializeOfflineItemsCache();
~~~

### InitializeOfflineItemsCache

DownloadBubbleUpdateService::InitializeOfflineItemsCache
~~~cpp
void InitializeOfflineItemsCache(
      const std::vector<offline_items_collection::OfflineItem>& all_items);
~~~

### GetAllDownloadItems

DownloadBubbleUpdateService::GetAllDownloadItems
~~~cpp
std::vector<download::DownloadItem*> GetAllDownloadItems();
~~~
 Gets download items from profile and original profile.

### GetOfflineManager

DownloadBubbleUpdateService::GetOfflineManager
~~~cpp
OfflineItemModelManager* GetOfflineManager() const;
~~~

### MaybeAddDownloadItemModel

DownloadBubbleUpdateService::MaybeAddDownloadItemModel
~~~cpp
bool MaybeAddDownloadItemModel(
      download::DownloadItem* item,
      base::Time cutoff_time,
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models);
~~~
 Wraps an item into a DownloadUIModel and possibly adds it to |models|, if
 it is new enough (newer than |cutoff_time|) and meets other criteria.

 Returns whether model was eligible to be added, regardless of whether it
 was added (it may not have been added if |models| was at the size limit).

### MaybeAddOfflineItemModel

DownloadBubbleUpdateService::MaybeAddOfflineItemModel
~~~cpp
bool MaybeAddOfflineItemModel(
      const offline_items_collection::OfflineItem& item,
      base::Time cutoff_time,
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models);
~~~

### AppendBackfilledDownloadItems

DownloadBubbleUpdateService::AppendBackfilledDownloadItems
~~~cpp
void AppendBackfilledDownloadItems(
      const ItemSortKey& last_key,
      base::Time cutoff_time,
      std::vector<DownloadUIModel::DownloadUIModelPtr>& models);
~~~
 Append newly backfilled download items to |models|. |last_key| is the last
 key that was processed before backfilling. May prune any expired items
 (i.e. items older than |cutoff_time|).

### OnDelayedCrxDownloadCreated

DownloadBubbleUpdateService::OnDelayedCrxDownloadCreated
~~~cpp
void OnDelayedCrxDownloadCreated(const std::string& guid);
~~~
 Called when a crx download has waited out its 2 second delay. Adds the
 item to the cache if it's not already done, and notifies window-level
 controllers.

### UpdateAllModelsInfo

DownloadBubbleUpdateService::UpdateAllModelsInfo
~~~cpp
void UpdateAllModelsInfo();
~~~
 Updates |all_models_info_| based on the current contents of the cache.

 This is kept updated as items are added or removed from the cache.

### profile_



~~~cpp

const raw_ptr<Profile> profile_ = nullptr;

~~~

 DCHECK_IS_ON()
 Profile corresponding to this object.

### original_profile_



~~~cpp

const raw_ptr<Profile> original_profile_ = nullptr;

~~~

 Null if the profile is not OTR.

### max_num_items_to_show_for_testing_



~~~cpp

absl::optional<size_t> max_num_items_to_show_for_testing_;

~~~

 Override for the number of combined items to return.

### extra_items_to_cache_for_testing_



~~~cpp

absl::optional<size_t> extra_items_to_cache_for_testing_;

~~~

 Override for the number of extra items to cache.

### download_items_



~~~cpp

SortedDownloadItems download_items_;

~~~

 Caches the current most-relevant items in sorted order. Size of each map
 will generally be limited to GetMaxNumItemsToShow (except during addition
 of an item). Note: These are multimaps because, in theory, multiple items
 might have the same sort key. The cache manipulation logic in this class
 accounts for this by assuming that, if there's not enough space for all the
 items with the last key, then caching an arbitrary subset of them is fine.

### offline_items_



~~~cpp

SortedOfflineItems offline_items_;

~~~


### download_items_iter_map_



~~~cpp

DownloadItemIterMap download_items_iter_map_;

~~~

 Holds iterators pointing into the above two maps, allowing lookup of an
 item by GUID or ContentId.

### offline_items_iter_map_



~~~cpp

OfflineItemIterMap offline_items_iter_map_;

~~~


### download_item_notifier_



~~~cpp

std::unique_ptr<download::AllDownloadItemNotifier> download_item_notifier_;

~~~

 Notifier for the current profile's DownloadManager. Null until initialized
 in Initialize().

### original_download_item_notifier_



~~~cpp

std::unique_ptr<download::AllDownloadItemNotifier>
      original_download_item_notifier_;

~~~

 Null if the profile is not OTR. Null until the original profile initiates
 a download. If the profile is OTR, this holds a notifier for the original
 profile.

### offline_items_initialized_



~~~cpp

bool offline_items_initialized_ = false;

~~~


### offline_item_callbacks_



~~~cpp

std::vector<base::OnceClosure> offline_item_callbacks_;

~~~

 Holds functions queued up while offline items were being initialized.

### offline_content_provider_shut_down_



~~~cpp

bool offline_content_provider_shut_down_ = false;

~~~


### delayed_crx_guids_



~~~cpp

std::set<std::string> delayed_crx_guids_;

~~~

 Set of GUIDs for extension/theme (crx) downloads that are pending notifying
 the UI. GUIDs are added here when the download begins, and are removed
 when the 2 second delay is up.

### all_models_info_



~~~cpp

DownloadDisplayController::AllDownloadUIModelsInfo all_models_info_;

~~~

 Holds the latest info about all models, relevant to the display state of
 the download toolbar icon.

### offline_content_provider_observation_



~~~cpp

base::ScopedObservation<
      offline_items_collection::OfflineContentProvider,
      offline_items_collection::OfflineContentProvider::Observer>
      offline_content_provider_observation_{this};

~~~

 Observes the offline content provider.

### weak_factory_



~~~cpp

base::WeakPtrFactory<DownloadBubbleUpdateService> weak_factory_{this};

~~~

