### IsDownloadBubbleEnabled

IsDownloadBubbleEnabled
~~~cpp
bool IsDownloadBubbleEnabled(Profile* profile);
~~~
 Called when deciding whether to show the bubble or the old download shelf UI.

### IsDownloadBubbleV2Enabled

IsDownloadBubbleV2Enabled
~~~cpp
bool IsDownloadBubbleV2Enabled(Profile* profile);
~~~
 V2 is only eligible to be enabled if V1 is also enabled.

### ShouldShowDownloadBubble

ShouldShowDownloadBubble
~~~cpp
bool ShouldShowDownloadBubble(Profile* profile);
~~~
 Called when deciding whether to show or hide the bubble.

### IsDownloadConnectorEnabled

IsDownloadConnectorEnabled
~~~cpp
bool IsDownloadConnectorEnabled(Profile* profile);
~~~

### ShouldSuppressDownloadBubbleIph

ShouldSuppressDownloadBubbleIph
~~~cpp
bool ShouldSuppressDownloadBubbleIph(Profile* profile);
~~~
 Whether to suppress the download bubble IPH. This will be true for users
 who have interacted with the download bubble prior to the IPH being added.

