
## class DefaultDownloadDirPolicyHandler
 ConfigurationPolicyHandler for the DefaultDownloadDirectory policy.

### DefaultDownloadDirPolicyHandler

DefaultDownloadDirPolicyHandler::DefaultDownloadDirPolicyHandler
~~~cpp
DefaultDownloadDirPolicyHandler();
~~~

### DefaultDownloadDirPolicyHandler

DefaultDownloadDirPolicyHandler
~~~cpp
DefaultDownloadDirPolicyHandler(const DefaultDownloadDirPolicyHandler&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DefaultDownloadDirPolicyHandler& operator=(
      const DefaultDownloadDirPolicyHandler&) = delete;
~~~

### ~DefaultDownloadDirPolicyHandler

DefaultDownloadDirPolicyHandler::~DefaultDownloadDirPolicyHandler
~~~cpp
~DefaultDownloadDirPolicyHandler() override;
~~~

### CheckPolicySettings

DefaultDownloadDirPolicyHandler::CheckPolicySettings
~~~cpp
bool CheckPolicySettings(const policy::PolicyMap& policies,
                           policy::PolicyErrorMap* errors) override;
~~~
 ConfigurationPolicyHandler:
### ApplyPolicySettingsWithParameters

DefaultDownloadDirPolicyHandler::ApplyPolicySettingsWithParameters
~~~cpp
void ApplyPolicySettingsWithParameters(
      const policy::PolicyMap& policies,
      const policy::PolicyHandlerParameters& parameters,
      PrefValueMap* prefs) override;
~~~

### ApplyPolicySettings

DefaultDownloadDirPolicyHandler::ApplyPolicySettings
~~~cpp
void ApplyPolicySettings(const policy::PolicyMap& policies,
                           PrefValueMap* prefs) override;
~~~

### DefaultDownloadDirPolicyHandler

DefaultDownloadDirPolicyHandler
~~~cpp
DefaultDownloadDirPolicyHandler(const DefaultDownloadDirPolicyHandler&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DefaultDownloadDirPolicyHandler& operator=(
      const DefaultDownloadDirPolicyHandler&) = delete;
~~~

### CheckPolicySettings

DefaultDownloadDirPolicyHandler::CheckPolicySettings
~~~cpp
bool CheckPolicySettings(const policy::PolicyMap& policies,
                           policy::PolicyErrorMap* errors) override;
~~~
 ConfigurationPolicyHandler:
### ApplyPolicySettingsWithParameters

DefaultDownloadDirPolicyHandler::ApplyPolicySettingsWithParameters
~~~cpp
void ApplyPolicySettingsWithParameters(
      const policy::PolicyMap& policies,
      const policy::PolicyHandlerParameters& parameters,
      PrefValueMap* prefs) override;
~~~

### ApplyPolicySettings

DefaultDownloadDirPolicyHandler::ApplyPolicySettings
~~~cpp
void ApplyPolicySettings(const policy::PolicyMap& policies,
                           PrefValueMap* prefs) override;
~~~
