
## class DeferredClientWrapper

### DeferredClientWrapper

DeferredClientWrapper::DeferredClientWrapper
~~~cpp
DeferredClientWrapper(ClientFactory factory, SimpleFactoryKey* key);
~~~

### ~DeferredClientWrapper

DeferredClientWrapper::~DeferredClientWrapper
~~~cpp
~DeferredClientWrapper() override;
~~~

### OnServiceInitialized

DeferredClientWrapper::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<DownloadMetaData>& downloads) override;
~~~
 Client implementation.

### OnServiceUnavailable

DeferredClientWrapper::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

DeferredClientWrapper::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadUpdated

DeferredClientWrapper::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(const std::string& guid,
                         uint64_t bytes_uploaded,
                         uint64_t bytes_downloaded) override;
~~~

### OnDownloadFailed

DeferredClientWrapper::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& info,
                        FailureReason reason) override;
~~~

### OnDownloadSucceeded

DeferredClientWrapper::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(const std::string& guid,
                           const CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

DeferredClientWrapper::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

DeferredClientWrapper::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     GetUploadDataCallback callback) override;
~~~

### ForwardOnServiceInitialized

DeferredClientWrapper::ForwardOnServiceInitialized
~~~cpp
void ForwardOnServiceInitialized(
      bool state_lost,
      const std::vector<DownloadMetaData>& downloads);
~~~
 Forwarding functions
### ForwardOnServiceUnavailable

DeferredClientWrapper::ForwardOnServiceUnavailable
~~~cpp
void ForwardOnServiceUnavailable();
~~~

### ForwardOnDownloadStarted

DeferredClientWrapper::ForwardOnDownloadStarted
~~~cpp
void ForwardOnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers);
~~~

### ForwardOnDownloadUpdated

DeferredClientWrapper::ForwardOnDownloadUpdated
~~~cpp
void ForwardOnDownloadUpdated(const std::string& guid,
                                uint64_t bytes_uploaded,
                                uint64_t bytes_downloaded);
~~~

### ForwardOnDownloadFailed

DeferredClientWrapper::ForwardOnDownloadFailed
~~~cpp
void ForwardOnDownloadFailed(const std::string& guid,
                               const download::CompletionInfo& info,
                               FailureReason reason);
~~~

### ForwardOnDownloadSucceeded

DeferredClientWrapper::ForwardOnDownloadSucceeded
~~~cpp
void ForwardOnDownloadSucceeded(const std::string& guid,
                                  const CompletionInfo& completion_info);
~~~

### ForwardCanServiceRemoveDownloadedFile

DeferredClientWrapper::ForwardCanServiceRemoveDownloadedFile
~~~cpp
void ForwardCanServiceRemoveDownloadedFile(const std::string& guid,
                                             bool force_delete);
~~~

### ForwardGetUploadData

DeferredClientWrapper::ForwardGetUploadData
~~~cpp
void ForwardGetUploadData(const std::string& guid,
                            GetUploadDataCallback callback);
~~~

### RunDeferredClosures

DeferredClientWrapper::RunDeferredClosures
~~~cpp
void RunDeferredClosures(bool force_inflate);
~~~

### DoRunDeferredClosures

DeferredClientWrapper::DoRunDeferredClosures
~~~cpp
void DoRunDeferredClosures();
~~~

### InflateClient

DeferredClientWrapper::InflateClient
~~~cpp
void InflateClient(Profile* profile);
~~~

### wrapped_client_



~~~cpp

std::unique_ptr<download::Client> wrapped_client_;

~~~


### deferred_closures_



~~~cpp

std::vector<base::OnceClosure> deferred_closures_;

~~~


### client_factory_



~~~cpp

ClientFactory client_factory_;

~~~


### key_



~~~cpp

raw_ptr<SimpleFactoryKey> key_;

~~~


### full_browser_requested_



~~~cpp

bool full_browser_requested_;

~~~

 This is needed to record UMA for when DownloadClientWrapper requests a full
 browser start while the browser is running in reduced mode. Reduced mode is
 only on Android so it is ifdefed out on other platforms to prevent the
 compiler from complaining that it is unused.

### LaunchFullBrowser

DeferredClientWrapper::LaunchFullBrowser
~~~cpp
void LaunchFullBrowser();
~~~

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DeferredClientWrapper> weak_ptr_factory_{this};

~~~


### OnServiceInitialized

DeferredClientWrapper::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<DownloadMetaData>& downloads) override;
~~~
 Client implementation.

### OnServiceUnavailable

DeferredClientWrapper::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

DeferredClientWrapper::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadUpdated

DeferredClientWrapper::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(const std::string& guid,
                         uint64_t bytes_uploaded,
                         uint64_t bytes_downloaded) override;
~~~

### OnDownloadFailed

DeferredClientWrapper::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& info,
                        FailureReason reason) override;
~~~

### OnDownloadSucceeded

DeferredClientWrapper::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(const std::string& guid,
                           const CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

DeferredClientWrapper::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

DeferredClientWrapper::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     GetUploadDataCallback callback) override;
~~~

### ForwardOnServiceInitialized

DeferredClientWrapper::ForwardOnServiceInitialized
~~~cpp
void ForwardOnServiceInitialized(
      bool state_lost,
      const std::vector<DownloadMetaData>& downloads);
~~~
 Forwarding functions
### ForwardOnServiceUnavailable

DeferredClientWrapper::ForwardOnServiceUnavailable
~~~cpp
void ForwardOnServiceUnavailable();
~~~

### ForwardOnDownloadStarted

DeferredClientWrapper::ForwardOnDownloadStarted
~~~cpp
void ForwardOnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers);
~~~

### ForwardOnDownloadUpdated

DeferredClientWrapper::ForwardOnDownloadUpdated
~~~cpp
void ForwardOnDownloadUpdated(const std::string& guid,
                                uint64_t bytes_uploaded,
                                uint64_t bytes_downloaded);
~~~

### ForwardOnDownloadFailed

DeferredClientWrapper::ForwardOnDownloadFailed
~~~cpp
void ForwardOnDownloadFailed(const std::string& guid,
                               const download::CompletionInfo& info,
                               FailureReason reason);
~~~

### ForwardOnDownloadSucceeded

DeferredClientWrapper::ForwardOnDownloadSucceeded
~~~cpp
void ForwardOnDownloadSucceeded(const std::string& guid,
                                  const CompletionInfo& completion_info);
~~~

### ForwardCanServiceRemoveDownloadedFile

DeferredClientWrapper::ForwardCanServiceRemoveDownloadedFile
~~~cpp
void ForwardCanServiceRemoveDownloadedFile(const std::string& guid,
                                             bool force_delete);
~~~

### ForwardGetUploadData

DeferredClientWrapper::ForwardGetUploadData
~~~cpp
void ForwardGetUploadData(const std::string& guid,
                            GetUploadDataCallback callback);
~~~

### RunDeferredClosures

DeferredClientWrapper::RunDeferredClosures
~~~cpp
void RunDeferredClosures(bool force_inflate);
~~~

### DoRunDeferredClosures

DeferredClientWrapper::DoRunDeferredClosures
~~~cpp
void DoRunDeferredClosures();
~~~

### InflateClient

DeferredClientWrapper::InflateClient
~~~cpp
void InflateClient(Profile* profile);
~~~

### wrapped_client_



~~~cpp

std::unique_ptr<download::Client> wrapped_client_;

~~~


### deferred_closures_



~~~cpp

std::vector<base::OnceClosure> deferred_closures_;

~~~


### client_factory_



~~~cpp

ClientFactory client_factory_;

~~~


### key_



~~~cpp

raw_ptr<SimpleFactoryKey> key_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DeferredClientWrapper> weak_ptr_factory_{this};

~~~

