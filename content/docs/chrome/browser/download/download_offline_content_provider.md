
## class DownloadOfflineContentProvider
 This class handles the task of observing the downloads associated with a
 SimpleDownloadManagerCoordinator and notifies UI about updates about various
 downloads. This is a per-profile class which works with both reduced mode and
 full browser mode. It also provides internal buffering of the download
 actions if the required backend is not ready.

### DownloadOfflineContentProvider

DownloadOfflineContentProvider::DownloadOfflineContentProvider
~~~cpp
explicit DownloadOfflineContentProvider(OfflineContentAggregator* aggregator,
                                          const std::string& name_space);
~~~

### DownloadOfflineContentProvider

DownloadOfflineContentProvider
~~~cpp
DownloadOfflineContentProvider(const DownloadOfflineContentProvider&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadOfflineContentProvider& operator=(
      const DownloadOfflineContentProvider&) = delete;
~~~

### ~DownloadOfflineContentProvider

DownloadOfflineContentProvider::~DownloadOfflineContentProvider
~~~cpp
~DownloadOfflineContentProvider() override;
~~~

### SetSimpleDownloadManagerCoordinator

DownloadOfflineContentProvider::SetSimpleDownloadManagerCoordinator
~~~cpp
void SetSimpleDownloadManagerCoordinator(
      SimpleDownloadManagerCoordinator* manager);
~~~
 Should be called when a DownloadManager is available.

### OpenItem

DownloadOfflineContentProvider::OpenItem
~~~cpp
void OpenItem(const OpenParams& open_params, const ContentId& id) override;
~~~
 OfflineContentProvider implmentation.

 Some of these methods can be run in reduced mode while others require the
 full browser process to be started as mentioned below.

 Methods that require full browser process.

### RemoveItem

DownloadOfflineContentProvider::RemoveItem
~~~cpp
void RemoveItem(const ContentId& id) override;
~~~

### GetItemById

DownloadOfflineContentProvider::GetItemById
~~~cpp
void GetItemById(
      const ContentId& id,
      OfflineContentProvider::SingleItemCallback callback) override;
~~~

### GetAllItems

DownloadOfflineContentProvider::GetAllItems
~~~cpp
void GetAllItems(
      OfflineContentProvider::MultipleItemCallback callback) override;
~~~

### GetVisualsForItem

DownloadOfflineContentProvider::GetVisualsForItem
~~~cpp
void GetVisualsForItem(
      const ContentId& id,
      GetVisualsOptions options,
      OfflineContentProvider::VisualsCallback callback) override;
~~~

### GetShareInfoForItem

DownloadOfflineContentProvider::GetShareInfoForItem
~~~cpp
void GetShareInfoForItem(const ContentId& id,
                           ShareCallback callback) override;
~~~

### RenameItem

DownloadOfflineContentProvider::RenameItem
~~~cpp
void RenameItem(const ContentId& id,
                  const std::string& name,
                  RenameCallback callback) override;
~~~

### CancelDownload

DownloadOfflineContentProvider::CancelDownload
~~~cpp
void CancelDownload(const ContentId& id) override;
~~~
 Methods that can be run in reduced mode.

### PauseDownload

DownloadOfflineContentProvider::PauseDownload
~~~cpp
void PauseDownload(const ContentId& id) override;
~~~

### ResumeDownload

DownloadOfflineContentProvider::ResumeDownload
~~~cpp
void ResumeDownload(const ContentId& id, bool has_user_gesture) override;
~~~

### OnDownloadStarted

DownloadOfflineContentProvider::OnDownloadStarted
~~~cpp
void OnDownloadStarted(DownloadItem* download_item);
~~~
 Entry point for associating this class with a download item. Must be called
 for all new and in-progress downloads, after which this class will start
 observing the given download.

### OnDownloadUpdated

DownloadOfflineContentProvider::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(DownloadItem* item) override;
~~~
 DownloadItem::Observer overrides
### OnDownloadRemoved

DownloadOfflineContentProvider::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(DownloadItem* item) override;
~~~

### OnProfileCreated

DownloadOfflineContentProvider::OnProfileCreated
~~~cpp
void OnProfileCreated(Profile* profile);
~~~

### enum class

~~~cpp
enum class State {
    // Download system is not yet initialized.
    UNINITIALIZED,

    // Only active downloads have been loaded.
    ACTIVE_DOWNLOADS_ONLY,

    // All downloads including ones from history have been loaded.
    HISTORY_LOADED,
  }
~~~
### OnDownloadsInitialized

DownloadOfflineContentProvider::OnDownloadsInitialized
~~~cpp
void OnDownloadsInitialized(bool active_downloads_only) override;
~~~
 SimpleDownloadManagerCoordinator::Observer overrides
### OnManagerGoingDown

DownloadOfflineContentProvider::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(SimpleDownloadManagerCoordinator* manager) override;
~~~

### GetAllDownloads

DownloadOfflineContentProvider::GetAllDownloads
~~~cpp
void GetAllDownloads(std::vector<DownloadItem*>* all_items);
~~~

### GetDownload

DownloadOfflineContentProvider::GetDownload
~~~cpp
DownloadItem* GetDownload(const std::string& download_guid);
~~~

### OnThumbnailRetrieved

DownloadOfflineContentProvider::OnThumbnailRetrieved
~~~cpp
void OnThumbnailRetrieved(const ContentId& id,
                            VisualsCallback callback,
                            const SkBitmap& bitmap);
~~~

### AddCompletedDownload

DownloadOfflineContentProvider::AddCompletedDownload
~~~cpp
void AddCompletedDownload(DownloadItem* item);
~~~

### AddCompletedDownloadDone

DownloadOfflineContentProvider::AddCompletedDownloadDone
~~~cpp
void AddCompletedDownloadDone(const std::string& download_guid,
                                int64_t system_download_id);
~~~

### OnRenameDownloadCallbackDone

DownloadOfflineContentProvider::OnRenameDownloadCallbackDone
~~~cpp
void OnRenameDownloadCallbackDone(RenameCallback callback,
                                    DownloadItem* item,
                                    DownloadItem::DownloadRenameResult result);
~~~

### UpdateObservers

DownloadOfflineContentProvider::UpdateObservers
~~~cpp
void UpdateObservers(const OfflineItem& item,
                       const absl::optional<UpdateDelta>& update_delta);
~~~

### CheckForExternallyRemovedDownloads

DownloadOfflineContentProvider::CheckForExternallyRemovedDownloads
~~~cpp
void CheckForExternallyRemovedDownloads();
~~~

### EnsureDownloadCoreServiceStarted

DownloadOfflineContentProvider::EnsureDownloadCoreServiceStarted
~~~cpp
void EnsureDownloadCoreServiceStarted();
~~~
 Ensure that download core service is started.

### RunGetAllItemsCallback

DownloadOfflineContentProvider::RunGetAllItemsCallback
~~~cpp
void RunGetAllItemsCallback(
      OfflineContentProvider::MultipleItemCallback callback);
~~~
 Helper method to run callbacks with the latest download information.

### RunGetItemByIdCallback

DownloadOfflineContentProvider::RunGetItemByIdCallback
~~~cpp
void RunGetItemByIdCallback(
      const ContentId& id,
      OfflineContentProvider::SingleItemCallback callback);
~~~

### aggregator_



~~~cpp

raw_ptr<OfflineContentAggregator> aggregator_;

~~~


### name_space_



~~~cpp

std::string name_space_;

~~~


### manager_



~~~cpp

raw_ptr<SimpleDownloadManagerCoordinator> manager_;

~~~


### all_download_observer_



~~~cpp

std::unique_ptr<download::AllDownloadEventNotifier::Observer>
      all_download_observer_;

~~~


### checked_for_externally_removed_downloads_



~~~cpp

bool checked_for_externally_removed_downloads_;

~~~


### state_



~~~cpp

State state_;

~~~


### pending_actions_for_reduced_mode_



~~~cpp

base::circular_deque<base::OnceClosure> pending_actions_for_reduced_mode_;

~~~


### pending_actions_for_full_browser_



~~~cpp

base::circular_deque<base::OnceClosure> pending_actions_for_full_browser_;

~~~


### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadOfflineContentProvider> weak_ptr_factory_{this};

~~~


### DownloadOfflineContentProvider

DownloadOfflineContentProvider
~~~cpp
DownloadOfflineContentProvider(const DownloadOfflineContentProvider&) =
      delete;
~~~

### operator=

operator=
~~~cpp
DownloadOfflineContentProvider& operator=(
      const DownloadOfflineContentProvider&) = delete;
~~~

### SetSimpleDownloadManagerCoordinator

DownloadOfflineContentProvider::SetSimpleDownloadManagerCoordinator
~~~cpp
void SetSimpleDownloadManagerCoordinator(
      SimpleDownloadManagerCoordinator* manager);
~~~
 Should be called when a DownloadManager is available.

### OpenItem

DownloadOfflineContentProvider::OpenItem
~~~cpp
void OpenItem(const OpenParams& open_params, const ContentId& id) override;
~~~
 OfflineContentProvider implmentation.

 Some of these methods can be run in reduced mode while others require the
 full browser process to be started as mentioned below.

 Methods that require full browser process.

### RemoveItem

DownloadOfflineContentProvider::RemoveItem
~~~cpp
void RemoveItem(const ContentId& id) override;
~~~

### GetItemById

DownloadOfflineContentProvider::GetItemById
~~~cpp
void GetItemById(
      const ContentId& id,
      OfflineContentProvider::SingleItemCallback callback) override;
~~~

### GetAllItems

DownloadOfflineContentProvider::GetAllItems
~~~cpp
void GetAllItems(
      OfflineContentProvider::MultipleItemCallback callback) override;
~~~

### GetVisualsForItem

DownloadOfflineContentProvider::GetVisualsForItem
~~~cpp
void GetVisualsForItem(
      const ContentId& id,
      GetVisualsOptions options,
      OfflineContentProvider::VisualsCallback callback) override;
~~~

### GetShareInfoForItem

DownloadOfflineContentProvider::GetShareInfoForItem
~~~cpp
void GetShareInfoForItem(const ContentId& id,
                           ShareCallback callback) override;
~~~

### RenameItem

DownloadOfflineContentProvider::RenameItem
~~~cpp
void RenameItem(const ContentId& id,
                  const std::string& name,
                  RenameCallback callback) override;
~~~

### CancelDownload

DownloadOfflineContentProvider::CancelDownload
~~~cpp
void CancelDownload(const ContentId& id) override;
~~~
 Methods that can be run in reduced mode.

### PauseDownload

DownloadOfflineContentProvider::PauseDownload
~~~cpp
void PauseDownload(const ContentId& id) override;
~~~

### ResumeDownload

DownloadOfflineContentProvider::ResumeDownload
~~~cpp
void ResumeDownload(const ContentId& id, bool has_user_gesture) override;
~~~

### OnDownloadStarted

DownloadOfflineContentProvider::OnDownloadStarted
~~~cpp
void OnDownloadStarted(DownloadItem* download_item);
~~~
 Entry point for associating this class with a download item. Must be called
 for all new and in-progress downloads, after which this class will start
 observing the given download.

### OnDownloadUpdated

DownloadOfflineContentProvider::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(DownloadItem* item) override;
~~~
 DownloadItem::Observer overrides
### OnDownloadRemoved

DownloadOfflineContentProvider::OnDownloadRemoved
~~~cpp
void OnDownloadRemoved(DownloadItem* item) override;
~~~

### OnProfileCreated

DownloadOfflineContentProvider::OnProfileCreated
~~~cpp
void OnProfileCreated(Profile* profile);
~~~

### enum class

~~~cpp
enum class State {
    // Download system is not yet initialized.
    UNINITIALIZED,

    // Only active downloads have been loaded.
    ACTIVE_DOWNLOADS_ONLY,

    // All downloads including ones from history have been loaded.
    HISTORY_LOADED,
  };
~~~
### OnDownloadsInitialized

DownloadOfflineContentProvider::OnDownloadsInitialized
~~~cpp
void OnDownloadsInitialized(bool active_downloads_only) override;
~~~
 SimpleDownloadManagerCoordinator::Observer overrides
### OnManagerGoingDown

DownloadOfflineContentProvider::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(SimpleDownloadManagerCoordinator* manager) override;
~~~

### GetAllDownloads

DownloadOfflineContentProvider::GetAllDownloads
~~~cpp
void GetAllDownloads(std::vector<DownloadItem*>* all_items);
~~~

### GetDownload

DownloadOfflineContentProvider::GetDownload
~~~cpp
DownloadItem* GetDownload(const std::string& download_guid);
~~~

### OnThumbnailRetrieved

DownloadOfflineContentProvider::OnThumbnailRetrieved
~~~cpp
void OnThumbnailRetrieved(const ContentId& id,
                            VisualsCallback callback,
                            const SkBitmap& bitmap);
~~~

### AddCompletedDownload

DownloadOfflineContentProvider::AddCompletedDownload
~~~cpp
void AddCompletedDownload(DownloadItem* item);
~~~

### AddCompletedDownloadDone

DownloadOfflineContentProvider::AddCompletedDownloadDone
~~~cpp
void AddCompletedDownloadDone(const std::string& download_guid,
                                int64_t system_download_id);
~~~

### OnRenameDownloadCallbackDone

DownloadOfflineContentProvider::OnRenameDownloadCallbackDone
~~~cpp
void OnRenameDownloadCallbackDone(RenameCallback callback,
                                    DownloadItem* item,
                                    DownloadItem::DownloadRenameResult result);
~~~

### UpdateObservers

DownloadOfflineContentProvider::UpdateObservers
~~~cpp
void UpdateObservers(const OfflineItem& item,
                       const absl::optional<UpdateDelta>& update_delta);
~~~

### CheckForExternallyRemovedDownloads

DownloadOfflineContentProvider::CheckForExternallyRemovedDownloads
~~~cpp
void CheckForExternallyRemovedDownloads();
~~~

### EnsureDownloadCoreServiceStarted

DownloadOfflineContentProvider::EnsureDownloadCoreServiceStarted
~~~cpp
void EnsureDownloadCoreServiceStarted();
~~~
 Ensure that download core service is started.

### RunGetAllItemsCallback

DownloadOfflineContentProvider::RunGetAllItemsCallback
~~~cpp
void RunGetAllItemsCallback(
      OfflineContentProvider::MultipleItemCallback callback);
~~~
 Helper method to run callbacks with the latest download information.

### RunGetItemByIdCallback

DownloadOfflineContentProvider::RunGetItemByIdCallback
~~~cpp
void RunGetItemByIdCallback(
      const ContentId& id,
      OfflineContentProvider::SingleItemCallback callback);
~~~

### aggregator_



~~~cpp

raw_ptr<OfflineContentAggregator> aggregator_;

~~~


### name_space_



~~~cpp

std::string name_space_;

~~~


### manager_



~~~cpp

raw_ptr<SimpleDownloadManagerCoordinator> manager_;

~~~


### all_download_observer_



~~~cpp

std::unique_ptr<download::AllDownloadEventNotifier::Observer>
      all_download_observer_;

~~~


### checked_for_externally_removed_downloads_



~~~cpp

bool checked_for_externally_removed_downloads_;

~~~


### state_



~~~cpp

State state_;

~~~


### pending_actions_for_reduced_mode_



~~~cpp

base::circular_deque<base::OnceClosure> pending_actions_for_reduced_mode_;

~~~


### pending_actions_for_full_browser_



~~~cpp

base::circular_deque<base::OnceClosure> pending_actions_for_full_browser_;

~~~


### profile_



~~~cpp

raw_ptr<Profile> profile_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadOfflineContentProvider> weak_ptr_factory_{this};

~~~

