
## class DownloadStatusUpdater
 Keeps track of download progress for the entire browser.

### DownloadStatusUpdater

DownloadStatusUpdater::DownloadStatusUpdater
~~~cpp
DownloadStatusUpdater();
~~~

### DownloadStatusUpdater

DownloadStatusUpdater
~~~cpp
DownloadStatusUpdater(const DownloadStatusUpdater&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadStatusUpdater& operator=(const DownloadStatusUpdater&) = delete;
~~~

### ~DownloadStatusUpdater

DownloadStatusUpdater::~DownloadStatusUpdater
~~~cpp
~DownloadStatusUpdater() override;
~~~

### GetProgress

DownloadStatusUpdater::GetProgress
~~~cpp
bool GetProgress(float* progress, int* download_count) const;
~~~
 Fills in |*download_count| with the number of currently active downloads.

 If we know the final size of all downloads, this routine returns true
 with |*progress| set to the percentage complete of all in-progress
 downloads.  Otherwise, it returns false.

### AddManager

DownloadStatusUpdater::AddManager
~~~cpp
void AddManager(content::DownloadManager* manager);
~~~
 Add the specified DownloadManager to the list of managers for which
 this object reports status.

 The manager must not have previously been added to this updater.

 The updater will automatically disassociate itself from the
 manager when the manager is shutdown.

### OnManagerGoingDown

DownloadStatusUpdater::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~
 AllDownloadItemNotifier::Observer
### OnDownloadCreated

DownloadStatusUpdater::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadStatusUpdater::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### UpdateAppIconDownloadProgress

DownloadStatusUpdater::UpdateAppIconDownloadProgress
~~~cpp
virtual void UpdateAppIconDownloadProgress(download::DownloadItem* download);
~~~
 Platform-specific function to update the platform UI for download progress.

 |download| is the download item that changed. Implementations should not
 hold the value of |download| as it is not guaranteed to remain valid.

 Virtual to be overridable for testing.

### UpdateProfileKeepAlive

DownloadStatusUpdater::UpdateProfileKeepAlive
~~~cpp
void UpdateProfileKeepAlive(content::DownloadManager* manager);
~~~
 Updates the ScopedProfileKeepAlive for the profile tied to |manager|. If
 there are in-progress downloads, it will acquire a keepalive. Otherwise, it
 will release it.


 This prevents deleting the Profile* too early when there are still
 in-progress downloads, and the browser is not tearing down yet.

### UpdatePrefsOnDownloadUpdated

DownloadStatusUpdater::UpdatePrefsOnDownloadUpdated
~~~cpp
void UpdatePrefsOnDownloadUpdated(content::DownloadManager* manager,
                                    download::DownloadItem* download);
~~~
 Updates the download prefs when downloads are updated.

### notifiers_



~~~cpp

std::vector<std::unique_ptr<download::AllDownloadItemNotifier>> notifiers_;

~~~


### profile_keep_alives_



~~~cpp

std::map<Profile*, std::unique_ptr<ScopedProfileKeepAlive>>
      profile_keep_alives_;

~~~


### DownloadStatusUpdater

DownloadStatusUpdater
~~~cpp
DownloadStatusUpdater(const DownloadStatusUpdater&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadStatusUpdater& operator=(const DownloadStatusUpdater&) = delete;
~~~

### GetProgress

DownloadStatusUpdater::GetProgress
~~~cpp
bool GetProgress(float* progress, int* download_count) const;
~~~
 Fills in |*download_count| with the number of currently active downloads.

 If we know the final size of all downloads, this routine returns true
 with |*progress| set to the percentage complete of all in-progress
 downloads.  Otherwise, it returns false.

### AddManager

DownloadStatusUpdater::AddManager
~~~cpp
void AddManager(content::DownloadManager* manager);
~~~
 Add the specified DownloadManager to the list of managers for which
 this object reports status.

 The manager must not have previously been added to this updater.

 The updater will automatically disassociate itself from the
 manager when the manager is shutdown.

### OnManagerGoingDown

DownloadStatusUpdater::OnManagerGoingDown
~~~cpp
void OnManagerGoingDown(content::DownloadManager* manager) override;
~~~
 AllDownloadItemNotifier::Observer
### OnDownloadCreated

DownloadStatusUpdater::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadStatusUpdater::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### UpdateAppIconDownloadProgress

DownloadStatusUpdater::UpdateAppIconDownloadProgress
~~~cpp
virtual void UpdateAppIconDownloadProgress(download::DownloadItem* download);
~~~
 Platform-specific function to update the platform UI for download progress.

 |download| is the download item that changed. Implementations should not
 hold the value of |download| as it is not guaranteed to remain valid.

 Virtual to be overridable for testing.

### UpdateProfileKeepAlive

DownloadStatusUpdater::UpdateProfileKeepAlive
~~~cpp
void UpdateProfileKeepAlive(content::DownloadManager* manager);
~~~
 Updates the ScopedProfileKeepAlive for the profile tied to |manager|. If
 there are in-progress downloads, it will acquire a keepalive. Otherwise, it
 will release it.


 This prevents deleting the Profile* too early when there are still
 in-progress downloads, and the browser is not tearing down yet.

### UpdatePrefsOnDownloadUpdated

DownloadStatusUpdater::UpdatePrefsOnDownloadUpdated
~~~cpp
void UpdatePrefsOnDownloadUpdated(content::DownloadManager* manager,
                                    download::DownloadItem* download);
~~~
 Updates the download prefs when downloads are updated.

### notifiers_



~~~cpp

std::vector<std::unique_ptr<download::AllDownloadItemNotifier>> notifiers_;

~~~


### profile_keep_alives_



~~~cpp

std::map<Profile*, std::unique_ptr<ScopedProfileKeepAlive>>
      profile_keep_alives_;

~~~

