
## class DownloadManagerUtils

### DownloadManagerUtils

DownloadManagerUtils
~~~cpp
DownloadManagerUtils(const DownloadManagerUtils&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadManagerUtils& operator=(const DownloadManagerUtils&) = delete;
~~~

### RetrieveInProgressDownloadManager

DownloadManagerUtils::RetrieveInProgressDownloadManager
~~~cpp
static std::unique_ptr<download::InProgressDownloadManager>
  RetrieveInProgressDownloadManager(Profile* profile);
~~~
 Creates an InProgressDownloadManager from a profile.

### InitializeSimpleDownloadManager

DownloadManagerUtils::InitializeSimpleDownloadManager
~~~cpp
static void InitializeSimpleDownloadManager(ProfileKey* key);
~~~
 Initializes the SimpleDownloadManager that is associated with |key| whenver
 possible.

### GetInProgressDownloadManager

DownloadManagerUtils::GetInProgressDownloadManager
~~~cpp
static download::InProgressDownloadManager* GetInProgressDownloadManager(
      ProfileKey* key);
~~~
 Creates an InProgressDownloadManager for a particular |key| if it doesn't
 exist and return the pointer.

### SetRetrieveInProgressDownloadManagerCallbackForTesting

DownloadManagerUtils::SetRetrieveInProgressDownloadManagerCallbackForTesting
~~~cpp
static void SetRetrieveInProgressDownloadManagerCallbackForTesting(
      base::RepeatingCallback<void(download::InProgressDownloadManager*)>
          callback);
~~~
 Registers a `callback` to be run during subsequent invocations of
 `RetrieveInProgressDownloadManager()`, providing an opportunity to cache
 a pointer to the in progress download manager being released.

### DownloadManagerUtils

DownloadManagerUtils
~~~cpp
DownloadManagerUtils(const DownloadManagerUtils&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadManagerUtils& operator=(const DownloadManagerUtils&) = delete;
~~~

### RetrieveInProgressDownloadManager

DownloadManagerUtils::RetrieveInProgressDownloadManager
~~~cpp
static std::unique_ptr<download::InProgressDownloadManager>
  RetrieveInProgressDownloadManager(Profile* profile);
~~~
 Creates an InProgressDownloadManager from a profile.

### InitializeSimpleDownloadManager

DownloadManagerUtils::InitializeSimpleDownloadManager
~~~cpp
static void InitializeSimpleDownloadManager(ProfileKey* key);
~~~
 Initializes the SimpleDownloadManager that is associated with |key| whenver
 possible.

### GetInProgressDownloadManager

DownloadManagerUtils::GetInProgressDownloadManager
~~~cpp
static download::InProgressDownloadManager* GetInProgressDownloadManager(
      ProfileKey* key);
~~~
 Creates an InProgressDownloadManager for a particular |key| if it doesn't
 exist and return the pointer.

### SetRetrieveInProgressDownloadManagerCallbackForTesting

DownloadManagerUtils::SetRetrieveInProgressDownloadManagerCallbackForTesting
~~~cpp
static void SetRetrieveInProgressDownloadManagerCallbackForTesting(
      base::RepeatingCallback<void(download::InProgressDownloadManager*)>
          callback);
~~~
 Registers a `callback` to be run during subsequent invocations of
 `RetrieveInProgressDownloadManager()`, providing an opportunity to cache
 a pointer to the in progress download manager being released.
