
## class DownloadTargetDeterminer
 Determines the target of the download.


 Terminology:
   Virtual Path: A path representing the target of the download that may or
     may not be a physical file path. E.g. if the target of the download is in
     cloud storage, then the virtual path may be relative to a logical mount
     point.


   Local Path: A local file system path where the downloads system should
     write the file to.


   Intermediate Path: Where the data should be written to during the course of
     the download. Once the download completes, the file could be renamed to
     Local Path.


 DownloadTargetDeterminer is a self owned object that performs the work of
 determining the download target. It observes the DownloadItem and aborts the
 process if the download is removed. DownloadTargetDeterminerDelegate is
 responsible for providing external dependencies and prompting the user if
 necessary.


 The only public entrypoint is the static Start() method which creates an
 instance of DownloadTargetDeterminer.

### DownloadTargetDeterminer

DownloadTargetDeterminer
~~~cpp
DownloadTargetDeterminer(const DownloadTargetDeterminer&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadTargetDeterminer& operator=(const DownloadTargetDeterminer&) = delete;
~~~

### Start

DownloadTargetDeterminer::Start
~~~cpp
static void Start(
      download::DownloadItem* download,
      const base::FilePath& initial_virtual_path,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      DownloadPrefs* download_prefs,
      DownloadTargetDeterminerDelegate* delegate,
      CompletionCallback callback);
~~~
 Start the process of determing the target of |download|.


 |initial_virtual_path| if non-empty, defines the initial virtual path for
   the target determination process. If one isn't specified, one will be
   generated based on the response data specified in |download| and the
   users' downloads directory.

   Note: |initial_virtual_path| is only used if download has prompted the
       user before and doesn't have a forced path.

 |download_prefs| is required and must outlive |download|. It is used for
   determining the user's preferences regarding the default downloads
   directory, prompting and auto-open behavior.

 |delegate| is required and must live until |callback| is invoked.

 |callback| will be scheduled asynchronously on the UI thread after download
   determination is complete or after |download| is destroyed.


 Start() should be called on the UI thread.

### GetCrDownloadPath

DownloadTargetDeterminer::GetCrDownloadPath
~~~cpp
static base::FilePath GetCrDownloadPath(const base::FilePath& suggested_path);
~~~
 Returns a .crdownload intermediate path for the |suggested_path|.

### IsAdobeReaderUpToDate

DownloadTargetDeterminer::IsAdobeReaderUpToDate
~~~cpp
static bool IsAdobeReaderUpToDate();
~~~
 Returns true if Adobe Reader is up to date. This information refreshed
 only when Start() gets called for a PDF and Adobe Reader is the default
 System PDF viewer.

### DetermineIfHandledSafelyHelper

DownloadTargetDeterminer::DetermineIfHandledSafelyHelper
~~~cpp
static void DetermineIfHandledSafelyHelper(
      download::DownloadItem* download,
      const base::FilePath& local_path,
      const std::string& mime_type,
      base::OnceCallback<void(bool)> callback);
~~~
 Determine if the file type can be handled safely by the browser if it were
 to be opened via a file:// URL. Execute the callback with the determined
 value.

### DetermineIfHandledSafelyHelperSynchronous

DownloadTargetDeterminer::DetermineIfHandledSafelyHelperSynchronous
~~~cpp
static bool DetermineIfHandledSafelyHelperSynchronous(
      download::DownloadItem* download,
      const base::FilePath& local_path,
      const std::string& mime_type);
~~~
 Determine if the file type can be handled safely by the browser if it were
 to be opened via a file:// URL. Returns the determined value.

### enum State

~~~cpp
enum State {
    STATE_GENERATE_TARGET_PATH,
    STATE_SET_INSECURE_DOWNLOAD_STATUS,
    STATE_NOTIFY_EXTENSIONS,
    STATE_RESERVE_VIRTUAL_PATH,
    STATE_PROMPT_USER_FOR_DOWNLOAD_PATH,
    STATE_DETERMINE_LOCAL_PATH,
    STATE_DETERMINE_MIME_TYPE,
    STATE_DETERMINE_IF_HANDLED_SAFELY_BY_BROWSER,
    STATE_DETERMINE_IF_ADOBE_READER_UP_TO_DATE,
    STATE_CHECK_DOWNLOAD_URL,
    STATE_CHECK_VISITED_REFERRER_BEFORE,
    STATE_DETERMINE_INTERMEDIATE_PATH,
    STATE_NONE,
  }
~~~
### enum Result

~~~cpp
enum Result {
    // Continue processing. next_state_ is required to not be STATE_NONE.
    CONTINUE,

    // The DoLoop() that invoked the handler should exit. This value is
    // typically returned when the handler has invoked an asynchronous operation
    // and is expecting a callback. If a handler returns this value, it has
    // taken responsibility for ensuring that DoLoop() is invoked. It is
    // possible that the handler has invoked another DoLoop() already.
    QUIT_DOLOOP,

    // Target determination is complete.
    COMPLETE
  }
~~~
### enum PriorVisitsToReferrer

~~~cpp
enum PriorVisitsToReferrer {
    NO_VISITS_TO_REFERRER,
    VISITED_REFERRER,
  }
~~~
### DownloadTargetDeterminer

DownloadTargetDeterminer::DownloadTargetDeterminer
~~~cpp
DownloadTargetDeterminer(
      download::DownloadItem* download,
      const base::FilePath& initial_virtual_path,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      DownloadPrefs* download_prefs,
      DownloadTargetDeterminerDelegate* delegate,
      CompletionCallback callback);
~~~
 Construct a DownloadTargetDeterminer object. Constraints on the arguments
 are as per Start() above.

### ~DownloadTargetDeterminer

DownloadTargetDeterminer::~DownloadTargetDeterminer
~~~cpp
~DownloadTargetDeterminer() override;
~~~

### DoLoop

DownloadTargetDeterminer::DoLoop
~~~cpp
void DoLoop();
~~~
 Invoke each successive handler until a handler returns QUIT_DOLOOP or
 COMPLETE. Note that as a result, this object might be deleted. So |this|
 should not be accessed after calling DoLoop().

### DoGenerateTargetPath

DownloadTargetDeterminer::DoGenerateTargetPath
~~~cpp
Result DoGenerateTargetPath();
~~~
 === Main workflow ===
 Generates an initial target path. This target is based only on the state of
 the download item.

 Next state:
 - STATE_NONE : If the download is not in progress, returns COMPLETE.

 - STATE_SET_INSECURE_DOWNLOAD_STATUS : All other downloads.

### DoSetInsecureDownloadStatus

DownloadTargetDeterminer::DoSetInsecureDownloadStatus
~~~cpp
Result DoSetInsecureDownloadStatus();
~~~
 Determines the insecure download status of the download, so as to block it
 prior to prompting the user for the file path.  This function relies on the
 delegate for the actual determination.


 Next state:
 - STATE_NOTIFY_EXTENSIONS
### GetInsecureDownloadStatusDone

DownloadTargetDeterminer::GetInsecureDownloadStatusDone
~~~cpp
void GetInsecureDownloadStatusDone(
      download::DownloadItem::InsecureDownloadStatus status);
~~~
 Callback invoked by delegate after insecure download status is determined.

 Cancels the download if status indicates blocking is necessary.

### DoNotifyExtensions

DownloadTargetDeterminer::DoNotifyExtensions
~~~cpp
Result DoNotifyExtensions();
~~~
 Notifies downloads extensions. If any extension wishes to override the
 download filename, it will respond to the OnDeterminingFilename()
 notification.

 Next state:
 - STATE_RESERVE_VIRTUAL_PATH.

### NotifyExtensionsDone

DownloadTargetDeterminer::NotifyExtensionsDone
~~~cpp
void NotifyExtensionsDone(
      const base::FilePath& new_path,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action);
~~~
 Callback invoked after extensions are notified. Updates |virtual_path_| and
 |conflict_action_|.

### DoReserveVirtualPath

DownloadTargetDeterminer::DoReserveVirtualPath
~~~cpp
Result DoReserveVirtualPath();
~~~
 Invokes ReserveVirtualPath() on the delegate to acquire a reservation for
 the path. See DownloadPathReservationTracker.

 Next state:
 - STATE_PROMPT_USER_FOR_DOWNLOAD_PATH.

### ReserveVirtualPathDone

DownloadTargetDeterminer::ReserveVirtualPathDone
~~~cpp
void ReserveVirtualPathDone(download::PathValidationResult result,
                              const base::FilePath& path);
~~~
 Callback invoked after the delegate aquires a path reservation.

### DoRequestConfirmation

DownloadTargetDeterminer::DoRequestConfirmation
~~~cpp
Result DoRequestConfirmation();
~~~
 Presents a file picker to the user if necessary.

 Next state:
 - STATE_DETERMINE_LOCAL_PATH.

### RequestConfirmationDone

DownloadTargetDeterminer::RequestConfirmationDone
~~~cpp
void RequestConfirmationDone(DownloadConfirmationResult result,
                               const base::FilePath& virtual_path);
~~~
 Callback invoked after the file picker completes. Cancels the download if
 the user cancels the file picker.

### RequestIncognitoWarningConfirmationDone

DownloadTargetDeterminer::RequestIncognitoWarningConfirmationDone
~~~cpp
void RequestIncognitoWarningConfirmationDone(bool accepted);
~~~
 Callback invoked after the incognito message has been accepted/rejected
 from the user.

### DoDetermineLocalPath

DownloadTargetDeterminer::DoDetermineLocalPath
~~~cpp
Result DoDetermineLocalPath();
~~~
 Up until this point, the path that was used is considered to be a virtual
 path. This step determines the local file system path corresponding to this
 virtual path. The translation is done by invoking the DetermineLocalPath()
 method on the delegate.

 Next state:
 - STATE_DETERMINE_MIME_TYPE.

### DetermineLocalPathDone

DownloadTargetDeterminer::DetermineLocalPathDone
~~~cpp
void DetermineLocalPathDone(const base::FilePath& local_path,
                              const base::FilePath& file_name);
~~~
 Callback invoked when the delegate has determined local path. |file_name|
 is supplied in case it cannot be determined from local_path (e.g. local
 path is a content Uri: content://media/12345). |file_name| could be empty
 if it is the last component of |local_path|.

### DoDetermineMimeType

DownloadTargetDeterminer::DoDetermineMimeType
~~~cpp
Result DoDetermineMimeType();
~~~
 Determine the MIME type corresponding to the local file path. This is only
 done if the local path and the virtual path was the same. I.e. The file is
 intended for the local file system. This restriction is there because the
 resulting MIME type is only valid for determining whether the browser can
 handle the download if it were opened via a file:// URL.

 Next state:
 - STATE_DETERMINE_IF_HANDLED_SAFELY_BY_BROWSER.

### DetermineMimeTypeDone

DownloadTargetDeterminer::DetermineMimeTypeDone
~~~cpp
void DetermineMimeTypeDone(const std::string& mime_type);
~~~
 Callback invoked when the MIME type is available. Since determination of
 the MIME type can involve disk access, it is done in the blocking pool.

### DoDetermineIfHandledSafely

DownloadTargetDeterminer::DoDetermineIfHandledSafely
~~~cpp
Result DoDetermineIfHandledSafely();
~~~
 Determine if the file type can be handled safely by the browser if it were
 to be opened via a file:// URL.

 Next state:
 - STATE_DETERMINE_IF_ADOBE_READER_UP_TO_DATE.

### DetermineIfHandledSafelyDone

DownloadTargetDeterminer::DetermineIfHandledSafelyDone
~~~cpp
void DetermineIfHandledSafelyDone(bool is_handled_safely);
~~~
 Callback invoked when a decision is available about whether the file type
 can be handled safely by the browser.

### DoDetermineIfAdobeReaderUpToDate

DownloadTargetDeterminer::DoDetermineIfAdobeReaderUpToDate
~~~cpp
Result DoDetermineIfAdobeReaderUpToDate();
~~~
 Determine if Adobe Reader is up to date. Only do the check on Windows for
 .pdf file targets.

 Next state:
 - STATE_CHECK_DOWNLOAD_URL.

### DetermineIfAdobeReaderUpToDateDone

DownloadTargetDeterminer::DetermineIfAdobeReaderUpToDateDone
~~~cpp
void DetermineIfAdobeReaderUpToDateDone(bool adobe_reader_up_to_date);
~~~
 Callback invoked when a decision is available about whether Adobe Reader
 is up to date.

### DoCheckDownloadUrl

DownloadTargetDeterminer::DoCheckDownloadUrl
~~~cpp
Result DoCheckDownloadUrl();
~~~
 Checks whether the downloaded URL is malicious. Invokes the
 DownloadProtectionService via the delegate.

 Next state:
 - STATE_CHECK_VISITED_REFERRER_BEFORE.

### CheckDownloadUrlDone

DownloadTargetDeterminer::CheckDownloadUrlDone
~~~cpp
void CheckDownloadUrlDone(download::DownloadDangerType danger_type);
~~~
 Callback invoked after the delegate has checked the download URL. Sets the
 danger type of the download to |danger_type|.

### DoCheckVisitedReferrerBefore

DownloadTargetDeterminer::DoCheckVisitedReferrerBefore
~~~cpp
Result DoCheckVisitedReferrerBefore();
~~~
 Checks if the user has visited the referrer URL of the download prior to
 today. The actual check is only performed if it would be needed to
 determine the danger type of the download.

 Next state:
 - STATE_DETERMINE_INTERMEDIATE_PATH.

### CheckVisitedReferrerBeforeDone

DownloadTargetDeterminer::CheckVisitedReferrerBeforeDone
~~~cpp
void CheckVisitedReferrerBeforeDone(bool visited_referrer_before);
~~~
 Callback invoked after completion of history check for prior visits to
 referrer URL.

### DoDetermineIntermediatePath

DownloadTargetDeterminer::DoDetermineIntermediatePath
~~~cpp
Result DoDetermineIntermediatePath();
~~~
 Determines the intermediate path. Once this step completes, downloads
 target determination is complete. The determination assumes that the
 intermediate file will never be overwritten (always uniquified if needed).

 Next state:
 - STATE_NONE: Returns COMPLETE.

### ScheduleCallbackAndDeleteSelf

DownloadTargetDeterminer::ScheduleCallbackAndDeleteSelf
~~~cpp
void ScheduleCallbackAndDeleteSelf(download::DownloadInterruptReason result);
~~~
 === End of main workflow ===
 Utilities:
 Schedules the completion callback to be run on the UI thread and deletes
 this object. The determined target info will be passed into the callback
 if |interrupt_reason| is NONE. Otherwise, only the interrupt reason will be
 passed on.

### GetProfile

DownloadTargetDeterminer::GetProfile
~~~cpp
Profile* GetProfile() const;
~~~

### NeedsConfirmation

DownloadTargetDeterminer::NeedsConfirmation
~~~cpp
DownloadConfirmationReason NeedsConfirmation(
      const base::FilePath& filename) const;
~~~
 Determine if the download requires confirmation from the user. For regular
 downloads, this determination is based on the target disposition, auto-open
 behavior, among other factors. For an interrupted download, this
 determination will be based on the interrupt reason. It is assumed that
 download interruptions always occur after the first round of download
 target determination is complete.

### IsDownloadDlpBlocked

DownloadTargetDeterminer::IsDownloadDlpBlocked
~~~cpp
bool IsDownloadDlpBlocked(const base::FilePath& download_path) const;
~~~
 Returns true if the DLP feature is enabled and downloading the item to
 `download_path` is blocked, in which case the user should be prompted
 regardless of the preferences.

### HasPromptedForPath

DownloadTargetDeterminer::HasPromptedForPath
~~~cpp
bool HasPromptedForPath() const;
~~~
 Returns true if the user has been prompted for this download at least once
 prior to this target determination operation. This method is only expected
 to return true for a resuming interrupted download that has prompted the
 user before interruption. The return value does not depend on whether the
 user will be or has been prompted during the current target determination
 operation.

### GetDangerLevel

DownloadTargetDeterminer::GetDangerLevel
~~~cpp
safe_browsing::DownloadFileType::DangerLevel GetDangerLevel(
      PriorVisitsToReferrer visits) const;
~~~
 Returns true if this download should show the "dangerous file" warning.

 Various factors are considered, such as the type of the file, whether a
 user action initiated the download, and whether the user has explicitly
 marked the file type as "auto open". Protected virtual for testing.


 If |require_explicit_consent| is non-null then the pointed bool will be set
 to true if the download requires explicit user consent.

### GetLastDownloadBypassTimestamp

DownloadTargetDeterminer::GetLastDownloadBypassTimestamp
~~~cpp
absl::optional<base::Time> GetLastDownloadBypassTimestamp() const;
~~~
 Returns the timestamp of the last download bypass.

### GenerateFileName

DownloadTargetDeterminer::GenerateFileName
~~~cpp
base::FilePath GenerateFileName() const;
~~~
 Generates the download file name based on information from URL, response
 headers and sniffed mime type.

### OnDownloadDestroyed

DownloadTargetDeterminer::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer
### next_state_



~~~cpp

State next_state_;

~~~

 state
### confirmation_reason_



~~~cpp

DownloadConfirmationReason confirmation_reason_;

~~~


### should_notify_extensions_



~~~cpp

bool should_notify_extensions_;

~~~


### create_target_directory_



~~~cpp

bool create_target_directory_;

~~~


### conflict_action_



~~~cpp

download::DownloadPathReservationTracker::FilenameConflictAction
      conflict_action_;

~~~


### danger_type_



~~~cpp

download::DownloadDangerType danger_type_;

~~~


### danger_level_



~~~cpp

safe_browsing::DownloadFileType::DangerLevel danger_level_;

~~~


### virtual_path_



~~~cpp

base::FilePath virtual_path_;

~~~


### local_path_



~~~cpp

base::FilePath local_path_;

~~~


### intermediate_path_



~~~cpp

base::FilePath intermediate_path_;

~~~


### mime_type_



~~~cpp

std::string mime_type_;

~~~


### is_filetype_handled_safely_



~~~cpp

bool is_filetype_handled_safely_ = false;

~~~


### insecure_download_status_



~~~cpp

download::DownloadItem::InsecureDownloadStatus insecure_download_status_;

~~~


### is_checking_dialog_confirmed_path_



~~~cpp

bool is_checking_dialog_confirmed_path_;

~~~


### download_



~~~cpp

raw_ptr<download::DownloadItem> download_;

~~~


### is_resumption_



~~~cpp

const bool is_resumption_;

~~~


### download_prefs_



~~~cpp

raw_ptr<DownloadPrefs> download_prefs_;

~~~


### delegate_



~~~cpp

raw_ptr<DownloadTargetDeterminerDelegate> delegate_;

~~~


### completion_callback_



~~~cpp

CompletionCallback completion_callback_;

~~~


### history_tracker_



~~~cpp

base::CancelableTaskTracker history_tracker_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadTargetDeterminer> weak_ptr_factory_{this};

~~~


### DownloadTargetDeterminer

DownloadTargetDeterminer
~~~cpp
DownloadTargetDeterminer(const DownloadTargetDeterminer&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadTargetDeterminer& operator=(const DownloadTargetDeterminer&) = delete;
~~~

### Start

DownloadTargetDeterminer::Start
~~~cpp
static void Start(
      download::DownloadItem* download,
      const base::FilePath& initial_virtual_path,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action,
      DownloadPrefs* download_prefs,
      DownloadTargetDeterminerDelegate* delegate,
      CompletionCallback callback);
~~~
 Start the process of determing the target of |download|.


 |initial_virtual_path| if non-empty, defines the initial virtual path for
   the target determination process. If one isn't specified, one will be
   generated based on the response data specified in |download| and the
   users' downloads directory.

   Note: |initial_virtual_path| is only used if download has prompted the
       user before and doesn't have a forced path.

 |download_prefs| is required and must outlive |download|. It is used for
   determining the user's preferences regarding the default downloads
   directory, prompting and auto-open behavior.

 |delegate| is required and must live until |callback| is invoked.

 |callback| will be scheduled asynchronously on the UI thread after download
   determination is complete or after |download| is destroyed.


 Start() should be called on the UI thread.

### GetCrDownloadPath

DownloadTargetDeterminer::GetCrDownloadPath
~~~cpp
static base::FilePath GetCrDownloadPath(const base::FilePath& suggested_path);
~~~
 Returns a .crdownload intermediate path for the |suggested_path|.

### DetermineIfHandledSafelyHelper

DownloadTargetDeterminer::DetermineIfHandledSafelyHelper
~~~cpp
static void DetermineIfHandledSafelyHelper(
      download::DownloadItem* download,
      const base::FilePath& local_path,
      const std::string& mime_type,
      base::OnceCallback<void(bool)> callback);
~~~
 Determine if the file type can be handled safely by the browser if it were
 to be opened via a file:// URL. Execute the callback with the determined
 value.

### DetermineIfHandledSafelyHelperSynchronous

DownloadTargetDeterminer::DetermineIfHandledSafelyHelperSynchronous
~~~cpp
static bool DetermineIfHandledSafelyHelperSynchronous(
      download::DownloadItem* download,
      const base::FilePath& local_path,
      const std::string& mime_type);
~~~
 Determine if the file type can be handled safely by the browser if it were
 to be opened via a file:// URL. Returns the determined value.

### enum State
 The main workflow is controlled via a set of state transitions. Each state
 has an associated handler. The handler for STATE_FOO is DoFoo. Each handler
 performs work, determines the next state to transition to and returns a
 Result indicating how the workflow should proceed. The loop ends when a
 handler returns COMPLETE.

~~~cpp
enum State {
    STATE_GENERATE_TARGET_PATH,
    STATE_SET_INSECURE_DOWNLOAD_STATUS,
    STATE_NOTIFY_EXTENSIONS,
    STATE_RESERVE_VIRTUAL_PATH,
    STATE_PROMPT_USER_FOR_DOWNLOAD_PATH,
    STATE_DETERMINE_LOCAL_PATH,
    STATE_DETERMINE_MIME_TYPE,
    STATE_DETERMINE_IF_HANDLED_SAFELY_BY_BROWSER,
    STATE_DETERMINE_IF_ADOBE_READER_UP_TO_DATE,
    STATE_CHECK_DOWNLOAD_URL,
    STATE_CHECK_VISITED_REFERRER_BEFORE,
    STATE_DETERMINE_INTERMEDIATE_PATH,
    STATE_NONE,
  };
~~~
### enum Result
 Result code returned by each step of the workflow below. Controls execution
 of DoLoop().

~~~cpp
enum Result {
    // Continue processing. next_state_ is required to not be STATE_NONE.
    CONTINUE,

    // The DoLoop() that invoked the handler should exit. This value is
    // typically returned when the handler has invoked an asynchronous operation
    // and is expecting a callback. If a handler returns this value, it has
    // taken responsibility for ensuring that DoLoop() is invoked. It is
    // possible that the handler has invoked another DoLoop() already.
    QUIT_DOLOOP,

    // Target determination is complete.
    COMPLETE
  };
~~~
### enum PriorVisitsToReferrer
 Used with GetDangerLevel to indicate whether the user has visited the
 referrer URL for the download prior to today.

~~~cpp
enum PriorVisitsToReferrer {
    NO_VISITS_TO_REFERRER,
    VISITED_REFERRER,
  };
~~~
### DoLoop

DownloadTargetDeterminer::DoLoop
~~~cpp
void DoLoop();
~~~
 Invoke each successive handler until a handler returns QUIT_DOLOOP or
 COMPLETE. Note that as a result, this object might be deleted. So |this|
 should not be accessed after calling DoLoop().

### DoGenerateTargetPath

DownloadTargetDeterminer::DoGenerateTargetPath
~~~cpp
Result DoGenerateTargetPath();
~~~
 === Main workflow ===
 Generates an initial target path. This target is based only on the state of
 the download item.

 Next state:
 - STATE_NONE : If the download is not in progress, returns COMPLETE.

 - STATE_SET_INSECURE_DOWNLOAD_STATUS : All other downloads.

### DoSetInsecureDownloadStatus

DownloadTargetDeterminer::DoSetInsecureDownloadStatus
~~~cpp
Result DoSetInsecureDownloadStatus();
~~~
 Determines the insecure download status of the download, so as to block it
 prior to prompting the user for the file path.  This function relies on the
 delegate for the actual determination.


 Next state:
 - STATE_NOTIFY_EXTENSIONS
### GetInsecureDownloadStatusDone

DownloadTargetDeterminer::GetInsecureDownloadStatusDone
~~~cpp
void GetInsecureDownloadStatusDone(
      download::DownloadItem::InsecureDownloadStatus status);
~~~
 Callback invoked by delegate after insecure download status is determined.

 Cancels the download if status indicates blocking is necessary.

### DoNotifyExtensions

DownloadTargetDeterminer::DoNotifyExtensions
~~~cpp
Result DoNotifyExtensions();
~~~
 Notifies downloads extensions. If any extension wishes to override the
 download filename, it will respond to the OnDeterminingFilename()
 notification.

 Next state:
 - STATE_RESERVE_VIRTUAL_PATH.

### NotifyExtensionsDone

DownloadTargetDeterminer::NotifyExtensionsDone
~~~cpp
void NotifyExtensionsDone(
      const base::FilePath& new_path,
      download::DownloadPathReservationTracker::FilenameConflictAction
          conflict_action);
~~~
 Callback invoked after extensions are notified. Updates |virtual_path_| and
 |conflict_action_|.

### DoReserveVirtualPath

DownloadTargetDeterminer::DoReserveVirtualPath
~~~cpp
Result DoReserveVirtualPath();
~~~
 Invokes ReserveVirtualPath() on the delegate to acquire a reservation for
 the path. See DownloadPathReservationTracker.

 Next state:
 - STATE_PROMPT_USER_FOR_DOWNLOAD_PATH.

### ReserveVirtualPathDone

DownloadTargetDeterminer::ReserveVirtualPathDone
~~~cpp
void ReserveVirtualPathDone(download::PathValidationResult result,
                              const base::FilePath& path);
~~~
 Callback invoked after the delegate aquires a path reservation.

### DoRequestConfirmation

DownloadTargetDeterminer::DoRequestConfirmation
~~~cpp
Result DoRequestConfirmation();
~~~
 Presents a file picker to the user if necessary.

 Next state:
 - STATE_DETERMINE_LOCAL_PATH.

### RequestConfirmationDone

DownloadTargetDeterminer::RequestConfirmationDone
~~~cpp
void RequestConfirmationDone(DownloadConfirmationResult result,
                               const base::FilePath& virtual_path);
~~~
 Callback invoked after the file picker completes. Cancels the download if
 the user cancels the file picker.

### DoDetermineLocalPath

DownloadTargetDeterminer::DoDetermineLocalPath
~~~cpp
Result DoDetermineLocalPath();
~~~
 Up until this point, the path that was used is considered to be a virtual
 path. This step determines the local file system path corresponding to this
 virtual path. The translation is done by invoking the DetermineLocalPath()
 method on the delegate.

 Next state:
 - STATE_DETERMINE_MIME_TYPE.

### DetermineLocalPathDone

DownloadTargetDeterminer::DetermineLocalPathDone
~~~cpp
void DetermineLocalPathDone(const base::FilePath& local_path,
                              const base::FilePath& file_name);
~~~
 Callback invoked when the delegate has determined local path. |file_name|
 is supplied in case it cannot be determined from local_path (e.g. local
 path is a content Uri: content://media/12345). |file_name| could be empty
 if it is the last component of |local_path|.

### DoDetermineMimeType

DownloadTargetDeterminer::DoDetermineMimeType
~~~cpp
Result DoDetermineMimeType();
~~~
 Determine the MIME type corresponding to the local file path. This is only
 done if the local path and the virtual path was the same. I.e. The file is
 intended for the local file system. This restriction is there because the
 resulting MIME type is only valid for determining whether the browser can
 handle the download if it were opened via a file:// URL.

 Next state:
 - STATE_DETERMINE_IF_HANDLED_SAFELY_BY_BROWSER.

### DetermineMimeTypeDone

DownloadTargetDeterminer::DetermineMimeTypeDone
~~~cpp
void DetermineMimeTypeDone(const std::string& mime_type);
~~~
 Callback invoked when the MIME type is available. Since determination of
 the MIME type can involve disk access, it is done in the blocking pool.

### DoDetermineIfHandledSafely

DownloadTargetDeterminer::DoDetermineIfHandledSafely
~~~cpp
Result DoDetermineIfHandledSafely();
~~~
 Determine if the file type can be handled safely by the browser if it were
 to be opened via a file:// URL.

 Next state:
 - STATE_DETERMINE_IF_ADOBE_READER_UP_TO_DATE.

### DetermineIfHandledSafelyDone

DownloadTargetDeterminer::DetermineIfHandledSafelyDone
~~~cpp
void DetermineIfHandledSafelyDone(bool is_handled_safely);
~~~
 Callback invoked when a decision is available about whether the file type
 can be handled safely by the browser.

### DoDetermineIfAdobeReaderUpToDate

DownloadTargetDeterminer::DoDetermineIfAdobeReaderUpToDate
~~~cpp
Result DoDetermineIfAdobeReaderUpToDate();
~~~
 Determine if Adobe Reader is up to date. Only do the check on Windows for
 .pdf file targets.

 Next state:
 - STATE_CHECK_DOWNLOAD_URL.

### DoCheckDownloadUrl

DownloadTargetDeterminer::DoCheckDownloadUrl
~~~cpp
Result DoCheckDownloadUrl();
~~~
 Checks whether the downloaded URL is malicious. Invokes the
 DownloadProtectionService via the delegate.

 Next state:
 - STATE_CHECK_VISITED_REFERRER_BEFORE.

### CheckDownloadUrlDone

DownloadTargetDeterminer::CheckDownloadUrlDone
~~~cpp
void CheckDownloadUrlDone(download::DownloadDangerType danger_type);
~~~
 Callback invoked after the delegate has checked the download URL. Sets the
 danger type of the download to |danger_type|.

### DoCheckVisitedReferrerBefore

DownloadTargetDeterminer::DoCheckVisitedReferrerBefore
~~~cpp
Result DoCheckVisitedReferrerBefore();
~~~
 Checks if the user has visited the referrer URL of the download prior to
 today. The actual check is only performed if it would be needed to
 determine the danger type of the download.

 Next state:
 - STATE_DETERMINE_INTERMEDIATE_PATH.

### CheckVisitedReferrerBeforeDone

DownloadTargetDeterminer::CheckVisitedReferrerBeforeDone
~~~cpp
void CheckVisitedReferrerBeforeDone(bool visited_referrer_before);
~~~
 Callback invoked after completion of history check for prior visits to
 referrer URL.

### DoDetermineIntermediatePath

DownloadTargetDeterminer::DoDetermineIntermediatePath
~~~cpp
Result DoDetermineIntermediatePath();
~~~
 Determines the intermediate path. Once this step completes, downloads
 target determination is complete. The determination assumes that the
 intermediate file will never be overwritten (always uniquified if needed).

 Next state:
 - STATE_NONE: Returns COMPLETE.

### ScheduleCallbackAndDeleteSelf

DownloadTargetDeterminer::ScheduleCallbackAndDeleteSelf
~~~cpp
void ScheduleCallbackAndDeleteSelf(download::DownloadInterruptReason result);
~~~
 === End of main workflow ===
 Utilities:
 Schedules the completion callback to be run on the UI thread and deletes
 this object. The determined target info will be passed into the callback
 if |interrupt_reason| is NONE. Otherwise, only the interrupt reason will be
 passed on.

### GetProfile

DownloadTargetDeterminer::GetProfile
~~~cpp
Profile* GetProfile() const;
~~~

### NeedsConfirmation

DownloadTargetDeterminer::NeedsConfirmation
~~~cpp
DownloadConfirmationReason NeedsConfirmation(
      const base::FilePath& filename) const;
~~~
 Determine if the download requires confirmation from the user. For regular
 downloads, this determination is based on the target disposition, auto-open
 behavior, among other factors. For an interrupted download, this
 determination will be based on the interrupt reason. It is assumed that
 download interruptions always occur after the first round of download
 target determination is complete.

### IsDownloadDlpBlocked

DownloadTargetDeterminer::IsDownloadDlpBlocked
~~~cpp
bool IsDownloadDlpBlocked(const base::FilePath& download_path) const;
~~~
 Returns true if the DLP feature is enabled and downloading the item to
 `download_path` is blocked, in which case the user should be prompted
 regardless of the preferences.

### HasPromptedForPath

DownloadTargetDeterminer::HasPromptedForPath
~~~cpp
bool HasPromptedForPath() const;
~~~
 Returns true if the user has been prompted for this download at least once
 prior to this target determination operation. This method is only expected
 to return true for a resuming interrupted download that has prompted the
 user before interruption. The return value does not depend on whether the
 user will be or has been prompted during the current target determination
 operation.

### GetDangerLevel

DownloadTargetDeterminer::GetDangerLevel
~~~cpp
safe_browsing::DownloadFileType::DangerLevel GetDangerLevel(
      PriorVisitsToReferrer visits) const;
~~~
 Returns true if this download should show the "dangerous file" warning.

 Various factors are considered, such as the type of the file, whether a
 user action initiated the download, and whether the user has explicitly
 marked the file type as "auto open". Protected virtual for testing.


 If |require_explicit_consent| is non-null then the pointed bool will be set
 to true if the download requires explicit user consent.

### GetLastDownloadBypassTimestamp

DownloadTargetDeterminer::GetLastDownloadBypassTimestamp
~~~cpp
absl::optional<base::Time> GetLastDownloadBypassTimestamp() const;
~~~
 Returns the timestamp of the last download bypass.

### GenerateFileName

DownloadTargetDeterminer::GenerateFileName
~~~cpp
base::FilePath GenerateFileName() const;
~~~
 Generates the download file name based on information from URL, response
 headers and sniffed mime type.

### OnDownloadDestroyed

DownloadTargetDeterminer::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(download::DownloadItem* download) override;
~~~
 download::DownloadItem::Observer
### next_state_



~~~cpp

State next_state_;

~~~

 state
### confirmation_reason_



~~~cpp

DownloadConfirmationReason confirmation_reason_;

~~~


### should_notify_extensions_



~~~cpp

bool should_notify_extensions_;

~~~


### create_target_directory_



~~~cpp

bool create_target_directory_;

~~~


### conflict_action_



~~~cpp

download::DownloadPathReservationTracker::FilenameConflictAction
      conflict_action_;

~~~


### danger_type_



~~~cpp

download::DownloadDangerType danger_type_;

~~~


### danger_level_



~~~cpp

safe_browsing::DownloadFileType::DangerLevel danger_level_;

~~~


### virtual_path_



~~~cpp

base::FilePath virtual_path_;

~~~


### local_path_



~~~cpp

base::FilePath local_path_;

~~~


### intermediate_path_



~~~cpp

base::FilePath intermediate_path_;

~~~


### mime_type_



~~~cpp

std::string mime_type_;

~~~


### is_filetype_handled_safely_



~~~cpp

bool is_filetype_handled_safely_ = false;

~~~


### insecure_download_status_



~~~cpp

download::DownloadItem::InsecureDownloadStatus insecure_download_status_;

~~~


### download_



~~~cpp

raw_ptr<download::DownloadItem> download_;

~~~


### is_resumption_



~~~cpp

const bool is_resumption_;

~~~


### download_prefs_



~~~cpp

raw_ptr<DownloadPrefs> download_prefs_;

~~~


### delegate_



~~~cpp

raw_ptr<DownloadTargetDeterminerDelegate> delegate_;

~~~


### completion_callback_



~~~cpp

CompletionCallback completion_callback_;

~~~


### history_tracker_



~~~cpp

base::CancelableTaskTracker history_tracker_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<DownloadTargetDeterminer> weak_ptr_factory_{this};

~~~

