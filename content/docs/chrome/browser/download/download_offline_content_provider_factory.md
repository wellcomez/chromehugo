
## class DownloadOfflineContentProviderFactory
 namespace base
 This class builds and associates DownloadOfflineContentProvider with their
 Profiles, represented by SimpleFactoryKeys.

### GetInstance

DownloadOfflineContentProviderFactory::GetInstance
~~~cpp
static DownloadOfflineContentProviderFactory* GetInstance();
~~~
 Returns a singleton instance of an DownloadOfflineContentProviderFactory.

### GetForKey

DownloadOfflineContentProviderFactory::GetForKey
~~~cpp
static DownloadOfflineContentProvider* GetForKey(SimpleFactoryKey* key);
~~~
 Returns the DownloadOfflineContentProvider associated with |key| or creates
 and associates one if it doesn't exist.

### DownloadOfflineContentProviderFactory

DownloadOfflineContentProviderFactory
~~~cpp
DownloadOfflineContentProviderFactory(
      const DownloadOfflineContentProviderFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadOfflineContentProviderFactory& operator=(
      const DownloadOfflineContentProviderFactory&) = delete;
~~~

### DownloadOfflineContentProviderFactory

DownloadOfflineContentProviderFactory::DownloadOfflineContentProviderFactory
~~~cpp
DownloadOfflineContentProviderFactory();
~~~

### ~DownloadOfflineContentProviderFactory

DownloadOfflineContentProviderFactory::~DownloadOfflineContentProviderFactory
~~~cpp
~DownloadOfflineContentProviderFactory() override;
~~~

### BuildServiceInstanceFor

DownloadOfflineContentProviderFactory::BuildServiceInstanceFor
~~~cpp
std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      SimpleFactoryKey* key) const override;
~~~
 SimpleKeyedServiceFactory implementation.

### GetKeyToUse

DownloadOfflineContentProviderFactory::GetKeyToUse
~~~cpp
SimpleFactoryKey* GetKeyToUse(SimpleFactoryKey* key) const override;
~~~

### DownloadOfflineContentProviderFactory

DownloadOfflineContentProviderFactory
~~~cpp
DownloadOfflineContentProviderFactory(
      const DownloadOfflineContentProviderFactory&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadOfflineContentProviderFactory& operator=(
      const DownloadOfflineContentProviderFactory&) = delete;
~~~

### GetInstance

DownloadOfflineContentProviderFactory::GetInstance
~~~cpp
static DownloadOfflineContentProviderFactory* GetInstance();
~~~
 Returns a singleton instance of an DownloadOfflineContentProviderFactory.

### GetForKey

DownloadOfflineContentProviderFactory::GetForKey
~~~cpp
static DownloadOfflineContentProvider* GetForKey(SimpleFactoryKey* key);
~~~
 Returns the DownloadOfflineContentProvider associated with |key| or creates
 and associates one if it doesn't exist.

### BuildServiceInstanceFor

DownloadOfflineContentProviderFactory::BuildServiceInstanceFor
~~~cpp
std::unique_ptr<KeyedService> BuildServiceInstanceFor(
      SimpleFactoryKey* key) const override;
~~~
 SimpleKeyedServiceFactory implementation.

### GetKeyToUse

DownloadOfflineContentProviderFactory::GetKeyToUse
~~~cpp
SimpleFactoryKey* GetKeyToUse(SimpleFactoryKey* key) const override;
~~~
