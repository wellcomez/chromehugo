
## class OfflineItemModelManager
 Class for managing all the OfflineModels for a profile.

### OfflineItemModelManager

OfflineItemModelManager::OfflineItemModelManager
~~~cpp
explicit OfflineItemModelManager(content::BrowserContext* browser_context);
~~~
 Constructs a OfflineItemModelManager.

### OfflineItemModelManager

OfflineItemModelManager
~~~cpp
OfflineItemModelManager(const OfflineItemModelManager&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemModelManager& operator=(const OfflineItemModelManager&) = delete;
~~~

### ~OfflineItemModelManager

OfflineItemModelManager::~OfflineItemModelManager
~~~cpp
~OfflineItemModelManager() override;
~~~

### GetOrCreateOfflineItemModelData

OfflineItemModelManager::GetOrCreateOfflineItemModelData
~~~cpp
OfflineItemModelData* GetOrCreateOfflineItemModelData(const ContentId& id);
~~~
 Returns the OfflineItemModel for the ContentId, if not found, an empty
 OfflineItemModel will be created and returned.

### RemoveOfflineItemModelData

OfflineItemModelManager::RemoveOfflineItemModelData
~~~cpp
void RemoveOfflineItemModelData(const ContentId& id);
~~~

### browser_context

browser_context
~~~cpp
content::BrowserContext* browser_context() { return browser_context_; }
~~~

### browser_context_



~~~cpp

raw_ptr<content::BrowserContext> browser_context_;

~~~


### offline_item_model_data_



~~~cpp

std::map<ContentId, std::unique_ptr<OfflineItemModelData>>
      offline_item_model_data_;

~~~


### OfflineItemModelManager

OfflineItemModelManager
~~~cpp
OfflineItemModelManager(const OfflineItemModelManager&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemModelManager& operator=(const OfflineItemModelManager&) = delete;
~~~

### browser_context

browser_context
~~~cpp
content::BrowserContext* browser_context() { return browser_context_; }
~~~

### GetOrCreateOfflineItemModelData

OfflineItemModelManager::GetOrCreateOfflineItemModelData
~~~cpp
OfflineItemModelData* GetOrCreateOfflineItemModelData(const ContentId& id);
~~~
 Returns the OfflineItemModel for the ContentId, if not found, an empty
 OfflineItemModel will be created and returned.

### RemoveOfflineItemModelData

OfflineItemModelManager::RemoveOfflineItemModelData
~~~cpp
void RemoveOfflineItemModelData(const ContentId& id);
~~~

### browser_context_



~~~cpp

raw_ptr<content::BrowserContext> browser_context_;

~~~


### offline_item_model_data_



~~~cpp

std::map<ContentId, std::unique_ptr<OfflineItemModelData>>
      offline_item_model_data_;

~~~

