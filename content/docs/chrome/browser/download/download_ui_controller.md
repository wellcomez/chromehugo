
## class DownloadUIController
 This class handles the task of observing a single DownloadManager for
 notifying the UI when a new download should be displayed in the UI.

 It invokes the OnNewDownloadReady() method of hte Delegate when the
 target path is available for a new download.

###  ~Delegate

 The delegate is responsible for figuring out how to notify the UI.

~~~cpp
class Delegate {
   public:
    virtual ~Delegate();

    // This method is invoked to notify the UI of the new download |item|. Note
    // that |item| may be in any state by the time this method is invoked.
    virtual void OnNewDownloadReady(download::DownloadItem* item) = 0;

    // Notifies the controller that the main download button is clicked. Only
    // invoked by the download bubble UI.
    virtual void OnButtonClicked();
  };
~~~
### DownloadUIController

DownloadUIController::DownloadUIController
~~~cpp
DownloadUIController(content::DownloadManager* manager,
                       std::unique_ptr<Delegate> delegate);
~~~
 |manager| is the download manager to observe for new downloads. If
 |delegate.get()| is NULL, then the default delegate is constructed.


 On Android the default delegate notifies DownloadControllerAndroid. On
 other platforms the target of the notification is a Browser object.


 Currently explicit delegates are only used for testing.

### DownloadUIController

DownloadUIController
~~~cpp
DownloadUIController(const DownloadUIController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadUIController& operator=(const DownloadUIController&) = delete;
~~~

### ~DownloadUIController

DownloadUIController::~DownloadUIController
~~~cpp
~DownloadUIController() override;
~~~

### OnButtonClicked

DownloadUIController::OnButtonClicked
~~~cpp
void OnButtonClicked();
~~~
 Notifies the controller that the main download button is clicked. Currently
 only invoked by the download bubble UI.

### OnDownloadCreated

DownloadUIController::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadUIController::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### download_notifier_



~~~cpp

download::AllDownloadItemNotifier download_notifier_;

~~~


### delegate_



~~~cpp

std::unique_ptr<Delegate> delegate_;

~~~


### DownloadUIController

DownloadUIController
~~~cpp
DownloadUIController(const DownloadUIController&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadUIController& operator=(const DownloadUIController&) = delete;
~~~

###  ~Delegate

 The delegate is responsible for figuring out how to notify the UI.

~~~cpp
class Delegate {
   public:
    virtual ~Delegate();

    // This method is invoked to notify the UI of the new download |item|. Note
    // that |item| may be in any state by the time this method is invoked.
    virtual void OnNewDownloadReady(download::DownloadItem* item) = 0;

    // Notifies the controller that the main download button is clicked. Only
    // invoked by the download bubble UI.
    virtual void OnButtonClicked();
  };
~~~
### OnButtonClicked

DownloadUIController::OnButtonClicked
~~~cpp
void OnButtonClicked();
~~~
 Notifies the controller that the main download button is clicked. Currently
 only invoked by the download bubble UI.

### OnDownloadCreated

DownloadUIController::OnDownloadCreated
~~~cpp
void OnDownloadCreated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### OnDownloadUpdated

DownloadUIController::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(content::DownloadManager* manager,
                         download::DownloadItem* item) override;
~~~

### download_notifier_



~~~cpp

download::AllDownloadItemNotifier download_notifier_;

~~~


### delegate_



~~~cpp

std::unique_ptr<Delegate> delegate_;

~~~

