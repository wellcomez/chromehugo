
## class OfflineItemUtils
 Contains various utility methods for conversions between DownloadItem and
 OfflineItem.

### OfflineItemUtils

OfflineItemUtils
~~~cpp
OfflineItemUtils(const OfflineItemUtils&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemUtils& operator=(const OfflineItemUtils&) = delete;
~~~

### CreateOfflineItem

OfflineItemUtils::CreateOfflineItem
~~~cpp
static offline_items_collection::OfflineItem CreateOfflineItem(
      const std::string& name_space,
      download::DownloadItem* item);
~~~

### GetContentIdForDownload

OfflineItemUtils::GetContentIdForDownload
~~~cpp
static offline_items_collection::ContentId GetContentIdForDownload(
      download::DownloadItem* download);
~~~

### GetDownloadNamespacePrefix

OfflineItemUtils::GetDownloadNamespacePrefix
~~~cpp
static std::string GetDownloadNamespacePrefix(bool is_off_the_record);
~~~

### IsDownload

OfflineItemUtils::IsDownload
~~~cpp
static bool IsDownload(const offline_items_collection::ContentId& id);
~~~

### ConvertDownloadInterruptReasonToFailState

OfflineItemUtils::ConvertDownloadInterruptReasonToFailState
~~~cpp
static offline_items_collection::FailState
  ConvertDownloadInterruptReasonToFailState(
      download::DownloadInterruptReason reason);
~~~
 Converts DownloadInterruptReason to offline_items_collection::FailState.

### ConvertFailStateToDownloadInterruptReason

OfflineItemUtils::ConvertFailStateToDownloadInterruptReason
~~~cpp
static download::DownloadInterruptReason
  ConvertFailStateToDownloadInterruptReason(
      offline_items_collection::FailState fail_state);
~~~
 Converts offline_items_collection::FailState to DownloadInterruptReason.

### GetFailStateMessage

OfflineItemUtils::GetFailStateMessage
~~~cpp
static std::u16string GetFailStateMessage(
      offline_items_collection::FailState fail_state);
~~~
 Gets the short text to display for a offline_items_collection::FailState.

### ConvertDownloadRenameResultToRenameResult

OfflineItemUtils::ConvertDownloadRenameResultToRenameResult
~~~cpp
static RenameResult ConvertDownloadRenameResultToRenameResult(
      DownloadRenameResult download_rename_result);
~~~
 Converts download::DownloadItem::DownloadRenameResult to
 offline_items_collection::RenameResult.

### OfflineItemUtils

OfflineItemUtils
~~~cpp
OfflineItemUtils(const OfflineItemUtils&) = delete;
~~~

### operator=

operator=
~~~cpp
OfflineItemUtils& operator=(const OfflineItemUtils&) = delete;
~~~

### CreateOfflineItem

OfflineItemUtils::CreateOfflineItem
~~~cpp
static offline_items_collection::OfflineItem CreateOfflineItem(
      const std::string& name_space,
      download::DownloadItem* item);
~~~

### GetContentIdForDownload

OfflineItemUtils::GetContentIdForDownload
~~~cpp
static offline_items_collection::ContentId GetContentIdForDownload(
      download::DownloadItem* download);
~~~

### GetDownloadNamespacePrefix

OfflineItemUtils::GetDownloadNamespacePrefix
~~~cpp
static std::string GetDownloadNamespacePrefix(bool is_off_the_record);
~~~

### IsDownload

OfflineItemUtils::IsDownload
~~~cpp
static bool IsDownload(const offline_items_collection::ContentId& id);
~~~

### ConvertDownloadInterruptReasonToFailState

OfflineItemUtils::ConvertDownloadInterruptReasonToFailState
~~~cpp
static offline_items_collection::FailState
  ConvertDownloadInterruptReasonToFailState(
      download::DownloadInterruptReason reason);
~~~
 Converts DownloadInterruptReason to offline_items_collection::FailState.

### ConvertFailStateToDownloadInterruptReason

OfflineItemUtils::ConvertFailStateToDownloadInterruptReason
~~~cpp
static download::DownloadInterruptReason
  ConvertFailStateToDownloadInterruptReason(
      offline_items_collection::FailState fail_state);
~~~
 Converts offline_items_collection::FailState to DownloadInterruptReason.

### GetFailStateMessage

OfflineItemUtils::GetFailStateMessage
~~~cpp
static std::u16string GetFailStateMessage(
      offline_items_collection::FailState fail_state);
~~~
 Gets the short text to display for a offline_items_collection::FailState.

### ConvertDownloadRenameResultToRenameResult

OfflineItemUtils::ConvertDownloadRenameResultToRenameResult
~~~cpp
static RenameResult ConvertDownloadRenameResultToRenameResult(
      DownloadRenameResult download_rename_result);
~~~
 Converts download::DownloadItem::DownloadRenameResult to
 offline_items_collection::RenameResult.
