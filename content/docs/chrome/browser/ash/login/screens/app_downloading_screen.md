
## class AppDownloadingScreen
 This is App Downloading screen that tells the user the selected Android apps
 are being downloaded.

### AppDownloadingScreen

AppDownloadingScreen::AppDownloadingScreen
~~~cpp
AppDownloadingScreen(base::WeakPtr<TView> view,
                       const base::RepeatingClosure& exit_callback);
~~~

### AppDownloadingScreen

AppDownloadingScreen
~~~cpp
AppDownloadingScreen(const AppDownloadingScreen&) = delete;
~~~

### operator=

operator=
~~~cpp
AppDownloadingScreen& operator=(const AppDownloadingScreen&) = delete;
~~~

### ~AppDownloadingScreen

AppDownloadingScreen::~AppDownloadingScreen
~~~cpp
~AppDownloadingScreen() override;
~~~

### set_exit_callback_for_testing

set_exit_callback_for_testing
~~~cpp
void set_exit_callback_for_testing(base::RepeatingClosure exit_callback) {
    exit_callback_ = exit_callback;
  }
~~~

### ShowImpl

AppDownloadingScreen::ShowImpl
~~~cpp
void ShowImpl() override;
~~~
 BaseScreen:
### HideImpl

AppDownloadingScreen::HideImpl
~~~cpp
void HideImpl() override;
~~~

### OnUserAction

AppDownloadingScreen::OnUserAction
~~~cpp
void OnUserAction(const base::Value::List& args) override;
~~~

### view_



~~~cpp

base::WeakPtr<TView> view_;

~~~


### exit_callback_



~~~cpp

base::RepeatingClosure exit_callback_;

~~~


### AppDownloadingScreen

AppDownloadingScreen
~~~cpp
AppDownloadingScreen(const AppDownloadingScreen&) = delete;
~~~

### operator=

operator=
~~~cpp
AppDownloadingScreen& operator=(const AppDownloadingScreen&) = delete;
~~~

### set_exit_callback_for_testing

set_exit_callback_for_testing
~~~cpp
void set_exit_callback_for_testing(base::RepeatingClosure exit_callback) {
    exit_callback_ = exit_callback;
  }
~~~

### ShowImpl

AppDownloadingScreen::ShowImpl
~~~cpp
void ShowImpl() override;
~~~
 BaseScreen:
### HideImpl

AppDownloadingScreen::HideImpl
~~~cpp
void HideImpl() override;
~~~

### OnUserAction

AppDownloadingScreen::OnUserAction
~~~cpp
void OnUserAction(const base::Value::List& args) override;
~~~

### view_



~~~cpp

base::WeakPtr<TView> view_;

~~~


### exit_callback_



~~~cpp

base::RepeatingClosure exit_callback_;

~~~

