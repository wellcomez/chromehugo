
## class CustomizationWallpaperDownloader
 Download customized wallpaper.

 Owner of this class must provide callback, which will be called on
 finished (either successful or failed) wallpaper download.

### CustomizationWallpaperDownloader

CustomizationWallpaperDownloader::CustomizationWallpaperDownloader
~~~cpp
CustomizationWallpaperDownloader(
      const GURL& wallpaper_url,
      const base::FilePath& wallpaper_dir,
      const base::FilePath& wallpaper_downloaded_file,
      base::OnceCallback<void(bool success, const GURL&)>
          on_wallpaper_fetch_completed);
~~~
 - |wallpaper_url| - wallpaper URL to download.

 - |wallpaper_dir| - directory, where wallpaper will be downloaded
 (it will be created).

 - |wallpaper_downloaded_file| - full path to local file to store downloaded
 wallpaper file. File is downloaded to temporary location
 |wallpaper_downloaded_file| + ".tmp", so directory must be writable.

 After download is completed, temporary file will be renamed to
 |wallpaper_downloaded_file|.

### CustomizationWallpaperDownloader

CustomizationWallpaperDownloader
~~~cpp
CustomizationWallpaperDownloader(const CustomizationWallpaperDownloader&) =
      delete;
~~~

### operator=

operator=
~~~cpp
CustomizationWallpaperDownloader& operator=(
      const CustomizationWallpaperDownloader&) = delete;
~~~

### ~CustomizationWallpaperDownloader

CustomizationWallpaperDownloader::~CustomizationWallpaperDownloader
~~~cpp
~CustomizationWallpaperDownloader();
~~~

### Start

CustomizationWallpaperDownloader::Start
~~~cpp
void Start();
~~~
 Start download.

### set_retry_delay_for_testing

set_retry_delay_for_testing
~~~cpp
void set_retry_delay_for_testing(base::TimeDelta value) {
    retry_delay_ = value;
  }
~~~
 This is called in tests to modify (lower) retry delay.

### retry_current_delay_for_testing

retry_current_delay_for_testing
~~~cpp
base::TimeDelta retry_current_delay_for_testing() const {
    return retry_current_delay_;
  }
~~~

### StartRequest

CustomizationWallpaperDownloader::StartRequest
~~~cpp
void StartRequest();
~~~
 Start new request.

### Retry

CustomizationWallpaperDownloader::Retry
~~~cpp
void Retry();
~~~
 Schedules retry.

### OnSimpleLoaderComplete

CustomizationWallpaperDownloader::OnSimpleLoaderComplete
~~~cpp
void OnSimpleLoaderComplete(base::FilePath file_path);
~~~
 This is called when the download has finished.

### OnWallpaperDirectoryCreated

CustomizationWallpaperDownloader::OnWallpaperDirectoryCreated
~~~cpp
void OnWallpaperDirectoryCreated(std::unique_ptr<bool> success);
~~~
 Called on UI thread.

### OnTemporaryFileRenamed

CustomizationWallpaperDownloader::OnTemporaryFileRenamed
~~~cpp
void OnTemporaryFileRenamed(std::unique_ptr<bool> success);
~~~
 Called on UI thread.

### simple_loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> simple_loader_;

~~~

 This loader is used to download wallpaper file.

### wallpaper_url_



~~~cpp

const GURL wallpaper_url_;

~~~

 The wallpaper URL to fetch.

### wallpaper_dir_



~~~cpp

const base::FilePath wallpaper_dir_;

~~~

 Wallpaper directory (to be created).

### wallpaper_downloaded_file_



~~~cpp

const base::FilePath wallpaper_downloaded_file_;

~~~

 Full path to local file to save downloaded wallpaper.

### wallpaper_temporary_file_



~~~cpp

const base::FilePath wallpaper_temporary_file_;

~~~

 Full path to temporary file to fetch downloaded wallpper.

### request_scheduled_



~~~cpp

base::OneShotTimer request_scheduled_;

~~~

 Pending retry.

### retries_



~~~cpp

size_t retries_;

~~~

 Number of download retries (first attempt is not counted as retry).

### retry_delay_



~~~cpp

base::TimeDelta retry_delay_;

~~~

 Sleep between retry requests (increasing, see Retry() method for details).

 Non-constant value for tests.

### retry_current_delay_



~~~cpp

base::TimeDelta retry_current_delay_;

~~~

 Retry delay of the last attempt. For testing only.

### d

 Callback supplied by caller.

~~~cpp
base::OnceCallback<void(bool success, const GURL&)>
      on_wallpaper_fetch_completed_;
~~~
### weak_factory_



~~~cpp

base::WeakPtrFactory<CustomizationWallpaperDownloader> weak_factory_{this};

~~~


### CustomizationWallpaperDownloader

CustomizationWallpaperDownloader
~~~cpp
CustomizationWallpaperDownloader(const CustomizationWallpaperDownloader&) =
      delete;
~~~

### operator=

operator=
~~~cpp
CustomizationWallpaperDownloader& operator=(
      const CustomizationWallpaperDownloader&) = delete;
~~~

### set_retry_delay_for_testing

set_retry_delay_for_testing
~~~cpp
void set_retry_delay_for_testing(base::TimeDelta value) {
    retry_delay_ = value;
  }
~~~
 This is called in tests to modify (lower) retry delay.

### retry_current_delay_for_testing

retry_current_delay_for_testing
~~~cpp
base::TimeDelta retry_current_delay_for_testing() const {
    return retry_current_delay_;
  }
~~~

### Start

CustomizationWallpaperDownloader::Start
~~~cpp
void Start();
~~~
 Start download.

### StartRequest

CustomizationWallpaperDownloader::StartRequest
~~~cpp
void StartRequest();
~~~
 Start new request.

### Retry

CustomizationWallpaperDownloader::Retry
~~~cpp
void Retry();
~~~
 Schedules retry.

### OnSimpleLoaderComplete

CustomizationWallpaperDownloader::OnSimpleLoaderComplete
~~~cpp
void OnSimpleLoaderComplete(base::FilePath file_path);
~~~
 This is called when the download has finished.

### OnWallpaperDirectoryCreated

CustomizationWallpaperDownloader::OnWallpaperDirectoryCreated
~~~cpp
void OnWallpaperDirectoryCreated(std::unique_ptr<bool> success);
~~~
 Called on UI thread.

### OnTemporaryFileRenamed

CustomizationWallpaperDownloader::OnTemporaryFileRenamed
~~~cpp
void OnTemporaryFileRenamed(std::unique_ptr<bool> success);
~~~
 Called on UI thread.

### simple_loader_



~~~cpp

std::unique_ptr<network::SimpleURLLoader> simple_loader_;

~~~

 This loader is used to download wallpaper file.

### wallpaper_url_



~~~cpp

const GURL wallpaper_url_;

~~~

 The wallpaper URL to fetch.

### wallpaper_dir_



~~~cpp

const base::FilePath wallpaper_dir_;

~~~

 Wallpaper directory (to be created).

### wallpaper_downloaded_file_



~~~cpp

const base::FilePath wallpaper_downloaded_file_;

~~~

 Full path to local file to save downloaded wallpaper.

### wallpaper_temporary_file_



~~~cpp

const base::FilePath wallpaper_temporary_file_;

~~~

 Full path to temporary file to fetch downloaded wallpper.

### request_scheduled_



~~~cpp

base::OneShotTimer request_scheduled_;

~~~

 Pending retry.

### retries_



~~~cpp

size_t retries_;

~~~

 Number of download retries (first attempt is not counted as retry).

### retry_delay_



~~~cpp

base::TimeDelta retry_delay_;

~~~

 Sleep between retry requests (increasing, see Retry() method for details).

 Non-constant value for tests.

### retry_current_delay_



~~~cpp

base::TimeDelta retry_current_delay_;

~~~

 Retry delay of the last attempt. For testing only.

### d

 Callback supplied by caller.

~~~cpp
base::OnceCallback<void(bool success, const GURL&)>
      on_wallpaper_fetch_completed_;
~~~
### weak_factory_



~~~cpp

base::WeakPtrFactory<CustomizationWallpaperDownloader> weak_factory_{this};

~~~

