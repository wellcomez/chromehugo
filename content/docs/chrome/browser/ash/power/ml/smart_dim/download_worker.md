
## class DownloadWorker
 SmartDimWorker that loads meta info, preprocessor config and ML service model
 files from smart dim components.

### DownloadWorker

DownloadWorker::DownloadWorker
~~~cpp
DownloadWorker();
~~~

### DownloadWorker

DownloadWorker
~~~cpp
DownloadWorker(const DownloadWorker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadWorker& operator=(const DownloadWorker&) = delete;
~~~

### ~DownloadWorker

DownloadWorker::~DownloadWorker
~~~cpp
~DownloadWorker() override;
~~~

### GetPreprocessorConfig

DownloadWorker::GetPreprocessorConfig
~~~cpp
const assist_ranker::ExamplePreprocessorConfig* GetPreprocessorConfig()
      override;
~~~
 SmartDimWorker overrides:
###  GetExecutor


~~~cpp
const mojo::Remote<chromeos::machine_learning::mojom::GraphExecutor>&
  GetExecutor() override;
~~~
### LoadModelCallback

DownloadWorker::LoadModelCallback
~~~cpp
void LoadModelCallback(
      chromeos::machine_learning::mojom::LoadModelResult result);
~~~
 Remotes used to execute functions in the ML service side.

### CreateGraphExecutorCallback

DownloadWorker::CreateGraphExecutorCallback
~~~cpp
void CreateGraphExecutorCallback(
      chromeos::machine_learning::mojom::CreateGraphExecutorResult result);
~~~

### IsReady

DownloadWorker::IsReady
~~~cpp
bool IsReady();
~~~
 Returns true if it has loaded components successfully.

### InitializeFromComponent

DownloadWorker::InitializeFromComponent
~~~cpp
void InitializeFromComponent(const ComponentFileContents& contents);
~~~
 Loads meta info, preprocessor config and ML service model from smart dim
 components.

 Called by component updater when it gets a verified smart dim component and
 DownloadWorker is not ready.

 If IsReady(), this function won't be called again.

### inputs_



~~~cpp

base::flat_map<std::string, int> inputs_;

~~~


### outputs_



~~~cpp

base::flat_map<std::string, int> outputs_;

~~~


### metrics_model_name_



~~~cpp

std::string metrics_model_name_;

~~~


### LoadModelAndCreateGraphExecutor

DownloadWorker::LoadModelAndCreateGraphExecutor
~~~cpp
void LoadModelAndCreateGraphExecutor(const std::string& model_flatbuffer);
~~~

### OnJsonParsed

DownloadWorker::OnJsonParsed
~~~cpp
void OnJsonParsed(const std::string& model_flatbuffer,
                    const data_decoder::DataDecoder::ValueOrError result);
~~~

### DownloadWorker

DownloadWorker
~~~cpp
DownloadWorker(const DownloadWorker&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadWorker& operator=(const DownloadWorker&) = delete;
~~~

### GetPreprocessorConfig

DownloadWorker::GetPreprocessorConfig
~~~cpp
const assist_ranker::ExamplePreprocessorConfig* GetPreprocessorConfig()
      override;
~~~
 SmartDimWorker overrides:
###  GetExecutor


~~~cpp
const mojo::Remote<chromeos::machine_learning::mojom::GraphExecutor>&
  GetExecutor() override;
~~~
### LoadModelCallback

DownloadWorker::LoadModelCallback
~~~cpp
void LoadModelCallback(
      chromeos::machine_learning::mojom::LoadModelResult result);
~~~
 Remotes used to execute functions in the ML service side.

### CreateGraphExecutorCallback

DownloadWorker::CreateGraphExecutorCallback
~~~cpp
void CreateGraphExecutorCallback(
      chromeos::machine_learning::mojom::CreateGraphExecutorResult result);
~~~

### IsReady

DownloadWorker::IsReady
~~~cpp
bool IsReady();
~~~
 Returns true if it has loaded components successfully.

### InitializeFromComponent

DownloadWorker::InitializeFromComponent
~~~cpp
void InitializeFromComponent(const ComponentFileContents& contents);
~~~
 Loads meta info, preprocessor config and ML service model from smart dim
 components.

 Called by component updater when it gets a verified smart dim component and
 DownloadWorker is not ready.

 If IsReady(), this function won't be called again.

### inputs_



~~~cpp

base::flat_map<std::string, int> inputs_;

~~~


### outputs_



~~~cpp

base::flat_map<std::string, int> outputs_;

~~~


### metrics_model_name_



~~~cpp

std::string metrics_model_name_;

~~~


### LoadModelAndCreateGraphExecutor

DownloadWorker::LoadModelAndCreateGraphExecutor
~~~cpp
void LoadModelAndCreateGraphExecutor(const std::string& model_flatbuffer);
~~~

### OnJsonParsed

DownloadWorker::OnJsonParsed
~~~cpp
void OnJsonParsed(const std::string& model_flatbuffer,
                    const data_decoder::DataDecoder::ValueOrError result);
~~~
