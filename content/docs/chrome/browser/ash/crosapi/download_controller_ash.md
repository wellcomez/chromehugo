
## class DownloadControllerAsh
 The ash-chrome implementation of the DownloadController crosapi interface.

 This is where ash-chrome receives information on download events from lacros.

 This class must only be used from the main thread.

###  OnLacrosDownloadCreated

 Allows ash classes to observe download events.

~~~cpp
class DownloadControllerObserver : public base::CheckedObserver {
   public:
    virtual void OnLacrosDownloadCreated(const mojom::DownloadItem&) {}
    virtual void OnLacrosDownloadUpdated(const mojom::DownloadItem&) {}
    virtual void OnLacrosDownloadDestroyed(const mojom::DownloadItem&) {}
  };
~~~
### DownloadControllerAsh

DownloadControllerAsh::DownloadControllerAsh
~~~cpp
DownloadControllerAsh();
~~~

### DownloadControllerAsh

DownloadControllerAsh
~~~cpp
DownloadControllerAsh(const DownloadControllerAsh&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadControllerAsh& operator=(const DownloadControllerAsh&) = delete;
~~~

### ~DownloadControllerAsh

DownloadControllerAsh::~DownloadControllerAsh
~~~cpp
~DownloadControllerAsh() override;
~~~

### BindReceiver

DownloadControllerAsh::BindReceiver
~~~cpp
void BindReceiver(mojo::PendingReceiver<mojom::DownloadController> receiver);
~~~
 Bind this receiver for `mojom::DownloadController`. This is used by
 crosapi.

### BindClient

DownloadControllerAsh::BindClient
~~~cpp
void BindClient(
      mojo::PendingRemote<mojom::DownloadControllerClient> client) override;
~~~
 mojom::DownloadController:
### OnDownloadCreated

DownloadControllerAsh::OnDownloadCreated
~~~cpp
void OnDownloadCreated(mojom::DownloadItemPtr download) override;
~~~

### OnDownloadUpdated

DownloadControllerAsh::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(mojom::DownloadItemPtr download) override;
~~~

### OnDownloadDestroyed

DownloadControllerAsh::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(mojom::DownloadItemPtr download) override;
~~~

### AddObserver

DownloadControllerAsh::AddObserver
~~~cpp
void AddObserver(DownloadControllerObserver* observer);
~~~
 Required for the below `base::ObserverList`:
### RemoveObserver

DownloadControllerAsh::RemoveObserver
~~~cpp
void RemoveObserver(DownloadControllerObserver* observer);
~~~

### GetAllDownloads

DownloadControllerAsh::GetAllDownloads
~~~cpp
void GetAllDownloads(
      mojom::DownloadControllerClient::GetAllDownloadsCallback callback);
~~~
 Asynchronously returns all downloads from each Lacros client via the
 specified `callback`, no matter the type or state. Downloads are sorted
 chronologically by start time.

### Pause

DownloadControllerAsh::Pause
~~~cpp
void Pause(const std::string& download_guid);
~~~
 Pauses the download associated with the specified `download_guid`. This
 method will ultimately invoke `download::DownloadItem::Pause()`.

### Resume

DownloadControllerAsh::Resume
~~~cpp
void Resume(const std::string& download_guid, bool user_resume);
~~~
 Resumes the download associated with the specified `download_guid`. If
 `user_resume` is set to `true`, it signifies that this invocation was
 triggered by an explicit user action. This method will ultimately invoke
 `download::DownloadItem::Resume()`.

### Cancel

DownloadControllerAsh::Cancel
~~~cpp
void Cancel(const std::string& download_guid, bool user_cancel);
~~~
 Cancels the download associated with the specified `download_guid`.  If
 `user_cancel` is set to `true`, it signifies that this invocation was
 triggered by an explicit user action. This method will ultimately invoke
 `download::DownloadItem::Cancel()`.

### SetOpenWhenComplete

DownloadControllerAsh::SetOpenWhenComplete
~~~cpp
void SetOpenWhenComplete(const std::string& download_guid,
                           bool open_when_complete);
~~~
 Marks the download associated with the specified `download_guid` to be
 `open_when_complete`. This method will ultimately invoke
 `download::DownloadItem::SetOpenWhenComplete()`.

### receivers_



~~~cpp

mojo::ReceiverSet<mojom::DownloadController> receivers_;

~~~


### clients_



~~~cpp

mojo::RemoteSet<mojom::DownloadControllerClient> clients_;

~~~


### observers_



~~~cpp

base::ObserverList<DownloadControllerObserver> observers_;

~~~


### DownloadControllerAsh

DownloadControllerAsh
~~~cpp
DownloadControllerAsh(const DownloadControllerAsh&) = delete;
~~~

### operator=

operator=
~~~cpp
DownloadControllerAsh& operator=(const DownloadControllerAsh&) = delete;
~~~

###  OnLacrosDownloadCreated

 Allows ash classes to observe download events.

~~~cpp
class DownloadControllerObserver : public base::CheckedObserver {
   public:
    virtual void OnLacrosDownloadCreated(const mojom::DownloadItem&) {}
    virtual void OnLacrosDownloadUpdated(const mojom::DownloadItem&) {}
    virtual void OnLacrosDownloadDestroyed(const mojom::DownloadItem&) {}
  };
~~~
### BindReceiver

DownloadControllerAsh::BindReceiver
~~~cpp
void BindReceiver(mojo::PendingReceiver<mojom::DownloadController> receiver);
~~~
 Bind this receiver for `mojom::DownloadController`. This is used by
 crosapi.

### BindClient

DownloadControllerAsh::BindClient
~~~cpp
void BindClient(
      mojo::PendingRemote<mojom::DownloadControllerClient> client) override;
~~~
 mojom::DownloadController:
### OnDownloadCreated

DownloadControllerAsh::OnDownloadCreated
~~~cpp
void OnDownloadCreated(mojom::DownloadItemPtr download) override;
~~~

### OnDownloadUpdated

DownloadControllerAsh::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(mojom::DownloadItemPtr download) override;
~~~

### OnDownloadDestroyed

DownloadControllerAsh::OnDownloadDestroyed
~~~cpp
void OnDownloadDestroyed(mojom::DownloadItemPtr download) override;
~~~

### AddObserver

DownloadControllerAsh::AddObserver
~~~cpp
void AddObserver(DownloadControllerObserver* observer);
~~~
 Required for the below `base::ObserverList`:
### RemoveObserver

DownloadControllerAsh::RemoveObserver
~~~cpp
void RemoveObserver(DownloadControllerObserver* observer);
~~~

### GetAllDownloads

DownloadControllerAsh::GetAllDownloads
~~~cpp
void GetAllDownloads(
      mojom::DownloadControllerClient::GetAllDownloadsCallback callback);
~~~
 Asynchronously returns all downloads from each Lacros client via the
 specified `callback`, no matter the type or state. Downloads are sorted
 chronologically by start time.

### Pause

DownloadControllerAsh::Pause
~~~cpp
void Pause(const std::string& download_guid);
~~~
 Pauses the download associated with the specified `download_guid`. This
 method will ultimately invoke `download::DownloadItem::Pause()`.

### Resume

DownloadControllerAsh::Resume
~~~cpp
void Resume(const std::string& download_guid, bool user_resume);
~~~
 Resumes the download associated with the specified `download_guid`. If
 `user_resume` is set to `true`, it signifies that this invocation was
 triggered by an explicit user action. This method will ultimately invoke
 `download::DownloadItem::Resume()`.

### Cancel

DownloadControllerAsh::Cancel
~~~cpp
void Cancel(const std::string& download_guid, bool user_cancel);
~~~
 Cancels the download associated with the specified `download_guid`.  If
 `user_cancel` is set to `true`, it signifies that this invocation was
 triggered by an explicit user action. This method will ultimately invoke
 `download::DownloadItem::Cancel()`.

### SetOpenWhenComplete

DownloadControllerAsh::SetOpenWhenComplete
~~~cpp
void SetOpenWhenComplete(const std::string& download_guid,
                           bool open_when_complete);
~~~
 Marks the download associated with the specified `download_guid` to be
 `open_when_complete`. This method will ultimately invoke
 `download::DownloadItem::SetOpenWhenComplete()`.

### receivers_



~~~cpp

mojo::ReceiverSet<mojom::DownloadController> receivers_;

~~~


### clients_



~~~cpp

mojo::RemoteSet<mojom::DownloadControllerClient> clients_;

~~~


### observers_



~~~cpp

base::ObserverList<DownloadControllerObserver> observers_;

~~~

