
## class CameraRollDownloadManagerImpl
 CameraRollDownloadManager implementation.

### CameraRollDownloadManagerImpl

CameraRollDownloadManagerImpl::CameraRollDownloadManagerImpl
~~~cpp
CameraRollDownloadManagerImpl(
      const base::FilePath& download_path,
      ash::HoldingSpaceKeyedService* holding_space_keyed_service);
~~~

### ~CameraRollDownloadManagerImpl

CameraRollDownloadManagerImpl::~CameraRollDownloadManagerImpl
~~~cpp
~CameraRollDownloadManagerImpl() override;
~~~

### CreatePayloadFiles

CameraRollDownloadManagerImpl::CreatePayloadFiles
~~~cpp
void CreatePayloadFiles(
      int64_t payload_id,
      const proto::CameraRollItemMetadata& item_metadata,
      CreatePayloadFilesCallback payload_files_callback) override;
~~~
 CameraRollDownloadManager:
### UpdateDownloadProgress

CameraRollDownloadManagerImpl::UpdateDownloadProgress
~~~cpp
void UpdateDownloadProgress(
      secure_channel::mojom::FileTransferUpdatePtr update) override;
~~~

### DeleteFile

CameraRollDownloadManagerImpl::DeleteFile
~~~cpp
void DeleteFile(int64_t payload_id) override;
~~~

###  DownloadItem

 Internal representation of an item being downloaded.

~~~cpp
struct DownloadItem {
    DownloadItem(int64_t payload_id,
                 const base::FilePath& file_path,
                 int64_t file_size_bytes,
                 const std::string& holding_space_item_id);
    DownloadItem(DownloadItem&&) = default;
    DownloadItem& operator=(DownloadItem&&) = default;
    ~DownloadItem();

    int64_t payload_id;
    base::FilePath file_path;
    int64_t file_size_bytes;
    std::string holding_space_item_id;
    base::TimeTicks start_timestamp = base::TimeTicks::Now();
  };
~~~
### OnDiskSpaceCheckComplete

CameraRollDownloadManagerImpl::OnDiskSpaceCheckComplete
~~~cpp
void OnDiskSpaceCheckComplete(
      const base::SafeBaseName& base_name,
      int64_t payload_id,
      int64_t file_size_bytes,
      CreatePayloadFilesCallback payload_files_callback,
      bool has_enough_disk_space);
~~~

### OnUniquePathFetched

CameraRollDownloadManagerImpl::OnUniquePathFetched
~~~cpp
void OnUniquePathFetched(int64_t payload_id,
                           int64_t file_size_bytes,
                           CreatePayloadFilesCallback payload_files_callback,
                           const base::FilePath& unique_path);
~~~

### OnPayloadFilesCreated

CameraRollDownloadManagerImpl::OnPayloadFilesCreated
~~~cpp
void OnPayloadFilesCreated(
      int64_t payload_id,
      const base::FilePath& file_path,
      int64_t file_size_bytes,
      CreatePayloadFilesCallback payload_files_callback,
      absl::optional<secure_channel::mojom::PayloadFilesPtr> payload_files);
~~~

### CalculateItemTransferRate

CameraRollDownloadManagerImpl::CalculateItemTransferRate
~~~cpp
int CalculateItemTransferRate(const DownloadItem& download_item) const;
~~~

### download_path_



~~~cpp

const base::FilePath download_path_;

~~~


### field error



~~~cpp

ash::HoldingSpaceKeyedService* holding_space_keyed_service_;

~~~


### task_runner_



~~~cpp

const scoped_refptr<base::SequencedTaskRunner> task_runner_;

~~~

 Performs blocking I/O operations for creating and deleting payload files.

### pending_downloads_



~~~cpp

base::flat_map<int64_t, DownloadItem> pending_downloads_;

~~~

 Item downloads that are still in progress, keyed by payload IDs.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<CameraRollDownloadManagerImpl> weak_ptr_factory_{this};

~~~


### CreatePayloadFiles

CameraRollDownloadManagerImpl::CreatePayloadFiles
~~~cpp
void CreatePayloadFiles(
      int64_t payload_id,
      const proto::CameraRollItemMetadata& item_metadata,
      CreatePayloadFilesCallback payload_files_callback) override;
~~~
 CameraRollDownloadManager:
### UpdateDownloadProgress

CameraRollDownloadManagerImpl::UpdateDownloadProgress
~~~cpp
void UpdateDownloadProgress(
      secure_channel::mojom::FileTransferUpdatePtr update) override;
~~~

### DeleteFile

CameraRollDownloadManagerImpl::DeleteFile
~~~cpp
void DeleteFile(int64_t payload_id) override;
~~~

###  DownloadItem

 Internal representation of an item being downloaded.

~~~cpp
struct DownloadItem {
    DownloadItem(int64_t payload_id,
                 const base::FilePath& file_path,
                 int64_t file_size_bytes,
                 const std::string& holding_space_item_id);
    DownloadItem(DownloadItem&&) = default;
    DownloadItem& operator=(DownloadItem&&) = default;
    ~DownloadItem();

    int64_t payload_id;
    base::FilePath file_path;
    int64_t file_size_bytes;
    std::string holding_space_item_id;
    base::TimeTicks start_timestamp = base::TimeTicks::Now();
  };
~~~
### OnDiskSpaceCheckComplete

CameraRollDownloadManagerImpl::OnDiskSpaceCheckComplete
~~~cpp
void OnDiskSpaceCheckComplete(
      const base::SafeBaseName& base_name,
      int64_t payload_id,
      int64_t file_size_bytes,
      CreatePayloadFilesCallback payload_files_callback,
      bool has_enough_disk_space);
~~~

### OnUniquePathFetched

CameraRollDownloadManagerImpl::OnUniquePathFetched
~~~cpp
void OnUniquePathFetched(int64_t payload_id,
                           int64_t file_size_bytes,
                           CreatePayloadFilesCallback payload_files_callback,
                           const base::FilePath& unique_path);
~~~

### OnPayloadFilesCreated

CameraRollDownloadManagerImpl::OnPayloadFilesCreated
~~~cpp
void OnPayloadFilesCreated(
      int64_t payload_id,
      const base::FilePath& file_path,
      int64_t file_size_bytes,
      CreatePayloadFilesCallback payload_files_callback,
      absl::optional<secure_channel::mojom::PayloadFilesPtr> payload_files);
~~~

### CalculateItemTransferRate

CameraRollDownloadManagerImpl::CalculateItemTransferRate
~~~cpp
int CalculateItemTransferRate(const DownloadItem& download_item) const;
~~~

### download_path_



~~~cpp

const base::FilePath download_path_;

~~~


### field error



~~~cpp

ash::HoldingSpaceKeyedService* holding_space_keyed_service_;

~~~


### task_runner_



~~~cpp

const scoped_refptr<base::SequencedTaskRunner> task_runner_;

~~~

 Performs blocking I/O operations for creating and deleting payload files.

### pending_downloads_



~~~cpp

base::flat_map<int64_t, DownloadItem> pending_downloads_;

~~~

 Item downloads that are still in progress, keyed by payload IDs.

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<CameraRollDownloadManagerImpl> weak_ptr_factory_{this};

~~~

