
## class BruschettaDownloadClient

### BruschettaDownloadClient

BruschettaDownloadClient::BruschettaDownloadClient
~~~cpp
explicit BruschettaDownloadClient(Profile* profile);
~~~

### OnServiceInitialized

BruschettaDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~

### OnServiceUnavailable

BruschettaDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

BruschettaDownloadClient::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadUpdated

BruschettaDownloadClient::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(const std::string& guid,
                         uint64_t bytes_uploaded,
                         uint64_t bytes_downloaded) override;
~~~

### OnDownloadFailed

BruschettaDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& info,
                        FailureReason reason) override;
~~~

### OnDownloadSucceeded

BruschettaDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

BruschettaDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

BruschettaDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### SetInstallerInstance

BruschettaDownloadClient::SetInstallerInstance
~~~cpp
static void SetInstallerInstance(BruschettaInstaller* instance);
~~~

### field error



~~~cpp

static BruschettaInstaller* installer_;

~~~

 base::raw_ptr can't be used as a static member variable
### MaybeCancelDownload

BruschettaDownloadClient::MaybeCancelDownload
~~~cpp
bool MaybeCancelDownload(const std::string& guid);
~~~

### profile_



~~~cpp

const base::raw_ptr<Profile> profile_;

~~~


### OnServiceInitialized

BruschettaDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~

### OnServiceUnavailable

BruschettaDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

BruschettaDownloadClient::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadUpdated

BruschettaDownloadClient::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(const std::string& guid,
                         uint64_t bytes_uploaded,
                         uint64_t bytes_downloaded) override;
~~~

### OnDownloadFailed

BruschettaDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& info,
                        FailureReason reason) override;
~~~

### OnDownloadSucceeded

BruschettaDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

BruschettaDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

BruschettaDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### SetInstallerInstance

BruschettaDownloadClient::SetInstallerInstance
~~~cpp
static void SetInstallerInstance(BruschettaInstaller* instance);
~~~

### field error



~~~cpp

static BruschettaInstaller* installer_;

~~~

 base::raw_ptr can't be used as a static member variable
### MaybeCancelDownload

BruschettaDownloadClient::MaybeCancelDownload
~~~cpp
bool MaybeCancelDownload(const std::string& guid);
~~~

### profile_



~~~cpp

const base::raw_ptr<Profile> profile_;

~~~

