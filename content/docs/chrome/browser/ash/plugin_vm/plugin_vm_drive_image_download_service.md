
## class PluginVmDriveImageDownloadService

### PluginVmDriveImageDownloadService

PluginVmDriveImageDownloadService::PluginVmDriveImageDownloadService
~~~cpp
PluginVmDriveImageDownloadService(PluginVmInstaller* plugin_vm_installer,
                                    Profile* profile);
~~~

### PluginVmDriveImageDownloadService

PluginVmDriveImageDownloadService
~~~cpp
PluginVmDriveImageDownloadService(const PluginVmDriveImageDownloadService&) =
      delete;
~~~

### operator=

operator=
~~~cpp
PluginVmDriveImageDownloadService& operator=(
      const PluginVmDriveImageDownloadService&) = delete;
~~~

### ~PluginVmDriveImageDownloadService

PluginVmDriveImageDownloadService::~PluginVmDriveImageDownloadService
~~~cpp
~PluginVmDriveImageDownloadService();
~~~

### StartDownload

PluginVmDriveImageDownloadService::StartDownload
~~~cpp
void StartDownload(const std::string& id);
~~~

### CancelDownload

PluginVmDriveImageDownloadService::CancelDownload
~~~cpp
void CancelDownload();
~~~

### ResetState

PluginVmDriveImageDownloadService::ResetState
~~~cpp
void ResetState();
~~~
 Used to reset the internal state of the downloader.

### RemoveTemporaryArchive

PluginVmDriveImageDownloadService::RemoveTemporaryArchive
~~~cpp
void RemoveTemporaryArchive(OnFileDeletedCallback on_file_deleted_callback);
~~~
 Removes the temporary image archive and the containing folder
 on a non-UI thread.

### SetDriveServiceForTesting

PluginVmDriveImageDownloadService::SetDriveServiceForTesting
~~~cpp
void SetDriveServiceForTesting(
      std::unique_ptr<drive::DriveServiceInterface> drive_service);
~~~

### SetDownloadDirectoryForTesting

PluginVmDriveImageDownloadService::SetDownloadDirectoryForTesting
~~~cpp
void SetDownloadDirectoryForTesting(const base::FilePath& download_directory);
~~~

### DispatchDownloadFile

PluginVmDriveImageDownloadService::DispatchDownloadFile
~~~cpp
void DispatchDownloadFile();
~~~

### DownloadActionCallback

PluginVmDriveImageDownloadService::DownloadActionCallback
~~~cpp
void DownloadActionCallback(google_apis::ApiErrorCode error_code,
                              const base::FilePath& file_path);
~~~

### GetContentCallback

PluginVmDriveImageDownloadService::GetContentCallback
~~~cpp
void GetContentCallback(google_apis::ApiErrorCode error_code,
                          std::unique_ptr<std::string> content,
                          bool first_chunk);
~~~

### ProgressCallback

PluginVmDriveImageDownloadService::ProgressCallback
~~~cpp
void ProgressCallback(int64_t progress, int64_t total);
~~~

### field error



~~~cpp

PluginVmInstaller* plugin_vm_installer_;

~~~


### drive_service_



~~~cpp

std::unique_ptr<drive::DriveServiceInterface> drive_service_;

~~~


### secure_hash_service_



~~~cpp

std::unique_ptr<crypto::SecureHash> secure_hash_service_;

~~~


### file_id_



~~~cpp

std::string file_id_;

~~~


### 


~~~cpp
int64_t total_bytes_downloaded_ = 0;
~~~

### download_directory_



~~~cpp

base::FilePath download_directory_{kPluginVmDriveDownloadDirectory};

~~~


### download_file_path_



~~~cpp

base::FilePath download_file_path_;

~~~


### cancel_callback_



~~~cpp

google_apis::CancelCallbackOnce cancel_callback_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<PluginVmDriveImageDownloadService> weak_ptr_factory_{
      this};

~~~


### PluginVmDriveImageDownloadService

PluginVmDriveImageDownloadService
~~~cpp
PluginVmDriveImageDownloadService(const PluginVmDriveImageDownloadService&) =
      delete;
~~~

### operator=

operator=
~~~cpp
PluginVmDriveImageDownloadService& operator=(
      const PluginVmDriveImageDownloadService&) = delete;
~~~

### 


~~~cpp
int64_t total_bytes_downloaded_ = 0;
~~~

### StartDownload

PluginVmDriveImageDownloadService::StartDownload
~~~cpp
void StartDownload(const std::string& id);
~~~

### CancelDownload

PluginVmDriveImageDownloadService::CancelDownload
~~~cpp
void CancelDownload();
~~~

### ResetState

PluginVmDriveImageDownloadService::ResetState
~~~cpp
void ResetState();
~~~
 Used to reset the internal state of the downloader.

### RemoveTemporaryArchive

PluginVmDriveImageDownloadService::RemoveTemporaryArchive
~~~cpp
void RemoveTemporaryArchive(OnFileDeletedCallback on_file_deleted_callback);
~~~
 Removes the temporary image archive and the containing folder
 on a non-UI thread.

### SetDriveServiceForTesting

PluginVmDriveImageDownloadService::SetDriveServiceForTesting
~~~cpp
void SetDriveServiceForTesting(
      std::unique_ptr<drive::DriveServiceInterface> drive_service);
~~~

### SetDownloadDirectoryForTesting

PluginVmDriveImageDownloadService::SetDownloadDirectoryForTesting
~~~cpp
void SetDownloadDirectoryForTesting(const base::FilePath& download_directory);
~~~

### DispatchDownloadFile

PluginVmDriveImageDownloadService::DispatchDownloadFile
~~~cpp
void DispatchDownloadFile();
~~~

### DownloadActionCallback

PluginVmDriveImageDownloadService::DownloadActionCallback
~~~cpp
void DownloadActionCallback(google_apis::ApiErrorCode error_code,
                              const base::FilePath& file_path);
~~~

### GetContentCallback

PluginVmDriveImageDownloadService::GetContentCallback
~~~cpp
void GetContentCallback(google_apis::ApiErrorCode error_code,
                          std::unique_ptr<std::string> content,
                          bool first_chunk);
~~~

### ProgressCallback

PluginVmDriveImageDownloadService::ProgressCallback
~~~cpp
void ProgressCallback(int64_t progress, int64_t total);
~~~

### field error



~~~cpp

PluginVmInstaller* plugin_vm_installer_;

~~~


### drive_service_



~~~cpp

std::unique_ptr<drive::DriveServiceInterface> drive_service_;

~~~


### secure_hash_service_



~~~cpp

std::unique_ptr<crypto::SecureHash> secure_hash_service_;

~~~


### file_id_



~~~cpp

std::string file_id_;

~~~


### download_directory_



~~~cpp

base::FilePath download_directory_{kPluginVmDriveDownloadDirectory};

~~~


### download_file_path_



~~~cpp

base::FilePath download_file_path_;

~~~


### cancel_callback_



~~~cpp

google_apis::CancelCallbackOnce cancel_callback_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<PluginVmDriveImageDownloadService> weak_ptr_factory_{
      this};

~~~

