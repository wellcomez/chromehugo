
## class PluginVmImageDownloadClient

### PluginVmImageDownloadClient

PluginVmImageDownloadClient::PluginVmImageDownloadClient
~~~cpp
explicit PluginVmImageDownloadClient(Profile* profile);
~~~

### PluginVmImageDownloadClient

PluginVmImageDownloadClient
~~~cpp
PluginVmImageDownloadClient(const PluginVmImageDownloadClient&) = delete;
~~~

### operator=

operator=
~~~cpp
PluginVmImageDownloadClient& operator=(const PluginVmImageDownloadClient&) =
      delete;
~~~

### ~PluginVmImageDownloadClient

PluginVmImageDownloadClient::~PluginVmImageDownloadClient
~~~cpp
~PluginVmImageDownloadClient() override;
~~~

### profile_



~~~cpp

raw_ptr<Profile> profile_ = nullptr;

~~~


### content_length_



~~~cpp

int64_t content_length_ = -1;

~~~


### response_code_



~~~cpp

int response_code_ = -1;

~~~


### GetInstaller

PluginVmImageDownloadClient::GetInstaller
~~~cpp
PluginVmInstaller* GetInstaller();
~~~

### IsCurrentDownload

PluginVmImageDownloadClient::IsCurrentDownload
~~~cpp
bool IsCurrentDownload(const std::string& guid);
~~~
 Returns false for cancelled downloads.

### OnServiceInitialized

PluginVmImageDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~
 download::Client implementation.

### OnServiceUnavailable

PluginVmImageDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

PluginVmImageDownloadClient::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadUpdated

PluginVmImageDownloadClient::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(const std::string& guid,
                         uint64_t bytes_uploaded,
                         uint64_t bytes_downloaded) override;
~~~

### OnDownloadFailed

PluginVmImageDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& completion_info,
                        download::Client::FailureReason reason) override;
~~~

### OnDownloadSucceeded

PluginVmImageDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

PluginVmImageDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

PluginVmImageDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### GetFractionComplete

PluginVmImageDownloadClient::GetFractionComplete
~~~cpp
absl::optional<double> GetFractionComplete(int64_t bytes_downloaded);
~~~

### PluginVmImageDownloadClient

PluginVmImageDownloadClient
~~~cpp
PluginVmImageDownloadClient(const PluginVmImageDownloadClient&) = delete;
~~~

### operator=

operator=
~~~cpp
PluginVmImageDownloadClient& operator=(const PluginVmImageDownloadClient&) =
      delete;
~~~

### profile_



~~~cpp

raw_ptr<Profile> profile_ = nullptr;

~~~


### content_length_



~~~cpp

int64_t content_length_ = -1;

~~~


### response_code_



~~~cpp

int response_code_ = -1;

~~~


### GetInstaller

PluginVmImageDownloadClient::GetInstaller
~~~cpp
PluginVmInstaller* GetInstaller();
~~~

### IsCurrentDownload

PluginVmImageDownloadClient::IsCurrentDownload
~~~cpp
bool IsCurrentDownload(const std::string& guid);
~~~
 Returns false for cancelled downloads.

### OnServiceInitialized

PluginVmImageDownloadClient::OnServiceInitialized
~~~cpp
void OnServiceInitialized(
      bool state_lost,
      const std::vector<download::DownloadMetaData>& downloads) override;
~~~
 download::Client implementation.

### OnServiceUnavailable

PluginVmImageDownloadClient::OnServiceUnavailable
~~~cpp
void OnServiceUnavailable() override;
~~~

### OnDownloadStarted

PluginVmImageDownloadClient::OnDownloadStarted
~~~cpp
void OnDownloadStarted(
      const std::string& guid,
      const std::vector<GURL>& url_chain,
      const scoped_refptr<const net::HttpResponseHeaders>& headers) override;
~~~

### OnDownloadUpdated

PluginVmImageDownloadClient::OnDownloadUpdated
~~~cpp
void OnDownloadUpdated(const std::string& guid,
                         uint64_t bytes_uploaded,
                         uint64_t bytes_downloaded) override;
~~~

### OnDownloadFailed

PluginVmImageDownloadClient::OnDownloadFailed
~~~cpp
void OnDownloadFailed(const std::string& guid,
                        const download::CompletionInfo& completion_info,
                        download::Client::FailureReason reason) override;
~~~

### OnDownloadSucceeded

PluginVmImageDownloadClient::OnDownloadSucceeded
~~~cpp
void OnDownloadSucceeded(
      const std::string& guid,
      const download::CompletionInfo& completion_info) override;
~~~

### CanServiceRemoveDownloadedFile

PluginVmImageDownloadClient::CanServiceRemoveDownloadedFile
~~~cpp
bool CanServiceRemoveDownloadedFile(const std::string& guid,
                                      bool force_delete) override;
~~~

### GetUploadData

PluginVmImageDownloadClient::GetUploadData
~~~cpp
void GetUploadData(const std::string& guid,
                     download::GetUploadDataCallback callback) override;
~~~

### GetFractionComplete

PluginVmImageDownloadClient::GetFractionComplete
~~~cpp
absl::optional<double> GetFractionComplete(int64_t bytes_downloaded);
~~~
