### GetDownloadType

GetDownloadType
~~~cpp
ClientDownloadRequest::DownloadType GetDownloadType(const base::FilePath& file);
~~~
 Returns the DownloadType of the file at |path|. This function is only valid
 for paths that satisfy IsSupportedBinaryFile() above.

