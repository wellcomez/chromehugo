### operator&lt;

operator&lt;
~~~cpp
bool operator<(const T& lhs, const T& rhs);
~~~

### operator&lt;=

operator&lt;=
~~~cpp
bool operator<=(const T& lhs, const T& rhs) {
  return !(rhs < lhs);
}
~~~

### operator&gt;

operator&gt;
~~~cpp
bool operator>(const T& lhs, const T& rhs) {
  return rhs < lhs;
}
~~~

### operator&gt;=

operator&gt;=
~~~cpp
bool operator>=(const T& lhs, const T& rhs) {
  return !(lhs < rhs);
}
~~~

### URLLoaderClientEndpoints::Clone

URLLoaderClientEndpoints::Clone
~~~cpp
URLLoaderClientEndpointsPtr URLLoaderClientEndpoints::Clone() const {
  return New(
      mojo::Clone(url_loader),
      mojo::Clone(url_loader_client)
  );
}
~~~

### URLLoaderClientEndpoints::Equals

URLLoaderClientEndpoints::Equals
~~~cpp
bool URLLoaderClientEndpoints::Equals(const T& other_struct) const {
  if (!mojo::Equals(this->url_loader, other_struct.url_loader))
    return false;
  if (!mojo::Equals(this->url_loader_client, other_struct.url_loader_client))
    return false;
  return true;
}
~~~

### operator&lt;

operator&lt;
~~~cpp
bool operator<(const T& lhs, const T& rhs) {
  if (lhs.url_loader < rhs.url_loader)
    return true;
  if (rhs.url_loader < lhs.url_loader)
    return false;
  if (lhs.url_loader_client < rhs.url_loader_client)
    return true;
  if (rhs.url_loader_client < lhs.url_loader_client)
    return false;
  return false;
}
~~~

## class URLLoader

### field error



~~~cpp

static const char Name_[];

~~~


### MessageToMethodInfo_

URLLoader::MessageToMethodInfo_
~~~cpp
static IPCStableHashFunction MessageToMethodInfo_(mojo::Message& message);
~~~

### MessageToMethodName_

URLLoader::MessageToMethodName_
~~~cpp
static const char* MessageToMethodName_(mojo::Message& message);
~~~

### 


~~~cpp
static constexpr uint32_t Version_ = 0;
~~~

### PassesAssociatedKinds_



~~~cpp

static constexpr bool PassesAssociatedKinds_ = false;

~~~


### HasUninterruptableMethods_



~~~cpp

static constexpr bool HasUninterruptableMethods_ = false;

~~~


### enum MethodMinVersions

~~~cpp
enum MethodMinVersions : uint32_t {
    kFollowRedirectMinVersion = 0,
    kSetPriorityMinVersion = 0,
    kPauseReadingBodyFromNetMinVersion = 0,
    kResumeReadingBodyFromNetMinVersion = 0,
  }
~~~
###  IPCStableHash


~~~cpp
struct FollowRedirect_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct SetPriority_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct PauseReadingBodyFromNet_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct ResumeReadingBodyFromNet_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
### kClientDisconnectReason



~~~cpp

static constexpr uint32_t kClientDisconnectReason = 1U;

~~~

 !BUILDFLAG(IS_FUCHSIA)
### ~URLLoader

~URLLoader
~~~cpp
virtual ~URLLoader() = default;
~~~

### FollowRedirect

FollowRedirect
~~~cpp
virtual void FollowRedirect(const std::vector<std::string>& removed_headers, const ::net::HttpRequestHeaders& modified_headers, const ::net::HttpRequestHeaders& modified_cors_exempt_headers, const absl::optional<::GURL>& new_url) = 0;
~~~

### SetPriority

SetPriority
~~~cpp
virtual void SetPriority(::net::RequestPriority priority, int32_t intra_priority_value) = 0;
~~~

### PauseReadingBodyFromNet

PauseReadingBodyFromNet
~~~cpp
virtual void PauseReadingBodyFromNet() = 0;
~~~

### ResumeReadingBodyFromNet

ResumeReadingBodyFromNet
~~~cpp
virtual void ResumeReadingBodyFromNet() = 0;
~~~

### 


~~~cpp
static constexpr uint32_t Version_ = 0;
~~~

### ~URLLoader

~URLLoader
~~~cpp
virtual ~URLLoader() = default;
~~~

### FollowRedirect

FollowRedirect
~~~cpp
virtual void FollowRedirect(const std::vector<std::string>& removed_headers, const ::net::HttpRequestHeaders& modified_headers, const ::net::HttpRequestHeaders& modified_cors_exempt_headers, const absl::optional<::GURL>& new_url) = 0;
~~~

### SetPriority

SetPriority
~~~cpp
virtual void SetPriority(::net::RequestPriority priority, int32_t intra_priority_value) = 0;
~~~

### PauseReadingBodyFromNet

PauseReadingBodyFromNet
~~~cpp
virtual void PauseReadingBodyFromNet() = 0;
~~~

### ResumeReadingBodyFromNet

ResumeReadingBodyFromNet
~~~cpp
virtual void ResumeReadingBodyFromNet() = 0;
~~~

### field error



~~~cpp

static const char Name_[];

~~~


### MessageToMethodInfo_

URLLoader::MessageToMethodInfo_
~~~cpp
static IPCStableHashFunction MessageToMethodInfo_(mojo::Message& message);
~~~

### MessageToMethodName_

URLLoader::MessageToMethodName_
~~~cpp
static const char* MessageToMethodName_(mojo::Message& message);
~~~

### PassesAssociatedKinds_



~~~cpp

static constexpr bool PassesAssociatedKinds_ = false;

~~~


### HasUninterruptableMethods_



~~~cpp

static constexpr bool HasUninterruptableMethods_ = false;

~~~


### enum MethodMinVersions

~~~cpp
enum MethodMinVersions : uint32_t {
    kFollowRedirectMinVersion = 0,
    kSetPriorityMinVersion = 0,
    kPauseReadingBodyFromNetMinVersion = 0,
    kResumeReadingBodyFromNetMinVersion = 0,
  };
~~~
### kClientDisconnectReason



~~~cpp

static constexpr uint32_t kClientDisconnectReason = 1U;

~~~

 !BUILDFLAG(IS_FUCHSIA)
## class URLLoaderClient

### field error



~~~cpp

static const char Name_[];

~~~


### MessageToMethodInfo_

URLLoaderClient::MessageToMethodInfo_
~~~cpp
static IPCStableHashFunction MessageToMethodInfo_(mojo::Message& message);
~~~

### MessageToMethodName_

URLLoaderClient::MessageToMethodName_
~~~cpp
static const char* MessageToMethodName_(mojo::Message& message);
~~~

### 


~~~cpp
static constexpr uint32_t Version_ = 0;
~~~

### PassesAssociatedKinds_



~~~cpp

static constexpr bool PassesAssociatedKinds_ = false;

~~~


### HasUninterruptableMethods_



~~~cpp

static constexpr bool HasUninterruptableMethods_ = false;

~~~


### enum MethodMinVersions

~~~cpp
enum MethodMinVersions : uint32_t {
    kOnReceiveEarlyHintsMinVersion = 0,
    kOnReceiveResponseMinVersion = 0,
    kOnReceiveRedirectMinVersion = 0,
    kOnUploadProgressMinVersion = 0,
    kOnTransferSizeUpdatedMinVersion = 0,
    kOnCompleteMinVersion = 0,
  }
~~~
###  IPCStableHash


~~~cpp
struct OnReceiveEarlyHints_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct OnReceiveResponse_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct OnReceiveRedirect_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct OnUploadProgress_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct OnTransferSizeUpdated_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
###  IPCStableHash


~~~cpp
struct OnComplete_Sym {
    NOINLINE static uint32_t IPCStableHash();
  };
~~~
### ~URLLoaderClient

~URLLoaderClient
~~~cpp
virtual ~URLLoaderClient() = default;
~~~
 !BUILDFLAG(IS_FUCHSIA)
### OnReceiveEarlyHints

OnReceiveEarlyHints
~~~cpp
virtual void OnReceiveEarlyHints(::network::mojom::EarlyHintsPtr early_hints) = 0;
~~~

### OnReceiveResponse

OnReceiveResponse
~~~cpp
virtual void OnReceiveResponse(::network::mojom::URLResponseHeadPtr head, ::mojo::ScopedDataPipeConsumerHandle body, absl::optional<::mojo_base::BigBuffer> cached_metadata) = 0;
~~~

### OnReceiveRedirect

OnReceiveRedirect
~~~cpp
virtual void OnReceiveRedirect(const ::net::RedirectInfo& redirect_info, ::network::mojom::URLResponseHeadPtr head) = 0;
~~~

### OnUploadProgress

OnUploadProgress
~~~cpp
virtual void OnUploadProgress(int64_t current_position, int64_t total_size, OnUploadProgressCallback callback) = 0;
~~~

### OnTransferSizeUpdated

OnTransferSizeUpdated
~~~cpp
virtual void OnTransferSizeUpdated(int32_t transfer_size_diff) = 0;
~~~

### OnComplete

OnComplete
~~~cpp
virtual void OnComplete(const ::network::URLLoaderCompletionStatus& status) = 0;
~~~

### 


~~~cpp
static constexpr uint32_t Version_ = 0;
~~~

### ~URLLoaderClient

~URLLoaderClient
~~~cpp
virtual ~URLLoaderClient() = default;
~~~
 !BUILDFLAG(IS_FUCHSIA)
### OnReceiveEarlyHints

OnReceiveEarlyHints
~~~cpp
virtual void OnReceiveEarlyHints(::network::mojom::EarlyHintsPtr early_hints) = 0;
~~~

### OnReceiveResponse

OnReceiveResponse
~~~cpp
virtual void OnReceiveResponse(::network::mojom::URLResponseHeadPtr head, ::mojo::ScopedDataPipeConsumerHandle body, absl::optional<::mojo_base::BigBuffer> cached_metadata) = 0;
~~~

### OnReceiveRedirect

OnReceiveRedirect
~~~cpp
virtual void OnReceiveRedirect(const ::net::RedirectInfo& redirect_info, ::network::mojom::URLResponseHeadPtr head) = 0;
~~~

### OnUploadProgress

OnUploadProgress
~~~cpp
virtual void OnUploadProgress(int64_t current_position, int64_t total_size, OnUploadProgressCallback callback) = 0;
~~~

### OnTransferSizeUpdated

OnTransferSizeUpdated
~~~cpp
virtual void OnTransferSizeUpdated(int32_t transfer_size_diff) = 0;
~~~

### OnComplete

OnComplete
~~~cpp
virtual void OnComplete(const ::network::URLLoaderCompletionStatus& status) = 0;
~~~

### field error



~~~cpp

static const char Name_[];

~~~


### MessageToMethodInfo_

URLLoaderClient::MessageToMethodInfo_
~~~cpp
static IPCStableHashFunction MessageToMethodInfo_(mojo::Message& message);
~~~

### MessageToMethodName_

URLLoaderClient::MessageToMethodName_
~~~cpp
static const char* MessageToMethodName_(mojo::Message& message);
~~~

### PassesAssociatedKinds_



~~~cpp

static constexpr bool PassesAssociatedKinds_ = false;

~~~


### HasUninterruptableMethods_



~~~cpp

static constexpr bool HasUninterruptableMethods_ = false;

~~~


### enum MethodMinVersions

~~~cpp
enum MethodMinVersions : uint32_t {
    kOnReceiveEarlyHintsMinVersion = 0,
    kOnReceiveResponseMinVersion = 0,
    kOnReceiveRedirectMinVersion = 0,
    kOnUploadProgressMinVersion = 0,
    kOnTransferSizeUpdatedMinVersion = 0,
    kOnCompleteMinVersion = 0,
  };
~~~
## class URLLoaderProxy

### URLLoaderProxy

URLLoaderProxy::URLLoaderProxy
~~~cpp
explicit URLLoaderProxy(mojo::MessageReceiverWithResponder* receiver);
~~~

### FollowRedirect

URLLoaderProxy::FollowRedirect
~~~cpp
void FollowRedirect(const std::vector<std::string>& removed_headers, const ::net::HttpRequestHeaders& modified_headers, const ::net::HttpRequestHeaders& modified_cors_exempt_headers, const absl::optional<::GURL>& new_url) final;
~~~

### SetPriority

URLLoaderProxy::SetPriority
~~~cpp
void SetPriority(::net::RequestPriority priority, int32_t intra_priority_value) final;
~~~

### PauseReadingBodyFromNet

URLLoaderProxy::PauseReadingBodyFromNet
~~~cpp
void PauseReadingBodyFromNet() final;
~~~

### ResumeReadingBodyFromNet

URLLoaderProxy::ResumeReadingBodyFromNet
~~~cpp
void ResumeReadingBodyFromNet() final;
~~~

### field error



~~~cpp

mojo::MessageReceiverWithResponder* receiver_;

~~~


### FollowRedirect

URLLoaderProxy::FollowRedirect
~~~cpp
void FollowRedirect(const std::vector<std::string>& removed_headers, const ::net::HttpRequestHeaders& modified_headers, const ::net::HttpRequestHeaders& modified_cors_exempt_headers, const absl::optional<::GURL>& new_url) final;
~~~

### SetPriority

URLLoaderProxy::SetPriority
~~~cpp
void SetPriority(::net::RequestPriority priority, int32_t intra_priority_value) final;
~~~

### PauseReadingBodyFromNet

URLLoaderProxy::PauseReadingBodyFromNet
~~~cpp
void PauseReadingBodyFromNet() final;
~~~

### ResumeReadingBodyFromNet

URLLoaderProxy::ResumeReadingBodyFromNet
~~~cpp
void ResumeReadingBodyFromNet() final;
~~~

### field error



~~~cpp

mojo::MessageReceiverWithResponder* receiver_;

~~~


## class URLLoaderClientProxy

### URLLoaderClientProxy

URLLoaderClientProxy::URLLoaderClientProxy
~~~cpp
explicit URLLoaderClientProxy(mojo::MessageReceiverWithResponder* receiver);
~~~

### OnReceiveEarlyHints

URLLoaderClientProxy::OnReceiveEarlyHints
~~~cpp
void OnReceiveEarlyHints(::network::mojom::EarlyHintsPtr early_hints) final;
~~~

### OnReceiveResponse

URLLoaderClientProxy::OnReceiveResponse
~~~cpp
void OnReceiveResponse(::network::mojom::URLResponseHeadPtr head, ::mojo::ScopedDataPipeConsumerHandle body, absl::optional<::mojo_base::BigBuffer> cached_metadata) final;
~~~

### OnReceiveRedirect

URLLoaderClientProxy::OnReceiveRedirect
~~~cpp
void OnReceiveRedirect(const ::net::RedirectInfo& redirect_info, ::network::mojom::URLResponseHeadPtr head) final;
~~~

### OnUploadProgress

URLLoaderClientProxy::OnUploadProgress
~~~cpp
void OnUploadProgress(int64_t current_position, int64_t total_size, OnUploadProgressCallback callback) final;
~~~

### OnTransferSizeUpdated

URLLoaderClientProxy::OnTransferSizeUpdated
~~~cpp
void OnTransferSizeUpdated(int32_t transfer_size_diff) final;
~~~

### OnComplete

URLLoaderClientProxy::OnComplete
~~~cpp
void OnComplete(const ::network::URLLoaderCompletionStatus& status) final;
~~~

### field error



~~~cpp

mojo::MessageReceiverWithResponder* receiver_;

~~~


### OnReceiveEarlyHints

URLLoaderClientProxy::OnReceiveEarlyHints
~~~cpp
void OnReceiveEarlyHints(::network::mojom::EarlyHintsPtr early_hints) final;
~~~

### OnReceiveResponse

URLLoaderClientProxy::OnReceiveResponse
~~~cpp
void OnReceiveResponse(::network::mojom::URLResponseHeadPtr head, ::mojo::ScopedDataPipeConsumerHandle body, absl::optional<::mojo_base::BigBuffer> cached_metadata) final;
~~~

### OnReceiveRedirect

URLLoaderClientProxy::OnReceiveRedirect
~~~cpp
void OnReceiveRedirect(const ::net::RedirectInfo& redirect_info, ::network::mojom::URLResponseHeadPtr head) final;
~~~

### OnUploadProgress

URLLoaderClientProxy::OnUploadProgress
~~~cpp
void OnUploadProgress(int64_t current_position, int64_t total_size, OnUploadProgressCallback callback) final;
~~~

### OnTransferSizeUpdated

URLLoaderClientProxy::OnTransferSizeUpdated
~~~cpp
void OnTransferSizeUpdated(int32_t transfer_size_diff) final;
~~~

### OnComplete

URLLoaderClientProxy::OnComplete
~~~cpp
void OnComplete(const ::network::URLLoaderCompletionStatus& status) final;
~~~

### field error



~~~cpp

mojo::MessageReceiverWithResponder* receiver_;

~~~


## class URLLoaderStubDispatch

### Accept

URLLoaderStubDispatch::Accept
~~~cpp
static bool Accept(URLLoader* impl, mojo::Message* message);
~~~

### AcceptWithResponder

URLLoaderStubDispatch::AcceptWithResponder
~~~cpp
static bool AcceptWithResponder(
      URLLoader* impl,
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder);
~~~

### Accept

URLLoaderStubDispatch::Accept
~~~cpp
static bool Accept(URLLoader* impl, mojo::Message* message);
~~~

### AcceptWithResponder

URLLoaderStubDispatch::AcceptWithResponder
~~~cpp
static bool AcceptWithResponder(
      URLLoader* impl,
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder);
~~~

## class URLLoaderStub

### URLLoaderStub

URLLoaderStub
~~~cpp
URLLoaderStub() = default;
~~~

### ~URLLoaderStub

~URLLoaderStub
~~~cpp
~URLLoaderStub() override = default;
~~~

### set_sink

set_sink
~~~cpp
void set_sink(ImplPointerType sink) { sink_ = std::move(sink); }
~~~

### sink

sink
~~~cpp
ImplPointerType& sink() { return sink_; }
~~~

### Accept

Accept
~~~cpp
bool Accept(mojo::Message* message) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderStubDispatch::Accept(
        ImplRefTraits::GetRawPointer(&sink_), message);
  }
~~~

### AcceptWithResponder

AcceptWithResponder
~~~cpp
bool AcceptWithResponder(
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderStubDispatch::AcceptWithResponder(
        ImplRefTraits::GetRawPointer(&sink_), message, std::move(responder));
  }
~~~

### sink_



~~~cpp

ImplPointerType sink_;

~~~


### URLLoaderStub

URLLoaderStub
~~~cpp
URLLoaderStub() = default;
~~~

### ~URLLoaderStub

~URLLoaderStub
~~~cpp
~URLLoaderStub() override = default;
~~~

### set_sink

set_sink
~~~cpp
void set_sink(ImplPointerType sink) { sink_ = std::move(sink); }
~~~

### sink

sink
~~~cpp
ImplPointerType& sink() { return sink_; }
~~~

### Accept

Accept
~~~cpp
bool Accept(mojo::Message* message) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderStubDispatch::Accept(
        ImplRefTraits::GetRawPointer(&sink_), message);
  }
~~~

### AcceptWithResponder

AcceptWithResponder
~~~cpp
bool AcceptWithResponder(
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderStubDispatch::AcceptWithResponder(
        ImplRefTraits::GetRawPointer(&sink_), message, std::move(responder));
  }
~~~

### sink_



~~~cpp

ImplPointerType sink_;

~~~


## class URLLoaderClientStubDispatch

### Accept

URLLoaderClientStubDispatch::Accept
~~~cpp
static bool Accept(URLLoaderClient* impl, mojo::Message* message);
~~~

### AcceptWithResponder

URLLoaderClientStubDispatch::AcceptWithResponder
~~~cpp
static bool AcceptWithResponder(
      URLLoaderClient* impl,
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder);
~~~

### Accept

URLLoaderClientStubDispatch::Accept
~~~cpp
static bool Accept(URLLoaderClient* impl, mojo::Message* message);
~~~

### AcceptWithResponder

URLLoaderClientStubDispatch::AcceptWithResponder
~~~cpp
static bool AcceptWithResponder(
      URLLoaderClient* impl,
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder);
~~~

## class URLLoaderClientStub

### URLLoaderClientStub

URLLoaderClientStub
~~~cpp
URLLoaderClientStub() = default;
~~~

### ~URLLoaderClientStub

~URLLoaderClientStub
~~~cpp
~URLLoaderClientStub() override = default;
~~~

### set_sink

set_sink
~~~cpp
void set_sink(ImplPointerType sink) { sink_ = std::move(sink); }
~~~

### sink

sink
~~~cpp
ImplPointerType& sink() { return sink_; }
~~~

### Accept

Accept
~~~cpp
bool Accept(mojo::Message* message) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderClientStubDispatch::Accept(
        ImplRefTraits::GetRawPointer(&sink_), message);
  }
~~~

### AcceptWithResponder

AcceptWithResponder
~~~cpp
bool AcceptWithResponder(
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderClientStubDispatch::AcceptWithResponder(
        ImplRefTraits::GetRawPointer(&sink_), message, std::move(responder));
  }
~~~

### sink_



~~~cpp

ImplPointerType sink_;

~~~


### URLLoaderClientStub

URLLoaderClientStub
~~~cpp
URLLoaderClientStub() = default;
~~~

### ~URLLoaderClientStub

~URLLoaderClientStub
~~~cpp
~URLLoaderClientStub() override = default;
~~~

### set_sink

set_sink
~~~cpp
void set_sink(ImplPointerType sink) { sink_ = std::move(sink); }
~~~

### sink

sink
~~~cpp
ImplPointerType& sink() { return sink_; }
~~~

### Accept

Accept
~~~cpp
bool Accept(mojo::Message* message) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderClientStubDispatch::Accept(
        ImplRefTraits::GetRawPointer(&sink_), message);
  }
~~~

### AcceptWithResponder

AcceptWithResponder
~~~cpp
bool AcceptWithResponder(
      mojo::Message* message,
      std::unique_ptr<mojo::MessageReceiverWithStatus> responder) override {
    if (ImplRefTraits::IsNull(sink_))
      return false;
    return URLLoaderClientStubDispatch::AcceptWithResponder(
        ImplRefTraits::GetRawPointer(&sink_), message, std::move(responder));
  }
~~~

### sink_



~~~cpp

ImplPointerType sink_;

~~~


## class URLLoaderRequestValidator

### Accept

URLLoaderRequestValidator::Accept
~~~cpp
bool Accept(mojo::Message* message) override;
~~~

### Accept

URLLoaderRequestValidator::Accept
~~~cpp
bool Accept(mojo::Message* message) override;
~~~

## class URLLoaderClientRequestValidator

### Accept

URLLoaderClientRequestValidator::Accept
~~~cpp
bool Accept(mojo::Message* message) override;
~~~

### Accept

URLLoaderClientRequestValidator::Accept
~~~cpp
bool Accept(mojo::Message* message) override;
~~~

## class URLLoaderClientResponseValidator

### Accept

URLLoaderClientResponseValidator::Accept
~~~cpp
bool Accept(mojo::Message* message) override;
~~~

### Accept

URLLoaderClientResponseValidator::Accept
~~~cpp
bool Accept(mojo::Message* message) override;
~~~

## class URLLoaderClientEndpoints

### New

New
~~~cpp
static URLLoaderClientEndpointsPtr New(Args&&... args) {
    return URLLoaderClientEndpointsPtr(
        absl::in_place, std::forward<Args>(args)...);
  }
~~~

### From

From
~~~cpp
static URLLoaderClientEndpointsPtr From(const U& u) {
    return mojo::TypeConverter<URLLoaderClientEndpointsPtr, U>::Convert(u);
  }
~~~

### To

To
~~~cpp
U To() const {
    return mojo::TypeConverter<U, URLLoaderClientEndpoints>::Convert(*this);
  }
~~~

### URLLoaderClientEndpoints

URLLoaderClientEndpoints::URLLoaderClientEndpoints
~~~cpp
URLLoaderClientEndpoints();
~~~

### URLLoaderClientEndpoints

URLLoaderClientEndpoints::URLLoaderClientEndpoints
~~~cpp
URLLoaderClientEndpoints(
      ::mojo::PendingRemote<URLLoader> url_loader,
      ::mojo::PendingReceiver<URLLoaderClient> url_loader_client);
~~~

### URLLoaderClientEndpoints

URLLoaderClientEndpoints
~~~cpp
URLLoaderClientEndpoints(const URLLoaderClientEndpoints&) = delete;
~~~

### operator=

operator=
~~~cpp
URLLoaderClientEndpoints& operator=(const URLLoaderClientEndpoints&) = delete;
~~~

### ~URLLoaderClientEndpoints

URLLoaderClientEndpoints::~URLLoaderClientEndpoints
~~~cpp
~URLLoaderClientEndpoints();
~~~

### Clone

URLLoaderClientEndpoints::Clone
~~~cpp
URLLoaderClientEndpointsPtr Clone() const;
~~~

### Equals

URLLoaderClientEndpoints::Equals
~~~cpp
bool Equals(const T& other) const;
~~~

### operator==

operator==
~~~cpp
bool operator==(const T& rhs) const { return Equals(rhs); }
~~~

### SerializeAsMessage

SerializeAsMessage
~~~cpp
static mojo::Message SerializeAsMessage(UserType* input) {
    return mojo::internal::SerializeAsMessageImpl<
        URLLoaderClientEndpoints::DataView>(input);
  }
~~~

### WrapAsMessage

WrapAsMessage
~~~cpp
static mojo::Message WrapAsMessage(UserType input) {
    return mojo::Message(std::make_unique<
        internal::URLLoaderClientEndpoints_UnserializedMessageContext<
            UserType, URLLoaderClientEndpoints::DataView>>(0, 0, std::move(input)),
        MOJO_CREATE_MESSAGE_FLAG_NONE);
  }
~~~

### Deserialize

Deserialize
~~~cpp
static bool Deserialize(const void* data,
                          size_t data_num_bytes,
                          UserType* output) {
    mojo::Message message;
    return mojo::internal::DeserializeImpl<URLLoaderClientEndpoints::DataView>(
        message, data, data_num_bytes, output, Validate);
  }
~~~

### Deserialize

Deserialize
~~~cpp
static bool Deserialize(const std::vector<uint8_t>& input,
                          UserType* output) {
    return URLLoaderClientEndpoints::Deserialize(
        input.size() == 0 ? nullptr : &input.front(), input.size(), output);
  }
~~~

### DeserializeFromMessage

DeserializeFromMessage
~~~cpp
static bool DeserializeFromMessage(mojo::Message input,
                                     UserType* output) {
    auto context = input.TakeUnserializedContext<
        internal::URLLoaderClientEndpoints_UnserializedMessageContext<
            UserType, URLLoaderClientEndpoints::DataView>>();
    if (context) {
      *output = std::move(context->TakeData());
      return true;
    }
    input.SerializeIfNecessary();
    return mojo::internal::DeserializeImpl<URLLoaderClientEndpoints::DataView>(
        input, input.payload(), input.payload_num_bytes(), output, Validate);
  }
~~~

### url_loader



~~~cpp

::mojo::PendingRemote<URLLoader> url_loader;

~~~


### url_loader_client



~~~cpp

::mojo::PendingReceiver<URLLoaderClient> url_loader_client;

~~~


### WriteIntoTrace

URLLoaderClientEndpoints::WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedValue traced_context) const;
~~~
 Serialise this struct into a trace.

### Validate

URLLoaderClientEndpoints::Validate
~~~cpp
static bool Validate(const void* data,
                       mojo::internal::ValidationContext* validation_context);
~~~

### URLLoaderClientEndpoints

URLLoaderClientEndpoints
~~~cpp
URLLoaderClientEndpoints(const URLLoaderClientEndpoints&) = delete;
~~~

### operator=

operator=
~~~cpp
URLLoaderClientEndpoints& operator=(const URLLoaderClientEndpoints&) = delete;
~~~

### url_loader



~~~cpp

::mojo::PendingRemote<URLLoader> url_loader;

~~~


### url_loader_client



~~~cpp

::mojo::PendingReceiver<URLLoaderClient> url_loader_client;

~~~


### WriteIntoTrace

URLLoaderClientEndpoints::WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedValue traced_context) const;
~~~
 Serialise this struct into a trace.

### Validate

URLLoaderClientEndpoints::Validate
~~~cpp
static bool Validate(const void* data,
                       mojo::internal::ValidationContext* validation_context);
~~~
