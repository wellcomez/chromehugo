### WebView

WebView
~~~cpp
explicit WebView(content::BrowserContext* browser_context = nullptr);

  WebView(const WebView&) = delete;
~~~

### operator=

operator=
~~~cpp
WebView& operator=(const WebView&) = delete;
~~~

### ~WebView

~WebView
~~~cpp
~WebView() override
~~~

### GetWebContents

GetWebContents
~~~cpp
content::WebContents* GetWebContents(
      base::Location creator_location = base::Location::Current());
~~~
 This creates a WebContents if |browser_context_| has been set and there is
 not yet a WebContents associated with this WebView, otherwise it will
 return a nullptr.

### SetWebContents

SetWebContents
~~~cpp
void SetWebContents(content::WebContents* web_contents);
~~~
 WebView does not assume ownership of WebContents set via this method, only
 those it implicitly creates via GetWebContents() above.

### GetBrowserContext

GetBrowserContext
~~~cpp
content::BrowserContext* GetBrowserContext();
~~~

### SetBrowserContext

SetBrowserContext
~~~cpp
void SetBrowserContext(content::BrowserContext* browser_context);
~~~

### LoadInitialURL

LoadInitialURL
~~~cpp
void LoadInitialURL(const GURL& url);
~~~
 Loads the initial URL to display in the attached WebContents. Creates the
 WebContents if none is attached yet. Note that this is intended as a
 convenience for loading the initial URL, and so URLs are navigated with
 PAGE_TRANSITION_AUTO_TOPLEVEL, so this is not intended as a general purpose
 navigation method - use WebContents' API directly.

### SetFastResize

SetFastResize
~~~cpp
void SetFastResize(bool fast_resize);
~~~
 Controls how the attached WebContents is resized.

 false = WebContents' views' bounds are updated continuously as the
         WebView's bounds change (default).

 true  = WebContents' views' position is updated continuously but its size
         is not (which may result in some clipping or under-painting) until
         a continuous size operation completes. This allows for smoother
         resizing performance during interactive resizes and animations.

### EnableSizingFromWebContents

EnableSizingFromWebContents
~~~cpp
void EnableSizingFromWebContents(const gfx::Size& min_size,
                                   const gfx::Size& max_size);
~~~
 If enabled, this will make the WebView's preferred size dependent on the
 WebContents' size.

### SetCrashedOverlayView

SetCrashedOverlayView
~~~cpp
void SetCrashedOverlayView(View* crashed_overlay_view);
~~~
 If provided, this View will be shown in place of the web contents
 when the web contents is in a crashed state. This is cleared automatically
 if the web contents is changed.

### set_is_primary_web_contents_for_window

set_is_primary_web_contents_for_window
~~~cpp
void set_is_primary_web_contents_for_window(bool is_primary) {
    is_primary_web_contents_for_window_ = is_primary;
  }
~~~
 Sets whether this is the primary web contents for the window.

### set_allow_accelerators

set_allow_accelerators
~~~cpp
void set_allow_accelerators(bool allow_accelerators) {
    allow_accelerators_ = allow_accelerators;
  }
~~~
 When used to host UI, we need to explicitly allow accelerators to be
 processed. Default is false.

### ResizeDueToAutoResize

ResizeDueToAutoResize
~~~cpp
void ResizeDueToAutoResize(content::WebContents* source,
                             const gfx::Size& new_size) override;
~~~
 Overridden from content::WebContentsDelegate:
### holder

holder
~~~cpp
NativeViewHost* holder() { return holder_; }
~~~

### GetAccessibleNodeData

GetAccessibleNodeData
~~~cpp
void GetAccessibleNodeData(ui::AXNodeData* node_data) override;
~~~
 View:
### OnLetterboxingChanged

OnLetterboxingChanged
~~~cpp
virtual void OnLetterboxingChanged() {}
~~~
 Called when letterboxing (scaling the native view to preserve aspect
 ratio) is enabled or disabled.

### is_letterboxing

is_letterboxing
~~~cpp
bool is_letterboxing() const { return is_letterboxing_; }
~~~

### min_size

min_size
~~~cpp
const gfx::Size& min_size() const { return min_size_; }
~~~

### max_size

max_size
~~~cpp
const gfx::Size& max_size() const { return max_size_; }
~~~

### OnBoundsChanged

OnBoundsChanged
~~~cpp
void OnBoundsChanged(const gfx::Rect& previous_bounds) override;
~~~
 View:
### ViewHierarchyChanged

ViewHierarchyChanged
~~~cpp
void ViewHierarchyChanged(
      const ViewHierarchyChangedDetails& details) override;
~~~

### SkipDefaultKeyEventProcessing

SkipDefaultKeyEventProcessing
~~~cpp
bool SkipDefaultKeyEventProcessing(const ui::KeyEvent& event) override;
~~~

### OnMousePressed

OnMousePressed
~~~cpp
bool OnMousePressed(const ui::MouseEvent& event) override;
~~~

### OnFocus

OnFocus
~~~cpp
void OnFocus() override;
~~~

### AboutToRequestFocusFromTabTraversal

AboutToRequestFocusFromTabTraversal
~~~cpp
void AboutToRequestFocusFromTabTraversal(bool reverse) override;
~~~

### GetNativeViewAccessible

GetNativeViewAccessible
~~~cpp
gfx::NativeViewAccessible GetNativeViewAccessible() override;
~~~

### AddedToWidget

AddedToWidget
~~~cpp
void AddedToWidget() override;
~~~

### RenderFrameCreated

RenderFrameCreated
~~~cpp
void RenderFrameCreated(content::RenderFrameHost* render_frame_host) override;
~~~
 Overridden from content::WebContentsObserver:
### RenderFrameDeleted

RenderFrameDeleted
~~~cpp
void RenderFrameDeleted(content::RenderFrameHost* render_frame_host) override;
~~~

### RenderFrameHostChanged

RenderFrameHostChanged
~~~cpp
void RenderFrameHostChanged(content::RenderFrameHost* old_host,
                              content::RenderFrameHost* new_host) override;
~~~

### DidToggleFullscreenModeForTab

DidToggleFullscreenModeForTab
~~~cpp
void DidToggleFullscreenModeForTab(bool entered_fullscreen,
                                     bool will_cause_resize) override;
~~~

### OnWebContentsFocused

OnWebContentsFocused
~~~cpp
void OnWebContentsFocused(
      content::RenderWidgetHost* render_widget_host) override;
~~~

### AXTreeIDForMainFrameHasChanged

AXTreeIDForMainFrameHasChanged
~~~cpp
void AXTreeIDForMainFrameHasChanged() override;
~~~

### WebContentsDestroyed

WebContentsDestroyed
~~~cpp
void WebContentsDestroyed() override;
~~~

### OnAXModeAdded

OnAXModeAdded
~~~cpp
void OnAXModeAdded(ui::AXMode mode) override;
~~~
 Override from ui::AXModeObserver
### AttachWebContentsNativeView

AttachWebContentsNativeView
~~~cpp
void AttachWebContentsNativeView();
~~~

### DetachWebContentsNativeView

DetachWebContentsNativeView
~~~cpp
void DetachWebContentsNativeView();
~~~

### UpdateCrashedOverlayView

UpdateCrashedOverlayView
~~~cpp
void UpdateCrashedOverlayView();
~~~

### NotifyAccessibilityWebContentsChanged

NotifyAccessibilityWebContentsChanged
~~~cpp
void NotifyAccessibilityWebContentsChanged();
~~~

### SetUpNewMainFrame

SetUpNewMainFrame
~~~cpp
void SetUpNewMainFrame(content::RenderFrameHost* frame_host);
~~~
 Called when the main frame in the renderer becomes present.

### LostMainFrame

LostMainFrame
~~~cpp
void LostMainFrame();
~~~
 Called when the main frame in the renderer is no longer present.

### MaybeEnableAutoResize

MaybeEnableAutoResize
~~~cpp
void MaybeEnableAutoResize(content::RenderFrameHost* frame_host);
~~~
 Registers for ResizeDueToAutoResize() notifications from `frame_host`'s
 RenderWidgetHostView whenever it is created or changes, if
 EnableSizingFromWebContents() has been called. This should only be called
 for main frames; other frames can not have auto resize set.

### CreateWebContents

CreateWebContents
~~~cpp
std::unique_ptr<content::WebContents> CreateWebContents(
      content::BrowserContext* browser_context,
      base::Location creator_location = base::Location::Current());
~~~
 Create a regular or test web contents (based on whether we're running
 in a unit test or not).

### VIEW_BUILDER_PROPERTY

VIEW_BUILDER_PROPERTY
~~~cpp
VIEW_BUILDER_PROPERTY(content::BrowserContext*, BrowserContext)
~~~

### VIEW_BUILDER_PROPERTY

VIEW_BUILDER_PROPERTY
~~~cpp
VIEW_BUILDER_PROPERTY(content::WebContents*, WebContents)
~~~

### VIEW_BUILDER_PROPERTY

VIEW_BUILDER_PROPERTY
~~~cpp
VIEW_BUILDER_PROPERTY(bool, FastResize)
~~~

### VIEW_BUILDER_METHOD

VIEW_BUILDER_METHOD
~~~cpp
VIEW_BUILDER_METHOD(EnableSizingFromWebContents,
                    const gfx::Size&,
                    const gfx::Size&)
~~~

### VIEW_BUILDER_PROPERTY

VIEW_BUILDER_PROPERTY
~~~cpp
VIEW_BUILDER_PROPERTY(View*, CrashedOverlayView)
~~~

### VIEW_BUILDER_METHOD

VIEW_BUILDER_METHOD
~~~cpp
VIEW_BUILDER_METHOD(set_is_primary_web_contents_for_window, bool)
~~~

### VIEW_BUILDER_METHOD

VIEW_BUILDER_METHOD
~~~cpp
VIEW_BUILDER_METHOD(set_allow_accelerators, bool)
~~~

## class ScopedWebContentsCreatorForTesting
 An instance of this class registers a WebContentsCreator on construction
 and deregisters the WebContentsCreator on destruction.

### ScopedWebContentsCreatorForTesting

ScopedWebContentsCreatorForTesting
~~~cpp
ScopedWebContentsCreatorForTesting(
        const ScopedWebContentsCreatorForTesting&) = delete;
~~~

### operator=

ScopedWebContentsCreatorForTesting::operator=
~~~cpp
ScopedWebContentsCreatorForTesting& operator=(
        const ScopedWebContentsCreatorForTesting&) = delete;

    ~ScopedWebContentsCreatorForTesting();
~~~
