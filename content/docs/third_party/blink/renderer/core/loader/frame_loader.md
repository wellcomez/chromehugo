### IsBackForwardLoadType

IsBackForwardLoadType
~~~cpp
CORE_EXPORT bool IsBackForwardLoadType(WebFrameLoadType);
~~~

### IsReloadLoadType

IsReloadLoadType
~~~cpp
CORE_EXPORT bool IsReloadLoadType(WebFrameLoadType);
~~~

## class ;::;::;

### IsBackForwardLoadType

;::;::;::IsBackForwardLoadType
~~~cpp
CORE_EXPORT bool IsBackForwardLoadType(WebFrameLoadType);
~~~

## class ;::;

### IsBackForwardLoadType

;::;::IsBackForwardLoadType
~~~cpp
CORE_EXPORT bool IsBackForwardLoadType(WebFrameLoadType);
~~~

## class ;

### IsBackForwardLoadType

;::IsBackForwardLoadType
~~~cpp
CORE_EXPORT bool IsBackForwardLoadType(WebFrameLoadType);
~~~

## class FrameLoader

### FrameLoader

FrameLoader
~~~cpp
FrameLoader(const FrameLoader&) = delete;
~~~

### operator=

FrameLoader::operator=
~~~cpp
FrameLoader& operator=(const FrameLoader&) = delete;
  ~FrameLoader();
~~~

### Init

FrameLoader::Init
~~~cpp
void Init(const DocumentToken& document_token,
            std::unique_ptr<PolicyContainer> policy_container,
            const StorageKey& storage_key,
            ukm::SourceId document_ukm_source_id);
~~~

### ResourceRequestForReload

FrameLoader::ResourceRequestForReload
~~~cpp
ResourceRequest ResourceRequestForReload(
      WebFrameLoadType,
      ClientRedirectPolicy = ClientRedirectPolicy::kNotClientRedirect);
~~~

### Progress

Progress
~~~cpp
ProgressTracker& Progress() const { return *progress_tracker_; }
~~~

### StartNavigation

FrameLoader::StartNavigation
~~~cpp
void StartNavigation(FrameLoadRequest&,
                       WebFrameLoadType = WebFrameLoadType::kStandard);
~~~
 This is the entry-point for all renderer-initiated navigations except
 history traversals. It will eventually send the navigation to the browser
 process, or call DocumentLoader::CommitSameDocumentNavigation for
 same-document navigation. For reloads, an appropriate WebFrameLoadType
 should be given. Otherwise, kStandard should be used (and the final
 WebFrameLoadType will be computed).

### CommitNavigation

FrameLoader::CommitNavigation
~~~cpp
void CommitNavigation(
      std::unique_ptr<WebNavigationParams> navigation_params,
      std::unique_ptr<WebDocumentLoader::ExtraData> extra_data,
      CommitReason = CommitReason::kRegular);
~~~
 Called when the browser process has asked this renderer process to commit
 a navigation in this frame. This method skips most of the checks assuming
 that browser process has already performed any checks necessary.

 See WebNavigationParams for details.

### WillStartNavigation

FrameLoader::WillStartNavigation
~~~cpp
bool WillStartNavigation(const WebNavigationInfo& info);
~~~
 Called before the browser process is asked to navigate this frame, to mark
 the frame as loading and save some navigation information for later use.

### StopAllLoaders

FrameLoader::StopAllLoaders
~~~cpp
void StopAllLoaders(bool abort_client);
~~~
 This runs the "stop document loading" algorithm in HTML:
 https://html.spec.whatwg.org/C/browsing-the-web.html#stop-document-loading
 Note, this function only cancels ongoing navigation handled through
 FrameLoader.


 If |abort_client| is true, then the frame's client will have
 AbortClientNavigation() called if a navigation was aborted. Normally this
 should be passed as true, unless the navigation has been migrated to a
 provisional frame, while this frame is going away, so the navigation isn't
 actually being aborted.


 Warning: StopAllLoaders() may detach the LocalFrame to which this
 FrameLoader belongs. Callers need to be careful about checking the
 existence of the frame after StopAllLoaders() returns.

### DidAccessInitialDocument

FrameLoader::DidAccessInitialDocument
~~~cpp
void DidAccessInitialDocument();
~~~
 Notifies the client that the initial empty document has been accessed, and
 thus it is no longer safe to show a provisional URL above the document
 without risking a URL spoof. The client must not call back into JavaScript.

### GetDocumentLoader

GetDocumentLoader
~~~cpp
DocumentLoader* GetDocumentLoader() const { return document_loader_.Get(); }
~~~

### SetDefersLoading

FrameLoader::SetDefersLoading
~~~cpp
void SetDefersLoading(LoaderFreezeMode mode);
~~~

### DidExplicitOpen

FrameLoader::DidExplicitOpen
~~~cpp
void DidExplicitOpen();
~~~

### UserAgent

FrameLoader::UserAgent
~~~cpp
String UserAgent() const;
~~~

### FullUserAgent

FrameLoader::FullUserAgent
~~~cpp
String FullUserAgent() const;
~~~

### ReducedUserAgent

FrameLoader::ReducedUserAgent
~~~cpp
String ReducedUserAgent() const;
~~~

### UserAgentMetadata

FrameLoader::UserAgentMetadata
~~~cpp
absl::optional<blink::UserAgentMetadata> UserAgentMetadata() const;
~~~

### DispatchDidClearWindowObjectInMainWorld

FrameLoader::DispatchDidClearWindowObjectInMainWorld
~~~cpp
void DispatchDidClearWindowObjectInMainWorld();
~~~

### DispatchDidClearDocumentOfWindowObject

FrameLoader::DispatchDidClearDocumentOfWindowObject
~~~cpp
void DispatchDidClearDocumentOfWindowObject();
~~~

### DispatchDocumentElementAvailable

FrameLoader::DispatchDocumentElementAvailable
~~~cpp
void DispatchDocumentElementAvailable();
~~~

### RunScriptsAtDocumentElementAvailable

FrameLoader::RunScriptsAtDocumentElementAvailable
~~~cpp
void RunScriptsAtDocumentElementAvailable();
~~~

### PendingEffectiveSandboxFlags

FrameLoader::PendingEffectiveSandboxFlags
~~~cpp
network::mojom::blink::WebSandboxFlags PendingEffectiveSandboxFlags() const;
~~~
 See content/browser/renderer_host/sandbox_flags.md
 This contains the sandbox flags to commit for new documents.

 - For main documents, it contains the sandbox inherited from the opener.

 - For nested documents, it contains the sandbox flags inherited from the
   parent and the one defined in the <iframe>'s sandbox attribute.

### ModifyRequestForCSP

FrameLoader::ModifyRequestForCSP
~~~cpp
void ModifyRequestForCSP(
      ResourceRequest&,
      const FetchClientSettingsObject* fetch_client_settings_object,
      LocalDOMWindow* window_for_logging,
      mojom::RequestContextFrameType) const;
~~~
 Modifying itself is done based on |fetch_client_settings_object|.

 |document_for_logging| is used only for logging, use counters,
 UKM-related things.

### Opener

FrameLoader::Opener
~~~cpp
Frame* Opener();
~~~

### SetOpener

FrameLoader::SetOpener
~~~cpp
void SetOpener(LocalFrame*);
~~~

### Detach

FrameLoader::Detach
~~~cpp
void Detach();
~~~

### FinishedParsing

FrameLoader::FinishedParsing
~~~cpp
void FinishedParsing();
~~~

### DidFinishNavigation

FrameLoader::DidFinishNavigation
~~~cpp
void DidFinishNavigation(NavigationFinishState);
~~~

### ProcessScrollForSameDocumentNavigation

FrameLoader::ProcessScrollForSameDocumentNavigation
~~~cpp
void ProcessScrollForSameDocumentNavigation(
      const KURL&,
      WebFrameLoadType,
      absl::optional<HistoryItem::ViewState>,
      mojom::blink::ScrollRestorationType);
~~~

### DetachDocument

FrameLoader::DetachDocument
~~~cpp
bool DetachDocument();
~~~
 This will attempt to detach the current document. It will dispatch unload
 events and abort XHR requests. Returns true if the frame is ready to
 receive the next document commit, or false otherwise.

### ShouldClose

FrameLoader::ShouldClose
~~~cpp
bool ShouldClose(bool is_reload = false);
~~~

### DispatchUnloadEventAndFillOldDocumentInfoIfNeeded

FrameLoader::DispatchUnloadEventAndFillOldDocumentInfoIfNeeded
~~~cpp
void DispatchUnloadEventAndFillOldDocumentInfoIfNeeded(
      bool will_commit_new_document_in_this_frame);
~~~
 Dispatches the Unload event for the current document and fills in this
 document's info in OldDocumentInfoForCommit if
 `will_commit_new_document_in_this_frame` is true (which will only be
 the case when the current document in this frame is being unloaded for
 committing a new document).

### AllowPlugins

FrameLoader::AllowPlugins
~~~cpp
bool AllowPlugins();
~~~

### SaveScrollAnchor

FrameLoader::SaveScrollAnchor
~~~cpp
void SaveScrollAnchor();
~~~

### SaveScrollState

FrameLoader::SaveScrollState
~~~cpp
void SaveScrollState();
~~~

### RestoreScrollPositionAndViewState

FrameLoader::RestoreScrollPositionAndViewState
~~~cpp
void RestoreScrollPositionAndViewState();
~~~

### HasProvisionalNavigation

HasProvisionalNavigation
~~~cpp
bool HasProvisionalNavigation() const {
    return committing_navigation_ || client_navigation_.get();
  }
~~~

### CancelClientNavigation

FrameLoader::CancelClientNavigation
~~~cpp
void CancelClientNavigation(
      CancelNavigationReason reason = CancelNavigationReason::kOther);
~~~
 Like ClearClientNavigation, but also notifies the client to actually cancel
 the navigation.

### Trace

FrameLoader::Trace
~~~cpp
void Trace(Visitor*) const;
~~~

### DidDropNavigation

FrameLoader::DidDropNavigation
~~~cpp
void DidDropNavigation();
~~~

### HasAccessedInitialDocument

HasAccessedInitialDocument
~~~cpp
bool HasAccessedInitialDocument() { return has_accessed_initial_document_; }
~~~

### SetIsNotOnInitialEmptyDocument

SetIsNotOnInitialEmptyDocument
~~~cpp
void SetIsNotOnInitialEmptyDocument() {
    // The "initial empty document" state can be false if the frame has loaded
    // a non-initial/synchronous about:blank document, or if the document has
    // done a document.open() before. However, this function can only be called
    // when a frame is first re-created in a new renderer, which can only be
    // caused by a new document load. So, we know that the state must be set to
    // kNotInitialOrSynchronousAboutBlank instead of
    // kInitialOrSynchronousAboutBlankButExplicitlyOpened here.
    initial_empty_document_status_ =
        InitialEmptyDocumentStatus::kNotInitialOrSynchronousAboutBlank;
  }
~~~

### IsOnInitialEmptyDocument

IsOnInitialEmptyDocument
~~~cpp
bool IsOnInitialEmptyDocument() {
    return initial_empty_document_status_ ==
           InitialEmptyDocumentStatus::kInitialOrSynchronousAboutBlank;
  }
~~~
 Whether the frame's current document is still considered as the "initial
 empty document" or not. Might be false even when
 HasLoadedNonInitialEmptyDocument() is false, if the frame is still on the
 first about:blank document that loaded in the frame, but it has done
 a document.open(), causing it to lose its "initial empty document"-ness
 even though it's still on the same document.

### HasLoadedNonInitialEmptyDocument

HasLoadedNonInitialEmptyDocument
~~~cpp
bool HasLoadedNonInitialEmptyDocument() {
    return initial_empty_document_status_ ==
           InitialEmptyDocumentStatus::kNotInitialOrSynchronousAboutBlank;
  }
~~~
 Whether the frame has loaded a document that is not the initial empty
 document. Might be false even when IsOnInitialEmptyDocument() is false (see
 comment for IsOnInitialEmptyDocument() for details).

### NeedsHistoryItemRestore

FrameLoader::NeedsHistoryItemRestore
~~~cpp
static bool NeedsHistoryItemRestore(WebFrameLoadType type);
~~~

### WriteIntoTrace

FrameLoader::WriteIntoTrace
~~~cpp
void WriteIntoTrace(perfetto::TracedValue context) const;
~~~

### CreateWorkerCodeCacheHost

FrameLoader::CreateWorkerCodeCacheHost
~~~cpp
mojo::PendingRemote<mojom::blink::CodeCacheHost> CreateWorkerCodeCacheHost();
~~~

### ShouldPerformFragmentNavigation

FrameLoader::ShouldPerformFragmentNavigation
~~~cpp
bool ShouldPerformFragmentNavigation(bool is_form_submission,
                                       const String& http_method,
                                       WebFrameLoadType,
                                       const KURL&);
~~~

### ProcessFragment

FrameLoader::ProcessFragment
~~~cpp
void ProcessFragment(const KURL&, WebFrameLoadType, LoadStartType);
~~~

### CancelProvisionalLoaderForNewNavigation

FrameLoader::CancelProvisionalLoaderForNewNavigation
~~~cpp
bool CancelProvisionalLoaderForNewNavigation();
~~~
 Returns whether we should continue with new navigation.

### ClearClientNavigation

FrameLoader::ClearClientNavigation
~~~cpp
void ClearClientNavigation();
~~~
 Clears any information about client navigation, see client_navigation_.

### RestoreScrollPositionAndViewState

FrameLoader::RestoreScrollPositionAndViewState
~~~cpp
void RestoreScrollPositionAndViewState(WebFrameLoadType,
                                         const HistoryItem::ViewState&,
                                         mojom::blink::ScrollRestorationType);
~~~

### DetachDocumentLoader

FrameLoader::DetachDocumentLoader
~~~cpp
void DetachDocumentLoader(Member<DocumentLoader>&,
                            bool flush_microtask_queue = false);
~~~

### TakeObjectSnapshot

FrameLoader::TakeObjectSnapshot
~~~cpp
void TakeObjectSnapshot() const;
~~~

### CommitDocumentLoader

FrameLoader::CommitDocumentLoader
~~~cpp
void CommitDocumentLoader(DocumentLoader* document_loader,
                            HistoryItem* previous_history_item,
                            CommitReason);
~~~
 Commits the given |document_loader|.

### Client

FrameLoader::Client
~~~cpp
LocalFrameClient* Client() const;
~~~

### ApplyUserAgentOverride

FrameLoader::ApplyUserAgentOverride
~~~cpp
String ApplyUserAgentOverride(const String& user_agent) const;
~~~
