### ~DocumentLoader

~DocumentLoader
~~~cpp
~DocumentLoader() override
~~~

### CreateWebNavigationParamsToCloneDocument

CreateWebNavigationParamsToCloneDocument
~~~cpp
std::unique_ptr<WebNavigationParams>
  CreateWebNavigationParamsToCloneDocument();
~~~
 Returns WebNavigationParams that can be used to clone DocumentLoader. Used
 for javascript: URL and XSLT commits, where we want to create a new
 Document but keep most of the property of the current DocumentLoader.

### WillLoadUrlAsEmpty

WillLoadUrlAsEmpty
~~~cpp
static bool WillLoadUrlAsEmpty(const KURL&);
~~~

### GetFrame

GetFrame
~~~cpp
LocalFrame* GetFrame() const { return frame_; }
~~~

### TakeNavigationTimingInfo

TakeNavigationTimingInfo
~~~cpp
mojom::blink::ResourceTimingInfoPtr TakeNavigationTimingInfo();
~~~

### DetachFromFrame

DetachFromFrame
~~~cpp
void DetachFromFrame(bool flush_microtask_queue);
~~~

### MainResourceIdentifier

MainResourceIdentifier
~~~cpp
uint64_t MainResourceIdentifier() const;
~~~

### MimeType

MimeType
~~~cpp
const AtomicString& MimeType() const;
~~~

### OriginalReferrer

OriginalReferrer
~~~cpp
WebString OriginalReferrer() const override;
~~~
 WebDocumentLoader overrides:
### GetUrl

GetUrl
~~~cpp
WebURL GetUrl() const override { return Url(); }
~~~

### HttpMethod

HttpMethod
~~~cpp
WebString HttpMethod() const override;
~~~

### Referrer

Referrer
~~~cpp
WebString Referrer() const override { return GetReferrer(); }
~~~

### HasUnreachableURL

HasUnreachableURL
~~~cpp
bool HasUnreachableURL() const override {
    return !UnreachableURL().IsEmpty();
  }
~~~

### UnreachableWebURL

UnreachableWebURL
~~~cpp
WebURL UnreachableWebURL() const override { return UnreachableURL(); }
~~~

### GetWebResponse

GetWebResponse
~~~cpp
const WebURLResponse& GetWebResponse() const override {
    return response_wrapper_;
  }
~~~

### IsClientRedirect

IsClientRedirect
~~~cpp
bool IsClientRedirect() const override { return is_client_redirect_; }
~~~

### ReplacesCurrentHistoryItem

ReplacesCurrentHistoryItem
~~~cpp
bool ReplacesCurrentHistoryItem() const override {
    return replaces_current_history_item_;
  }
~~~

### GetNavigationType

GetNavigationType
~~~cpp
WebNavigationType GetNavigationType() const override {
    return navigation_type_;
  }
~~~

### GetExtraData

GetExtraData
~~~cpp
ExtraData* GetExtraData() const override;
~~~

### TakeExtraData

TakeExtraData
~~~cpp
std::unique_ptr<ExtraData> TakeExtraData() override;
~~~

### SetExtraData

SetExtraData
~~~cpp
void SetExtraData(std::unique_ptr<ExtraData>) override;
~~~

### SetSubresourceFilter

SetSubresourceFilter
~~~cpp
void SetSubresourceFilter(WebDocumentSubresourceFilter*) override;
~~~

### SetServiceWorkerNetworkProvider

SetServiceWorkerNetworkProvider
~~~cpp
void SetServiceWorkerNetworkProvider(
      std::unique_ptr<WebServiceWorkerNetworkProvider>) override;
~~~

### GetServiceWorkerNetworkProvider

GetServiceWorkerNetworkProvider
~~~cpp
WebServiceWorkerNetworkProvider* GetServiceWorkerNetworkProvider() override {
    return service_worker_network_provider_.get();
  }
~~~
 May return null before the first HTML tag is inserted by the
 parser (before didCreateDataSource is called), after the document
 is detached from frame, or in tests.

### BlockParser

BlockParser
~~~cpp
void BlockParser() override;
~~~
 Can be used to temporarily suspend feeding the parser with new data. The
 parser will be allowed to read new data when ResumeParser() is called the
 same number of time than BlockParser().

### ResumeParser

ResumeParser
~~~cpp
void ResumeParser() override;
~~~

### HasBeenLoadedAsWebArchive

HasBeenLoadedAsWebArchive
~~~cpp
bool HasBeenLoadedAsWebArchive() const override { return archive_; }
~~~

### GetArchiveInfo

GetArchiveInfo
~~~cpp
WebArchiveInfo GetArchiveInfo() const override;
~~~

### LastNavigationHadTransientUserActivation

LastNavigationHadTransientUserActivation
~~~cpp
bool LastNavigationHadTransientUserActivation() const override {
    return last_navigation_had_transient_user_activation_;
  }
~~~

### SetCodeCacheHost

SetCodeCacheHost
~~~cpp
void SetCodeCacheHost(
      CrossVariantMojoRemote<mojom::blink::CodeCacheHostInterfaceBase>
          code_cache_host) override;
~~~

### OriginCalculationDebugInfo

OriginCalculationDebugInfo
~~~cpp
WebString OriginCalculationDebugInfo() const override {
    return origin_calculation_debug_info_;
  }
~~~

### Archive

Archive
~~~cpp
MHTMLArchive* Archive() const { return archive_.Get(); }
~~~

### GetSubresourceFilter

GetSubresourceFilter
~~~cpp
SubresourceFilter* GetSubresourceFilter() const {
    return subresource_filter_.Get();
  }
~~~

### Url

Url
~~~cpp
const KURL& Url() const;
~~~
 TODO(dcheng, japhet): Some day, Document::Url() will always match
 DocumentLoader::Url(), and one of them will be removed. Today is not that
 day though.

### UrlForHistory

UrlForHistory
~~~cpp
const KURL& UrlForHistory() const;
~~~

### GetReferrer

GetReferrer
~~~cpp
const AtomicString& GetReferrer() const;
~~~

### GetRequestorOrigin

GetRequestorOrigin
~~~cpp
const SecurityOrigin* GetRequestorOrigin() const;
~~~

### UnreachableURL

UnreachableURL
~~~cpp
const KURL& UnreachableURL() const;
~~~

### ForceFetchCacheMode

ForceFetchCacheMode
~~~cpp
const absl::optional<blink::mojom::FetchCacheMode>& ForceFetchCacheMode()
      const;
~~~

### DidChangePerformanceTiming

DidChangePerformanceTiming
~~~cpp
void DidChangePerformanceTiming();
~~~

### DidObserveInputDelay

DidObserveInputDelay
~~~cpp
void DidObserveInputDelay(base::TimeDelta input_delay);
~~~

### DidObserveLoadingBehavior

DidObserveLoadingBehavior
~~~cpp
void DidObserveLoadingBehavior(LoadingBehaviorFlag);
~~~

### RunURLAndHistoryUpdateSteps

RunURLAndHistoryUpdateSteps
~~~cpp
void RunURLAndHistoryUpdateSteps(const KURL&,
                                   HistoryItem*,
                                   mojom::blink::SameDocumentNavigationType,
                                   scoped_refptr<SerializedScriptValue>,
                                   WebFrameLoadType,
                                   bool is_browser_initiated = false,
                                   bool is_synchronously_committed = true);
~~~
 https://html.spec.whatwg.org/multipage/history.html#url-and-history-update-steps
### UpdateForSameDocumentNavigation

UpdateForSameDocumentNavigation
~~~cpp
void UpdateForSameDocumentNavigation(
      const KURL&,
      HistoryItem*,
      mojom::blink::SameDocumentNavigationType,
      scoped_refptr<SerializedScriptValue>,
      WebFrameLoadType,
      const SecurityOrigin* initiator_origin,
      bool is_browser_initiated,
      bool is_synchronously_committed,
      absl::optional<scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id);
~~~
 |is_synchronously_committed| is described in comment for
 CommitSameDocumentNavigation.

### GetResponse

GetResponse
~~~cpp
const ResourceResponse& GetResponse() const { return response_; }
~~~

### IsCommittedButEmpty

IsCommittedButEmpty
~~~cpp
bool IsCommittedButEmpty() const {
    return state_ >= kCommitted && !data_received_;
  }
~~~

### SetSentDidFinishLoad

SetSentDidFinishLoad
~~~cpp
void SetSentDidFinishLoad() { state_ = kSentDidFinishLoad; }
~~~

### SentDidFinishLoad

SentDidFinishLoad
~~~cpp
bool SentDidFinishLoad() const { return state_ == kSentDidFinishLoad; }
~~~

### LoadType

LoadType
~~~cpp
WebFrameLoadType LoadType() const { return load_type_; }
~~~

### SetLoadType

SetLoadType
~~~cpp
void SetLoadType(WebFrameLoadType load_type) { load_type_ = load_type; }
~~~

### SetNavigationType

SetNavigationType
~~~cpp
void SetNavigationType(WebNavigationType navigation_type) {
    navigation_type_ = navigation_type;
  }
~~~

### GetHistoryItem

GetHistoryItem
~~~cpp
HistoryItem* GetHistoryItem() const { return history_item_; }
~~~

### StartLoading

StartLoading
~~~cpp
void StartLoading();
~~~

### StopLoading

StopLoading
~~~cpp
void StopLoading();
~~~

### CommitNavigation

CommitNavigation
~~~cpp
void CommitNavigation();
~~~
 CommitNavigation() does the work of creating a Document and
 DocumentParser, as well as creating a new LocalDOMWindow if needed. It also
 initializes a bunch of state on the Document (e.g., the state based on
 response headers).

### CommitSameDocumentNavigation

CommitSameDocumentNavigation
~~~cpp
mojom::CommitResult CommitSameDocumentNavigation(
      const KURL&,
      WebFrameLoadType,
      HistoryItem*,
      ClientRedirectPolicy,
      bool has_transient_user_activation,
      const SecurityOrigin* initiator_origin,
      bool is_synchronously_committed,
      mojom::blink::TriggeringEventInfo,
      bool is_browser_initiated,
      absl::optional<scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id);
~~~
 Called when the browser process has asked this renderer process to commit a
 same document navigation in that frame. Returns false if the navigation
 cannot commit, true otherwise.

 |initiator_origin| is the origin of the document or script that initiated
 the navigation or nullptr if the navigation is initiated via browser UI
 (e.g. typed in omnibox), or a history traversal to a previous navigation
 via browser UI.

 |is_synchronously_committed| is true if the navigation is synchronously
 committed from within Blink, rather than being driven by the browser's
 navigation stack.

### SetDefersLoading

SetDefersLoading
~~~cpp
void SetDefersLoading(LoaderFreezeMode);
~~~

### GetTiming

GetTiming
~~~cpp
DocumentLoadTiming& GetTiming() { return document_load_timing_; }
~~~

### GetInitialScrollState

GetInitialScrollState
~~~cpp
InitialScrollState& GetInitialScrollState() { return initial_scroll_state_; }
~~~

### DispatchLinkHeaderPreloads

DispatchLinkHeaderPreloads
~~~cpp
void DispatchLinkHeaderPreloads(const ViewportDescription*,
                                  PreloadHelper::LoadLinksFromHeaderMode);
~~~

### LoadFailed

LoadFailed
~~~cpp
void LoadFailed(const ResourceError&);
~~~

### Trace

Trace
~~~cpp
void Trace(Visitor*) const override;
~~~

### GetDevToolsNavigationToken

GetDevToolsNavigationToken
~~~cpp
const base::UnguessableToken& GetDevToolsNavigationToken() {
    return devtools_navigation_token_;
  }
~~~
 For automation driver-initiated navigations over the devtools protocol,
 |devtools_navigation_token_| is used to tag the navigation. This navigation
 token is then sent into the renderer and lands on the DocumentLoader. That
 way subsequent Blink-level frame lifecycle events can be associated with
 the concrete navigation.

 - The value should not be sent back to the browser.

 - The value on DocumentLoader may be generated in the renderer in some
 cases, and thus shouldn't be trusted.

 TODO(crbug.com/783506): Replace devtools navigation token with the generic
 navigation token that can be passed from renderer to the browser.

### GetUseCounter

GetUseCounter
~~~cpp
UseCounterImpl& GetUseCounter() { return use_counter_; }
~~~

### GetPrefetchedSignedExchangeManager

GetPrefetchedSignedExchangeManager
~~~cpp
PrefetchedSignedExchangeManager* GetPrefetchedSignedExchangeManager() const;
~~~

### CountUse

CountUse
~~~cpp
void CountUse(mojom::WebFeature) override;
~~~
 UseCounter
### CountDeprecation

CountDeprecation
~~~cpp
void CountDeprecation(mojom::WebFeature) override;
~~~

### SetCommitReason

SetCommitReason
~~~cpp
void SetCommitReason(CommitReason reason) { commit_reason_ = reason; }
~~~

### IsBrowserInitiated

IsBrowserInitiated
~~~cpp
bool IsBrowserInitiated() const { return is_browser_initiated_; }
~~~
 Whether the navigation originated from the browser process. Note: history
 navigation is always considered to be browser initiated, even if the
 navigation was started using the history API in the renderer.

### LastNavigationHadTrustedInitiator

LastNavigationHadTrustedInitiator
~~~cpp
bool LastNavigationHadTrustedInitiator() const {
    return last_navigation_had_trusted_initiator_;
  }
~~~

### DidOpenDocumentInputStream

DidOpenDocumentInputStream
~~~cpp
void DidOpenDocumentInputStream(const KURL& url);
~~~
 Called when the URL needs to be updated due to a document.open() call.

### SetHistoryItemStateForCommit

SetHistoryItemStateForCommit
~~~cpp
void SetHistoryItemStateForCommit(HistoryItem* old_item,
                                    WebFrameLoadType,
                                    HistoryNavigationType,
                                    CommitReason commit_reason);
~~~

### NavigationScrollAllowed

NavigationScrollAllowed
~~~cpp
bool NavigationScrollAllowed() const { return navigation_scroll_allowed_; }
~~~

### RemainingTimeToLCPLimit

RemainingTimeToLCPLimit
~~~cpp
base::TimeDelta RemainingTimeToLCPLimit() const;
~~~
 We want to make sure that the largest content is painted before the "LCP
 limit", so that we get a good LCP value. This returns the remaining time to
 the LCP limit. See crbug.com/1065508 for details.

### RemainingTimeToRenderBlockingFontMaxBlockingTime

RemainingTimeToRenderBlockingFontMaxBlockingTime
~~~cpp
base::TimeDelta RemainingTimeToRenderBlockingFontMaxBlockingTime() const;
~~~
 We are experimenting the idea of making preloaded fonts render-blocking up
 to a certain amount of time after navigation starts. This returns the
 remaining time to that time limit. See crbug.com/1412861 for details.

### GetContentSecurityNotifier

GetContentSecurityNotifier
~~~cpp
mojom::blink::ContentSecurityNotifier& GetContentSecurityNotifier();
~~~

### ConsumeTextFragmentToken

ConsumeTextFragmentToken
~~~cpp
bool ConsumeTextFragmentToken();
~~~
 Returns the value of the text fragment token and then resets it to false
 to ensure the token can only be used to invoke a single text fragment.

### NotifyPrerenderingDocumentActivated

NotifyPrerenderingDocumentActivated
~~~cpp
void NotifyPrerenderingDocumentActivated(
      const mojom::blink::PrerenderPageActivationParams& params);
~~~
 Notifies that the prerendering document this loader is working for is
 activated.

### GetCodeCacheHost

GetCodeCacheHost
~~~cpp
CodeCacheHost* GetCodeCacheHost();
~~~

### DisableCodeCacheForTesting

DisableCodeCacheForTesting
~~~cpp
static void DisableCodeCacheForTesting();
~~~

### CreateWorkerCodeCacheHost

CreateWorkerCodeCacheHost
~~~cpp
mojo::PendingRemote<mojom::blink::CodeCacheHost> CreateWorkerCodeCacheHost();
~~~

### GetEarlyHintsPreloadedResources

GetEarlyHintsPreloadedResources
~~~cpp
HashMap<KURL, EarlyHintsPreloadEntry> GetEarlyHintsPreloadedResources();
~~~

### AdAuctionComponents

AdAuctionComponents
~~~cpp
const absl::optional<Vector<KURL>>& AdAuctionComponents() const {
    return ad_auction_components_;
  }
~~~

### FencedFrameProperties

FencedFrameProperties
~~~cpp
const absl::optional<FencedFrame::RedactedFencedFrameProperties>&
  FencedFrameProperties() const {
    return fenced_frame_properties_;
  }
~~~

### IsReloadedOrFormSubmitted

IsReloadedOrFormSubmitted
~~~cpp
bool IsReloadedOrFormSubmitted() const;
~~~
 Detect if the page is reloaded or after form submitted. This method is
 called in order to disable some interventions or optimizations based on the
 heuristic that the user might reload the page when interventions cause
 problems. Also, the user is likely to avoid reloading the page when they
 submit forms. So this method is useful to skip interventions in the
 following conditions.

 - Reload a page.

 - Submit a form.

 - Resubmit a form.

 The reason why we use DocumentLoader::GetNavigationType() instead of
 DocumentLoader::LoadType() is that DocumentLoader::LoadType() is reset to
 WebFrameLoadType::kStandard on DidFinishNavigation(). When JavaScript adds
 iframes after navigation, DocumentLoader::LoadType() always returns
 WebFrameLoadType::kStandard. DocumentLoader::GetNavigationType() doesn't
 have this problem.

### MaybeRecordServiceWorkerFallbackMainResource

MaybeRecordServiceWorkerFallbackMainResource
~~~cpp
void MaybeRecordServiceWorkerFallbackMainResource(
      bool was_subresource_fetched_via_service_worker);
~~~
 (crbug.com/1371756) Record the page if the main resource is not fetched via
 ServiceWorker and at least one subresource is fetched via ServiceWorker.

 This method won't record the page if the main resource was not controlled
 by ServiceWorker at the time of the initial navigation. This helps us to
 understand the potential impact of the fetch fast-path effort.

### ServiceWorkerInitialControllerMode

ServiceWorkerInitialControllerMode
~~~cpp
mojom::blink::ControllerServiceWorkerMode ServiceWorkerInitialControllerMode()
      const {
    return service_worker_initial_controller_mode_;
  }
~~~
 (crbug.com/1371756) Returns the initial state of
 ControllerServiceWorkerMode in the document. We store this info to capture
 the case when the main document has installed ServiceWorker and the page is
 already controlled or not.

### MaybeStartLoadingBodyInBackground

MaybeStartLoadingBodyInBackground
~~~cpp
static void MaybeStartLoadingBodyInBackground(
      WebNavigationBodyLoader* body_loader,
      LocalFrame* frame,
      const KURL& url,
      const ResourceResponse& response);
~~~
 Starts loading the navigation body in a background thread.

### GetNavigationDeliveryType

GetNavigationDeliveryType
~~~cpp
network::mojom::NavigationDeliveryType GetNavigationDeliveryType() const {
    return navigation_delivery_type_;
  }
~~~

### UpdateSubresourceLoadMetrics

UpdateSubresourceLoadMetrics
~~~cpp
void UpdateSubresourceLoadMetrics(
      uint32_t number_of_subresources_loaded,
      uint32_t number_of_subresource_loads_handled_by_service_worker,
      bool pervasive_payload_requested,
      int64_t pervasive_bytes_fetched,
      int64_t total_bytes_fetched);
~~~

### CalculateOwnerFrame

CalculateOwnerFrame
~~~cpp
Frame* CalculateOwnerFrame();
~~~

### CalculateOrigin

CalculateOrigin
~~~cpp
scoped_refptr<SecurityOrigin> CalculateOrigin(Document* owner_document);
~~~

### InitializeWindow

InitializeWindow
~~~cpp
void InitializeWindow(Document* owner_document);
~~~

### DidInstallNewDocument

DidInstallNewDocument
~~~cpp
void DidInstallNewDocument(Document*);
~~~

### WillCommitNavigation

WillCommitNavigation
~~~cpp
void WillCommitNavigation();
~~~

### DidCommitNavigation

DidCommitNavigation
~~~cpp
void DidCommitNavigation();
~~~

### RecordUseCountersForCommit

RecordUseCountersForCommit
~~~cpp
void RecordUseCountersForCommit();
~~~

### RecordConsoleMessagesForCommit

RecordConsoleMessagesForCommit
~~~cpp
void RecordConsoleMessagesForCommit();
~~~

### RecordAcceptLanguageAndContentLanguageMetric

RecordAcceptLanguageAndContentLanguageMetric
~~~cpp
void RecordAcceptLanguageAndContentLanguageMetric();
~~~
 Use to record UMA metrics on the matches between the Content-Language
 response header value and the Accept-Language request header values.

### RecordParentAndChildContentLanguageMetric

RecordParentAndChildContentLanguageMetric
~~~cpp
void RecordParentAndChildContentLanguageMetric();
~~~
 Use to record UMA metrics on the matches between the parent frame's
 Content-Language request header value and child frame's Content-Language
 request header values.

### CreateParserPostCommit

CreateParserPostCommit
~~~cpp
void CreateParserPostCommit();
~~~

### CommitSameDocumentNavigationInternal

CommitSameDocumentNavigationInternal
~~~cpp
void CommitSameDocumentNavigationInternal(
      const KURL&,
      WebFrameLoadType,
      HistoryItem*,
      mojom::blink::SameDocumentNavigationType,
      ClientRedirectPolicy,
      bool has_transient_user_activation,
      const SecurityOrigin* initiator_origin,
      bool is_browser_initiated,
      bool is_synchronously_committed,
      mojom::blink::TriggeringEventInfo,
      absl::optional<scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id);
~~~

### GetFrameLoader

GetFrameLoader
~~~cpp
FrameLoader& GetFrameLoader() const;
~~~
 Use these method only where it's guaranteed that |m_frame| hasn't been
 cleared.

### GetLocalFrameClient

GetLocalFrameClient
~~~cpp
LocalFrameClient& GetLocalFrameClient() const;
~~~

### ConsoleError

ConsoleError
~~~cpp
void ConsoleError(const String& message);
~~~

### ReplaceWithEmptyDocument

ReplaceWithEmptyDocument
~~~cpp
void ReplaceWithEmptyDocument();
~~~
 Replace the current document with a empty one and the URL with a unique
 opaque origin.

### CreateDocumentPolicy

CreateDocumentPolicy
~~~cpp
DocumentPolicy::ParsedDocumentPolicy CreateDocumentPolicy();
~~~

### StartLoadingInternal

StartLoadingInternal
~~~cpp
void StartLoadingInternal();
~~~

### StartLoadingResponse

StartLoadingResponse
~~~cpp
void StartLoadingResponse();
~~~

### FinishedLoading

FinishedLoading
~~~cpp
void FinishedLoading(base::TimeTicks finish_time);
~~~

### CancelLoadAfterCSPDenied

CancelLoadAfterCSPDenied
~~~cpp
void CancelLoadAfterCSPDenied(const ResourceResponse&);
~~~

### HandleRedirect

HandleRedirect
~~~cpp
void HandleRedirect(WebNavigationParams::RedirectInfo& redirect);
~~~
 Process a redirect to update the redirect chain, current URL, referrer,
 etc.

### HandleResponse

HandleResponse
~~~cpp
void HandleResponse();
~~~

### InitializeEmptyResponse

InitializeEmptyResponse
~~~cpp
void InitializeEmptyResponse();
~~~

### CommitData

CommitData
~~~cpp
void CommitData(BodyData& data);
~~~

### ProcessDataBuffer

ProcessDataBuffer
~~~cpp
void ProcessDataBuffer(BodyData* data = nullptr);
~~~
 Processes the data stored in |data_buffer_| or |decoded_data_buffer_|, used
 to avoid appending data to the parser in a nested message loop.

### BodyDataReceivedImpl

BodyDataReceivedImpl
~~~cpp
void BodyDataReceivedImpl(BodyData& data);
~~~

### BodyDataReceived

BodyDataReceived
~~~cpp
void BodyDataReceived(base::span<const char> data) override;
~~~
 WebNavigationBodyLoader::Client
### DecodedBodyDataReceived

DecodedBodyDataReceived
~~~cpp
void DecodedBodyDataReceived(const WebString& data,
                               const WebEncodingData& encoding_data,
                               base::span<const char> encoded_data) override;
~~~

### BodyLoadingFinished

BodyLoadingFinished
~~~cpp
void BodyLoadingFinished(base::TimeTicks completion_time,
                           int64_t total_encoded_data_length,
                           int64_t total_encoded_body_length,
                           int64_t total_decoded_body_length,
                           bool should_report_corb_blocking,
                           const absl::optional<WebURLError>& error) override;
~~~

### TakeProcessBackgroundDataCallback

TakeProcessBackgroundDataCallback
~~~cpp
ProcessBackgroundDataCallback TakeProcessBackgroundDataCallback() override;
~~~

### ApplyClientHintsConfig

ApplyClientHintsConfig
~~~cpp
void ApplyClientHintsConfig(
      const WebVector<network::mojom::WebClientHintsType>&
          enabled_client_hints);
~~~

### InitializePrefetchedSignedExchangeManager

InitializePrefetchedSignedExchangeManager
~~~cpp
void InitializePrefetchedSignedExchangeManager();
~~~
 If the page was loaded from a signed exchange which has "allowed-alt-sxg"
 link headers in the inner response and PrefetchedSignedExchanges were
 passed from the previous page, initializes a
 PrefetchedSignedExchangeManager which will hold the subresource signed
 exchange related headers ("alternate" link header in the outer response and
 "allowed-alt-sxg" link header in the inner response of the page's signed
 exchange), and the passed PrefetchedSignedExchanges. The created
 PrefetchedSignedExchangeManager will be used to load the prefetched signed
 exchanges for matching requests.

### IsJavaScriptURLOrXSLTCommit

IsJavaScriptURLOrXSLTCommit
~~~cpp
bool IsJavaScriptURLOrXSLTCommit() const {
    return commit_reason_ == CommitReason::kJavascriptUrl ||
           commit_reason_ == CommitReason::kXSLT;
  }
~~~

### CreateCSP

CreateCSP
~~~cpp
ContentSecurityPolicy* CreateCSP();
~~~
 Computes and creates CSP for this document.

### IsSameOriginInitiator

IsSameOriginInitiator
~~~cpp
bool IsSameOriginInitiator() const;
~~~

### StartViewTransitionIfNeeded

StartViewTransitionIfNeeded
~~~cpp
void StartViewTransitionIfNeeded(Document& document);
~~~
 This initiates a view transition if the `view_transition_state_` has been
 specified.

## class ;

### GetInitialScrollState

GetInitialScrollState
~~~cpp
InitialScrollState& GetInitialScrollState() { return initial_scroll_state_; }
~~~
