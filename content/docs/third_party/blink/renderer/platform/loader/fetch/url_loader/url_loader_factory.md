
## class URLLoaderFactory
 An abstract interface to create a URLLoader. It is expected that each
 loading context holds its own per-context URLLoaderFactory.

### URLLoaderFactory

URLLoaderFactory
~~~cpp
URLLoaderFactory(const URLLoaderFactory&) = delete;
~~~

### operator=

URLLoaderFactory::operator=
~~~cpp
URLLoaderFactory& operator=(const URLLoaderFactory&) = delete;
~~~

### ~URLLoaderFactory

URLLoaderFactory::~URLLoaderFactory
~~~cpp
~URLLoaderFactory()
~~~

### CreateURLLoader

URLLoaderFactory::CreateURLLoader
~~~cpp
virtual std::unique_ptr<URLLoader> CreateURLLoader(
      const WebURLRequest& webreq,
      scoped_refptr<base::SingleThreadTaskRunner> freezable_task_runner,
      scoped_refptr<base::SingleThreadTaskRunner> unfreezable_task_runner,
      mojo::PendingRemote<mojom::blink::KeepAliveHandle> keep_alive_handle,
      BackForwardCacheLoaderHelper* back_forward_cache_loader_helper);
~~~
 Returns a new URLLoader instance. This should internally choose
 the most appropriate URLLoaderFactory implementation.

 TODO(yuzus): Only take unfreezable task runner once both
 URLLoaderClientImpl and ResponseBodyLoader use unfreezable task runner.

 This currently takes two task runners: freezable and unfreezable one.

## class URLLoaderFactoryForTest
 A test version of the above factory interface, which supports cloning the
 factory.

### Clone

Clone
~~~cpp
virtual std::unique_ptr<URLLoaderFactoryForTest> Clone() = 0;
~~~
 Clones this factory.

### Clone

Clone
~~~cpp
virtual std::unique_ptr<URLLoaderFactoryForTest> Clone() = 0;
~~~
 Clones this factory.
