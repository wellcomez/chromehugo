
## class ThrottlingURLLoader
    : public
 ThrottlingURLLoader is a wrapper around the network::mojom::URLLoader[Factory] interfaces. 
 
 It applies a list of URLLoaderThrottle instances which could defer, resume restart or cancel the
 URL loading. 
 If the Mojo connection fails during the request it is canceled
with net::ERR_ABORTED.

### CreateLoaderAndStart

ThrottlingURLLoader
    : public::CreateLoaderAndStart
~~~cpp
static std::unique_ptr<ThrottlingURLLoader> CreateLoaderAndStart(
      scoped_refptr<network::SharedURLLoaderFactory> factory,
      std::vector<std::unique_ptr<URLLoaderThrottle>> throttles,
      int32_t request_id,
      uint32_t options,
      network::ResourceRequest* url_request,
      network::mojom::URLLoaderClient* client,
      const net::NetworkTrafficAnnotationTag& traffic_annotation,
      scoped_refptr<base::SingleThreadTaskRunner> task_runner,
      absl::optional<std::vector<std::string>> cors_exempt_header_list =
          absl::nullopt);
~~~
 |url_request| can be mutated by this function, and doesn't need to stay
 alive after calling this function.


 |client| must stay alive during the lifetime of the returned object. Please
 note that the request may not start immediately since it could be deferred
 by throttles.

### ThrottlingURLLoader

ThrottlingURLLoader
~~~cpp
ThrottlingURLLoader(const ThrottlingURLLoader&) = delete;
~~~

### operator=

ThrottlingURLLoader
    : public::operator=
~~~cpp
ThrottlingURLLoader& operator=(const ThrottlingURLLoader&) = delete;
~~~

### ~ThrottlingURLLoader

ThrottlingURLLoader
    : public::~ThrottlingURLLoader
~~~cpp
~ThrottlingURLLoader() override
~~~

### FollowRedirectForcingRestart

ThrottlingURLLoader
    : public::FollowRedirectForcingRestart
~~~cpp
void FollowRedirectForcingRestart();
~~~
 Follows a redirect, calling CreateLoaderAndStart() on the factory. This
 is useful if the factory uses different loaders for different URLs.

### ResetForFollowRedirect

ThrottlingURLLoader
    : public::ResetForFollowRedirect
~~~cpp
void ResetForFollowRedirect(
      network::ResourceRequest& resource_request,
      const std::vector<std::string>& removed_headers,
      const net::HttpRequestHeaders& modified_headers,
      const net::HttpRequestHeaders& modified_cors_exempt_headers);
~~~
 This should be called if the loader will be recreated to follow a redirect
 instead of calling FollowRedirect(). This can be used if a loader is
 implementing similar logic to FollowRedirectForcingRestart(). If this is
 called, a future request for the redirect should be guaranteed to be sent
 with the same request_id.

 `removed_headers`, `modified_headers` and `modified_cors_exempt_headers`
 will be merged to corresponding members in the ThrottlingURLLoader, and
 then apply updates against `resource_request`.

### FollowRedirect

ThrottlingURLLoader
    : public::FollowRedirect
~~~cpp
void FollowRedirect(
      const std::vector<std::string>& removed_headers,
      const net::HttpRequestHeaders& modified_headers,
      const net::HttpRequestHeaders& modified_cors_exempt_headers);
~~~

### SetPriority

ThrottlingURLLoader
    : public::SetPriority
~~~cpp
void SetPriority(net::RequestPriority priority, int32_t intra_priority_value);
~~~

### PauseReadingBodyFromNet

ThrottlingURLLoader
    : public::PauseReadingBodyFromNet
~~~cpp
void PauseReadingBodyFromNet();
~~~

### ResumeReadingBodyFromNet

ThrottlingURLLoader
    : public::ResumeReadingBodyFromNet
~~~cpp
void ResumeReadingBodyFromNet();
~~~

### RestartWithFactory

ThrottlingURLLoader
    : public::RestartWithFactory
~~~cpp
void RestartWithFactory(
      scoped_refptr<network::SharedURLLoaderFactory> factory,
      uint32_t url_loader_options);
~~~
 Restarts the load immediately with |factory| and |url_loader_options|.

 It must only be called when the following conditions are met:
 1. The request already started and the original factory decided to not
    handle the request. This condition is required because throttles are not
    consulted prior to restarting.

 2. The original factory did not call URLLoaderClient callbacks (e.g.,
    OnReceiveResponse).

 This function is useful in the case of service worker network fallback.

### Unbind

ThrottlingURLLoader
    : public::Unbind
~~~cpp
network::mojom::URLLoaderClientEndpointsPtr Unbind();
~~~
 Disconnect the forwarding URLLoaderClient and the URLLoader. Returns the
 datapipe endpoints.

### CancelWithError

ThrottlingURLLoader
    : public::CancelWithError
~~~cpp
void CancelWithError(int error_code, base::StringPiece custom_reason);
~~~

### CancelWithExtendedError

ThrottlingURLLoader
    : public::CancelWithExtendedError
~~~cpp
void CancelWithExtendedError(int error_code,
                               int extended_reason_code,
                               base::StringPiece custom_reason);
~~~

### set_forwarding_client

set_forwarding_client
~~~cpp
void set_forwarding_client(network::mojom::URLLoaderClient* client) {
    forwarding_client_ = client;
  }
~~~
 Sets the forwarding client to receive all subsequent notifications.

### response_intercepted

response_intercepted
~~~cpp
bool response_intercepted() const { return response_intercepted_; }
~~~

### ThrottlingURLLoader

ThrottlingURLLoader
    : public::ThrottlingURLLoader
~~~cpp
ThrottlingURLLoader(
      std::vector<std::unique_ptr<URLLoaderThrottle>> throttles,
      network::mojom::URLLoaderClient* client,
      const net::NetworkTrafficAnnotationTag& traffic_annotation)
~~~

### Start

ThrottlingURLLoader
    : public::Start
~~~cpp
void Start(scoped_refptr<network::SharedURLLoaderFactory> factory,
             int32_t request_id,
             uint32_t options,
             network::ResourceRequest* url_request,
             scoped_refptr<base::SingleThreadTaskRunner> task_runner,
             absl::optional<std::vector<std::string>> cors_exempt_header_list);
~~~

### StartNow

ThrottlingURLLoader
    : public::StartNow
~~~cpp
void StartNow();
~~~

### RestartWithFlagsNow

ThrottlingURLLoader
    : public::RestartWithFlagsNow
~~~cpp
void RestartWithFlagsNow();
~~~

### HandleThrottleResult

ThrottlingURLLoader
    : public::HandleThrottleResult
~~~cpp
bool HandleThrottleResult(URLLoaderThrottle* throttle,
                            bool throttle_deferred,
                            bool* should_defer);
~~~
 Processes the result of a URLLoaderThrottle call, adding the throttle to
 the blocking set if it deferred and updating |*should_defer| accordingly.

 Returns |true| if the request should continue to be processed (regardless
 of whether it's been deferred) or |false| if it's been cancelled.

### StopDeferringForThrottle

ThrottlingURLLoader
    : public::StopDeferringForThrottle
~~~cpp
void StopDeferringForThrottle(URLLoaderThrottle* throttle);
~~~
 Stops a given throttle from deferring the request. If this was not the last
 deferring throttle, the request remains deferred. Otherwise it resumes
 progress.

### RestartWithFlags

ThrottlingURLLoader
    : public::RestartWithFlags
~~~cpp
void RestartWithFlags(int additional_load_flags);
~~~

### RestartWithURLResetAndFlags

ThrottlingURLLoader
    : public::RestartWithURLResetAndFlags
~~~cpp
void RestartWithURLResetAndFlags(int additional_load_flags);
~~~
 Restart the request using |original_url_|.

### RestartWithURLResetAndFlagsNow

ThrottlingURLLoader
    : public::RestartWithURLResetAndFlagsNow
~~~cpp
void RestartWithURLResetAndFlagsNow(int additional_load_flags);
~~~
 Restart the request immediately if the response has not started yet.

### OnReceiveEarlyHints

ThrottlingURLLoader
    : public::OnReceiveEarlyHints
~~~cpp
void OnReceiveEarlyHints(network::mojom::EarlyHintsPtr early_hints) override;
~~~
 network::mojom::URLLoaderClient implementation:
### OnReceiveResponse

ThrottlingURLLoader
    : public::OnReceiveResponse
~~~cpp
void OnReceiveResponse(
      network::mojom::URLResponseHeadPtr response_head,
      mojo::ScopedDataPipeConsumerHandle body,
      absl::optional<mojo_base::BigBuffer> cached_metadata) override;
~~~

### OnReceiveRedirect

ThrottlingURLLoader
    : public::OnReceiveRedirect
~~~cpp
void OnReceiveRedirect(
      const net::RedirectInfo& redirect_info,
      network::mojom::URLResponseHeadPtr response_head) override;
~~~

### OnUploadProgress

ThrottlingURLLoader
    : public::OnUploadProgress
~~~cpp
void OnUploadProgress(int64_t current_position,
                        int64_t total_size,
                        OnUploadProgressCallback ack_callback) override;
~~~

### OnTransferSizeUpdated

ThrottlingURLLoader
    : public::OnTransferSizeUpdated
~~~cpp
void OnTransferSizeUpdated(int32_t transfer_size_diff) override;
~~~

### OnComplete

ThrottlingURLLoader
    : public::OnComplete
~~~cpp
void OnComplete(const network::URLLoaderCompletionStatus& status) override;
~~~

### OnClientConnectionError

ThrottlingURLLoader
    : public::OnClientConnectionError
~~~cpp
void OnClientConnectionError();
~~~

### Resume

ThrottlingURLLoader
    : public::Resume
~~~cpp
void Resume();
~~~

### SetPriority

ThrottlingURLLoader
    : public::SetPriority
~~~cpp
void SetPriority(net::RequestPriority priority);
~~~

### UpdateDeferredRequestHeaders

ThrottlingURLLoader
    : public::UpdateDeferredRequestHeaders
~~~cpp
void UpdateDeferredRequestHeaders(
      const net::HttpRequestHeaders& modified_request_headers,
      const net::HttpRequestHeaders& modified_cors_exempt_request_headers);
~~~

### UpdateRequestHeaders

ThrottlingURLLoader
    : public::UpdateRequestHeaders
~~~cpp
void UpdateRequestHeaders(network::ResourceRequest& resource_request);
~~~

### UpdateDeferredResponseHead

ThrottlingURLLoader
    : public::UpdateDeferredResponseHead
~~~cpp
void UpdateDeferredResponseHead(
      network::mojom::URLResponseHeadPtr new_response_head,
      mojo::ScopedDataPipeConsumerHandle body);
~~~

### PauseReadingBodyFromNet

ThrottlingURLLoader
    : public::PauseReadingBodyFromNet
~~~cpp
void PauseReadingBodyFromNet(URLLoaderThrottle* throttle);
~~~

### ResumeReadingBodyFromNet

ThrottlingURLLoader
    : public::ResumeReadingBodyFromNet
~~~cpp
void ResumeReadingBodyFromNet(URLLoaderThrottle* throttle);
~~~

### InterceptResponse

ThrottlingURLLoader
    : public::InterceptResponse
~~~cpp
void InterceptResponse(
      mojo::PendingRemote<network::mojom::URLLoader> new_loader,
      mojo::PendingReceiver<network::mojom::URLLoaderClient>
          new_client_receiver,
      mojo::PendingRemote<network::mojom::URLLoader>* original_loader,
      mojo::PendingReceiver<network::mojom::URLLoaderClient>*
          original_client_receiver,
      mojo::ScopedDataPipeConsumerHandle* body);
~~~

### DisconnectClient

ThrottlingURLLoader
    : public::DisconnectClient
~~~cpp
void DisconnectClient(base::StringPiece custom_description);
~~~
 Disconnects the client connection and releases the URLLoader.

### GetStageNameForHistogram

ThrottlingURLLoader
    : public::GetStageNameForHistogram
~~~cpp
const char* GetStageNameForHistogram(DeferredStage stage);
~~~
