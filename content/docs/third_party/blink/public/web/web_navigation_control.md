
## class WebNavigationControl
 namespace scheduler
 This interface gives control to navigation-related functionality of
 WebLocalFrame. It is separated from WebLocalFrame to give precise control
 over callers of navigation methods.

 WebLocalFrameClient gets a reference to this interface in BindToFrame.

### ~WebNavigationControl

~WebNavigationControl
~~~cpp
~WebNavigationControl() override {}
~~~

### DispatchBeforeUnloadEvent

DispatchBeforeUnloadEvent
~~~cpp
virtual bool DispatchBeforeUnloadEvent(bool is_reload) = 0;
~~~
 Runs beforeunload handlers for this frame and its local descendants.

 Returns `true` if all the frames agreed to proceed with unloading
 from their respective event handlers.

 Note: this may lead to the destruction of the frame.

### CommitNavigation

CommitNavigation
~~~cpp
virtual void CommitNavigation(
      std::unique_ptr<WebNavigationParams> navigation_params,
      std::unique_ptr<WebDocumentLoader::ExtraData> extra_data) = 0;
~~~
 Commits a cross-document navigation in the frame. See WebNavigationParams
 for details. Calls WebLocalFrameClient::DidCommitNavigation synchronously
 after new document commit, but before loading any content, unless commit
 fails.

 TODO(dgozman): return mojom::CommitResult.

### CommitSameDocumentNavigation

CommitSameDocumentNavigation
~~~cpp
virtual mojom::CommitResult CommitSameDocumentNavigation(
      const WebURL&,
      WebFrameLoadType,
      const WebHistoryItem&,
      bool is_client_redirect,
      bool has_transient_user_activation,
      const WebSecurityOrigin& initiator_origin,
      bool is_browser_initiated,
      absl::optional<scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id) = 0;
~~~
 Commits a same-document navigation in the frame. For history navigations,
 a valid WebHistoryItem should be provided. `initiator_origin` is null
 for browser-initiated navigations. Returns CommitResult::Ok if the
 navigation has actually committed.

### SetIsNotOnInitialEmptyDocument

SetIsNotOnInitialEmptyDocument
~~~cpp
virtual void SetIsNotOnInitialEmptyDocument() = 0;
~~~
 Override the normal rules that determine whether the frame is on the
 initial empty document or not. Used to propagate state when this frame has
 navigated cross process.

### IsOnInitialEmptyDocument

IsOnInitialEmptyDocument
~~~cpp
virtual bool IsOnInitialEmptyDocument() = 0;
~~~

### WillPotentiallyStartOutermostMainFrameNavigation

WillPotentiallyStartOutermostMainFrameNavigation
~~~cpp
virtual void WillPotentiallyStartOutermostMainFrameNavigation(
      const WebURL&) const = 0;
~~~
 Notifies that a renderer-initiated navigation to `url` will
 potentially start soon. This is fired before the beforeunload event
 (if it's needed) gets dispatched in the renderer, so that the
 browser can speculatively start service worker before processing
 beforeunload event, which might take a long time. Note that the
 navigation might not actually start, e.g. if it gets canceled by
 beforeunload.

### WillStartNavigation

WillStartNavigation
~~~cpp
virtual bool WillStartNavigation(const WebNavigationInfo&) = 0;
~~~
 Marks the frame as loading, before WebLocalFrameClient issues a navigation
 request through the browser process on behalf of the frame.

 This runs some JavaScript event listeners, which may cancel the navigation
 or detach the frame. In this case the method returns false and client
 should not proceed with the navigation.

### DidDropNavigation

DidDropNavigation
~~~cpp
virtual void DidDropNavigation() = 0;
~~~
 Informs the frame that the navigation it asked the client to do was
 dropped.

### WebNavigationControl

WebNavigationControl
~~~cpp
explicit WebNavigationControl(mojom::TreeScopeType scope,
                                const LocalFrameToken& frame_token)
      : WebLocalFrame(scope, frame_token) {}
~~~

### ~WebNavigationControl

~WebNavigationControl
~~~cpp
~WebNavigationControl() override {}
~~~

### DispatchBeforeUnloadEvent

DispatchBeforeUnloadEvent
~~~cpp
virtual bool DispatchBeforeUnloadEvent(bool is_reload) = 0;
~~~
 Runs beforeunload handlers for this frame and its local descendants.

 Returns `true` if all the frames agreed to proceed with unloading
 from their respective event handlers.

 Note: this may lead to the destruction of the frame.

### CommitNavigation

CommitNavigation
~~~cpp
virtual void CommitNavigation(
      std::unique_ptr<WebNavigationParams> navigation_params,
      std::unique_ptr<WebDocumentLoader::ExtraData> extra_data) = 0;
~~~
 Commits a cross-document navigation in the frame. See WebNavigationParams
 for details. Calls WebLocalFrameClient::DidCommitNavigation synchronously
 after new document commit, but before loading any content, unless commit
 fails.

 TODO(dgozman): return mojom::CommitResult.

### CommitSameDocumentNavigation

CommitSameDocumentNavigation
~~~cpp
virtual mojom::CommitResult CommitSameDocumentNavigation(
      const WebURL&,
      WebFrameLoadType,
      const WebHistoryItem&,
      bool is_client_redirect,
      bool has_transient_user_activation,
      const WebSecurityOrigin& initiator_origin,
      bool is_browser_initiated,
      absl::optional<scheduler::TaskAttributionId>
          soft_navigation_heuristics_task_id) = 0;
~~~
 Commits a same-document navigation in the frame. For history navigations,
 a valid WebHistoryItem should be provided. `initiator_origin` is null
 for browser-initiated navigations. Returns CommitResult::Ok if the
 navigation has actually committed.

### SetIsNotOnInitialEmptyDocument

SetIsNotOnInitialEmptyDocument
~~~cpp
virtual void SetIsNotOnInitialEmptyDocument() = 0;
~~~
 Override the normal rules that determine whether the frame is on the
 initial empty document or not. Used to propagate state when this frame has
 navigated cross process.

### IsOnInitialEmptyDocument

IsOnInitialEmptyDocument
~~~cpp
virtual bool IsOnInitialEmptyDocument() = 0;
~~~

### WillPotentiallyStartOutermostMainFrameNavigation

WillPotentiallyStartOutermostMainFrameNavigation
~~~cpp
virtual void WillPotentiallyStartOutermostMainFrameNavigation(
      const WebURL&) const = 0;
~~~
 Notifies that a renderer-initiated navigation to `url` will
 potentially start soon. This is fired before the beforeunload event
 (if it's needed) gets dispatched in the renderer, so that the
 browser can speculatively start service worker before processing
 beforeunload event, which might take a long time. Note that the
 navigation might not actually start, e.g. if it gets canceled by
 beforeunload.

### WillStartNavigation

WillStartNavigation
~~~cpp
virtual bool WillStartNavigation(const WebNavigationInfo&) = 0;
~~~
 Marks the frame as loading, before WebLocalFrameClient issues a navigation
 request through the browser process on behalf of the frame.

 This runs some JavaScript event listeners, which may cancel the navigation
 or detach the frame. In this case the method returns false and client
 should not proceed with the navigation.

### DidDropNavigation

DidDropNavigation
~~~cpp
virtual void DidDropNavigation() = 0;
~~~
 Informs the frame that the navigation it asked the client to do was
 dropped.

### WebNavigationControl

WebNavigationControl
~~~cpp
explicit WebNavigationControl(mojom::TreeScopeType scope,
                                const LocalFrameToken& frame_token)
      : WebLocalFrame(scope, frame_token) {}
~~~
