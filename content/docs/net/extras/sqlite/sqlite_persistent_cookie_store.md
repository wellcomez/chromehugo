### SQLitePersistentCookieStore

SQLitePersistentCookieStore
~~~cpp
SQLitePersistentCookieStore(
      const base::FilePath& path,
      const scoped_refptr<base::SequencedTaskRunner>& client_task_runner,
      const scoped_refptr<base::SequencedTaskRunner>& background_task_runner,
      bool restore_old_session_cookies,
      CookieCryptoDelegate* crypto_delegate)
~~~

### SQLitePersistentCookieStore

SQLitePersistentCookieStore
~~~cpp
SQLitePersistentCookieStore(const SQLitePersistentCookieStore&) = delete;
~~~

### operator=

operator=
~~~cpp
SQLitePersistentCookieStore& operator=(const SQLitePersistentCookieStore&) =
      delete;
~~~

### DeleteAllInList

DeleteAllInList
~~~cpp
void DeleteAllInList(const std::list<CookieOrigin>& cookies);
~~~
 Deletes the cookies whose origins match those given in |cookies|.

### Load

Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~
 CookieMonster::PersistentCookieStore:
### LoadCookiesForKey

LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback callback) override;
~~~

### AddCookie

AddCookie
~~~cpp
void AddCookie(const CanonicalCookie& cc) override;
~~~

### UpdateCookieAccessTime

UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie& cc) override;
~~~

### DeleteCookie

DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie& cc) override;
~~~

### SetForceKeepSessionState

SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### GetQueueLengthForTesting

GetQueueLengthForTesting
~~~cpp
size_t GetQueueLengthForTesting();
~~~
 Returns how many operations are currently queued. For test use only;
 and the background thread needs to be wedged for accessing this to be
 non-racey. Also requires the client thread to be current.

### CompleteLoad

CompleteLoad
~~~cpp
void CompleteLoad(LoadedCallback callback,
                    std::vector<std::unique_ptr<CanonicalCookie>> cookie_list);
~~~

### CompleteKeyedLoad

CompleteKeyedLoad
~~~cpp
void CompleteKeyedLoad(
      const std::string& key,
      LoadedCallback callback,
      std::vector<std::unique_ptr<CanonicalCookie>> cookie_list);
~~~

