### ShouldEncrypt

ShouldEncrypt
~~~cpp
virtual bool ShouldEncrypt() = 0;
~~~
 Return if cookies should be encrypted on this platform.  Decryption of
 previously encrypted cookies is always possible.

### EncryptString

EncryptString
~~~cpp
virtual bool EncryptString(const std::string& plaintext,
                             std::string* ciphertext) = 0;
~~~
 Encrypt |plaintext| string and store the result in |ciphertext|.  This
 method is always functional even if ShouldEncrypt() is false.

### DecryptString

DecryptString
~~~cpp
virtual bool DecryptString(const std::string& ciphertext,
                             std::string* plaintext) = 0;
~~~
 Decrypt |ciphertext| string and store the result in |plaintext|.  This
 method is always functional even if ShouldEncrypt() is false.

