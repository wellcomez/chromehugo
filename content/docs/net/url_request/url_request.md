
## class URLRequest : public::Delegate
 The delegate's methods are called from the message loop of the thread
 on which the request's Start() method is called. See above for the
 ordering of callbacks.


 The callbacks will be called in the following order:
   Start()
    - OnConnected* (zero or more calls, see method comment)
    - OnCertificateRequested* (zero or more calls, if the SSL server and/or
      SSL proxy requests a client certificate for authentication)
    - OnSSLCertificateError* (zero or one call, if the SSL server's
      certificate has an error)
    - OnReceivedRedirect* (zero or more calls, for the number of redirects)
    - OnAuthRequired* (zero or more calls, for the number of
      authentication failures)
    - OnResponseStarted
   Read() initiated by delegate
    - OnReadCompleted* (zero or more calls until all data is read)

 Read() must be called at least once. Read() returns bytes read when it
 completes immediately, and a negative error value if an IO is pending or if
 there is an error.

### OnReceivedRedirect

Delegate::URLRequest : public::OnReceivedRedirect
~~~cpp
virtual void OnReceivedRedirect(URLRequest* request,
                                    const RedirectInfo& redirect_info,
                                    bool* defer_redirect);
~~~
 Called upon receiving a redirect.  The delegate may call the request's
 Cancel method to prevent the redirect from being followed.  Since there
 may be multiple chained redirects, there may also be more than one
 redirect call.


 When this function is called, the request will still contain the
 original URL, the destination of the redirect is provided in
 |redirect_info.new_url|.  If the delegate does not cancel the request
 and |*defer_redirect| is false, then the redirect will be followed, and
 the request's URL will be changed to the new URL.  Otherwise if the
 delegate does not cancel the request and |*defer_redirect| is true, then
 the redirect will be followed once FollowDeferredRedirect is called
 on the URLRequest.


 The caller must set |*defer_redirect| to false, so that delegates do not
 need to set it if they are happy with the default behavior of not
 deferring redirect.

### OnAuthRequired

Delegate::URLRequest : public::OnAuthRequired
~~~cpp
virtual void OnAuthRequired(URLRequest* request,
                                const AuthChallengeInfo& auth_info);
~~~
 Called when we receive an authentication failure.  The delegate should
 call request->SetAuth() with the user's credentials once it obtains them,
 or request->CancelAuth() to cancel the login and display the error page.

 When it does so, the request will be reissued, restarting the sequence
 of On* callbacks.

### OnCertificateRequested

Delegate::URLRequest : public::OnCertificateRequested
~~~cpp
virtual void OnCertificateRequested(URLRequest* request,
                                        SSLCertRequestInfo* cert_request_info);
~~~
 Called when we receive an SSL CertificateRequest message for client
 authentication.  The delegate should call
 request->ContinueWithCertificate() with the client certificate the user
 selected and its private key, or request->ContinueWithCertificate(NULL,
 NULL)
 to continue the SSL handshake without a client certificate.

### OnSSLCertificateError

Delegate::URLRequest : public::OnSSLCertificateError
~~~cpp
virtual void OnSSLCertificateError(URLRequest* request,
                                       int net_error,
                                       const SSLInfo& ssl_info,
                                       bool fatal);
~~~
 Called when using SSL and the server responds with a certificate with
 an error, for example, whose common name does not match the common name
 we were expecting for that host.  The delegate should either do the
 safe thing and Cancel() the request or decide to proceed by calling
 ContinueDespiteLastError().  cert_error is a ERR_* error code
 indicating what's wrong with the certificate.

 If |fatal| is true then the host in question demands a higher level
 of security (due e.g. to HTTP Strict Transport Security, user
 preference, or built-in policy). In this case, errors must not be
 bypassable by the user.

### OnResponseStarted

Delegate::URLRequest : public::OnResponseStarted
~~~cpp
virtual void OnResponseStarted(URLRequest* request, int net_error);
~~~
 After calling Start(), the delegate will receive an OnResponseStarted
 callback when the request has completed. |net_error| will be set to OK
 or an actual net error.  On success, all redirects have been
 followed and the final response is beginning to arrive.  At this point,
 meta data about the response is available, including for example HTTP
 response headers if this is a request for a HTTP resource.

### OnReadCompleted

Delegate::URLRequest : public::OnReadCompleted
~~~cpp
virtual void OnReadCompleted(URLRequest* request, int bytes_read) = 0;
~~~
 Called when the a Read of the response body is completed after an
 IO_PENDING status from a Read() call.

 The data read is filled into the buffer which the caller passed
 to Read() previously.


 If an error occurred, |bytes_read| will be set to the error.

## class URLRequest : public
-----------------------------------------------------------------------------
 A class  representing the asynchronous load of a data stream from an URL.


 The lifetime of an instance of this class is completely controlled by the
 consumer, and the instance is not required to live on the heap or be
 allocated in any special way.  It is also valid to delete an URLRequest
 object during the handling of a callback to its delegate.  Of course, once
 the URLRequest is deleted, no further callbacks to its delegate will occur.


 NOTE: All usage of all instances of this class should be on the same thread.


### URLRequest

URLRequest : public::URLRequest
~~~cpp
URLRequest(base::PassKey<URLRequestContext> pass_key,
             const GURL& url,
             RequestPriority priority,
             Delegate* delegate,
             const URLRequestContext* context,
             NetworkTrafficAnnotationTag traffic_annotation,
             bool is_for_websockets,
             absl::optional<net::NetLogSource> net_log_source)
~~~

### URLRequest

URLRequest
~~~cpp
URLRequest(const URLRequest&) = delete;
~~~

### operator=

URLRequest : public::operator=
~~~cpp
URLRequest& operator=(const URLRequest&) = delete;
~~~

### ~URLRequest

URLRequest : public::~URLRequest
~~~cpp
~URLRequest() override
~~~

### SetDefaultCookiePolicyToBlock

URLRequest : public::SetDefaultCookiePolicyToBlock
~~~cpp
static void SetDefaultCookiePolicyToBlock();
~~~
 Changes the default cookie policy from allowing all cookies to blocking all
 cookies. Embedders that want to implement a more flexible policy should
 change the default to blocking all cookies, and provide a NetworkDelegate
 with the URLRequestContext that maintains the CookieStore.

 The cookie policy default has to be set before the first URLRequest is
 started. Once it was set to block all cookies, it cannot be changed back.

### original_url

original_url
~~~cpp
const GURL& original_url() const { return url_chain_.front(); }
~~~
 The original url is the url used to initialize the request, and it may
 differ from the url if the request was redirected.

### url_chain

url_chain
~~~cpp
const std::vector<GURL>& url_chain() const { return url_chain_; }
~~~
 The chain of urls traversed by this request.  If the request had no
 redirects, this vector will contain one element.

### url

url
~~~cpp
const GURL& url() const { return url_chain_.back(); }
~~~

### SetURLChain

URLRequest : public::SetURLChain
~~~cpp
void SetURLChain(const std::vector<GURL>& url_chain);
~~~
 Explicitly set the URL chain for this request.  This can be used to
 indicate a chain of redirects that happen at a layer above the network
 service; e.g. navigation redirects.


 Note, the last entry in the new `url_chain` will be ignored.  Instead
 the request will preserve its current URL.  This is done since the higher
 layer providing the explicit `url_chain` may not be aware of modifications
 to the request URL by throttles.


 This method should only be called on new requests that have a single
 entry in their existing `url_chain_`.

### site_for_cookies

site_for_cookies
~~~cpp
const SiteForCookies& site_for_cookies() const { return site_for_cookies_; }
~~~
 The URL that should be consulted for the third-party cookie blocking
 policy, as defined in Section 2.1.1 and 2.1.2 of
 https://tools.ietf.org/html/draft-ietf-httpbis-cookie-same-site.


 WARNING: This URL must only be used for the third-party cookie blocking
          policy. It MUST NEVER be used for any kind of SECURITY check.


          For example, if a top-level navigation is redirected, the
          first-party for cookies will be the URL of the first URL in the
          redirect chain throughout the whole redirect. If it was used for
          a security check, an attacker might try to get around this check
          by starting from some page that redirects to the
          host-to-be-attacked.


### set_site_for_cookies

URLRequest : public::set_site_for_cookies
~~~cpp
void set_site_for_cookies(const SiteForCookies& site_for_cookies);
~~~
 This method may only be called before Start().

### set_isolation_info

set_isolation_info
~~~cpp
void set_isolation_info(const IsolationInfo& isolation_info) {
    isolation_info_ = isolation_info;
    cookie_partition_key_ = CookiePartitionKey::FromNetworkIsolationKey(
        isolation_info.network_isolation_key());
  }
~~~
 Sets IsolationInfo for the request, which affects whether SameSite cookies
 are sent, what NetworkAnonymizationKey is used for cached resources, and
 how that behavior changes when following redirects. This may only be
 changed before Start() is called.


 TODO(https://crbug.com/1060631): This isn't actually used yet for SameSite
 cookies. Update consumers and fix that.

### set_isolation_info_from_network_anonymization_key

URLRequest : public::set_isolation_info_from_network_anonymization_key
~~~cpp
void set_isolation_info_from_network_anonymization_key(
      const NetworkAnonymizationKey& network_anonymization_key);
~~~
 This will convert the passed NetworkAnonymizationKey to an IsolationInfo.

 This IsolationInfo mmay be assigned an inaccurate frame origin because the
 NetworkAnonymizationKey might not contain all the information to populate
 it. Additionally the NetworkAnonymizationKey uses sites which will be
 converted to origins when set on the IsolationInfo. If using this method it
 is required to skip the cache and not use credentials. Before starting the
 request, it must have the LoadFlag LOAD_DISABLE_CACHE set, and must be set
 to not allow credentials, to ensure that the inaccurate frame origin has no
 impact. The request will DCHECK otherwise.

### isolation_info

isolation_info
~~~cpp
const IsolationInfo& isolation_info() const { return isolation_info_; }
~~~

### cookie_partition_key

cookie_partition_key
~~~cpp
const absl::optional<CookiePartitionKey>& cookie_partition_key() const {
    return cookie_partition_key_;
  }
~~~

### force_ignore_site_for_cookies

force_ignore_site_for_cookies
~~~cpp
bool force_ignore_site_for_cookies() const {
    return force_ignore_site_for_cookies_;
  }
~~~
 Indicate whether SameSite cookies should be attached even though the
 request is cross-site.

### set_force_ignore_site_for_cookies

set_force_ignore_site_for_cookies
~~~cpp
void set_force_ignore_site_for_cookies(bool attach) {
    force_ignore_site_for_cookies_ = attach;
  }
~~~

### force_ignore_top_frame_party_for_cookies

force_ignore_top_frame_party_for_cookies
~~~cpp
bool force_ignore_top_frame_party_for_cookies() const {
    return force_ignore_top_frame_party_for_cookies_;
  }
~~~
 Indicates whether the top frame party will be considered same-party to the
 request URL (regardless of what it is), for the purpose of SameParty
 cookies.

### set_force_ignore_top_frame_party_for_cookies

set_force_ignore_top_frame_party_for_cookies
~~~cpp
void set_force_ignore_top_frame_party_for_cookies(bool force) {
    force_ignore_top_frame_party_for_cookies_ = force;
  }
~~~

### force_main_frame_for_same_site_cookies

force_main_frame_for_same_site_cookies
~~~cpp
bool force_main_frame_for_same_site_cookies() const {
    return force_main_frame_for_same_site_cookies_;
  }
~~~
 Indicates if the request should be treated as a main frame navigation for
 SameSite cookie computations.  This flag overrides the IsolationInfo
 request type associated with fetches from a service worker context.

### set_force_main_frame_for_same_site_cookies

set_force_main_frame_for_same_site_cookies
~~~cpp
void set_force_main_frame_for_same_site_cookies(bool value) {
    force_main_frame_for_same_site_cookies_ = value;
  }
~~~

### cookie_setting_overrides

cookie_setting_overrides
~~~cpp
CookieSettingOverrides& cookie_setting_overrides() {
    return cookie_setting_overrides_;
  }
~~~
 Overrides pertaining to cookie settings for this particular request.

### cookie_setting_overrides

cookie_setting_overrides
~~~cpp
const CookieSettingOverrides& cookie_setting_overrides() const {
    return cookie_setting_overrides_;
  }
~~~

### first_party_url_policy

first_party_url_policy
~~~cpp
RedirectInfo::FirstPartyURLPolicy first_party_url_policy() const {
    return first_party_url_policy_;
  }
~~~
 The first-party URL policy to apply when updating the first party URL
 during redirects. The first-party URL policy may only be changed before
 Start() is called.

### set_first_party_url_policy

URLRequest : public::set_first_party_url_policy
~~~cpp
void set_first_party_url_policy(
      RedirectInfo::FirstPartyURLPolicy first_party_url_policy);
~~~

### initiator

initiator
~~~cpp
const absl::optional<url::Origin>& initiator() const { return initiator_; }
~~~
 The origin of the context which initiated the request. This is distinct
 from the "first party for cookies" discussed above in a number of ways:

 1. The request's initiator does not change during a redirect. If a form
    submission from `https://example.com/` redirects through a number of
    sites before landing on `https://not-example.com/`, the initiator for
    each of those requests will be `https://example.com/`.


 2. The request's initiator is the origin of the frame or worker which made
    the request, even for top-level navigations. That is, if
    `https://example.com/`'s form submission is made in the top-level frame,
    the first party for cookies would be the target URL's origin. The
    initiator remains `https://example.com/`.


 This value is used to perform the cross-origin check specified in Section
 4.3 of https://tools.ietf.org/html/draft-ietf-httpbis-cookie-same-site.


 Note: the initiator can be null for browser-initiated top level
 navigations. This is different from a unique Origin (e.g. in sandboxed
 iframes).

### set_initiator

URLRequest : public::set_initiator
~~~cpp
void set_initiator(const absl::optional<url::Origin>& initiator);
~~~
 This method may only be called before Start().

### method

method
~~~cpp
const std::string& method() const { return method_; }
~~~
 The request method.  "GET" is the default value. The request method may
 only be changed before Start() is called. Request methods are
 case-sensitive, so standard HTTP methods like GET or POST should be
 specified in uppercase.

### set_method

URLRequest : public::set_method
~~~cpp
void set_method(base::StringPiece method);
~~~

### reporting_upload_depth

reporting_upload_depth
~~~cpp
int reporting_upload_depth() const { return reporting_upload_depth_; }
~~~
 Reporting upload nesting depth of this request.


 If the request is not a Reporting upload, the depth is 0.


 If the request is a Reporting upload, the depth is the max of the depth
 of the requests reported within it plus 1. (Non-NEL reports are
 considered to have depth 0.)
### set_reporting_upload_depth

URLRequest : public::set_reporting_upload_depth
~~~cpp
void set_reporting_upload_depth(int reporting_upload_depth);
~~~

### referrer

referrer
~~~cpp
const std::string& referrer() const { return referrer_; }
~~~
 The referrer URL for the request
### SetReferrer

URLRequest : public::SetReferrer
~~~cpp
void SetReferrer(base::StringPiece referrer);
~~~
 Sets the referrer URL for the request. Can only be changed before Start()
 is called. |referrer| is sanitized to remove URL fragment, user name and
 password. If a referrer policy is set via set_referrer_policy(), then
 |referrer| should obey the policy; if it doesn't, it will be cleared when
 the request is started. The referrer URL may be suppressed or changed
 during the course of the request, for example because of a referrer policy
 set with set_referrer_policy().

### referrer_policy

referrer_policy
~~~cpp
ReferrerPolicy referrer_policy() const { return referrer_policy_; }
~~~
 The referrer policy to apply when updating the referrer during redirects.

 The referrer policy may only be changed before Start() is called. Any
 referrer set via SetReferrer() is expected to obey the policy set via
 set_referrer_policy(); otherwise the referrer will be cleared when the
 request is started.

### set_referrer_policy

URLRequest : public::set_referrer_policy
~~~cpp
void set_referrer_policy(ReferrerPolicy referrer_policy);
~~~

### set_allow_credentials

URLRequest : public::set_allow_credentials
~~~cpp
void set_allow_credentials(bool allow_credentials);
~~~
 Sets whether credentials are allowed.

 If credentials are allowed, the request will send and save HTTP
 cookies, as well as authentication to the origin server. If not,
 they will not be sent, however proxy-level authentication will
 still occur. Setting this will force the LOAD_DO_NOT_SAVE_COOKIES field to
 be set in |load_flags_|. See https://crbug.com/799935.

### allow_credentials

allow_credentials
~~~cpp
bool allow_credentials() const { return allow_credentials_; }
~~~

### set_upload

URLRequest : public::set_upload
~~~cpp
void set_upload(std::unique_ptr<UploadDataStream> upload);
~~~
 Sets the upload data.

### get_upload_for_testing

URLRequest : public::get_upload_for_testing
~~~cpp
const UploadDataStream* get_upload_for_testing() const;
~~~
 Gets the upload data.

### has_upload

URLRequest : public::has_upload
~~~cpp
bool has_upload() const;
~~~
 Returns true if the request has a non-empty message body to upload.

### SetExtraRequestHeaderByName

URLRequest : public::SetExtraRequestHeaderByName
~~~cpp
void SetExtraRequestHeaderByName(base::StringPiece name,
                                   base::StringPiece value,
                                   bool overwrite);
~~~
 Set or remove a extra request header.  These methods may only be called
 before Start() is called, or between receiving a redirect and trying to
 follow it.

### RemoveRequestHeaderByName

URLRequest : public::RemoveRequestHeaderByName
~~~cpp
void RemoveRequestHeaderByName(base::StringPiece name);
~~~

### SetExtraRequestHeaders

URLRequest : public::SetExtraRequestHeaders
~~~cpp
void SetExtraRequestHeaders(const HttpRequestHeaders& headers);
~~~
 Sets all extra request headers.  Any extra request headers set by other
 methods are overwritten by this method.  This method may only be called
 before Start() is called.  It is an error to call it later.

### extra_request_headers

extra_request_headers
~~~cpp
const HttpRequestHeaders& extra_request_headers() const {
    return extra_request_headers_;
  }
~~~

### GetTotalReceivedBytes

URLRequest : public::GetTotalReceivedBytes
~~~cpp
int64_t GetTotalReceivedBytes() const;
~~~
 Gets the total amount of data received from network after SSL decoding and
 proxy handling. Pertains only to the last URLRequestJob issued by this
 URLRequest, i.e. reset on redirects, but not reset when multiple roundtrips
 are used for range requests or auth.

### GetTotalSentBytes

URLRequest : public::GetTotalSentBytes
~~~cpp
int64_t GetTotalSentBytes() const;
~~~
 Gets the total amount of data sent over the network before SSL encoding and
 proxy handling. Pertains only to the last URLRequestJob issued by this
 URLRequest, i.e. reset on redirects, but not reset when multiple roundtrips
 are used for range requests or auth.

### GetRawBodyBytes

URLRequest : public::GetRawBodyBytes
~~~cpp
int64_t GetRawBodyBytes() const;
~~~
 The size of the response body before removing any content encodings.

 Does not include redirects or sub-requests issued at lower levels (range
 requests or auth). Only includes bytes which have been read so far,
 including bytes from the cache.

### GetLoadState

URLRequest : public::GetLoadState
~~~cpp
LoadStateWithParam GetLoadState() const;
~~~
 Returns the current load state for the request. The returned value's
 |param| field is an optional parameter describing details related to the
 load state. Not all load states have a parameter.

### GetStateAsValue

URLRequest : public::GetStateAsValue
~~~cpp
base::Value::Dict GetStateAsValue() const;
~~~
 Returns a partial representation of the request's state as a value, for
 debugging.

### LogBlockedBy

URLRequest : public::LogBlockedBy
~~~cpp
void LogBlockedBy(base::StringPiece blocked_by);
~~~
 Logs information about the what external object currently blocking the
 request.  LogUnblocked must be called before resuming the request.  This
 can be called multiple times in a row either with or without calling
 LogUnblocked between calls.  |blocked_by| must not be empty.

### LogAndReportBlockedBy

URLRequest : public::LogAndReportBlockedBy
~~~cpp
void LogAndReportBlockedBy(base::StringPiece blocked_by);
~~~
 Just like LogBlockedBy, but also makes GetLoadState return source as the
 |param| in the value returned by GetLoadState.  Calling LogUnblocked or
 LogBlockedBy will clear the load param.  |blocked_by| must not be empty.

### LogUnblocked

URLRequest : public::LogUnblocked
~~~cpp
void LogUnblocked();
~~~
 Logs that the request is no longer blocked by the last caller to
 LogBlockedBy.

### GetUploadProgress

URLRequest : public::GetUploadProgress
~~~cpp
UploadProgress GetUploadProgress() const;
~~~
 Returns the current upload progress in bytes. When the upload data is
 chunked, size is set to zero, but position will not be.

### GetResponseHeaderByName

URLRequest : public::GetResponseHeaderByName
~~~cpp
void GetResponseHeaderByName(base::StringPiece name,
                               std::string* value) const;
~~~
 Get response header(s) by name.  This method may only be called
 once the delegate's OnResponseStarted method has been called.  Headers
 that appear more than once in the response are coalesced, with values
 separated by commas (per RFC 2616). This will not work with cookies since
 comma can be used in cookie values.

### creation_time

creation_time
~~~cpp
base::TimeTicks creation_time() const { return creation_time_; }
~~~
 The time when |this| was constructed.

### request_time

request_time
~~~cpp
const base::Time& request_time() const { return response_info_.request_time; }
~~~
 The time at which the returned response was requested.  For cached
 responses, this is the last time the cache entry was validated.

### response_time

response_time
~~~cpp
const base::Time& response_time() const {
    return response_info_.response_time;
  }
~~~
 The time at which the returned response was generated.  For cached
 responses, this is the last time the cache entry was validated.

### was_cached

was_cached
~~~cpp
bool was_cached() const { return response_info_.was_cached; }
~~~
 Indicate if this response was fetched from disk cache.

### was_fetched_via_spdy

was_fetched_via_spdy
~~~cpp
bool was_fetched_via_spdy() const {
    return response_info_.was_fetched_via_spdy;
  }
~~~
 Returns true if the URLRequest was delivered over SPDY.

### GetResponseRemoteEndpoint

URLRequest : public::GetResponseRemoteEndpoint
~~~cpp
IPEndPoint GetResponseRemoteEndpoint() const;
~~~
 Returns the host and port that the content was fetched from.  See
 http_response_info.h for caveats relating to cached content.

### response_headers

URLRequest : public::response_headers
~~~cpp
HttpResponseHeaders* response_headers() const;
~~~
 Get all response headers, as a HttpResponseHeaders object.  See comments
 in HttpResponseHeaders class as to the format of the data.

### ssl_info

ssl_info
~~~cpp
const SSLInfo& ssl_info() const { return response_info_.ssl_info; }
~~~
 Get the SSL connection info.

### auth_challenge_info

URLRequest : public::auth_challenge_info
~~~cpp
const absl::optional<AuthChallengeInfo>& auth_challenge_info() const;
~~~

### GetLoadTimingInfo

URLRequest : public::GetLoadTimingInfo
~~~cpp
void GetLoadTimingInfo(LoadTimingInfo* load_timing_info) const;
~~~
 Gets timing information related to the request.  Events that have not yet
 occurred are left uninitialized.  After a second request starts, due to
 a redirect or authentication, values will be reset.


 LoadTimingInfo only contains ConnectTiming information and socket IDs for
 non-cached HTTP responses.

### PopulateNetErrorDetails

URLRequest : public::PopulateNetErrorDetails
~~~cpp
void PopulateNetErrorDetails(NetErrorDetails* details) const;
~~~
 Gets the networkd error details of the most recent origin that the network
 stack makes the request to.

### GetTransactionRemoteEndpoint

URLRequest : public::GetTransactionRemoteEndpoint
~~~cpp
bool GetTransactionRemoteEndpoint(IPEndPoint* endpoint) const;
~~~
 Gets the remote endpoint of the most recent socket that the network stack
 used to make this request.


 Note that GetResponseRemoteEndpoint returns the |socket_address| field from
 HttpResponseInfo, which is only populated once the response headers are
 received, and can return cached values for cache revalidation requests.

 GetTransactionRemoteEndpoint will only return addresses from the current
 request.


 Returns true and fills in |endpoint| if the endpoint is available; returns
 false and leaves |endpoint| unchanged if it is unavailable.

### GetMimeType

URLRequest : public::GetMimeType
~~~cpp
void GetMimeType(std::string* mime_type) const;
~~~
 Get the mime type.  This method may only be called once the delegate's
 OnResponseStarted method has been called.

### GetCharset

URLRequest : public::GetCharset
~~~cpp
void GetCharset(std::string* charset) const;
~~~
 Get the charset (character encoding).  This method may only be called once
 the delegate's OnResponseStarted method has been called.

### GetResponseCode

URLRequest : public::GetResponseCode
~~~cpp
int GetResponseCode() const;
~~~
 Returns the HTTP response code (e.g., 200, 404, and so on).  This method
 may only be called once the delegate's OnResponseStarted method has been
 called.  For non-HTTP requests, this method returns -1.

### response_info

response_info
~~~cpp
const HttpResponseInfo& response_info() const { return response_info_; }
~~~
 Get the HTTP response info in its entirety.

### load_flags

load_flags
~~~cpp
int load_flags() const { return load_flags_; }
~~~
 Access the LOAD_* flags modifying this request (see load_flags.h).

### is_created_from_network_anonymization_key

is_created_from_network_anonymization_key
~~~cpp
bool is_created_from_network_anonymization_key() const {
    return is_created_from_network_anonymization_key_;
  }
~~~

### secure_dns_policy

secure_dns_policy
~~~cpp
SecureDnsPolicy secure_dns_policy() const { return secure_dns_policy_; }
~~~
 Returns the Secure DNS Policy for the request.

### set_maybe_sent_cookies

URLRequest : public::set_maybe_sent_cookies
~~~cpp
void set_maybe_sent_cookies(CookieAccessResultList cookies);
~~~

### set_maybe_stored_cookies

URLRequest : public::set_maybe_stored_cookies
~~~cpp
void set_maybe_stored_cookies(CookieAndLineAccessResultList cookies);
~~~

### maybe_sent_cookies

maybe_sent_cookies
~~~cpp
const CookieAccessResultList& maybe_sent_cookies() const {
    return maybe_sent_cookies_;
  }
~~~
 These lists contain a list of cookies that are associated with the given
 request, both those that were sent and accepted, and those that were
 removed or flagged from the request before use. The status indicates
 whether they were actually used (INCLUDE), or the reason they were removed
 or flagged. They are cleared on redirects and other request restarts that
 cause sent cookies to be recomputed / new cookies to potentially be
 received (such as calling SetAuth() to send HTTP auth credentials, but not
 calling ContinueWithCertification() to respond to client cert challenges),
 and only contain the cookies relevant to the most recent roundtrip.

 Populated while the http request is being built.

### maybe_stored_cookies

maybe_stored_cookies
~~~cpp
const CookieAndLineAccessResultList& maybe_stored_cookies() const {
    return maybe_stored_cookies_;
  }
~~~
 Populated after the response headers are received.

### SetLoadFlags

URLRequest : public::SetLoadFlags
~~~cpp
void SetLoadFlags(int flags);
~~~
 The new flags may change the IGNORE_LIMITS flag only when called
 before Start() is called, it must only set the flag, and if set,
 the priority of this request must already be MAXIMUM_PRIORITY.

### SetSecureDnsPolicy

URLRequest : public::SetSecureDnsPolicy
~~~cpp
void SetSecureDnsPolicy(SecureDnsPolicy secure_dns_policy);
~~~
 Controls the Secure DNS behavior to use when creating the socket for this
 request.

### is_pending

is_pending
~~~cpp
bool is_pending() const { return is_pending_; }
~~~
 Returns true if the request is "pending" (i.e., if Start() has been called,
 and the response has not yet been called).

### is_redirecting

is_redirecting
~~~cpp
bool is_redirecting() const { return is_redirecting_; }
~~~
 Returns true if the request is in the process of redirecting to a new
 URL but has not yet initiated the new request.

### Start

URLRequest : public::Start
~~~cpp
void Start();
~~~
 This method is called to start the request.  The delegate will receive
 a OnResponseStarted callback when the request is started.  The request
 must have a delegate set before this method is called.

### Cancel

URLRequest : public::Cancel
~~~cpp
int Cancel();
~~~
 This method may be called at any time after Start() has been called to
 cancel the request.  This method may be called many times, and it has
 no effect once the response has completed.  It is guaranteed that no
 methods of the delegate will be called after the request has been
 cancelled, except that this may call the delegate's OnReadCompleted()
 during the call to Cancel itself. Returns |ERR_ABORTED| or other net error
 if there was one.

### CancelWithError

URLRequest : public::CancelWithError
~~~cpp
int CancelWithError(int error);
~~~
 Cancels the request and sets the error to |error|, unless the request
 already failed with another error code (see net_error_list.h). Returns
 final network error code.

### CancelWithSSLError

URLRequest : public::CancelWithSSLError
~~~cpp
void CancelWithSSLError(int error, const SSLInfo& ssl_info);
~~~
 Cancels the request and sets the error to |error| (see net_error_list.h
 for values) and attaches |ssl_info| as the SSLInfo for that request.  This
 is useful to attach a certificate and certificate error to a canceled
 request.

### Read

URLRequest : public::Read
~~~cpp
int Read(IOBuffer* buf, int max_bytes);
~~~
 Read initiates an asynchronous read from the response, and must only be
 called after the OnResponseStarted callback is received with a net::OK. If
 data is available, length and the data will be returned immediately. If the
 request has failed, an error code will be returned. If data is not yet
 available, Read returns net::ERR_IO_PENDING, and the Delegate's
 OnReadComplete method will be called asynchronously with the result of the
 read, unless the URLRequest is canceled.


 The |buf| parameter is a buffer to receive the data. If the operation
 completes asynchronously, the implementation will reference the buffer
 until OnReadComplete is called. The buffer must be at least |max_bytes| in
 length.


 The |max_bytes| parameter is the maximum number of bytes to read.

### FollowDeferredRedirect

URLRequest : public::FollowDeferredRedirect
~~~cpp
void FollowDeferredRedirect(
      const absl::optional<std::vector<std::string>>& removed_headers,
      const absl::optional<net::HttpRequestHeaders>& modified_headers);
~~~
 This method may be called to follow a redirect that was deferred in
 response to an OnReceivedRedirect call. If non-null,
 |modified_headers| are changes applied to the request headers after
 updating them for the redirect.

### SetAuth

URLRequest : public::SetAuth
~~~cpp
void SetAuth(const AuthCredentials& credentials);
~~~
 One of the following two methods should be called in response to an
 OnAuthRequired() callback (and only then).

 SetAuth will reissue the request with the given credentials.

 CancelAuth will give up and display the error page.

### CancelAuth

URLRequest : public::CancelAuth
~~~cpp
void CancelAuth();
~~~

### ContinueWithCertificate

URLRequest : public::ContinueWithCertificate
~~~cpp
void ContinueWithCertificate(scoped_refptr<X509Certificate> client_cert,
                               scoped_refptr<SSLPrivateKey> client_private_key);
~~~
 This method can be called after the user selects a client certificate to
 instruct this URLRequest to continue with the request with the
 certificate.  Pass NULL if the user doesn't have a client certificate.

### ContinueDespiteLastError

URLRequest : public::ContinueDespiteLastError
~~~cpp
void ContinueDespiteLastError();
~~~
 This method can be called after some error notifications to instruct this
 URLRequest to ignore the current error and continue with the request.  To
 cancel the request instead, call Cancel().

### AbortAndCloseConnection

URLRequest : public::AbortAndCloseConnection
~~~cpp
void AbortAndCloseConnection();
~~~
 Aborts the request (without invoking any completion callbacks) and closes
 the current connection, rather than returning it to the socket pool. Only
 affects HTTP/1.1 connections and tunnels.


 Intended to be used in cases where socket reuse can potentially leak data
 across sites.


 May only be called after Delegate::OnResponseStarted() has been invoked
 with net::OK, but before the body has been completely read. After the last
 body has been read, the socket may have already been handed off to another
 consumer.


 Due to transactions potentially being shared by multiple URLRequests in
 some cases, it is possible the socket may not be immediately closed, but
 will instead be closed when all URLRequests sharing the socket have been
 destroyed.

### context

URLRequest : public::context
~~~cpp
const URLRequestContext* context() const;
~~~
 Used to specify the context (cookie store, cache) for this request.

### network_delegate

URLRequest : public::network_delegate
~~~cpp
NetworkDelegate* network_delegate() const;
~~~
 Returns context()->network_delegate().

### net_log

net_log
~~~cpp
const NetLogWithSource& net_log() const { return net_log_; }
~~~

### GetExpectedContentSize

URLRequest : public::GetExpectedContentSize
~~~cpp
int64_t GetExpectedContentSize() const;
~~~
 Returns the expected content size if available
### priority

priority
~~~cpp
RequestPriority priority() const { return priority_; }
~~~
 Returns the priority level for this request.

### priority_incremental

priority_incremental
~~~cpp
bool priority_incremental() const { return priority_incremental_; }
~~~
 Returns the incremental loading priority flag for this request.

### SetPriority

URLRequest : public::SetPriority
~~~cpp
void SetPriority(RequestPriority priority);
~~~
 Sets the priority level for this request and any related
 jobs. Must not change the priority to anything other than
 MAXIMUM_PRIORITY if the IGNORE_LIMITS load flag is set.

### SetPriorityIncremental

URLRequest : public::SetPriorityIncremental
~~~cpp
void SetPriorityIncremental(bool priority_incremental);
~~~
 Sets the incremental priority flag for this request.

### set_received_response_content_length

set_received_response_content_length
~~~cpp
void set_received_response_content_length(int64_t received_content_length) {
    received_response_content_length_ = received_content_length;
  }
~~~

### received_response_content_length

received_response_content_length
~~~cpp
int64_t received_response_content_length() const {
    return received_response_content_length_;
  }
~~~
 The number of bytes in the raw response body (before any decompression,
 etc.). This is only available after the final Read completes.

### proxy_server

proxy_server
~~~cpp
const ProxyServer& proxy_server() const { return proxy_server_; }
~~~
 Available when the request headers are sent, which is before the more
 general response_info() is available.

### GetConnectionAttempts

URLRequest : public::GetConnectionAttempts
~~~cpp
ConnectionAttempts GetConnectionAttempts() const;
~~~
 Gets the connection attempts made in the process of servicing this
 URLRequest. Only guaranteed to be valid if called after the request fails
 or after the response headers are received.

### traffic_annotation

traffic_annotation
~~~cpp
const NetworkTrafficAnnotationTag& traffic_annotation() const {
    return traffic_annotation_;
  }
~~~

### accepted_stream_types

accepted_stream_types
~~~cpp
const absl::optional<base::flat_set<net::SourceStream::SourceType>>&
  accepted_stream_types() const {
    return accepted_stream_types_;
  }
~~~

### set_accepted_stream_types

set_accepted_stream_types
~~~cpp
void set_accepted_stream_types(
      const absl::optional<base::flat_set<net::SourceStream::SourceType>>&
          types) {
    if (types) {
      DCHECK(!types->contains(net::SourceStream::SourceType::TYPE_NONE));
      DCHECK(!types->contains(net::SourceStream::SourceType::TYPE_UNKNOWN));
    }
    accepted_stream_types_ = types;
  }
~~~

### SetRequestHeadersCallback

URLRequest : public::SetRequestHeadersCallback
~~~cpp
void SetRequestHeadersCallback(RequestHeadersCallback callback);
~~~
 Sets a callback that will be invoked each time the request is about to
 be actually sent and will receive actual request headers that are about
 to hit the wire, including SPDY/QUIC internal headers.


 Can only be set once before the request is started.

### SetResponseHeadersCallback

URLRequest : public::SetResponseHeadersCallback
~~~cpp
void SetResponseHeadersCallback(ResponseHeadersCallback callback);
~~~
 Sets a callback that will be invoked each time the response is received
 from the remote party with the actual response headers received. Note this
 is different from response_headers() getter in that in case of revalidation
 request, the latter will return cached headers, while the callback will be
 called with a response from the server.

### SetEarlyResponseHeadersCallback

URLRequest : public::SetEarlyResponseHeadersCallback
~~~cpp
void SetEarlyResponseHeadersCallback(ResponseHeadersCallback callback);
~~~
 Sets a callback that will be invoked each time a 103 Early Hints response
 is received from the remote party.

### set_socket_tag

URLRequest : public::set_socket_tag
~~~cpp
void set_socket_tag(const SocketTag& socket_tag);
~~~
 Sets socket tag to be applied to all sockets used to execute this request.

 Must be set before Start() is called.  Only currently supported for HTTP
 and HTTPS requests on Android; UID tagging requires
 MODIFY_NETWORK_ACCOUNTING permission.

 NOTE(pauljensen): Setting a tag disallows sharing of sockets with requests
 with other tags, which may adversely effect performance by prohibiting
 connection sharing. In other words use of multiplexed sockets (e.g. HTTP/2
 and QUIC) will only be allowed if all requests have the same socket tag.

### socket_tag

socket_tag
~~~cpp
const SocketTag& socket_tag() const { return socket_tag_; }
~~~

### set_upgrade_if_insecure

set_upgrade_if_insecure
~~~cpp
void set_upgrade_if_insecure(bool upgrade_if_insecure) {
    upgrade_if_insecure_ = upgrade_if_insecure;
  }
~~~
 |upgrade_if_insecure| should be set to true if this request (including
 redirects) should be upgraded to HTTPS due to an Upgrade-Insecure-Requests
 requirement.

### upgrade_if_insecure

upgrade_if_insecure
~~~cpp
bool upgrade_if_insecure() const { return upgrade_if_insecure_; }
~~~

### set_send_client_certs

set_send_client_certs
~~~cpp
void set_send_client_certs(bool send_client_certs) {
    send_client_certs_ = send_client_certs;
  }
~~~
 By default, client certs will be sent (provided via
 Delegate::OnCertificateRequested) when cookies are disabled
 (LOAD_DO_NOT_SEND_COOKIES / LOAD_DO_NOT_SAVE_COOKIES). As described at
 https://crbug.com/775438, this is not the desired behavior. When
 |send_client_certs| is set to false, this will suppress the
 Delegate::OnCertificateRequested callback when cookies/credentials are also
 suppressed. This method has no effect if credentials are enabled (cookies
 saved and sent).

 TODO(https://crbug.com/775438): Remove this when the underlying
 issue is fixed.

### send_client_certs

send_client_certs
~~~cpp
bool send_client_certs() const { return send_client_certs_; }
~~~

### is_for_websockets

is_for_websockets
~~~cpp
bool is_for_websockets() const { return is_for_websockets_; }
~~~

### SetIdempotency

SetIdempotency
~~~cpp
void SetIdempotency(Idempotency idempotency) { idempotency_ = idempotency; }
~~~

### GetIdempotency

GetIdempotency
~~~cpp
Idempotency GetIdempotency() const { return idempotency_; }
~~~

### pervasive_payloads_index_for_logging

pervasive_payloads_index_for_logging
~~~cpp
int pervasive_payloads_index_for_logging() const {
    return pervasive_payloads_index_for_logging_;
  }
~~~

### set_pervasive_payloads_index_for_logging

set_pervasive_payloads_index_for_logging
~~~cpp
void set_pervasive_payloads_index_for_logging(int index) {
    pervasive_payloads_index_for_logging_ = index;
  }
~~~

### expected_response_checksum

expected_response_checksum
~~~cpp
const std::string& expected_response_checksum() const {
    return expected_response_checksum_;
  }
~~~

### set_expected_response_checksum

set_expected_response_checksum
~~~cpp
void set_expected_response_checksum(base::StringPiece checksum) {
    expected_response_checksum_ = std::string(checksum);
  }
~~~

### set_has_storage_access

set_has_storage_access
~~~cpp
void set_has_storage_access(bool has_storage_access) {
    DCHECK(!is_pending_);
    DCHECK(!has_notified_completion_);
    has_storage_access_ = has_storage_access;
  }
~~~

### has_storage_access

has_storage_access
~~~cpp
bool has_storage_access() const { return has_storage_access_; }
~~~

### DefaultCanUseCookies

URLRequest : public::DefaultCanUseCookies
~~~cpp
static bool DefaultCanUseCookies();
~~~

### GetWeakPtr

URLRequest : public::GetWeakPtr
~~~cpp
base::WeakPtr<URLRequest> GetWeakPtr();
~~~

### status

status
~~~cpp
int status() const { return status_; }
~~~
 Setter / getter for the status of the request. Status is represented as a
 net::Error code. See |status_|.

### set_status

URLRequest : public::set_status
~~~cpp
void set_status(int status);
~~~

### failed

URLRequest : public::failed
~~~cpp
bool failed() const;
~~~
 Returns true if the request failed or was cancelled.

### Redirect

URLRequest : public::Redirect
~~~cpp
void Redirect(
      const RedirectInfo& redirect_info,
      const absl::optional<std::vector<std::string>>& removed_headers,
      const absl::optional<net::HttpRequestHeaders>& modified_headers);
~~~
 Returns the error status of the request.

 Allow the URLRequestJob to redirect this request. If non-null,
 |removed_headers| and |modified_headers| are changes
 applied to the request headers after updating them for the redirect.

### NotifyReceivedRedirect

URLRequest : public::NotifyReceivedRedirect
~~~cpp
void NotifyReceivedRedirect(const RedirectInfo& redirect_info,
                              bool* defer_redirect);
~~~
 Called by URLRequestJob to allow interception when a redirect occurs.

### BeforeRequestComplete

URLRequest : public::BeforeRequestComplete
~~~cpp
void BeforeRequestComplete(int error);
~~~
 Resumes or blocks a request paused by the NetworkDelegate::OnBeforeRequest
 handler. If |blocked| is true, the request is blocked and an error page is
 returned indicating so. This should only be called after Start is called
 and OnBeforeRequest returns true (signalling that the request should be
 paused).

### StartJob

URLRequest : public::StartJob
~~~cpp
void StartJob(std::unique_ptr<URLRequestJob> job);
~~~

### RestartWithJob

URLRequest : public::RestartWithJob
~~~cpp
void RestartWithJob(std::unique_ptr<URLRequestJob> job);
~~~
 Restarting involves replacing the current job with a new one such as what
 happens when following a HTTP redirect.

### PrepareToRestart

URLRequest : public::PrepareToRestart
~~~cpp
void PrepareToRestart();
~~~

### DoCancel

URLRequest : public::DoCancel
~~~cpp
int DoCancel(int error, const SSLInfo& ssl_info);
~~~
 Cancels the request and set the error and ssl info for this request to the
 passed values. Returns the error that was set.

### OnHeadersComplete

URLRequest : public::OnHeadersComplete
~~~cpp
void OnHeadersComplete();
~~~
 Called by the URLRequestJob when the headers are received, before any other
 method, to allow caching of load timing information.

### NotifyRequestCompleted

URLRequest : public::NotifyRequestCompleted
~~~cpp
void NotifyRequestCompleted();
~~~
 Notifies the network delegate that the request has been completed.

 This does not imply a successful completion. Also a canceled request is
 considered completed.

### NotifyResponseStarted

URLRequest : public::NotifyResponseStarted
~~~cpp
void NotifyResponseStarted(int net_error);
~~~
 Called by URLRequestJob to allow interception when the final response
 occurs.

### NotifyConnected

URLRequest : public::NotifyConnected
~~~cpp
int NotifyConnected(const TransportInfo& info,
                      CompletionOnceCallback callback);
~~~
 These functions delegate to |delegate_|.  See URLRequest::Delegate for the
 meaning of these functions.

### NotifyAuthRequired

URLRequest : public::NotifyAuthRequired
~~~cpp
void NotifyAuthRequired(std::unique_ptr<AuthChallengeInfo> auth_info);
~~~

### NotifyCertificateRequested

URLRequest : public::NotifyCertificateRequested
~~~cpp
void NotifyCertificateRequested(SSLCertRequestInfo* cert_request_info);
~~~

### NotifySSLCertificateError

URLRequest : public::NotifySSLCertificateError
~~~cpp
void NotifySSLCertificateError(int net_error,
                                 const SSLInfo& ssl_info,
                                 bool fatal);
~~~

### NotifyReadCompleted

URLRequest : public::NotifyReadCompleted
~~~cpp
void NotifyReadCompleted(int bytes_read);
~~~

### CanSetCookie

URLRequest : public::CanSetCookie
~~~cpp
bool CanSetCookie(const net::CanonicalCookie& cookie,
                    CookieOptions* options) const;
~~~
 This function delegates to the NetworkDelegate if it is not nullptr.

 Otherwise, cookies can be used unless SetDefaultCookiePolicyToBlock() has
 been called.

### OnCallToDelegate

URLRequest : public::OnCallToDelegate
~~~cpp
void OnCallToDelegate(NetLogEventType type);
~~~
 Called just before calling a delegate that may block a request. |type|
 should be the delegate's event type,
 e.g. NetLogEventType::NETWORK_DELEGATE_AUTH_REQUIRED.

### OnCallToDelegateComplete

URLRequest : public::OnCallToDelegateComplete
~~~cpp
void OnCallToDelegateComplete(int error = OK);
~~~
 Called when the delegate lets a request continue.  Also called on
 cancellation. `error` is an optional error code associated with
 completion. It's only for logging purposes, and will not directly cancel
 the request if it's a value other than OK.

### RecordReferrerGranularityMetrics

URLRequest : public::RecordReferrerGranularityMetrics
~~~cpp
void RecordReferrerGranularityMetrics(bool request_is_same_origin) const;
~~~
 Records the referrer policy of the given request, bucketed by
 whether the request is same-origin or not. To save computation,
 takes this fact as a boolean parameter rather than dynamically
 checking.

### CreateIsolationInfoFromNetworkAnonymizationKey

URLRequest : public::CreateIsolationInfoFromNetworkAnonymizationKey
~~~cpp
net::IsolationInfo CreateIsolationInfoFromNetworkAnonymizationKey(
      const NetworkAnonymizationKey& network_anonymization_key);
~~~
 Creates a partial IsolationInfo with the information accessible from the
 NetworkAnonymiationKey.
