
## class CookiePartitionKeyCollection
 A data structure used to represent a collection of cookie partition keys.


 It can represent all possible cookie partition keys when
 `contains_all_keys_` is true.


 It can also represent a finite number of cookie partition keys, including
 zero.

### CookiePartitionKeyCollection

CookiePartitionKeyCollection::CookiePartitionKeyCollection
~~~cpp
CookiePartitionKeyCollection(const CookiePartitionKeyCollection& other)
~~~

### CookiePartitionKeyCollection ContainsAll

CookiePartitionKeyCollection ContainsAll
~~~cpp
explicit CookiePartitionKeyCollection(const CookiePartitionKey& key);
  // Creates a set that contains each partition key in the set.
  explicit CookiePartitionKeyCollection(
      base::flat_set<CookiePartitionKey> keys);

  CookiePartitionKeyCollection& operator=(
      const CookiePartitionKeyCollection& other);
  CookiePartitionKeyCollection& operator=(CookiePartitionKeyCollection&& other);
  ~CookiePartitionKeyCollection();

  static CookiePartitionKeyCollection ContainsAll() {
    return CookiePartitionKeyCollection(true);
  }
~~~
 Creates a key collection with a single element.

### FromOptional

FromOptional
~~~cpp
static CookiePartitionKeyCollection FromOptional(
      const absl::optional<CookiePartitionKey>& opt_key) {
    return opt_key ? CookiePartitionKeyCollection(opt_key.value())
                   : CookiePartitionKeyCollection();
  }
~~~

### Todo

Todo
~~~cpp
static CookiePartitionKeyCollection Todo() {
    return CookiePartitionKeyCollection();
  }
~~~
 Temporary method used to record where we need to decide how to build the
 CookiePartitionKeyCollection.


 Returns an empty key collection, so no partitioned cookies will be returned
 at callsites this is used.


 TODO(crbug.com/1225444): Remove this method and update callsites to use
 appropriate constructor.

### IsEmpty

IsEmpty
~~~cpp
bool IsEmpty() const { return !contains_all_keys_ && keys_.empty(); }
~~~
 CookieMonster can check if the key collection is empty to avoid searching
 the PartitionedCookieMap at all.

### ContainsAllKeys

ContainsAllKeys
~~~cpp
bool ContainsAllKeys() const { return contains_all_keys_; }
~~~
 Returns if the key collection contains every partition key.

### PartitionKeys

PartitionKeys
~~~cpp
const base::flat_set<CookiePartitionKey>& PartitionKeys() const {
    DCHECK(!contains_all_keys_);
    return keys_;
  }
~~~
 Iterate over all keys in the key collection, do not call this method if
 `contains_all_keys` is true.

### Contains

CookiePartitionKeyCollection::Contains
~~~cpp
bool Contains(const CookiePartitionKey& key) const;
~~~
 Returns true if the collection contains the passed key.
