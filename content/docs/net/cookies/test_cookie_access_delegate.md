
## class TestCookieAccessDelegate
 CookieAccessDelegate for testing. You can set the return value for a given
 cookie_domain (modulo any leading dot). Calling GetAccessSemantics() will
 then return the given value, or UNKNOWN if you haven't set one.

### TestCookieAccessDelegate

TestCookieAccessDelegate::TestCookieAccessDelegate
~~~cpp
TestCookieAccessDelegate();
~~~

### TestCookieAccessDelegate

TestCookieAccessDelegate
~~~cpp
TestCookieAccessDelegate(const TestCookieAccessDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
TestCookieAccessDelegate& operator=(const TestCookieAccessDelegate&) = delete;
~~~

### ~TestCookieAccessDelegate

TestCookieAccessDelegate::~TestCookieAccessDelegate
~~~cpp
~TestCookieAccessDelegate() override;
~~~

### GetAccessSemantics

TestCookieAccessDelegate::GetAccessSemantics
~~~cpp
CookieAccessSemantics GetAccessSemantics(
      const CanonicalCookie& cookie) const override;
~~~
 CookieAccessDelegate implementation:
### ShouldIgnoreSameSiteRestrictions

TestCookieAccessDelegate::ShouldIgnoreSameSiteRestrictions
~~~cpp
bool ShouldIgnoreSameSiteRestrictions(
      const GURL& url,
      const SiteForCookies& site_for_cookies) const override;
~~~

### ComputeFirstPartySetMetadataMaybeAsync

TestCookieAccessDelegate::ComputeFirstPartySetMetadataMaybeAsync
~~~cpp
absl::optional<FirstPartySetMetadata> ComputeFirstPartySetMetadataMaybeAsync(
      const SchemefulSite& site,
      const SchemefulSite* top_frame_site,
      const std::set<SchemefulSite>& party_context,
      base::OnceCallback<void(FirstPartySetMetadata)> callback) const override;
~~~

### FindFirstPartySetEntries

TestCookieAccessDelegate::FindFirstPartySetEntries
~~~cpp
absl::optional<base::flat_map<SchemefulSite, FirstPartySetEntry>>
  FindFirstPartySetEntries(
      const base::flat_set<SchemefulSite>& sites,
      base::OnceCallback<
          void(base::flat_map<SchemefulSite, FirstPartySetEntry>)> callback)
      const override;
~~~

### SetExpectationForCookieDomain

TestCookieAccessDelegate::SetExpectationForCookieDomain
~~~cpp
void SetExpectationForCookieDomain(const std::string& cookie_domain,
                                     CookieAccessSemantics access_semantics);
~~~
 Sets the expected return value for any cookie whose Domain
 matches |cookie_domain|. Pass the value of |cookie.Domain()| and any
 leading dot will be discarded.

### SetIgnoreSameSiteRestrictionsScheme

TestCookieAccessDelegate::SetIgnoreSameSiteRestrictionsScheme
~~~cpp
void SetIgnoreSameSiteRestrictionsScheme(
      const std::string& site_for_cookies_scheme,
      bool require_secure_origin);
~~~
 Sets the expected return value for ShouldAlwaysAttachSameSiteCookies.

 Can set schemes that always attach SameSite cookies, or schemes that always
 attach SameSite cookies if the request URL is secure.

### SetFirstPartySets

TestCookieAccessDelegate::SetFirstPartySets
~~~cpp
void SetFirstPartySets(
      const base::flat_map<SchemefulSite, FirstPartySetEntry>& sets);
~~~
 Set the test delegate's First-Party Sets. The map's keys are the sites in
 the sets. Primary sites must be included among the keys for a given set.

### set_invoke_callbacks_asynchronously

set_invoke_callbacks_asynchronously
~~~cpp
void set_invoke_callbacks_asynchronously(bool async) {
    invoke_callbacks_asynchronously_ = async;
  }
~~~

### FindFirstPartySetEntry

TestCookieAccessDelegate::FindFirstPartySetEntry
~~~cpp
absl::optional<FirstPartySetEntry> FindFirstPartySetEntry(
      const SchemefulSite& site) const;
~~~
 Finds a FirstPartySetEntry for the given site, if one exists.

### GetKeyForDomainValue

TestCookieAccessDelegate::GetKeyForDomainValue
~~~cpp
std::string GetKeyForDomainValue(const std::string& domain) const;
~~~
 Discard any leading dot in the domain string.

### RunMaybeAsync

TestCookieAccessDelegate::RunMaybeAsync
~~~cpp
absl::optional<T> RunMaybeAsync(T result,
                                  base::OnceCallback<void(T)> callback) const;
~~~

### expectations_



~~~cpp

std::map<std::string, CookieAccessSemantics> expectations_;

~~~


### ignore_samesite_restrictions_schemes_



~~~cpp

std::map<std::string, bool> ignore_samesite_restrictions_schemes_;

~~~


### first_party_sets_



~~~cpp

base::flat_map<SchemefulSite, FirstPartySetEntry> first_party_sets_;

~~~


### invoke_callbacks_asynchronously_



~~~cpp

bool invoke_callbacks_asynchronously_ = false;

~~~


### TestCookieAccessDelegate

TestCookieAccessDelegate
~~~cpp
TestCookieAccessDelegate(const TestCookieAccessDelegate&) = delete;
~~~

### operator=

operator=
~~~cpp
TestCookieAccessDelegate& operator=(const TestCookieAccessDelegate&) = delete;
~~~

### set_invoke_callbacks_asynchronously

set_invoke_callbacks_asynchronously
~~~cpp
void set_invoke_callbacks_asynchronously(bool async) {
    invoke_callbacks_asynchronously_ = async;
  }
~~~

### GetAccessSemantics

TestCookieAccessDelegate::GetAccessSemantics
~~~cpp
CookieAccessSemantics GetAccessSemantics(
      const CanonicalCookie& cookie) const override;
~~~
 CookieAccessDelegate implementation:
### ShouldIgnoreSameSiteRestrictions

TestCookieAccessDelegate::ShouldIgnoreSameSiteRestrictions
~~~cpp
bool ShouldIgnoreSameSiteRestrictions(
      const GURL& url,
      const SiteForCookies& site_for_cookies) const override;
~~~

### ComputeFirstPartySetMetadataMaybeAsync

TestCookieAccessDelegate::ComputeFirstPartySetMetadataMaybeAsync
~~~cpp
absl::optional<FirstPartySetMetadata> ComputeFirstPartySetMetadataMaybeAsync(
      const SchemefulSite& site,
      const SchemefulSite* top_frame_site,
      const std::set<SchemefulSite>& party_context,
      base::OnceCallback<void(FirstPartySetMetadata)> callback) const override;
~~~

### FindFirstPartySetEntries

TestCookieAccessDelegate::FindFirstPartySetEntries
~~~cpp
absl::optional<base::flat_map<SchemefulSite, FirstPartySetEntry>>
  FindFirstPartySetEntries(
      const base::flat_set<SchemefulSite>& sites,
      base::OnceCallback<
          void(base::flat_map<SchemefulSite, FirstPartySetEntry>)> callback)
      const override;
~~~

### SetExpectationForCookieDomain

TestCookieAccessDelegate::SetExpectationForCookieDomain
~~~cpp
void SetExpectationForCookieDomain(const std::string& cookie_domain,
                                     CookieAccessSemantics access_semantics);
~~~
 Sets the expected return value for any cookie whose Domain
 matches |cookie_domain|. Pass the value of |cookie.Domain()| and any
 leading dot will be discarded.

### SetIgnoreSameSiteRestrictionsScheme

TestCookieAccessDelegate::SetIgnoreSameSiteRestrictionsScheme
~~~cpp
void SetIgnoreSameSiteRestrictionsScheme(
      const std::string& site_for_cookies_scheme,
      bool require_secure_origin);
~~~
 Sets the expected return value for ShouldAlwaysAttachSameSiteCookies.

 Can set schemes that always attach SameSite cookies, or schemes that always
 attach SameSite cookies if the request URL is secure.

### SetFirstPartySets

TestCookieAccessDelegate::SetFirstPartySets
~~~cpp
void SetFirstPartySets(
      const base::flat_map<SchemefulSite, FirstPartySetEntry>& sets);
~~~
 Set the test delegate's First-Party Sets. The map's keys are the sites in
 the sets. Primary sites must be included among the keys for a given set.

### FindFirstPartySetEntry

TestCookieAccessDelegate::FindFirstPartySetEntry
~~~cpp
absl::optional<FirstPartySetEntry> FindFirstPartySetEntry(
      const SchemefulSite& site) const;
~~~
 Finds a FirstPartySetEntry for the given site, if one exists.

### GetKeyForDomainValue

TestCookieAccessDelegate::GetKeyForDomainValue
~~~cpp
std::string GetKeyForDomainValue(const std::string& domain) const;
~~~
 Discard any leading dot in the domain string.

### expectations_



~~~cpp

std::map<std::string, CookieAccessSemantics> expectations_;

~~~


### ignore_samesite_restrictions_schemes_



~~~cpp

std::map<std::string, bool> ignore_samesite_restrictions_schemes_;

~~~


### first_party_sets_



~~~cpp

base::flat_map<SchemefulSite, FirstPartySetEntry> first_party_sets_;

~~~


### invoke_callbacks_asynchronously_



~~~cpp

bool invoke_callbacks_asynchronously_ = false;

~~~

