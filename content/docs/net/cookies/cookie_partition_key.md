### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
NET_EXPORT std::ostream& operator<<(std::ostream& os,
                                    const CookiePartitionKey& cpk);
~~~
 Used so that CookiePartitionKeys can be the arguments of DCHECK_EQ.

## class CookiePartitionKey

### CookiePartitionKey

CookiePartitionKey::CookiePartitionKey
~~~cpp
CookiePartitionKey(const CookiePartitionKey& other)
~~~

### operator=

CookiePartitionKey::operator=
~~~cpp
CookiePartitionKey& operator=(const CookiePartitionKey& other);
~~~

### operator=

CookiePartitionKey::operator=
~~~cpp
CookiePartitionKey& operator=(CookiePartitionKey&& other);
~~~

### ~CookiePartitionKey

CookiePartitionKey::~CookiePartitionKey
~~~cpp
~CookiePartitionKey()
~~~

### operator==

CookiePartitionKey::operator==
~~~cpp
bool operator==(const CookiePartitionKey& other) const;
~~~

### operator!=

CookiePartitionKey::operator!=
~~~cpp
bool operator!=(const CookiePartitionKey& other) const;
~~~

### operator&lt;

CookiePartitionKey::operator&lt;
~~~cpp
bool operator<(const CookiePartitionKey& other) const;
~~~

### Serialize

CookiePartitionKey::Serialize
~~~cpp
[[nodiscard]] static bool Serialize(
      const absl::optional<CookiePartitionKey>& in,
      std::string& out);
~~~
 Methods for serializing and deserializing a partition key to/from a string.

 This is currently used for:
 -  Storing persistent partitioned cookies
 -  Loading partitioned cookies into Java code
 -  Sending cookie partition keys as strings in the DevTools protocol

 This function returns true if the partition key is not opaque and if nonce_
 is not present. We do not want to serialize cookies with opaque origins or
 nonce in their partition key to disk, because if the browser session ends
 we will not be able to attach the saved cookie to any future requests. This
 is because opaque origins' nonces are only stored in volatile memory.


 TODO(crbug.com/1225444) Investigate ways to persist partition keys with
 opaque origins if a browser session is restored.

### Deserialize

CookiePartitionKey::Deserialize
~~~cpp
[[nodiscard]] static bool Deserialize(
      const std::string& in,
      absl::optional<CookiePartitionKey>& out);
~~~
 Deserializes the result of the method above.

 If the result is absl::nullopt, the resulting cookie is not partitioned.


 Returns if the resulting partition key is valid.

### FromURLForTesting

FromURLForTesting
~~~cpp
static CookiePartitionKey FromURLForTesting(
      const GURL& url,
      const absl::optional<base::UnguessableToken> nonce = absl::nullopt) {
    return nonce ? CookiePartitionKey(SchemefulSite(url), nonce)
                 : CookiePartitionKey(url);
  }
~~~

### FromNetworkIsolationKey

CookiePartitionKey::FromNetworkIsolationKey
~~~cpp
static absl::optional<CookiePartitionKey> FromNetworkIsolationKey(
      const NetworkIsolationKey& network_isolation_key);
~~~
 Create a partition key from a network isolation key. Partition key is
 derived from the key's top-frame site.

### FromWire

FromWire
~~~cpp
static CookiePartitionKey FromWire(
      const SchemefulSite& site,
      absl::optional<base::UnguessableToken> nonce = absl::nullopt) {
    return CookiePartitionKey(site, nonce);
  }
~~~
 Create a new CookiePartitionKey from the site of an existing
 CookiePartitionKey. This should only be used for sites of partition keys
 which were already created using Deserialize or FromNetworkIsolationKey.

### FromScript

FromScript
~~~cpp
static absl::optional<CookiePartitionKey> FromScript() {
    return absl::make_optional(CookiePartitionKey(true));
  }
~~~
 Create a new CookiePartitionKey in a script running in a renderer. We do
 not trust the renderer to provide us with a cookie partition key, so we let
 the renderer use this method to indicate the cookie is partitioned but the
 key still needs to be determined.


 When the browser is ingesting cookie partition keys from the renderer,
 either the `from_script_` flag should be set or the cookie partition key
 should match the browser's. Otherwise the renderer may be compromised.


 TODO(crbug.com/1225444) Consider removing this factory method and
 `from_script_` flag when BlinkStorageKey is available in
 ServiceWorkerGlobalScope.

### FromStorageKeyComponents

CookiePartitionKey::FromStorageKeyComponents
~~~cpp
static absl::optional<CookiePartitionKey> FromStorageKeyComponents(
      const SchemefulSite& top_level_site,
      const absl::optional<base::UnguessableToken>& nonce);
~~~
 Create a new CookiePartitionKey from the components of a StorageKey.

 Forwards to FromWire, but unlike that method in this one the optional nonce
 argument has no default. It also checks that cookie partitioning is enabled
 before returning a valid key, which FromWire does not check.

### site

site
~~~cpp
const SchemefulSite& site() const { return site_; }
~~~

### from_script

from_script
~~~cpp
bool from_script() const { return from_script_; }
~~~

### IsSerializeable

CookiePartitionKey::IsSerializeable
~~~cpp
bool IsSerializeable() const;
~~~
 Returns true if the current partition key can be serialized to a string.

 Cookie partition keys whose internal site is opaque cannot be serialized.

### nonce

nonce
~~~cpp
const absl::optional<base::UnguessableToken>& nonce() const { return nonce_; }
~~~

### HasNonce

HasNonce
~~~cpp
static bool HasNonce(const absl::optional<CookiePartitionKey>& key) {
    return key && key->nonce();
  }
~~~

### CookiePartitionKey

CookiePartitionKey::CookiePartitionKey
~~~cpp
CookiePartitionKey(const GURL& url)
~~~

### CookiePartitionKey

CookiePartitionKey::CookiePartitionKey
~~~cpp
CookiePartitionKey(bool from_script)
~~~
