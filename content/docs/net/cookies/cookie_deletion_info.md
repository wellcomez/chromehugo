
## class CookieDeletionInfo::TimeRange
 Define a range of time from [start, end) where start is inclusive and end
 is exclusive. There is a special case where |start| == |end| (matching a
 single time) where |end| is inclusive. This special case is for iOS that
 will be removed in the future.


 TODO(crbug.com/830689): Delete the start=end special case.

### TimeRange

TimeRange::CookieDeletionInfo::TimeRange
~~~cpp
TimeRange(const TimeRange& other)
~~~

### TimeRange

TimeRange::CookieDeletionInfo::TimeRange
~~~cpp
TimeRange(base::Time start, base::Time end)
~~~

### operator=

TimeRange::CookieDeletionInfo::operator=
~~~cpp
TimeRange& operator=(const TimeRange& rhs);
~~~

### Contains

TimeRange::CookieDeletionInfo::Contains
~~~cpp
bool Contains(const base::Time& time) const;
~~~
 Is |time| within this time range?

 Will return true if:

   |start_| <= |time| < |end_|

 If |start_| is null then the range is unbounded on the lower range.

 If |end_| is null then the range is unbounded on the upper range.


 Note 1: |time| cannot be null.

 Note 2: If |start_| == |end_| then end_ is inclusive.


### SetStart

TimeRange::CookieDeletionInfo::SetStart
~~~cpp
void SetStart(base::Time value);
~~~
 Set the range start time. Set to null (i.e. Time()) to indicated an
 unbounded lower range.

### SetEnd

TimeRange::CookieDeletionInfo::SetEnd
~~~cpp
void SetEnd(base::Time value);
~~~
 Set the range end time. Set to null (i.e. Time()) to indicated an
 unbounded upper range.

### start

start
~~~cpp
base::Time start() const { return start_; }
~~~
 Return the start time.

### end

end
~~~cpp
base::Time end() const { return end_; }
~~~
 Return the end time.

## struct CookieDeletionInfo
 Used to specify which cookies to delete. All members are ANDed together.

### CookieDeletionInfo

CookieDeletionInfo::CookieDeletionInfo
~~~cpp
CookieDeletionInfo(const CookieDeletionInfo& other)
~~~

### CookieDeletionInfo

CookieDeletionInfo::CookieDeletionInfo
~~~cpp
CookieDeletionInfo(base::Time start_time, base::Time end_time)
~~~

### ~CookieDeletionInfo

CookieDeletionInfo::~CookieDeletionInfo
~~~cpp
~CookieDeletionInfo()
~~~

### operator=

CookieDeletionInfo::operator=
~~~cpp
CookieDeletionInfo& operator=(CookieDeletionInfo&& rhs);
~~~

### operator=

CookieDeletionInfo::operator=
~~~cpp
CookieDeletionInfo& operator=(const CookieDeletionInfo& rhs);
~~~

### Matches

CookieDeletionInfo::Matches
~~~cpp
bool Matches(const CanonicalCookie& cookie,
               const CookieAccessParams& params) const;
~~~
 Return true if |cookie| matches all members of this instance. All members
 are ANDed together. For example: if the |cookie| creation date is within
 |creation_range| AND the |cookie| name is equal to |name|, etc. then true
 will be returned. If not false.


 |params.access_semantics| is the access semantics mode of the cookie at the
 time of the attempted match. This is used to determine whether the cookie
 matches a particular URL based on effective SameSite mode. (But the value
 should not matter because the CookieOptions used for this check includes
 all cookies for a URL regardless of SameSite).


 |params.delegate_treats_url_as_trustworthy| should be set to true if |url|
 was granted access to secure cookies by the CookieAccessDelegate.


 All members are used. See comments above other members for specifics
 about how checking is done for that value.
