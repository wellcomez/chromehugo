### BuildCanonicalCookie

BuildCanonicalCookie
~~~cpp
std::unique_ptr<CanonicalCookie> BuildCanonicalCookie(
    const GURL& url,
    const std::string& cookie_line,
    const base::Time& creation_time);
~~~
 Helper to build a single CanonicalCookie.

### AddCookieToList

AddCookieToList
~~~cpp
void AddCookieToList(const GURL& url,
                     const std::string& cookie_line,
                     const base::Time& creation_time,
                     std::vector<std::unique_ptr<CanonicalCookie>>* out_list);
~~~
 Helper to build a list of CanonicalCookie*s.

### CreateMonsterFromStoreForGC

CreateMonsterFromStoreForGC
~~~cpp
std::unique_ptr<CookieMonster> CreateMonsterFromStoreForGC(
    int num_secure_cookies,
    int num_old_secure_cookies,
    int num_non_secure_cookies,
    int num_old_non_secure_cookies,
    int days_old);
~~~
 Helper function for creating a CookieMonster backed by a
 MockSimplePersistentCookieStore for garbage collection testing.


 Fill the store through import with |num_*_cookies| cookies,
 |num_old_*_cookies| with access time Now()-days_old, the rest with access
 time Now(). Cookies made by |num_secure_cookies| and |num_non_secure_cookies|
 will be marked secure and non-secure, respectively. Do two SetCookies().

 Return whether each of the two SetCookies() took longer than |gc_perf_micros|
 to complete, and how many cookie were left in the store afterwards.

## class MockPersistentCookieStore
 Implementation of PersistentCookieStore that captures the
 received commands and saves them to a list.

 The result of calls to Load() can be configured using SetLoadExpectation().

### MockPersistentCookieStore

MockPersistentCookieStore::MockPersistentCookieStore
~~~cpp
MockPersistentCookieStore();
~~~

### MockPersistentCookieStore

MockPersistentCookieStore
~~~cpp
MockPersistentCookieStore(const MockPersistentCookieStore&) = delete;
~~~

### operator=

operator=
~~~cpp
MockPersistentCookieStore& operator=(const MockPersistentCookieStore&) =
      delete;
~~~

### set_store_load_commands

set_store_load_commands
~~~cpp
void set_store_load_commands(bool store_load_commands) {
    store_load_commands_ = store_load_commands;
  }
~~~
 When set, Load() and LoadCookiesForKey() calls are store in the command
 list, rather than being automatically executed. Defaults to false.

### SetLoadExpectation

MockPersistentCookieStore::SetLoadExpectation
~~~cpp
void SetLoadExpectation(bool return_value,
                          std::vector<std::unique_ptr<CanonicalCookie>> result);
~~~

### commands

commands
~~~cpp
const CommandList& commands() const { return commands_; }
~~~

### TakeCommands

TakeCommands
~~~cpp
CommandList TakeCommands() { return std::move(commands_); }
~~~

### TakeCallbackAt

TakeCallbackAt
~~~cpp
CookieMonster::PersistentCookieStore::LoadedCallback TakeCallbackAt(
      size_t i) {
    return std::move(commands_[i].loaded_callback);
  }
~~~

### Load

MockPersistentCookieStore::Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~

### LoadCookiesForKey

MockPersistentCookieStore::LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback loaded_callback) override;
~~~

### AddCookie

MockPersistentCookieStore::AddCookie
~~~cpp
void AddCookie(const CanonicalCookie& cookie) override;
~~~

### UpdateCookieAccessTime

MockPersistentCookieStore::UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie& cookie) override;
~~~

### DeleteCookie

MockPersistentCookieStore::DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie& cookie) override;
~~~

### SetForceKeepSessionState

MockPersistentCookieStore::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

MockPersistentCookieStore::SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

MockPersistentCookieStore::Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### ~MockPersistentCookieStore

MockPersistentCookieStore::~MockPersistentCookieStore
~~~cpp
~MockPersistentCookieStore() override;
~~~

### commands_



~~~cpp

CommandList commands_;

~~~


### store_load_commands_



~~~cpp

bool store_load_commands_ = false;

~~~


### load_return_value_



~~~cpp

bool load_return_value_ = true;

~~~

 Deferred result to use when Load() is called.

### load_result_



~~~cpp

std::vector<std::unique_ptr<CanonicalCookie>> load_result_;

~~~


### loaded_



~~~cpp

bool loaded_ = false;

~~~

 Indicates if the store has been fully loaded to avoid returning duplicate
 cookies.

### MockPersistentCookieStore

MockPersistentCookieStore
~~~cpp
MockPersistentCookieStore(const MockPersistentCookieStore&) = delete;
~~~

### operator=

operator=
~~~cpp
MockPersistentCookieStore& operator=(const MockPersistentCookieStore&) =
      delete;
~~~

### set_store_load_commands

set_store_load_commands
~~~cpp
void set_store_load_commands(bool store_load_commands) {
    store_load_commands_ = store_load_commands;
  }
~~~
 When set, Load() and LoadCookiesForKey() calls are store in the command
 list, rather than being automatically executed. Defaults to false.

### commands

commands
~~~cpp
const CommandList& commands() const { return commands_; }
~~~

### TakeCommands

TakeCommands
~~~cpp
CommandList TakeCommands() { return std::move(commands_); }
~~~

### TakeCallbackAt

TakeCallbackAt
~~~cpp
CookieMonster::PersistentCookieStore::LoadedCallback TakeCallbackAt(
      size_t i) {
    return std::move(commands_[i].loaded_callback);
  }
~~~

### SetLoadExpectation

MockPersistentCookieStore::SetLoadExpectation
~~~cpp
void SetLoadExpectation(bool return_value,
                          std::vector<std::unique_ptr<CanonicalCookie>> result);
~~~

### Load

MockPersistentCookieStore::Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~

### LoadCookiesForKey

MockPersistentCookieStore::LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback loaded_callback) override;
~~~

### AddCookie

MockPersistentCookieStore::AddCookie
~~~cpp
void AddCookie(const CanonicalCookie& cookie) override;
~~~

### UpdateCookieAccessTime

MockPersistentCookieStore::UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie& cookie) override;
~~~

### DeleteCookie

MockPersistentCookieStore::DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie& cookie) override;
~~~

### SetForceKeepSessionState

MockPersistentCookieStore::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

MockPersistentCookieStore::SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

MockPersistentCookieStore::Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### commands_



~~~cpp

CommandList commands_;

~~~


### store_load_commands_



~~~cpp

bool store_load_commands_ = false;

~~~


### load_return_value_



~~~cpp

bool load_return_value_ = true;

~~~

 Deferred result to use when Load() is called.

### load_result_



~~~cpp

std::vector<std::unique_ptr<CanonicalCookie>> load_result_;

~~~


### loaded_



~~~cpp

bool loaded_ = false;

~~~

 Indicates if the store has been fully loaded to avoid returning duplicate
 cookies.

## class MockSimplePersistentCookieStore
 Just act like a backing database.  Keep cookie information from
 Add/Update/Delete and regurgitate it when Load is called.

### MockSimplePersistentCookieStore

MockSimplePersistentCookieStore::MockSimplePersistentCookieStore
~~~cpp
MockSimplePersistentCookieStore();
~~~

### Load

MockSimplePersistentCookieStore::Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~

### LoadCookiesForKey

MockSimplePersistentCookieStore::LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback loaded_callback) override;
~~~

### AddCookie

MockSimplePersistentCookieStore::AddCookie
~~~cpp
void AddCookie(const CanonicalCookie& cookie) override;
~~~

### UpdateCookieAccessTime

MockSimplePersistentCookieStore::UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie& cookie) override;
~~~

### DeleteCookie

MockSimplePersistentCookieStore::DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie& cookie) override;
~~~

### SetForceKeepSessionState

MockSimplePersistentCookieStore::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

MockSimplePersistentCookieStore::SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

MockSimplePersistentCookieStore::Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### ~MockSimplePersistentCookieStore

MockSimplePersistentCookieStore::~MockSimplePersistentCookieStore
~~~cpp
~MockSimplePersistentCookieStore() override;
~~~

### cookies_



~~~cpp

CanonicalCookieMap cookies_;

~~~


### loaded_



~~~cpp

bool loaded_ = false;

~~~

 Indicates if the store has been fully loaded to avoid return duplicate
 cookies in subsequent load requests
### Load

MockSimplePersistentCookieStore::Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~

### LoadCookiesForKey

MockSimplePersistentCookieStore::LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback loaded_callback) override;
~~~

### AddCookie

MockSimplePersistentCookieStore::AddCookie
~~~cpp
void AddCookie(const CanonicalCookie& cookie) override;
~~~

### UpdateCookieAccessTime

MockSimplePersistentCookieStore::UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie& cookie) override;
~~~

### DeleteCookie

MockSimplePersistentCookieStore::DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie& cookie) override;
~~~

### SetForceKeepSessionState

MockSimplePersistentCookieStore::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

MockSimplePersistentCookieStore::SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

MockSimplePersistentCookieStore::Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### cookies_



~~~cpp

CanonicalCookieMap cookies_;

~~~


### loaded_



~~~cpp

bool loaded_ = false;

~~~

 Indicates if the store has been fully loaded to avoid return duplicate
 cookies in subsequent load requests