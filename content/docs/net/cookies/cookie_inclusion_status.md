### operator&lt;&lt;

operator&lt;&lt;
~~~cpp
NET_EXPORT inline std::ostream& operator<<(std::ostream& os,
                                           const CookieInclusionStatus status) {
  return os << status.GetDebugString();
}
~~~

### PrintTo

PrintTo
~~~cpp
inline void PrintTo(const CookieInclusionStatus& cis, std::ostream* os) {
  *os << cis;
}
~~~
 Provided to allow gtest to create more helpful error messages, instead of
 printing hex.

## class CookieInclusionStatus
 This class represents if a cookie was included or excluded in a cookie get or
 set operation, and if excluded why. It holds a vector of reasons for
 exclusion, where cookie inclusion is represented by the absence of any
 exclusion reasons. Also marks whether a cookie should be warned about, e.g.

 for deprecation or intervention reasons.

 TODO(crbug.com/1310444): Improve serialization validation comments.

### ExclusionReasonBitset exclusion_reasons

ExclusionReasonBitset exclusion_reasons
~~~cpp
explicit CookieInclusionStatus(ExclusionReason reason);
  // Makes a status that contains the given exclusion reason and warning.
  CookieInclusionStatus(ExclusionReason reason, WarningReason warning);
  // Makes a status that contains the given warning.
  explicit CookieInclusionStatus(WarningReason warning);

  // Copyable.
  CookieInclusionStatus(const CookieInclusionStatus& other);
  CookieInclusionStatus& operator=(const CookieInclusionStatus& other);

  bool operator==(const CookieInclusionStatus& other) const;
  bool operator!=(const CookieInclusionStatus& other) const;

  // Whether the status is to include the cookie, and has no other reasons for
  // exclusion.
  bool IsInclude() const;

  // Whether the given reason for exclusion is present.
  bool HasExclusionReason(ExclusionReason status_type) const;

  // Whether the given reason for exclusion is present, and is the ONLY reason
  // for exclusion.
  bool HasOnlyExclusionReason(ExclusionReason status_type) const;

  // Add an exclusion reason.
  void AddExclusionReason(ExclusionReason status_type);

  // Remove an exclusion reason.
  void RemoveExclusionReason(ExclusionReason reason);

  // Remove multiple exclusion reasons.
  void RemoveExclusionReasons(const std::vector<ExclusionReason>& reasons);

  // If the cookie would have been excluded for reasons other than
  // SameSite-related reasons, don't bother warning about it (clear the
  // warning).
  void MaybeClearSameSiteWarning();

  // Whether to record the breaking downgrade metrics if the cookie is included
  // or if it's only excluded because of insufficient same-site context.
  bool ShouldRecordDowngradeMetrics() const;

  // Whether the cookie should be warned about.
  bool ShouldWarn() const;

  // Whether the given reason for warning is present.
  bool HasWarningReason(WarningReason reason) const;

  // Whether a schemeful downgrade warning is present.
  // A schemeful downgrade means that an included cookie with an effective
  // SameSite is experiencing a SameSiteCookieContext::|context| ->
  // SameSiteCookieContext::|schemeful_context| downgrade that will prevent its
  // access schemefully. If the function returns true and |reason| is valid then
  // |reason| will contain which warning was found.
  bool HasDowngradeWarning(
      CookieInclusionStatus::WarningReason* reason = nullptr) const;

  // Add an warning reason.
  void AddWarningReason(WarningReason reason);

  // Remove an warning reason.
  void RemoveWarningReason(WarningReason reason);

  // Used for serialization/deserialization.
  ExclusionReasonBitset exclusion_reasons() const { return exclusion_reasons_; }
~~~
 Make a status that contains the given exclusion reason.

### set_exclusion_reasons

set_exclusion_reasons
~~~cpp
void set_exclusion_reasons(ExclusionReasonBitset exclusion_reasons) {
    exclusion_reasons_ = exclusion_reasons;
  }
~~~

### warning_reasons

warning_reasons
~~~cpp
WarningReasonBitset warning_reasons() const { return warning_reasons_; }
~~~

### set_warning_reasons

set_warning_reasons
~~~cpp
void set_warning_reasons(WarningReasonBitset warning_reasons) {
    warning_reasons_ = warning_reasons;
  }
~~~

### GetBreakingDowngradeMetricsEnumValue

CookieInclusionStatus::GetBreakingDowngradeMetricsEnumValue
~~~cpp
ContextDowngradeMetricValues GetBreakingDowngradeMetricsEnumValue(
      const GURL& url) const;
~~~

### GetDebugString

CookieInclusionStatus::GetDebugString
~~~cpp
std::string GetDebugString() const;
~~~
 Get exclusion reason(s) and warning in string format.

### HasExactlyExclusionReasonsForTesting

CookieInclusionStatus::HasExactlyExclusionReasonsForTesting
~~~cpp
bool HasExactlyExclusionReasonsForTesting(
      std::vector<ExclusionReason> reasons) const;
~~~
 Checks whether the exclusion reasons are exactly the set of exclusion
 reasons in the vector. (Ignores warnings.)
### HasExactlyWarningReasonsForTesting

CookieInclusionStatus::HasExactlyWarningReasonsForTesting
~~~cpp
bool HasExactlyWarningReasonsForTesting(
      std::vector<WarningReason> reasons) const;
~~~
 Checks whether the warning reasons are exactly the set of warning
 reasons in the vector. (Ignores exclusions.)
### ValidateExclusionAndWarningFromWire

CookieInclusionStatus::ValidateExclusionAndWarningFromWire
~~~cpp
static bool ValidateExclusionAndWarningFromWire(uint32_t exclusion_reasons,
                                                  uint32_t warning_reasons);
~~~
 Validates mojo data, since mojo does not support bitsets.

 TODO(crbug.com/1310444): Improve serialization validation comments
 and check for mutually exclusive values.

### MakeFromReasonsForTesting

CookieInclusionStatus::MakeFromReasonsForTesting
~~~cpp
static CookieInclusionStatus MakeFromReasonsForTesting(
      std::vector<ExclusionReason> reasons,
      std::vector<WarningReason> warnings = std::vector<WarningReason>());
~~~
 Makes a status that contains the given exclusion reasons and warning.

### ExcludedByUserPreferences

CookieInclusionStatus::ExcludedByUserPreferences
~~~cpp
bool ExcludedByUserPreferences() const;
~~~
 Returns true if the cookie was excluded because of user preferences.

 HasOnlyExclusionReason(EXCLUDE_USER_PREFERENCES) will not return true for
 third-party cookies blocked in sites in the same First-Party Set (note:
 this is not the same as the cookie being blocked in a same-party context,
 which takes the entire ancestor chain into account). See
 https://crbug.com/1366868.
