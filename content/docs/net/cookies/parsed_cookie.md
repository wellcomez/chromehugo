
## class ParsedCookie

### ParsedCookie

ParsedCookie
~~~cpp
explicit ParsedCookie(const std::string& cookie_line,
                        CookieInclusionStatus* status_out = nullptr);

  ParsedCookie(const ParsedCookie&) = delete;
~~~
 Construct from a cookie string like "BLAH=1; path=/; domain=.google.com"
 Format is according to RFC6265bis. Cookies with both name and value empty
 will be considered invalid.

 `status_out` is a nullable output param which will be populated with
 informative exclusion reasons if the resulting ParsedCookie is invalid.

 The CookieInclusionStatus will not be altered if the resulting ParsedCookie
 is valid.

### operator=

ParsedCookie::operator=
~~~cpp
ParsedCookie& operator=(const ParsedCookie&) = delete;

  ~ParsedCookie();
~~~

### IsValid

ParsedCookie::IsValid
~~~cpp
bool IsValid() const;
~~~
 You should not call any other methods except for SetName/SetValue on the
 class if !IsValid.

### Name

Name
~~~cpp
const std::string& Name() const { return pairs_[0].first; }
~~~

### Token

Token
~~~cpp
const std::string& Token() const { return Name(); }
~~~

### Value

Value
~~~cpp
const std::string& Value() const { return pairs_[0].second; }
~~~

### HasPath

HasPath
~~~cpp
bool HasPath() const { return path_index_ != 0; }
~~~

### Path

Path
~~~cpp
const std::string& Path() const {
    DCHECK(HasPath());
    return pairs_[path_index_].second;
  }
~~~

### HasDomain

HasDomain
~~~cpp
bool HasDomain() const { return domain_index_ != 0; }
~~~
 Note that Domain() may return the empty string; in the case of cookie_line
 "domain=", HasDomain() will return true (as the empty string is an
 acceptable domain value), so Domain() will return std::string().

### Domain

Domain
~~~cpp
const std::string& Domain() const {
    DCHECK(HasDomain());
    return pairs_[domain_index_].second;
  }
~~~

### HasExpires

HasExpires
~~~cpp
bool HasExpires() const { return expires_index_ != 0; }
~~~

### Expires

Expires
~~~cpp
const std::string& Expires() const {
    DCHECK(HasExpires());
    return pairs_[expires_index_].second;
  }
~~~

### HasMaxAge

HasMaxAge
~~~cpp
bool HasMaxAge() const { return maxage_index_ != 0; }
~~~

### MaxAge

MaxAge
~~~cpp
const std::string& MaxAge() const {
    DCHECK(HasMaxAge());
    return pairs_[maxage_index_].second;
  }
~~~

### IsSecure

IsSecure
~~~cpp
bool IsSecure() const { return secure_index_ != 0; }
~~~

### IsHttpOnly

IsHttpOnly
~~~cpp
bool IsHttpOnly() const { return httponly_index_ != 0; }
~~~

### SameSite

ParsedCookie::SameSite
~~~cpp
CookieSameSite SameSite(
      CookieSameSiteString* samesite_string = nullptr) const;
~~~
 Also spits out an enum value representing the string given as the SameSite
 attribute value, if |samesite_string| is non-null.

### Priority

ParsedCookie::Priority
~~~cpp
CookiePriority Priority() const;
~~~

### IsSameParty

IsSameParty
~~~cpp
bool IsSameParty() const { return same_party_index_ != 0; }
~~~

### IsPartitioned

IsPartitioned
~~~cpp
bool IsPartitioned() const { return partitioned_index_ != 0; }
~~~

### HasInternalHtab

HasInternalHtab
~~~cpp
bool HasInternalHtab() const { return internal_htab_; }
~~~

### GetTruncatingCharacterInCookieStringType

GetTruncatingCharacterInCookieStringType
~~~cpp
TruncatingCharacterInCookieStringType
  GetTruncatingCharacterInCookieStringType() const {
    return truncating_char_in_cookie_string_type_;
  }
~~~

### NumberOfAttributes

NumberOfAttributes
~~~cpp
size_t NumberOfAttributes() const { return pairs_.size() - 1; }
~~~
 Returns the number of attributes, for example, returning 2 for:
   "BLAH=hah; path=/; domain=.google.com"
### SetName

ParsedCookie::SetName
~~~cpp
bool SetName(const std::string& name);
~~~
 These functions set the respective properties of the cookie. If the
 parameters are empty, the respective properties are cleared.

 The functions return false in case an error occurred.

 The cookie needs to be assigned a name/value before setting the other
 attributes.


 These functions should only be used if you need to modify a response's
 Set-Cookie string. The resulting ParsedCookie and its Set-Cookie string
 should still go through the regular cookie parsing process before entering
 the cookie jar.

### SetValue

ParsedCookie::SetValue
~~~cpp
bool SetValue(const std::string& value);
~~~

### SetPath

ParsedCookie::SetPath
~~~cpp
bool SetPath(const std::string& path);
~~~

### SetDomain

ParsedCookie::SetDomain
~~~cpp
bool SetDomain(const std::string& domain);
~~~

### SetExpires

ParsedCookie::SetExpires
~~~cpp
bool SetExpires(const std::string& expires);
~~~

### SetMaxAge

ParsedCookie::SetMaxAge
~~~cpp
bool SetMaxAge(const std::string& maxage);
~~~

### SetIsSecure

ParsedCookie::SetIsSecure
~~~cpp
bool SetIsSecure(bool is_secure);
~~~

### SetIsHttpOnly

ParsedCookie::SetIsHttpOnly
~~~cpp
bool SetIsHttpOnly(bool is_http_only);
~~~

### SetSameSite

ParsedCookie::SetSameSite
~~~cpp
bool SetSameSite(const std::string& same_site);
~~~

### SetPriority

ParsedCookie::SetPriority
~~~cpp
bool SetPriority(const std::string& priority);
~~~

### SetIsSameParty

ParsedCookie::SetIsSameParty
~~~cpp
bool SetIsSameParty(bool is_same_party);
~~~

### SetIsPartitioned

ParsedCookie::SetIsPartitioned
~~~cpp
bool SetIsPartitioned(bool is_partitioned);
~~~

### ToCookieLine

ParsedCookie::ToCookieLine
~~~cpp
std::string ToCookieLine() const;
~~~
 Returns the cookie description as it appears in a HTML response header.

### FindFirstTerminator

ParsedCookie::FindFirstTerminator
~~~cpp
static std::string::const_iterator FindFirstTerminator(const std::string& s);
~~~
 Returns an iterator pointing to the first terminator character found in
 the given string.

### ParseToken

ParsedCookie::ParseToken
~~~cpp
static bool ParseToken(std::string::const_iterator* it,
                         const std::string::const_iterator& end,
                         std::string::const_iterator* token_start,
                         std::string::const_iterator* token_end);
~~~
 Given iterators pointing to the beginning and end of a string segment,
 returns as output arguments token_start and token_end to the start and end
 positions of a cookie attribute token name parsed from the segment, and
 updates the segment iterator to point to the next segment to be parsed.

 If no token is found, the function returns false and the segment iterator
 is set to end.

### ParseValue

ParsedCookie::ParseValue
~~~cpp
static void ParseValue(std::string::const_iterator* it,
                         const std::string::const_iterator& end,
                         std::string::const_iterator* value_start,
                         std::string::const_iterator* value_end);
~~~
 Given iterators pointing to the beginning and end of a string segment,
 returns as output arguments value_start and value_end to the start and end
 positions of a cookie attribute value parsed from the segment, and updates
 the segment iterator to point to the next segment to be parsed.

### ParseTokenString

ParsedCookie::ParseTokenString
~~~cpp
static std::string ParseTokenString(const std::string& token);
~~~
 Same as the above functions, except the input is assumed to contain the
 desired token/value and nothing else.

### ParseValueString

ParsedCookie::ParseValueString
~~~cpp
static std::string ParseValueString(const std::string& value);
~~~

### ValueMatchesParsedValue

ParsedCookie::ValueMatchesParsedValue
~~~cpp
static bool ValueMatchesParsedValue(const std::string& value);
~~~
 Returns |true| if the parsed version of |value| matches |value|.

### IsValidCookieName

ParsedCookie::IsValidCookieName
~~~cpp
static bool IsValidCookieName(const std::string& name);
~~~
 Is the string valid as the name of the cookie or as an attribute name?
### IsValidCookieValue

ParsedCookie::IsValidCookieValue
~~~cpp
static bool IsValidCookieValue(const std::string& value);
~~~
 Is the string valid as the value of the cookie?
### CookieAttributeValueHasValidCharSet

ParsedCookie::CookieAttributeValueHasValidCharSet
~~~cpp
static bool CookieAttributeValueHasValidCharSet(const std::string& value);
~~~
 Is the string free of any characters not allowed in attribute values?
### CookieAttributeValueHasValidSize

ParsedCookie::CookieAttributeValueHasValidSize
~~~cpp
static bool CookieAttributeValueHasValidSize(const std::string& value);
~~~
 Is the string less than the size limits set for attribute values?
### IsValidCookieNameValuePair

ParsedCookie::IsValidCookieNameValuePair
~~~cpp
static bool IsValidCookieNameValuePair(
      const std::string& name,
      const std::string& value,
      CookieInclusionStatus* status_out = nullptr);
~~~
 Returns `true` if the name and value combination are valid. Calls
 IsValidCookieName() and IsValidCookieValue() on `name` and `value`
 respectively, in addition to checking that the sum of the two doesn't
 exceed size limits specified in RFC6265bis.

### SetupAttributes

ParsedCookie::SetupAttributes
~~~cpp
void SetupAttributes();
~~~

### SetString

ParsedCookie::SetString
~~~cpp
bool SetString(size_t* index,
                 const std::string& key,
                 const std::string& value);
~~~
 Sets a key/value pair for a cookie. |index| has to point to one of the
 |*_index_| fields in ParsedCookie and is updated to the position where
 the key/value pair is set in |pairs_|. Accordingly, |key| has to correspond
 to the token matching |index|. If |value| contains invalid characters, the
 cookie parameter is not changed and the function returns false.

 If |value| is empty/false the key/value pair is removed.

### SetBool

ParsedCookie::SetBool
~~~cpp
bool SetBool(size_t* index, const std::string& key, bool value);
~~~

### SetAttributePair

ParsedCookie::SetAttributePair
~~~cpp
bool SetAttributePair(size_t* index,
                        const std::string& key,
                        const std::string& value);
~~~
 Helper function for SetString and SetBool handling the case that the
 key/value pair shall not be removed.

### ClearAttributePair

ParsedCookie::ClearAttributePair
~~~cpp
void ClearAttributePair(size_t index);
~~~
 Removes the key/value pair from a cookie that is identified by |index|.

 |index| refers to a position in |pairs_|.
