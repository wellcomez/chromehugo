
## class StaticCookiePolicy
 The StaticCookiePolicy class implements a static cookie policy that supports
 three modes: allow all, deny all, or block third-party cookies.

### StaticCookiePolicy

StaticCookiePolicy
~~~cpp
StaticCookiePolicy() : type_(StaticCookiePolicy::ALLOW_ALL_COOKIES) {}
~~~

### StaticCookiePolicy

StaticCookiePolicy
~~~cpp
explicit StaticCookiePolicy(Type type) : type_(type) {}
~~~

### StaticCookiePolicy

StaticCookiePolicy
~~~cpp
StaticCookiePolicy(const StaticCookiePolicy&) = delete;
~~~

### operator=

StaticCookiePolicy::operator=
~~~cpp
StaticCookiePolicy& operator=(const StaticCookiePolicy&) = delete;
~~~

### set_type

set_type
~~~cpp
void set_type(Type type) { type_ = type; }
~~~
 Sets the current policy to enforce. This should be called when the user's
 preferences change.

### type

type
~~~cpp
Type type() const { return type_; }
~~~

### CanAccessCookies

StaticCookiePolicy::CanAccessCookies
~~~cpp
int CanAccessCookies(const GURL& url,
                       const net::SiteForCookies& site_for_cookies) const;
~~~
 Consults the user's third-party cookie blocking preferences to determine
 whether the URL's cookies can be accessed (i.e., can be get or set).
