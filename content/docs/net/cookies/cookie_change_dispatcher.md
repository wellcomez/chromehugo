### CookieChangeCauseToString

CookieChangeCauseToString
~~~cpp
NET_EXPORT const char* CookieChangeCauseToString(CookieChangeCause cause);
~~~
 Return a string corresponding to the change cause.  For debugging/logging.

### CookieChangeCauseIsDeletion

CookieChangeCauseIsDeletion
~~~cpp
NET_EXPORT bool CookieChangeCauseIsDeletion(CookieChangeCause cause);
~~~
 Returns whether |cause| is one that could be a reason for deleting a cookie.

 This function assumes that ChangeCause::EXPLICIT is a reason for deletion.

## struct CookieChangeInfo

### CookieChangeInfo

CookieChangeInfo::CookieChangeInfo
~~~cpp
CookieChangeInfo(const CanonicalCookie& cookie,
                   CookieAccessResult access_result,
                   CookieChangeCause cause)
~~~

### ~CookieChangeInfo

CookieChangeInfo::~CookieChangeInfo
~~~cpp
~CookieChangeInfo()
~~~

## class CookieChangeSubscription
 Records a listener's interest in CookieStore changes.


 Each call to CookieChangeDispatcher::Add*() is a listener expressing an
 interest in observing CookieStore changes. Each call creates a
 CookieChangeSubscription instance whose ownership is passed to the listener.


 When the listener's interest disappears (usually at destruction time), the
 listener must communicate this by destroying the CookieChangeSubscription
 instance. The callback passed to the Add*() method will not to be called
 after the returned handle is destroyed.


 CookieChangeSubscription instances do not keep the observed CookieStores
 alive.


 Instances of this class are not thread-safe, and must be destroyed on the
 same thread that they were obtained on.

### CookieChangeSubscription

CookieChangeSubscription
~~~cpp
CookieChangeSubscription() = default;
~~~

### CookieChangeSubscription

CookieChangeSubscription
~~~cpp
CookieChangeSubscription(const CookieChangeSubscription&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieChangeSubscription& operator=(const CookieChangeSubscription&) = delete;
~~~

### ~CookieChangeSubscription

~CookieChangeSubscription
~~~cpp
virtual ~CookieChangeSubscription() = default;
~~~

### CookieChangeSubscription

CookieChangeSubscription
~~~cpp
CookieChangeSubscription() = default;
~~~

### CookieChangeSubscription

CookieChangeSubscription
~~~cpp
CookieChangeSubscription(const CookieChangeSubscription&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieChangeSubscription& operator=(const CookieChangeSubscription&) = delete;
~~~

### ~CookieChangeSubscription

~CookieChangeSubscription
~~~cpp
virtual ~CookieChangeSubscription() = default;
~~~

## class CookieChangeDispatcher
 Exposes changes to a CookieStore's contents.


 A component that wishes to react to changes in a CookieStore (the listener)
 must register its interest (subscribe) by calling one of the Add*() methods
 exposed by this interface.


 CookieChangeDispatch instances are intended to be embedded in CookieStore
 implementations, so they are not intended to be created as standalone objects
 on the heap.


 At the time of this writing (Q1 2018), using this interface has non-trivial
 performance implications on all implementations. This issue should be fixed
 by the end of 2018, at which point this warning should go away. Until then,
 please understand and reason about the performance impact of your change if
 you're adding uses of this to the codebase.

### CookieChangeDispatcher

CookieChangeDispatcher
~~~cpp
CookieChangeDispatcher() = default;
~~~

### CookieChangeDispatcher

CookieChangeDispatcher
~~~cpp
CookieChangeDispatcher(const CookieChangeDispatcher&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieChangeDispatcher& operator=(const CookieChangeDispatcher&) = delete;
~~~

### ~CookieChangeDispatcher

~CookieChangeDispatcher
~~~cpp
virtual ~CookieChangeDispatcher() = default;
~~~

### AddCallbackForCookie

AddCallbackForCookie
~~~cpp
[[nodiscard]] virtual std::unique_ptr<CookieChangeSubscription>
  AddCallbackForCookie(
      const GURL& url,
      const std::string& name,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) = 0;
~~~
 Observe changes to all cookies named `name` that would be sent in a
 request to `url`.


 If `cookie_partition_key` is nullopt, then we ignore all change events for
 partitioned cookies. Otherwise it only subscribes to change events for
 partitioned cookies with the same provided key.

 Unpartitioned cookies are not affected by the `cookie_partition_key`
 parameter.

### AddCallbackForUrl

AddCallbackForUrl
~~~cpp
[[nodiscard]] virtual std::unique_ptr<CookieChangeSubscription>
  AddCallbackForUrl(
      const GURL& url,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) = 0;
~~~
 Observe changes to the cookies that would be sent for a request to `url`.


 If `cookie_partition_key` is nullopt, then we ignore all change events for
 partitioned cookies. Otherwise it only subscribes to change events for
 partitioned cookies with the same provided key.

 Unpartitioned cookies are not affected by the `cookie_partition_key`
 parameter.

### AddCallbackForAllChanges

AddCallbackForAllChanges
~~~cpp
[[nodiscard]] virtual std::unique_ptr<CookieChangeSubscription>
  AddCallbackForAllChanges(CookieChangeCallback callback) = 0;
~~~
 Observe all the CookieStore's changes.


 The callback will not observe a few bookkeeping changes.

 See kChangeCauseMapping in cookie_monster.cc for details.

 TODO(crbug.com/1225444): Add support for Partitioned cookies.

### CookieChangeDispatcher

CookieChangeDispatcher
~~~cpp
CookieChangeDispatcher() = default;
~~~

### CookieChangeDispatcher

CookieChangeDispatcher
~~~cpp
CookieChangeDispatcher(const CookieChangeDispatcher&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieChangeDispatcher& operator=(const CookieChangeDispatcher&) = delete;
~~~

### ~CookieChangeDispatcher

~CookieChangeDispatcher
~~~cpp
virtual ~CookieChangeDispatcher() = default;
~~~

### AddCallbackForCookie

AddCallbackForCookie
~~~cpp
[[nodiscard]] virtual std::unique_ptr<CookieChangeSubscription>
  AddCallbackForCookie(
      const GURL& url,
      const std::string& name,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) = 0;
~~~
 Observe changes to all cookies named `name` that would be sent in a
 request to `url`.


 If `cookie_partition_key` is nullopt, then we ignore all change events for
 partitioned cookies. Otherwise it only subscribes to change events for
 partitioned cookies with the same provided key.

 Unpartitioned cookies are not affected by the `cookie_partition_key`
 parameter.

### AddCallbackForUrl

AddCallbackForUrl
~~~cpp
[[nodiscard]] virtual std::unique_ptr<CookieChangeSubscription>
  AddCallbackForUrl(
      const GURL& url,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) = 0;
~~~
 Observe changes to the cookies that would be sent for a request to `url`.


 If `cookie_partition_key` is nullopt, then we ignore all change events for
 partitioned cookies. Otherwise it only subscribes to change events for
 partitioned cookies with the same provided key.

 Unpartitioned cookies are not affected by the `cookie_partition_key`
 parameter.

### AddCallbackForAllChanges

AddCallbackForAllChanges
~~~cpp
[[nodiscard]] virtual std::unique_ptr<CookieChangeSubscription>
  AddCallbackForAllChanges(CookieChangeCallback callback) = 0;
~~~
 Observe all the CookieStore's changes.


 The callback will not observe a few bookkeeping changes.

 See kChangeCauseMapping in cookie_monster.cc for details.

 TODO(crbug.com/1225444): Add support for Partitioned cookies.
