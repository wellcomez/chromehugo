### std::string CookiePriorityToString

std::string CookiePriorityToString
~~~cpp
NET_EXPORT std::string CookiePriorityToString(CookiePriority priority);
~~~
 Returns the Set-Cookie header priority token corresponding to |priority|.

### CookiePriority StringToCookiePriority

CookiePriority StringToCookiePriority
~~~cpp
NET_EXPORT CookiePriority StringToCookiePriority(const std::string& priority);
~~~
 Converts the Set-Cookie header priority token |priority| to a CookiePriority.

 Defaults to COOKIE_PRIORITY_DEFAULT for empty or unrecognized strings.

### std::string CookieSameSiteToString

std::string CookieSameSiteToString
~~~cpp
NET_EXPORT std::string CookieSameSiteToString(CookieSameSite same_site);
~~~
 Returns a string corresponding to the value of the |same_site| token.

 Intended only for debugging/logging.

### CookieSameSite
StringToCookieSameSite

CookieSameSite
StringToCookieSameSite
~~~cpp
NET_EXPORT CookieSameSite
StringToCookieSameSite(const std::string& same_site,
                       CookieSameSiteString* samesite_string = nullptr);
~~~
 Converts the Set-Cookie header SameSite token |same_site| to a
 CookieSameSite. Defaults to CookieSameSite::UNSPECIFIED for empty or
 unrecognized strings. Returns an appropriate value of CookieSameSiteString in
 |samesite_string| to indicate what type of string was parsed as the SameSite
 attribute value, if a pointer is provided.

### RecordCookieSameSiteAttributeValueHistogram

RecordCookieSameSiteAttributeValueHistogram
~~~cpp
NET_EXPORT void RecordCookieSameSiteAttributeValueHistogram(
    CookieSameSiteString value);
~~~

### ReducePortRangeForCookieHistogram

ReducePortRangeForCookieHistogram
~~~cpp
NET_EXPORT CookiePort ReducePortRangeForCookieHistogram(const int port);
~~~
 This function reduces the 65535 available TCP port values down to a <100
 potentially interesting values that cookies could be set by or sent to. This
 is because UMA cannot handle the full range.

### GetSchemeNameEnum

GetSchemeNameEnum
~~~cpp
CookieSourceSchemeName GetSchemeNameEnum(const GURL& url);
~~~
 Returns the appropriate enum value for the scheme of the given GURL.

