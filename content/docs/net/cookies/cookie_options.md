### CookieOptions

CookieOptions
~~~cpp
CookieOptions(const CookieOptions& other)
~~~

### ~CookieOptions

~CookieOptions
~~~cpp
~CookieOptions()
~~~

### operator=

operator=
~~~cpp
CookieOptions& operator=(const CookieOptions&);
~~~

### operator=

operator=
~~~cpp
CookieOptions& operator=(CookieOptions&&);
~~~

### set_exclude_httponly

set_exclude_httponly
~~~cpp
void set_exclude_httponly() { exclude_httponly_ = true; }
~~~

### set_include_httponly

set_include_httponly
~~~cpp
void set_include_httponly() { exclude_httponly_ = false; }
~~~

### exclude_httponly

exclude_httponly
~~~cpp
bool exclude_httponly() const { return exclude_httponly_; }
~~~

### set_same_site_cookie_context

set_same_site_cookie_context
~~~cpp
void set_same_site_cookie_context(const SameSiteCookieContext& context) {
    same_site_cookie_context_ = context;
  }
~~~
 How trusted is the current browser environment when it comes to accessing
 SameSite cookies. Default is not trusted, e.g. CROSS_SITE.

### same_site_cookie_context

same_site_cookie_context
~~~cpp
const SameSiteCookieContext& same_site_cookie_context() const {
    return same_site_cookie_context_;
  }
~~~

### set_update_access_time

set_update_access_time
~~~cpp
void set_update_access_time() { update_access_time_ = true; }
~~~

### set_do_not_update_access_time

set_do_not_update_access_time
~~~cpp
void set_do_not_update_access_time() { update_access_time_ = false; }
~~~

### update_access_time

update_access_time
~~~cpp
bool update_access_time() const { return update_access_time_; }
~~~

### set_return_excluded_cookies

set_return_excluded_cookies
~~~cpp
void set_return_excluded_cookies() { return_excluded_cookies_ = true; }
~~~

### unset_return_excluded_cookies

unset_return_excluded_cookies
~~~cpp
void unset_return_excluded_cookies() { return_excluded_cookies_ = false; }
~~~

### return_excluded_cookies

return_excluded_cookies
~~~cpp
bool return_excluded_cookies() const { return return_excluded_cookies_; }
~~~

### set_same_party_context

set_same_party_context
~~~cpp
void set_same_party_context(const SamePartyContext& context) {
    same_party_context_ = context;
  }
~~~

### same_party_context

same_party_context
~~~cpp
const SamePartyContext& same_party_context() const {
    return same_party_context_;
  }
~~~

### set_full_party_context_size

set_full_party_context_size
~~~cpp
void set_full_party_context_size(uint32_t len) {
    full_party_context_size_ = len;
  }
~~~
 Getter/setter of |full_party_context_size_| for logging purposes.

### full_party_context_size

full_party_context_size
~~~cpp
uint32_t full_party_context_size() const { return full_party_context_size_; }
~~~

### set_is_in_nontrivial_first_party_set

set_is_in_nontrivial_first_party_set
~~~cpp
void set_is_in_nontrivial_first_party_set(bool is_member) {
    is_in_nontrivial_first_party_set_ = is_member;
  }
~~~

### is_in_nontrivial_first_party_set

is_in_nontrivial_first_party_set
~~~cpp
bool is_in_nontrivial_first_party_set() const {
    return is_in_nontrivial_first_party_set_;
  }
~~~

### MakeAllInclusive

MakeAllInclusive
~~~cpp
static CookieOptions MakeAllInclusive();
~~~
 Convenience method for where you need a CookieOptions that will
 work for getting/setting all types of cookies, including HttpOnly and
 SameSite cookies. Also specifies not to update the access time, because
 usually this is done to get all the cookies to check that they are correct,
 including the creation time. This basically makes a CookieOptions that is
 the opposite of the default CookieOptions.

### operator==

operator==
~~~cpp
NET_EXPORT bool operator==(
    const CookieOptions::SameSiteCookieContext::ContextMetadata& lhs,
    const CookieOptions::SameSiteCookieContext::ContextMetadata& rhs);
~~~

### operator!=

operator!=
~~~cpp
NET_EXPORT bool operator!=(
    const CookieOptions::SameSiteCookieContext::ContextMetadata& lhs,
    const CookieOptions::SameSiteCookieContext::ContextMetadata& rhs);
~~~

### PrintTo

PrintTo
~~~cpp
inline void PrintTo(CookieOptions::SameSiteCookieContext::ContextType ct,
                    std::ostream* os) {
  *os << static_cast<int>(ct);
}
~~~
 Allows gtest to print more helpful error messages instead of printing hex.

 (No need to null-check `os` because we can assume gtest will properly pass a
 non-null pointer, and it is dereferenced immediately anyway.)
### PrintTo

PrintTo
~~~cpp
inline void PrintTo(
    const CookieOptions::SameSiteCookieContext::ContextMetadata& m,
    std::ostream* os) {
  *os << "{";
  *os << " cross_site_redirect_downgrade: "
      << static_cast<int>(m.cross_site_redirect_downgrade);
  *os << ", redirect_type_bug_1221316: "
      << static_cast<int>(m.redirect_type_bug_1221316);
  *os << ", http_method_bug_1221316: "
      << static_cast<int>(m.http_method_bug_1221316);
  *os << " }";
}
~~~

### PrintTo

PrintTo
~~~cpp
inline void PrintTo(const CookieOptions::SameSiteCookieContext& sscc,
                    std::ostream* os) {
  *os << "{ context: ";
  PrintTo(sscc.context(), os);
  *os << ", schemeful_context: ";
  PrintTo(sscc.schemeful_context(), os);
  *os << ", metadata: ";
  PrintTo(sscc.metadata(), os);
  *os << ", schemeful_metadata: ";
  PrintTo(sscc.schemeful_metadata(), os);
  *os << " }";
}
~~~

## class CookieOptions

### SameSiteCookieContext

SameSiteCookieContext
~~~cpp
SameSiteCookieContext()
        : SameSiteCookieContext(ContextType::CROSS_SITE,
                                ContextType::CROSS_SITE) {}
~~~
 The following three constructors apply default values for the metadata
 members.

### SameSiteCookieContext

SameSiteCookieContext
~~~cpp
explicit SameSiteCookieContext(ContextType same_site_context)
        : SameSiteCookieContext(same_site_context, same_site_context) {}
~~~

### SameSiteCookieContext

SameSiteCookieContext
~~~cpp
SameSiteCookieContext(ContextType same_site_context,
                          ContextType schemeful_same_site_context)
        : SameSiteCookieContext(same_site_context,
                                schemeful_same_site_context,
                                ContextMetadata(),
                                ContextMetadata()) {}
~~~

### SameSiteCookieContext

SameSiteCookieContext
~~~cpp
SameSiteCookieContext(ContextType same_site_context,
                          ContextType schemeful_same_site_context,
                          ContextMetadata metadata,
                          ContextMetadata schemeful_metadata)
        : context_(same_site_context),
          schemeful_context_(schemeful_same_site_context),
          metadata_(metadata),
          schemeful_metadata_(schemeful_metadata) {
      DCHECK_LE(schemeful_context_, context_);
    }
~~~
 Schemeful and schemeless context types are consistency-checked against
 each other, but the metadata is stored as-is (i.e. the values in
 `metadata` and `schemeful_metadata` may be logically inconsistent), as
 the metadata is not relied upon for correctness.

### MakeInclusive

CookieOptions::MakeInclusive
~~~cpp
static SameSiteCookieContext MakeInclusive();
~~~
 Convenience method which returns a SameSiteCookieContext with the most
 inclusive contexts. This allows access to all SameSite cookies.

### MakeInclusiveForSet

CookieOptions::MakeInclusiveForSet
~~~cpp
static SameSiteCookieContext MakeInclusiveForSet();
~~~
 Convenience method which returns a SameSiteCookieContext with the most
 inclusive contexts for set. This allows setting all SameSite cookies.

### GetContextForCookieInclusion

CookieOptions::GetContextForCookieInclusion
~~~cpp
ContextType GetContextForCookieInclusion() const;
~~~
 Returns the context for determining SameSite cookie inclusion.

### GetMetadataForCurrentSchemefulMode

CookieOptions::GetMetadataForCurrentSchemefulMode
~~~cpp
const ContextMetadata& GetMetadataForCurrentSchemefulMode() const;
~~~
 Returns the metadata describing how this context was calculated, under
 the currently applicable schemeful/schemeless mode.

 TODO(chlily): Should take the CookieAccessSemantics as well, to
 accurately account for the context actually used for a given cookie.

### context

context
~~~cpp
ContextType context() const { return context_; }
~~~
 If you're just trying to determine if a cookie is accessible you likely
 want to use GetContextForCookieInclusion() which will return the correct
 context regardless the status of same-site features.

### schemeful_context

schemeful_context
~~~cpp
ContextType schemeful_context() const { return schemeful_context_; }
~~~

### metadata

metadata
~~~cpp
const ContextMetadata& metadata() const { return metadata_; }
~~~
 You probably want to use GetMetadataForCurrentSchemefulMode() instead of
 these getters, since that takes into account the applicable schemeful
 mode.

### metadata

metadata
~~~cpp
ContextMetadata& metadata() { return metadata_; }
~~~

### schemeful_metadata

schemeful_metadata
~~~cpp
const ContextMetadata& schemeful_metadata() const {
      return schemeful_metadata_;
    }
~~~

### schemeful_metadata

schemeful_metadata
~~~cpp
ContextMetadata& schemeful_metadata() { return schemeful_metadata_; }
~~~

### SetContextTypesForTesting

CookieOptions::SetContextTypesForTesting
~~~cpp
void SetContextTypesForTesting(ContextType context_type,
                                   ContextType schemeful_context_type);
~~~
 Sets context types. Does not check for consistency between context and
 schemeful context. Does not touch the metadata.

### CompleteEquivalenceForTesting

CookieOptions::CompleteEquivalenceForTesting
~~~cpp
bool CompleteEquivalenceForTesting(
        const SameSiteCookieContext& other) const;
~~~
 Returns whether the context types and all fields of the metadata structs
 are the same.

### operator==

CookieOptions::operator==
~~~cpp
NET_EXPORT friend bool operator==(
        const CookieOptions::SameSiteCookieContext& lhs,
        const CookieOptions::SameSiteCookieContext& rhs);
~~~
 Equality operators disregard any metadata! (Only the context types are
 compared, not how they were computed.)
### operator!=

CookieOptions::operator!=
~~~cpp
NET_EXPORT friend bool operator!=(
        const CookieOptions::SameSiteCookieContext& lhs,
        const CookieOptions::SameSiteCookieContext& rhs);
~~~
