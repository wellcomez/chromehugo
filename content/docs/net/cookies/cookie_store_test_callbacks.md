
## class CookieCallback
 Defines common behaviour for the callbacks from GetCookies, SetCookies, etc.

 Asserts that the current thread is the expected invocation thread, sends a
 quit to the thread in which it was constructed.

### WaitUntilDone

CookieCallback::WaitUntilDone
~~~cpp
void WaitUntilDone();
~~~
 Waits until the callback is invoked.

### was_run

CookieCallback::was_run
~~~cpp
bool was_run() const;
~~~
 Returns whether the callback was invoked. Should only be used on the thread
 the callback runs on.

### CookieCallback

CookieCallback::CookieCallback
~~~cpp
explicit CookieCallback(base::Thread* run_in_thread);
~~~
 Constructs a callback that expects to be called in the given thread.

### CookieCallback

CookieCallback::CookieCallback
~~~cpp
CookieCallback();
~~~
 Constructs a callback that expects to be called in current thread and will
 send a QUIT to the constructing thread.

### ~CookieCallback

CookieCallback::~CookieCallback
~~~cpp
~CookieCallback();
~~~

### CallbackEpilogue

CookieCallback::CallbackEpilogue
~~~cpp
void CallbackEpilogue();
~~~
 Tests whether the current thread was the caller's thread.

 Sends a QUIT to the constructing thread.

### ValidateThread

CookieCallback::ValidateThread
~~~cpp
void ValidateThread() const;
~~~

### run_in_thread_



~~~cpp

raw_ptr<base::Thread> run_in_thread_;

~~~


### run_in_task_runner_



~~~cpp

scoped_refptr<base::SingleThreadTaskRunner> run_in_task_runner_;

~~~


### loop_to_quit_



~~~cpp

base::RunLoop loop_to_quit_;

~~~


### was_run_



~~~cpp

bool was_run_ = false;

~~~


### WaitUntilDone

CookieCallback::WaitUntilDone
~~~cpp
void WaitUntilDone();
~~~
 Waits until the callback is invoked.

### was_run

CookieCallback::was_run
~~~cpp
bool was_run() const;
~~~
 Returns whether the callback was invoked. Should only be used on the thread
 the callback runs on.

### CallbackEpilogue

CookieCallback::CallbackEpilogue
~~~cpp
void CallbackEpilogue();
~~~
 Tests whether the current thread was the caller's thread.

 Sends a QUIT to the constructing thread.

### ValidateThread

CookieCallback::ValidateThread
~~~cpp
void ValidateThread() const;
~~~

### run_in_thread_



~~~cpp

raw_ptr<base::Thread> run_in_thread_;

~~~


### run_in_task_runner_



~~~cpp

scoped_refptr<base::SingleThreadTaskRunner> run_in_task_runner_;

~~~


### loop_to_quit_



~~~cpp

base::RunLoop loop_to_quit_;

~~~


### was_run_



~~~cpp

bool was_run_ = false;

~~~


## class ResultSavingCookieCallback

### ResultSavingCookieCallback

ResultSavingCookieCallback
~~~cpp
ResultSavingCookieCallback() = default;
~~~

### ResultSavingCookieCallback

ResultSavingCookieCallback
~~~cpp
explicit ResultSavingCookieCallback(base::Thread* run_in_thread)
      : CookieCallback(run_in_thread) {
  }
~~~

### Run

Run
~~~cpp
void Run(T result) {
    result_ = result;
    CallbackEpilogue();
  }
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(T)> MakeCallback() {
    return base::BindOnce(&ResultSavingCookieCallback<T>::Run,
                          base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### result

result
~~~cpp
const T& result() { return result_; }
~~~

### result_



~~~cpp

T result_;

~~~


### ResultSavingCookieCallback

ResultSavingCookieCallback
~~~cpp
ResultSavingCookieCallback() = default;
~~~

### ResultSavingCookieCallback

ResultSavingCookieCallback
~~~cpp
explicit ResultSavingCookieCallback(base::Thread* run_in_thread)
      : CookieCallback(run_in_thread) {
  }
~~~

### Run

Run
~~~cpp
void Run(T result) {
    result_ = result;
    CallbackEpilogue();
  }
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(T)> MakeCallback() {
    return base::BindOnce(&ResultSavingCookieCallback<T>::Run,
                          base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### result

result
~~~cpp
const T& result() { return result_; }
~~~

### result_



~~~cpp

T result_;

~~~


## class NoResultCookieCallback

### NoResultCookieCallback

NoResultCookieCallback::NoResultCookieCallback
~~~cpp
NoResultCookieCallback();
~~~

### NoResultCookieCallback

NoResultCookieCallback::NoResultCookieCallback
~~~cpp
explicit NoResultCookieCallback(base::Thread* run_in_thread);
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void()> MakeCallback() {
    return base::BindOnce(&NoResultCookieCallback::Run, base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### Run

Run
~~~cpp
void Run() {
    CallbackEpilogue();
  }
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void()> MakeCallback() {
    return base::BindOnce(&NoResultCookieCallback::Run, base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### Run

Run
~~~cpp
void Run() {
    CallbackEpilogue();
  }
~~~

## class GetCookieListCallback

### GetCookieListCallback

GetCookieListCallback::GetCookieListCallback
~~~cpp
GetCookieListCallback();
~~~

### GetCookieListCallback

GetCookieListCallback::GetCookieListCallback
~~~cpp
explicit GetCookieListCallback(base::Thread* run_in_thread);
~~~

### ~GetCookieListCallback

GetCookieListCallback::~GetCookieListCallback
~~~cpp
~GetCookieListCallback();
~~~

### Run

GetCookieListCallback::Run
~~~cpp
void Run(const CookieAccessResultList& cookies,
           const CookieAccessResultList& excluded_cookies);
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(const CookieAccessResultList&,
                          const CookieAccessResultList&)>
  MakeCallback() {
    return base::BindOnce(&GetCookieListCallback::Run, base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### cookies

cookies
~~~cpp
const CookieList& cookies() { return cookies_; }
~~~

### cookies_with_access_results

cookies_with_access_results
~~~cpp
const CookieAccessResultList& cookies_with_access_results() {
    return cookies_with_access_results_;
  }
~~~

### excluded_cookies

excluded_cookies
~~~cpp
const CookieAccessResultList& excluded_cookies() { return excluded_cookies_; }
~~~

### cookies_



~~~cpp

CookieList cookies_;

~~~


### cookies_with_access_results_



~~~cpp

CookieAccessResultList cookies_with_access_results_;

~~~


### excluded_cookies_



~~~cpp

CookieAccessResultList excluded_cookies_;

~~~


### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(const CookieAccessResultList&,
                          const CookieAccessResultList&)>
  MakeCallback() {
    return base::BindOnce(&GetCookieListCallback::Run, base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### cookies

cookies
~~~cpp
const CookieList& cookies() { return cookies_; }
~~~

### cookies_with_access_results

cookies_with_access_results
~~~cpp
const CookieAccessResultList& cookies_with_access_results() {
    return cookies_with_access_results_;
  }
~~~

### excluded_cookies

excluded_cookies
~~~cpp
const CookieAccessResultList& excluded_cookies() { return excluded_cookies_; }
~~~

### Run

GetCookieListCallback::Run
~~~cpp
void Run(const CookieAccessResultList& cookies,
           const CookieAccessResultList& excluded_cookies);
~~~

### cookies_



~~~cpp

CookieList cookies_;

~~~


### cookies_with_access_results_



~~~cpp

CookieAccessResultList cookies_with_access_results_;

~~~


### excluded_cookies_



~~~cpp

CookieAccessResultList excluded_cookies_;

~~~


## class GetAllCookiesCallback

### GetAllCookiesCallback

GetAllCookiesCallback::GetAllCookiesCallback
~~~cpp
GetAllCookiesCallback();
~~~

### GetAllCookiesCallback

GetAllCookiesCallback::GetAllCookiesCallback
~~~cpp
explicit GetAllCookiesCallback(base::Thread* run_in_thread);
~~~

### ~GetAllCookiesCallback

GetAllCookiesCallback::~GetAllCookiesCallback
~~~cpp
~GetAllCookiesCallback();
~~~

### Run

GetAllCookiesCallback::Run
~~~cpp
void Run(const CookieList& cookies);
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(const CookieList&)> MakeCallback() {
    return base::BindOnce(&GetAllCookiesCallback::Run, base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### cookies

cookies
~~~cpp
const CookieList& cookies() { return cookies_; }
~~~

### cookies_



~~~cpp

CookieList cookies_;

~~~


### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(const CookieList&)> MakeCallback() {
    return base::BindOnce(&GetAllCookiesCallback::Run, base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### cookies

cookies
~~~cpp
const CookieList& cookies() { return cookies_; }
~~~

### Run

GetAllCookiesCallback::Run
~~~cpp
void Run(const CookieList& cookies);
~~~

### cookies_



~~~cpp

CookieList cookies_;

~~~


## class GetAllCookiesWithAccessSemanticsCallback

### GetAllCookiesWithAccessSemanticsCallback

GetAllCookiesWithAccessSemanticsCallback::GetAllCookiesWithAccessSemanticsCallback
~~~cpp
GetAllCookiesWithAccessSemanticsCallback();
~~~

### GetAllCookiesWithAccessSemanticsCallback

GetAllCookiesWithAccessSemanticsCallback::GetAllCookiesWithAccessSemanticsCallback
~~~cpp
explicit GetAllCookiesWithAccessSemanticsCallback(
      base::Thread* run_in_thread);
~~~

### ~GetAllCookiesWithAccessSemanticsCallback

GetAllCookiesWithAccessSemanticsCallback::~GetAllCookiesWithAccessSemanticsCallback
~~~cpp
~GetAllCookiesWithAccessSemanticsCallback();
~~~

### Run

GetAllCookiesWithAccessSemanticsCallback::Run
~~~cpp
void Run(const CookieList& cookies,
           const std::vector<CookieAccessSemantics>& access_semantics_list);
~~~

### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(const CookieList&,
                          const std::vector<CookieAccessSemantics>&)>
  MakeCallback() {
    return base::BindOnce(&GetAllCookiesWithAccessSemanticsCallback::Run,
                          base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### cookies

cookies
~~~cpp
const CookieList& cookies() { return cookies_; }
~~~

### access_semantics_list

access_semantics_list
~~~cpp
const std::vector<CookieAccessSemantics>& access_semantics_list() {
    return access_semantics_list_;
  }
~~~

### cookies_



~~~cpp

CookieList cookies_;

~~~


### access_semantics_list_



~~~cpp

std::vector<CookieAccessSemantics> access_semantics_list_;

~~~


### MakeCallback

MakeCallback
~~~cpp
base::OnceCallback<void(const CookieList&,
                          const std::vector<CookieAccessSemantics>&)>
  MakeCallback() {
    return base::BindOnce(&GetAllCookiesWithAccessSemanticsCallback::Run,
                          base::Unretained(this));
  }
~~~
 Makes a callback that will invoke Run. Assumes that |this| will be kept
 alive till the time the callback is used.

### cookies

cookies
~~~cpp
const CookieList& cookies() { return cookies_; }
~~~

### access_semantics_list

access_semantics_list
~~~cpp
const std::vector<CookieAccessSemantics>& access_semantics_list() {
    return access_semantics_list_;
  }
~~~

### Run

GetAllCookiesWithAccessSemanticsCallback::Run
~~~cpp
void Run(const CookieList& cookies,
           const std::vector<CookieAccessSemantics>& access_semantics_list);
~~~

### cookies_



~~~cpp

CookieList cookies_;

~~~


### access_semantics_list_



~~~cpp

std::vector<CookieAccessSemantics> access_semantics_list_;

~~~

