### NetLogCookieMonsterConstructorParams

NetLogCookieMonsterConstructorParams
~~~cpp
base::Value::Dict NetLogCookieMonsterConstructorParams(bool persistent_store);
~~~
 Returns a Value containing NetLog parameters for constructing
 a CookieMonster.

### NetLogCookieMonsterCookieAdded

NetLogCookieMonsterCookieAdded
~~~cpp
base::Value::Dict NetLogCookieMonsterCookieAdded(
    const CanonicalCookie* cookie,
    bool sync_requested,
    NetLogCaptureMode capture_mode);
~~~
 Returns a Value containing NetLog parameters for adding a cookie.

### NetLogCookieMonsterCookieDeleted

NetLogCookieMonsterCookieDeleted
~~~cpp
base::Value::Dict NetLogCookieMonsterCookieDeleted(
    const CanonicalCookie* cookie,
    CookieChangeCause cause,
    bool sync_requested,
    NetLogCaptureMode capture_mode);
~~~
 Returns a Value containing NetLog parameters for deleting a cookie.

### NetLogCookieMonsterCookieRejectedSecure

NetLogCookieMonsterCookieRejectedSecure
~~~cpp
base::Value::Dict NetLogCookieMonsterCookieRejectedSecure(
    const CanonicalCookie* old_cookie,
    const CanonicalCookie* new_cookie,
    NetLogCaptureMode capture_mode);
~~~
 Returns a Value containing NetLog parameters for when a cookie addition
 is rejected because of a conflict with a secure cookie.

### NetLogCookieMonsterCookieRejectedHttponly

NetLogCookieMonsterCookieRejectedHttponly
~~~cpp
base::Value::Dict NetLogCookieMonsterCookieRejectedHttponly(
    const CanonicalCookie* old_cookie,
    const CanonicalCookie* new_cookie,
    NetLogCaptureMode capture_mode);
~~~
 Returns a Value containing NetLog parameters for when a cookie addition
 is rejected because of a conflict with an httponly cookie.

### NetLogCookieMonsterCookiePreservedSkippedSecure

NetLogCookieMonsterCookiePreservedSkippedSecure
~~~cpp
base::Value::Dict NetLogCookieMonsterCookiePreservedSkippedSecure(
    const CanonicalCookie* skipped_secure,
    const CanonicalCookie* preserved,
    const CanonicalCookie* new_cookie,
    NetLogCaptureMode capture_mode);
~~~
 Returns a Value containing NetLog parameters for when, upon an attempted
 cookie addition which is rejected due to a conflict with a secure cookie, a
 pre-existing cookie would have been deleted but is instead preserved because
 the addition failed.

