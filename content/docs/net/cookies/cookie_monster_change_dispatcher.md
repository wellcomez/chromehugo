
## class CookieMonsterChangeDispatcher
 CookieChangeDispatcher implementation used by CookieMonster.

### CookieMonsterChangeDispatcher

CookieMonsterChangeDispatcher::CookieMonsterChangeDispatcher
~~~cpp
CookieMonsterChangeDispatcher(const CookieMonster* cookie_monster,
                                bool same_party_attribute_enabled);
~~~
 Expects |cookie_monster| to outlive this.

### CookieMonsterChangeDispatcher

CookieMonsterChangeDispatcher
~~~cpp
CookieMonsterChangeDispatcher(const CookieMonsterChangeDispatcher&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieMonsterChangeDispatcher& operator=(
      const CookieMonsterChangeDispatcher&) = delete;
~~~

### ~CookieMonsterChangeDispatcher

CookieMonsterChangeDispatcher::~CookieMonsterChangeDispatcher
~~~cpp
~CookieMonsterChangeDispatcher() override;
~~~

### NameKey

CookieMonsterChangeDispatcher::NameKey
~~~cpp
static std::string NameKey(std::string name);
~~~
 The key in CookieNameMap for a cookie name.

### DomainKey

CookieMonsterChangeDispatcher::DomainKey
~~~cpp
static std::string DomainKey(const std::string& domain);
~~~
 The key in CookieDomainName for a cookie domain.

### DomainKey

CookieMonsterChangeDispatcher::DomainKey
~~~cpp
static std::string DomainKey(const GURL& url);
~~~
 The key in CookieDomainName for a listener URL.

### AddCallbackForCookie

CookieMonsterChangeDispatcher::AddCallbackForCookie
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForCookie(
      const GURL& url,
      const std::string& name,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~
 net::CookieChangeDispatcher
### AddCallbackForUrl

CookieMonsterChangeDispatcher::AddCallbackForUrl
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForUrl(
      const GURL& url,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~

### AddCallbackForAllChanges

CookieMonsterChangeDispatcher::AddCallbackForAllChanges
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription>
  AddCallbackForAllChanges(CookieChangeCallback callback) override;
~~~

### DispatchChange

CookieMonsterChangeDispatcher::DispatchChange
~~~cpp
void DispatchChange(const CookieChangeInfo& change, bool notify_global_hooks);
~~~
 |notify_global_hooks| is true if the function should run the
 global hooks in addition to the per-cookie hooks.


 TODO(pwnall): Remove |notify_global_hooks| and fix consumers.

###  Subscription


~~~cpp
class Subscription : public base::LinkNode<Subscription>,
                       public CookieChangeSubscription {
   public:
    Subscription(base::WeakPtr<CookieMonsterChangeDispatcher> change_dispatcher,
                 std::string domain_key,
                 std::string name_key,
                 GURL url,
                 absl::optional<CookiePartitionKey> cookie_partition_key,
                 bool same_party_attribute_enabled,
                 net::CookieChangeCallback callback);

    Subscription(const Subscription&) = delete;
    Subscription& operator=(const Subscription&) = delete;

    ~Subscription() override;

    // The lookup key used in the domain subscription map.
    //
    // The empty string means no domain filtering.
    const std::string& domain_key() const { return domain_key_; }
    // The lookup key used in the name subscription map.
    //
    // The empty string means no name filtering.
    const std::string& name_key() const { return name_key_; }

    // Dispatches a cookie change notification if the listener is interested.
    void DispatchChange(const CookieChangeInfo& change,
                        const CookieAccessDelegate* cookie_access_delegate);

   private:
    base::WeakPtr<CookieMonsterChangeDispatcher> change_dispatcher_;
    const std::string domain_key_;  // kGlobalDomainKey means no filtering.
    const std::string name_key_;    // kGlobalNameKey means no filtering.
    const GURL url_;                // empty() means no URL-based filtering.
    // nullopt means all Partitioned cookies will be ignored.
    const absl::optional<CookiePartitionKey> cookie_partition_key_;
    const net::CookieChangeCallback callback_;
    bool same_party_attribute_enabled_;

    void DoDispatchChange(const CookieChangeInfo& change) const;

    // Used to post DoDispatchChange() calls to this subscription's thread.
    scoped_refptr<base::SingleThreadTaskRunner> task_runner_;

    THREAD_CHECKER(thread_checker_);

    // Used to cancel delayed calls to DoDispatchChange() when the subscription
    // gets destroyed.
    base::WeakPtrFactory<Subscription> weak_ptr_factory_{this};
  };
~~~
### DispatchChangeToDomainKey

CookieMonsterChangeDispatcher::DispatchChangeToDomainKey
~~~cpp
void DispatchChangeToDomainKey(const CookieChangeInfo& change,
                                 const std::string& domain_key);
~~~

### DispatchChangeToNameKey

CookieMonsterChangeDispatcher::DispatchChangeToNameKey
~~~cpp
void DispatchChangeToNameKey(const CookieChangeInfo& change,
                               CookieNameMap& name_map,
                               const std::string& name_key);
~~~

### LinkSubscription

CookieMonsterChangeDispatcher::LinkSubscription
~~~cpp
void LinkSubscription(Subscription* subscription);
~~~
 Inserts a subscription into the map.


 Called by the AddCallback* methods, after creating the Subscription.

### UnlinkSubscription

CookieMonsterChangeDispatcher::UnlinkSubscription
~~~cpp
void UnlinkSubscription(Subscription* subscription);
~~~
 Removes a subscription from the map.


 Called by the Subscription destructor.

### cookie_monster_



~~~cpp

raw_ptr<const CookieMonster> cookie_monster_;

~~~


### cookie_domain_map_



~~~cpp

CookieDomainMap cookie_domain_map_;

~~~


### same_party_attribute_enabled_



~~~cpp

const bool same_party_attribute_enabled_;

~~~


### THREAD_CHECKER

CookieMonsterChangeDispatcher::THREAD_CHECKER
~~~cpp
THREAD_CHECKER(thread_checker_);
~~~

### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<CookieMonsterChangeDispatcher> weak_ptr_factory_{this};

~~~

 Vends weak pointers to subscriptions.

### CookieMonsterChangeDispatcher

CookieMonsterChangeDispatcher
~~~cpp
CookieMonsterChangeDispatcher(const CookieMonsterChangeDispatcher&) = delete;
~~~

### operator=

operator=
~~~cpp
CookieMonsterChangeDispatcher& operator=(
      const CookieMonsterChangeDispatcher&) = delete;
~~~

### NameKey

CookieMonsterChangeDispatcher::NameKey
~~~cpp
static std::string NameKey(std::string name);
~~~
 The key in CookieNameMap for a cookie name.

### DomainKey

CookieMonsterChangeDispatcher::DomainKey
~~~cpp
static std::string DomainKey(const std::string& domain);
~~~
 The key in CookieDomainName for a cookie domain.

### DomainKey

CookieMonsterChangeDispatcher::DomainKey
~~~cpp
static std::string DomainKey(const GURL& url);
~~~
 The key in CookieDomainName for a listener URL.

### AddCallbackForCookie

CookieMonsterChangeDispatcher::AddCallbackForCookie
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForCookie(
      const GURL& url,
      const std::string& name,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~
 net::CookieChangeDispatcher
### AddCallbackForUrl

CookieMonsterChangeDispatcher::AddCallbackForUrl
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForUrl(
      const GURL& url,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~

### AddCallbackForAllChanges

CookieMonsterChangeDispatcher::AddCallbackForAllChanges
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription>
  AddCallbackForAllChanges(CookieChangeCallback callback) override;
~~~

### DispatchChange

CookieMonsterChangeDispatcher::DispatchChange
~~~cpp
void DispatchChange(const CookieChangeInfo& change, bool notify_global_hooks);
~~~
 |notify_global_hooks| is true if the function should run the
 global hooks in addition to the per-cookie hooks.


 TODO(pwnall): Remove |notify_global_hooks| and fix consumers.

###  Subscription


~~~cpp
class Subscription : public base::LinkNode<Subscription>,
                       public CookieChangeSubscription {
   public:
    Subscription(base::WeakPtr<CookieMonsterChangeDispatcher> change_dispatcher,
                 std::string domain_key,
                 std::string name_key,
                 GURL url,
                 absl::optional<CookiePartitionKey> cookie_partition_key,
                 bool same_party_attribute_enabled,
                 net::CookieChangeCallback callback);

    Subscription(const Subscription&) = delete;
    Subscription& operator=(const Subscription&) = delete;

    ~Subscription() override;

    // The lookup key used in the domain subscription map.
    //
    // The empty string means no domain filtering.
    const std::string& domain_key() const { return domain_key_; }
    // The lookup key used in the name subscription map.
    //
    // The empty string means no name filtering.
    const std::string& name_key() const { return name_key_; }

    // Dispatches a cookie change notification if the listener is interested.
    void DispatchChange(const CookieChangeInfo& change,
                        const CookieAccessDelegate* cookie_access_delegate);

   private:
    base::WeakPtr<CookieMonsterChangeDispatcher> change_dispatcher_;
    const std::string domain_key_;  // kGlobalDomainKey means no filtering.
    const std::string name_key_;    // kGlobalNameKey means no filtering.
    const GURL url_;                // empty() means no URL-based filtering.
    // nullopt means all Partitioned cookies will be ignored.
    const absl::optional<CookiePartitionKey> cookie_partition_key_;
    const net::CookieChangeCallback callback_;
    bool same_party_attribute_enabled_;

    void DoDispatchChange(const CookieChangeInfo& change) const;

    // Used to post DoDispatchChange() calls to this subscription's thread.
    scoped_refptr<base::SingleThreadTaskRunner> task_runner_;

    THREAD_CHECKER(thread_checker_);

    // Used to cancel delayed calls to DoDispatchChange() when the subscription
    // gets destroyed.
    base::WeakPtrFactory<Subscription> weak_ptr_factory_{this};
  };
~~~
### DispatchChangeToDomainKey

CookieMonsterChangeDispatcher::DispatchChangeToDomainKey
~~~cpp
void DispatchChangeToDomainKey(const CookieChangeInfo& change,
                                 const std::string& domain_key);
~~~

### DispatchChangeToNameKey

CookieMonsterChangeDispatcher::DispatchChangeToNameKey
~~~cpp
void DispatchChangeToNameKey(const CookieChangeInfo& change,
                               CookieNameMap& name_map,
                               const std::string& name_key);
~~~

### LinkSubscription

CookieMonsterChangeDispatcher::LinkSubscription
~~~cpp
void LinkSubscription(Subscription* subscription);
~~~
 Inserts a subscription into the map.


 Called by the AddCallback* methods, after creating the Subscription.

### UnlinkSubscription

CookieMonsterChangeDispatcher::UnlinkSubscription
~~~cpp
void UnlinkSubscription(Subscription* subscription);
~~~
 Removes a subscription from the map.


 Called by the Subscription destructor.

### cookie_monster_



~~~cpp

raw_ptr<const CookieMonster> cookie_monster_;

~~~


### cookie_domain_map_



~~~cpp

CookieDomainMap cookie_domain_map_;

~~~


### same_party_attribute_enabled_



~~~cpp

const bool same_party_attribute_enabled_;

~~~


### weak_ptr_factory_



~~~cpp

base::WeakPtrFactory<CookieMonsterChangeDispatcher> weak_ptr_factory_{this};

~~~

 Vends weak pointers to subscriptions.
