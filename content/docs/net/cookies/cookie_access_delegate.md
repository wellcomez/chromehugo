
## class CookieAccessDelegate

### CookieAccessDelegate

CookieAccessDelegate
~~~cpp
CookieAccessDelegate(const CookieAccessDelegate&) = delete;
~~~

### operator=

CookieAccessDelegate::operator=
~~~cpp
CookieAccessDelegate& operator=(const CookieAccessDelegate&) = delete;
~~~

### ~CookieAccessDelegate

CookieAccessDelegate::~CookieAccessDelegate
~~~cpp
~CookieAccessDelegate()
~~~

### ShouldTreatUrlAsTrustworthy

CookieAccessDelegate::ShouldTreatUrlAsTrustworthy
~~~cpp
virtual bool ShouldTreatUrlAsTrustworthy(const GURL& url) const;
~~~
 Returns true if the passed in |url| should be permitted to access secure
 cookies in addition to URLs that normally do so. Returning false from this
 method on a URL that would already be treated as secure by default, e.g. an
 https:// one has no effect.

### GetAccessSemantics

CookieAccessDelegate::GetAccessSemantics
~~~cpp
virtual CookieAccessSemantics GetAccessSemantics(
      const CanonicalCookie& cookie) const = 0;
~~~
 Gets the access semantics to apply to |cookie|, based on its domain (i.e.,
 whether a policy specifies that legacy access semantics should apply).

### ShouldIgnoreSameSiteRestrictions

CookieAccessDelegate::ShouldIgnoreSameSiteRestrictions
~~~cpp
virtual bool ShouldIgnoreSameSiteRestrictions(
      const GURL& url,
      const SiteForCookies& site_for_cookies) const = 0;
~~~
 Returns whether a cookie should be attached regardless of its SameSite
 value vs the request context.

### ComputeFirstPartySetMetadataMaybeAsync

CookieAccessDelegate::ComputeFirstPartySetMetadataMaybeAsync
~~~cpp
[[nodiscard]] virtual absl::optional<FirstPartySetMetadata>
  ComputeFirstPartySetMetadataMaybeAsync(
      const net::SchemefulSite& site,
      const net::SchemefulSite* top_frame_site,
      const std::set<net::SchemefulSite>& party_context,
      base::OnceCallback<void(FirstPartySetMetadata)> callback) const = 0;
~~~
 Calls `callback` with metadata indicating whether `site` is same-party with
 `party_context` and `top_frame_site`; and `site`'s owner, if applicable..

 If `top_frame_site` is nullptr, then `site` will be checked only against
 `party_context`.


 This may return a result synchronously, or asynchronously invoke `callback`
 with the result. The callback will be invoked iff the return value is
 nullopt; i.e. a result will be provided via return value or callback, but
 not both, and not neither.

### FindFirstPartySetEntries

CookieAccessDelegate::FindFirstPartySetEntries
~~~cpp
[[nodiscard]] virtual absl::optional<
      base::flat_map<net::SchemefulSite, net::FirstPartySetEntry>>
  FindFirstPartySetEntries(
      const base::flat_set<net::SchemefulSite>& sites,
      base::OnceCallback<
          void(base::flat_map<net::SchemefulSite, net::FirstPartySetEntry>)>
          callback) const = 0;
~~~
 Returns the entries of a set of sites if the sites are in non-trivial sets.

 If a given site is not in a non-trivial set, the output does not contain a
 corresponding entry.


 This may return a result synchronously, or asynchronously invoke `callback`
 with the result. The callback will be invoked iff the return value is
 nullopt; i.e. a result will be provided via return value or callback, but
 not both, and not neither.
