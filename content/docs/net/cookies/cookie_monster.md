
## class CookieMonster
 The cookie monster is the system for storing and retrieving cookies. It has
 an in-memory list of all cookies, and synchronizes non-session cookies to an
 optional permanent storage that implements the PersistentCookieStore
 interface.


 Tasks may be deferred if all affected cookies are not yet loaded from the
 backing store. Otherwise, callbacks may be invoked immediately.


 A cookie task is either pending loading of the entire cookie store, or
 loading of cookies for a specific domain key (GetKey(), roughly eTLD+1). In
 the former case, the cookie callback will be queued in tasks_pending_ while
 PersistentCookieStore chain loads the cookie store on DB thread. In the
 latter case, the cookie callback will be queued in tasks_pending_for_key_
 while PermanentCookieStore loads cookies for the specified domain key on DB
 thread.

### CookieMonster

CookieMonster::CookieMonster
~~~cpp
CookieMonster(scoped_refptr<PersistentCookieStore> store,
                base::TimeDelta last_access_threshold,
                NetLog* net_log)
~~~

### CookieMonster

CookieMonster
~~~cpp
CookieMonster(const CookieMonster&) = delete;
~~~

### operator=

CookieMonster::operator=
~~~cpp
CookieMonster& operator=(const CookieMonster&) = delete;
~~~

### ~CookieMonster

CookieMonster::~CookieMonster
~~~cpp
~CookieMonster() override
~~~

### SetAllCookiesAsync

CookieMonster::SetAllCookiesAsync
~~~cpp
void SetAllCookiesAsync(const CookieList& list, SetCookiesCallback callback);
~~~
 Writes all the cookies in |list| into the store, replacing all cookies
 currently present in store.

 This method does not flush the backend.

 TODO(rdsmith, mmenke): Do not use this function; it is deprecated
 and should be removed.

 See https://codereview.chromium.org/2882063002/#msg64.

### SetCanonicalCookieAsync

CookieMonster::SetCanonicalCookieAsync
~~~cpp
void SetCanonicalCookieAsync(
      std::unique_ptr<CanonicalCookie> cookie,
      const GURL& source_url,
      const CookieOptions& options,
      SetCookiesCallback callback,
      absl::optional<CookieAccessResult> cookie_access_result =
          absl::nullopt) override;
~~~
 CookieStore implementation.

### GetCookieListWithOptionsAsync

CookieMonster::GetCookieListWithOptionsAsync
~~~cpp
void GetCookieListWithOptionsAsync(const GURL& url,
                                     const CookieOptions& options,
                                     const CookiePartitionKeyCollection& s,
                                     GetCookieListCallback callback) override;
~~~

### GetAllCookiesAsync

CookieMonster::GetAllCookiesAsync
~~~cpp
void GetAllCookiesAsync(GetAllCookiesCallback callback) override;
~~~

### GetAllCookiesWithAccessSemanticsAsync

CookieMonster::GetAllCookiesWithAccessSemanticsAsync
~~~cpp
void GetAllCookiesWithAccessSemanticsAsync(
      GetAllCookiesWithAccessSemanticsCallback callback) override;
~~~

### DeleteCanonicalCookieAsync

CookieMonster::DeleteCanonicalCookieAsync
~~~cpp
void DeleteCanonicalCookieAsync(const CanonicalCookie& cookie,
                                  DeleteCallback callback) override;
~~~

### DeleteAllCreatedInTimeRangeAsync

CookieMonster::DeleteAllCreatedInTimeRangeAsync
~~~cpp
void DeleteAllCreatedInTimeRangeAsync(
      const CookieDeletionInfo::TimeRange& creation_range,
      DeleteCallback callback) override;
~~~

### DeleteAllMatchingInfoAsync

CookieMonster::DeleteAllMatchingInfoAsync
~~~cpp
void DeleteAllMatchingInfoAsync(CookieDeletionInfo delete_info,
                                  DeleteCallback callback) override;
~~~

### DeleteSessionCookiesAsync

CookieMonster::DeleteSessionCookiesAsync
~~~cpp
void DeleteSessionCookiesAsync(DeleteCallback callback) override;
~~~

### DeleteMatchingCookiesAsync

CookieMonster::DeleteMatchingCookiesAsync
~~~cpp
void DeleteMatchingCookiesAsync(DeletePredicate predicate,
                                  DeleteCallback callback) override;
~~~

### FlushStore

CookieMonster::FlushStore
~~~cpp
void FlushStore(base::OnceClosure callback) override;
~~~

### SetForceKeepSessionState

CookieMonster::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### GetChangeDispatcher

CookieMonster::GetChangeDispatcher
~~~cpp
CookieChangeDispatcher& GetChangeDispatcher() override;
~~~

### SetCookieableSchemes

CookieMonster::SetCookieableSchemes
~~~cpp
void SetCookieableSchemes(const std::vector<std::string>& schemes,
                            SetCookieableSchemesCallback callback) override;
~~~

### SiteHasCookieInOtherPartition

CookieMonster::SiteHasCookieInOtherPartition
~~~cpp
absl::optional<bool> SiteHasCookieInOtherPartition(
      const net::SchemefulSite& site,
      const absl::optional<CookiePartitionKey>& partition_key) const override;
~~~

### SetPersistSessionCookies

CookieMonster::SetPersistSessionCookies
~~~cpp
void SetPersistSessionCookies(bool persist_session_cookies);
~~~
 Enables writing session cookies into the cookie database. If this this
 method is called, it must be called before first use of the instance
 (i.e. as part of the instance initialization process).

### GetKey

CookieMonster::GetKey
~~~cpp
static std::string GetKey(base::StringPiece domain);
~~~
 Find a key based on the given domain, which will be used to find all
 cookies potentially relevant to it. This is used for lookup in cookies_ as
 well as for PersistentCookieStore::LoadCookiesForKey. See comment on keys
 before the CookieMap typedef.

### CookieSorter

CookieMonster::CookieSorter
~~~cpp
static bool CookieSorter(const CanonicalCookie* cc1,
                           const CanonicalCookie* cc2);
~~~
 Exposes the comparison function used when sorting cookies.

### DoRecordPeriodicStatsForTesting

DoRecordPeriodicStatsForTesting
~~~cpp
bool DoRecordPeriodicStatsForTesting() { return DoRecordPeriodicStats(); }
~~~
 Triggers immediate recording of stats that are typically reported
 periodically.

### SetCanonicalCookie

CookieMonster::SetCanonicalCookie
~~~cpp
void SetCanonicalCookie(
      std::unique_ptr<CanonicalCookie> cookie,
      const GURL& source_url,
      const CookieOptions& options,
      SetCookiesCallback callback,
      absl::optional<CookieAccessResult> cookie_access_result = absl::nullopt);
~~~
 Sets a canonical cookie, deletes equivalents and performs garbage
 collection.  |source_url| indicates what URL the cookie is being set
 from; secure cookies cannot be altered from insecure schemes, and some
 schemes may not be authorized.


 |options| indicates if this setting operation is allowed
 to affect http_only or same-site cookies.


 |cookie_access_result| is an optional input status, to allow for status
 chaining from callers. It helps callers provide the status of a
 canonical cookie that may have warnings associated with it.

### GetAllCookies

CookieMonster::GetAllCookies
~~~cpp
void GetAllCookies(GetAllCookiesCallback callback);
~~~

### AttachAccessSemanticsListForCookieList

CookieMonster::AttachAccessSemanticsListForCookieList
~~~cpp
void AttachAccessSemanticsListForCookieList(
      GetAllCookiesWithAccessSemanticsCallback callback,
      const CookieList& cookie_list);
~~~

### GetCookieListWithOptions

CookieMonster::GetCookieListWithOptions
~~~cpp
void GetCookieListWithOptions(
      const GURL& url,
      const CookieOptions& options,
      const CookiePartitionKeyCollection& cookie_partition_key_collection,
      GetCookieListCallback callback);
~~~

### DeleteAllCreatedInTimeRange

CookieMonster::DeleteAllCreatedInTimeRange
~~~cpp
void DeleteAllCreatedInTimeRange(
      const CookieDeletionInfo::TimeRange& creation_range,
      DeleteCallback callback);
~~~

### MatchCookieDeletionInfo

CookieMonster::MatchCookieDeletionInfo
~~~cpp
bool MatchCookieDeletionInfo(const CookieDeletionInfo& delete_info,
                               const net::CanonicalCookie& cookie);
~~~
 Returns whether |cookie| matches |delete_info|.

### DeleteCanonicalCookie

CookieMonster::DeleteCanonicalCookie
~~~cpp
void DeleteCanonicalCookie(const CanonicalCookie& cookie,
                             DeleteCallback callback);
~~~

### DeleteMatchingCookies

CookieMonster::DeleteMatchingCookies
~~~cpp
void DeleteMatchingCookies(DeletePredicate predicate,
                             DeletionCause cause,
                             DeleteCallback callback);
~~~

### MarkCookieStoreAsInitialized

CookieMonster::MarkCookieStoreAsInitialized
~~~cpp
void MarkCookieStoreAsInitialized();
~~~
 The first access to the cookie store initializes it. This method should be
 called before any access to the cookie store.

### FetchAllCookiesIfNecessary

CookieMonster::FetchAllCookiesIfNecessary
~~~cpp
void FetchAllCookiesIfNecessary();
~~~
 Fetches all cookies if the backing store exists and they're not already
 being fetched.

### FetchAllCookies

CookieMonster::FetchAllCookies
~~~cpp
void FetchAllCookies();
~~~
 Fetches all cookies from the backing store.

### ShouldFetchAllCookiesWhenFetchingAnyCookie

CookieMonster::ShouldFetchAllCookiesWhenFetchingAnyCookie
~~~cpp
bool ShouldFetchAllCookiesWhenFetchingAnyCookie();
~~~
 Whether all cookies should be fetched as soon as any is requested.

### OnLoaded

CookieMonster::OnLoaded
~~~cpp
void OnLoaded(base::TimeTicks beginning_time,
                std::vector<std::unique_ptr<CanonicalCookie>> cookies);
~~~
 Stores cookies loaded from the backing store and invokes any deferred
 calls. |beginning_time| should be the moment PersistentCookieStore::Load
 was invoked and is used for reporting histogram_time_blocked_on_load_.

 See PersistentCookieStore::Load for details on the contents of cookies.

### OnKeyLoaded

CookieMonster::OnKeyLoaded
~~~cpp
void OnKeyLoaded(const std::string& key,
                   std::vector<std::unique_ptr<CanonicalCookie>> cookies);
~~~
 Stores cookies loaded from the backing store and invokes the deferred
 task(s) pending loading of cookies associated with the domain key
 (GetKey, roughly eTLD+1). Called when all cookies for the domain key have
 been loaded from DB. See PersistentCookieStore::Load for details on the
 contents of cookies.

### StoreLoadedCookies

CookieMonster::StoreLoadedCookies
~~~cpp
void StoreLoadedCookies(
      std::vector<std::unique_ptr<CanonicalCookie>> cookies);
~~~
 Stores the loaded cookies.

### InvokeQueue

CookieMonster::InvokeQueue
~~~cpp
void InvokeQueue();
~~~
 Invokes deferred calls.

### EnsureCookiesMapIsValid

CookieMonster::EnsureCookiesMapIsValid
~~~cpp
void EnsureCookiesMapIsValid();
~~~
 Checks that |cookies_| matches our invariants, and tries to repair any
 inconsistencies. (In other words, it does not have duplicate cookies).

### TrimDuplicateCookiesForKey

CookieMonster::TrimDuplicateCookiesForKey
~~~cpp
void TrimDuplicateCookiesForKey(
      const std::string& key,
      CookieMap::iterator begin,
      CookieMap::iterator end,
      absl::optional<PartitionedCookieMap::iterator> cookie_partition_it);
~~~
 Checks for any duplicate cookies for CookieMap key |key| which lie between
 |begin| and |end|. If any are found, all but the most recent are deleted.


 If |cookie_partition_it| is not nullopt, then this function trims cookies
 from the CookieMap in |partitioned_cookies_| at |cookie_partition_it|
 instead of trimming cookies from |cookies_|.

### SetDefaultCookieableSchemes

CookieMonster::SetDefaultCookieableSchemes
~~~cpp
void SetDefaultCookieableSchemes();
~~~

### FindCookiesForRegistryControlledHost

CookieMonster::FindCookiesForRegistryControlledHost
~~~cpp
std::vector<CanonicalCookie*> FindCookiesForRegistryControlledHost(
      const GURL& url,
      CookieMap* cookie_map = nullptr,
      PartitionedCookieMap::iterator* partition_it = nullptr);
~~~

### FindPartitionedCookiesForRegistryControlledHost

CookieMonster::FindPartitionedCookiesForRegistryControlledHost
~~~cpp
std::vector<CanonicalCookie*> FindPartitionedCookiesForRegistryControlledHost(
      const CookiePartitionKey& cookie_partition_key,
      const GURL& url);
~~~

### FilterCookiesWithOptions

CookieMonster::FilterCookiesWithOptions
~~~cpp
void FilterCookiesWithOptions(const GURL url,
                                const CookieOptions options,
                                std::vector<CanonicalCookie*>* cookie_ptrs,
                                CookieAccessResultList* included_cookies,
                                CookieAccessResultList* excluded_cookies);
~~~

### MaybeDeleteEquivalentCookieAndUpdateStatus

CookieMonster::MaybeDeleteEquivalentCookieAndUpdateStatus
~~~cpp
void MaybeDeleteEquivalentCookieAndUpdateStatus(
      const std::string& key,
      const CanonicalCookie& cookie_being_set,
      bool allowed_to_set_secure_cookie,
      bool skip_httponly,
      bool already_expired,
      base::Time* creation_date_to_inherit,
      CookieInclusionStatus* status,
      absl::optional<PartitionedCookieMap::iterator> cookie_partition_it);
~~~
 Possibly delete an existing cookie equivalent to |cookie_being_set| (same
 path, domain, and name).


 |allowed_to_set_secure_cookie| indicates if the source may override
 existing secure cookies. If the source is not trustworthy, and there is an
 existing "equivalent" cookie that is Secure, that cookie will be preserved,
 under "Leave Secure Cookies Alone" (see
 https://tools.ietf.org/html/draft-ietf-httpbis-cookie-alone-01).

 ("equivalent" here is in quotes because the equivalency check for the
 purposes of preserving existing Secure cookies is slightly more inclusive.)

 If |skip_httponly| is true, httponly cookies will not be deleted even if
 they are equivalent.

 |key| is the key to find the cookie in cookies_; see the comment before the
 CookieMap typedef for details.


 If a cookie is deleted, and its value matches |cookie_being_set|'s value,
 then |creation_date_to_inherit| will be set to that cookie's creation date.


 The cookie will not be deleted if |*status| is not "include" when calling
 the function. The function will update |*status| with exclusion reasons if
 a secure cookie was skipped or an httponly cookie was skipped.


 If |cookie_partition_it| is nullopt, it will search |cookies_| for
 duplicates of |cookie_being_set|. Otherwise, |cookie_partition_it|'s value
 is the iterator of the CookieMap in |partitioned_cookies_| we should search
 for duplicates.


 NOTE: There should never be more than a single matching equivalent cookie.

### InternalInsertCookie

CookieMonster::InternalInsertCookie
~~~cpp
CookieMap::iterator InternalInsertCookie(
      const std::string& key,
      std::unique_ptr<CanonicalCookie> cc,
      bool sync_to_store,
      const CookieAccessResult& access_result,
      bool dispatch_change = true);
~~~
 Inserts `cc` into cookies_. Returns an iterator that points to the inserted
 cookie in `cookies_`. Guarantee: all iterators to `cookies_` remain valid.

 Dispatches the change to `change_dispatcher_` iff `dispatch_change` is
 true.

### ShouldUpdatePersistentStore

CookieMonster::ShouldUpdatePersistentStore
~~~cpp
bool ShouldUpdatePersistentStore(CanonicalCookie* cc);
~~~
 Returns true if the cookie should be (or is already) synced to the store.

 Used for cookies during insertion and deletion into the in-memory store.

### LogCookieTypeToUMA

CookieMonster::LogCookieTypeToUMA
~~~cpp
void LogCookieTypeToUMA(CanonicalCookie* cc,
                          const CookieAccessResult& access_result);
~~~

### InternalInsertPartitionedCookie

CookieMonster::InternalInsertPartitionedCookie
~~~cpp
PartitionedCookieMapIterators InternalInsertPartitionedCookie(
      std::string key,
      std::unique_ptr<CanonicalCookie> cc,
      bool sync_to_store,
      const CookieAccessResult& access_result,
      bool dispatch_change = true);
~~~
 Inserts `cc` into partitioned_cookies_. Should only be used when
 cc->IsPartitioned() is true.

### SetAllCookies

CookieMonster::SetAllCookies
~~~cpp
void SetAllCookies(CookieList list, SetCookiesCallback callback);
~~~
 Sets all cookies from |list| after deleting any equivalent cookie.

 For data gathering purposes, this routine is treated as if it is
 restoring saved cookies; some statistics are not gathered in this case.

### InternalUpdateCookieAccessTime

CookieMonster::InternalUpdateCookieAccessTime
~~~cpp
void InternalUpdateCookieAccessTime(CanonicalCookie* cc,
                                      const base::Time& current_time);
~~~

### InternalDeleteCookie

CookieMonster::InternalDeleteCookie
~~~cpp
void InternalDeleteCookie(CookieMap::iterator it,
                            bool sync_to_store,
                            DeletionCause deletion_cause);
~~~
 |deletion_cause| argument is used for collecting statistics and choosing
 the correct CookieChangeCause for OnCookieChange notifications. Guarantee:
 All iterators to cookies_, except for the deleted entry, remain valid.

### InternalDeletePartitionedCookie

CookieMonster::InternalDeletePartitionedCookie
~~~cpp
void InternalDeletePartitionedCookie(
      PartitionedCookieMap::iterator partition_it,
      CookieMap::iterator cookie_it,
      bool sync_to_store,
      DeletionCause deletion_cause);
~~~
 Deletes a Partitioned cookie. Returns true if the deletion operation
 resulted in the CookieMap the cookie was stored in was deleted.


 If the CookieMap which contains the deleted cookie only has one entry, then
 this function will also delete the CookieMap from PartitionedCookieMap.

 This may invalidate the |cookie_partition_it| argument.

### GarbageCollect

CookieMonster::GarbageCollect
~~~cpp
size_t GarbageCollect(const base::Time& current, const std::string& key);
~~~
 If the number of cookies for CookieMap key |key|, or globally, are
 over the preset maximums above, garbage collect, first for the host and
 then globally.  See comments above garbage collection threshold
 constants for details. Also removes expired cookies.


 Returns the number of cookies deleted (useful for debugging).

### GarbageCollectPartitionedCookies

CookieMonster::GarbageCollectPartitionedCookies
~~~cpp
size_t GarbageCollectPartitionedCookies(
      const base::Time& current,
      const CookiePartitionKey& cookie_partition_key,
      const std::string& key);
~~~
 Run garbage collection for PartitionedCookieMap keys |cookie_partition_key|
 and |key|.


 Partitioned cookies are subject to different limits than unpartitioned
 cookies in order to prevent leaking entropy about user behavior across
 cookie partitions.

### PurgeLeastRecentMatches

CookieMonster::PurgeLeastRecentMatches
~~~cpp
size_t PurgeLeastRecentMatches(CookieItVector* cookies,
                                 CookiePriority priority,
                                 size_t to_protect,
                                 size_t purge_goal,
                                 bool protect_secure_cookies);
~~~
 Helper for GarbageCollect(). Deletes up to |purge_goal| cookies with a
 priority less than or equal to |priority| from |cookies|, while ensuring
 that at least the |to_protect| most-recent cookies are retained.

 |protected_secure_cookies| specifies whether or not secure cookies should
 be protected from deletion.


 |cookies| must be sorted from least-recent to most-recent.


 Returns the number of cookies deleted.

### GarbageCollectExpired

CookieMonster::GarbageCollectExpired
~~~cpp
size_t GarbageCollectExpired(const base::Time& current,
                               const CookieMapItPair& itpair,
                               CookieItVector* cookie_its);
~~~
 Helper for GarbageCollect(); can be called directly as well.  Deletes all
 expired cookies in |itpair|.  If |cookie_its| is non-NULL, all the
 non-expired cookies from |itpair| are appended to |cookie_its|.


 Returns the number of cookies deleted.

### GarbageCollectExpiredPartitionedCookies

CookieMonster::GarbageCollectExpiredPartitionedCookies
~~~cpp
size_t GarbageCollectExpiredPartitionedCookies(
      const base::Time& current,
      const PartitionedCookieMap::iterator& cookie_partition_it,
      const CookieMapItPair& itpair,
      CookieItVector* cookie_its);
~~~
 Deletes all expired cookies in the double-keyed PartitionedCookie map in
 the CookieMap at |cookie_partition_it|. It deletes all cookies in that
 CookieMap in |itpair|. If |cookie_its| is non-NULL, all non-expired cookies
 from |itpair| are appended to |cookie_its|.


 Returns the number of cookies deleted.

### GarbageCollectAllExpiredPartitionedCookies

CookieMonster::GarbageCollectAllExpiredPartitionedCookies
~~~cpp
void GarbageCollectAllExpiredPartitionedCookies(const base::Time& current);
~~~
 Helper function to garbage collect all expired cookies in
 PartitionedCookieMap.

### GarbageCollectDeleteRange

CookieMonster::GarbageCollectDeleteRange
~~~cpp
size_t GarbageCollectDeleteRange(const base::Time& current,
                                   DeletionCause cause,
                                   CookieItVector::iterator cookie_its_begin,
                                   CookieItVector::iterator cookie_its_end);
~~~
 Helper for GarbageCollect(). Deletes all cookies in the range specified by
 [|it_begin|, |it_end|). Returns the number of cookies deleted.

### GarbageCollectLeastRecentlyAccessed

CookieMonster::GarbageCollectLeastRecentlyAccessed
~~~cpp
size_t GarbageCollectLeastRecentlyAccessed(const base::Time& current,
                                             const base::Time& safe_date,
                                             size_t purge_goal,
                                             CookieItVector cookie_its,
                                             base::Time* earliest_time);
~~~
 Helper for GarbageCollect(). Deletes cookies in |cookie_its| from least to
 most recently used, but only before |safe_date|. Also will stop deleting
 when the number of remaining cookies hits |purge_goal|.


 Sets |earliest_time| to be the earliest last access time of a cookie that
 was not deleted, or base::Time() if no such cookie exists.

### HasCookieableScheme

CookieMonster::HasCookieableScheme
~~~cpp
bool HasCookieableScheme(const GURL& url);
~~~

### GetAccessSemanticsForCookie

CookieMonster::GetAccessSemanticsForCookie
~~~cpp
CookieAccessSemantics GetAccessSemanticsForCookie(
      const CanonicalCookie& cookie) const;
~~~
 Get the cookie's access semantics (LEGACY or NONLEGACY), by checking for a
 value from the cookie access delegate, if it is non-null. Otherwise returns
 UNKNOWN.

### RecordPeriodicStats

CookieMonster::RecordPeriodicStats
~~~cpp
void RecordPeriodicStats(const base::Time& current_time);
~~~
 Statistics support
 This function should be called repeatedly, and will record
 statistics if a sufficient time period has passed.

### DoRecordPeriodicStats

CookieMonster::DoRecordPeriodicStats
~~~cpp
bool DoRecordPeriodicStats();
~~~
 Records the aforementioned stats if we have already finished loading all
 cookies. Returns whether stats were recorded.

### RecordPeriodicFirstPartySetsStats

CookieMonster::RecordPeriodicFirstPartySetsStats
~~~cpp
void RecordPeriodicFirstPartySetsStats(
      base::flat_map<SchemefulSite, FirstPartySetEntry> sets) const;
~~~
 Records periodic stats related to First-Party Sets usage. Note that since
 First-Party Sets presents a potentially asynchronous interface, these stats
 may be collected asynchronously w.r.t. the rest of the stats collected by
 `RecordPeriodicStats`.

### DoCookieCallback

CookieMonster::DoCookieCallback
~~~cpp
void DoCookieCallback(base::OnceClosure callback);
~~~
 Defers the callback until the full coookie database has been loaded. If
 it's already been loaded, runs the callback synchronously.

### DoCookieCallbackForURL

CookieMonster::DoCookieCallbackForURL
~~~cpp
void DoCookieCallbackForURL(base::OnceClosure callback, const GURL& url);
~~~
 Defers the callback until the cookies relevant to given URL have been
 loaded. If they've already been loaded, runs the callback synchronously.

### DoCookieCallbackForHostOrDomain

CookieMonster::DoCookieCallbackForHostOrDomain
~~~cpp
void DoCookieCallbackForHostOrDomain(base::OnceClosure callback,
                                       base::StringPiece host_or_domain);
~~~
 Defers the callback until the cookies relevant to given host or domain
 have been loaded. If they've already been loaded, runs the callback
 synchronously.

### IsCookieSentToSamePortThatSetIt

CookieMonster::IsCookieSentToSamePortThatSetIt
~~~cpp
static CookieSentToSamePort IsCookieSentToSamePortThatSetIt(
      const GURL& destination,
      int source_port,
      CookieSourceScheme source_scheme);
~~~
 Checks to see if a cookie is being sent to the same port it was set by. For
 metrics.


 This is in CookieMonster because only CookieMonster uses it. It's otherwise
 a standalone utility function.

### domain_purged_keys_ GUARDED_BY_CONTEXT

CookieMonster::domain_purged_keys_ GUARDED_BY_CONTEXT
~~~cpp
std::set<std::string> domain_purged_keys_ GUARDED_BY_CONTEXT(thread_checker_);
~~~
 Set of keys (eTLD+1's) for which non-expired cookies have
 been evicted for hitting the per-domain max. The size of this set is
 histogrammed periodically. The size is limited to |kMaxDomainPurgedKeys|.

### GUARDED_BY_CONTEXT

CookieMonster::GUARDED_BY_CONTEXT
~~~cpp
CookieMap cookies_ GUARDED_BY_CONTEXT(thread_checker_);
~~~

### partitioned_cookies_ GUARDED_BY_CONTEXT

CookieMonster::partitioned_cookies_ GUARDED_BY_CONTEXT
~~~cpp
PartitionedCookieMap partitioned_cookies_ GUARDED_BY_CONTEXT(thread_checker_);
~~~

### tasks_pending_for_key_ GUARDED_BY_CONTEXT

CookieMonster::tasks_pending_for_key_ GUARDED_BY_CONTEXT
~~~cpp
std::map<std::string, base::circular_deque<base::OnceClosure>>
      tasks_pending_for_key_ GUARDED_BY_CONTEXT(thread_checker_);
~~~
 Map of domain keys to their associated task queues. These tasks are blocked
 until all cookies for the associated domain key eTLD+1 are loaded from the
 backend store.

### tasks_pending_
      GUARDED_BY_CONTEXT

CookieMonster::tasks_pending_
      GUARDED_BY_CONTEXT
~~~cpp
base::circular_deque<base::OnceClosure> tasks_pending_
      GUARDED_BY_CONTEXT(thread_checker_);
~~~
 Queues tasks that are blocked until all cookies are loaded from the backend
 store.

## class CookieMonster::PersistentCookieStore

### PersistentCookieStore

PersistentCookieStore
~~~cpp
PersistentCookieStore(const PersistentCookieStore&) = delete;
~~~

### operator=

CookieMonster::PersistentCookieStore::operator=
~~~cpp
PersistentCookieStore& operator=(const PersistentCookieStore&) = delete;
~~~

### Load

CookieMonster::PersistentCookieStore::Load
~~~cpp
virtual void Load(LoadedCallback loaded_callback,
                    const NetLogWithSource& net_log) = 0;
~~~
 Initializes the store and retrieves the existing cookies. This will be
 called only once at startup. The callback will return all the cookies
 that are not yet returned to CookieMonster by previous priority loads.


 |loaded_callback| may not be NULL.

 |net_log| is a NetLogWithSource that may be copied if the persistent
 store wishes to log NetLog events.

### LoadCookiesForKey

CookieMonster::PersistentCookieStore::LoadCookiesForKey
~~~cpp
virtual void LoadCookiesForKey(const std::string& key,
                                 LoadedCallback loaded_callback) = 0;
~~~
 Does a priority load of all cookies for the domain key (eTLD+1). The
 callback will return all the cookies that are not yet returned by previous
 loads, which includes cookies for the requested domain key if they are not
 already returned, plus all cookies that are chain-loaded and not yet
 returned to CookieMonster.


 |loaded_callback| may not be NULL.

### AddCookie

CookieMonster::PersistentCookieStore::AddCookie
~~~cpp
virtual void AddCookie(const CanonicalCookie& cc) = 0;
~~~

### UpdateCookieAccessTime

CookieMonster::PersistentCookieStore::UpdateCookieAccessTime
~~~cpp
virtual void UpdateCookieAccessTime(const CanonicalCookie& cc) = 0;
~~~

### DeleteCookie

CookieMonster::PersistentCookieStore::DeleteCookie
~~~cpp
virtual void DeleteCookie(const CanonicalCookie& cc) = 0;
~~~

### SetForceKeepSessionState

CookieMonster::PersistentCookieStore::SetForceKeepSessionState
~~~cpp
virtual void SetForceKeepSessionState() = 0;
~~~
 Instructs the store to not discard session only cookies on shutdown.

### SetBeforeCommitCallback

CookieMonster::PersistentCookieStore::SetBeforeCommitCallback
~~~cpp
virtual void SetBeforeCommitCallback(base::RepeatingClosure callback) = 0;
~~~
 Sets a callback that will be run before the store flushes.  If |callback|
 performs any async operations, the store will not wait for those to finish
 before flushing.

### Flush

CookieMonster::PersistentCookieStore::Flush
~~~cpp
virtual void Flush(base::OnceClosure callback) = 0;
~~~
 Flushes the store and posts |callback| when complete. |callback| may be
 NULL.

### ~PersistentCookieStore

~PersistentCookieStore
~~~cpp
virtual ~PersistentCookieStore() = default;
~~~
