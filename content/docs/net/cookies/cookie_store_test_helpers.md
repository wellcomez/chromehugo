### FutureCookieExpirationString

FutureCookieExpirationString
~~~cpp
std::string FutureCookieExpirationString();
~~~
 Returns a cookie expiration string in the form of "; expires=<date>", where
 date is an RFC 7231 date a year in the future, which can be appended to
 cookie lines.

## class DelayedCookieMonsterChangeDispatcher

### DelayedCookieMonsterChangeDispatcher

DelayedCookieMonsterChangeDispatcher::DelayedCookieMonsterChangeDispatcher
~~~cpp
DelayedCookieMonsterChangeDispatcher();
~~~

### DelayedCookieMonsterChangeDispatcher

DelayedCookieMonsterChangeDispatcher
~~~cpp
DelayedCookieMonsterChangeDispatcher(
      const DelayedCookieMonsterChangeDispatcher&) = delete;
~~~

### operator=

operator=
~~~cpp
DelayedCookieMonsterChangeDispatcher& operator=(
      const DelayedCookieMonsterChangeDispatcher&) = delete;
~~~

### ~DelayedCookieMonsterChangeDispatcher

DelayedCookieMonsterChangeDispatcher::~DelayedCookieMonsterChangeDispatcher
~~~cpp
~DelayedCookieMonsterChangeDispatcher() override;
~~~

### AddCallbackForCookie

DelayedCookieMonsterChangeDispatcher::AddCallbackForCookie
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForCookie(
      const GURL& url,
      const std::string& name,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~
 net::CookieChangeDispatcher
### AddCallbackForUrl

DelayedCookieMonsterChangeDispatcher::AddCallbackForUrl
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForUrl(
      const GURL& url,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~

### AddCallbackForAllChanges

DelayedCookieMonsterChangeDispatcher::AddCallbackForAllChanges
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription>
  AddCallbackForAllChanges(CookieChangeCallback callback) override;
~~~

### DelayedCookieMonsterChangeDispatcher

DelayedCookieMonsterChangeDispatcher
~~~cpp
DelayedCookieMonsterChangeDispatcher(
      const DelayedCookieMonsterChangeDispatcher&) = delete;
~~~

### operator=

operator=
~~~cpp
DelayedCookieMonsterChangeDispatcher& operator=(
      const DelayedCookieMonsterChangeDispatcher&) = delete;
~~~

### AddCallbackForCookie

DelayedCookieMonsterChangeDispatcher::AddCallbackForCookie
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForCookie(
      const GURL& url,
      const std::string& name,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~
 net::CookieChangeDispatcher
### AddCallbackForUrl

DelayedCookieMonsterChangeDispatcher::AddCallbackForUrl
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription> AddCallbackForUrl(
      const GURL& url,
      const absl::optional<CookiePartitionKey>& cookie_partition_key,
      CookieChangeCallback callback) override;
~~~

### AddCallbackForAllChanges

DelayedCookieMonsterChangeDispatcher::AddCallbackForAllChanges
~~~cpp
[[nodiscard]] std::unique_ptr<CookieChangeSubscription>
  AddCallbackForAllChanges(CookieChangeCallback callback) override;
~~~

## class DelayedCookieMonster

### DelayedCookieMonster

DelayedCookieMonster::DelayedCookieMonster
~~~cpp
DelayedCookieMonster();
~~~

### DelayedCookieMonster

DelayedCookieMonster
~~~cpp
DelayedCookieMonster(const DelayedCookieMonster&) = delete;
~~~

### operator=

operator=
~~~cpp
DelayedCookieMonster& operator=(const DelayedCookieMonster&) = delete;
~~~

### ~DelayedCookieMonster

DelayedCookieMonster::~DelayedCookieMonster
~~~cpp
~DelayedCookieMonster() override;
~~~

### SetCanonicalCookieAsync

DelayedCookieMonster::SetCanonicalCookieAsync
~~~cpp
void SetCanonicalCookieAsync(
      std::unique_ptr<CanonicalCookie> cookie,
      const GURL& source_url,
      const CookieOptions& options,
      SetCookiesCallback callback,
      const absl::optional<CookieAccessResult> cookie_access_result =
          absl::nullopt) override;
~~~
 Call the asynchronous CookieMonster function, expect it to immediately
 invoke the internal callback.

 Post a delayed task to invoke the original callback with the results.

### GetCookieListWithOptionsAsync

DelayedCookieMonster::GetCookieListWithOptionsAsync
~~~cpp
void GetCookieListWithOptionsAsync(
      const GURL& url,
      const CookieOptions& options,
      const CookiePartitionKeyCollection& cookie_partition_key_collection,
      GetCookieListCallback callback) override;
~~~

### GetAllCookiesAsync

DelayedCookieMonster::GetAllCookiesAsync
~~~cpp
void GetAllCookiesAsync(GetAllCookiesCallback callback) override;
~~~

### DeleteCanonicalCookieAsync

DelayedCookieMonster::DeleteCanonicalCookieAsync
~~~cpp
void DeleteCanonicalCookieAsync(const CanonicalCookie& cookie,
                                  DeleteCallback callback) override;
~~~

### DeleteAllCreatedInTimeRangeAsync

DelayedCookieMonster::DeleteAllCreatedInTimeRangeAsync
~~~cpp
void DeleteAllCreatedInTimeRangeAsync(
      const CookieDeletionInfo::TimeRange& creation_range,
      DeleteCallback callback) override;
~~~

### DeleteAllMatchingInfoAsync

DelayedCookieMonster::DeleteAllMatchingInfoAsync
~~~cpp
void DeleteAllMatchingInfoAsync(net::CookieDeletionInfo delete_info,
                                  DeleteCallback callback) override;
~~~

### DeleteSessionCookiesAsync

DelayedCookieMonster::DeleteSessionCookiesAsync
~~~cpp
void DeleteSessionCookiesAsync(DeleteCallback) override;
~~~

### DeleteMatchingCookiesAsync

DelayedCookieMonster::DeleteMatchingCookiesAsync
~~~cpp
void DeleteMatchingCookiesAsync(DeletePredicate, DeleteCallback) override;
~~~

### FlushStore

DelayedCookieMonster::FlushStore
~~~cpp
void FlushStore(base::OnceClosure callback) override;
~~~

###  GetChangeDispatcher


~~~cpp
CookieChangeDispatcher& GetChangeDispatcher() override;
~~~
### SetCookieableSchemes

DelayedCookieMonster::SetCookieableSchemes
~~~cpp
void SetCookieableSchemes(const std::vector<std::string>& schemes,
                            SetCookieableSchemesCallback callback) override;
~~~

### SetCookiesInternalCallback

DelayedCookieMonster::SetCookiesInternalCallback
~~~cpp
void SetCookiesInternalCallback(CookieAccessResult result);
~~~
 Be called immediately from CookieMonster.

### GetCookiesWithOptionsInternalCallback

DelayedCookieMonster::GetCookiesWithOptionsInternalCallback
~~~cpp
void GetCookiesWithOptionsInternalCallback(const std::string& cookie);
~~~

### GetCookieListWithOptionsInternalCallback

DelayedCookieMonster::GetCookieListWithOptionsInternalCallback
~~~cpp
void GetCookieListWithOptionsInternalCallback(
      const CookieAccessResultList& cookie,
      const CookieAccessResultList& excluded_cookies);
~~~

### InvokeSetCookiesCallback

DelayedCookieMonster::InvokeSetCookiesCallback
~~~cpp
void InvokeSetCookiesCallback(CookieMonster::SetCookiesCallback callback);
~~~
 Invoke the original callbacks.

### InvokeGetCookieListCallback

DelayedCookieMonster::InvokeGetCookieListCallback
~~~cpp
void InvokeGetCookieListCallback(
      CookieMonster::GetCookieListCallback callback);
~~~

### cookie_monster_



~~~cpp

std::unique_ptr<CookieMonster> cookie_monster_;

~~~


### change_dispatcher_



~~~cpp

DelayedCookieMonsterChangeDispatcher change_dispatcher_;

~~~


### did_run_



~~~cpp

bool did_run_ = false;

~~~


### result_



~~~cpp

CookieAccessResult result_;

~~~


### cookie_



~~~cpp

std::string cookie_;

~~~


### cookie_line_



~~~cpp

std::string cookie_line_;

~~~


### cookie_access_result_list_



~~~cpp

CookieAccessResultList cookie_access_result_list_;

~~~


### cookie_list_



~~~cpp

CookieList cookie_list_;

~~~


### DelayedCookieMonster

DelayedCookieMonster
~~~cpp
DelayedCookieMonster(const DelayedCookieMonster&) = delete;
~~~

### operator=

operator=
~~~cpp
DelayedCookieMonster& operator=(const DelayedCookieMonster&) = delete;
~~~

### SetCanonicalCookieAsync

DelayedCookieMonster::SetCanonicalCookieAsync
~~~cpp
void SetCanonicalCookieAsync(
      std::unique_ptr<CanonicalCookie> cookie,
      const GURL& source_url,
      const CookieOptions& options,
      SetCookiesCallback callback,
      const absl::optional<CookieAccessResult> cookie_access_result =
          absl::nullopt) override;
~~~
 Call the asynchronous CookieMonster function, expect it to immediately
 invoke the internal callback.

 Post a delayed task to invoke the original callback with the results.

### GetCookieListWithOptionsAsync

DelayedCookieMonster::GetCookieListWithOptionsAsync
~~~cpp
void GetCookieListWithOptionsAsync(
      const GURL& url,
      const CookieOptions& options,
      const CookiePartitionKeyCollection& cookie_partition_key_collection,
      GetCookieListCallback callback) override;
~~~

### GetAllCookiesAsync

DelayedCookieMonster::GetAllCookiesAsync
~~~cpp
void GetAllCookiesAsync(GetAllCookiesCallback callback) override;
~~~

### DeleteCanonicalCookieAsync

DelayedCookieMonster::DeleteCanonicalCookieAsync
~~~cpp
void DeleteCanonicalCookieAsync(const CanonicalCookie& cookie,
                                  DeleteCallback callback) override;
~~~

### DeleteAllCreatedInTimeRangeAsync

DelayedCookieMonster::DeleteAllCreatedInTimeRangeAsync
~~~cpp
void DeleteAllCreatedInTimeRangeAsync(
      const CookieDeletionInfo::TimeRange& creation_range,
      DeleteCallback callback) override;
~~~

### DeleteAllMatchingInfoAsync

DelayedCookieMonster::DeleteAllMatchingInfoAsync
~~~cpp
void DeleteAllMatchingInfoAsync(net::CookieDeletionInfo delete_info,
                                  DeleteCallback callback) override;
~~~

### DeleteSessionCookiesAsync

DelayedCookieMonster::DeleteSessionCookiesAsync
~~~cpp
void DeleteSessionCookiesAsync(DeleteCallback) override;
~~~

### DeleteMatchingCookiesAsync

DelayedCookieMonster::DeleteMatchingCookiesAsync
~~~cpp
void DeleteMatchingCookiesAsync(DeletePredicate, DeleteCallback) override;
~~~

### FlushStore

DelayedCookieMonster::FlushStore
~~~cpp
void FlushStore(base::OnceClosure callback) override;
~~~

###  GetChangeDispatcher


~~~cpp
CookieChangeDispatcher& GetChangeDispatcher() override;
~~~
### SetCookieableSchemes

DelayedCookieMonster::SetCookieableSchemes
~~~cpp
void SetCookieableSchemes(const std::vector<std::string>& schemes,
                            SetCookieableSchemesCallback callback) override;
~~~

### SetCookiesInternalCallback

DelayedCookieMonster::SetCookiesInternalCallback
~~~cpp
void SetCookiesInternalCallback(CookieAccessResult result);
~~~
 Be called immediately from CookieMonster.

### GetCookiesWithOptionsInternalCallback

DelayedCookieMonster::GetCookiesWithOptionsInternalCallback
~~~cpp
void GetCookiesWithOptionsInternalCallback(const std::string& cookie);
~~~

### GetCookieListWithOptionsInternalCallback

DelayedCookieMonster::GetCookieListWithOptionsInternalCallback
~~~cpp
void GetCookieListWithOptionsInternalCallback(
      const CookieAccessResultList& cookie,
      const CookieAccessResultList& excluded_cookies);
~~~

### InvokeSetCookiesCallback

DelayedCookieMonster::InvokeSetCookiesCallback
~~~cpp
void InvokeSetCookiesCallback(CookieMonster::SetCookiesCallback callback);
~~~
 Invoke the original callbacks.

### InvokeGetCookieListCallback

DelayedCookieMonster::InvokeGetCookieListCallback
~~~cpp
void InvokeGetCookieListCallback(
      CookieMonster::GetCookieListCallback callback);
~~~

### cookie_monster_



~~~cpp

std::unique_ptr<CookieMonster> cookie_monster_;

~~~


### change_dispatcher_



~~~cpp

DelayedCookieMonsterChangeDispatcher change_dispatcher_;

~~~


### did_run_



~~~cpp

bool did_run_ = false;

~~~


### result_



~~~cpp

CookieAccessResult result_;

~~~


### cookie_



~~~cpp

std::string cookie_;

~~~


### cookie_line_



~~~cpp

std::string cookie_line_;

~~~


### cookie_access_result_list_



~~~cpp

CookieAccessResultList cookie_access_result_list_;

~~~


### cookie_list_



~~~cpp

CookieList cookie_list_;

~~~


## class CookieURLHelper

### CookieURLHelper

CookieURLHelper::CookieURLHelper
~~~cpp
explicit CookieURLHelper(const std::string& url_string);
~~~

### domain

domain
~~~cpp
const std::string& domain() const { return domain_and_registry_; }
~~~

### host

host
~~~cpp
std::string host() const { return url_.host(); }
~~~

### url

url
~~~cpp
const GURL& url() const { return url_; }
~~~

### AppendPath

CookieURLHelper::AppendPath
~~~cpp
const GURL AppendPath(const std::string& path) const;
~~~

### Format

CookieURLHelper::Format
~~~cpp
std::string Format(const std::string& format_string) const;
~~~
 Return a new string with the following substitutions:
 1. "%R" -> Domain registry (i.e. "com")
 2. "%D" -> Domain + registry (i.e. "google.com")
### url_



~~~cpp

const GURL url_;

~~~


### registry_



~~~cpp

const std::string registry_;

~~~


### domain_and_registry_



~~~cpp

const std::string domain_and_registry_;

~~~


### domain

domain
~~~cpp
const std::string& domain() const { return domain_and_registry_; }
~~~

### host

host
~~~cpp
std::string host() const { return url_.host(); }
~~~

### url

url
~~~cpp
const GURL& url() const { return url_; }
~~~

### AppendPath

CookieURLHelper::AppendPath
~~~cpp
const GURL AppendPath(const std::string& path) const;
~~~

### Format

CookieURLHelper::Format
~~~cpp
std::string Format(const std::string& format_string) const;
~~~
 Return a new string with the following substitutions:
 1. "%R" -> Domain registry (i.e. "com")
 2. "%D" -> Domain + registry (i.e. "google.com")
### url_



~~~cpp

const GURL url_;

~~~


### registry_



~~~cpp

const std::string registry_;

~~~


### domain_and_registry_



~~~cpp

const std::string domain_and_registry_;

~~~


## class FlushablePersistentStore
 Mock PersistentCookieStore that keeps track of the number of Flush() calls.

### FlushablePersistentStore

FlushablePersistentStore::FlushablePersistentStore
~~~cpp
FlushablePersistentStore();
~~~

### Load

FlushablePersistentStore::Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~
 CookieMonster::PersistentCookieStore implementation:
### LoadCookiesForKey

FlushablePersistentStore::LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback loaded_callback) override;
~~~

### AddCookie

FlushablePersistentStore::AddCookie
~~~cpp
void AddCookie(const CanonicalCookie&) override;
~~~

### UpdateCookieAccessTime

FlushablePersistentStore::UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie&) override;
~~~

### DeleteCookie

FlushablePersistentStore::DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie&) override;
~~~

### SetForceKeepSessionState

FlushablePersistentStore::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

FlushablePersistentStore::SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

FlushablePersistentStore::Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### flush_count

FlushablePersistentStore::flush_count
~~~cpp
int flush_count();
~~~

### ~FlushablePersistentStore

FlushablePersistentStore::~FlushablePersistentStore
~~~cpp
~FlushablePersistentStore() override;
~~~

### 


~~~cpp
int flush_count_ = 0;
~~~

### flush_count_lock_



~~~cpp

base::Lock flush_count_lock_;

~~~


### 


~~~cpp
int flush_count_ = 0;
~~~

### Load

FlushablePersistentStore::Load
~~~cpp
void Load(LoadedCallback loaded_callback,
            const NetLogWithSource& net_log) override;
~~~
 CookieMonster::PersistentCookieStore implementation:
### LoadCookiesForKey

FlushablePersistentStore::LoadCookiesForKey
~~~cpp
void LoadCookiesForKey(const std::string& key,
                         LoadedCallback loaded_callback) override;
~~~

### AddCookie

FlushablePersistentStore::AddCookie
~~~cpp
void AddCookie(const CanonicalCookie&) override;
~~~

### UpdateCookieAccessTime

FlushablePersistentStore::UpdateCookieAccessTime
~~~cpp
void UpdateCookieAccessTime(const CanonicalCookie&) override;
~~~

### DeleteCookie

FlushablePersistentStore::DeleteCookie
~~~cpp
void DeleteCookie(const CanonicalCookie&) override;
~~~

### SetForceKeepSessionState

FlushablePersistentStore::SetForceKeepSessionState
~~~cpp
void SetForceKeepSessionState() override;
~~~

### SetBeforeCommitCallback

FlushablePersistentStore::SetBeforeCommitCallback
~~~cpp
void SetBeforeCommitCallback(base::RepeatingClosure callback) override;
~~~

### Flush

FlushablePersistentStore::Flush
~~~cpp
void Flush(base::OnceClosure callback) override;
~~~

### flush_count

FlushablePersistentStore::flush_count
~~~cpp
int flush_count();
~~~

### flush_count_lock_



~~~cpp

base::Lock flush_count_lock_;

~~~


## class CallbackCounter
 Counts the number of times Callback() has been run.

### CallbackCounter

CallbackCounter::CallbackCounter
~~~cpp
CallbackCounter();
~~~

### Callback

CallbackCounter::Callback
~~~cpp
void Callback();
~~~

### callback_count

CallbackCounter::callback_count
~~~cpp
int callback_count();
~~~

### ~CallbackCounter

CallbackCounter::~CallbackCounter
~~~cpp
~CallbackCounter();
~~~

### 


~~~cpp
int callback_count_ = 0;
~~~

### callback_count_lock_



~~~cpp

base::Lock callback_count_lock_;

~~~


### 


~~~cpp
int callback_count_ = 0;
~~~

### Callback

CallbackCounter::Callback
~~~cpp
void Callback();
~~~

### callback_count

CallbackCounter::callback_count
~~~cpp
int callback_count();
~~~

### callback_count_lock_



~~~cpp

base::Lock callback_count_lock_;

~~~

