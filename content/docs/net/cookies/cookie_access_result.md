### PrintTo

PrintTo
~~~cpp
inline void PrintTo(const CookieAccessResult& car, std::ostream* os) {
  *os << "{ { ";
  PrintTo(car.status, os);
  *os << " }, " << static_cast<int>(car.effective_same_site) << ", "
      << static_cast<int>(car.access_semantics) << ", "
      << car.is_allowed_to_access_secure_cookies << " }";
}
~~~
 Provided to allow gtest to create more helpful error messages, instead of
 printing hex.

## struct CookieAccessResult

### CookieAccessResult

CookieAccessResult::CookieAccessResult
~~~cpp
CookieAccessResult(CookieEffectiveSameSite effective_same_site,
                     CookieInclusionStatus status,
                     CookieAccessSemantics access_semantics,
                     bool is_allowed_to_access_secure_cookie)
~~~

### operator==

operator==
~~~cpp
explicit CookieAccessResult(CookieInclusionStatus status);

  CookieAccessResult(const CookieAccessResult& cookie_access_result);

  CookieAccessResult& operator=(const CookieAccessResult& cookie_access_result);

  CookieAccessResult(CookieAccessResult&& cookie_access_result);

  ~CookieAccessResult();

  bool operator==(const CookieAccessResult& other) const {
    return status == other.status &&
           effective_same_site == other.effective_same_site &&
           access_semantics == other.access_semantics &&
           is_allowed_to_access_secure_cookies ==
               other.is_allowed_to_access_secure_cookies;
  }
~~~
