import os

prefix = os.path.abspath(os.path.join(os.curdir, "content"))
root = os.path.join(os.curdir, "content/docs")
root = os.path.abspath(root)
baseURL = '/chromehugo'
google_root = os.path.join(root,"docs/website/site/")
# /home/z/dev/chromehugo/content/docs/docs/website/site/developers/design-documents/downloadmanagersequences/_index.md

import re
def find_image_positions(filename):
    """查找Markdown文件中图片链接的位置"""
    image_pattern = re.compile(r'(!\[.*?\])\((.*?)(\))')  # 匹配Markdown图片链接的正则表达式
    data = []
    with open(filename, 'r', encoding='utf-8') as file:
        content = file.read()
        positions = []  # 存储图片链接及其开始位置
        
        for match in image_pattern.finditer(content):
            link = match.group(1)  # 图片链接
            start_pos1 = match.start()  # 图片链接在文件中的起始位置
            positions.append((link, start_pos1))

            link = match.group(3)  # 图片链接
            start_pos2 = match.end()  # 图片链接在文件中的起始位置
            positions.append((link, start_pos2))
            image = match.group(2)

            old=content[start_pos1:start_pos2]
            x=os.path.dirname(filename).replace(prefix,"")

            # x="".join(x.split(".")[:-1])
            ss = "/chromehugo" + x+ "/"+image
            new_str="![](%s)"%ss
            print("+"*20+"\n")
            data.append((old,new_str))

    for x in data:
        content = content.replace(x[0],x[1]) 
    open(filename,"w").write(content)
    return positions

# 使用方法
replace = ["note/navigation.md",
'note/safe_browsing_navigation.md'
]



for file in replace:
    filename= os.path.join(root, file)
    image_positions =find_image_positions(filename)
    for link, position in image_positions:
        print(f"图片链接: {link}, 位置: {position}",len(link))
class menu:
    item: str


def get_all_files(dir, level=0) -> menu:
    index_md = os.path.join(dir, "_index.md")
    md_files = map(lambda a: os.path.join(dir, a), os.listdir(dir))

    def convert(md_file):
        flag= "->" if os.path.isdir(md_file) else " "
        if os.path.isfile(md_file):
            if md_file.split(".")[-1]!="md" :
                return None
        md_file = os.path.basename(md_file)
        if md_file == "_index.md":
            return None
        no_md = "".join(md_file.split(".")[:-1])
        return "- [%s%s](%s)" % ( no_md,flag, no_md)
    oldIndex= os.path.join(dir,"index.md")
    if os.path.exists(oldIndex):
        os.rename(oldIndex,index_md)
        pass
    else:
        # os.path.exists(index_md)==False:
        data = map(convert, md_files)
        data = filter(lambda a: a is not None, data)
        sss = ["---", "bookCollapseSection: true", "---", "\n" * 2]
        # sss.extend(list(data))
        index_data = "\n".join(sss)
        open(index_md, "w").write(index_data)

    name = dir.split("/")[-1]
    hugodir = dir.replace(prefix, "")
    # print("[%s](%s)"%(name,dir))
    ret = menu()
    ret.item = '''%s- [%s]({{< relref "%s" >}})''' % ('  ' * level if level > 0
                                                      else "", name, hugodir)
    return ret


get_all_files(root)


def walk(dir, ret, level=0):
    if os.path.isdir(dir):
        a = get_all_files(dir, level)
        ret.append(a)
        for d in os.listdir(dir):
            walk(os.path.join(dir, d), ret, level + 1)


ret = []
walk(root, ret, 0)

def add_google(file):
    pattern='''[<img alt="image"
src="/'''
    content=open(file,"r").read()
    a = content.replace(pattern,pattern[:-1]+"https://www.chromium.org/")
    open(file,"w").write(a)
def fix_google():
    def walk(dir, ret, level=0):
        if os.path.isfile(dir):
                add_google(dir)
                return
        for d in os.listdir(dir):
            walk(os.path.join(dir, d), ret, level + 1)
    walk(google_root, None, 0)

fix_google()
# sss = ["---", "headless: true", "---", "\n" * 2]
# sss.extend(list(map(lambda x: x.item, ret)))
# sss = "\n".join(sss)
# open(os.path.join(os.path.curdir, "content/menu/index.md"), "w").write(sss)
