import  os
import urllib
def walk(root):
    ret = [root]
    for a in os.listdir(root):
        path = os.path.join(root,a)
        # print(a)
        if os.path.isdir(path):
            # print(a)
            ret.extend(walk(path))
        else:
            continue
    return ret
import sys
page='''
---
---
目录'''
def run(root):
    root =os.path.abspath(root)
    dirs = walk(root)
    # print(dirs)

    def createindex(folder):
        removed = list(map(lambda x:x.upper(), ["readme.md","_sidebar.md","index.md","url.md"]))
        for a in os.listdir(folder):
            f = os.path.join(folder,a)
            if os.path.isdir(os.path.join(folder,a)):
                continue
            elif a.upper() in  removed:
                os.system("rm \"%s\""%(f))
                continue
            else:
                continue
        open(os.path.join(folder,"_index.md"),"w").write(page)
    for a in dirs:
        createindex(a)

# \[!\[@([\w\d]+)\]\(https://[\d|\w&=./-\?]+\)\]
if __name__ == "__main__":
    root =sys.argv[1]
    run(root=root)
