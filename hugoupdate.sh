#!/usr/bin/env bash
#主文件
doc=content/docs
rm -fr content/*
rm -fr public/*
rm -fr $doc
if [[ ! -d $doc ]]; then
  mkdir -p $doc
fi
cp -fr /chrome/buildcef/automate/ob/chromehugo/docs/* $doc/
rm -f $doc/template.md
python3 hugomenu.py
if [[ $1 == "build" ]]; then
  # python3 hugobook.py $doc
  bash hugobook_pagefind.sh
else
  hugo server
fi
